--// Migraate  to new Server at 2020-07-23 14:36:34


--7623 = 0 ->PROVIDERS
--2720 = 1 ->FUNTIONARIES


-- PROVIDERS FROM SOLOMON 
-- View for logins

select * from [192.168.20.240].integraapp.dbo.Vendor

--use sltestapp

select count(len) as 'count',len from sistemas.dbo.providers_view_vendors
group by len
where vendid = 'GCA950503PT4'


	select max(len(name)) as 'lenght',min(len(name)) from sistemas.dbo.providers_view_vendors
	
	select 
		len(name)
		,name
		,case 
		when 
			len(name) > 46
		then 
			left(name , 46 ) + '...'
		else 
			name 
		end as 'name'
		,len(left(name , 46 ) + '...') as 'new_len'
		,len(rtrim(ltrim(name))) as 'fixedLen'
	from
		sistemas.dbo.providers_view_vendors
	where 
		len(name) > 45
		
	
	


-- select * from portal_users.users where number_id = '2744'

-- update portal_users.users set group_id = 16 where number_id = '2744'



use sistemas
IF OBJECT_ID ('providers_view_vendors', 'V') IS NOT NULL
    DROP VIEW providers_view_vendors;

create
--alter 
view providers_view_vendors
with encryption
as 		
with "ven" as (
	select 
			 row_number() over(order by vendid) as 'id'
--			,newid() as 'newid'
			,ltrim(rtrim(vendid)) as 'vendid'
			,len(ltrim(rtrim(vendid))) as 'len'
			,isnumeric(replace(vendid,'-','')) as 'is_vendor'
			,row_number() over(partition by vendid,name order by vendid) as 'id_repeat'
			,name
			,Status
			,ClassID
			,tstamp 
	from 
		integraapp.dbo.Vendor
)
	select 
			 "vendor".id
--		 	,"vendor".newid
			,"vendor".vendid
			,"vendor".len
			,"vendor".is_vendor
			,"vendor".id_repeat
			,"vendor".name
			,"vendor".Status
			,"vendor".ClassID
			,"vendor".tstamp
	from 
			"ven" as "vendor" 
	where 
			is_vendor = 0

	
-- 		select * from sistemas.dbo.providers_view_vendors
	
-- Storage for files 

-- ctrl file and route file 

	select * from portal_users.users where username = 'CIJ1712199H0'

	
SHOW VARIABLES LIKE '%log%'
		
		
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Catalog Status 
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--truncate table sistemas.dbo.providers_parents
use [sistemas]
-- go
IF OBJECT_ID('dbo.providers_parents', 'U') IS NOT NULL 
  DROP TABLE dbo.providers_parents; 
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on
-- go
 
create table [dbo].[providers_parents](
		id						int identity(1,1),
		providers_parents_name	nvarchar(150) 		collate		sql_latin1_general_cp1_ci_as,
		created					datetime,
		modified				datetime,
		user_id					int,
		_status					tinyint default 1 null
) on [primary]
-- go

set ansi_padding off
-- go

insert into dbo.providers_parents values	  
										   ('File',CURRENT_TIMESTAMP,null,1,1)									-- 1
										  ,('binary',CURRENT_TIMESTAMP,null,1,1)									-- 1
--										  ,('order',CURRENT_TIMESTAMP,null,1,1)									-- 1
										;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Catalog Status Parents
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
use [sistemas]
-- go
-- 
--select * from sistemas.dbo.providers_standings
--truncate table sistemas.dbo.providers_standings

IF OBJECT_ID('providers_standings', 'U') IS NOT NULL 
  DROP TABLE providers_standings;  
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on
-- go
 
create table [providers_standings](
		id							int identity(1,1),
		providers_parents_id		int,
		providers_standings_name	nvarchar(150) 		collate		sql_latin1_general_cp1_ci_as,
		created						datetime,
		modified					datetime,
		user_id						int,
		_status						tinyint default 1 null
) on [primary]
-- go

set ansi_padding off
-- go

insert into dbo.providers_standings values	 (1,'xml',CURRENT_TIMESTAMP,null,1,1)												-- 1
											,(1,'voucher',CURRENT_TIMESTAMP,null,1,1)												-- 2
											,(1,'order',CURRENT_TIMESTAMP,null,1,1)												-- 2
											;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Controls providersControlsFiles
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
use [sistemas]
-- go

-- select * from sistemas.dbo.providers_controls_files

--truncate table  sistemas.dbo.providers_controls_files

IF OBJECT_ID('providers_controls_files', 'U') IS NOT NULL 
  DROP TABLE providers_controls_files; 
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on
-- go
 
create table [dbo].[providers_controls_files](
		id							bigint identity(1,1),
		providers_assoc_vendors_id	int null,
		user_id						int null,	
		labelname					char(255) null,
		_filename					nvarchar(250) 		collate		sql_latin1_general_cp1_ci_as null,
		_pathname					char(255) null,
		_extname					char(10) null,
		_md5sum						nvarchar(250)		collate		sql_latin1_general_cp1_ci_as null,
		_file_size					char(255)				collate		sql_latin1_general_cp1_ci_as null,
	 	_atime 						nvarchar(150)		collate		sql_latin1_general_cp1_ci_as null,
	 	_mtime						nvarchar(150)		collate		sql_latin1_general_cp1_ci_as null,
	 	_ctime						nvarchar(150)		collate		sql_latin1_general_cp1_ci_as null,
		_username					nvarchar(50) 		collate		sql_latin1_general_cp1_ci_as null,
		_datetime_login				datetime null,
		_ip_remote					nvarchar(25) 		collate		sql_latin1_general_cp1_ci_as null,
		created						datetime null,
		modified					datetime null,
		providers_standings_id		int null,
		providers_parents_id		int	null,
		_status						tinyint default 1 null
) on [primary]
-- go

set ansi_padding off
-- go
insert into sistemas.dbo.providers_controls_files values 
(
	 1,1,'filename.xml','xxxxxxxxxxxxx','url','xml','xxxxxxxxxxxxxxxxx','1','1','1','1','username',current_timestamp,'1.1.1.1',current_timestamp,current_timestamp,1,1,1
),
(
	 1,1,'filename1.pdf','xxxxxxxxxxxxx','url','pdf','xxxxxxxxxxxxxxxxx','1','1','1','1','username',current_timestamp,'1.1.1.1',current_timestamp,current_timestamp,2,1,1
),
(
	 1,1,'filename2.pdf','xxxxxxxxxxxxx','url','pdf','xxxxxxxxxxxxxxxxx','1','1','1','1','username',current_timestamp,'1.1.1.1',current_timestamp,current_timestamp,3,1,1
),
(
	 2,1,'otherfilename.xml','xxxxxxxxxxxxx','url','xml','xxxxxxxxxxxxxxxxx','1','1','1','1','username',current_timestamp,'1.1.1.1',current_timestamp,current_timestamp,1,1,1
),
(
	 2,1,'otrfilename1.pdf','xxxxxxxxxxxxx','url','pdf','xxxxxxxxxxxxxxxxx','1','1','1','1','username',current_timestamp,'1.1.1.1',current_timestamp,current_timestamp,2,1,1
),
(
	 2,1,'trefilename2.pdf','xxxxxxxxxxxxx','url','pdf','xxxxxxxxxxxxxxxxx','1','1','1','1','username',current_timestamp,'1.1.1.1',current_timestamp,current_timestamp,3,1,1
)

use sistemas
 	select * from sistemas.dbo.providers_assoc_vendors
--truncate table sistemas.dbo.providers_assoc_vendors
--drop table sistemas.dbo.providers_assoc_vendors 
IF OBJECT_ID('providers_assoc_vendors', 'U') IS NOT NULL 
  DROP TABLE providers_assoc_vendors; 
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on
-- go
 
create table [dbo].[providers_assoc_vendors](
		id								bigint identity(1,1),
		BatNbr							char(10) null,				
		PONbr							char(10) null,
		RefNbr							char(10) null,
		VendId							char(15) null,
		CpnyId							char(10) null,
		created							datetime null,
		modified						datetime null,
		providers_standings_id			int null,
		providers_parents_id			int	null,
		_status							tinyint default 1 null
) on [primary]
-- go

set ansi_padding off
-- go
insert into sistemas.dbo.providers_assoc_vendors values
(
	49635,03562,'qweq5165ads5','TEICUA',current_timestamp,current_timestamp,1,1,1
),
(
	49636,03563,'qweq5165ads5','TEICUA',current_timestamp,current_timestamp,1,1,1
)




--view thats join all squemas 
		select 
--			 "docs".id
			 "files".id
			,"docs".BatNbr
			,"docs".PONbr
			,"docs".VendId
			,"docs".CpnyId
--			,"docs".created
--			,"docs".modified
--			,"docs".providers_standings_id
--			,"docs".providers_parents_id
--			,"docs"._status
			,"files".providers_assoc_vendors_id
			,"files".user_id
			,"files".labelname
			,"files"._filename
			,"files"._pathname
--			,"files"._extname
			,"files"._md5sum
--			,"files"._file_size
--			,"files"._atime
--			,"files"._mtime
--			,"files"._ctime
--			,"files"._username
--			,"files"._datetime_login
--			,"files"._ip_remote
			,"files".created
			,"files".modified
--			,"files".providers_standings_id
--			,"files".providers_parents_id
--			,"files"._status
			,"stands".providers_standings_name
			,"parents".providers_parents_name
		from 
				sistemas.dbo.providers_assoc_vendors as "docs"
		left join
				sistemas.dbo.providers_controls_files as "files"
			on 
				"docs".id = "files".providers_assoc_vendors_id
			and 
				"docs".[_status] = 1 and "files".[_status] = 1
		inner join 
				sistemas.dbo.providers_standings as "stands"
			on 
				"stands".id = "files".providers_standings_id
		inner join
				sistemas.dbo.providers_parents as "parents"
			on
				"parents".id = "files".providers_parents_id	
			

-- =================================================================================================== --			
-- NOTE providers_vendor_files v 0.0.1			
-- =================================================================================================== --						
			
use sistemas
IF OBJECT_ID ('providers_vendor_files', 'V') IS NOT NULL		
    DROP VIEW providers_vendor_files
    
-- select * from sistemas.dbo.providers_vendor_files
-- select * from sistemas.dbo.providers_assoc_vendors
    
create
-- alter 
view providers_vendor_files
with encryption
as 
with "files" as (
		select 
				 "BatNbr"
				,"PONbr"
				,"Vendid"
				,"CpnyId"
				,"RefNbr"
				,[xml]
				,[voucher]
				,[order]
		from 
		(
				select 
					 "files".id
					,"docs".BatNbr
					,"docs".PONbr
					,"docs".VendId
					,"docs".CpnyId
					,"docs".RefNbr
					,"stands".providers_standings_name
				from 
						sistemas.dbo.providers_assoc_vendors as "docs"
				left join
						sistemas.dbo.providers_controls_files as "files"
					on 
						"docs".id = "files".providers_assoc_vendors_id
					and 
						"docs".[_status] = 1 and "files".[_status] = 1
				inner join 
						sistemas.dbo.providers_standings as "stands"
					on 
						"stands".id = "files".providers_standings_id
				inner join
						sistemas.dbo.providers_parents as "parents"
					on
						"parents".id = "files".providers_parents_id	
		) as "SourceTable"
		pivot
		(
			max(id)
				for providers_standings_name in 
											(
											 [xml]
											,[voucher]
											,[order]
											)
		) as PivotTable
)
	select 
		 "doc".BatNbr
		,"doc".PONbr
		,"doc".Vendid
		,"doc".CpnyId
		,"doc".RefNbr
		,"doc".[xml]
		,"doc".voucher
		,"doc".[order]
		,"file_xml".labelname as 'xml_name'
		,"file_xml".[_filename] as 'xml_src'
		,"file_voucher".labelname as 'voucher_name'
		,"file_voucher".[_filename] as 'voucher_src'
		,"file_order".labelname as 'order_name'
		,"file_order".[_filename] as 'order_src'
	from "files" as "doc"
	left join 
		sistemas.dbo.providers_controls_files as "file_xml"
	on 
		"file_xml".id = "doc".[xml]
	left join 
		sistemas.dbo.providers_controls_files as "file_voucher"
	on 
		"file_voucher".id = "doc".[voucher]
	left join 
		sistemas.dbo.providers_controls_files as "file_order"
	on 
		"file_order".id = "doc".[order]	
	
	
-- --REBUILT : 

-- --I know I can do this with a pivot like:

with cte as (
 select obsdata.name, obsdata.value, obs.lat, obs.lon, obs.created
 from obsdata
 left join obs on obs.id = obsdata.obs_id
)
-- pivot section
-- ======================================================================== --
select lat, lon, created, gender, type, description
from cte

pivot(
 max(value)
 for [name] in (gender, type, description)
) as pvt

-- ======================================================================== --
-- Another option is

Select A.lat
      ,A.lon
      ,A.created
      ,gender      = max(IIF(B.name='gender',B.value,null))
      ,type        = max(IIF(B.name='type',B.value,null))
      ,description = max(IIF(B.name='description',B.value,null))
 From  tbl_obs A
 Join  tbl_obsdata B on (A.id=B.obs_id)
 Group By A.lat
      ,A.lon
      ,A.created


use sistemas
IF OBJECT_ID ('providers_vendor_files_', 'V') IS NOT NULL		
    DROP VIEW providers_vendor_files_
    
-- select * from sistemas.dbo.providers_vendor_files
-- select * from sistemas.dbo.providers_assoc_vendors
-- NOTE version 1.0.0    
create
-- alter 
view providers_vendor_files
with encryption
as 
	select 
		 "docs".BatNbr
		,"docs".PONbr
		,"docs".VendId
		,"docs".CpnyId
		,"docs".RefNbr
		,[xml]			= max( case when "stands".providers_standings_name = 'xml' then "files".id else null end)
		,[voucher]      = max( case when "stands".providers_standings_name = 'voucher' then "files".id else null end)
		,[order]        = max( case when "stands".providers_standings_name = 'order' then "files".id else null end)
		,[xml_name]     = max( case when "stands".providers_standings_name = 'xml' then "files".labelname else null end)
		,[xml_src]   = max( case when "stands".providers_standings_name = 'xml' then "files".[_filename] else null end)		
		,[voucher_name]	= max( case when "stands".providers_standings_name = 'voucher' then "files".labelname else null end)
		,[voucher_src]	= max( case when "stands".providers_standings_name = 'voucher' then "files".[_filename] else null end)
		,[order_name]   = max( case when "stands".providers_standings_name = 'order' then "files".labelname else null end)
		,[order_src]    = max( case when "stands".providers_standings_name = 'order' then "files".[_filename] else null end)	
	from 
			sistemas.dbo.providers_assoc_vendors as "docs"
	left join
			sistemas.dbo.providers_controls_files as "files"
		on 
			"docs".id = "files".providers_assoc_vendors_id
		and 
			"docs".[_status] = 1 and "files".[_status] = 1
	inner join 
			sistemas.dbo.providers_standings as "stands"
		on 
			"stands".id = "files".providers_standings_id
	inner join
			sistemas.dbo.providers_parents as "parents"
		on
			"parents".id = "files".providers_parents_id	
--where "docs".BatNbr = '481583'
	group by 
		 "docs".BatNbr
		,"docs".PONbr
		,"docs".VendId
		,"docs".CpnyId
		,"docs".RefNbr

	
--select * from sistemas.dbo.providers_controls_files
--select * from sistemas.dbo.providers_vendor_files
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Controls providersControlsConciliations
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
use [sistemas]
-- go
IF OBJECT_ID('dbo.providers_controls_conciliations', 'U') IS NOT NULL 
  DROP TABLE dbo.providers_controls_conciliations; 
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on
-- go
 
create table [dbo].[providers_controls_conciliations](
		id							int identity(1,1),
		user_id						int,
		providers_controls_files_id	int,
		conciliations_count			int,
		created						datetime,
		modified					datetime,
		providers_standings_id		int,
		providers_parents_id		int,
		_status						tinyint default 1 null
) on [primary]
-- go

set ansi_padding off
-- go





--	select PONbr,* from integraapp.dbo.PurchOrd	



--check status of 

select
	
	 b.CpnyID
	,ap.VendId
	,b.BatNbr
	,b.Status
	,zp.ZAPBatNbr
	,ZP.ZEstatus
	,zp.ZUUID
from
		integraapp.dbo.Batch B
inner join 
		integraapp.dbo.APDoc AP 
	on
		B.CpnyID = AP.CpnyID
	and 
		B.BatNbr = AP.BatNbr
inner join 
		integraapp.dbo.ZPortalProv ZP 
	on
		AP.CpnyID = ZP.ZCpnyId
	and 
		AP.VendId = ZP.ZVendId
	and 
		AP.BatNbr = ZP.ZAPBatNbr
	and 
		B.Module = 'AP'
	and 
		B.Status = 'H'
where b.BatNbr in (471835,471840,466049)
--AND zp.ZEstatus = 's' AND zp.ZRcptDate >='2019-12-01 00:00:00'

	


--work from Hir



use sistemas
IF OBJECT_ID ('providers_view_relations', 'V') IS NOT NULL		
    DROP VIEW providers_view_relations
--select * from providers_vendor_files order by BatNbr desc
--select * from integraapp.dbo.ZPortalProv

create 
--	alter
view providers_view_relations
with encryption
as 
	select
			 row_number() over(order by "batch".BatNbr) as 'id'
			,"batch".BatNbr,"batch".CpnyID,"batch".Status as "xstatus","st".description as "Status","batch".Module,"batch".JrnlType,"batch".CuryCrTot
			,"ap".Status as 'ap_status',"ap".Acct as 'Account',"ap".CuryOrigDocAmt,"ap".CuryId,"ap".CuryRate
			,"ap".VendId,"ap".PerPost,"ap".PONbr,"ap".RefNbr,"ap".DocType,"ap".DocDesc
			-- FechaIngreso de Documento
			,"ap".DiscDate,"ap".DocDate
			-- Fecha Factura[Solomon] = Fecha Validacion
			,"ap".Crtd_DateTime,"ap".CuryEffDate,"ap".InvcDate,"ap".LUpd_DateTime
			-- FechaPago 
			,"ap".DueDate,"ap".PayDate
			,cast("ap".InvcDate as date) as 'InvDate',"ap".InvcNbr
			,"ap".Rlsed,"ap".User1
			,"terms".TermsId,"terms".DueIntrv,"terms".Descr
			,"vendor".name
--			,"file".CpnyId
			,"file".[xml]
			,"file".voucher
			,"file".[order]
			,"sxml".Acct
--			,"sxml".totalAmt
			,coalesce("sxml".totalAmt,"zuid".ZMontoPago) as 'totalAmt'
			,coalesce("sxml".UUID,"zuid".ZUUID,'') as 'UUID'
			,"sxml".UUID as 'suuid'
			,"uuid".uuid as 'xuuid'
			,"zuid".ZUUID
			,"uuid".created as 'FechaValidacion'
			,"file".xml_name
			,"file".xml_src
			,"file".voucher_name
			,"file".voucher_src
			,"file".order_name
			,"file".order_src
			,case  
				when len("ap".PONbr) > 0
					then 1
				else 0
			 end as 'isview'
	from 
			integraapp.dbo.Batch as "batch"
--			sltestapp.dbo.Batch as "batch"
	inner join 
			integraapp.dbo.APDoc as "ap"
--			sltestapp.dbo.APDoc as "ap"
		on 
				"batch".BatNbr = "ap".BatNbr 
			and 
				"batch".CpnyID = "ap".CpnyID
			and 
				"batch".Module = 'AP'		
			and 
				"ap".PerPost > 202000
	inner join 
			sistemas.dbo.providers_view_vendors as "vendor"
		on 
			"ap".VendId = "vendor".vendid
	left join 
			integraapp.dbo.Terms as "terms"
		on 
			"ap".Terms = "terms".TermsId
	left join 
			integraapp.dbo.GRW_CfdiInvcAddData as "sxml"
--			sltestapp.dbo.GRW_CfdiInvcAddData as "sxml"
		on 
			"batch".BatNbr = "sxml".batnbr 
		and 
			"ap".RefNbr = "sxml".refnbr 
		and 
			"batch".CpnyID = "sxml".cpnyId
	left join 
			sistemas.dbo.providers_vendor_files as "file"
		on
			"file".BatNbr /*collate SQL_Latin1_General_CP1_CI_AS*/ = "batch".BatNbr and "file".CpnyId /*collate SQL_Latin1_General_CP1_CI_AS*/ = "ap".CpnyID 
		and 
			"file".PONbr /*collate SQL_Latin1_General_CP1_CI_AS*/ = "ap".PONbr and "file".RefNbr /*collate SQL_Latin1_General_CP1_CI_AS*/ = "ap".RefNbr
		-- join upload files
	left join 
			sistemas.dbo.providers_uuid_requests as "uuid"
		on 	
--			"file".[xml] = "uuid".id
			"batch".BatNbr = "uuid".BatNbr /*collate SQL_Latin1_General_CP1_CI_AS*/ and "batch".CpnyID = "uuid".CpnyId /*collate SQL_Latin1_General_CP1_CI_AS*/
--		-- =================================================== --
	left join 
--			sltestapp.dbo.ZPortalProv as "zuid" --select * from 
			integraapp.dbo.ZPortalProv as "zuid"
		on 
			"batch".BatNbr = "zuid".ZAPBatNbr and "batch".CpnyID = "zuid".ZCpnyId and "ap".VendId = "zuid".ZVendId
			and 
				len("zuid".ZUUID) > 0  
	left join 
			sistemas.dbo.providers_status_definitions as "st"
		on 
			"batch".Status = "st".code /*collate SQL_Latin1_General_CP1_CI_AS*/
		
-- =================================================== --
--	where 
--				(	len("ap".PONbr) > 0 
--				 or 
--					"ap".PONbr is not null
--				)
			
--and 
--			"batch".BatNbr in (430113)
--

		
	select * from integraapp.dbo.Batch
		
	
	-- BUILT a better mechanism for date range
	
	select 
			PerPost
			,cast(substring( convert(nvarchar(MAX), InvcDate, 112) , 1, 6 ) as int) as 'post'
			,InvcDate
			,* 
	from 
			integraapp.dbo.APDoc where BatNbr = '481583'
		
		
	select cast(substring( convert(nvarchar(MAX), current_timestamp, 112) , 1, 6 ) as int) 


	-- BUILT a better mechanism for date range
		
		
	select * from sltestapp.dbo.ZPortalProv where ZAPBatNbr = 479080     
		
	select * from sltestapp.dbo.GRW_CfdiInvcAddData where BatNbr = 429733
		
	select len(PONbr) as 'len',rtrim(ltrim(PONbr)) as 'tr',* from sistemas.dbo.providers_view_relations 
		where InvcDate = '2020-01-30' and CpnyID = 'TBKGDL'
	
	
	and PONbr <> '' 


select len(PONbr) as 'len',rtrim(ltrim(PONbr)) as 'tr',* from sistemas.dbo.providers_view_relations
where BatNbr in (479080)


select * from sltestapp.dbo.APDoc where BatNbr in ('479080')
				



select len(PONbr) as 'len',rtrim(ltrim(PONbr)) as 'tr',* from sistemas.dbo.providers_view_relations
where year(InvcDate) >= '2019' and xstatus = 'H' and isview = 1 and Rlsed = 0

--754432,351931 dula 

--510346 baizabal

select * from sistemas.dbo.providers_controls_files
		
-- baizabal ene 2020 --> 665834


-- ============================================================================================================ --
-- 	View for check the amounts between xml and solomon --
-- ============================================================================================================ --
use sistemas

-- select * from sistemas.dbo.providers_view_batch_amounts where BatNbr = '490261'

IF OBJECT_ID ('providers_view_batch_amounts', 'V') IS NOT NULL		
    DROP VIEW providers_view_batch_amounts;
create    
--alter
	view providers_view_batch_amounts
with encryption
as 	
	select 
			 "batch".BatNbr
			,"batch".crtot
			,"batch".CuryCrTot
			,"batch".CpnyId
			,"batch".Crtd_DateTime
			,"batch".Crtd_User
	from  
			integraapp.dbo.batch as "batch" 
	where 
			"batch".Module = 'AP'	
	
-- ============================================================================================================ --




	select 
		 "cfdi".cpnyId
		,"cfdi".batnbr
		,"cfdi".refnbr
		,"cfdi".module
		,"cfdi".Acct
		,"cfdi".totalAmt
		,"cfdi".UUID
		,"cfdi".User1
		,"cfdi".User2
		,"cfdi".User3
		,"cfdi".User4
		,"cfdi".User5
		,"cfdi".User6
		,"cfdi".User7
		,"cfdi".User8
		,"cfdi".Line
		,"cfdi".tstamp
	from
		integraapp.dbo.GRW_CfdiInvcAddData as 'cfdi' -- Table from Almacen build in set update H to U

	select 
		 "xcee".Batnbr
		,"xcee".UUID
		,"xcee".RFC
		,"xcee".CpnyId
		,"xcee".Refnbr
		,"xcee".Account
		,"xcee".Monto
		,"xcee".Module
		,"xcee".LineRef
		,"xcee".CuryId
		,"xcee".CuryRate
		,"xcee".Serie
		,"xcee".Folio
		,"xcee".tipo
	from 
		integraapp.dbo.xCEEIDoc as "xcee"

		
-- Work from HIR		



use sistemas

IF OBJECT_ID('providers_uuid_requests', 'U') IS NOT NULL 
  DROP TABLE providers_uuid_requests;
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on
-- go
create table [dbo].[providers_uuid_requests](
		id								bigint identity(1,1),
		BatNbr							char(10) null,
		RefNbr							char(10) null,
		PONbr							char(10) null,
		VendId							char(15) null,
		CpnyId							char(10) null,
		-- Update from xml
		Sello							text null,
		Total							float(2) null,
		SubTotal						float(2) null,
		Certificado						text null,
		MetodoPago						char(10) null,
		NoCertificado					char(255) null,
		TipoDeComprobante				char(255) null,
		re								char(16) null,
		rr 								char(16) null,
		Unidad							char(255) null,
		ImporteConcepto					float(2) null,
		Cantidad 						int null,
		Descripcion 					char(255) null,
		ValorUnitario					float(2) null,
		TasaOCuota						float(6) null,
		ImporteTraslado					float(2) null,
		Impuesto 						char(15) null,
		uuid							char(36) null,
		SelloCFD						varchar(255) null,
		FechaTimbrado					datetime null,
		NoCertificadoSAT				char(255) null,
		Version 						char(255) null,
		selloSAT 						varchar(255) null,
		TermsId							char(2) null,
		DueIntrv						smallint null,
		InvDate							date null,
		InvcNbr							char(55) null,
--		rfc								char(16) null,
--		monto							float(2) null,
--		uuid							char(40) null,
		created							datetime null,
		modified						datetime null,
		providers_standings_id			int null,
		providers_parents_id			int	null,
		_status							tinyint default 1 null
) on [primary]
-- go
set ansi_padding off		
		


-- =========================================================================== --
-- Logs   select * from galdb.dbo.api_ws_historico_logs
-- =========================================================================== --

use sistemas

select * from sistemas.dbo.api_sat_historico_logs order by created desc

IF OBJECT_ID('dbo.api_sat_historico_logs', 'U') IS NOT NULL 
  DROP TABLE dbo.api_sat_historico_logs
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on
-- go
create table dbo.api_sat_historico_logs(
--		 id					 bigint					identity(1,1)
		 message		     text			    	null
		,created			 datetime 				default current_timestamp null
--		,modified			 datetime				null
		,status				 tinyint 				default 1 null
		,BatNbr				 char(10) 				null,
		
) on [primary]
-- go
set ansi_padding off


-- =========================================================================== --
-- Catalog of Solomon Status
-- =========================================================================== --

use sistemas

IF OBJECT_ID('sistemas.dbo.providers_status_definitions', 'U') IS NOT NULL 
  DROP TABLE sistemas.dbo.providers_status_definitions
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on
-- go
create table sistemas.dbo.providers_status_definitions(
		 id					 bigint					identity(1,1)
		,code		     	 char(1)			    null
		,description		 char(255)				null
		,created			 datetime 				default current_timestamp null
		,modified			 datetime				null
		,status				 tinyint 				default 1 null
) on [primary]
-- go
set ansi_padding off

insert into sistemas.dbo.providers_status_definitions
values 
        ('I','Parcialmente Cancelado',current_timestamp,current_timestamp,1)
	   ,('V','Cancelado',current_timestamp,current_timestamp,1)
	   ,('H','Retenido',current_timestamp,current_timestamp,1)
	   ,('B','Balanceado',current_timestamp,current_timestamp,1)
	   ,('S','Parcialmente Liberado',current_timestamp,current_timestamp,1)
	   ,('C','Completo',current_timestamp,current_timestamp,1)
	   ,('U','No Asentado',current_timestamp,current_timestamp,1)   
	   ,('P','Asentado',current_timestamp,current_timestamp,1)   
   
exec sp_who2


use integraapp

exec sp_helptext XZportalprov_Update_Ardoc


exec sp_helptext pp_03400
-- 	
-- save files to db 	
	
select Descr,DueIntrv,DueType,* from integraapp.dbo.Terms where NbrInstall > 0


select Status,CpnyID,JrnlType,Rlsed,LedgerID,Module,User1,* from integraapp.dbo.Batch where batnbr = '466050'
select Terms,Status,Rlsed,* from integraapp.dbo.APDoc where batnbr = '466050' 



select Status,CpnyID,JrnlType,Rlsed,LedgerID,Module,User1,* from sltestapp.dbo.Batch where batnbr = '477075'
select Terms,Status,Rlsed,* from sltestapp.dbo.APDoc where batnbr = '477075' 
select * from sistemas.dbo.providers_view_relations where batnbr = '477075'

-- === Search for more == --
--477075 477077 477079


select Rlsed,User1,Status,CpnyID,JrnlType,LedgerID,Module,* from sltestapp.dbo.Batch where batnbr in ('477075', '477077' ,'477079','471835','471840','466049','466050')

update sltestapp.dbo.Batch
	set Status = 'H'  where batnbr = '471835'
update sltestapp.dbo.ApDoc 
		set Rlsed = 0 where batnbr = '471835'  and Acct = '0201010000'

select Rlsed,User1,Terms,Status,* from sltestapp.dbo.APDoc where batnbr in ('477075', '477077' ,'477079','471835','471840','466049','466050')

select Rlsed,User1,Status,* from sistemas.dbo.providers_view_relations where batnbr in ('477075', '477077' ,'477079','471840','466049')


select * from sistemas.dbo.providers_uuid_requests where batnbr in ('477075', '477077' ,'477079','471835','471840','466049','466050')


select * from sltestapp.dbo.ZPortalProv

select * from sltestapp.dbo.ZDiasPago


--on Update
--crear trigger para insertar en grw-cfdi y xcee-fiscal

--== GET Examples === ---

select 
		"doc".Rlsed
		,"bat".Status
		,"bat".BatNbr
		,"doc".DocDate
		,"doc".InvcNbr
		,"doc".DocType
	,* 
from 
		sltestapp.dbo.Batch as "bat"
inner join 
		sltestapp.dbo.ApDoc as "doc" 
	on 
		"bat".BatNbr = "doc".BatNbr and "bat".CpnyID = "doc".CpnyID
where 
		"bat".Status = 'H'
	and 
		"bat".Module = 'AP'
	and 
		"doc".Rlsed = 0
	and 
		"doc".DocType = 'VO'
	and 
		year("doc".DocDate) = '2019'

--425771    
--425867    
--426094    
--445005    
--458467    
--461357    
--465698    
--429882    
--435440    
--441823    
--442326    
--453675    
--456110    

--	vendid = 'GCA950503PT4'

	use sistemas
-- ====================================================================================== --
-- DEBUG RESTORE SOME RECORD
-- ====================================================================================== --
-- WARNING check the route before execute

use sistemas	
declare @batnbr char(10) 
--set @batnbr = '490261' -- Montos Distintos
--set @batnbr = '487330' -- No carga XML
--set @batnbr = '481583' -- xml valido
--set @batnbr = '493550' -- xml structura con errores

--set @batnbr = '500518'  --normal process
--set @batnbr = '501585'  --normal process
--set @batnbr = '502585' --current test
set @batnbr = '481583' --good lote for test 
 
--	select * from
 delete 
 	sistemas.dbo.providers_controls_files
where providers_assoc_vendors_id = (select id from sistemas.dbo.providers_assoc_vendors where BatNbr = @batnbr)
	
	delete sistemas.dbo.providers_assoc_vendors where BatNbr = @batnbr

	delete sistemas.dbo.providers_uuid_requests where BatNbr = @batnbr

-- this affect to SL	
	
delete integraapp.dbo.GRW_CfdiInvcAddData where batnbr = @batnbr
delete integraapp.dbo.WrkRelease where BatNbr = @batnbr
delete integraapp.dbo.WrkReleaseBad where BatNbr = @batnbr
delete integraapp.dbo.xCEEIDoc where Batnbr = @batnbr
-- delete the zportal record 
--delete integraapp.dbo.ZPortalProv where ZAPBatNbr = @batnbr
update integraapp.dbo.Batch set Status = 'H' where BatNbr = @batnbr
update integraapp.dbo.APDoc set Rlsed = 0 where batnbr = @batnbr

use sistemas
select * from  providers_uuid_requests order by id desc

exec sp_helpserver


declare @batnbr char(10)
--set @batnbr = '490261' -- Montos Distintos
--set @batnbr = '487330' -- No carga XML


--set @batnbr = '472860' 
--set @batnbr = '490261' -- Montos Distintos
--set @batnbr = '487330' -- No carga XML
--set @batnbr = '481583' -- xml valido
--set @batnbr = '493550' -- xml structura con errores
--set @batnbr = '497457' -- fvalidation is null

--set @batnbr = '496328' -- failed extension upload

--set @batnbr = '500518' --check for some issues
--set @batnbr = '501585'
--set @batnbr = '502633'
--set @batnbr = '502585' --current test
set @batnbr = '481583' --good lote for test 

	select Status,Module,* from integraapp.dbo.Batch where BatNbr = @batnbr
	select Rlsed,* from integraapp.dbo.APDoc where BatNbr = @batnbr 
	
	select * from sistemas.dbo.providers_view_relations where BatNbr = @batnbr
	select * from sistemas.dbo.api_sat_historico_logs order by created desc
	select * from sistemas.dbo.providers_view_batch_amounts where BatNbr = @batnbr

	select * from sistemas.dbo.providers_controls_files where providers_assoc_vendors_id = (select id from sistemas.dbo.providers_assoc_vendors where BatNbr = @batnbr)
	select * from sistemas.dbo.providers_assoc_vendors where BatNbr = @batnbr
 	select * from sistemas.dbo.providers_uuid_requests where BatNbr = @batnbr
 	
-- 	select * from sistemas.dbo.providers_vendor_files
-- 	truncate table sistemas.dbo.providers_assoc_vendors
-- delete sistemas.dbo.providers_assoc_vendors where id = 15
--	work from hir
-- 	select * from sistemas.dbo.providers_view_vendors
	select * from sistemas.dbo.providers_vendor_files where BatNbr = @batnbr
	
--	update sltestapp.dbo.Batch set Status = 'H' where BatNbr = '471835'
	
	select * from integraapp.dbo.GRW_CfdiInvcAddData where batnbr = @batnbr --and totalAmt = 5377.18 and Acct = '0201010000'  -- Table from Almacen build in set update H to U
--	delete sltestapp.dbo.GRW_CfdiInvcAddData where batnbr = '471835'
	select * from integraapp.dbo.xCEEIDoc where Batnbr = @batnbr
	select * from integraapp.dbo.WrkRelease where BatNbr = @batnbr
	select * from integraapp.dbo.WrkReleaseBad where BatNbr = @batnbr


	
	
	
	select [_status] ,[_md5sum] ,* from sistemas.dbo.providers_controls_files where [_md5sum] = '2a9125b6086fedefd9526a5db421d635'

	select * from sistemas.dbo.providers_assoc_vendors where id = 
		( 
			select providers_assoc_vendors_id from sistemas.dbo.providers_controls_files where [_md5sum] = '9873152e3b80ea16a12745f84d702b72'
		)
	
		
	update 
	sistemas.dbo.providers_controls_files 
	set [_status] = 0
	where [_md5sum] = '9873152e3b80ea16a12745f84d702b72'
		
	delete from sistemas.dbo.providers_controls_files where [_md5sum] = '40f7721fa5b2e26ab0ec879c8f56fdf5'
	delete from sistemas.dbo.providers_controls_files where [_md5sum] = '6e3a884bd1bb162902d56c13f40f9d8c'
	delete from sistemas.dbo.providers_controls_files where [_md5sum] = '2a9125b6086fedefd9526a5db421d635'
--==
use sistemas

declare @batnbr char(10)
set @batnbr = '490261'

--set @batnbr = '487330' -- No carga XML
--set @batnbr = '494620'
--set @batnbr = '472862'

select crtot,* from sistemas.dbo.providers_view_batch_amounts where BatNbr = @batnbr
 
--select totalAmt,CuryOrigDocAmt,* from sistemas.dbo.providers_view_relations where BatNbr = @batnbr 
 
select Total,* from sistemas.dbo.providers_uuid_requests where BatNbr = @batnbr
 	
select * from sistemas.dbo.api_sat_historico_logs where BatNbr = '501563' order by created desc 


select * from sistemas.dbo.providers_view_relations where xuuid is not null

--

select * from sistemas.dbo.providers_assoc_vendors
--delete from sltestapp.dbo.GRW_CfdiInvcAddData where batnbr = '471835'
--delete from sltestapp.dbo.WrkRelease where BatNbr = '471835'

--
--      Select @ErrorMsg = Case @Msgid
--                          When 0    Then 'Batch okay, will release normally.'
--                          When -1   Then 'Error -1, APDocs have invalid Vendor ID.'
--                          When -2   Then 'Error -2, APDocs have invalid Tax ID.'
--                          When 6019 Then 'Error 6019, APTrans are missing or the sum of amounts does not match the APDoc'
--                          When 12008 Then 'Error 12008, Batch is already released'
--                          When 8058 Then 'Error 8058, CuryID does not exist in the Currncy table.'
--                          When 16210 Then 'Error 16210, Error in AP_ApplyPP or AP_UnapplyPP.'
--                          When 12902 Then 'Error 12902, Application amount is greater than the outstanding document balance.'
--                          Else 'UnKnown Error.'
--                       End
		
		 

select * 
-- delete 
from sltestapp.dbo.xCEEIDoc where uuid = '7818c27c-d184-4819-8c77-108cc6f63bd6' or lineref = '307206' or batnbr = '471835'


--start trigger

--create
-- 

use sistemas

--drop trigger trigger_insert_api_sat_providers
--disable trigger trigger_insert_api_sat_providers on sistemas.dbo.providers_uuid_requests

--exec sp_helptext trigger_insert_api_sat_providers

--for update trigger 
-- alter
-- create   
trigger trigger_insert_api_sat_providers
	ON sistemas.dbo.providers_uuid_requests
after insert
as
begin -- trigger

	set nocount on

--		NOTE set the primary var table for move data 
	declare 
		-- Start anticipo
			 @BatNbr					char(10)
		 	,@RefNbr					char(10)
			,@PONbr						char(10)
			,@VendId					char(15)
			,@CpnyId					char(10)
			,@Sello						varchar(3000)
			,@Total						float(2)					
			,@SubTotal					float(2)
			,@Certificado				varchar(3000)
			,@MetodoPago				char(10)
			,@NoCertificado				varchar(300)
			,@TipoDeComprobante			varchar(300)
			,@re						char(16)
			,@rr						char(16)
			,@Unidad					char(255)
			,@ImporteConcepto			float(2)
			,@Cantidad					int
			,@Descripcion				char(255)
			,@ValorUnitario				float(2)
			,@TasaOCuota				decimal(18,6)
			,@ImporteTraslado			float(2)
			,@Impuesto					char(15)
			,@uuid						char(36)
			,@SelloCFD					varchar(255)
			,@FechaTimbrado				datetime
			,@NoCertificadoSAT			varchar(255)
			,@Version					varchar(255)
			,@selloSAT					varchar(255)
			,@TermsId					char(2)
			,@DueIntrv					smallint
			,@InvcDate					datetime
			,@InvcNbr					char(55)
			,@created					datetime
			,@modified					datetime
			,@providers_standings_id	int
			,@providers_parents_id		int
			,@_status					tinyint
		-- start CapcomPowerSystem	
	-- Internal operation Variables
	declare 
			  @diaPago int
		 	 ,@line int
	    
-- CURRENT WORK FOR ADD THE LAST METHODS IN WS		
 
--	declare working table 

   declare @sat table (
  			 BatNbr						char(10)
		 	,RefNbr						char(10)
			,PONbr						char(10)
			,VendId						char(15)
			,CpnyId						char(10)
			,Sello						char(255)
			,Total						float(2)					
			,SubTotal					float(2)
			,Certificado				char(255)
			,MetodoPago					char(10)
			,NoCertificado				char(255)
			,TipoDeComprobante			char(255)
			,re							char(16)
			,rr							char(16)
			,Unidad						char(255)
			,ImporteConcepto			float(2)
			,Cantidad					int
			,Descripcion				char(255)
			,ValorUnitario				float(2)
			,TasaOCuota					decimal(18,6)
			,ImporteTraslado			float(2)
			,Impuesto					char(15)
			,uuid						char(36)
			,SelloCFD					char(255)
			,FechaTimbrado				datetime
			,NoCertificadoSAT			char(255)
			,Version					char(255)
			,selloSAT					char(255)
			,TermsId					char(2)
			,DueIntrv					smallint
			,InvcDate					datetime
			,InvcNbr					char(55)
			,created					datetime
			,modified					datetime
  
   )
--
 	select 
	 	 @BatNbr = "insert".BatNbr
	    ,@CpnyId = "insert".CpnyId
    	,@RefNbr = "insert".RefNbr
        ,@VendId = "insert".VendId
    	,@TermsId = "insert".TermsId
    	,@DueIntrv = "insert".DueIntrv
    	,@InvcDate = "insert".InvDate
    	,@InvcNbr = "insert".InvcNbr
    	,@uuid	=  "insert".uuid
		,@created = "insert".created 
		,@diaPago = "pago".ZDiaPago
	from 
		inserted  as "insert"
	left join 
		integraapp.dbo.ZDiasPago as "pago" 
	on 
		"pago".ZCpnyId = @CpnyId

	
	insert into sistemas.dbo.api_sat_historico_logs (message,status,Batnbr)
	values (
		 'init trigger proccess for  =>' + @Batnbr + 'uuid => ' + @uuid + ' and VendId => ' + @VendId
--		, current_timestamp
		, 1
		,@BatNbr
	)				
	
-- build the squema with procedures 							

if 	( select Status from integraapp.dbo.Batch where BatNbr = @BatNbr ) = 'H'
	begin 
--		select * from integraapp.dbo.WrkRelease where BatNbr = @batnbr
--		select * from integraapp.dbo.WrkReleaseBad where BatNbr = @batnbr
			
		set XACT_ABORT on   	
		
		
		
		-- =========================================================================== --
		--  trigger
		-- =========================================================================== --
		-- Next issue	
		
		--	begin TRY
		--		begin transaction apdoc_insert
			-- ========================================================================================== --	
					  Update "a"
					  Set "a".DueDate	 = integraapp.dbo.XGetDueDate(@created, @diaPago, @DueIntrv),
		--			      "a".InvcDate 	 = @InvDate,
		--			      "a".InvcNbr	 = @InvcNbr,
					      "a".PayDate	 = integraapp.dbo.XGetDueDate(@created, @diaPago, @DueIntrv),
		--			      "a".PerPost  	 = z.ZPerPost,
					      "a".User1    	 = 'EC'
		--			  From sltestapp.dbo.APDoc as "a"
					  From integraapp.dbo.APDoc as "a"
		--			  Inner Join ZPortalProv z (Nolock) On a.RefNbr    = z.ZAPRefno And
		--			                                                    a.CpnyId    = z.ZCpnyId	And
		--			                                                    a.VendId    = z.ZVendId And
		--			                                                    z.ZEstatus  = 'Z'
		--			       Inner Join Terms t (Nolock) On a.Terms = t.TermsId
		--			       Inner Join ZDiasPago (Nolock) d On a.CpnyId = d.ZCpnyId
					  Where 
					  		"a".DocType   = 'VO' 
			  			and 
					        "a".Rlsed     = 0   --Released
					    and 
					    	"a".BatNbr = @BatNbr
					    and 
					    	"a".CpnyId = @CpnyId
			-- ========================================================================================== --
					insert into sistemas.dbo.api_sat_historico_logs (message,status,Batnbr)
						values (
							 'Update DueDate and PayDate in ApDoc with BAtNbr => ' + @Batnbr 
--							, current_timestamp
							, 1
							, @BatNbr
						)
					    	
		--		commit transaction apdoc_insert
		--		
		--	end TRY
		---- block for prepare the procedure
		--	begin CATCH
		--
		--		    if (XACT_STATE()) = -1  
		--	    begin  
		--	        
		--	       	insert into sistemas.dbo.api_sat_historico_logs (message)
		--				values ('The transaction is in an uncommittable state.: Rolling back transaction.')
		--			rollback transaction apdoc_insert
		--	    end  
		--	    -- Test whether the transaction is committable.  
		--	    if (XACT_STATE()) = 1  
		--	    begin  
		--	       	insert into sistemas.dbo.api_sat_historico_logs (message)
		--				values ('The transaction is committable.: Committing transaction.')  
		--	        commit transaction apdoc_insert
		--	    end   
		--    
		--		insert into sistemas.dbo.api_sat_historico_logs (message)
		--		values (	
		--					'errNum : ' + cast(ERROR_NUMBER() as varchar(255)) + '| ' +
		--					'errLine : ' + cast(ERROR_LINE() as varchar(500)) + '| ' +
		--					'errMsg : ' + cast(ERROR_MESSAGE() as varchar(500)) + '| ' +
		--					'errState : ' + cast(ERROR_STATE() as varchar(255)) + '| ' +
		--					'errSeverity : ' + cast(ERROR_SEVERITY() as varchar(255)) + '| '
		--			   )
		--	
		--	end CATCH
		-- ======================================================================================================== --
			
		-- =========================================================================== --
		-- insert into wkreleased
		-- =========================================================================== --
		-- select * from integraapp.dbo.WrkRelease
		-- Next issue	
			begin TRY
				begin transaction wk
		--		  select * from [192.168.20.240]."integraapp"."dbo"."WrkRelease"
			  	  Insert Into "integraapp"."dbo"."WrkRelease"
					values (
				              @BatNbr, --BatNbr
					         'AP',	   --Module
					         'SQLEC',  --UserAddress
					         null      --tstamp
					       )
				commit transaction wk
			end TRY
			begin CATCH
		
			if (XACT_STATE()) = -1  
			    begin  
			        
			       	insert into "sistemas"."dbo"."api_sat_historico_logs" (message,status,Batnbr)
						values (
									'The transaction is in an uncommittable state.: Rolling back transaction.'
--									, current_timestamp
									, 9
									, @BatNbr
								)
					rollback transaction wk
			    end  
			    -- Test whether the transaction is committable.  
			 if (XACT_STATE()) = 1  
			    begin  
			       	insert into "sistemas"."dbo"."api_sat_historico_logs" (message,status,Batnbr)
						values (
									 'The transaction is committable.: Committing transaction.'
--									,current_timestamp
									,9
									, @BatNbr
								)  
			        commit transaction wk
			    end   
		    
				insert into "sistemas"."dbo"."api_sat_historico_logs" (message,status,Batnbr)
				values (	
							'errNum : ' + cast(ERROR_NUMBER() as varchar(255)) + '| ' +
							'errLine : ' + cast(ERROR_LINE() as varchar(500)) + '| ' +
							'errMsg : ' + cast(ERROR_MESSAGE() as varchar(500)) + '| ' +
							'errState : ' + cast(ERROR_STATE() as varchar(255)) + '| ' +
							'errSeverity : ' + cast(ERROR_SEVERITY() as varchar(255)) + '| ' +
							'batnbr : ' + @BatNbr 
--							,current_timestamp
							,8
							, @BatNbr
					   )
			
			end CATCH
			
				insert into sistemas.dbo.api_sat_historico_logs (message,status,Batnbr)
				values (
					 'Store  batnbr '+ @Batnbr +' for released in wrkReleased => '
--					, current_timestamp
					, 1
					, @BatNbr
				)
		-- ======================================================================================================== --
		--execute procedure
		--		Exec sltestapp.dbo.pp_03400 'SQLEC', 'SQLEC'
				exec integraapp.dbo.pp_03400 'SQLEC', 'SQLEC'
		
						
				insert into sistemas.dbo.api_sat_historico_logs (message,status,Batnbr)
				values (
					 'Executing store procedure pp_03400 ...' 
--					, current_timestamp
					, 7
					, @BatNbr
				)
		--	select (select (max(line) + 1) from [192.168.20.240].integraapp.dbo.GRW_CfdiInvcAddData)
			set @line =	(select (max(line) + 1) from integraapp.dbo.GRW_CfdiInvcAddData)
		-- free batch 
		-- insert into tables 
		--	insert into sltestapp.dbo.GRW_CfdiInvcAddData
			insert into integraapp.dbo.GRW_CfdiInvcAddData
				( 
					 cpnyId
					,batnbr
					,refnbr
					,module
					,Acct
					,totalAmt
					,UUID
					,User1
					,User2
					,User3
					,User4
					,User5
					,User6
					,User7
					,User8
		--			,Line
		--			,tstamp
				)
			select 
					 @CpnyId --"cfdi".cpnyId
					,@BatNbr --"cfdi".batnbr
					,@RefNbr --"cfdi".refnbr
					,"rel".Module
					,"rel".Account
					,"rel".CuryOrigDocAmt -- "cfdi".totalAmt
					,@uuid  --"cfdi".UUID
					,'' --"cfdi".User1 --null
					,'' --"cfdi".User2 --null
					,0 --"cfdi".User3
					,0 --"cfdi".User4
					,'' --"cfdi".User5 --null
					,'' --"cfdi".User6 --null
					,'1900-01-01 00:00:00' --"cfdi".User7 
					,'1900-01-01 00:00:00' --"cfdi".User8
		--			,@line --"cfdi".Line
		--			,null --"cfdi".tstamp
			from 
					sistemas.dbo.providers_view_relations as "rel"
			where 
					 	CpnyID = @CpnyId --"cfdi".cpnyId
					and
						BatNbr = @BatNbr --"cfdi".batnbr
					and 
						RefNbr = @RefNbr --"cfdi".refnbr
		
						
			insert into sistemas.dbo.api_sat_historico_logs (message,status,Batnbr)
			values (
				 'Insert into GRW_CfdiInvcAddData =>' + @Batnbr
--				, current_timestamp
				, 7
				, @BatNbr
			)				
		
		 
			insert into 
		--			sltestapp.dbo.xCEEIDoc (
					integraapp.dbo.xCEEIDoc (
				
						 Batnbr
						,UUID
						,RFC
						,CpnyId
						,Refnbr
						,Account
						,Monto
						,Module
		--				,LineRef
						,CuryId
						,CuryRate
						,Serie
						,Folio
						,tipo		
				)
			select 
						 @batnbr--"ee".Batnbr
						,@uuid --"ee".UUID
						,@VendId --"ee".RFC
						,@CpnyId --"ee".CpnyId
						,@RefNbr --"ee".Refnbr
						,"rlt".Account --"ee".Account
						,"rlt".CuryOrigDocAmt --"ee".Monto
						,"rlt".Module --"ee".Module
		--				,0 --"ee".LineRef
						,case
							when "rlt".CuryId = 'MN'--"ee".CuryId
								then 'MXN'
							else 
								"rlt".CuryId
						end 
						,"rlt".CuryRate  -- "ee".CuryRate
						,'' --"ee".Serie
						,'' --"ee".Folio
						,1 --"ee".tipo	
			from 
					sistemas.dbo.providers_view_relations as "rlt"
			where 
					 	CpnyID = @CpnyId --"cfdi".cpnyId
					and
						BatNbr = @BatNbr --"cfdi".batnbr
					and 
						RefNbr = @RefNbr --"cfdi".refnbr
						
			insert into sistemas.dbo.api_sat_historico_logs (message,status,Batnbr)
			values (
				 'Insert into xCEEIDoc =>' + @Batnbr
--				, current_timestamp
				, 7
				, @BatNbr
			)				
		
			
			insert into sistemas.dbo.api_sat_historico_logs (message,status,Batnbr)
			values (
				 'batnbr '+ @Batnbr +' are released now continue ... => '
--				, current_timestamp
				, 1
				, @BatNbr
			)
		-- ================================================================= --
		
		--			
		--	update "z"
		--		set "z".ZEstatus = '2'
		--	from 
		----		sltestapp.dbo.ZPortalProv as "z"
		--		integraapp.dbo.ZPortalProv as "z"
		--	where 
		--		 	"z".ZCpnyId = @CpnyId --"cfdi".cpnyId
		--		and
		--			"z".ZAPBatNbr = @BatNbr --"cfdi".batnbr
		--		and 
		--			"z".ZAPRefno = @RefNbr --"cfdi".refnbr
		
		end -- End IF			
	else 
		begin 
			insert into sistemas.dbo.api_sat_historico_logs (message,status,Batnbr)
				values (
					 'Found that the batnbr => ' + @Batnbr + ' is already Released or in other status so the released proccess is skipped ...'
	--				, current_timestamp
					, 7
					, @BatNbr
					)
		end -- End of IF-ELSE 

end -- Trigger






--select * from sistemas.dbo.providers_uuid_requests


insert into sistemas.dbo.providers_uuid_requests(
             BatNbr    
            ,CpnyId    
            ,RefNbr    
            ,VendId  
            ,PONbr    
            ,TermsId
            ,DueIntrv
            ,InvDate
            ,InvcNbr          
            ,Sello
            ,Total
            ,SubTotal
            ,Certificado
--            ,FormaDePago 
            ,MetodoPago
            ,NoCertificado
            ,TipoDeComprobante
            ,re
            ,rr
            ,Unidad
            ,ImporteConcepto
            ,Cantidad
            ,Descripcion
            ,ValorUnitario
            ,TasaOCuota
            ,ImporteTraslado
            ,Impuesto
            ,uuid
            ,selloCFD 
            ,FechaTimbrado
            ,NoCertificadoSAT
            ,Version
            ,selloSAT
            ,created
            ,modified
            ,providers_standings_id
            ,providers_parents_id
            ,_status
)
values		(
             471835    
            ,'TEICUA'    
            ,307206    
            ,'MOPE791207JX1'
            ,'099649'
            ,'CO'
            ,0
            ,'2019-12-06'
            ,'A 415'          
            ,'bc2lcnzIBNac8dLfagV97h2rM/re5rBELl1C9nfhav5q8+Z5xjdvpdk2EHPJmiKy9Ef2fQWwFjBGQsFwQtvaJtfc64B0gYXLLWECzfdLSrUEzLpnBDxBKB9nWelX8vYjqh66tv0RVq3uhO9VN6gnzOZaW5jgsmbLZgmfdK056MLz/lwkflWLr8fAuxg0z0XXobBcPFHO/KHhUOwsoKZnueBY3tKJuJ7ZTxFUNUH7Erb0Yi12QBG/GDHZI1X/oikB8JNF+wXZG633cAIE/xH9RLc2muTCQHexknCmdvAZnr77191nlUuGiyqCemzpv1US4zUKecaftnFsnY0/7/bfNg=='
            ,2992.00
            ,2500.00
            ,'MIIGtDCCBJygAwIBAgIUMDAwMDEwMDAwMDA0MDYwMDM0NTQwDQYJKoZIhvcNAQELBQAwggGyMTgwNgYDVQQDDC9BLkMuIGRlbCBTZXJ2aWNpbyBkZSBBZG1pbmlzdHJhY2nDs24gVHJpYnV0YXJpYTEvMC0GA1UECgwmU2VydmljaW8gZGUgQWRtaW5pc3RyYWNpw7NuIFRyaWJ1dGFyaWExODA2BgNVBAsML0FkbWluaXN0cmFjacOzbiBkZSBTZWd1cmlkYWQgZGUgbGEgSW5mb3JtYWNpw7NuMR8wHQYJKoZIhvcNAQkBFhBhY29kc0BzYXQuZ29iLm14MSYwJAYDVQQJDB1Bdi4gSGlkYWxnbyA3NywgQ29sLiBHdWVycmVybzEOMAwGA1UEEQwFMDYzMDAxCzAJBgNVBAYTAk1YMRkwFwYDVQQIDBBEaXN0cml0byBGZWRlcmFsMRQwEgYDVQQHDAtDdWF1aHTDqW1vYzEVMBMGA1UELRMMU0FUOTcwNzAxTk4zMV0wWwYJKoZIhvcNAQkCDE5SZXNwb25zYWJsZTogQWRtaW5pc3RyYWNpw7NuIENlbnRyYWwgZGUgU2VydmljaW9zIFRyaWJ1dGFyaW9zIGFsIENvbnRyaWJ1eWVudGUwHhcNMTcwNDI4MTUwMjU2WhcNMjEwNDI4MTUwMjU2WjCCAVMxQTA/BgNVBAMTOE9QRVJBRE9SQSBERSBIT1RFTEVTIFkgU0VSVklDSU9TIFRVUklTVElDT1MgU0VBIFNBIERFIENWMUEwPwYDVQQpEzhPUEVSQURPUkEgREUgSE9URUxFUyBZIFNFUlZJQ0lPUyBUVVJJU1RJQ09TIFNFQSBTQSBERSBDVjFBMD8GA1UEChM4T1BFUkFET1JBIERFIEhPVEVMRVMgWSBTRVJWSUNJT1MgVFVSSVNUSUNPUyBTRUEgU0EgREUgQ1YxJTAjBgNVBC0THE9IUzAxMDYwNlRaOCAvIE1FUko0MTExMDI3WDUxHjAcBgNVBAUTFSAvIE1FUko0MTExMDJIREZSTFMwNTFBMD8GA1UECxM4T1BFUkFET1JBIERFIEhPVEVMRVMgWSBTRVJWSUNJT1MgVFVSSVNUSUNPUyBTRUEgU0EgREUgQ1YwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCotjBb0MSqOAO+BD1QEEL2QGWW6dwIjihsMrkqkHl04rQ5nlcO3HKEvGZnC/542SAbr2G0vSnQgm44pV8V5R5Vu4AHcEsN8DdgZlAM41PTDGC7xneKlQypX62ulnSz07BTxGCcHbAGEVkAWww0LxuFChuKR6olnWw9o9aMbGHqCqAfOYxF4DoqKjH0wOQATrqYFc9+aumr3WnWOS653tS+4V5czdmlUmA8ksl1yDj+ySbfpQYPPg7mlcgFkVL7jftuZ7SrAPyCcX0j0tDr8SQTy3kV0M8hdp+m2XxEmNMhWwAb89p5QbsQ+PaOSs6zH9NH6PRsPUr8GBxY7qfhieVpAgMBAAGjHTAbMAwGA1UdEwEB/wQCMAAwCwYDVR0PBAQDAgbAMA0GCSqGSIb3DQEBCwUAA4ICAQCVBrEUzrkK6/3lghgXXJU4DUo22CGokUX+p15dxwc82lV8JogeqPCyuiqdFDBC/dmIDXHUtjQJrYX9FhUUKRI5nR4Mi49f3XEcOGAG6YFAAWI6v3zN9NW69qRDvBedQ73Btt7LZEDqtrb9+ru+GQi8TWubXiz521g4wLk7mmOeKYYP+0s6TBGYm3gHDTtVafJm6NOEwWFlPmUK2KpG6xC7wDt3nb9xyYMgqoO2X8EwXYVCeA0K/EGcHCS3EAbt3xVGAjKm5ADRDI8gs2qhSAsCBAxywVxvgZ0tzGpw+a4uU6EQtaUkex1eMDpHgAgLcZ2Esad1sSWCWIEYhao8SUUpduvL1T3AxnmnpLySZqMAhdpowxXDyKDqQ2Qp43HOWVMXowRgt0aVnaMyjvKkMAbQguqtGrGgsBxNq5DIPrTlo5DZDypTLZxmO0k01praku9RPNE50kRMqa3QPNH5jHCLC17at9JgqkokTRgCC8dG+wIrlCn6r+Sx9CdT7GOsM6LVZ6rLnKtOgSlDX4giWv1nRjZTTda+eNTWy/WT19NinAb+NCeI6PXPNu5I0fC9vjFaI2ri2NnZVGWEf4KwvjJd3UR7BD0s0EG7+WHkH3m+Bu8qOCxAUbzZBSnMIh7NaAFoY+XS9s4cwOwQT0g5V5lptu06p2UaoNHVIhetISuNDw=='
--            ,'' 
            ,'PUE'
            ,'00001000000406003454'
            ,'I'
            ,'OHS010606TZ8'
            ,'TBO7911166Z6'
            ,'E48'
            ,2500.00
            ,1
            ,'Habitacion                (  4)'
            ,2500.00
            ,0.160000
            ,400.00
            ,'002'
            ,'7818c27c-d184-4819-8c77-108cc6f63bd6'
            ,''
            ,'2019-12-06T09:30:14'
            ,'00001000000404399419'
            ,'1.1'
            ,''
            ,'2020-01-31 17:51:30'
            ,'2020-01-31 17:51:30'
            ,1
            ,1
            ,1
)



use sistemas
--ALTER TABLE sistemas.dbo.providers_uuid_requests ALTER COLUMN Sello nvarchar(max) collate sql_latin1_general_cp1_ci_as null
--ALTER TABLE sistemas.dbo.providers_uuid_requests ALTER COLUMN Certificado nvarchar(max) collate sql_latin1_general_cp1_ci_as null

 SET QUOTED_IDENTIFIER OFF;SET	ANSI_NULLS	ON;SET	ANSI_WARNINGS	ON;

select * from sistemas.dbo.providers_uuid_requests


INSERT INTO [providers_uuid_requests] 
				(
					  [BatNbr], [CpnyId], [RefNbr], [VendId], [PONbr], [TermsId], [DueIntrv], [InvDate]
					, [InvcNbr]
					, [Sello]
					, [Total], [SubTotal]
					, [Certificado]
					, [MetodoPago], [NoCertificado]
					, [TipoDeComprobante], [re], [rr], [Unidad], [ImporteConcepto], [Cantidad], [Descripcion]
					, [ValorUnitario], [TasaOCuota], [ImporteTraslado], [Impuesto], [uuid], [FechaTimbrado]
					, [NoCertificadoSAT], [Version], [selloSAT], [created], [modified], [providers_standings_id]
					, [providers_parents_id], [_status] 
				)
VALUES (
			 '481583', 'TEICUA', '313527', 'MES7901241N3', '102507', '', 28, '', ''
			, 'RmpsdnRIdmtPRS9KbDlVcWpXcVMyd01VbjlHVG9ZL1NOSXMyZ2FkR2ViRUhqeHNrOGw4a1dZNEhRbmI3RDFYa1ZnU0NqV0JlL0VJYzEwSjQ4NDJVSlVmcDFTYTdYMnBJZllzNkROWHNvVnVlanV2ZE16RGdFRXZPQmwyQ1cySW5UQUxZbWdIL3NpZjh1cEFzM2NCQncrVGxLamR1cnlLMW1oZDRtVUtndlVSTXFGZ3FmQXEybWc5dmRGQUJkL0kvanI5UHhnczB2OW5nM2lmUzdSVGRZUEFVR0pWOVVRZmpLeWxoVUxYZTc1VFY0Z290NElBenhJaFRQTjJUK1ExOGZKRWplS2JzM21hYm1jTEtBVXRvYXE5T3NyL1ZVaHdKaGRNVDZMZHRlS1pNK2xISndaLzZoRWllOXhtY0djK1NaZUtOTU5FOWtBZ0xoSUNGOGx0cC9nPT0=', 6629.40, 5715.00, 'TUlJR09qQ0NCQ0tnQXdJQkFnSVVNREF3TURFd01EQXdNREEwTURrNU5qVTNOVE13RFFZSktvWklodmNOQVFFTEJRQXdnZ0d5TVRnd05nWURWUVFEREM5QkxrTXVJR1JsYkNCVFpYSjJhV05wYnlCa1pTQkJaRzFwYm1semRISmhZMm5EczI0Z1ZISnBZblYwWVhKcFlURXZNQzBHQTFVRUNnd21VMlZ5ZG1samFXOGdaR1VnUVdSdGFXNXBjM1J5WVdOcHc3TnVJRlJ5YVdKMWRHRnlhV0V4T0RBMkJnTlZCQXNNTDBGa2JXbHVhWE4wY21GamFjT3piaUJrWlNCVFpXZDFjbWxrWVdRZ1pHVWdiR0VnU1c1bWIzSnRZV05wdzdOdU1SOHdIUVlKS29aSWh2Y05BUWtCRmhCaFkyOWtjMEJ6WVhRdVoyOWlMbTE0TVNZd0pBWURWUVFKREIxQmRpNGdTR2xrWVd4bmJ5QTNOeXdnUTI5c0xpQkhkV1Z5Y21WeWJ6RU9NQXdHQTFVRUVRd0ZNRFl6TURBeEN6QUpCZ05WQkFZVEFrMVlNUmt3RndZRFZRUUlEQkJFYVhOMGNtbDBieUJHWldSbGNtRnNNUlF3RWdZRFZRUUhEQXREZFdGMWFIVERxVzF2WXpFVk1CTUdBMVVFTFJNTVUwRlVPVGN3TnpBeFRrNHpNVjB3V3dZSktvWklodmNOQVFrQ0RFNVNaWE53YjI1ellXSnNaVG9nUVdSdGFXNXBjM1J5WVdOcHc3TnVJRU5sYm5SeVlXd2daR1VnVTJWeWRtbGphVzl6SUZSeWFXSjFkR0Z5YVc5eklHRnNJRU52Ym5SeWFXSjFlV1Z1ZEdVd0hoY05NVGd3TXpFME1qTXpOalE1V2hjTk1qSXdNekUwTWpNek5qUTVXakNCMmpFcU1DZ0dBMVVFQXhNaFRVRk9SMVZGVWtGVElFVlRVRVZEU1VGTVNWcEJSRUZUSUZOQklFUkZJRU5XTVNvd0tBWURWUVFwRXlGTlFVNUhWVVZTUVZNZ1JWTlFSVU5KUVV4SldrRkVRVk1nVTBFZ1JFVWdRMVl4S2pBb0JnTlZCQW9USVUxQlRrZFZSVkpCVXlCRlUxQkZRMGxCVEVsYVFVUkJVeUJUUVNCRVJTQkRWakVsTUNNR0ExVUVMUk1jVFVWVE56a3dNVEkwTVU0eklDOGdVa0ZXVXpjMk1EUXhOVkJCTWpFZU1Cd0dBMVVFQlJNVklDOGdVa0ZXVXpjMk1EUXhOVWhFUmsxTVVqQTVNUTB3Q3dZRFZRUUxFd1J0WlhOaE1JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0NBUThBTUlJQkNnS0NBUUVBakxVdWQyRzhwTjUxWm9qZStxSDdzVkEraVRGNUVDR2t1cEljeDJEdWZFVE5oSHBHUk4yT3Y2RUdLZUIreEFVQ2tudnNnUWlpejk1VGw1OVNRcVhxcDBrcUNGd21MV1RySThicnovbUlycXA5TlhnVGxqUGxTMlh0UVBsd202eDZVanQxNWtXNkpqSjArM1JiY2t5SEV6QUV5WVBTYno3T0JDMzU4Y3pTYm50eVE2WDB3c2RUU25OY3U1Kzc5Sjl4UDVVTmJSUHR2SDlhcjE1RlhVTTZZMFBBbndpY3cwYTlFem5YT2w2M1J2QUF0dVZOOUU3ZG5USU9ScVZoV2twVlVaR2ZrOHRDMUtnRXJWRWxpNExyVTNtR0lnenJoK2RyTURNTnJTZHdlaGN3QzA4ZHIvcHVlSW9PQS8xZUc3U2Q1OGRESmxPWGVoUjJMS2E5bzJjSFZ3SURBUUFCb3gwd0d6QU1CZ05WSFJNQkFmOEVBakFBTUFzR0ExVWREd1FFQXdJR3dEQU5CZ2txaGtpRzl3MEJBUXNGQUFPQ0FnRUFWSlRPWHRGQi85cVRlaGN6ejNuK2FtNHNDQmNteTR6MERhNlV1SW16bFlXbVJzZDBuaStUQVhnUTNlK01aaTFUU3FnNEdjbjFpNDNPNXhIYlNZNUtBZUF2aUdDRDdYQXZma0o2bURjdGV5UUNzbklwa0VzRFY4OXRLYmoxbjF2dVlEa21yZXBSKytlR1FpRWdFdlZsTWZpZG12b0tyQS9GUUhGYWZIOWx1cVlRYXlpYU05T0JRUzdEQlhxNitTL0F5eUlJNk4wTE5XNW9RcCtUNjE4UERXUUFhcjNacUNoTTdaRkdQeVFsWDY0ZER3aE1hNENZLzBjbUh6VWZLaVJ0TTQvRCtaSlZpRmRFUGxmZjRRSnd5UzNhOUNHTmQyanUxc0kvUW1URTFPeUk3dlRGaUdUdWJaSnF2VDV4b09yRWRuUXB1Z0ZIZlBvTldCNWVxdlFsQVR5eVJWQm5HSmVKWUVyd29nVG9oa3ZkVjBkTG5WUE1aVU02c3FtcFZnZXhnYUhWVlM5NmdRNU8zdjZWb3JMeStCTlc5QnB0c1pGaFBvcnQ3aTNyQklLZEVEQ0hudEZ0eTNMVHpTRWhxUGhFOFlrUUZLUkxib2phZHNtTlJadU1yTitwVGZIdUdCa0N5Yno1SGtHdVd5NDk3QVdUVHJFSUw4eEhWVFRBOGdRWXk0M09HbmhIVXRVb1dTVlRRbmFmT0FjbUZaOUNRUXhIaFlrV3lDemNrakxKZ21paStMb3lyeGNjZWxKQ1JSK3QvMjlYZ2Y2WHI2VEdqUkNTbCtveDhwQURuMUZnQmxVS1VZYUM3K1FncVlnb3AvMEZIOFNOVUFScEJXWmFBTGkwTXBLallnNURmTHVlcXFFZ3hXY0VJUWZmK0RzLy9wVVU4SWMvRVowTWd3az0='
			, 'PPD', '00001000000409965753'
			, 'I', 'MES7901241N3', 'TEI791220DAA', 'm', 5715.00, 15.24, 'SuperTolva, DI: 4 pulg.'
			, 375.00, 0.160000, 914.40, '002', '159bde7d-8806-4df4-b383-609baeb72fed', '2020-02-19T10:06:13', '00001000000404991284'
			, '1.1', NULL, '2020-08-15 17:46:39', '2020-08-15 17:46:39', 1, 1, 1
	   ) 


	

 use sltestapp 
 	select * from sltestapp.dbo.GRW_CfdiInvcAddData where BatNbr = '466050'
 
 
	select * from dbo.Batch where BatNbr = '466050'
	select * from dbo.APDoc as "doc" where BatNbr = '466050' 
	
-- lineref 32767

	select * from dbo.APTran as "doc" where BatNbr = '466050' and CpnyId = 'TBKGDL' and Acct = '0201010000'


	select * from dbo.APDoc as "doc" where BatNbr = '466050'

	select * from sistemas.dbo.providers_view_relations where BatNbr = '466050'

	
	select * from integraapp.dbo.xCEEIDoc where Module = 'AP' and Batnbr = '466050'

	select LineRef,CuryId,CuryRate,tipo from integraapp.dbo.xCEEIDoc where Module = 'AP' group by LineRef,CuryId,CuryRate,tipo
		
	--with "line" as (
	select count(line) as 'cn',line from dbo.GRW_CfdiInvcAddData group by line
	--)select * from "line" where cn > 1 
	select line from sltestapp.dbo.GRW_CfdiInvcAddData order by line --group by line
--	exec sp_desc GRW_CfdiInvcAddData
--start procedure 


-- conditions for released procedure 
a = APDoc 
a.User1 = 'EC'
a.DueDate = ??
a.PayDate = ??
a.DocType   = 'VO'
a.Rlsed     = 0   --Released



select TermsId,DueIntrv,Descr,* from integraapp.dbo.Terms









select * from integraapp.dbo.ZDiasPago

exec sp_helptext XGetDueDate

select * from sistemas.dbo.api_

use sistemas

exec sp_desc operations_view_full_gst_indicators

use gstdb

exec sp_desc mtto_tipos_unidades

-- Modern_Spanish_CI_AS

-- clean tables 

select * from [192.168.20.235].sistemas.dbo.api_sat_historico_logs order by created desc

--start Migration 
--FIRTS the static tables
--drop table sistemas.dbo.providers_parents
	select *
		into providers_parents
	from [192.168.20.235].sistemas.dbo.providers_parents as "parents" order by id asc
	
--TEST select * from sistemas.dbo.providers_parents order by id asc
--drop table providers_standings
	select *
		into providers_standings
	from -- select * from 
		[192.168.20.235].sistemas.dbo.providers_standings 

-- TEST select * from sistemas.dbo.providers_standings  as "parents" order by "parents".id asc 

-- CRITICAL Migration		
--	drop table sistemas.dbo.providers_controls_files 		
--	use sistemas
	select *
		into providers_controls_files
	from 
		[192.168.20.235].sistemas.dbo.providers_controls_files
-- TEST select * from sistemas.dbo.providers_controls_files as "files" order by "files".id asc
--drop table providers_assoc_vendors
	select * 
		into providers_assoc_vendors
	from 
		[192.168.20.235].sistemas.dbo.providers_assoc_vendors
		
--	select * 
--		into providers_controls_conciliations -- select * 
--	from [192.168.20.235].sistemas.dbo.providers_controls_conciliations
--drop table 	providers_uuid_requests
	select * 
		into providers_uuid_requests
	from [192.168.20.235].sistemas.dbo.providers_uuid_requests
--drop table api_sat_historico_logs
	select * 
		into api_sat_historico_logs
	from [192.168.20.235].sistemas.dbo.api_sat_historico_logs
--drop table providers_status_definitions
	select *
		into providers_status_definitions
	from 
		[192.168.20.235].sistemas.dbo.providers_status_definitions
	
	
		
		
SELECT TOP 10 SUBSTRING(qt.TEXT, (qs.statement_start_offset/2)+1,
((CASE qs.statement_end_offset
WHEN -1 THEN DATALENGTH(qt.TEXT)
ELSE qs.statement_end_offset
END - qs.statement_start_offset)/2)+1),
qs.execution_count,
qs.total_logical_reads, qs.last_logical_reads,
qs.total_logical_writes, qs.last_logical_writes,
qs.total_worker_time,
qs.last_worker_time,
qs.total_elapsed_time/1000000 total_elapsed_time_in_S,
qs.last_elapsed_time/1000000 last_elapsed_time_in_S,
qs.last_execution_time,
qp.query_plan
FROM sys.dm_exec_query_stats qs
CROSS APPLY sys.dm_exec_sql_text(qs.sql_handle) qt
CROSS APPLY sys.dm_exec_query_plan(qs.plan_handle) qp
ORDER BY qs.total_logical_reads DESC -- logical reads
-- ORDER BY qs.total_logical_writes DESC -- logical writes
-- ORDER BY qs.total_worker_time DESC -- CPU time		
		


SELECT TOP 20
total_worker_time/execution_count AS Avg_CPU_Time
,Execution_count
,total_elapsed_time/execution_count as AVG_Run_Time
,total_elapsed_time
--,(select SUBSTRING(text,statement_start_offset/2+1,statement_end_offset) FROM sys.dm_exec_sql_text(sql_handle)) AS Query_Text
,(select [text] FROM sys.dm_exec_sql_text(sql_handle)) AS Query_Text
FROM sys.dm_exec_query_stats
ORDER BY Avg_CPU_Time DESC		
		




WITH CTE_VW_STATS AS
(
  SELECT
    SCHEMA_NAME(vw.schema_id) AS schemaname
    ,vw.name AS viewname
    ,vw.object_id AS viewid
  FROM
    sys.views AS vw
  WHERE
    (vw.is_ms_shipped = 0)
  INTERSECT
  SELECT
    SCHEMA_NAME(o.schema_id) AS schemaname
    ,o.Name AS name
    ,st.objectid AS viewid
  FROM
    sys.dm_exec_cached_plans cp
  CROSS APPLY
    sys.dm_exec_sql_text(cp.plan_handle) st
  INNER JOIN
    sys.objects o ON st.[objectid] = o.[object_id]
  WHERE
    st.dbid = DB_ID()
)
SELECT
  vw.schemaname
  ,vw.viewname
  ,vw.viewid
  ,DB_NAME(t.databaseid) AS databasename
  ,t.databaseid
  ,t.*
FROM
  CTE_VW_STATS AS vw
CROSS APPLY
  (
    SELECT
      st.dbid AS databaseid
      ,st.text
      ,qp.query_plan
      ,qs.*
    FROM
      sys.dm_exec_query_stats AS qs
    CROSS APPLY
      sys.dm_exec_sql_text(qs.plan_handle) AS st
    CROSS APPLY
      sys.dm_exec_query_plan(qs.plan_handle) AS qp
    WHERE
      (CHARINDEX(vw.schemaname, st.text, 1) > 0)
      AND (st.dbid = DB_ID())
  ) AS t

  
  
  SELECT
  SCHEMA_NAME(vw.schema_id) AS schemaname
  ,vw.name AS name
  ,vw.object_id AS viewid
FROM
  sys.views AS vw
WHERE
  (vw.is_ms_shipped = 0)
EXCEPT
SELECT
  SCHEMA_NAME(o.schema_id) AS schemaname
  ,o.name AS name
  ,st.objectid AS viewid
FROM
  sys.dm_exec_cached_plans cp
CROSS APPLY
  sys.dm_exec_sql_text(cp.plan_handle) st
INNER JOIN
  sys.objects o ON st.[objectid] = o.[object_id]
WHERE
  st.dbid = DB_ID();
GO
		

use sistemas
select * from sistemas.dbo.providers_view_relations where BatNbr = 481583

select * from sistemas.dbo.providers_view
		
