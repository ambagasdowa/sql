-- ===================================================================================================================== --		
-- Addenum build
-- ===================================================================================================================== --


	select 
			cast(XmlData as varchar(max)) as 'xml' , cast(PdfData as varchar(max)) as 'pdf'
			,* 
	from 
			integraapp.dbo.XGRWFEInvc as "inv"
		inner join 
			integraapp.dbo.XGRWFEDocs as "hdr"
		on 
			"inv".Id = "hdr".HdrId
	where 
			CustId = 'EUM000707DQ2'
		and 
			Doctype = 'INVC'
	and 
		BatNbr = '078270'

		-- CP OG-112377
		
		
		
-- ADD to ADDENDA		
	select cast(XmlData as varchar(max)) as 'xml' , cast(PdfData as varchar(max)) as 'pdf',HdrId,* from integraapp.dbo.XGRWFEDocs where HdrId = 20618
-- ADD to ADDENDA

select 
		* 
from 
		integraapp.dbo.XGRWFEInvc 
where 
		CustId = 'EUM000707DQ2'
	and 
		Doctype = 'INVC'
	and 
		BatNbr = '078270'
	
		
		
		
select * from integraapp.dbo.ARTran as "tran" where ExtRefNbr = 'OG-116258'
		

		
select * from integraapp.dbo.ARDoc where BatNbr = '083379' and RefNbr = '066064'

select * from sistemas.dbo.projections_view_full_company_testcore_indicators where tipo_doc = 2 and num_guia_asignado is null and id_cliente = 35
		
select * from sistemas.dbo.projections_view_full_gst_core_indicators where num_guia in ('OG-114730','OG-114983','OG-115016')

select * from sistemas.dbo.projections_view_full_gst_core_indicators where tipo_doc = 2 and num_guia_asignado is null and id_cliente = 35
and num_guia in ('OG-114730','OG-114983','OG-115016')

select status_guia,tipo_doc,prestamo,* from bonampakdb.dbo.trafico_guia where tipo_doc = 2 --and num_guia_asignado is null and id_cliente = 35
and num_guia in ('OG-112377','OG-113652')

select status_guia,tipo_doc,prestamo,* from gstdb.dbo.trafico_guia where tipo_doc = 2 and num_guia_asignado is null and id_cliente = 35
and num_guia = 'OG=112377'

select 
		 num_guia
		,status_guia
		,tipo_doc
		,prestamo
		,flete
		,subtotal
		,iva_guia
		,monto_retencion
		,no_remision
		,* 
from gstdb.dbo.trafico_guia where 
 num_guia in (
 			'OG-112377'
-- 			'OG-116258'
 			)
		

WITH XMLNAMESPACES('AddendaEU' as eu)  
SELECT 1 as 'eu:AddendaEU'  
FOR XML PATH ('Addenda') 


-- ===================================================================================================================== --
-- ADDENUM from Solomon
-- ===================================================================================================================== --

WITH XMLNAMESPACES('AddendaEU' as eu)

select 
		 ltrim(rtrim("tran".ExtRefNbr)) as 'CartaPorte'
		,cast("tran".TaxAmt00 as decimal(18,2)) as 'iva'
		,cast("tran".TaxAmt01 as decimal(18,2)) as 'retencion'
		,ltrim(rtrim("tran".TaxId00)) as 'ivaDesc'
		,ltrim(rtrim("tran".TaxId01)) as 'Retencion' 
		,cast("tran".TranAmt as decimal(18,2)) as 'subtotal'  -- flete
		,'ex' as 'Type/data'
		,ltrim(rtrim("tran".BatNbr)) as 'Eu/adde'
		,*
from 
		integraapp.dbo.ARTran as "tran" 
	left join
		integraapp.dbo.XGRWFEInvc as "addenum"
	on 
		"addenum".BatNbr = "tran".BatNbr and "addenum".RefNbr = "tran".RefNbr and "addenum".DocType = 'INVC' and  "addenum".CustId = 'EUM000707DQ2'
	left join 
		 gstdb.dbo.trafico_guia as "guia"
--		 bonampakdb.dbo.trafico_guia as "guia"
	on 
		"tran".ExtRefNbr = "guia".num_guia collate SQL_Latin1_General_CP1_CI_AS
   and 	
   		"guia".tipo_doc = 2 and "guia".num_guia_asignado is null and "guia".id_cliente = 35
where
--		ltrim(rtrim("tran".ExtRefNbr)) in ('OG-116258')
		"tran".BatNbr = '078270' -- and "tran".RefNbr = '066064'
	and
		"tran".Crtd_Prog = 'AR010'
for XML path ('Addenda')
		, root ('addenum-xsd') 
		, ELEMENTS XSINIL --, XMLSCHEMA ('urn:schema_example.com')
		
		

-- ===================================================================================================================== --
-- ADDENUM from ZAM 
-- ===================================================================================================================== --
		
WITH XMLNAMESPACES(
--					 default 'http://www.w3.org/2001/XMLSchema-instance',
					 'http://factura.envasesuniversales.com/addenda/eu' as eu
					,'http://www.w3.org/2001/XMLSchema-instance' as xsi
--					,'https://url' as xsd
				  )
select 
--		'http://somewhere/v1/ schemaname.xsd' as "@xsi:schemaLocation"
--		,'Factura 2.xsd' as "@eu:Addenda"
 		 tipo_doc,prestamo,num_guia,num_guia_asignado,id_cliente
 		,flete,subtotal,iva_guia,no_remision,monto_retencion
 		,(subtotal+iva_guia-monto_retencion) as 'total'
--		,* 
from
--		gstdb.dbo.trafico_guia 
		gstdb.dbo.trafico_guia
where 
--		num_guia = 'OG-116258'
--		num_guia = 'OG-113652'
--		num_guia = 'OG-112377'
--		num_guia in ('OG-112377','OG-113652')
--	and 
		tipo_doc = 2 
	and
		num_guia in (
    		'TU-18633'
--			,'TU-18616','TU-18559','TU-18551','TU-18507','TU-18499','TU-18219','TU-017109','TU-18465','TU-18462','TU-18288'
		)
--		num_guia_asignado is null 
--	and 
--		id_cliente = 35

for XML path ('eu:AddendaEU')
		, root ('Addenda') 
		, ELEMENTS -- XSINIL --, XMLSCHEMA ('urn:schema_example.com')
		
	
		
		
-- ===================================================================================================================== --
-- 
-- ===================================================================================================================== --		
		
SELECT 'en'    as "English/@xml:lang",  
       'food'  as "English",  
       'ger'   as "German/@xml:lang",  
       'Essen' as "German"  
FOR XML PATH ('Translation')  

-- ===================================================================================================================== --		
		
		
		
