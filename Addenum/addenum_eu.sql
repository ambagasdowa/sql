

WITH XMLNAMESPACES (
					'http://factura.envasesuniversales.com/addenda/eu' as "eu"
--					'' as "cfdi"
--					,'http://factura.envasesuniversales.com/addenda/eu http://factura.envasesuniversales.com/addenda/eu/EU_Addenda.xsd' as "xsi"
--					,default 'http://factura.envasesuniversales.com/addenda/eu'
				   )  
--with "addenum" as (
select 
--		 convert(varchar(max),XmlData) as 'xml'
--		,convert(varchar(max),PdfData) as 'pdf'
	   --,
--	   	'http://factura.envasesuniversales.com/addenda/eu http://factura.envasesuniversales.com/addenda/eu/EU_Addenda.xsd' as "@xsi:schemaLocation"
	   	'http://factura.envasesuniversales.com/addenda/eu http://factura.envasesuniversales.com/addenda/eu/EU_Addenda.xsd' as "@xsi:schemaLocation"
		,ltrim(rtrim("inv".S4Future01)) as 'eu:TipoFactura/eu:IdFactura'
		,"xg".Version as 'eu:TipoFactura/eu:Version'
		,cast("xg".FechaTimbrado as date) as 'eu:TipoFactura/eu:FechaMensaje'
       	,'Con_Pedido' as 'eu:TipoTransaccion/eu:IdTransaccion'
       	,ltrim(rtrim("tran".ExtRefNbr)) as 'eu:TipoTransaccion/eu:Transaccion'
	   	,1 as "eu:OrdenesCompra/eu:Secuencia/@consec"
	   	,ltrim(rtrim("inv".BatNbr)) as "eu:OrdenesCompra/eu:Secuencia/eu:IdPedido"
	   	,ltrim(rtrim("inv".InvcNbr)) as "eu:OrdenesCompra/eu:Secuencia/eu:EntradaAlmacen/eu:Albaran"
       	,case "inv".CuryId when 'MN' then 'MXN' else "inv".CuryId end as 'eu:Moneda/eu:MonedaCve'
       	,case "inv".CuryId when 'MN' then cast(1 as decimal(16,4)) else cast(1 as decimal(16,4)) end  as 'eu:Moneda/eu:TipoCambio'
       	,cast("doc".CuryTxblTot00 as decimal(18,2)) as 'eu:Moneda/eu:SubtotalM'
       	,cast("doc".CuryOrigDocAmt as decimal(18,2)) as 'eu:Moneda/eu:TotalM'
       	,cast("doc".TaxTot00 as decimal(18,2)) as 'eu:Moneda/eu:ImpuestoM'
       	,cast(abs("doc".TaxTot01) as decimal(18,2)) as 'eu:ImpuestosR/eu:BaseImpuesto'
from 
		integraapp.dbo.XGRWFEDocs as "xg" -- source origin
	inner join 
		integraapp.dbo.XGRWFEInvc as "inv" -- extra solomon
	on 
		"xg".HdrId = "inv".id
	inner join 
		integraapp.dbo.ARDoc as "doc"
	on 
		"inv".BatNbr = "doc".BatNbr and "inv".RefNbr = "doc".RefNbr and "inv".CpnyId = "doc".CpnyID
	inner join 
		integraapp.dbo.ARTran as "tran"
	on 
		"inv".BatNbr = "tran".BatNbr and "inv".RefNbr = "tran".RefNbr and "tran".CpnyId = "doc".CpnyID and "doc".LUpd_Prog = "tran".LUpd_Prog
where 
	"inv".custId = 'EUM000707DQ2'
--) 
--WITH XMLNAMESPACES (
--					'http://factura.envasesuniversales.com/addenda/eu' as "eu"
----					'' as "cfdi"
----					,'http://factura.envasesuniversales.com/addenda/eu http://factura.envasesuniversales.com/addenda/eu/EU_Addenda.xsd' as "xsi"
----					,default 'http://factura.envasesuniversales.com/addenda/eu'
--				   )  
--select * from "addenum"
and 
--where
	"inv".BatNbr = 082881
-- for xml RAW
FOR XML path 
							('eu:AddendaEU') 
--							,ROOT ('cfdi:Addenda') 
							,ELEMENTS XSINIL 
--							,TYPE
--							,XMLSCHEMA
