-- Upstream in Production

use [sistemas]

	
select * from sistemas.dbo.addenum_view_albaran_relations where IdPedido = 4501855858 or Albaran = 5052397521 or num_guia = 'CU-189205' or BatNbr = 106975 or RefNbr = 085715 or no_viaje = 199390
4501855858	5052397521


select * from sistemas.dbo.addenum_tbl_albaran_relations where batnbr = 106975 --106975  107001
	where 
--		Albaran = 4501855858 
--	or 
		IdPedido = 5052397521
	
select * from sistemas.dbo.addenum_view_albaran_relations where BatNbr = 146076

select * from sistemas.dbo.addenum_tbl_albaran_relations where BatNbr = 146076

use sistemas	
IF OBJECT_ID ('addenum_view_albaran_relations', 'V') IS NOT NULL
    DROP VIEW addenum_view_albaran_relations;
-- now build the view
alter view addenum_view_albaran_relations
with encryption
as -- 
	select 
			 convert(varchar(max),XmlData) as 'xml'
			,convert(varchar(max),PdfData) as 'pdf'
			,row_number() over(partition by "inv".BatNbr,"doc".CuryTxblTot00,"doc".CuryOrigDocAmt,"doc".TaxTot00 order by "inv".BatNbr) as 'index'
	       	,ltrim(rtrim("tran".RefNbr)) as 'RefNbr'
	       	,ltrim(rtrim("tran".BatNbr)) as 'BatNbr'
	       	,"lois".no_remision
			,'http://factura.envasesuniversales.com/addenda/eu http://factura.envasesuniversales.com/addenda/eu/EU_Addenda.xsd' as "@xsi:schemaLocation"
			,ltrim(rtrim("inv".S4Future01)) as 'IdFactura' -- inside TipoFactura
--			,"xg".Version as 'Version'	-- inside TipoFactura
			,"xg".Version as 'Version'	-- inside TipoFactura
			,cast("xg".FechaTimbrado as date) as 'FechaMensaje'	-- inside TipoFactura
	       	,'Con_Pedido' as 'IdTransaccion'  -- eu:TipoTransaccion
--	       	,ltrim(rtrim("tran".ExtRefNbr)) as 'eu:TipoTransaccion/eu:Transaccion'
--	       	,ltrim(rtrim("tran".RefNbr)) as 'eu:TipoTransaccion/eu:Transaccion'
	       	,ltrim(rtrim("lois".no_remision)) as 'Transaccion' -- eu:TipoTransaccion
		   	,1 as "eu:OrdenesCompra/eu:Secuencia/@consec"
		   	,"upload".id as 'idx'
		   	,ltrim(rtrim("upload".IdPedido)) as 'IdPedido' --eu:OrdenesCompra
		   	,ltrim(rtrim("upload".Albaran)) as 'Albaran'  --eu:OrdenesCompra/eu:Secuencia/eu:EntradaAlmacen
	       	,case "inv".CuryId when 'MN' then 'MXN' else "inv".CuryId end as 'MonedaCve'  -- eu:Moneda
	       	,case "inv".CuryId when 'MN' then cast(1 as decimal(16,4)) else cast(1 as decimal(16,4)) end  as 'TipoCambio'  -- eu:Moneda
	       	,cast("doc".CuryTxblTot00 as decimal(18,2)) as 'SubtotalM'  -- eu:Moneda
	       	,cast("doc".CuryOrigDocAmt as decimal(18,2)) as 'TotalM'  -- eu:Moneda
	       	,cast("doc".TaxTot00 as decimal(18,2)) as 'ImpuestoM'  -- eu:Moneda
	       	,cast(abs("doc".TaxTot01) as decimal(18,2)) as 'BaseImpuesto'   --eu:ImpuestosR
	        ,"xg".CadenaOriginal,"lois".no_guia
			,"lois".num_guia
        	,"lois".no_viaje,"lois".subtotal,"lois".desc_flete,"lois".observacion
        	,"lois".id_area
	       	,"lois".FlagIsDisminution,"lois".FlagIsProvision
	        ,"tran".PerPost,"lois".fecha_guia
	        ,"lois".status_guia
--	        ,"inv".BatNbr
--	       	,*
	from 
			[192.168.20.240].integraapp.dbo.XGRWFEDocs as "xg" -- source origin
		inner join 
			[192.168.20.240].integraapp.dbo.XGRWFEInvc as "inv" -- extra solomon
		on 
			"xg".HdrId = "inv".id and "Inv".CustId = 'EUM000707DQ2'
		left join 
			[192.168.20.240].integraapp.dbo.ARDoc as "doc"
		on 
			"inv".BatNbr = "doc".BatNbr and "inv".RefNbr = "doc".RefNbr and "inv".CpnyId = "doc".CpnyID and "doc".PerPost > 202100
		left join 
			[192.168.20.240].integraapp.dbo.ARTran as "tran"
		on 
			"inv".BatNbr = "tran".BatNbr and "inv".RefNbr = "tran".RefNbr and "tran".CpnyId = "doc".CpnyID and "doc".LUpd_Prog = "tran".LUpd_Prog and "tran".PerPost > 202100
		left join
			(
				select * from sistemas.dbo.projections_view_full_gst_core_indicators as "inlois" where year("inlois".fecha_guia) > 2020
			) as "lois"
		on 
			ltrim(rtrim("tran".ExtRefNbr)) = "lois".num_guia collate SQL_Latin1_General_CP1_CI_AS
		left join 
			sistemas.dbo.addenum_tbl_albaran_relations as "upload"
		on 
--			(
			"upload".batnbr collate SQL_Latin1_General_CP1_CI_AS = "inv".BatNbr
--			or
--			"upload".Refnbr collate SQL_Latin1_General_CP1_CI_AS = "tran".RefNbr
--			or
--			"upload".num_guias collate SQL_Latin1_General_CP1_CI_AS = "tran".ExtRefNbr
--			)
	where 
		"inv".custId = 'EUM000707DQ2'
--	and 
--		"inv".BatNbr = 102575
