 select
			id_area,id_compania,ciudad,nombre,nombrecorto 
from 
			bonampak_migracion.dbo.general_area 
for xml raw ('AddendaEU'), ROOT ('Addenda') ,ELEMENTS ,XMLSCHEMA ('urn:http://factura.envasesuniversales.com/addenda/eu')


select
			 "ar".id_area,"ar".id_compania
--			,ciudad
			,"ar".nombre,"ar".nombrecorto
--			,"ar".query('declare namespace ns="http://factura.envasesuniversales.com/addenda/eu";/ns:ciudad/ns:nombrecorto')
from 
			bonampak_migracion.dbo.general_area as "ar"
for xml raw ('AddendaEU'), ROOT ('Addenda') ,ELEMENTS ,XMLSCHEMA ('urn:http://factura.envasesuniversales.com/addenda/eu')






-- ==========================  Addenum ===========================--- 

WITH XMLNAMESPACES (
					'http://factura.envasesuniversales.com/addenda/eu' as "eu"
					,'url2' as "cfdi"
					,default 'http://factura.envasesuniversales.com/addenda/eu'
				   )  
SELECT 
		nombre_corto as 'eu:TipoFactura'
       ,nombre_corto      as 'eu:TipoTransaccion'  
       ,     as 'eu:OrdenesCompra'
       ,id_area 			as 'eu:Moneda'
       ,id_nivel1		as 'eu:ImpuestosR'
from bonampak_migracion.dbo.compania as "comp"
inner join  bonampak_migracion.dbo.general_area as "area"
on "area".id_compania = "comp".id_compania
FOR XML RAW ('eu:AddendaEU') , ROOT ('cfdi:Addenda') , ELEMENTS  




WITH XMLNAMESPACES ('uri1' as ns1,   
                    'uri2' as ns2,  
                    DEFAULT 'uri2')  
SELECT ProductID,   
      Name,  
      Color  
FROM Production.Product   
WHERE ProductID=316 or ProductID=317  
FOR XML RAW ('ns1:Product'), ROOT('ns2:root'), ELEMENTS  

 




WITH XMLNAMESPACES ('uri' as ns1)  
SELECT ProductID as 'ns1:ProductID',  
       Name      as 'ns1:Name',   
       Color     as 'ns1:Color'  
FROM Production.Product  
WHERE ProductID=316 or ProductID=317  
FOR XML RAW ('ns1:Prod'), ELEMENTS  


 	
SELECT e.EmployeeID, c.FirstName, c.LastName, 
  jc.Resume.query('declare namespace ns=
     "http://schemas.microsoft.com/sqlserver/2004/07/adventure-works/Resume";
     /ns:Resume/ns:Education')
FROM HumanResources.Employee e INNER JOIN Person.Contact c 
   ON c.ContactID = e.ContactID
   INNER JOIN HumanResources.JobCandidate jc
   ON e.EmployeeID = jc.EmployeeID
WHERE e.EmployeeID = 268
FOR XML RAW ('Employee'), ELEMENTS; 



SELECT CONVERT(varchar(max), YourVarBinaryCol) AS TextCol
FROM yourTable













-- ==========================  Addenum ===========================---
-- nota de entrega ,8digits, '',/,' ' , init -> 8
-- nota de entrada ,4digits,'',/,' ', init -> 4
	
	select 
			 convert(varchar(max),XmlData) as 'xml'
			,convert(varchar(max),PdfData) as 'pdf'
	,
			ltrim(rtrim("inv".S4Future01)) as 'eu:TipoFactura/eu:IdFactura'
			,"xg".Version as 'eu:TipoFactura/eu:Version'
			,cast("xg".FechaTimbrado as date) as 'eu:TipoFactura/eu:FechaMensaje'
	       	,'Con_Pedido' as 'eu:TipoTransaccion/eu:IdTransaccion'
	       	,ltrim(rtrim("tran".ExtRefNbr)) as 'eu:TipoTransaccion/eu:Transaccion'
		   	,1 as "eu:OrdenesCompra/eu:Secuencia/@consec"
		   	,ltrim(rtrim("inv".BatNbr)) as "eu:OrdenesCompra/eu:Secuencia/eu:IdPedido"
		   	,ltrim(rtrim("inv".InvcNbr)) as "eu:OrdenesCompra/eu:Secuencia/eu:EntradaAlmacen/eu:Albaran"
	       	,case "inv".CuryId when 'MN' then 'MXN' else "inv".CuryId end as 'eu:Moneda/eu:MonedaCve'
	       	,case "inv".CuryId when 'MN' then cast(1 as decimal(16,4)) else cast(1 as decimal(16,4)) end  as 'eu:Moneda/eu:TipoCambio'
	       	,cast("doc".CuryTxblTot00 as decimal(18,2)) as 'eu:Moneda/eu:SubtotalM'
	       	,cast("doc".CuryOrigDocAmt as decimal(18,2)) as 'eu:Moneda/eu:TotalM'
	       	,cast("doc".TaxTot00 as decimal(18,2)) as 'eu:Moneda/eu:ImpuestoM'
	       	,cast(abs("doc".TaxTot01) as decimal(18,2)) as 'eu:ImpuestosR/eu:BaseImpuesto'
	       	,"lois".no_remision,"xg".CadenaOriginal,"lois".no_guia,"lois".no_viaje,"lois".subtotal,"lois".desc_flete,"lois".observacion
	       	,"lois".FlagIsDisminution,"lois".FlagIsProvision,"lois".status_guia
	        ,"inv".BatNbr
--	       	,*
	from 
			integraapp.dbo.XGRWFEDocs as "xg" -- source origin
		inner join 
			integraapp.dbo.XGRWFEInvc as "inv" -- extra solomon
		on 
			"xg".HdrId = "inv".id
		left join 
			integraapp.dbo.ARDoc as "doc"
		on 
			"inv".BatNbr = "doc".BatNbr and "inv".RefNbr = "doc".RefNbr and "inv".CpnyId = "doc".CpnyID
		left join 
			integraapp.dbo.ARTran as "tran"
		on 
			"inv".BatNbr = "tran".BatNbr and "inv".RefNbr = "tran".RefNbr and "tran".CpnyId = "doc".CpnyID and "doc".LUpd_Prog = "tran".LUpd_Prog
		left join 
			sistemas.dbo.projections_view_full_gst_core_indicators as "lois"
		on 
			ltrim(rtrim("tran".ExtRefNbr)) = "lois".num_guia collate SQL_Latin1_General_CP1_CI_AS
	where 
		"inv".custId = 'EUM000707DQ2'
and 
--	"inv".BatNbr = 082881
--	"lois".num_guia = 'CU-179311'
--	"lois".no_guia = 128279
--	"inv".BatNbr = 423013
ltrim(rtrim("tran".ExtRefNbr)) = 'CU-179311'
	


	select * from gstdb_pba.dbo.trafico_guia where num_guia = 'CU-179311'

	select * from sistemas.dbo.projections_view_full_company_core_indicators where num_guia = 'CU-179311'

	select * from sistemas.dbo.projections_view_full_gst_core_indicators where num_guia = 'CU-179311'
	-- invnbr 065641         
	-- refnbr 065641    
--
--select no_remision,no_guia,no_viaje,subtotal,desc_flete,observacion,* from sistemas.dbo.projections_view_full_company_testcore_indicators where num_guia = 'OG-114390'
--	
--	
--select * from integraapp.dbo.ARDoc where batnbr = '082881' and CpnyID = 'TBKGDL'
--inner join 
--	select * from integraapp.dbo.ARTran where batnbr = '082881' and CpnyID = 'TBKGDL'
--	
--	select * from integraapp.dbo.Batch where batnbr = '082881' and CpnyID = 'TBKGDL'
--	
--	
--WITH XMLNAMESPACES (
--					'http://factura.envasesuniversales.com/addenda/eu' as "eu"
--					,'url2' as "cfdi"
--					,default 'http://factura.envasesuniversales.com/addenda/eu'
--				   )  
--SELECT 
--		as 'eu:TipoFactura'
--       ,as 'eu:TipoTransaccion'  
--       ,as 'eu:OrdenesCompra'
--       ,as 'eu:Moneda'
--       ,as 'eu:ImpuestosR'
--from bonampak_migracion.dbo.compania as "comp"
--inner join  bonampak_migracion.dbo.general_area as "area"
--on "area".id_compania = "comp".id_compania
--FOR XML RAW ('eu:AddendaEU') , ROOT ('cfdi:Addenda') , ELEMENTS  
--
--
--
--select 
--		'namespace-urn' as 'xmls:namespace'
--		,1 as 'namespace:a'
--for xml raw
--
--
--select 
--		'namespace-urn' as 'xmls:eu'
--		,1 as 'eu:a'
--for xml raw
--
--



-- ====================================================================================================== --
-- View for addenum
-- ====================================================================================================== --



--with "addenum" as (


WITH XMLNAMESPACES (
					'http://factura.envasesuniversales.com/addenda/eu' as "eu"
--					'' as "cfdi"
--					,'http://factura.envasesuniversales.com/addenda/eu http://factura.envasesuniversales.com/addenda/eu/EU_Addenda.xsd' as "xsi"
--					,default 'http://factura.envasesuniversales.com/addenda/eu'
				   )  
--with "addenum" as (
select 
--		 convert(varchar(max),XmlData) as 'xml'
--		,convert(varchar(max),PdfData) as 'pdf'
	   --,
--	   	'http://factura.envasesuniversales.com/addenda/eu http://factura.envasesuniversales.com/addenda/eu/EU_Addenda.xsd' as "@xsi:schemaLocation"
	   	'http://factura.envasesuniversales.com/addenda/eu http://factura.envasesuniversales.com/addenda/eu/EU_Addenda.xsd' as "@xsi:schemaLocation"
		,ltrim(rtrim("inv".S4Future01)) as 'eu:TipoFactura/eu:IdFactura'
		,"xg".Version as 'eu:TipoFactura/eu:Version'
		,cast("xg".FechaTimbrado as date) as 'eu:TipoFactura/eu:FechaMensaje'
       	,'Con_Pedido' as 'eu:TipoTransaccion/eu:IdTransaccion'
       	,ltrim(rtrim("tran".ExtRefNbr)) as 'eu:TipoTransaccion/eu:Transaccion'
	   	,1 as "eu:OrdenesCompra/eu:Secuencia/@consec"
	   	,ltrim(rtrim("inv".BatNbr)) as "eu:OrdenesCompra/eu:Secuencia/eu:IdPedido"
	   	,ltrim(rtrim("inv".InvcNbr)) as "eu:OrdenesCompra/eu:Secuencia/eu:EntradaAlmacen/eu:Albaran"
       	,case "inv".CuryId when 'MN' then 'MXN' else "inv".CuryId end as 'eu:Moneda/eu:MonedaCve'
       	,case "inv".CuryId when 'MN' then cast(1 as decimal(16,4)) else cast(1 as decimal(16,4)) end  as 'eu:Moneda/eu:TipoCambio'
       	,cast("doc".CuryTxblTot00 as decimal(18,2)) as 'eu:Moneda/eu:SubtotalM'
       	,cast("doc".CuryOrigDocAmt as decimal(18,2)) as 'eu:Moneda/eu:TotalM'
       	,cast("doc".TaxTot00 as decimal(18,2)) as 'eu:Moneda/eu:ImpuestoM'
       	,cast(abs("doc".TaxTot01) as decimal(18,2)) as 'eu:ImpuestosR/eu:BaseImpuesto'
from 
		integraapp.dbo.XGRWFEDocs as "xg" -- source origin
	inner join 
		integraapp.dbo.XGRWFEInvc as "inv" -- extra solomon
	on 
		"xg".HdrId = "inv".id
	inner join 
		integraapp.dbo.ARDoc as "doc"
	on 
		"inv".BatNbr = "doc".BatNbr and "inv".RefNbr = "doc".RefNbr and "inv".CpnyId = "doc".CpnyID
	inner join 
		integraapp.dbo.ARTran as "tran"
	on 
		"inv".BatNbr = "tran".BatNbr and "inv".RefNbr = "tran".RefNbr and "tran".CpnyId = "doc".CpnyID and "doc".LUpd_Prog = "tran".LUpd_Prog
where 
	"inv".custId = 'EUM000707DQ2'
--) 
--WITH XMLNAMESPACES (
--					'http://factura.envasesuniversales.com/addenda/eu' as "eu"
----					'' as "cfdi"
----					,'http://factura.envasesuniversales.com/addenda/eu http://factura.envasesuniversales.com/addenda/eu/EU_Addenda.xsd' as "xsi"
----					,default 'http://factura.envasesuniversales.com/addenda/eu'
--				   )  
--select * from "addenum"
and 
--where
	"inv".BatNbr = 094443    -- 082881

-- for xml RAW
FOR XML path 
							('eu:AddendaEU') 
--							,ROOT ('cfdi:Addenda') 
							,ELEMENTS XSINIL 
--							,TYPE
--							,XMLSCHEMA


						
						
						
							
-- ====================================================================================================== --
-- 											View for addenum
-- ====================================================================================================== --

WITH XMLNAMESPACES (
					'http://factura.envasesuniversales.com/addenda/eu' as "eu"
--					'' as "cfdi"
--					,'http://factura.envasesuniversales.com/addenda/eu http://factura.envasesuniversales.com/addenda/eu/EU_Addenda.xsd' as "xsi"
--					,default 'http://factura.envasesuniversales.com/addenda/eu'
				   ) 		   
	select 
--			 convert(varchar(max),XmlData) as 'xml'
--			,convert(varchar(max),PdfData) as 'pdf',
			'http://factura.envasesuniversales.com/addenda/eu http://factura.envasesuniversales.com/addenda/eu/EU_Addenda.xsd' as "@xsi:schemaLocation"
			,ltrim(rtrim("inv".S4Future01)) as 'eu:TipoFactura/eu:IdFactura'
			,"xg".Version as 'eu:TipoFactura/eu:Version'
			,cast("xg".FechaTimbrado as date) as 'eu:TipoFactura/eu:FechaMensaje'
	       	,'Con_Pedido' as 'eu:TipoTransaccion/eu:IdTransaccion'
--	       	,ltrim(rtrim("tran".ExtRefNbr)) as 'eu:TipoTransaccion/eu:Transaccion'
--	       	,ltrim(rtrim("tran".RefNbr)) as 'eu:TipoTransaccion/eu:Transaccion'
	       	,ltrim(rtrim("lois".no_remision)) as 'eu:TipoTransaccion/eu:Transaccion'
		   	,1 as "eu:OrdenesCompra/eu:Secuencia/@consec"
		   	,ltrim(rtrim("upload".IdPedido)) as "eu:OrdenesCompra/eu:Secuencia/eu:IdPedido"
		   	,ltrim(rtrim("upload".Albaran)) as "eu:OrdenesCompra/eu:Secuencia/eu:EntradaAlmacen/eu:Albaran"
	       	,case "inv".CuryId when 'MN' then 'MXN' else "inv".CuryId end as 'eu:Moneda/eu:MonedaCve'
	       	,case "inv".CuryId when 'MN' then cast(1 as decimal(16,4)) else cast(1 as decimal(16,4)) end  as 'eu:Moneda/eu:TipoCambio'
	       	,cast("doc".CuryTxblTot00 as decimal(18,2)) as 'eu:Moneda/eu:SubtotalM'
	       	,cast("doc".CuryOrigDocAmt as decimal(18,2)) as 'eu:Moneda/eu:TotalM'
	       	,cast("doc".TaxTot00 as decimal(18,2)) as 'eu:Moneda/eu:ImpuestoM'
	       	,cast(abs("doc".TaxTot01) as decimal(18,2)) as 'eu:ImpuestosR/eu:BaseImpuesto'
--	       	,"tran".RefNbr as 'Referencia'
--	       	,"tran".BatNbr as 'Lote'
--	       	,"lois".no_remision as 'Remision'
--	        ,"xg".CadenaOriginal,"lois".no_guia,"lois".no_viaje,"lois".subtotal,"lois".desc_flete,"lois".observacion
--	       	,"lois".FlagIsDisminution,"lois".FlagIsProvision,"lois".status_guia
--	        ,"inv".BatNbr
--	       	,*
	from 
			integraapp.dbo.XGRWFEDocs as "xg" -- source origin
		inner join 
			integraapp.dbo.XGRWFEInvc as "inv" -- extra solomon
		on 
			"xg".HdrId = "inv".id
		left join 
			integraapp.dbo.ARDoc as "doc"
		on 
			"inv".BatNbr = "doc".BatNbr and "inv".RefNbr = "doc".RefNbr and "inv".CpnyId = "doc".CpnyID
		left join 
			integraapp.dbo.ARTran as "tran"
		on 
			"inv".BatNbr = "tran".BatNbr and "inv".RefNbr = "tran".RefNbr and "tran".CpnyId = "doc".CpnyID and "doc".LUpd_Prog = "tran".LUpd_Prog
		left join 
			sistemas.dbo.projections_view_full_gst_core_indicators as "lois"
		on 
			ltrim(rtrim("tran".ExtRefNbr)) = "lois".num_guia collate SQL_Latin1_General_CP1_CI_AS
		left join 
			sistemas.dbo.addenum_tbl_albaran_relations as "upload"
		on 
--			(
			"upload".batnbr collate SQL_Latin1_General_CP1_CI_AS = "inv".BatNbr
--			or
--			"upload".Refnbr collate SQL_Latin1_General_CP1_CI_AS = "tran".RefNbr
--			or
--			"upload".num_guias collate SQL_Latin1_General_CP1_CI_AS = "tran".ExtRefNbr
--			)
	where 
		"inv".custId = 'EUM000707DQ2'
	and 
--		"inv".BatNbr = 094443
--		"inv".BatNbr = 102571
		"inv".BatNbr = 102575
	
	--	"lois".no_guia = 128279	
	--	ltrim(rtrim("tran".ExtRefNbr)) = 'OG-117262'	   
	--	ltrim(rtrim("tran".ExtRefNbr)) = 'TU-19359'
--		ltrim(rtrim("tran".ExtRefNbr)) = 'TU-19360'
--		ltrim(rtrim("tran".ExtRefNbr)) = 'CU-179311'
FOR XML path 
							('eu:AddendaEU') 
--							,ROOT ('cfdi:Addenda') 
							,ELEMENTS XSINIL 
--							,TYPE
--							,XMLSCHEMA
	
						
						
-- ============================================================================== --
-- 								Build Addenum Table 
-- ============================================================================== --						
						
use [sistemas]
-- go

-- select * from sistemas.dbo.addenum_tbl_albaran_relations

IF OBJECT_ID('sistemas.dbo.addenum_tbl_albaran_relations', 'U') IS NOT NULL 
  DROP TABLE sistemas.dbo.addenum_tbl_albaran_relations
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on
-- go
create table addenum_tbl_albaran_relations(
		 id					bigint		identity(1,1)
	 	,batnbr				char(10)	null -- artran.batnbr  // OK
	 	,num_guias			text 		null -- num_guias	   // OK
--	 	,no_remision		text 		null
	 	,RefNbr				char(10) 	null -- artran.refnbr  // OK
	 	,no_guia			int			null 
	 	,transaccion		char(50)	null --Número de Nota de entrega, Remisión, Carta Porte , etc. Con el que fue entregada la mercancia o servicio  (proveedor)
	 	,IdPedido			bigint		null --Número de  pedido (EUM) a la que hace referencia la factura 45XXXXXXXX
	 	,Albaran			bigint 		null --El documento de material que se genera al darle entrada al material y/o servicio por parte del almacenista y/o liberación de embarques para transportistas 50XXXXXXXX
		,description		text		null --Optional
		,created			datetime 	default current_timestamp null
		,modified			datetime	null
		,status				tinyint 	default 1 null
) on [primary]
-- go
set ansi_padding off



-- ======================================================================================================================== --
-- 		Addenum Information
-- ======================================================================================================================== --

use [sistemas]

select top 100 * from sistemas.dbo.addenum_view_albaran_relations order by BatNbr desc where BatNbr = 122528 


select * from [192.168.20.240].integraapp.dbo.ARTran where BatNbr in ('499213', '498102')

select * from integraapp.dbo.ARTran where BatNbr in ('499213', '498102')

select * from integraapp.dbo.ARDoc where BatNbr in ('499213', '498102')

select * from integraapp.dbo.XGRWFEInvc where BatNbr in ('499213', '498102')

select * from integraapp.dbo.Batch where BatNbr in ('499213', '498102')

select * from integraapp.dbo.XGRWFEDocs where BatNbr in ('499213', '498102')


use sistemas
IF OBJECT_ID ('addenum_view_albaran_relations', 'V') IS NOT NULL
    DROP VIEW addenum_view_albaran_relations;
-- now build the view
alter view addenum_view_albaran_relations
with encryption
as -- 
	select 
			 convert(varchar(max),XmlData) as 'xml'
			,convert(varchar(max),PdfData) as 'pdf'
			,row_number() over(partition by "inv".BatNbr,"doc".CuryTxblTot00,"doc".CuryOrigDocAmt,"doc".TaxTot00 order by "inv".BatNbr) as 'index'
	       	,ltrim(rtrim("tran".RefNbr)) as 'RefNbr'
	       	,ltrim(rtrim("tran".BatNbr)) as 'BatNbr'
	       	,"lois".no_remision
			,'http://factura.envasesuniversales.com/addenda/eu http://factura.envasesuniversales.com/addenda/eu/EU_Addenda.xsd' as "@xsi:schemaLocation"
			,ltrim(rtrim("inv".S4Future01)) as 'IdFactura' -- inside TipoFactura
--			,"xg".Version as 'Version'	-- inside TipoFactura
			,'1.0' as 'Version'	-- inside TipoFactura
			,cast("xg".FechaTimbrado as date) as 'FechaMensaje'	-- inside TipoFactura
	       	,'Con_Pedido' as 'IdTransaccion'  -- eu:TipoTransaccion
--	       	,ltrim(rtrim("tran".ExtRefNbr)) as 'eu:TipoTransaccion/eu:Transaccion'
--	       	,ltrim(rtrim("tran".RefNbr)) as 'eu:TipoTransaccion/eu:Transaccion'
	       	,ltrim(rtrim("lois".no_remision)) as 'Transaccion' -- eu:TipoTransaccion
		   	,1 as "eu:OrdenesCompra/eu:Secuencia/@consec"
		   	,"upload".id as 'idx'
		   	,ltrim(rtrim("upload".IdPedido)) as 'IdPedido' --eu:OrdenesCompra
		   	,ltrim(rtrim("upload".Albaran)) as 'Albaran'  --eu:OrdenesCompra/eu:Secuencia/eu:EntradaAlmacen
	       	,case "inv".CuryId when 'MN' then 'MXN' else "inv".CuryId end as 'MonedaCve'  -- eu:Moneda
	       	,case "inv".CuryId when 'MN' then cast(1 as decimal(16,4)) else cast(1 as decimal(16,4)) end  as 'TipoCambio'  -- eu:Moneda
	       	,cast("doc".CuryTxblTot00 as decimal(18,2)) as 'SubtotalM'  -- eu:Moneda
	       	,cast("doc".CuryOrigDocAmt as decimal(18,2)) as 'TotalM'  -- eu:Moneda
	       	,cast("doc".TaxTot00 as decimal(18,2)) as 'ImpuestoM'  -- eu:Moneda
	       	,cast(abs("doc".TaxTot01) as decimal(18,2)) as 'BaseImpuesto'   --eu:ImpuestosR
	        ,"xg".CadenaOriginal,"lois".no_guia
			,"lois".num_guia
        	,"lois".no_viaje,"lois".subtotal,"lois".desc_flete,"lois".observacion
        	,"lois".id_area
	       	,"lois".FlagIsDisminution,"lois".FlagIsProvision
	        ,"tran".PerPost,"lois".fecha_guia
	        ,"lois".status_guia
--	        ,"inv".BatNbr
--	       	,*
	from 
			[192.168.20.240].integraapp.dbo.XGRWFEDocs as "xg" -- source origin
		inner join 
			[192.168.20.240].integraapp.dbo.XGRWFEInvc as "inv" -- extra solomon
		on 
			"xg".HdrId = "inv".id
		left join 
			[192.168.20.240].integraapp.dbo.ARDoc as "doc"
		on 
			"inv".BatNbr = "doc".BatNbr and "inv".RefNbr = "doc".RefNbr and "inv".CpnyId = "doc".CpnyID --and "doc".PerPost > 202000
		left join 
			[192.168.20.240].integraapp.dbo.ARTran as "tran"
		on 
			"inv".BatNbr = "tran".BatNbr and "inv".RefNbr = "tran".RefNbr and "tran".CpnyId = "doc".CpnyID and "doc".LUpd_Prog = "tran".LUpd_Prog
		left join 
			sistemas.dbo.projections_view_full_gst_core_indicators as "lois"
		on 
			ltrim(rtrim("tran".ExtRefNbr)) = "lois".num_guia collate SQL_Latin1_General_CP1_CI_AS
		left join 
			sistemas.dbo.addenum_tbl_albaran_relations as "upload"
		on 
--			(
			"upload".batnbr collate SQL_Latin1_General_CP1_CI_AS = "inv".BatNbr
--			or
--			"upload".Refnbr collate SQL_Latin1_General_CP1_CI_AS = "tran".RefNbr
--			or
--			"upload".num_guias collate SQL_Latin1_General_CP1_CI_AS = "tran".ExtRefNbr
--			)
	where 
		"inv".custId = 'EUM000707DQ2'
--	and 
--		"inv".BatNbr = 102575



	
	

WITH XMLNAMESPACES (
					'http://factura.envasesuniversales.com/addenda/eu' as "eu"
				   ) 		
select *
--		 ,"xml".pdf,
--		 "xml"."@xsi:schemaLocation"
--		,"xml"."eu:TipoFactura/eu:IdFactura"
--		,"xml"."eu:TipoFactura/eu:Version"
--		,"xml"."eu:TipoFactura/eu:FechaMensaje"
--		,"xml"."eu:TipoTransaccion/eu:IdTransaccion"
--		,"xml"."eu:TipoTransaccion/eu:Transaccion"
--		,"xml"."eu:OrdenesCompra/eu:Secuencia/@consec"
--		,"xml"."eu:OrdenesCompra/eu:Secuencia/eu:IdPedido"
--		,"xml"."eu:OrdenesCompra/eu:Secuencia/eu:EntradaAlmacen/eu:Albaran"
--		,"xml"."eu:Moneda/eu:MonedaCve"
--		,"xml"."eu:Moneda/eu:TipoCambio"
--		,"xml"."eu:Moneda/eu:SubtotalM"
--		,"xml"."eu:Moneda/eu:TotalM"
--		,"xml"."eu:Moneda/eu:ImpuestoM"
--		,"xml"."eu:ImpuestosR/eu:BaseImpuesto"
	from sistemas.dbo.addenum_view_albaran_relations as "xml"
where 
	"index" = 1
and
	BatNbr = 102575

FOR XML path 
							('eu:AddendaEU') 
							,ELEMENTS XSINIL 

--method upload file 



--period	


select 
--		 "albaran".id_area
--	     ,"albaran".no_viaje
		 (
			select
					',' + "guiax".num_guia
			from 
					sistemas.dbo.addenum_view_albaran_relations as "guiax"
			where 
					"guiax".BatNbr = "albaran".BatNbr and "guiax".no_guia = "albaran".no_guia
			for xml path('')
		 ) as 'num_guiax'
		 ,"albaran".BatNbr 
from 
	sistemas.dbo.addenum_view_albaran_relations as "albaran"  where "albaran".BatNbr = 104923
group by 
--		  "albaran".id_area
--	  	 ,"albaran".no_viaje
	"albaran".no_guia
		 ,"albaran".BatNbr

	 
	 



select * from sistemas.dbo.addenum_view_albaran_relations  where BatNbr = 104923


select IdFactura from sistemas.dbo.addenum_view_albaran_relations group by IdFactura

-- truncate table  sistemas.dbo.addenum_tbl_albaran_relations



select * from sistemas.dbo.addenum_tbl_albaran_relations	
	
	
	
	
	








-- Create the relation


insert into sistemas.dbo.addenum_tbl_albaran_relations(num_guias,transaccion,batnbr,Refnbr,IdPedido) values ('CU-182887','82676635','102571','082058',4501818093)

insert into sistemas.dbo.addenum_tbl_albaran_relations(num_guias,transaccion,batnbr,RefNbr,IdPedido,Albaran)
values ('CU-182887','82676635','102571','082058',4501818093,5051389522)


insert into sistemas.dbo.addenum_tbl_albaran_relations (num_guias,transaccion,batnbr,RefNbr,IdPedido,Albaran)
values 
		('CU-182887',				'82676635',		'102571',	 	'082058',	4501818093,	5051389522),
		('CU-182947',				'82678271', 	'102572', 		'082059',	4501818090,	5051388974),
		('CU-182948',				'82678216',		'102574',		'082061',	4501818068,	5051388231),
		('CU-183066 / CU-183190',	'5779 / 5771',	'102575',		'082062',	4501818094,	5051389589),
		('CU-183145 / CU-184520',	'5784 / 5883',	'102576',		'082063',	4501818082,	5051388655),
		('CU-183276 / CU-184074',	'5780 / 5866',	'102577',		'082064',	4501818092,	5051389518)
       
	




--for join tables 
--	collation SQL_Latin1_General_CP1_CI_AS for integraapp
--	collation Modern_Spanish_CI_AS 		   for zam-suite



--use gstdb_pba 
--	select * from gstdb_pba.dbo.trafico_guia
--	exec sp_desc trafico_guia
--
--use integraapp
--exec sp_desc batch						
						
--						
--select 
--		 convert(varchar(max),"xg".XmlData) as 'xml'
--		,convert(varchar(max),"xg".PdfData) as 'pdf'
--		,"xg".*
--		,"inv".*
--from 
--		integraapp.dbo.XGRWFEDocs as "xg"
--	inner join 
--		integraapp.dbo.XGRWFEInvc as "inv"
--	on 
--		"xg".HdrId = "inv".id
--where 
--	"inv".custId = 'EUM000707DQ2'
--
--
--
--and BatNbr = 082881
--
--
--
--
--
--
--
--
--
--
--
--
