
-- ========================================================================================================= --
--  Reporte de ??
-- ========================================================================================================= --
use sistemas
IF OBJECT_ID ('rentabilidad_view_units_liquidations', 'V') IS NOT NULL
    DROP VIEW rentabilidad_view_units_liquidations

--create
alter
view rentabilidad_view_units_liquidations
with encryption
as

with "REPORTE" as (

	select
	 case
			when row_number () over (partition by tl.id_area, tl.no_liquidacion,tl.fecha_liquidacion,tl.id_unidad order by tl.no_liquidacion) > 1 then 0
		else
				tl.flete_liquidacion
		end as 'Ingreso total de ruta'
	,"areas".label as 'UdN'
	,tl.no_liquidacion as 'liquidacion'
	,tl.fecha_liquidacion
	,"month".month_uname  as 'MES'
	,tl.id_unidad  as 'Unidad'
	,trl.desc_concepto
	,case
		when trl.id_concepto in (301, 302, 303) then 'COMBUSTIBLE'
		when trl.id_concepto in (304, 305, 306) then 'CASETAS'
		when trl.id_concepto in (307, 332, 343, 344, 346, 347, 348) then 'SUELDO LIQUIDACION'
	else
		'OTROS'
--		when trl.id_concepto not in (301, 302, 303, 304, 305, 306, 307, 332, 343, 344, 346, 347, 348) then 'OTROS'
	end as 'CONCEPTO'
	,(trl.cantidad ) as 'Cantidad'
	,trl.monto_concepto as 'Costo'

	,case
		when
			trl.id_concepto in (307, 332, 343, 344, 346, 347, 348)
		then
			tl.monto_sueldo
		else
			0
	 end as 'monto_sueldo'

	from
		gstdb.dbo.trafico_liquidacion tl
	left join
		sistemas.dbo.projections_view_bussiness_units as "areas"
		on "tl".id_area = "areas".id_area
	left join
		sistemas.dbo.generals_month_translations as "month"
		on
			month(tl.fecha_liquidacion) = "month".month_num
	inner join
		gstdb.dbo.trafico_renglon_liquidacion trl on tl.id_area  = trl.id_area  and tl.no_liquidacion = trl.no_liquidacion
	where
		tl.status_liq = 'A' --and tl.id_area = 1
	and
		tl.fecha_liquidacion BETWEEN  '2019-01-01 00:00:00' and '2019-05-31 23:00:00'
	--and trl.id_concepto  in (1,2,301,302,303,304,305,306)
--	and tl.no_liquidacion in ( 23066 ,23067 ) -- monto => 2658.4
) --select * from REPORTE
select
	 REPORTE.UdN,REPORTE.liquidacion,REPORTE.fecha_liquidacion,REPORTE.mes,REPORTE.Unidad,REPORTE.CONCEPTO
	,SUM (REPORTE.Cantidad) as 'Cantidad'
	,SUM (REPORTE.Costo) as 'Cost'
	,SUM (REPORTE.[Ingreso total de ruta]) as 'Ingreso total ruta'
	,"REPORTE".monto_sueldo
	,case
		when
			"REPORTE".CONCEPTO = 'SUELDO LIQUIDACION'
	 	then
	 		"REPORTE".monto_sueldo + SUM (REPORTE.Costo)
	 	else
	 		SUM (REPORTE.Costo)
	 end as 'Costo'
from
	REPORTE
GROUP  by
	 REPORTE.UdN,REPORTE.liquidacion,REPORTE.fecha_liquidacion,REPORTE.mes,REPORTE.Unidad,REPORTE.CONCEPTO
	,"REPORTE".monto_sueldo



-- ========================================================================================================= --

use sistemas
IF OBJECT_ID ('rentabilidad_view_viajes_liquidations', 'V') IS NOT NULL
    DROP VIEW rentabilidad_view_viajes_liquidations

--create
--alter
view rentabilidad_view_viajes_liquidations
with encryption
as
	with "viajes" as (
	select
			 no_viaje
			,id_unidad
			,fecha_real_viaje
			,fecha_real_fin_viaje
			,kms_camion_lleno
			,kms_camion_vacio
			,no_liquidacion
	from
			gstdb.dbo.trafico_viaje
   )
   select
  		"trips".id_unidad
  		,"trips".no_liquidacion
  		,count(no_viaje) as "viajes"
		,min ("trips".fecha_real_viaje) as "fecha_real_viaje"
		,max ("trips".fecha_real_fin_viaje) as "fecha_real_fin_viaje"
		,sum ("trips".kms_camion_lleno) as "kms_camion_lleno"
		,sum ("trips".kms_camion_vacio) as "kms_camion_vacio"
   	from
   		"viajes" as "trips"
--	where
--			"trips".no_liquidacion = 23066
	group by
		  "trips".id_unidad
		 ,"trips".no_liquidacion



-- ========================================================================================================= --

-- ========================================================================================================= --
--  For pivot Example
-- ========================================================================================================= --


SELECT <non-pivoted column>,
    [first pivoted column] AS <column name>,
    [second pivoted column] AS <column name>,
    ...
    [last pivoted column] AS <column name>
FROM
    (<SELECT query that produces the data>)
    AS <alias for the source query>
PIVOT
(
    <aggregation function>(<column being aggregated>)
FOR
[<column that contains the values that will become column headers>]
    IN ( [first pivoted column], [second pivoted column],
    ... [last pivoted column])
) AS <alias for the pivot table>
<optional ORDER BY clause>;

-- ========================================================================================================= --

select top 10 * from sistemas.dbo.rentabilidad_view_units_liquidations -- to pi
select top 10 * from sistemas.dbo.rentabilidad_view_viajes_liquidations --by one

select * from sistemas.dbo.casetas_corporations

select * from blackops.users

select
		*
from
		sistemas.dbo.rentabilidad_view_units_liquidations as "liq"
	left join
		sistemas.dbo.rentabilidad_view_viajes_liquidations as "trip"
	on
		"liq".liquidacion = "trip".no_liquidacion and "liq".unidad = "trip".id_unidad
where
	"liq".liquidacion in ( 23066 ,23067 )



--try pivot
   with "report" as (
		select
			 UdN
			,liquidacion
			,fecha_liquidacion
			,mes
			,Unidad
	--		,CONCEPTO
			,Cantidad
	--		,Cost
	--		,[Ingreso total ruta]
	--		,monto_sueldo
	--		,Costo
			,[COMBUSTIBLE]
			,[CASETAS]
			,[SUELDO LIQUIDACION]
			,[OTROS]
		from (
			select
			       row_number () over (order by "liquidation".unidad) as 'id'
			      ,row_number () over 
						 (  partition by 
								   "liquidation".UdN
								  ,"liquidation".liquidacion
								  ,"liquidation".fecha_liquidacion
								  ,"liquidation".unidad 
						    order by "liquidation".liquidacion
						)
			      ,"liquidation".UdN
			      ,"liquidation".liquidacion
			      ,"liquidation".fecha_liquidacion
			      ,"liquidation".mes
			      ,"liquidation".Unidad
			      ,"liquidation".CONCEPTO
			      ,"liquidation".Cantidad
			      ,"liquidation".Cost
			      ,"liquidation".[Ingreso total ruta]
			      ,"liquidation".monto_sueldo
			      ,"liquidation".Costo
			from
			      sistemas.dbo.rentabilidad_view_units_liquidations as "liquidation"
		) as "SourceTable"
		  pivot
		(
			max(liquidacion) for CONCEPTO in (
							     [COMBUSTIBLE]
							    ,[CASETAS]
							    ,[SUELDO LIQUIDACION]
							    ,[OTROS]
			)
		) as PivotTable
	) -- end with
select * from "report"
