
-- ========================================================================================================= --
--  Reporte de ??
-- ========================================================================================================= --
use sistemas
IF OBJECT_ID ('rentabilidad_view_units_liquidations', 'V') IS NOT NULL
    DROP VIEW rentabilidad_view_units_liquidations

--create
alter
view rentabilidad_view_units_liquidations
with encryption
as

with "REPORTE" as (

	select
	 case
			when row_number () over (partition by tl.id_area, tl.no_liquidacion,tl.fecha_liquidacion,tl.id_unidad order by tl.no_liquidacion) > 1 then 0
		else
				tl.flete_liquidacion
		end as 'Ingreso total de ruta'
	,"tl".id_area
	,"areas".label as 'UdN'
--	,trl.id_concepto as 'id_concepto'
	,tl.no_liquidacion as 'liquidacion'
	,tl.fecha_liquidacion
	,"month".month_uname  as 'MES'
	,tl.id_unidad  as 'Unidad'
	,trl.Concepto
	,isnull(trl.cantidad,0) as 'Cantidad'
	,isnull(trl.monto_concepto,0) as 'Costo'
	,isnull(tl.monto_sueldo,0) as 'monto_sueldo'
--	,case
--		when
--			trl.id_concepto in (307, 332, 343, 344, 346, 347, 348)
--		then
--			isnull(tl.monto_sueldo,0)
--		else
--			0
--	 end as 'monto_sueldo'

	from
		gstdb.dbo.trafico_liquidacion tl
	left join
		sistemas.dbo.projections_view_bussiness_units as "areas"
		on "tl".id_area = "areas".id_area
	left join
		sistemas.dbo.generals_month_translations as "month"
		on
			month(tl.fecha_liquidacion) = "month".month_num
	inner join
		sistemas.dbo.rentabilidad_view_renglon_liquidations trl on tl.id_area  = trl.id_area  and tl.no_liquidacion = trl.no_liquidacion
	where
		tl.status_liq = 'A' --and tl.id_area = 1
	and
		year(tl.fecha_liquidacion) > 2018
--	and tl.no_liquidacion in (26906) and tl.id_area = 1
) --select * from REPORTE
select
	 "REPORTE".id_area,REPORTE.UdN,REPORTE.liquidacion,REPORTE.fecha_liquidacion,REPORTE.mes,REPORTE.Unidad,REPORTE.CONCEPTO
	,SUM (REPORTE.Cantidad) as 'Cantidad'
	,SUM (REPORTE.Costo) as 'Cost'
	,SUM (REPORTE.[Ingreso total de ruta]) as 'Ingreso total ruta'
	,"REPORTE".monto_sueldo
	,case  -- NOTE firts if monto_sueldo exits has precedence in the calculation 
		when
				"REPORTE".CONCEPTO = 'CONCEPTOS SUELDO'
	 	then
--	 		case
--		 		when 
--		 			(sum(REPORTE.Costo) > 0 )
--		 		then 
		 			isnull("REPORTE".monto_sueldo,0) + SUM(isnull(REPORTE.Costo,0)) --as 'Costo'
--		 		else
--		 			(isnull(REPORTE.monto_sueldo,0))
--	 		end
	 	else 
			max(isnull(REPORTE.costo,0))
	 end as 'Costo'
from
	REPORTE
--where liquidacion = 26906 and id_area = 1
GROUP  by
	 "REPORTE".id_area,REPORTE.UdN,REPORTE.liquidacion,REPORTE.fecha_liquidacion,REPORTE.mes,REPORTE.Unidad,REPORTE.CONCEPTO
	,"REPORTE".monto_sueldo


-- ========================================================================================================= --
-- Simplification TRL
-- ========================================================================================================= --
--select * from sistemas.dbo.rentabilidad_view_renglon_liquidations where no_liquidacion in (26906) and id_area = 8

use sistemas
IF OBJECT_ID ('rentabilidad_view_renglon_liquidations', 'V') IS NOT NULL
    DROP VIEW rentabilidad_view_renglon_liquidations
-- create
-- alter
view rentabilidad_view_renglon_liquidations
with encryption
as	
with "liquit" as (
select 
		 "rliquit".id_area
		,"rliquit".no_liquidacion
		,case -- NOTE Calcualte firts if monto_sueldo > 0 then begin clasification
			when "rliquit".id_concepto in (301, 302, 303) then 'COMBUSTIBLE'
			when "rliquit".id_concepto in (304, 305, 306) then 'CASETAS'
			when "rliquit".id_concepto in (307, 332, 343, 344, 346, 347, 348) then 'CONCEPTOS SUELDO'
			when "rliquit".id_concepto not in (301, 302, 303, 304, 305, 306, 307, 332, 343, 344, 346, 347, 348) then 'OTROS'
		 end as 'Concepto'
		,"rliquit".cantidad
		,"rliquit".monto_concepto
from 
		gstdb.dbo.trafico_renglon_liquidacion as "rliquit"
)
select 
	 "liquit".id_area
	,"liquit".no_liquidacion
	,"liquit".Concepto
	,sum("liquit".cantidad) as 'cantidad'
	,sum("liquit".monto_concepto) as 'monto_concepto' 
from 
	"liquit"
group by
		 "liquit".id_area
		,"liquit".no_liquidacion
		,"liquit".Concepto

	
	
use sistemas
IF OBJECT_ID ('rentabilidad_view_viajes_liquidations', 'V') IS NOT NULL
    DROP VIEW rentabilidad_view_viajes_liquidations

-- NOTE Update Add field rendimiento_reseteo 
-- create
alter
view rentabilidad_view_viajes_liquidations
with encryption
as
    with "viajes" as (
	    select
	             "viaje".id_area
	            ,"viaje".no_viaje
	            ,"viaje".id_unidad
						  ,"viaje".rendimiento_reseteo
/*	    		,case 
	            	when
	                	"trviaje".no_guia > 0
	            	then
	                	"viaje".fecha_real_viaje
	             end as "FechaIni"
	    		,case 
	            	when
	                	"trviaje".no_guia > 0
	            	then
	                	"viaje".fecha_real_fin_viaje
	             end as "FechaEnd"
	*/
							,"viaje".fecha_real_viaje as 'FechaIni'
							,"viaje".fecha_real_fin_viaje as 'FechaEnd'
	            ,"viaje".fecha_real_viaje
	            ,"viaje".fecha_real_fin_viaje
	    		,case 
	             	when
	                	"trviaje".no_guia > 0
	            	then
	                	"viaje".kms_real
	             end as "kmsCargados"
	    		,case 
	            	when
	                	"trviaje".no_guia = 0
	            	then
	                	"viaje".kms_real
	             end as "kmsVacios"
	    		,case    
	            	when
	                	"trviaje".no_guia = 0
	            	then
	                	0
	            	else
	                	1       
	             end as "IsEmpty"
	            ,"viaje".no_liquidacion
	    from
	            gstdb.dbo.trafico_viaje as "viaje"
	    left join
	            gstdb.dbo.trafico_renglon_viaje "trviaje" 
	     on 
	     		"viaje".no_viaje = "trviaje".no_viaje and "viaje".id_area = "trviaje".id_area
	   )
	   select
	           "trips".id_area
	          ,"trips".id_unidad
	          ,"trips".no_liquidacion
						,"trips".rendimiento_reseteo
	    --    ,count(no_viaje) as "viajes"
			  ,Sum(IsEmpty) as "viajes"
	       	  ,min ("trips".fecha_real_viaje) as 'del'
	          ,max ("trips".fecha_real_fin_viaje) as 'al'
	    --      ,datediff(dd,min ("trips".FechaIni),max ("trips".FechaEnd)) as 'DuracionViaje'
	          ,sum ("trips".kmsCargados) as "kms_camion_lleno"
	          ,sum ("trips".kmsVacios) as "kms_camion_vacio"
	       from
	           "viajes" as "trips"
	    --where
	    --        "trips".no_liquidacion = (27063, 27254)
	    group by
	           "trips".id_area
	          ,"trips".id_unidad
	          ,"trips".no_liquidacion
						,"trips".rendimiento_reseteo
         
-- POWERED BY JMB & JGV

 

-- ========================================================================================================= --

-- ========================================================================================================= --
--  For pivot Example
-- ========================================================================================================= --


SELECT <non-pivoted column>,
    [first pivoted column] AS <column name>,
    [second pivoted column] AS <column name>,
    ...
    [last pivoted column] AS <column name>
FROM
    (<SELECT query that produces the data>)
    AS <alias for the source query>
PIVOT
(
    <aggregation function>(<column being aggregated>)
FOR
[<column that contains the values that will become column headers>]
    IN ( [first pivoted column], [second pivoted column],
    ... [last pivoted column])
) AS <alias for the pivot table>
<optional ORDER BY clause>;

-- ========================================================================================================= --

testing etc 

select * from sistemas.dbo.rentabilidad_view_units_liquidations where liquidacion in (26906) and id_area = 8 -- to pivot

select * from sistemas.dbo.rentabilidad_view_viajes_liquidations --by one
	
select * from sistemas.dbo.rentabilidad_view_costos_liquidations where liquidacion in (26906) and id_area = 8 -- to pivot

select * from sistemas.dbo.rentabilidad_view_qty_liquidations where liquidacion in (26906) and id_area = 8
--try pivots
-- Pivot Pivots
use sistemas
IF OBJECT_ID ('rentabilidad_view_costos_liquidations', 'V') IS NOT NULL
    DROP VIEW rentabilidad_view_costos_liquidations

-- create
--
alter
view rentabilidad_view_costos_liquidations
with encryption
as
	with "report_cost" as (
		select 
			 id_area
			,UdN
			,liquidacion
			,fecha_liquidacion
			,mes
			,Unidad
	--		,CONCEPTO
--			,Cantidad
	--		,Cost
	--		,[Ingreso total ruta]
	--		,monto_sueldo
	--		,Costo	
			,[COMBUSTIBLE]
			,[CASETAS]
			,[CONCEPTOS SUELDO]
			,[OTROS]
		from (
				select 
					 "liquidation".id_area
					,"liquidation".UdN
					,"liquidation".liquidacion
					,"liquidation".fecha_liquidacion
					,"liquidation".mes
					,"liquidation".Unidad
					,"liquidation".CONCEPTO
--					,"liquidation".Cantidad
--					,"liquidation".Cost
--					,"liquidation".[Ingreso total ruta]
--					,"liquidation".monto_sueldo
					,"liquidation".Costo		
				from 
					sistemas.dbo.rentabilidad_view_units_liquidations as "liquidation"
		) as "SourceTable"
		  pivot
		(
			max(Costo) for CONCEPTO in (
							 [COMBUSTIBLE]
							,[CASETAS]
							,[CONCEPTOS SUELDO]
							,[OTROS]						
			)			
		) as PivotTable
	) -- end with
	select * from "report_cost" --where liquidacion in ( 23066 ) --,23067 )
	where liquidacion = 26906





use sistemas
IF OBJECT_ID ('rentabilidad_view_qty_liquidations', 'V') IS NOT NULL
    DROP VIEW rentabilidad_view_qty_liquidations

-- create
-- alter
view rentabilidad_view_qty_liquidations
with encryption
as	
	with "report_qty" as (
		select 
			 id_area
			,UdN
			,liquidacion
			,fecha_liquidacion
			,mes
			,Unidad
	--		,CONCEPTO
--			,Cantidad
	--		,Cost
	--		,[Ingreso total ruta]
	--		,monto_sueldo
	--		,Costo	
			,[COMBUSTIBLE]
			,[CASETAS]
			,[CONCEPTOS SUELDO]
			,[OTROS]
		from (
				select 
--				     row_number () over (order by "liquidation".unidad) as 'id'
--				    ,row_number () over (partition by "liquidation".UdN, "liquidation".liquidacion,"liquidation".fecha_liquidacion,"liquidation".unidad order by "liquidation".liquidacion)
					 "liquidation".id_area
					,"liquidation".UdN
					,"liquidation".liquidacion
					,"liquidation".fecha_liquidacion
					,"liquidation".mes
					,"liquidation".Unidad
					,"liquidation".CONCEPTO
					,"liquidation".Cantidad
--					,"liquidation".Cost
--					,"liquidation".[Ingreso total ruta]
--					,"liquidation".monto_sueldo
--					,"liquidation".Costo
				from 
					sistemas.dbo.rentabilidad_view_units_liquidations as "liquidation"
		) as "SourceTable"
		  pivot
		(
			max(Cantidad) for CONCEPTO in (
								 [COMBUSTIBLE]
								,[CASETAS]
								,[CONCEPTOS SUELDO]
								,[OTROS]						
			)			
		) as PivotTable
	) -- end with
	select * from "report_qty" --where liquidacion in ( 23066 ) --,23067 )

	
-- =========================================================================================================== --
-- NOTE Build the preGerencial	
-- =========================================================================================================== --	
use sistemas

    select top 10 * from gstdb.dbo.trafico_liquidacion
    select top 10 * from sistemas.dbo.rentabilidad_view_viajes_liquidations 
    select top 10 * from sistemas.dbo.rentabilidad_view_costos_liquidations 
    select top 10 * from sistemas.dbo.rentabilidad_view_qty_liquidations


use sistemas
IF OBJECT_ID ('rentabilidad_view_main_liquidations', 'V') IS NOT NULL
    DROP VIEW rentabilidad_view_main_liquidations
-- create
alter
view rentabilidad_view_main_liquidations
with encryption
as	
	select
		   row_number() over (order by "cost".id_area) as 'id'
			,row_number() over (partition by "cost".id_area,"cost".Unidad order by "cost".Unidad) as 'order'
		--	,rank() over (partition by "cost".Unidad order by "cost".Unidad) as 'block'
		  ,"cost".id_area
			,year("cost".fecha_liquidacion) as "year" 
			,"cost".UdN as 'UnidadNegocio'
			,"cost".liquidacion 
			,cast("cost".fecha_liquidacion as date) as 'fecha_liquidacion'
			,"cost".mes as  'Mes'
--			,'' as 'Operador'
--			,'' as 'TipoOperacion'
			,"cost".Unidad
			,"cost".COMBUSTIBLE
			,"cost".CASETAS
--			,"cost".[CONCEPTOS SUELDO]
			,case 
				when 
					"cost".[CONCEPTOS SUELDO] is null
				then 
					(
						select monto_sueldo from gstdb.dbo.trafico_liquidacion where no_liquidacion = "cost".liquidacion and id_area = "cost".id_area
					)
				else 
					"cost".[CONCEPTOS SUELDO]
			 end as [CONCEPTOS_SUELDO]
			,"cost".OTROS 
			,"qty".COMBUSTIBLE as 'qtyCombustible'
			,"qty".CASETAS as 'qtyCasetas'
			,"qty".[CONCEPTOS SUELDO] as 'qtySueldoLiquidacion'
			,"qty".OTROS as 'qtyOtros'
			,( isnull("cost".OTROS,0) + isnull("cost".COMBUSTIBLE,0) + isnull("cost".CASETAS,0) + isnull("cost".[CONCEPTOS SUELDO],0) ) as 'CostoDirectoViaje' 

			,((isnull("cost".OTROS,0) / "liquid".flete_liquidacion )*100) as 'percent_otros'
			,((isnull("cost".COMBUSTIBLE,0) / "liquid".flete_liquidacion )*100) as 'percent_combustible'
			,((isnull("cost".CASETAS,0) / "liquid".flete_liquidacion )*100) as 'percent_casetas'
			,((isnull("cost".[CONCEPTOS SUELDO],0) / "liquid".flete_liquidacion )*100) as 'percent_conceptos_sueldo'
			,(( isnull("cost".OTROS,0) + isnull("cost".COMBUSTIBLE,0) + isnull("cost".CASETAS,0) + isnull("cost".[CONCEPTOS SUELDO],0) ) / "liquid".flete_liquidacion)*100 as 'percent_CostoDirectoViaje' 
			,"liquid".flete_liquidacion as 'IngresoTotalRuta' 
			,"trip".viajes
			,"trip".rendimiento_reseteo
			,cast("trip".del as date) as 'del'
			,cast("trip".al as date) as 'al'
--			,"trip".DuracionViaje
			,"trip".kms_camion_lleno as 'KmsCaminoLleno'
			,"trip".kms_camion_vacio as 'KmsCamionVacio'
			,("trip".kms_camion_lleno + "trip".kms_camion_vacio) / ("qty".COMBUSTIBLE) as 'RendViaje' --this ugly VsStyle code is an aportation from one ImBREcil
			,"liquid".dias as 'DuracionViaje'     -- Agregado 06/07/2020
--			,"trip".*
	from 
			sistemas.dbo.rentabilidad_view_costos_liquidations as "cost"
		inner join 
			sistemas.dbo.rentabilidad_view_qty_liquidations as "qty"
		on 
			"cost".liquidacion = "qty".liquidacion and "cost".unidad = "qty".unidad
		left join
			sistemas.dbo.rentabilidad_view_viajes_liquidations as "trip" -- Insert Field rendimiento_reseteo
		on
			"cost".liquidacion = "trip".no_liquidacion and "cost".unidad = "trip".id_unidad
		left join 
			gstdb.dbo.trafico_liquidacion as "liquid"
		on 
			"liquid".id_area = "cost".id_area and "liquid".no_liquidacion = "cost".liquidacion
--	where
--		"cost".liquidacion in (26906) -- in ( 23066 ) --,23067 )
		


		select top 10 * from gstdb.dbo.trafico_liquidacion
		select top 10 * from sistemas.dbo.rentabilidad_view_renglon_liquidations 
    select top 10 * from sistemas.dbo.rentabilidad_view_costos_liquidations 
    select top 10 * from sistemas.dbo.rentabilidad_view_qty_liquidations

				select  top 10 * from sistemas.dbo.rentabilidad_view_viajes_liquidations  -- Main viajes Compount
						select top 10 rendimiento_reseteo,* from  gstdb.dbo.trafico_viaje			-- Component  
						select top 10 * from  gstdb.dbo.trafico_renglon_viaje									-- Component


		select --*
			IngresoTotalRuta,viajes,Unidad,liquidacion,UnidadNegocio,id_area 
		from 
			sistemas.dbo.rentabilidad_view_main_liquidations where Unidad='TT1235'
		and 
			fecha_liquidacion between '2021-01-11' and '2021-01-12' -- liquidacion = 31822 --and id_area = 2


  select 
					--count(liquidacion) as 'Num',
		--			block,liquidacion,"id_area","Unidad","Year","Mes"
					*
	from 
				sistemas.dbo.rentabilidad_view_main_liquidations
	where 
				"id_area" = 1 and "Year" = 2020 and Mes = 'Octubre' and Unidad in ('TT1219')
	group by 
				"id_area","Unidad","Year","Mes"
	order by 
				"Year","Mes" desc


	where 
			"Unidad" = 'TT1190'
	and 
			"Year" = '2020' 
	and	
			"Mes" = 'Septiembre'


--  fecha_liquidacion and Unidad

-- DEBUG Cakephp query

