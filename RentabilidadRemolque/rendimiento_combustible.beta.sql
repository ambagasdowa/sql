USE [sistemas]
GO

/****** Object:  View [dbo].[rentabilidad_unidad_v1]    Script Date: 28/08/2020 12:42:04 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE
view [dbo].[rentabilidad_unidad_v1]
as

	select 
			 "cost".id_area
			,YEAR ("cost".fecha_liquidacion) as 'A�o'
			,"cost".UdN
			,"cost".liquidacion
			,cast("cost".fecha_liquidacion as date) as 'fecha_liquidacion'
			,"cost".mes
			,"cost".Unidad
			,Round ("cost".COMBUSTIBLE, 2) as 'COMBUSTIBLE'
			,Round ("cost".CASETAS, 2) as 'CASETAS'
--			,"cost".[CONCEPTOS SUELDO]
			,case 
				when 
					"cost".[CONCEPTOS SUELDO] is null
				then 
					(
						select monto_sueldo from gstdb.dbo.trafico_liquidacion where no_liquidacion = "cost".liquidacion and id_area = "cost".id_area
					)
				else 
					Round ("cost".[CONCEPTOS SUELDO], 2) 
			 end as [CONCEPTOS SUELDO]
			,Round ("cost".OTROS, 2) as 'OTROS'
			,"qty".COMBUSTIBLE as 'Litros'
			,"qty".CASETAS as 'Cantidad de Casetas'
			,"qty".[CONCEPTOS SUELDO] as 'Cantidad de Conceptos'
			,"qty".OTROS as 'qtyOtros'
			,"liquid".flete_liquidacion as 'IngresoTotalRuta' 
			,"trip".viajes
			,cast("trip".del as date) as 'Del'
			,cast("trip".al as date) as 'Al'
--			,"trip".DuracionViaje
			,"trip".kms_camion_lleno as 'Kms Cargado'
			,"trip".kms_camion_vacio as 'Kms Vacio'
			,Round ((("trip".kms_camion_lleno + "trip".kms_camion_vacio) /("qty".COMBUSTIBLE)), 2) as 'Rendimiento'
			,"liquid".dias as 'Duracion viaje'     -- Agregado 06/07/2020
--			,"trip".*
	from 
			sistemas.dbo.rentabilidad_view_costos_liquidations as "cost"
		inner join 
			sistemas.dbo.rentabilidad_view_qty_liquidations as "qty"
		on 
			"cost".liquidacion = "qty".liquidacion and "cost".unidad = "qty".unidad
		left join
			sistemas.dbo.rentabilidad_view_viajes_liquidations as "trip"   -- agregar campo rendimiento_reseteo
		on
			"cost".liquidacion = "trip".no_liquidacion and "cost".unidad = "trip".id_unidad
		left join 
			gstdb.dbo.trafico_liquidacion as "liquid"
		on 
			"liquid".id_area = "cost".id_area and "liquid".no_liquidacion = "cost".liquidacion
GO


