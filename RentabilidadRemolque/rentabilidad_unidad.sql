--================================================================================ --
-- Concept proof
--================================================================================ --
Select
   case (tl.id_area) when 1 then 'Orizaba' end 'UdN'
  ,tl.no_liquidacion as 'liquidacion'
  ,tl.fecha_liquidacion
  ,TL.id_unidad as 'Unidad'
  ,trl.no_viaje,tv.kms_real
  ,tl.dias_viaje
  ,trl.id_concepto as '#Concepto', Trl.desc_concepto as'concepto'
  ,(trl.monto_concepto  trl.iva_concepto ) 'monto'
  ,trl.cantidad
  ,Tl.monto_sueldo as 'Sueldo'
from
  gstdb.dbo.trafico_liquidacion tl
inner join
  gstdb.dbo.trafico_renglon_liquidacion trl
  on
    tl.id_area  = trl.id_area
  and
    tl.no_liquidacion = trl.no_liquidacion
inner join gstdb.dbo.trafico_viaje  tv on tv.id_area  =tl.id_area  and trl.no_viaje =tv.no_viaje
where tl.status_liq ='A' and tl.id_area =1
and tl.fecha_liquidacion  >='2019-05-23 00:00:00'
--and trl.id_concepto  in (1,2,301,302,303,304,305,306)
--and tl.no_liquidacion=24509
--Todo lo liquidado del mes
