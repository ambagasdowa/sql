-- select * from gstdb.dbo.v_ingresos_2016_all

use gstdb
exec sp_helptext v_ingresos_2016_all


-- ================================================================================================= --
-- Original Query
-- ================================================================================================= --

create
	view v_ingresos_2016_all as select
		top (100) percent year(a.fecha_guia) as Año,
		case
			month(a.fecha_guia)
			when 01 then 'ENERO'
			when 02 then 'FEBRERO'
			when 03 then 'MARZO'
			when 04 then 'ABRIL'
			when 05 then 'MAYO'
			when 06 then 'JUNIO'
			when 07 then 'JULIO'
			when 08 then 'AGOSTO'
			when 09 then 'SEPTIEMBRE'
			when 10 then 'OCTUBRE'
			when 11 then 'NOVIEMBRE'
			else 'DICIEMBRE'
		end as Mes,
		case
			e.id_configuracionviaje
			when 1 then 'Sencillo'
			when 2 then 'Sencillo'
			when '3' then 'Full'
		end as SencFull,
		b.id_fraccion as [Tipo Carga],
		convert(varchar(10),
		a.fecha_guia,
		103) as fecha_guia,
		day(a.fecha_guia) as Día_del_mes,
		case
			a.id_area
			when 1 then 'Orizaba'
			when 2 then 'Guadalajara'
			when 3 then 'Ramos Arizpe'
			when 4 then 'Mexicali'
			when 5 then 'Hermosillo'
			when 6 then 'La Paz'
		end as Area,
		case
			b.id_tipo_operacion
			when 1 then 'Orizaba'
			when 2 then 'Orizaba'
			when 3 then 'Ramos Arizpe'
			when 4 then 'Escobedo'
			when 5 then 'San Luis Potosí'
			when 6 then 'Altamira'
			when 7 then 'Chihuahua'
			when 8 then 'Cd. Juárez'
			when 9 then 'Guadalajara'
			when 10 then 'Guadalajara'
			when 11 then 'Guadalajara'
			when 12 then 'La Paz'
			when 13 then 'Hermosillo'
			when 14 then 'Culiacán'
			when 15 then 'Hermosillo'
			when 16 then 'Mexicali'
			when 17 then 'Guadalajara'
			when 18 then 'Orizaba'
			when 19 then 'Orizaba'
			when 20 then 'Orizaba'
			when 21 then 'Orizaba'
			when 22 then 'Orizaba'
			when 23 then 'Orizaba'
			when 24 then 'Orizaba'
			when 25 then 'Guadalajara'
			when 26 then 'Orizaba'
			when 27 then 'Orizaba'
			when 28 then 'Orizaba'
			when 29 then 'Guadalajara'
			when 30 then 'Guadalajara'
			when 31 then 'Orizaba'
			else 'Sin Asignar'
		end as Flota,
		a.num_guia as Talon,
		b.no_viaje as Viaje,
		b.id_fraccion,
		f.desc_ruta as Ruta,
		b.id_origen,
		a.flete,
		a.otros as Adicional,
		a.subtotal,
		a.iva_guia as IVA,
		a.monto_retencion as Retencion,
		a.num_guia_asignado as Factura,
		c.nombre,
		g.id_unidad,
		b.id_convenio as Convenio,
		b.no_remision,
		b.convenido_tonelada,
		b.cobro_viaje_kms,
		case
			b.status_guia
			when 'A' then 'Pendiente'
			when 'B' then 'Cancelada'
			when 'C' then 'Confirmada'
			when 'R' then 'Regreso'
			when 'T' then 'Tránsito'
			else 'Sin Estatus'
		end as Estatus,
		case
			b.prestamo
			when 'N' then 'Aceptada'
			when 'P' then 'Edición'
		end as Expr1
	from
		dbo.trafico_guia as a
	inner join dbo.trafico_guia as b on
		a.id_area = b.id_area
		and a.num_guia = b.num_guia
	inner join dbo.trafico_cliente as c on
		b.id_cliente = c.id_cliente
	inner join dbo.mtto_unidades as g on
		a.id_unidad = g.id_unidad
	inner join dbo.trafico_plaza as d on
		b.id_destino = d.id_plaza
	inner join dbo.trafico_viaje as e on
		b.id_area = e.id_area
		and b.no_viaje = e.no_viaje
	inner join dbo.trafico_ruta as f on
		e.id_ruta = f.id_ruta
	where
		(b.status_guia <> 'B')
		and (a.fecha_guia > '2013-12-31 23:59.000')
		and a.id_area between 1 and 6
	order by
		b.status_guia


		
-- ================================================================================================= --
-- Original-Rebuild Query
-- ================================================================================================= --

select * from gstdb.dbo.facturacion
		

use sistemas
create
	view facturacion_view_ingresos_all_udns 
	as 
	select
		top (100) percent year(a.fecha_guia) as Año,
		case
			month(a.fecha_guia)
			when 01 then 'ENERO'
			when 02 then 'FEBRERO'
			when 03 then 'MARZO'
			when 04 then 'ABRIL'
			when 05 then 'MAYO'
			when 06 then 'JUNIO'
			when 07 then 'JULIO'
			when 08 then 'AGOSTO'
			when 09 then 'SEPTIEMBRE'
			when 10 then 'OCTUBRE'
			when 11 then 'NOVIEMBRE'
			else 'DICIEMBRE'
		end as Mes,
		case
			e.id_configuracionviaje
			when 1 then 'Sencillo'
			when 2 then 'Sencillo'
			when '3' then 'Full'
		end as SencFull,
--		b.id_fraccion as [Tipo Carga],
		ltrim(rtrim(replace("operacion".tipo_operacion ,'FLETES' , ''))) as [TipoOperacion],
		convert(varchar(10),a.fecha_guia,103) as fecha_guia,
		day(a.fecha_guia) as Día_del_mes,
--		case
--			a.id_area
--			when 1 then 'Orizaba'
--			when 2 then 'Guadalajara'
--			when 3 then 'Ramos Arizpe'
--			when 4 then 'Mexicali'
--			when 5 then 'Hermosillo'
--			when 6 then 'La Paz'
--		end as Area,
		"area".ciudad as 'area',
--		case
--			b.id_tipo_operacion
--			when 1 then 'Orizaba'
--			when 2 then 'Orizaba'
--			when 3 then 'Ramos Arizpe'
--			when 4 then 'Escobedo'
--			when 5 then 'San Luis Potosí'
--			when 6 then 'Altamira'
--			when 7 then 'Chihuahua'
--			when 8 then 'Cd. Juárez'
--			when 9 then 'Guadalajara'
--			when 10 then 'Guadalajara'
--			when 11 then 'Guadalajara'
--			when 12 then 'La Paz'
--			when 13 then 'Hermosillo'
--			when 14 then 'Culiacán'
--			when 15 then 'Hermosillo'
--			when 16 then 'Mexicali'
--			when 17 then 'Guadalajara'
--			when 18 then 'Orizaba'
--			when 19 then 'Orizaba'
--			when 20 then 'Orizaba'
--			when 21 then 'Orizaba'
--			when 22 then 'Orizaba'
--			when 23 then 'Orizaba'
--			when 24 then 'Orizaba'
--			when 25 then 'Guadalajara'
--			when 26 then 'Orizaba'
--			when 27 then 'Orizaba'
--			when 28 then 'Orizaba'
--			when 29 then 'Guadalajara'
--			when 30 then 'Guadalajara'
--			when 31 then 'Orizaba'
--			else 'Sin Asignar'
--		end as Flota,
--		'' as 'Flota', --compability issues
		"zona".Plaza,
		"zona".Zona, -- sust flota
		a.num_guia as Talon,
		b.no_viaje as Viaje,
--		b.id_fraccion, --ompability issues
		f.desc_ruta as Ruta,
--		b.id_origen,
		a.flete,
		a.otros as Adicional,
		a.subtotal,
		a.iva_guia as IVA,
		a.monto_retencion as Retencion,
		a.num_guia_asignado as Factura,
		c.nombre,
		g.id_unidad,
		b.id_convenio as Convenio,
		b.no_remision,
		b.convenido_tonelada,
		b.cobro_viaje_kms,
		case
			b.status_guia
			when 'A' then 'Pendiente'
			when 'B' then 'Cancelada'
			when 'C' then 'Confirmada'
			when 'R' then 'Regreso'
			when 'T' then 'Tránsito'
			else 'Sin Estatus'
		end as Estatus,
		case
			b.prestamo
			when 'N' then 'Aceptada'
			when 'P' then 'Edición'
		end as Expr1
	from
		gstdb.dbo.trafico_guia as a
	inner join gstdb.dbo.trafico_guia as b on
		a.id_area = b.id_area
		and a.num_guia = b.num_guia
	inner join gstdb.dbo.trafico_cliente as c on
		b.id_cliente = c.id_cliente
	inner join gstdb.dbo.mtto_unidades as g on
		a.id_unidad = g.id_unidad
	inner join gstdb.dbo.trafico_plaza as d on
		b.id_destino = d.id_plaza
	inner join gstdb.dbo.trafico_viaje as e on
		b.id_area = e.id_area
		and b.no_viaje = e.no_viaje
	inner join gstdb.dbo.trafico_ruta as f on
		e.id_ruta = f.id_ruta
	inner join gstdb.dbo.desp_tipooperacion as "operacion"
		on "operacion".id_tipo_operacion = "a".id_tipo_operacion
	inner join gstdb.dbo.general_area as "area"
		on "area".id_area = "a".id_area
	left join  -- Zonas reemplaza Flotas [Agrupador de Origen] 
			gstdb.dbo.gst_sub_tipo_operacion as "zona"
		on 
			"a".id_remitente = "zona".id_cliente
	where
		(b.status_guia <> 'B')
--		and (a.fecha_guia > '2013-12-31 23:59.000') -- miedos 
		and year(a.fecha_guia) > '2013'
		and a.id_area between 1 and 6
		and a.tipo_doc = 2  -- 1 = factura 2 = CP
	order by
		b.status_guia
		
		
		
		
		
		