-- Get how many triggers have a table 
use integraapp

use sistemas

use gstdb

SELECT 
     "obj".name AS trigger_name 
    ,USER_NAME("obj".uid) AS trigger_owner 
    ,s.name AS table_schema 
    ,OBJECT_NAME("obj".parent_obj) AS table_name 
    ,OBJECTPROPERTY( "obj".id, 'ExecIsUpdateTrigger') AS isupdate
    ,OBJECTPROPERTY( "obj".id, 'ExecIsDeleteTrigger') AS isdelete
    ,OBJECTPROPERTY( "obj".id, 'ExecIsInsertTrigger') AS isinsert
    ,OBJECTPROPERTY( "obj".id, 'ExecIsAfterTrigger') AS isafter
    ,OBJECTPROPERTY( "obj".id, 'ExecIsInsteadOfTrigger') AS isinsteadof
    ,OBJECTPROPERTY("obj".id, 'ExecIsTriggerDisabled') AS [disabled]
    ,"comment".[text] AS 'SqlContent'
FROM sysobjects as "obj"
INNER JOIN sysusers
    ON "obj".uid = sysusers.uid
INNER JOIN sys.syscomments as "comment"
	ON "obj".id = "comment".id
INNER JOIN sys.tables t
    ON "obj".parent_obj = t.object_id
INNER JOIN sys.schemas s
    ON t.schema_id = s.schema_id
WHERE "obj".[type] = 'TR'
	and OBJECT_NAME("obj".parent_obj) like 'mtto_unida%'


use integraapp
use sistemas
use sltestapp

SELECT
	name,"type"
	,case "type"
		when	'P' 	then 	'Stored Procedures'
		when	'FN' 	then	'Scalar Functions'
		when	'IF'	then 	'Inline Table-Value Functions'
		when	'TF'	then	'Table-Value Functions'
		when	'TR'	then 	'Trigger'
		when	'U'		then	'Base Table'
		when	'V'		then	'View'
	end as 'Type'
FROM
	dbo.sysobjects
WHERE
	type IN
		(
		'P', 	-- stored procedures
		'FN', 	-- scalar functions
		'IF', 	-- inline table-valued functions
		'TF', 	-- table-valued functions
		'TR',   	-- trigger
		'U',  	-- Base table
		'V'		-- View
		)
and "name" like '%XGetDueDate%'
ORDER BY  name
	

use bonampakdb
-- With this query you can find all Trigger in all tables and all views
;WITH
        TableTrigger
        AS
        (
            Select 
                Object_Kind = 'Table',
                Sys.Tables.Name As TableOrView_Name , 
                Sys.Tables.Object_Id As Table_Object_Id ,
                Sys.Triggers.Name As Trigger_Name, 
                Sys.Triggers.Object_Id As Trigger_Object_Id 
            From Sys.Tables 
            INNER Join Sys.Triggers On ( Sys.Triggers.Parent_id = Sys.Tables.Object_Id )
            Where ( Sys.Tables.Is_MS_Shipped = 0 )
        ),
        ViewTrigger
        AS
        (
            Select 
                Object_Kind = 'View',
                Sys.Views.Name As TableOrView_Name , 
                Sys.Views.Object_Id As TableOrView_Object_Id ,
                Sys.Triggers.Name As Trigger_Name, 
                Sys.Triggers.Object_Id As Trigger_Object_Id 
            From Sys.Views 
            INNER Join Sys.Triggers On ( Sys.Triggers.Parent_id = Sys.Views.Object_Id )
            Where ( Sys.Views.Is_MS_Shipped = 0 )
        ),
        AllObject
        AS
        (
            SELECT * FROM TableTrigger

            Union ALL

            SELECT * FROM ViewTrigger
        )


    Select 
        * 
    From AllObject
    Order By Object_Kind, Table_Object_Id 


    


-- find triggers on table:
use gstdb
select so.name, text
from sysobjects so, syscomments sc
where type = 'TR'
and so.id = sc.id
and text like '%personal_personal%'

-- and you can find store procedure which has reference of table:

SELECT Name
FROM sys.procedures
WHERE OBJECT_DEFINITION(OBJECT_ID) LIKE '%zpoling%'


-- 

select so.name, text
from sysobjects so, syscomments sc
where type = 'TR'
and so.id = sc.id
and text like '%zpoling%'

use bonampakdb
SELECT Name
FROM sys.procedures
WHERE OBJECT_DEFINITION(OBJECT_ID) LIKE '%trafico_guia%'    

use bonampakdb
exec sp_helptext tr_solomon_factycob_cancel
exec sp_helptext sp_solomon_factycob_cancel

exec sp_helptext sp_solomon_Ins_ZPoling

exec sp_helptext sp_solomon_factycob

exec sp_helptext sp_solomon_factycob_r