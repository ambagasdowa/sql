-- ADD a network resource in mssql

-- run in mssql 

-- allow changes to advanced options 
EXEC sp_configure 'show advanced options', 1
GO
-- Update currently configured values for advanced options.
RECONFIGURE
GO
-- To enable xp_cmdshell
EXEC sp_configure 'xp_cmdshell', 1
GO
-- Update currently configured values for advanced options.
RECONFIGURE
GO

EXEC xp_cmdshell 'NET USE Y: \\integrabox\ambagasdowa effeta /USER:Uruk\ambagasdowa'

-- qemu-img resize vmdisk.img +10G

EXEC xp_cmdshell 'NET USE X: \\integrabox\Backup effeta /USER:Uruk\ambagasdowa'

EXEC xp_cmdshell 'NET USE Z: \\192.168.20.235\Backup Database_Gst20.18 /USER:integrabd\Administrator'

--Afterwards drive Z: will be visible in Server Managment studio, or just

RESTORE DATABASE DataBaseNameHere FROM DISK = 'Z:\BackNameHere.BAK'

RESTORE DATABASE DataBaseNameHere FROM DISK = 'Z:\gstdb_backup_2019_01_23_030000_6708456.bak'




RESTORE DATABASE integraapp FROM DISK = 'Z:\integraapp_backup_2019_01_25_030001_3892292.bak'


CREATE PROCEDURE dbo.my_config
AS
    EXEC sp_configure 'show advanced option', 1;
    RECONFIGURE WITH OVERRIDE;
    EXEC sp_configure 'xp_cmdshell', 1;
    EXEC sp_configure 'ad hoc distributed queries', 1;
    RECONFIGURE WITH OVERRIDE;
GO
CREATE PROCEDURE dbo.nz_test1
AS
    SELECT 1 AS Value;
GO
CREATE PROCEDURE dbo.test_nz_tb3
AS
    EXEC dbo.my_config;
    CREATE TABLE #t (a varchar(10));
    INSERT INTO #t
        EXEC dbo.nz_test1;