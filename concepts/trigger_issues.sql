--use pruebastbk
--SELECT
--		name,type
--		,case type
--			when	'P' 	then 	'Stored Procedures'
--			when	'FN' 	then	'Scalar Functions'
--			when	'IF'	then 	'Inline Table-Value Functions'
--			when	'TF'	then	'Table-Value Functions'
--			when	'TR'	then 	'Trigger'
--			when	'U'		then	'Base Table'
--			when	'V'		then	'View'
--		end as 'Type'
--FROM
--	dbo.sysobjects
--WHERE
--	type IN
--		(
--		'P', -- stored procedures
--		'FN', -- scalar functions
--		'IF', -- inline table-valued functions
--		'TF', -- table-valued functions
--		'TR',
--		'U',
--		'V'
--		)
--ORDER BY type, name

--exec sp_helptext tbk_calcula_sueldo_por_base2

--pruebastbk , trafico_guia_liquidacion , tbk_calcula_sueldo_por_base2
/*
use bonampakdb
SELECT name, is_disabled FROM sys.triggers
SELECT OBJECT_NAME(parent_id) [table_name],[name] [trigger_name],is_disabled
FROM sys.triggers
where OBJECT_NAME(parent_id) in ('trafico_guia_liquidacion','trafico_liquidacion')
*/

-- DISABLE TRIGGER tbk_calcula_sueldo_por_base ON trafico_liquidacion;
-- DISABLE TRIGGER tbk_calcula_sueldo_por_base2 ON trafico_guia_liquidacion;

--disable trigers 
--Para deshabilitar un desencadenador DDL con ámbito de servidor (ON ALL SERVER) o un desencadenador de inicio de sesión, el usuario debe tener el permiso
--CONTROL SERVER en el servidor. Para deshabilitar un desencadenador DDL con ámbito en la base de datos (ON DATABASE) el usuario debe contar, como mínimo, con
--un permiso ALTER ANY DATABASE DDL TRIGGER en la base de datos actual.
--
--Ejemplos
-- 
--━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
--
--A. Deshabilitar un desencadenador DML en una tabla
--
--En el ejemplo siguiente se deshabilita el desencadenador uAddress, que se creó en la tabla Address.

USE AdventureWorks2012;
GO
DISABLE TRIGGER Person.uAddress ON Person.Address;
go


--B. Deshabilitar un desencadenador DDL
--
--En el ejemplo siguiente se crea un desencadenador DDL safety, con ámbito en la base de datos, y después se deshabilita.


IF EXISTS (SELECT * FROM sys.triggers
    WHERE parent_class = 0 AND name = 'safety')
DROP TRIGGER safety ON DATABASE;
GO
CREATE TRIGGER safety
ON DATABASE
FOR DROP_TABLE, ALTER_TABLE
AS
   PRINT 'You must disable Trigger "safety" to drop or alter tables!'
   ROLLBACK;
GO
DISABLE TRIGGER safety ON DATABASE;
GO

--C. Deshabilitar todos los desencadenadores que se definieron con el mismo ámbito
--
--En el ejemplo siguiente se deshabilitan todos los desencadenadores DDL creados en el ámbito de servidor.

USE AdventureWorks2012;
GO
DISABLE Trigger ALL ON ALL SERVER;
go


use sistemas

--with "q" as (
SELECT
		"obj".name
--		,"obj"."type"
--		,case "obj"."type"
--			when	'P' 	then 	'Stored Procedures'
--			when	'FN' 	then	'Scalar Functions'
--			when	'IF'	then 	'Inline Table-Value Functions'
--			when	'TF'	then	'Table-Value Functions'
--			when	'TR'	then 	'Trigger'
--			when	'U'		then	'Base Table'
--			when	'V'		then	'View'
--		end as 'Type'
FROM
	dbo.sysobjects as "obj"
WHERE
	type IN
		(
		'P', 	-- stored procedures
		'FN', 	-- scalar functions
		'IF', 	-- inline table-valued functions
		'TF', 	-- table-valued functions
		'TR',   -- trigger
		'U',  	-- Base table
		'V'		-- View
		)
	and "obj"."name" like '%costos%'
	ORDER BY "obj"."type", "obj"."name"
--) 
--select "name" from "q" where "name" like '%costos%'



-- stackoverflow https://stackoverflow.com/questions/4305691/need-to-list-all-triggers-in-sql-server-database-with-table-name-and-tables-sch
-- list trigger associated by table 
select name,database_id,create_date from sys.databases
-- check triggers
-- 
	 TR_MTTO_UNIDADES_ADICIONALES
	 t_mtto_unidades01
	gst_actualiza_area_depto
	gst_replica_status_disponibilidad
	gst_valida_estatus_alta_unidad




use integraapp;

use gstdb

use master

use sistemas

SELECT 
     sysobjects.name AS trigger_name
    ,USER_NAME(sysobjects.uid) AS trigger_owner 
    ,s.name AS table_schema 
    ,OBJECT_NAME(parent_obj) AS table_name 
    ,OBJECTPROPERTY( id, 'ExecIsUpdateTrigger') AS isupdate 
    ,OBJECTPROPERTY( id, 'ExecIsDeleteTrigger') AS isdelete 
    ,OBJECTPROPERTY( id, 'ExecIsInsertTrigger') AS isinsert 
    ,OBJECTPROPERTY( id, 'ExecIsAfterTrigger') AS isafter 
    ,OBJECTPROPERTY( id, 'ExecIsInsteadOfTrigger') AS isinsteadof 
    ,OBJECTPROPERTY(id, 'ExecIsTriggerDisabled') AS [disabled]
    ,sysobjects.type
    ,case sysobjects."type"
			when	'P' 	then 	'Stored Procedures'
			when	'FN' 	then	'Scalar Functions'
			when	'IF'	then 	'Inline Table-Value Functions'
			when	'TF'	then	'Table-Value Functions'
			when	'TR'	then 	'Trigger'
			when	'U'		then	'Base Table'
			when	'V'		then	'View'
		end as 'Type'
FROM sysobjects 

INNER JOIN sysusers 
    ON sysobjects.uid = sysusers.uid 

INNER JOIN sys.tables t 
    ON sysobjects.parent_obj = t.object_id 

INNER JOIN sys.schemas s 
    ON t.schema_id = s.schema_id 
WHERE 
	sysobjects.type IN
		(
		'P', 	-- stored procedures
		'FN', 	-- scalar functions
		'IF', 	-- inline table-valued functions
		'TF', 	-- table-valued functions
		'TR',   -- trigger
		'U',  	-- Base table
		'V'		-- View
		)
--and 
--	sysobjects.name like 'disponibilidad%'
and 
	OBJECT_NAME(parent_obj) like 'mtto_uni%'
	
--	sysobjects.type = 'TR' 
--and 
	OBJECT_NAME(parent_obj) like 'disponibilidad%'

exec sp_helptext gst_replica_status_disponibilidad
`

disable trigger gst_replica_status_disponibilidad on mtto_unidades

use macuspanadb
DISABLE TRIGGER dbo.permitir ON dbo.trafico_combustible;
ENABLE TRIGGER dbo.permitir ON dbo.trafico_combustible;



SELECT DISTINCT
       o.name AS Object_Name,
       o.type_desc
  FROM sys.sql_modules m
       INNER JOIN
       sys.objects o
         ON m.object_id = o.object_id
 --WHERE  '.' + m.definition + '.' LIKE '%[^a-z]employeeid[^a-z]%'
 order by type_desc, object_name
 
 
 
 
