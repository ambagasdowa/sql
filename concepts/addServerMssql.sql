--Adding a server for use with openquery

exec sp_dropserver 'LARSA\local'

--create database sistemas
--use master

-- exec sp_addserver  'Server',local

--name      |network_name                   |status                           |id   |collation_name |connect_timeout |query_timeout |
--------|-------------------------------|---------------------------------|-----|---------------|----------------|--------------|
--INTEGRABD |INTEGRABD                      |rpc,rpc out,use remote collation |0    |               |0               |0             |
--local     |                               |data access,use remote collation |1    |               |0               |0             |



EXEC sp_helplinkedsrvlogin;

 
exec sp_helpserver

exec sp_addlinkedserver
@server = 'local',
@srvproduct=N'',
@provider=N'SQLNCLI'



-- watch the role for users
	exec sp_helplogins 'zam' 		-- check login user has a login ?	
	exec sp_helpuser 'zam' 		-- check the user in db
	exec sp_helprole 'zam' 	-- check roles



EXEC sp_addlinkedserver    
   @server=N'OtherSQLServer',  				     -- Name of the server [any char]
   @srvproduct=N'',  
   @provider=N'SQLNCLI',  						
   @datasrc=N'WSERVER2012\SQLEXPRESS',			 -- important server\instatnce 
   
   @provstr = N'SERVER=WSERVER2012\SQLEXPRESS',  --optional
   @catalog  = N'AdventureWorks2014';			 --optional
   
--This example 
EXEC sp_addlinkedsrvlogin
   @rmtsrvname = N'192.168.20.235',
   @useself ='false', 
   @locallogin = N'contraloria',
   @rmtuser = N'zam',
   @rmtpassword = N'lis';
   
EXEC sp_addlinkedsrvlogin
   @rmtsrvname = N'192.168.20.240',
   @useself ='false', 
   @locallogin = N'integra',
   @rmtuser = N'enuma',
   @rmtpassword = N'@Elish#';
