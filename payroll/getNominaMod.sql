use NOM2001;

if OBJECT_ID ('getNominaMod','V') is not null drop view getNominaMod;

create
	view getNominaMod with encryption as
	select
		dbo.getNamePuesto("trabajador".cveare,"trabajador".cvecia) as Puesto,
		"trabajador".cvecia as ClaveEmpresa,
		"trabajador".cvetra as ClaveTrabajador,
		"trabajador".apepat + ' ' + "trabajador".apemat + ' ' + "trabajador".nombre as Nombre,
		"trabajador".cvetno as tipoNomina,
		"trabajador".numrfc as Rfc,
		"trabajador".numims as Imss,
		"trabajador".status as Status,
		"trabajador".fecalt as fechaAlta,
		"trabajador".fecbaj as fechaBaja,
		"trabajador".curp
	from

		dbo.nomtrab as trabajador 
