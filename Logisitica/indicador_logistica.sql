--===================  Logistica ===============================--


use sistemas
exec sp_helptext logistica_view_asignacion_gst_pedidos


 
 
	 
select   --v_status_viaje,v_status_desp,
		 no_viaje,id_area,num_pedido,id_seguimiento,id_asignacion
		,num_guia,status_guia,prestamo,tipo_doc,f_despachado,fecha_guia,fecha_confirmacion,fecha_cancelacion
		,FlagIsDisminution,FlagIsProvision,FlagIsNextMonth
		,*
from sistemas.dbo.projections_view_full_gst_core_indicators where no_viaje in (113487,115258)
 	
	 

select 
--		 count("asig".no_viaje)
	 	 "guia".num_guia
		,"asig".id_area
		,"asig".no_viaje
	    ,"asig".id_asignacion
    	,"pedido".num_pedido
        ,"guia".status_guia
        ,"asig".id_seguimiento
--        ,"guia".prestamo
from 
		gstdb.dbo.trafico_viaje as "viaje"
left join 
		gstdb.dbo.trafico_guia as "guia" 
	on 
		"viaje".no_viaje = "guia".no_viaje and "viaje".id_area = "guia".id_area 
inner join 
		gstdb.dbo.desp_asignacion as "asig"
on 
		"viaje".no_viaje = "asig".no_viaje and "viaje".id_area = "asig".id_area
left join 
		gstdb.dbo.desp_pedido as "pedido"
	on
		"guia".no_guia = "pedido".no_guia and "guia".id_area = "pedido".id_area
where 
		"viaje".no_viaje <> 0 and "viaje".id_area <> 0 
	and
		"viaje".no_viaje in (113487,115258)
--	and 
--		"pedido".num_pedido is null
--	and 
--		"guia".status_guia = 'C'
	and 
		"guia".tipo_doc = 2
		

use sistemas
IF OBJECT_ID ('logistica_view_clients', 'V') IS NOT NULL		
    DROP VIEW logistica_view_clients
    
create view logistica_view_clients
with encryption
as 		
		select 
				 id_cliente
				,nombre 
		from 
				gstdb.dbo.trafico_cliente 
		where id_cliente > 0
	
	
	select * from sistemas.dbo.logistica_view_clients
	
-- ============================================================================================================ --
-- =========================================== The Indicator ================================================== --
-- ============================================================================================================ --
-- Omit id_area = 0  and no_viaje = 0
	
--select * from sistemas.dbo.projections_view_full_gst_core_indicators as "logistic"
	
--select * from gstdb.dbo.trafico_guia where num_guia in ('OH-22920','OH-22919','OH-22946')


-- ================================== TEST =======================================-- 

	select * from sistemas.dbo.logistica_view_gst_datesheets where no_viaje in (222735,33974) --and id_area = 8
	
	select * from sistemas.dbo.logistica_view_gst_datesheets where no_viaje = 223618
	
	select no_guia,* from sistemas.dbo.projections_view_full_gst_core_indicators where no_viaje = 223618
	
	select * from gstdb.dbo.trafico_viaje where no_viaje = 223618
	
	select * from gstdb.dbo.trafico_guia where no_viaje = 223618


	select * from gstdb.dbo.trafico_reembarcar where no_guia = 226091


	from 
			gstdb.dbo.desp_bitacorastatusviaje as "bitacora"
	left join -- select * from
			gstdb.dbo.desp_statusviaje as "status_viaje"
		on 
			"status_viaje".id_statusviaje = "bitacora".id_statusviaje
	where "bitacora".no_viaje in (222735,33974)-- and "bitacora".id_area = 8


	select datediff('2020-03-12 13:00:00','2020-03-12')



-- == TEST == --

--fix hour definition 
-- check duplicated trip with CP 
-- check madrilenia   ganso
-- 
-- DATEDIFF ( datepart , startdate , enddate )
--							cita					llegadadescar
select datediff(minute,'2020-03-10 08:00:00','2020-03-12 13:13:00')
--							descar					cira
select ( datediff(minute,'2020-03-12 13:13:00','2020-03-10 08:00:00') / 60 ) / 24 / 60

-- ================================== TEST =======================================--


--====================== Definitions ================================ --

,"pedido".f_arribo_prog as [CitaProgramada]
,datediff(hour,"pedido".f_carini_prog,"pedido".f_desfin_prog) as [TiempoProgramado] --- Tiempo Total Del Viaje Segun el Cliente
	 	 			
--====================== Definitions ================================ --

use sistemas
IF OBJECT_ID ('logistica_view_gst_datesheets', 'V') IS NOT NULL		
    DROP VIEW logistica_view_gst_datesheets
    
alter view logistica_view_gst_datesheets
with encryption
as 	
		select   --v_status_viaje,v_status_desp,
 			     row_number() over(order by "indl".no_guia) as 'id'
				,(
					-- In this case can omit all cp in status B
					select
							', ' + "guiax".num_guia
					from 
							gstdb.dbo.trafico_guia as "guiax"
					where 
							"guiax".no_viaje = "indl".no_viaje and "guiax".id_area = "indl".id_area 
						and -- This Check for omit  cp in 'Baja'
							"guiax".tipo_doc = 2 and "guiax".status_guia <> 'B'
					for xml path('')
				 ) as 'Guias'
				,"indl".no_viaje							-- [2] RowData
			 	,"indl".num_guia
			 	,"indl".id_area
			 	,"indl".IsEmptyTrip							-- Parameter Cargado && Vacio [TipoViaje] 
			 	,' ' as 'InReembarco'
			 	,' ' as 'TipoViajeReembarco'
			 	,"indl".no_liquidacion
			 	,"indl".no_guia as 'viaje_no_guia'
--			 	,"reembarco".no_guia as 'reembarco_no_guia'
--			 	,"reembarco".viaje1
--			 	,"reembarco".viaje2
--			 	,"reembarco".kms_ruta1
--		 		,"reembarco".kms_ruta2
			 	,case 
			 		when "indl".IsEmptyTrip = 0 
			 			then 'Cargado' 
			 		else 'Vacio' 
			 	 end as 'TipoViaje'							-- Parameter Cargado && Vacio [TipoViaje]
			 	,"indl".area								-- Parameter [UnidadNegocio] 
			 	,"indl".tipo_de_operacion					-- [1] RowData
			 	,"indl".[f_despachado]						-- Parameter in the indicator date ini and date end [FechaInicio && FechaFin]
			 	,"indl".[mes-despacho]						-- Optiona;l
			 	,"indl".f_despachado_m  					-- periodo for despachado filter 
			 	,"indl".projections_rp_definition  			-- Parameter for filter Granel && Terceros set: [Tipo de Operacion]
			 	,"indl".fraccion
			 	,"indl".fraction
			 	,"indl".[peso-despachado]
			    ,"indl".subtotal
		    	,coalesce("indl".personalnombre,"indl".operador) as 'Operador' -- [3] RowData Operador
		    	,"indl".id_cliente								-- [4] RowData
		    	
		    	,"indl".cliente								-- [4] RowData ClientePaga
		    	,"indl".remitente							-- [5] RowData Remitente
		    	,"indl".destinatario						-- [6] RowData Destinatario
		    	
		    	,"indl".id_unidad							-- [7] RowData
		    	,"indl".fecha_guia							-- [8] RowData
		    	
		    	,case 
		    		when "indl".IsEmptyTrip = 0
		    			then "indl".gorigen
		    		else 
		    				 "indl".origen
		    	 end as 'Origen'
		    	,case 
		    		when "indl".IsEmptyTrip = 0
		    			then "indl".gdestino
		    		else 
		    				 "indl".destino
		    	 end as 'Destino'
		    	
		    	,isnull("indl".kms_viaje,0) as 'kms_viaje'							-- [11] RowData
		    	,isnull("indl".kms_real,0) as 'kms_real'							-- [11] RowData
--		    	NOTE DATEDIFF ( datepart , startdate , enddate ) 
		    	,datediff(minute,"timestamp".[LLEGADA A CARGA],"timestamp".[FIN DE VIAJE]) as 'TiempoTotal'						-- [12] RowData
		    	,datediff(minute,"timestamp".[LLEGADA A CARGA],"timestamp".[EN RUTA CARGADO]) as 'TiempoCarga'						-- [13] RowData
		    	,datediff(minute,"timestamp".[EN RUTA CARGADO],"timestamp".[LLEGADA A DESCARGA]) as 'TiempoTransito'						-- [14] RowData
		    	,datediff(minute,"timestamp".[LLEGADA A DESCARGA],"timestamp".[FIN DE VIAJE]) as 'TiempoDescarga'						-- [15] RowData
--		    	,( "indl".TiempoProgramado - datediff(minute,"timestamp".[LLEGADA A CARGA],"timestamp".[FIN DE VIAJE]) ) as 'TiempoCiclo'						-- [16] RowData
		    	,datediff(minute,"timestamp".[LLEGADA A CARGA],"timestamp".[FIN DE VIAJE]) as 'TiempoCiclo'						-- [16] RowData
--		    	,datediff(hour,"timestamp".[LLEGADA A DESCARGA],"indl".CitaProgramada) as 'LlegadaATiempo'						-- [17] RowData
--				,case 
--					when datediff(minute,"timestamp".[LLEGADA A DESCARGA],"indl".CitaProgramada) <= 0 
--						then 'SI'
--					else 
--						'NO'
--				 end as 'LlegadaATiempo' -- REview
				,case 
					when datediff(minute,"indl".CitaProgramada,"timestamp".[LLEGADA A DESCARGA]) <= 0 
						then 'SI'
					else 
						'NO'
				 end as 'LlegadaATiempo'
		    	,"indl".CitaProgramada
		    	,"indl".TiempoProgramado  --hours
		    	,"timestamp".[INICIO DE VIAJE] as 'InicioDeViaje'
				,"timestamp".[LLEGADA A CARGA] as 'LlegadaACarga'
				,"timestamp".[INICIO DE CARGA] as 'InicioDeCarga'
				,"timestamp".[FIN DE CARGA] as 'FinDeCarga'
				,"timestamp".[EN RUTA CARGADO] as 'EnRutaCargado'
				,"timestamp".[LLEGADA A DESCARGA] as 'LlegadaADescarga'
				,"timestamp".[INICIO DE DESCARGA] as 'InicioDeDescarga'
				,"timestamp".[FIN DE DESCARGA] as 'FinDeDescarga'
				,"timestamp".[FIN DE VIAJE] as 'FinDeViaje'
			 	,"indl".id_seguimiento
			 	,"indl".periodo_despachado
				,"indl".status_guia,"indl".prestamo,"indl".tipo_doc,"indl".trip_count
				,"indl".FlagIsDisminution,"indl".FlagIsProvision,"indl".FlagIsNextMonth
		from 
				sistemas.dbo.projections_view_full_gst_core_indicators as "indl"
		left join 
				sistemas.dbo.logistica_view_gst__trip_timestamps as "timestamp"
			on 
				"timestamp".id_area = "indl".id_area and "timestamp".no_viaje = "indl".no_viaje and "indl".trip_count = 1
-- === This is for ?			
--		left join 
--				gstdb.dbo.trafico_reembarcar as "reembarco"
--			on 
--				"reembarco".id_area = "indl".id_area and "reembarco".no_guia = "indl".no_guia 
-- === ??
--		left join 
--				gstdb.dbo.trafico_renglon_viaje as "renglon_viaje"
--			on 
--				"renglon_viaje".id_area = "indl".id_area and "renglon_viaje".no_viaje = "indl".no_viaje and "renglon_viaje".no_guia = "indl".no_guia
		where 
				(
					"indl".status_guia <> 'B' -- this include FlagIsDiminution equal 2  
				or 	
					"indl".status_guia is null  -- this pretends to be empty trip 
				)
--	================================================================================ --		
		and 
			"indl".no_viaje in (
223618
							    )
		and 
			"indl".IsEmptyTrip = 0
--		and 
--			"indl".FlagIsDisminution = 1 -- and "indl".no_viaje = 19727 -- and num_guia in ('OH-22920','OH-22919','OH-22946')
--			order by "indl".no_viaje

-- Viaje vivo por carta with pedido but without cp ,  as empty trip but appears
-- Ambagasdowa NOTE check with trafico_reembarcar :logic: when exists in reembarcar if the trip is empty [then es cargado] 
-- 

		select * from sistemas.dbo.logistica_view_gst_datesheets where IsEmptyTrip = 0
	
	
	
	 
	
	select 
	
	
select * from gstdb.dbo.trafico_reembarcar -- funtional in transbordo 
where no_guia = 71397


select * from sistemas.dbo.projections_fraccion_defs

select * from sistemas.dbo.projections_view_bussiness_units



	select 
			no_viaje,no_guia,id_area,f_arribo_prog,f_carini_prog,f_desfin_prog,* 
	from 
			gstdb.dbo.desp_pedido

		
select * from gstdb.dbo.trafico_renglon_viaje		
where no_guia = 71397


-- ================================================================================================================== --
--	get timestamps in trips 
-- ================================================================================================================== -- 

--		select * from sistemas.dbo.logistica_view_gst__trip_timestamps

use sistemas
IF OBJECT_ID ('logistica_view_gst__trip_timestamps', 'V') IS NOT NULL		
    DROP VIEW logistica_view_gst__trip_timestamps
    
create view logistica_view_gst__trip_timestamps
with encryption
as 
select 
		 "id_area"
		,"no_viaje"
		,[INICIO DE VIAJE]
		,[LLEGADA A CARGA]
		,[INICIO DE CARGA]
		,[FIN DE CARGA]
		,[EN RUTA CARGADO]
		,[LLEGADA A DESCARGA]
		,[INICIO DE DESCARGA]
		,[FIN DE DESCARGA]
		,[FIN DE VIAJE]
from 
(
	select
			 "bitacora".id_area
			,"bitacora".no_viaje
			,"status_viaje".nombre_status_viaje
			,"bitacora".f_ini_status
	from 
			gstdb.dbo.desp_bitacorastatusviaje as "bitacora"
	left join -- select * from
			gstdb.dbo.desp_statusviaje as "status_viaje"
		on 
			"status_viaje".id_statusviaje = "bitacora".id_statusviaje
--	where 
--			"bitacora".no_viaje in (106989) 
) as "SourceTable"
pivot
(
	max(f_ini_status)
		for nombre_status_viaje in 
									(
									 [INICIO DE VIAJE]
									,[LLEGADA A CARGA]
									,[INICIO DE CARGA]
									,[FIN DE CARGA]
									,[EN RUTA CARGADO]
									,[LLEGADA A DESCARGA]
									,[INICIO DE DESCARGA]
									,[FIN DE DESCARGA]
									,[FIN DE VIAJE]
									)
) as PivotTable

-- ================================================================================================================== --







use sistemas


SELECT compatibility_level  
FROM sys.databases WHERE name = 'sistemas'

ALTER DATABASE sistemas  
SET COMPATIBILITY_LEVEL = 90;  

--select * from gstdb.dbo.desp_statusviaje


-- =========================================== The Indicator ================================================== --


































