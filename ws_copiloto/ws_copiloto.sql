/*
 ==============================================================================================================
 Author         : Elephantom
 email			: 
 Create date    : Jun 2019
 Description    : 
 @license       : MIT License (http://www.opensource.org/licenses/mit-license.php)
 Database owner :  
 @status        : Stable
 @version		: 1.0.0
 ==============================================================================================================
*/

-- ============================================================================================================== --
-- Ws Copiloto 
-- ============================================================================================================== --

use sistemas

IF OBJECT_ID ('ws_georeference_view_post_services', 'V') IS NOT NULL
    DROP VIEW ws_georeference_view_post_services;
-- now build the view
create view ws_georeference_view_post_services
with encryption
as	

with "copiloto" as (
	select 
			 "ws".id
			,row_number() over(partition by "ws".alias,"ws".idMovil,"ws".latitud,"ws".longitud,"ws".fechahorautc,"ws".tipo order by "ws".id) as 'index'
			,"ws".alias
			,"ws".nombre
			,substring("ws".nombre,1,6) as 'id_unidad'
			,case "ws".tipo 
				when 21194 then 'Entrada a Geocerca de Hardware'
				when 211940 then 'Entrada a Geocerca de Software'
				when 21195 then 'Salida a Geocerca de Hardware'
				when 211950 then 'Salida a Geocerca de Hardware'
				when 21225 then 'Posicion'
			 end as 'TipoDescripcion'
			,"ws".idMovil
			,"ws".latitud
			,"ws".longitud
			,"ws".lugar
			,"ws".placas
			,"ws".fechahorautc
			,"ws".tipo
			,"ws".created
			,"ws".modified
			,"ws".status
			,"ws".ws_tbl_clkctrl_historico_unidades_id
	from 
		sistemas.dbo.ws_tbl_gst_copiloto_historico_unidades as "ws"
	inner join 
		gstdb.dbo.mtto_unidades as "unit"
		on 
			substring("ws".nombre,1,6) = "unit".id_unidad 
		and 
			"unit".tipo_unidad = 1
	where 
			"ws".tipo in (
--					  		 21194		-- Entrada a Geocerca de Hardware
							 211940		-- Entrada a Geocerca de Software
--							,21195  	-- Salida a Geocerca de Hardware
							,211950		-- Salida a Geocerca de software
--							,21225		-- Posicion
						)
		and 
			"ws".status = 1
)
select 
		 "copiloto".id
		,"copiloto".alias
		,"copiloto".nombre
		,"positions".base
		,"copiloto".id_unidad
		,"copiloto".TipoDescripcion
		,case 
			when
					"copiloto".latitud between "positions".min_latitud and "positions".max_latitud 
				and 
					"copiloto".longitud between "positions".min_longitud and "positions".max_longitud
			then 
				1
			else 
				0
		end as 'geocerca_in'
		,case 
			when
					"copiloto".latitud between "positions".min_latitud and "positions".max_latitud 
				and 
					"copiloto".longitud between "positions".min_longitud and "positions".max_longitud
			then 
					case "positions".tipo_geocerca
						when  
							101 -- Planta
						then 
							case  
								when "copiloto".tipo = 21194 or "copiloto".tipo = 211940
									then 21
								when "copiloto".tipo = 21195 or "copiloto".tipo = 211950
									then 22
								when  "copiloto".tipo = 21225
									then 25
							end
						when  
							2   -- Cliente
						then 
							case  
								when "copiloto".tipo = 21194 or "copiloto".tipo = 211940 
									then 23
								when "copiloto".tipo = 21195 or "copiloto".tipo = 211950 
									then 24
								when  "copiloto".tipo = 21225
									then 26
							end									
					end
--			else 
--				0
		 end as 'macro'
		,"copiloto".idMovil
		,"positions".tipo_geocerca
		,"copiloto".latitud
		,"copiloto".longitud
		,"positions".max_latitud
		,"positions".min_latitud
		,"positions".max_longitud
		,"positions".min_longitud
		,"copiloto".lugar
		,"copiloto".placas
		,"copiloto".fechahorautc
		,"copiloto".tipo
		,"copiloto".ws_tbl_clkctrl_historico_unidades_id
		,"copiloto".created
from 
		"copiloto"
cross join 
		sistemas.dbo.ws_georeference_view_positions_gst_indicators as "positions"
where 
		"copiloto"."index" = 1 
	and 
		"positions".tipo_geocerca in (2,101)
--	and 
--		"copiloto".id_unidad = 'TT3230'
--		"copiloto".id_unidad = 'RS3023'
group by 
		 "copiloto".id
		,"copiloto".alias
		,"copiloto".nombre
		,"positions".base
		,"copiloto".id_unidad
		,"copiloto".TipoDescripcion
		,"copiloto".idMovil
		,"positions".tipo_geocerca
		,"copiloto".latitud
		,"copiloto".longitud
		,"positions".max_latitud
		,"positions".min_latitud
		,"positions".max_longitud
		,"positions".min_longitud
		,"copiloto".lugar
		,"copiloto".placas
		,"copiloto".fechahorautc
		,"copiloto".tipo
		,"copiloto".ws_tbl_clkctrl_historico_unidades_id
		,"copiloto".created
order by 
		"copiloto".created desc	
	
--	use THE join 

		
		
		
-- ============================================================================================================== --
-- Ws Copiloto 
-- ============================================================================================================== --

	select * from gstdb.dbo.desp_geocercas where id = 3
		
use sistemas
-- select * from sistemas.dbo.ws_georeference_view_positions_gst_indicators where base like '%modelo%'   
IF OBJECT_ID ('ws_georeference_view_positions_gst_indicators', 'V') IS NOT NULL
    DROP VIEW ws_georeference_view_positions_gst_indicators;
-- now build the view
create view ws_georeference_view_positions_gst_indicators
with encryption
as	
with "pos_lat_max" as (
		select
			 "dg".id_geocerca
		 	,"dg".tipo_geocerca
			,case "dg".id_geocerca
				when 146
					then
						'CUAUTITLAN 1'
				when 147
					then
						'CUAUTITLAN 2' 
				when 150
					then
						'MACUSPANA'
				when 151
					then 'RAMOS ARIZPE'
				when 
					154
					then 
						'TULTITLAN'
				else
					ltrim(rtrim(replace("dg".nombre,'BASE' , '')))
			 end as 'base'
			,max("dp".latitud) as 'latitud'
		from 
			gstdb.dbo.desp_geocercas as "dg"
		left join
			gstdb.dbo.desp_puntos_geocercas as "dp"
		on 
			"dp".id_geocerca = "dg".id_geocerca
--		where 
--			"dg".id_geocerca in (148,149,150,151,152,153,154)
		group by 
			 "dg".id_geocerca
	   	    ,"dg".tipo_geocerca
			,"dg".nombre
),"pos_lat_min" as (
		select
			 "dg".id_geocerca
   		 	,"dg".tipo_geocerca
			,case "dg".id_geocerca
				when 146
					then
						'CUAUTITLAN 1'
				when 147
					then
						'CUAUTITLAN 2' 
				when 150
					then
						'MACUSPANA'
				when 151
					then 'RAMOS ARIZPE'
				when 
					154
					then 
						'TULTITLAN'
				else
					ltrim(rtrim(replace("dg".nombre,'BASE' , '')))
			 end as 'base'
			,min("dp".latitud) as 'latitud'
		from 
			gstdb.dbo.desp_geocercas as "dg"
		left join
			gstdb.dbo.desp_puntos_geocercas as "dp"
		on 
			"dp".id_geocerca = "dg".id_geocerca
--		where 
--			"dg".id_geocerca in (148,149,150,151,152,153,154)
		group by 
			 "dg".id_geocerca
	   	    ,"dg".tipo_geocerca
			,"dg".nombre
),"pos_lon_max" as (
		select
			 "dg".id_geocerca
	 	    ,"dg".tipo_geocerca
			,case "dg".id_geocerca
				when 146
					then
						'CUAUTITLAN 1'
				when 147
					then
						'CUAUTITLAN 2' 
				when 150
					then
						'MACUSPANA'
				when 151
					then 'RAMOS ARIZPE'
				when 
					154
					then 
						'TULTITLAN'
				else
					ltrim(rtrim(replace("dg".nombre,'BASE' , '')))
			 end as 'base'
			,max("dp".longitud) as 'longitud'
		from 
			gstdb.dbo.desp_geocercas as "dg"
		left join
			gstdb.dbo.desp_puntos_geocercas as "dp"
		on 
			"dp".id_geocerca = "dg".id_geocerca
--		where 
--			"dg".id_geocerca in (148,149,150,151,152,153,154)
		group by 
			 "dg".id_geocerca
	   	    ,"dg".tipo_geocerca
			,"dg".nombre
),"pos_lon_min" as (
		select
			 "dg".id_geocerca
		    ,"dg".tipo_geocerca
			,case "dg".id_geocerca
				when 146
					then
						'CUAUTITLAN'
				when 147
					then
						'CUAUTITLAN' 
				when 150
					then
						'MACUSPANA'
				when 151
					then 'RAMOS ARIZPE'
				when 
					154
					then 
						'TULTITLAN'
				else
					ltrim(rtrim(replace("dg".nombre,'BASE' , '')))
			 end as 'base'
			,min("dp".longitud) as 'longitud'
		from 
			gstdb.dbo.desp_geocercas as "dg"
		left join
			gstdb.dbo.desp_puntos_geocercas as "dp"
		on 
			"dp".id_geocerca = "dg".id_geocerca
--		where 
--			"dg".id_geocerca in (148,149,150,151,152,153,154)
		group by 
			 "dg".id_geocerca
	   	    ,"dg".tipo_geocerca
			,"dg".nombre
)
select 
--	 "pos_lat_max".id_geocerca
     row_number() over(order by "pos_lat_max".base) as 'id'
    ,"pos_lat_max".tipo_geocerca
	,"pos_lat_max".base
	,"pos_lat_max".latitud as 'max_latitud'
	,"pos_lat_min".latitud as 'min_latitud'
	,"pos_lon_max".longitud as 'max_longitud'
	,"pos_lon_min".longitud as 'min_longitud'
from 
	"pos_lat_max"
inner join 
	"pos_lat_min"
on 
	"pos_lat_max".id_geocerca = "pos_lat_min".id_geocerca
inner join 
	"pos_lon_max"
on 
	"pos_lat_max".id_geocerca = "pos_lon_max".id_geocerca
inner join 
	"pos_lon_min"
on 
	"pos_lat_max".id_geocerca = "pos_lon_min".id_geocerca


	
	

-- ============================================================================================================== --
-- Ws Copiloto 
-- ============================================================================================================== --

use [sistemas]
-- go
IF OBJECT_ID('sistemas.dbo.ws_tbl_gst_copiloto_historico_unidades', 'U') IS NOT NULL 
  DROP TABLE sistemas.dbo.ws_tbl_gst_copiloto_historico_unidades 
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on
-- go
create table ws_tbl_gst_copiloto_historico_unidades(
		 id					 bigint					identity(1,1)
		,alias				 char(55)				null
		,nombre				 char(255)				null
		,idMovil			 bigint					null
		,latitud			 decimal(18,6)			null
		,longitud			 decimal(18,6)			null
		,lugar				 char(255)				null
		,placas				 char(255)				null
		,fechahorautc		 datetime				null
		,tipo				 int 					null
		,created			 datetime 				default current_timestamp null
		,modified			 datetime				null
		,status				 tinyint 				default 1 null
) on [primary]
-- go
set ansi_padding off


-- ============================================================================================================== --
-- Ws Copiloto 
-- ============================================================================================================== --
-- truncate table sistemas.dbo.ws_tbl_clkctrl_historico_unidades


use [sistemas]
-- go
IF OBJECT_ID('sistemas.dbo.ws_tbl_clkctrl_historico_unidades', 'U') IS NOT NULL 
  DROP TABLE sistemas.dbo.ws_tbl_clkctrl_historico_unidades
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on
-- go
create table ws_tbl_clkctrl_historico_unidades(
		 id					 bigint					identity(1,1)
		,block_desc		     char(55)			    null
		,created			 datetime 				default current_timestamp null
		,modified			 datetime				null
		,status				 tinyint 				default 1 null
) on [primary]
-- go
set ansi_padding off


-- LOG errors

use [sistemas]
-- go
IF OBJECT_ID('sistemas.dbo.ws_tbl_historico_unidades_logs', 'U') IS NOT NULL 
  DROP TABLE sistemas.dbo.ws_tbl_historico_unidades_logs
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on
-- go
create table ws_tbl_historico_unidades_logs(
		 id					 bigint					identity(1,1)
		,message		     text			    	null
		,created			 datetime 				default current_timestamp null
		,modified			 datetime				null
		,status				 tinyint 				default 1 null
) on [primary]
-- go
set ansi_padding off


use sistemas

exec sp_desc ws_tbl_cls

exec sp_desc ws_georeference_view_post_services

use gstdb_pba 
--select * from gstdb.dbo.desp_importtodesp
exec sp_desc desp_importtodesp

use [sistemas]
-- go
IF OBJECT_ID('sistemas.dbo.ws_tbl_cls', 'U') IS NOT NULL 
  DROP TABLE sistemas.dbo.ws_tbl_cls
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on
-- go
create table ws_tbl_cls (
						 id 				bigint			identity(1,1)
--						wsid 				bigint			null
						,id_consecutivo 	int				null
						,tipopaquete 		char(1)			null
						,idmensaje 			int				null
						,mensaje 			text			null
						,zipcode 			varchar(8)		null
						,recvdate 			varchar(10)		null
						,recvtime 			varchar(11)		null
						,msgdate 			varchar(10)		null
						,msgtime 			varchar(11)		null
						,posdate 			varchar(10)		null
						,postime 			varchar(11)		null
						,prioridad 			int				null
						,num_antena 		varchar(50)		null
						,nombreantena 		varchar(50)		null
						,posicion 			varchar(200)	null
						,poslat 			decimal(18,6)	null
						,poslon 			decimal(18,6)	null
						,status_qt 			varchar(2)		null
						,timezone 			varchar(20)		null
						,ignition 			varchar(10)		null
						,sistemaorigen 		varchar(15)		null
						,clave_macro 		varchar(10)		null
						,tipomacro 			char(1)			null
						,detmacro 			varchar(250)	null
						,status_registro 	int				null
						,acuserecibo 		char(1)			null
						,desttype 			int				null
						,velocidad 			int				null
						,angulo 			int				null
						,direccion 			varchar(15)		null
						,fechaeta 			datetime		null
						,dist_faltante 		int				null
)
-- go
set ansi_padding off



--group by created order by created



select * from 
--delete from 
sistemas.dbo.ws_tbl_gst_copiloto_historico_unidades where ws_tbl_clkctrl_historico_unidades_id is not null

-- select * from sistemas.dbo.ws_tbl_clkctrl_historico_unidades 

 -- select * from ws_georeference_view_post_services where ws_tbl_clkctrl_historico_unidades_id is not null

select * from sistemas.dbo.ws_georeference_view_post_services where ws_tbl_clkctrl_historico_unidades_id is not null
	


use sistemas

alter trigger tr_insert_export_importtodes
ON sistemas.dbo.ws_tbl_clkctrl_historico_unidades
after update
as 
begin 
	
	declare @ins_id as int 

 	set @ins_id = (select "in".id from inserted as "in" where "in".status = 2 group by "in".id)
--	set @ins_id = 1936
	-- Catch the rows for examination
	set nocount on
	
	declare @ws table (

						 tipopaquete 		char(1)			null	--
						,idmensaje			int				null    -- x
						,zipcode			varchar(8)		null    -- x
						,recvdate 			varchar(10)		null	-- 
						,recvtime 			varchar(11)		null	-- 
						,msgdate			varchar(10)		null	-- x
						,msgtime			varchar(11)		null	-- x
						,posdate			varchar(10)		null    -- x
						,postime			varchar(11)		null    -- x
						,num_antena 		varchar(50)		null	--
						,nombreantena 		varchar(50)		null	--
						,posicion			varchar(200)	null
						,poslat 			decimal(18,6)	null  	--
						,poslon 			decimal(18,6)	null  	--
						,timezone 			varchar(20)		null	--
						,ignition 			varchar(10)		null	--
						,sistemaorigen 		varchar(15)		null	--
						,clave_macro 		varchar(10)		null    --
						,tipomacro 			char(1)			null	--
						,detmacro			varchar(250)	null
						,status_registro 	int				null	--
						,acuserecibo 		char(1)			null	--
						,desttype 			int				null	--
	) 
	
	
	begin try 
	begin transaction
	
			insert into @ws 
												(
													 tipopaquete 		
													,idmensaje			
													,zipcode			
													,recvdate 			 
													,recvtime 			 
													,msgdate			
													,msgtime			
													,posdate			
													,postime			
													,num_antena 		
													,nombreantena 		
													,posicion			
													,poslat 			
													,poslon
													,clave_macro
													,tipomacro
													,desttype
													,detmacro
													,status_registro
													,timezone 			
													,ignition 			
													,sistemaorigen 		 	
													,acuserecibo 															 			
												)
				select 
		--				 "hist".id
					 	 1 as 'tipopaquete'
					 	,1 as 'idmensaje'
					 	,'0' as 'zipcode'
					 	,cast("pos".created as date) as 'recvdate'
					 	,convert(varchar(8),"pos".created,108) as 'recvtime'
					 	,cast("pos".created as date) as 'msgdate'
					 	,convert(varchar(8),"pos".created,108) as 'msgtime'
					 	,cast("pos".created as date) as 'posdate'
					 	,convert(varchar(8),"pos".created,108) as 'postime'
					 	,"pos".idMovil as 'num_antena'
					 	,"pos".nombre as 'nombreantena'
					 	,'' as 'posicion'
					 	,"pos".latitud as 'poslat'
					 	,"pos".longitud as 'poslon'
					 	,"pos".macro as 'clave_macro'
					 	,'R' as 'tipomacro'
					 	,1 as 'desttype'
					 	,'[;]' as 'detmacro'
					 	,0 as 'status_registro'
					 	,"pos".created as 'timezone'
					 	,'E' as 'ignition'
					 	,'Copiloto' as 'sistemaorigen'
					 	,'N' as 'acuserecibo'
				from 
						sistemas.dbo.ws_tbl_clkctrl_historico_unidades as "hist"
--				inner join 
--						inserted as "i"
--					on 
--						"i".id = "hist".id --and "hist".status = 2
				inner join
						sistemas.dbo.ws_georeference_view_post_services as "pos"
					on 
						"hist".id = "pos".ws_tbl_clkctrl_historico_unidades_id 
						and 
							"pos".ws_tbl_clkctrl_historico_unidades_id is not null 
						and 
							"pos".geocerca_in = 1
						and 
							"pos".macro is not null
				where 
						"hist".id = @ins_id
					
				--		select "i".id from inserted as "i"
				--		where "i".status = 2
				--		group by "i".id
				

			select * from @ws
					
			insert into sistemas.dbo.ws_tbl_cls 
												(
													 tipopaquete 		
													,idmensaje			
													,zipcode			
													,recvdate 			 
													,recvtime 			 
													,msgdate			
													,msgtime			
													,posdate			
													,postime			
													,num_antena 		
													,nombreantena 		
													,posicion			
													,poslat 			
													,poslon
													,clave_macro
													,tipomacro
													,desttype
													,detmacro
													,status_registro
													,timezone 			
													,ignition 			
													,sistemaorigen 		 	
													,acuserecibo
												)
										select 
													 tipopaquete 		
													,idmensaje			
													,zipcode			
													,recvdate 			 
													,recvtime 			 
													,msgdate			
													,msgtime			
													,posdate			
													,postime			
													,num_antena 		
													,nombreantena 		
													,posicion			
													,poslat 			
													,poslon
													,clave_macro
													,tipomacro
													,desttype
													,detmacro
													,status_registro
													,timezone 			
													,ignition 			
													,sistemaorigen 		 	
													,acuserecibo
										from @ws
		
			insert into gstdb.dbo.desp_importtodesp 
												(
													 tipopaquete 		
													,idmensaje			
													,zipcode			
													,recvdate 			 
													,recvtime 			 
													,msgdate			
													,msgtime			
													,posdate			
													,postime			
													,num_antena 		
													,nombreantena 		
													,posicion			
													,poslat 			
													,poslon
													,clave_macro
													,tipomacro
													,desttype
													,detmacro
													,status_registro
													,timezone 			
													,ignition 			
													,sistemaorigen 		 	
													,acuserecibo
												)
											select 
													 tipopaquete 		
													,idmensaje			
													,zipcode			
													,recvdate 			 
													,recvtime 			 
													,msgdate			
													,msgtime			
													,posdate			
													,postime			
													,num_antena 		
													,nombreantena 		
													,posicion			
													,poslat 			
													,poslon
													,clave_macro
													,tipomacro
													,desttype
													,detmacro
													,status_registro
													,timezone 			
													,ignition 			
													,sistemaorigen 		 	
													,acuserecibo
											from @ws

			insert into sistemas.dbo.ws_tbl_historico_unidades_logs (message)
				select cast("i".id as varchar(max))+'--'+ cast("i".status as varchar(max)) +'_testing at ' + cast(current_timestamp as varchar(30)) from inserted as "i"
		commit transaction	
		PRINT 'Transaction committed'
	end try
	begin catch
--		rollback
--		PRINT 'Transaction rolled back'
		declare @errNum int = ERROR_NUMBER() ,@errLine varchar(500) = ERROR_LINE() ,@errMsg VARCHAR(500) = ERROR_MESSAGE() ,@errState INT = ERROR_STATE() ,@errSeverity int = ERROR_SEVERITY()
	--    ERROR_NUMBER() AS [Error_Number],
	--    ERROR_SEVERITY() AS [Error_Severity],
	--    ERROR_STATE() AS [Error_State],
	--    ERROR_LINE() AS [Error_Line],
	--    ERROR_MESSAGE() AS [Error_Message];
		insert into sistemas.dbo.ws_tbl_historico_unidades_logs (message)
		values (@errMsg)
--		raiserror(@errMsg, @errSeverity, @errState)
	end CATCH
end -- End trigger

-- ================================================================= --
	select * from sistemas.dbo.ws_tbl_cls
-- ================================================================= --


select 
		 "hist".id
	 	,1 as 'tipopaquete'
	 	,cast("pos".created as date) as 'recvdate'
	 	,convert(varchar(8),"pos".created,108) as 'recvtime'
	 	,"pos".idMovil as 'num_antena'
	 	,"pos".nombre as 'nombreantena'
	 	,"pos".latitud as 'poslat'
	 	,"pos".longitud as 'poslon'
	 	,"pos".macro as 'clave_macro'
	 	,'R' as 'tipomacro'
	 	,2 as 'tipopaquete'
	 	,1 as 'desttype'
	 	,0 as 'status_registro'
	 	,"pos".created as 'timezone'
	 	,'E' as 'ignition'
	 	,'Copiloto' as 'sistemasorigen'
	 	,'N' as 'acuserecibo'
from 
		sistemas.dbo.ws_tbl_clkctrl_historico_unidades as "hist"
inner join
		sistemas.dbo.ws_georeference_view_post_services as "pos"
	on 
		"hist".id = "pos".ws_tbl_clkctrl_historico_unidades_id 
		and 
			"pos".ws_tbl_clkctrl_historico_unidades_id is not null 
		and 
			"pos".geocerca_in = 1
		and 
			"pos".macro is not null


select * from sistemas.dbo.ws_tbl_clkctrl_historico_unidades order by id desc 


	select * from sistemas.dbo.ws_georeference_view_post_services where ws_tbl_clkctrl_historico_unidades_id = 8694
--	where 
--			ws_tbl_clkctrl_historico_unidades_id = 33 
	order by 
			created
		
-- =============================================================================================================== --
-- Monitor of transactions 
-- =============================================================================================================== --

		select 
--			id
		 "ws".alias
		,"ws".nombre
		,"ws".base
		,"ws".id_unidad
		,"ws".TipoDescripcion
		,"ws".geocerca_in
		,"ws".macro
		,"ws".idMovil
		,"ws".tipo_geocerca
		,"ws".latitud
		,"ws".longitud
		,"ws".max_latitud
		,"ws".min_latitud
		,"ws".max_longitud
		,"ws".min_longitud
		,"ws".lugar
		,"ws".placas
		,"ws".fechahorautc
		,"ws".tipo
		,"ws".ws_tbl_clkctrl_historico_unidades_id
		,"ws".created
	from 
			sistemas.dbo.ws_georeference_view_post_services as "ws" 
	where 
			"ws".geocerca_in = 1
	and 
			"ws".macro is not null
	and 
			"ws".ws_tbl_clkctrl_historico_unidades_id is not null
--	and 
--			"ws".base like '%culiacan%'
--	and 
--			"ws".ws_tbl_clkctrl_historico_unidades_id = 8703
--	and 
--			id_unidad = 'TT1316'
	order by 
			created desc
		

	select * from sistemas.dbo.ws_tbl_cls where idmensaje = 1 order by id  -- tabla paralela a importtodesp / registra si se envia informacion a importtodesp
		
	select * from sistemas.dbo.ws_tbl_historico_unidades_logs order by id desc -- en caso de error en la insercion en importto desp
--	truncate table sistemas.dbo.ws_tbl_historico_unidades_logs
	
	select * from sistemas.dbo.ws_tbl_clkctrl_historico_unidades where status = 2 order by id desc --control interno
		
	select * from gstdb.dbo.desp_importtodesp where idmensaje = 1



	select * from sistemas.dbo.ws_tbl_gst_copiloto_historico_unidades where ws_tbl_clkctrl_historico_unidades_id = 8703 order by id desc

	select * from sistemas.dbo.ws_georeference_view_positions_gst_indicators where base like '%culiacan%'
	
	select * from sistemas.dbo.ws_georeference_view_post_services where base = 'GST. WALMART CULIACAN' and idMovil = 2100102416 order by id desc
	
	select * from sistemas.dbo.ws_tbl_gst_copiloto_historico_unidades where idMovil = 2100102416 and tipo in (211940,211950) order by id desc
-- =============================================================================================================== --
-- Monitor of transactions 
-- =============================================================================================================== --
2288458534



	


