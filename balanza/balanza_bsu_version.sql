--ALTER PROCEDURE [dbo].[sp_udsp_getBalanzaComprobacion]
-- (
--	@beginDate nvarchar(6),
--	@endDate nvarchar(6),
--	@Company varchar(8000), -- just in case
--	@Delimiter varchar(10)
-- )
--
--as

/*===============================================================================
 Author         : Jesus Baizabal
 email			: ambagasdowa@gmail.com
 Create date    : April 20, 2015
 Description    : fetch the Balanza for accounts and sub accounts form GLTran
 @license       : MIT License (http://www.opensource.org/licenses/mit-license.php)
 Database owner : bonampak s.a de c.v
 @status        : Stable
 @version		: 1.19.25
 ===============================================================================*/

--set ANSI_NULLS on 
--set QUOTED_IDENTIFIER on 
--set NOCOUNT on 

	declare @bussiness_unit table
	(
		Company		nvarchar(6) collate SQL_Latin1_General_CP1_CI_AS
	)
	insert into @bussiness_unit
		select item from integraapp.dbo.fnSplit(@Company, @Delimiter)

	SELECT
		h.Acct AS 'Cuenta',
		h.Sub AS 'ENTIDADES',
		--h.BalanceType AS AcctHist_BalanceType,
		SUBSTRING(h.sub , 8, 2) as 'empresa',
--		SUBSTRING(h.sub , 10, 6) as CentroCosto,
		a.Descr as 'Descripción',
--		h.CpnyID AS EmpresaDesc,
		CASE SUBSTRING(@beginDate, 5, 2)
			WHEN '01'
				THEN h.BegBal
			WHEN '02'
				THEN h.YTDBal00
			WHEN '03'
				THEN h.YTDBal01
			WHEN '04'
				THEN h.YTDBal02
			WHEN '05'
				THEN h.YTDBal03
			WHEN '06'
				THEN h.YTDBal04
			WHEN '07'
				THEN h.YTDBal05
			WHEN '08'
				THEN h.YTDBal06
			WHEN '09'
				THEN h.YTDBal07
			WHEN '10'
				THEN h.YTDBal08
			WHEN '11'
				THEN h.YTDBal09
			WHEN '12'
				THEN h.YTDBal10
			WHEN '13'
				THEN h.YTDBal11
			ELSE 0
			END AS 'Inicial',
--
		(
			select
	           --cast( SUM(integraapp.dbo.GLTran.DrAmt) as float)
			   SUM(integraapp.dbo.GLTran.DrAmt)
				--SUM(CuryDrAmt) - SUM(CuryCrAmt) as Result
			from integraapp.dbo.GLTran
			where integraapp.dbo.GLTran.Acct = h.Acct --'0101040100'
					and integraapp.dbo.GLTran.PerPost = @beginDate
					and integraapp.dbo.GLTran.CpnyID = h.CpnyID -- sum againts Companies is not working because the where clause split the results per company
					and integraapp.dbo.GLTran.Posted = 'P' -- Contant
					and integraapp.dbo.GLTran.Sub = h.Sub -- '000'
					and integraapp.dbo.GLTran.FiscYr = SUBSTRING(@beginDate,1,4)
					and integraapp.dbo.GLTran.LedgerID = 'REAL' -- Constant
					and SUBSTRING(integraapp.dbo.GLTran.Sub , 8, 2) = SUBSTRING(h.Sub,8,2)
					and SUBSTRING(integraapp.dbo.GLTran.Sub , 10, 6) = SUBSTRING(h.Sub,10,6)
		) as 'Cargo',
		(
			select
				--cast (SUM(integraapp.dbo.GLTran.CrAmt) as float)
				SUM(integraapp.dbo.GLTran.CrAmt)
				--SUM(CuryDrAmt) - SUM(CuryCrAmt) as Result
			from integraapp.dbo.GLTran
			where integraapp.dbo.GLTran.Acct = h.Acct --'0101040100' done
					and integraapp.dbo.GLTran.PerPost = @beginDate -- done
					and integraapp.dbo.GLTran.CpnyID = h.CpnyID -- done sum againts Companies is not working because the where clause split the results per company
					and integraapp.dbo.GLTran.Posted = 'P' -- Constant
					and integraapp.dbo.GLTran.Sub = h.Sub -- '000' donw
					and integraapp.dbo.GLTran.FiscYr = SUBSTRING(@beginDate,1,4) -- done
					and integraapp.dbo.GLTran.LedgerID = 'REAL' -- Constant
					and SUBSTRING(integraapp.dbo.GLTran.Sub , 8, 2) = SUBSTRING(h.Sub,8,2) --donw 
					and SUBSTRING(integraapp.dbo.GLTran.Sub , 10, 6) = SUBSTRING(h.Sub,10,6) --down
		) as 'Crédito',
--
		CASE SUBSTRING(@endDate, 5, 2)
			WHEN '01'
				THEN h.YTDBal00
			WHEN '02'
				THEN h.YTDBal01
			WHEN '03'
				THEN h.YTDBal02
			WHEN '04'
				THEN h.YTDBal03
			WHEN '05'
				THEN h.YTDBal04
			WHEN '06'
				THEN h.YTDBal05
			WHEN '07'
				THEN h.YTDBal06
			WHEN '08'
				THEN h.YTDBal07
			WHEN '09'
				THEN h.YTDBal08
			WHEN '10'
				THEN h.YTDBal09
			WHEN '11'
				THEN h.YTDBal10
			WHEN '12'
				THEN h.YTDBal11
			WHEN '13'
				THEN h.YTDBal12
			ELSE 0
		END AS 'Final'
	FROM integraapp.dbo.AcctHist as h
		INNER JOIN dbo.Account AS a
			ON h.Acct = a.Acct
	where
		--h.CpnyID in (SELECT item from dbo.fnSplit(@Company, @Delimiter))
		h.CpnyID in (select Company from @bussiness_unit)
		and h.FiscYr = SUBSTRING(@beginDate, 1, 4)
		and h.LedgerID = 'REAL'
		and h.Acct <> '0304000000'
	order by h.Acct,SUBSTRING(h.sub , 8, 2)



use integraapp
exec sp_who2
use sltestapp
	exec sp_udsp_getBalanzaComprobacion '201911','201911','TBKORI|TBKGDL','|'
	
	exec sp_udsp_getBalanzaComprobacion_pruebas '201808','201808','TBKORI|TBKGDL','|'
	
	-- ==========================  New mode ======================== -- 
	-- Redeem duplicates with an optional index 

	use sistemas
	IF OBJECT_ID ('balanza_view_udns_rpts', 'V') IS NOT NULL		
	    DROP VIEW balanza_view_udns_rpts;
	create view balanza_view_udns_rpts
	with encryption
	as		

	select
		"hist".Acct AS 'Cuenta'
		,"hist".Sub AS 'Entidades'
		,SUBSTRING("hist".sub , 8, 2) as 'Empresa'
		,"tran".CpnyID as 'UnidadNegocio'
		,"acc".Descr as 'Descripción'
		,"tran".TranDesc
		,"tran".PerPost as 'Periodo'
		,"tran".ExtRefNbr
		,"tran".User1
		,cast(substring("hist".Sub,1,7) as int) as 'Empleado'
		,"tran".BatNbr
		,"vend".Name
		,case 
			when (
						substring("hist".Sub,8,14) = '00000000000000'
--					and 
--						substring("hist".Sub,1,1) in ('1','2','3','4','5')
					and
						substring("hist".Sub,1,1) not in ('0','8','7')
					and 
						isnumeric(substring("hist".Sub,1,1)) = 1
				 )
				then
					(
						select 
								"ven".Name
						from 
								integraapp.dbo.Vendor as "ven"
						where 
								ltrim(rtrim("ven".VendId)) = substring("hist".Sub,1,7)
					)
				else 
					null
		end as 'Funcionario'
		,case 
			when ( row_number() over(partition by "hist".Acct,"hist".Sub,"tran".PerPost,"hist".CpnyID order by "hist".Sub) ) > 1 
				then 0 
			else 
				CASE SUBSTRING("tran".PerPost, 5, 2)
					WHEN '01'
						THEN "hist".BegBal
					WHEN '02'
						THEN "hist".YTDBal00
					WHEN '03'
						THEN "hist".YTDBal01
					WHEN '04'
						THEN "hist".YTDBal02
					WHEN '05'
						THEN "hist".YTDBal03
					WHEN '06'
						THEN "hist".YTDBal04
					WHEN '07'
						THEN "hist".YTDBal05
					WHEN '08'
						THEN "hist".YTDBal06
					WHEN '09'
						THEN "hist".YTDBal07
					WHEN '10'
						THEN "hist".YTDBal08
					WHEN '11'
						THEN "hist".YTDBal09
					WHEN '12'
						THEN "hist".YTDBal10
					WHEN '13'
						THEN "hist".YTDBal11
					ELSE 0
				end
		end as 'Inicial'
--
		,"tran".DrAmt as 'Cargo'
		,"tran".CrAmt as 'Crédito'
--
		,case 
			when ( row_number() over(partition by "hist".Acct,"hist".Sub,"tran".PerPost,"hist".CpnyID order by "hist".Sub) ) > 1 
				then 0 
			else 
				CASE SUBSTRING("tran".PerPost, 5, 2)
					WHEN '01'
						THEN "hist".YTDBal00
					WHEN '02'
						THEN "hist".YTDBal01
					WHEN '03'
						THEN "hist".YTDBal02
					WHEN '04'
						THEN "hist".YTDBal03
					WHEN '05'
						THEN "hist".YTDBal04
					WHEN '06'
						THEN "hist".YTDBal05
					WHEN '07'
						THEN "hist".YTDBal06
					WHEN '08'
						THEN "hist".YTDBal07
					WHEN '09'
						THEN "hist".YTDBal08
					WHEN '10'
						THEN "hist".YTDBal09
					WHEN '11'
						THEN "hist".YTDBal10
					WHEN '12'
						THEN "hist".YTDBal11
					WHEN '13'
						THEN "hist".YTDBal12
					ELSE 0
				end
		 end as 'Final'
--		,row_number() over(partition by "hist".Acct,"hist".Sub,"tran".PerPost,"hist".CpnyID order by "hist".Sub) as 'inx'
	FROM 
			integraapp.dbo.AcctHist as "hist"
	INNER JOIN 
			integraapp.dbo.Account AS "acc"
		ON 
				"hist".Acct = "acc".Acct
	left join 
			integraapp.dbo.GLTran as "tran"
		on
				"tran".CpnyID = "hist".CpnyID
		and
				"tran".Acct = "hist".Acct
		and
				"tran".Sub = "hist".Sub
		and 
				SUBSTRING("tran".Sub , 8, 2) = SUBSTRING("hist".Sub,8,2)
		and 
				SUBSTRING("tran".Sub , 10, 6) = SUBSTRING("hist".Sub,10,6)
--		and
--				"tran".PerPost = @beginDate -- NOTE
		and
				"tran".FiscYr = "hist".FiscYr -- NOTE
		and 
				"tran".LedgerID = 'REAL'
		and 
				"tran".Posted = 'P'
	left join 
			integraapp.dbo.Vendor as "vend"
		on
				ltrim(rtrim("vend".VendId)) = ltrim(rtrim("tran".User1))
	inner join 
			sistemas.dbo.balanza_tbl_worker_accounts as "account"
		on
			"hist".Acct = "account".account collate SQL_Latin1_General_CP1_CI_AS
	where
--		"hist".CpnyID in ('TBKORI','TBKGDL')
--		Check This At first
--			"hist".FiscYr in ( year(CURRENT_TIMESTAMP), year(dateadd(year,-1,CURRENT_TIMESTAMP)) )
			"hist".FiscYr in ( year(CURRENT_TIMESTAMP) )
--			"tran".PerPost in (
--				  substring( convert(nvarchar(MAX), dateadd(mm,-2,cast(current_timestamp as date)), 112) , 1, 6 ) -- current month minus 2
--				 ,substring( convert(nvarchar(MAX), dateadd(mm,-1,cast(current_timestamp as date)), 112) , 1, 6 ) -- current month minus 1
--				 ,substring( convert(nvarchar(MAX), current_timestamp, 112) , 1, 6 ) 							 -- current month
--			)
		and 
			"hist".LedgerID = 'REAL'
		and 
			"hist".Acct <> '0304000000' -- Constant
			
--		and 
--			"hist".Acct in ('0101080000','0101090000','0101070000','0101060300')
--		and 
--			"hist".Sub in ('100070100000000000000')
--
 	order by "hist".Acct,SUBSTRING("hist".sub , 8, 2)

--create table 
exec sp_who2
-- =========================================================== --
use sistemas
IF OBJECT_ID('balanza_tbl_worker_accounts', 'U') IS NOT NULL 
  DROP TABLE balanza_tbl_worker_accounts; 

set ansi_nulls on
set quoted_identifier on
set ansi_padding on

create table [balanza_tbl_worker_accounts](
		id						int identity(1,1),
		account					char(10),
		created					datetime,
		modified				datetime,
		user_id					int null,
		"status"				tinyint default 1 null
) on [primary]

set ansi_padding off
-- =========================================================== --
-- insert into balanza_tbl_worker_accounts values	
--('0101080000',CURRENT_TIMESTAMP,current_timestamp,null,1),
--('0101090000',CURRENT_TIMESTAMP,current_timestamp,null,1),
--('0101070000',CURRENT_TIMESTAMP,current_timestamp,null,1),
--('0101060300',CURRENT_TIMESTAMP,current_timestamp,null,1)
--;


--- Accounts where search are made ! 	
select * from sistemas.dbo.balanza_tbl_worker_accounts

-- 	last query
 	select 
 		 "balanza".Cuenta
 		,"balanza".Entidades
 		,"balanza".Empresa
 		,"balanza".UnidadNegocio
 		,"balanza".Descripción
 		,"balanza".TranDesc
 		,"balanza".Periodo
 		,"balanza".ExtRefNbr
 		,"balanza".User1
 		,"balanza".Empleado
 		,"balanza".BatNbr
 		,"balanza".Name
 		,"balanza".Funcionario
 		,"balanza".Inicial
 		,"balanza".Cargo
 		,"balanza".Crédito
 		,"balanza"."Final"
	from 
		"sistemas"."dbo"."balanza_view_udns_rpts" as "balanza"
	where 
		"balanza".Empleado = '1000370'
		
		
-- 	
--		select status from NOM2001.dbo.getPayroll group by status 
--		
--		
--		
--		select Nombre,Stauts,ClaveTrabajador from NOM2001.dbo.getNomina 
----		where ClaveTrabajador = '1000476'
--		group by Nombre,Stauts,ClaveTrabajador
--		
--		
--		
--		

		select Puesto,ClaveEmpresa,Nombre,Status,ClaveTrabajador from NOM2001.dbo.getNominaMod 
		where Status = 'B'
		group by Puesto,ClaveEmpresa,Nombre,Status,ClaveTrabajador
		
		
--		select Baja from NOM2001.dbo.view_get_payrolls	group by Baja
		
 select * from "sistemas"."dbo"."balanza_view_udns_rpts" where 1 = 0	
 	
 	
 	select Name,TaxId00,VendId from integraapp.dbo.vendor
 	where
 		TaxId00 = 'NOSUJETO'

 		
 		
 -- ========================================================================================================= --
 -- 		balanza_view_udns_users_rpts
 -- ========================================================================================================= --		
 
 	use sistemas
	IF OBJECT_ID ('balanza_view_udns_users_rpts', 'V') IS NOT NULL		
	    DROP VIEW balanza_view_udns_users_rpts;
	create view balanza_view_udns_users_rpts
	with encryption
	as		
 	with "usr" as (
	 	select 
	 			cast(substring(Sub,1,7) as int) as 'usr_id',Descr,Active 
	 	from 
	 			integraapp.dbo.SubAcct
	 	where 
	 			substring(Sub,8,14) = '00000000000000'
	 		and
	 			substring(Sub,1,1) not in ('0','8','7')
	 		and 
	 			isnumeric(substring(Sub,1,1)) = 1
	 	group by 
	 	
	 			substring(Sub,1,7),Descr,Active
	)
	select  
		"usr".usr_id
		,case 
			when "usr".Descr = '' or "usr".Descr is null
				then "nom".Nombre
			else
				"usr".Descr
		end as 'Name'
		,case 
			when "usr".Descr = '' or "usr".Descr is null
				then "nom".ClaveTrabajador + ' ' + "nom".Nombre
			else
				"nom".ClaveTrabajador + ' ' + "usr".Descr
		end as 'id-text'
		,"usr".Active
		,"nom".Puesto,"nom".ClaveEmpresa,"nom".Nombre,"nom".Status,cast("nom".ClaveTrabajador as int) as "ClaveTrabajador"
	from 
		"usr"
	inner join 
		NOM2001.dbo.getNominaMod as "nom" 
	on 
		"usr".usr_id = "nom".ClaveTrabajador
		
		
		
		
		
		
		
--	group by Active

		select Puesto,ClaveEmpresa,Nombre,Status,ClaveTrabajador from NOM2001.dbo.getNominaMod 
		where ClaveTrabajador = 1000370
		group by Puesto,ClaveEmpresa,Nombre,Status,ClaveTrabajador
		
	
		
		
		
-- ===================================                          ============================================================== --
--									Operations Mods																			   --
-- ===================================                          ============================================================== --
	select * from sistemas.dbo.projections_view_full__acep_disp__indicators
	
	
	use sistemas
	IF OBJECT_ID ('projections_view_full__acep_disp__indicators', 'V') IS NOT NULL		
	    DROP VIEW projections_view_full__acep_disp__indicators;
	create view projections_view_full__acep_disp__indicators
	with encryption
	as	
	with "ThisSht" as (
	-- Aceptadas	
		select 
				id_area
				,id_unidad
				,id_configuracionviaje
				,id_tipo_operacion
				,id_fraccion
				,id_flota
				,no_viaje
	--			,num_guia
				,fecha_guia
				,mes
				,f_despachado
				,cliente
				,kms_viaje
				,kms_real
				,subtotal
				,peso
				,configuracion_viaje
				,tipo_de_operacion
				,flota
				,area
				,fraccion
				,company
				,trip_count
				,cast(substring( convert(nvarchar(MAX), fecha_guia, 112) , 1, 6 ) as int) as 'datelink'
		from 
			sistemas.dbo.projections_view_full_company_indicators
		where 
			substring( convert(nvarchar(MAX), f_despachado, 112) , 1, 6 ) <> substring( convert(nvarchar(MAX), fecha_guia, 112) , 1, 6 )
	--	and 
	--		(
	--			substring( convert(nvarchar(MAX), fecha_guia, 112) , 1, 6 ) = '201810'
			OR 
				f_despachado is null
	--		)
		union --all
		-- Despachado
		select 
				id_area
				,id_unidad
				,id_configuracionviaje
				,id_tipo_operacion
				,id_fraccion
				,id_flota
				,no_viaje
				,fecha_guia
				,mes
				,f_despachado
				,cliente
				,kms_viaje
				,kms_real
				,subtotal
				,peso
				,configuracion_viaje
				,tipo_de_operacion
				,flota
				,area
				,fraccion
				,company
				,trip_count
				,cast(substring( convert(nvarchar(MAX), f_despachado, 112) , 1, 6 ) as int) as 'datelink'
		from sistemas.dbo.projections_view_full_company_dispatched_indicators
	)
	select 
				 id_area
				,id_unidad
				,id_configuracionviaje
				,id_tipo_operacion
				,id_fraccion
				,id_flota
				,no_viaje
				,fecha_guia
				,mes
				,f_despachado
				,cliente
				,kms_viaje
				,kms_real
				,subtotal
				,peso
				,configuracion_viaje
				,tipo_de_operacion
				,flota
				,area
				,fraccion
				,company
				,trip_count
				,case
					when
						substring( convert(nvarchar(MAX), fecha_guia, 112) , 1, 6 ) <> substring( convert(nvarchar(MAX), f_despachado, 112) , 1, 6 )
					then 
						'Aceptado y Despachado en Otros Meses'
					when 
						substring( convert(nvarchar(MAX), fecha_guia, 112) , 1, 6 ) = substring( convert(nvarchar(MAX), f_despachado, 112) , 1, 6 )
					then 
						'Aceptado'
					when 
						f_despachado is not null and fecha_guia is null 
					then 
						'Viaje Sin Carta Porte'
					when 
						f_despachado is null and fecha_guia is not null
					then 
						'CartaPorte sin Viaje'
					else
						'Despachado'
				end as 'Descripcion'
				,"datelink"
	from
		"ThisSht"
--	where
--		"datelink" = '201810' 
--	and 
--		id_area = 1 
--	and 
--		company = 1
	
	
	-- =====
	
	
	
--	======================  provision de despachado =============================== --
use sistemas
IF OBJECT_ID ('operations_view_full_company_provision_indicators', 'V') IS NOT NULL		
    DROP VIEW operations_view_full_company_provision_indicators;
create view operations_view_full_company_provision_indicators
as 
	
			select 
				  "despachado".id_area
				 ,"despachado".id_unidad
				 ,"despachado".id_configuracionviaje
				 ,"despachado".id_tipo_operacion
				 ,"despachado".id_fraccion
				 ,"despachado".id_flota
				 ,"despachado".no_viaje
				 ,"despachado".num_guia
				 ,"despachado".id_ruta
				 ,"despachado".id_origen
				 ,"despachado".desc_ruta
				 ,"despachado".monto_retencion
				 ,"despachado".fecha_guia
				 ,"despachado".mes
				 ,"despachado"."year"
				 ,"despachado".dia
				 ,"despachado".f_despachado
				 ,"despachado".cliente
				 ,"despachado".kms
				 ,"despachado".subtotal
				 ,"despachado".peso
				 ,"despachado".configuracion_viaje
				 ,"despachado".tipo_de_operacion
				 ,"despachado".flota
				 ,"despachado".area
				 ,"despachado".fraction
				 ,"despachado".fraccion
				 ,"despachado".company
				 ,"despachado".num_viajes
				 ,"despachado".periodo
				 ,"despachado".datelink
		from 
				sistemas.dbo.operations_view_full_company_dispatched_indicators as "despachado"
		full join 
				sistemas.dbo.operations_view_full_company_indicators as "aceptado"
			on 
				"despachado".company = "aceptado".company and "despachado".id_area = "aceptado".id_area and "despachado".num_guia = "aceptado".num_guia
			and 
				"aceptado".id_fraccion = "despachado".id_fraccion
		where 
				"despachado".Descripcion = 'Provision'
			
--			union all	
		--	==	aceptado provision

--		select 
--				 "aceptado".id_area
--				,"aceptado".id_unidad
--				,"aceptado".id_configuracionviaje
--				,"aceptado".id_tipo_operacion
--				,"aceptado".id_fraccion
--				,"aceptado".id_flota
--				,"aceptado".no_viaje
--				,"aceptado".num_guia
--				,"aceptado".id_ruta
--				,"aceptado".id_origen
--				,"aceptado".desc_ruta
--				,"aceptado".monto_retencion
--				,"aceptado".fecha_guia
--				,"aceptado".mes
--				,"aceptado"."year"
--				,"aceptado".dia
--				,"aceptado".f_despachado
--				,"aceptado".cliente
--				,"aceptado".kms
--				,"aceptado".subtotal
--				,"aceptado".peso
--				,"aceptado".configuracion_viaje
--				,"aceptado".tipo_de_operacion
--				,"aceptado".flota
--				,"aceptado".area
--				,"aceptado".fraction
--				,"aceptado".fraccion
--				,"aceptado".company
--				,"aceptado".num_viajes
--				,"aceptado".Descripcion
--				,cast(substring( convert(nvarchar(MAX), "aceptado".fecha_guia, 112) , 1, 6 ) as int) as 'datelink'
--		from
--			sistemas.dbo.operations_view_full_company_indicators as "aceptado"
--		where 
--			Descripcion = 'Provision' 

select * from sistemas.dbo.operations_view_full_company_dispatched_indicators
			
use sistemas
IF OBJECT_ID ('operations_view_full_company_dispatched_indicators', 'V') IS NOT NULL		
    DROP VIEW operations_view_full_company_dispatched_indicators;
create view operations_view_full_company_dispatched_indicators
as 		
	select 
				  "despachado".id_area
				 ,"despachado".id_unidad
				 ,"despachado".id_configuracionviaje
				 ,"despachado".id_tipo_operacion
				 ,"despachado".id_fraccion
				 ,"despachado".id_flota
				 ,"despachado".no_viaje
--				 ,"aceptado".no_viaje
				 ,"despachado".num_guia
--				 ,"aceptado".num_guia
				 ,"despachado".id_ruta
				 ,"despachado".id_origen
				 ,"despachado".desc_ruta
				 ,"despachado".monto_retencion
				 ,"despachado".fecha_guia
				 ,"despachado".mes
				 ,"despachado"."year"
				 ,"despachado".dia
				 ,"despachado".f_despachado
				 ,"despachado".cliente
				 ,"despachado".kms
				 ,"despachado".subtotal
				 ,"despachado".peso
				 ,"despachado".configuracion_viaje
				 ,"despachado".tipo_de_operacion
				 ,"despachado".flota
				 ,"despachado".area
				 ,"despachado".fraction
				 ,"despachado".fraccion
				 ,"despachado".company
				 ,"despachado".num_viajes
				 ,"despachado".periodo
				 ,cast(substring( convert(nvarchar(MAX), "despachado".f_despachado, 112) , 1, 6 ) as int) as 'datelink'
				 ,case 
				 	when
				 		"despachado".num_guia is not null and "aceptado".num_guia is null
				 	then
				 		'Provision'
					when 
						("despachado".num_guia is not null and "aceptado".num_guia is not null) and  ("despachado".num_guia = "aceptado".num_guia)
					then 
						'Aceptado'
					when 
						"despachado".f_despachado is not null and "despachado".fecha_guia is null 
					then 
						'Viaje Sin Carta Porte'
					when 
						"despachado".f_despachado is null and "despachado".fecha_guia is not null
					then 
						'CartaPorte sin Viaje'
				end as 'Descripcion'
		from 
				sistemas.dbo.operations_view_full_company_dispatched_src_indicators as "despachado"
		full join 
				sistemas.dbo.operations_view_full_company_indicators as "aceptado"
			on 
				"despachado".company = "aceptado".company and "despachado".id_area = "aceptado".id_area and "despachado".num_guia = "aceptado".num_guia
			and 
				"aceptado".id_fraccion = "despachado".id_fraccion
--			where
--				"despachado".id_area = 1 and "despachado".company = 1 and "despachado".fraccion = 'GRANEL' and cast(substring( convert(nvarchar(MAX), "despachado".f_despachado, 112) , 1, 6 ) as int) = '201810'						
			
--				OO-053877,OO-053878

				
				
				
-- Mexicali		Z3218091109 
-- Hermosillo	E6480571109

				


