-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--  Author						: Jesus Baizabal
--  email		        : baizabal.jesus@gmail.com
--  Create date			: December 01, 2022
--  Description			: Build tables for cmx-api module
--  TODO		          : clean
--  @Last_patch			: --
--  @license					: MIT License (http://www.opensource.org/licenses/mit-license.php)
--  Database owner		: Jesus Baizabal
--  @status					: Stable
--  @version	        : 0.0.1
-- Copyright © 2022, UES devops - portalapps.com, All Rights Reserved
-------------------------------------------------------------------------------
-- Description:        Verbose description of what the query does goes here. Be specific and don't be
--                     afraid to say too much. More is better, than less, every single time. Think about
--                     "what, when, where, how and why" when authoring a description.
-- Call by:            [schema.usp_ProcThatCallsThis]
--                     [application Name]
--                     [Job]
--                     [PLC/Interface]
-- Affected table(s):  [schema.TableModifiedByProc1]
--                     [schema.TableModifiedByProc2]
-- Used By:            Functional Area this is use in, for example, Payroll, Accounting, Finance
-- Parameter(s):       @param1 - description and usage
--                     @param2 - description and usage
-- Usage:              EXEC dbo.usp_DoSomeStuff
--                         @param1 = 1,
--                         @param2 = 3,
--                         @param3 = 2
--                     Additional notes or caveats about this object, like where is can and cannot be run, or
--                     gotchas to watch for when using it.
-------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Catalog Status
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
--truncate table sistemas.dbo.app_api_parents
-- Test 

-- SELECT * FROM INFORMATION_SCHEMA.COLUMNS
-- example 
-- select * from sistemas.dbo.providers_parents

/* SELECT * FROM sysobjects WHERE xtype='U' */ 

/* SELECT TABLE_NAME */ 
/* FROM [<DATABASE_NAME>].INFORMATION_SCHEMA.TABLES */ 
/* WHERE TABLE_TYPE = 'BASE TABLE' */

/* SELECT TABLE_NAME */ 
/* FROM [sistemas].INFORMATION_SCHEMA.TABLES */ 
/* WHERE TABLE_TYPE = 'BASE TABLE' */
/* and table_name like 'app_%' */


IF OBJECT_ID('sistemas.dbo.app_main', 'U') IS NOT NULL
  DROP TABLE sistemas.dbo.app_main;
-- go
set ansi_nulls on
set quoted_identifier on
set ansi_padding on
create table [sistemas].[dbo].[app_main](
        id                      int identity(1,1),
        application             nvarchar(150)       collate     sql_latin1_general_cp1_ci_as,
        created                 datetime,
        modified                datetime,
        user_id                 int,
        _status                 tinyint default 1 null
) on [primary]
-- go

set ansi_padding off
-- go

insert into sistemas.dbo.app_main values
       ('cmex',CURRENT_TIMESTAMP,null,1,1)                                        -- 1
      ,('michelin',CURRENT_TIMESTAMP,null,1,1)                                      -- 2
    ;

IF OBJECT_ID('sistemas.dbo.app_api_methods', 'U') IS NOT NULL
  DROP TABLE sistemas.dbo.app_api_methods;
set ansi_nulls on
set quoted_identifier on
set ansi_padding on
create table [sistemas].[dbo].[app_api_methods](
        id                      int identity(1,1),
        app_id                  int null,
        methods                 nvarchar(150)       collate     sql_latin1_general_cp1_ci_as,
        created                 datetime,
        modified                datetime,
        user_id                 int,
        _status                 tinyint default 1 null
) on [primary]

set ansi_padding off

insert into sistemas.dbo.app_api_methods values
       (2,'obterPacotePosicoes',CURRENT_TIMESTAMP,null,1,1)                                        -- 1
      ,(2,'obterVeiculos',CURRENT_TIMESTAMP,null,1,1)                                      -- 2
    ;
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Catalog Status Parents
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- create method element
/* IF OBJECT_ID('sistemas.dbo.app_api_methods_elements', 'U') IS NOT NULL */
/*   DROP TABLE sistemas.dbo.app_api_methods_elements; */
/* set ansi_nulls on */
/* set quoted_identifier on */
/* set ansi_padding on */
/* create table [sistemas].[dbo].[app_api_methods_elements]( */
/*         id                      int identity(1,1), */
/*         app_api_methods_id      int null, */
/*         elements                nvarchar(max)       collate     sql_latin1_general_cp1_ci_as, */
/*         created                 datetime, */
/*         modified                datetime, */
/*         user_id                 int, */
/*         status                 tinyint default 1 null */
/* ) on [primary] */

/* set ansi_padding off */

/* insert into sistemas.dbo.app_api_methods_elements values */
/*        (2,'obterPacotePosicoes',CURRENT_TIMESTAMP,null,1,1)                                        -- 1 */
/*       ,(2,'obterVeiculos',CURRENT_TIMESTAMP,null,1,1)                                      -- 2 */
/*     ; */



IF OBJECT_ID('sistemas.dbo.app_block', 'U') IS NOT NULL
  DROP TABLE sistemas.dbo.app_block;
-- go
set ansi_nulls on
set quoted_identifier on
set ansi_padding on
create table [sistemas].[dbo].[app_block](
        id                      int identity(1,1),
        app_api_methods_id      int null,
        created                 datetime,
        modified                datetime null,
        user_id                 int null,
        status                 tinyint default 1 null
) on [primary]
-- go

set ansi_padding off
-- go

/* insert into sistemas.dbo.app_block values */
/*        (2,CURRENT_TIMESTAMP,null,1,1)                       -- 1 */
/*       ,(2,CURRENT_TIMESTAMP,null,1,1)                       -- 2 */
/*     ; */

------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Controls providersControlsFiles
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/* -- the last identity value generated by the current connection */
/* select  @@IDENTITY */
/* -- This function will return the value of the last identity inserted in the current executing batch. */
/* select SCOPE_IDENTITY() */
/* -- returns the last identity value inserted in the specified table */
/* select IDENT_CURRENT('sistemas.dbo.app_api_cfdi_comprobante'); */



IF OBJECT_ID('sistemas.dbo.app_api_data', 'U') IS NOT NULL
  DROP TABLE sistemas.dbo.app_api_data;
-- go
set ansi_nulls on
set quoted_identifier on
set ansi_padding on

create table sistemas.dbo.app_api_data (
        id                          bigint identity(1,1),
        app_block_id                int null,
        tag                         nvarchar(max)       collate     sql_latin1_general_cp1_ci_as,
        value                       nvarchar(max)       collate     sql_latin1_general_cp1_ci_as,
        created                     datetime null,
        modified                    datetime default null,
       status                     tinyint default 1 null
    )on [primary]

set ansi_padding off

/* insert into sistemas.dbo.app_block values */
/*        (1,'tag','value',CURRENT_TIMESTAMP,null,1,1)                       -- 1 */
/*       ,(2,'tag','value',CURRENT_TIMESTAMP,null,1,1)                       -- 2 */
/*     ; */

use sistemas

IF OBJECT_ID ('app_view_relations', 'V') IS NOT NULL
    DROP VIEW app_view_relations;
-- now build the view

create view app_view_relations
with encryption
as --

  select -- top 100
     app.application
    ,mts.id as method_id
    ,blk.id as block_id
    ,mts.methods
    ,dt.tag
    ,dt.value
    ,dt.created
    ,dt.modified
  from 
    sistemas.dbo.app_main as app
  inner join 
    sistemas.dbo.app_api_methods as mts
  on
    app.id = mts.app_id
  inner join 
    sistemas.dbo.app_block as blk
  on 
    mts.id = blk.app_api_methods_id
  left join 
    sistemas.dbo.app_api_data as dt
  on 
    blk.id = dt.app_block_id



