-- ============================================================================================================== --
-- Indicadores Operaciones
-- ============================================================================================================== --


-- ============================================================================================================== --
-- Indicador de Rendimiento
-- ============================================================================================================== --
select * from sistemas.dbo.projections_periods
--select * from sistemas.dbo.projections_view_gst_areas

--select * from sistemas.dbo.projections_
--- Esta consulta nos permite obtener los datos generales de los viajes realizados.

select
	tg.no_viaje as "VIAJE",
	pp.nombre as "OPERADOR",
	tg.id_unidad as "TRACTO",
	tg.id_tipo_operacion as "CONFIG",		--- Se debe obtener la descripcion de la tabla "desp_tipooperacion"
	tg.fecha_guia as "FECHA",
	tg.id_origen as "ORIGEN",		--- Se debe obtener la descripcion de la plaza de la tabla "trafico_plaza"
	tg.id_destino as "DESTINO",		--- Se debe obtener la descripcion de la plaza de la tabla "trafico_plaza"
	mu.modelo as "MODELO",
	tv.kms_real as "KMS",
	tv.lts_empresa as "DIESEL"
from
	gstdb.dbo.trafico_guia tg
inner join
	gstdb.dbo.trafico_viaje tv on tg.no_viaje = tv.no_viaje and tg.id_area = tv.id_area
inner join
	gstdb.dbo.mtto_unidades mu on mu.id_unidad = tg.id_unidad
inner join
	gstdb.dbo.personal_personal pp on tg.id_personal = pp.id_personal
where		------- En este bloque se define de que area se obtiene la informacion, asi como el rango de fecha.
--	tg.id_area = 2
--and
	tg.id_origen = tv.id_origen
and
	tg.id_destino = tv.id_destino
and
	tg.fecha_guia BETWEEN '2019-01-01' and '2019-01-31'
and
	tg.status_guia <> 'B'
and
	tv.no_liquidacion is not null
and
	tv.no_viaje = '45984'
-- ============================================================================================================== --
-- Build of Ind
-- ============================================================================================================== --
-- ===================================================================================================================== --
--  This table are the full core for all gst and replace old
-- ===================================================================================================================== --

-- select * from sistemas.dbo.rend_view_full_gst_core_indicators where periodo = 201901 and id_area = 1 and tracto = 'TT1369'
 select fecha_liquidacion,no_viaje,kms_real,status_guia,* from sistemas.dbo.projections_view_full_gst_core_indicators where no_viaje in (54360,56282,56284,56286,56288,57441)

 121105,126638

 select * from sistemas.dbo.rend_view_full_gst_core_indicators
 where viaje in (121105,126638) and area = 'ORIZABA' and year(fecha) = 2019 and month(fecha) = 08

 select id_unidad ,id+id_configuracionviaje, * from gstdb.dbo.trafico_viaje

 select * from gstdb.dbo.trafico_configuracionviaje

use sistemas;
IF OBJECT_ID ('rend_view_full_gst_core_indicators', 'V') IS NOT NULL
    DROP VIEW rend_view_full_gst_core_indicators;
-- now build the view
alter view rend_view_full_gst_core_indicators
with encryption
as -- fraccion
	select
			 row_number() over(order by "rendimiento".no_viaje) as 'id'
			,"rendimiento".no_viaje as 'viaje'
			,"rendimiento".id_area
			,ltrim(rtrim(isnull("rendimiento".ciudad,"rendimiento".area))) as 'area'
			,"rendimiento".operador
			,"rendimiento".cliente -- NEW ADD  Change means Cliente Paga
			,ltrim(rtrim("rendimiento".id_unidad)) as 'tracto'
			,"rendimiento".id_remolque1 as 'rem1'
			,"rendimiento".remolque1 as 'desc_rem1'
			,"rendimiento".id_remolque2 as 'rem2'
			,"rendimiento".remolque2 as 'desc_rem2'
			,case
				when 
					"rendimiento".id_tipo_unidad1 = 5
				then 
					'Sencillo Patona'
				else 
					case 
						when
							"rendimiento".id_remolque2 = '' or "rendimiento".id_remolque2 is null
						then
							'Sencillo'
						else 
							'Full'
					end 
			end as 'Config'
			,"rendimiento".configuracion_viaje as 'ConfigViaje'
			,"rendimiento".id_tipo_operacion
			,case "asignacion".id_seguimiento
				when 1 then 'Cargado'
				when 2 then 'Vacio'
			 else
			 	''
			 end as 'TipoViaje'
			,ltrim(rtrim(replace("rendimiento".tipo_de_operacion,'FLETES' , ''))) as 'tipoOperacion'
			,cast("rendimiento".fecha_liquidacion as date) as 'fecha' -- Change before are fecha_guia
			,ltrim(rtrim("rendimiento".origen)) as 'origen' --
			,ltrim(rtrim("rendimiento".destino)) as 'destino' -- plaza
			,ltrim(rtrim("rendimiento".origen)) + ' - ' + ltrim(rtrim("rendimiento".destino)) as 'route'
			,ltrim(rtrim("rendimiento".modelo)) as 'modelo'
			,cast("rendimiento".kms_real as decimal(18,2)) as 'kms'
			,cast(("rendimiento".lts_empresa + "rendimiento".lts_viaje) as decimal(18,2)) as 'diesel'
			,case
				when
					("rendimiento".lts_empresa + "rendimiento".lts_viaje) = 0.00
				then
					0
			 else
					cast( ("rendimiento".kms_real / ("rendimiento".lts_empresa + "rendimiento".lts_viaje) ) as decimal(18,2))
			 	end
			 as 'rendimiento'
			,"rendimiento".rendimiento_reseteo  -- NEW ADD  Change
			,"rendimiento".rendimiento_real  -- NEW ADD  Change
			,"rendimiento".trip_count as 'viajes'
			,"rendimiento".fecha_guia_m as 'periodo'
			,"rendimiento".mes
			,year("rendimiento".fecha_guia) as 'cyear'
			,'' as 'monto_comb' --"combustible".monto_comb --subtotal, costo diesel
			,'' 'monto_precio' --"combustible".monto_precio
			,'' as 'cantidad_comb' --"combustible".cantidad_comb
			,'' as 'observaciones' -- "combustible".observaciones
	from
			sistemas.dbo.projections_view_full_gst_core_indicators as "rendimiento"
		inner join
			gstdb.dbo.desp_asignacion as "asignacion"
		on
			"rendimiento".id_area = "asignacion".id_area
		and
			"rendimiento".no_viaje = "asignacion".no_viaje
--		inner join
--			gstdb.dbo.trafico_combustible as "combustible"
--		on
--			"rendimiento".id_area = "combustible".id_area
--		and
--			"asignacion".no_viaje = "combustible".no_viaje
--		and
--			"asignacion".id_asignacion = "combustible".id_asignacion
	where
			"rendimiento".no_liquidacion is not null
		and
			"rendimiento".trip_count = 1
--		and
--			"rendimiento".tipo_doc = 2
		and
			(
				"rendimiento".status_guia <> 'B'
			or
				"rendimiento".status_guia is null
			)
--		and
--			"rendimiento".no_viaje = 19476
--		and ltrim(rtrim("rendimiento".ciudad)) = 'Hermosillo'
--		and cast("rendimiento".fecha_liquidacion as date) between '2019-01-01' and '2019-06-30'
		and
			"rendimiento".no_viaje in (19640,19476)




	select id_area,area,ciudad from sistemas.dbo.projections_view_full_gst_core_indicators where no_viaje = 53100

	select no_viaje,num_guia,status_asignacion,* from sistemas.dbo.projections_view_full_gst_core_indicators where num_guia is null and no_liquidacion is not null
	and no_viaje = 19640

	select * from sistemas.dbo.rend_view_full_gst_core_indicators where viaje = 19476 and status_guia <> 'B'

	select * from sistemas.dbo.rend_view_full_gst_core_indicators where viaje = 19640

	select status_guia,tipo_doc,* from sistemas.dbo.projections_view_full_gst_core_indicators where no_viaje = 19640--19476

--  ============================================================================================================== --



		--TODO for indicator
SELECT
 tv.id_area,
 tv.id_unidad,
 tv.id_remolque1,
 tv.id_remolque2,
 mtu1.descripcion as 'TipoUnidadRem1',
 mtu2.descripcion as 'TipoUnidadRem2'
FROM
 gstdb.dbo.trafico_viaje tv
INNER JOIN  gstdb.dbo.mtto_unidades mu1 on
 tv.id_remolque1 = mu1.id_unidad
INNER JOIN  gstdb.dbo.mtto_tipos_unidades mtu1 on
 mu1.id_tipo_unidad = mtu1.id_tipo_unidad
LEFT JOIN gstdb.dbo.mtto_unidades mu2 on
 tv.id_remolque2 = mu2.id_unidad
LEFT JOIN gstdb.dbo.mtto_tipos_unidades mtu2 on
 mu2.id_tipo_unidad = mtu2.id_tipo_unidad
WHERE
 tv.id_area = 7

	
	
 select
        "viaje".no_viaje
	   ,"viaje".id_area
	   ,"asig".id_asignacion
	   ,"combustible".monto_comb
	   ,"combustible".monto_precio
  from
       gstdb.dbo.trafico_viaje as "viaje"
  inner join
       gstdb.dbo.desp_asignacion as "asig"
  on
       "viaje".id_area = "asig".id_area
  and
       "viaje".no_viaje = "asig".no_viaje
  inner join
       gstdb.dbo.trafico_combustible as "combustible"
  on
       "viaje".id_area = "combustible".id_area
  and
       "asig".id_asignacion = "combustible".id_asignacion
  where
       "viaje".no_viaje = 51442;


cliente que paga = id_cliente en trafico guia

select top 100 id_cliente,cliente,rendimiento_reseteo,* from sistemas.dbo.projections_view_full_gst_core_indicators


select top 100 rendimiento_real,rendimiento_reseteo,rendimiento_viaje from gstdb.dbo.trafico_viaje


--rendimiento_real --> rendimiento_reseteo ,rendimiento_real [trafico_viaje]


trafico_viaje -> desp_asignacion -> trafico_combustible
no_viaje,id_area  -> id_asignacion,id_area,no_viaje ->  id_area,id_asignacion


select top 100 id from gstdb.dbo.trafico_viaje

select id_asignacion from gstdb.dbo.trafico_viaje where no_viaje = 51442 and id_area = 1





--get diesel costs
 Search by id_asignacion




 select
	monto_comb, --subtotal, costo diesel
	monto_precio,
	cantidad_comb,
	no_viaje,
	id_asignacion,
	*
from
gstdb.dbo.trafico_combustible
where
no_consecutivo = '049002' --

-- ============================================================================================================== --
-- WARNING NOTE ALERT Indicador de Disponibilidad
-- ============================================================================================================== --

--- Consulta de disponibilidad

Select
mu.id_unidad as "UNIDAD",
pp.nombre as "OPERADOR",
tk.id_rem1 as "REMOLQUE",
mu.id_status as "ESTATUS",				---- Se obtiene la descripcion de la tabla "desp_status"
mu.id_tipo_operacion as "SEGMENTO"		---- Se obtine la descripcion de la tabla "desp_tipooperacion"
From
gstdb.dbo.mtto_unidades mu
inner join
gstdb.dbo.trafico_kit_unidad tk on mu.id_unidad = tk.unidad_motriz
inner join
gstdb.dbo.personal_personal pp on mu.id_operador = pp.id_personal
where
	mu.id_area= 2

-- ============================================================================================================== --
-- Build of
-- ============================================================================================================== --

use sistemas -- INCOMPLETE
-- select * from sistemas.dbo.disponibilidad_view_unidades_gst_indicators where area = 'Macuspana'
IF OBJECT_ID ('disponibilidad_view_unidades_gst_indicators', 'V') IS NOT NULL
    DROP VIEW disponibilidad_view_unidades_gst_indicators;
-- now build the view
alter view disponibilidad_view_unidades_gst_indicators
with encryption
as
-- with "unit" as (
select
		 "unidades".id_unidad as 'unidad'
		,"unidades".id_status as 'idestatus'
		,"status".nombre as 'estatus'
		,"status".tipo_status
--		,"unidades".id_tipo_operacion as 'segmento'
		,"personal".nombre as 'operador'
		,"kit".id_rem1 as 'remolque'
		,"area".ciudad as 'area'
		,ltrim(rtrim(replace("operacion".tipo_operacion,'FLETES' , ''))) as 'segmento'
from
		gstdb.dbo.mtto_unidades	as "unidades"
inner join
		gstdb.dbo.personal_personal as "personal"
	on
		"unidades".id_operador = "personal".id_personal
inner join
		gstdb.dbo.general_area as "area"
	on
		"unidades".id_area = "area".id_area and "area".id_area <> 7 -- NOTE workarround to drop macuca
inner join
		gstdb.dbo.desp_tipooperacion as "operacion"
	on
		"unidades".id_tipo_operacion = "operacion".id_tipo_operacion
inner join
		gstdb.dbo.desp_status as "status"
	on
		"unidades".id_status = "status".id_status
left join
		gstdb.dbo.trafico_kit_unidad as "kit"
	on
		"unidades".id_unidad = "kit".unidad_motriz
where
		"unidades".id_tipo_unidad = 1
--)



-- select * from "unit" where "unit".area = 'Orizaba'
-- =================================================================================================== --
-- =============  Build of store table for Description and Compromise Date
-- =================================================================================================== --

-- select * from
-- truncate
-- table
--
select * from sistemas.dbo.disponibilidad_tbl_unidades_gst_indicators

use [sistemas]
-- select db_name()
-- go
IF OBJECT_ID('sistemas.dbo.disponibilidad_tbl_unidades_gst_indicators', 'U') IS NOT NULL
  DROP TABLE sistemas.dbo.disponibilidad_tbl_unidades_gst_indicators
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on
-- NOTE rebuild table for add data from 
create table disponibilidad_tbl_unidades_gst_indicators(
		 id							int					identity(1,1)
		,unidad					char(6)			null
		,id_status			int					null
		,user_id				int					null
		,id_mtto_clone	bigint			not null
		,description		text				null
		,compromise			date				null
		,created				datetime		null
		,modified				datetime		null
		,status					tinyint 		default 1 null
--		,primary key (id_mtto_clone)
) on [primary]
-- go
set ansi_padding off
-- go
-- test


insert into sistemas.dbo.disponibilidad_tbl_unidades_gst_indicators
	select
			'TT1175',3,null,311,'en XXXcorralon',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1

select * from sistemas.dbo.disponibilidad_tbl_unidades_gst_indicators
-- ======================================================================================================== --
-- build of disponibilidad_view_join_unidades_gst_indicators
-- build an historical as view
-- ======================================================================================================== --
use sistemas
-- select * from sistemas.dbo.disponibilidad_view_id_unidades_gst_indicators
-- join unidades description
IF OBJECT_ID ('disponibilidad_view_id_unidades_gst_indicators', 'V') IS NOT NULL
    DROP VIEW disponibilidad_view_id_unidades_gst_indicators;
-- now build the view
--create 
alter 
view disponibilidad_view_id_unidades_gst_indicators
with encryption
as
	select
		 max("compromise".id) as 'id'
		,"compromise".unidad
	from
		sistemas.dbo.disponibilidad_tbl_unidades_gst_indicators as "compromise"
	group by
		"compromise".unidad

-- ======================================================================================================== --
-- ============================    Mtto Recepcion     ===================================================== --
-- ======================================================================================================== --
-- select fecha,id_unidad,* from gstdb.dbo.mtto_recepcion where id_unidad = 'TT1221'

use sistemas
IF OBJECT_ID ('disponibilidad_view_id_unidades_receptions', 'V') IS NOT NULL
    DROP VIEW disponibilidad_view_id_unidades_receptions;
-- now build the view
create 
--alter 
view disponibilidad_view_id_unidades_receptions
with encryption
as
--with "reception" as (

			select 
						max(fecha) as 'fecha',id_unidad
			from 
						gstdb.dbo.mtto_recepcion
			group by 
						id_unidad

--)
--select * from "reception" where id_unidad in ('TT1219','TT1221')













-- ======================================================================================================== --

use sistemas
-- check unidades con viaje
-- select * from sistemas.dbo.disponibilidad_view_viajes_gst_indicators
IF OBJECT_ID ('disponibilidad_view_viajes_gst_indicators', 'V') IS NOT NULL
    DROP VIEW disponibilidad_view_viajes_gst_indicators;
-- now build the view
--create 
alter 
view disponibilidad_view_viajes_gst_indicators
with encryption
as
select
		 "trajico".id_unidad
		,"trajico".viajeactual as 'status_viaje'
		,'unidad en viaje' as 'desc_viaje'
from
		gstdb.dbo.trafico_viaje  as "trajico"
where
		viajeactual = 'S'
group by
		id_unidad,viajeactual

-- ======================================================================================================== --

use sistemas
-- check unidades con viaje

select count(id_unidad) as 'unidades',count(id_orden) as 'orden',id_area,status_taller from sistemas.dbo.disponibilidad_view_taller_gst_indicators
group by id_area,status_taller

select count(id_unidad) from sistemas.dbo.disponibilidad_view_taller_gst_indicators
select * from sistemas.dbo.disponibilidad_view_taller_gst_indicators

select * from gstdb.dbo.mtto_orden
--========================================================================================================= --

IF OBJECT_ID ('disponibilidad_view_taller_gst_indicators', 'V') IS NOT NULL
    DROP VIEW disponibilidad_view_taller_gst_indicators;
-- now build the view
--create
alter view disponibilidad_view_taller_gst_indicators
with encryption
as
select
		 "ord".id_area
		,"ord".id_orden
		,"unit".id_unidad
		-- NOTE Add fields to 
		,"ord".id_razon
		,"ord".fecha_ingreso
		,"razon".descripcion as 'desc_taller'
		-- NOTE Fields added ...
		,"ord".estado as 'status_taller'
--		,'Unidad en Mantenimiento' as 'desc_taller'
		,"ord".fecha_inicio
		,"ord".fecha_prometida
from
		gstdb.dbo.mtto_orden as "ord"
inner join
		gstdb.dbo.mtto_unidades as "unit"
	on
--		"unit".id_tipo_unidad in (1,13)
--	and
		"unit".id_unidad = "ord".id_unidad
inner join 
		gstdb.dbo.mtto_razon_reparacion as "razon"
	on 
		"ord".id_razon = "razon".id_razon
where
		"ord".estado = 'A' and "ord".id_area <> 7 -- NOTE macuca work

--==================================================
-- TEST
select 
			count(id_unidad),status_taller,id_area 
from 
		  sistemas.dbo.disponibilidad_view_taller_gst_indicators
group by 
			status_taller,id_area 


-- ======================================================================================================== --
use sistemas
-- check unidades con viaje
--(4,6,8,15,14,18,11)
select * from sistemas.dbo.disponibilidad_view_status_gst_indicators

IF OBJECT_ID ('disponibilidad_view_status_gst_indicators', 'V') IS NOT NULL
    DROP VIEW disponibilidad_view_status_gst_indicators;
-- now build the view
--create 
alter 
view disponibilidad_view_status_gst_indicators
with encryption
as
select
		"status".id_status,"status".nombre
from
		gstdb.dbo.desp_status as "status"


-- ======================================================================================================== --
-- Status equivaken
-- ======================================================================================================== --
use [sistemas]
IF OBJECT_ID('sistemas.dbo.disponibilidad_tbl_unidades_groups', 'U') IS NOT NULL
  DROP TABLE sistemas.dbo.disponibilidad_tbl_unidades_groups
set ansi_nulls on
set quoted_identifier on
set ansi_padding on
create table disponibilidad_tbl_unidades_groups(
		 id					int 			identity(1,1)
		,group_name			varchar(100) null					
		,description		text 			null
		,created			datetime		null
		,modified			datetime		null
		,status				tinyint 		default 1 null
) on [primary]
set ansi_padding off

-- truncate table sistemas.dbo.disponibilidad_tbl_unidades_groups

insert into sistemas.dbo.disponibilidad_tbl_unidades_groups values 
 ('Disponibilidad','Disponibilidad de unidades',current_timestamp,current_timestamp,1)
,('Taller','Unidades en Taller',current_timestamp,current_timestamp,1)
,('Gestoria','Unidades en Gestoria',current_timestamp,current_timestamp,1)
,('Siniestrado','Unidades Siniestradas',current_timestamp,current_timestamp,1)
,('Robo','Unidades Robadas',current_timestamp,current_timestamp,1)
-- ======================================================================================================== --
-- Status equivaken
-- ======================================================================================================== --
use [sistemas]
IF OBJECT_ID('sistemas.dbo.disponibilidad_tbl_unidades_clasifications', 'U') IS NOT NULL
  DROP TABLE sistemas.dbo.disponibilidad_tbl_unidades_clasifications
set ansi_nulls on
set quoted_identifier on
set ansi_padding on
create table disponibilidad_tbl_unidades_clasifications(
		 id																						int								identity(1,1)
		,id_disponibilidad_tbl_unidades_groups				int								null
		,clasification_name														varchar(100)			null
		,is_sum																				tinyint						default 1 null
		,description																	text							null
		,created																			datetime					null
		,modified																			datetime					null
		,status																				tinyint						default 1 null
) on [primary]
set ansi_padding off

--truncate table sistemas.dbo.disponibilidad_tbl_unidades_clasifications

insert into sistemas.dbo.disponibilidad_tbl_unidades_clasifications values 
		 (1,'Operando',1,'Unidades Operando',current_timestamp,current_timestamp,1)									-- 1	
		,(2,'Taller',1,'Unidades en Taller',current_timestamp,current_timestamp,1)									-- 2
		,(1,'Disponible',1,'Unidades Disponibles',current_timestamp,current_timestamp,1)						-- 3
		,(4,'Siniestrado',1,'Unidades Siniestradas',current_timestamp,current_timestamp,1)					-- 4
		,(null,'En Exhibicion',0,'Unidades en Exhibicion',current_timestamp,current_timestamp,1)		-- 5
		,(3,'Gestoria',1,'Unidades en Gestoria',current_timestamp,current_timestamp,1)							-- 6
		,(5,'Robo',0,'Unidades Robadas',current_timestamp,current_timestamp,1)											-- 7
		,(null,'En Venta',0,'Unidades en Venta',current_timestamp,current_timestamp,1)							-- 8

-- ======================================================================================================== --
-- Status equivaken
-- ======================================================================================================== --
use [sistemas]
IF OBJECT_ID('sistemas.dbo.disponibilidad_tbl_unidades_joins', 'U') IS NOT NULL
  DROP TABLE sistemas.dbo.disponibilidad_tbl_unidades_joins
set ansi_nulls on
set quoted_identifier on
set ansi_padding on
create table sistemas.dbo.disponibilidad_tbl_unidades_joins(
		 id																						int								identity(1,1)
		,id_desp_status																int								null
--		,id_disponibilidad_tbl_unidades_groups				int								null
		,id_disponibilidad_tbl_unidades_clasifications int							null
		,description																	text							null
		,created																			datetime					null
		,modified																			datetime					null
		,status																				tinyint						default 1 null
) on [primary]
set ansi_padding off

truncate table sistemas.dbo.disponibilidad_tbl_unidades_joins

insert into sistemas.dbo.disponibilidad_tbl_unidades_joins values 
	 (1,1,'',current_timestamp,current_timestamp,1)
	,(2,1,'',current_timestamp,current_timestamp,1)
	,(3,1,'',current_timestamp,current_timestamp,1)
	,(4,2,'',current_timestamp,current_timestamp,1)
	,(5,1,'',current_timestamp,current_timestamp,1)
	,(6,3,'',current_timestamp,current_timestamp,1)
	,(7,3,'',current_timestamp,current_timestamp,1)
	,(8,4,'',current_timestamp,current_timestamp,1)
	,(9,5,'',current_timestamp,current_timestamp,1)
	,(10,null,'',current_timestamp,current_timestamp,1)
	,(11,6,'',current_timestamp,current_timestamp,1)
	,(12,null,'',current_timestamp,current_timestamp,1)
	,(13,7,'',current_timestamp,current_timestamp,1)
	,(14,8,'',current_timestamp,current_timestamp,1)
	,(15,6,'',current_timestamp,current_timestamp,1)
	,(16,1,'',current_timestamp,current_timestamp,1)
	,(17,1,'',current_timestamp,current_timestamp,1)
	,(18,2,'',current_timestamp,current_timestamp,1)


select *  from sistemas.dbo.disponibilidad_tbl_unidades_groups
select *  from sistemas.dbo.disponibilidad_tbl_unidades_clasifications
select *  from sistemas.dbo.disponibilidad_tbl_unidades_joins
select * from gstdb.dbo.desp_status
-- ======================================================================================================== --
-- NOTE Status Equivalents Main
-- ======================================================================================================== --
-- NOTE The joined view
select * from sistemas.dbo.disponibilidad_view_unidades_class
IF OBJECT_ID ('disponibilidad_view_unidades_class', 'V') IS NOT NULL
    DROP VIEW disponibilidad_view_unidades_class;
-- now build the view
create view disponibilidad_view_unidades_class
with encryption
as
select 
			id_status,nombre--,tipo_status
--			,"main".*
			,"class".clasification_name
			,"grps".group_name
from 
			gstdb.dbo.desp_status as "status"
left join 
			sistemas.dbo.disponibilidad_tbl_unidades_joins as "main"
		on 
			"main".id_desp_status = "status".id_status
left join 
			sistemas.dbo.disponibilidad_tbl_unidades_clasifications as "class"
		on 
			"main".id_disponibilidad_tbl_unidades_clasifications = "class".id
left join 
			sistemas.dbo.disponibilidad_tbl_unidades_groups as "grps"
		on 
			"class".id_disponibilidad_tbl_unidades_groups = "grps".id
where 
		"status".id_status <> 0
--	and 
--		"main".[status] = 1 and "class".[status] = 1 and "grps".[status] = 1


--		$units_id = array(
--				 1=>array(1,13)			//Tractocamiones
--				,2=>array(2,3,5,6,7,8,9)				//remolques
--				,3=>array(1,2,3,4,5,6,7,8,9,13)		//ALL
--				,4=>array(4)											//only dollys
--		);

use [sistemas]
-- go
select * from sistemas.dbo.disponibilidad_tbl_unit_types_relations

IF OBJECT_ID('sistemas.dbo.disponibilidad_tbl_unit_types_relations', 'U') IS NOT NULL
  DROP TABLE sistemas.dbo.disponibilidad_tbl_unit_types_relations
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on
-- go
create table disponibilidad_tbl_unit_types_relations(
		 id					int 			identity(1,1)
		,types			int				null
		,operacion int null 
		,description		text 			null
		,created			datetime		null
		,modified			datetime		null
		,status				tinyint 		default 1 null
) on [primary]
-- go
set ansi_padding off
-- truncate table  sistemas.dbo.disponibilidad_tbl_unit_types_relations
insert into sistemas.dbo.disponibilidad_tbl_unit_types_relations values
	 (1,1,'Tractocamion',current_timestamp,current_timestamp,1)
	,(1,13,'Tractocamion',current_timestamp,current_timestamp,1)
	,(2,2,'Remolque',current_timestamp,current_timestamp,1)
	,(2,3,'Remolque',current_timestamp,current_timestamp,1)
	,(2,5,'Remolque',current_timestamp,current_timestamp,1)
	,(2,6,'Remolque',current_timestamp,current_timestamp,1)
	,(2,7,'Remolque',current_timestamp,current_timestamp,1)
	,(2,8,'Remolque',current_timestamp,current_timestamp,1)
	,(2,9,'Remolque',current_timestamp,current_timestamp,1)
--	,(3,1,'All',current_timestamp,current_timestamp,1)
--	,(3,2,'All',current_timestamp,current_timestamp,1)
--	,(3,3,'All',current_timestamp,current_timestamp,1)
--	,(3,4,'All',current_timestamp,current_timestamp,1)
--	,(3,5,'All',current_timestamp,current_timestamp,1)
--	,(3,6,'All',current_timestamp,current_timestamp,1)
--	,(3,7,'All',current_timestamp,current_timestamp,1)
--	,(3,8,'All',current_timestamp,current_timestamp,1)
--	,(3,9,'All',current_timestamp,current_timestamp,1)
--	,(3,13,'All',current_timestamp,current_timestamp,1)
	,(4,4,'Dolly',current_timestamp,current_timestamp,1)



use [sistemas]
-- go
IF OBJECT_ID('sistemas.dbo.disponibilidad_tbl_unidades_gst_indicators', 'U') IS NOT NULL
  DROP TABLE sistemas.dbo.disponibilidad_tbl_unidades_gst_indicators
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on
-- go
create table disponibilidad_tbl_unidades_gst_indicators(
		 id					int 			identity(1,1)
		,id_status			int				null
		,clasificacion  
		,description		text 			null
		,created			datetime		null
		,modified			datetime		null
		,status				tinyint 		default 1 null
) on [primary]
-- go
set ansi_padding off



-- ======================================================================================================== --
-- Principal view
-- ======================================================================================================== --
-- NOTE ADD join to test table as mtto_unidades id_status and estatus fields
-- ======================================================================================================== --
select * from gstdb.dbo.desp_flotas
-- select * from sistemas.dbo.disponibilidad_view_unidades_gst_indicators where area = 'Orizaba' and segmento = 'GRANEL'
select * from gstdb.dbo.mtto_unidades where id_tipo_unidad in (1,13)
-- NOTE This must change for productive -- and trigger disponibilidad_update_unidad_status
use sistemas
--
select  count(unidad) as 'unidades',id_tipo_operacion,segmento,id_flota,flota--,unidad 
from sistemas.dbo.disponibilidad_view_rpt_unidades_gst_indicators 
group by id_tipo_operacion,id_flota,segmento,flota--,unidad
order by id_flota

-- ======================================================================================================== --
-- ======================================================================================================== --
-- NOTE Add new menu 
-- ======================================================================================================== --
IF OBJECT_ID ('disponibilidad_view_menu_operations', 'V') IS NOT NULL
    DROP VIEW disponibilidad_view_menu_operations;
-- now build the view
alter view disponibilidad_view_menu_operations
with encryption
as
select
		id_tipo_operacion
	 ,case
		when (id_tipo_operacion in (1,2,6))
				then 'GRANEL.'
				else 'OTROS'
		end as 'Operation'
		,replace(tipo_operacion, 'FLETES ','') as 'tipoOperacion'
from 
		gstdb.dbo.desp_tipooperacion
where 
		id_tipo_operacion <> 0

select * from sistemas.dbo.disponibilidad_view_menu_operations

select * from gstdb.dbo.trafico_producto where id_producto = 0 
-- ======================================================================================================== --
-- NOTE Historical Add on
-- ======================================================================================================== --
-- Proccess for add historical functionality
select  
		*
into 
	sistemas.dbo.diponibilidad_view_tmphist_unidades_gst_indicators
from 
	sistemas.dbo.disponibilidad_view_rpt_unidades_gst_indicators --where id_area = 7



-- ======================================================================================================= --
-- NOTE Control views
-- ======================================================================================================= --
-- -------------------------------------------------------------------------------------------------- --
--  Get days in period 
-- -------------------------------------------------------------------------------------------------- --
 select * from sistemas.dbo.disponibilidad_main_view_calculate_days

IF OBJECT_ID ('disponibilidad_view_calculate_days', 'V') IS NOT NULL
    DROP VIEW disponibilidad_view_calculate_days;
-- now build the view
alter
create 
view disponibilidad_main_view_calculate_days
with encryption
as
	select 
				 1 as 'day'
				--,count(unidad) as "counting_rows" 
				,created 
	from 
				sistemas.dbo.disponibilidad_main_view_rpt_unidades_gst_indicators 
--	where 
--				created between '2021-08-04' and '2021-08-27'
group by created





   -- sistemas.dbo.diponibilidad_view_tmphist_unidades_gst_indicators definition

	 -- Drop table

	 -- DROP TABLE sistemas.dbo.diponibilidad_view_tmphist_unidades_gst_indicators;

use [sistemas]
-- go
IF OBJECT_ID('sistemas.dbo.disponibilidad_tbl_hist_unidades_gst_indicators', 'U') IS NOT NULL print 'there is it'
  DROP TABLE sistemas.dbo.disponibilidad_tbl_hist_unidades_gst_indicators
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on

create table "sistemas"."dbo"."disponibilidad_tbl_hist_unidades_gst_indicators" (
							id bigint identity(1,1),
							unidad varchar(10) COLLATE Modern_Spanish_CI_AS NOT NULL,
							id_flota int NULL,
							flota varchar(40) COLLATE Modern_Spanish_CI_AS NULL,
							xid_status varchar(1) COLLATE Modern_Spanish_CI_AS NULL,
							xestatus varchar(1) COLLATE Modern_Spanish_CI_AS NULL,
							id_status int NULL,
							estatus varchar(20) COLLATE Modern_Spanish_CI_AS NULL,
							tipo_status char(1) COLLATE Modern_Spanish_CI_AS NULL,
							clasification_name varchar(100) COLLATE Modern_Spanish_CI_AS NULL,
							group_name varchar(100) COLLATE Modern_Spanish_CI_AS NULL,
							operador varchar(40) COLLATE Modern_Spanish_CI_AS NULL,
							remolque varchar(25) COLLATE Modern_Spanish_CI_AS NULL,
							id_area int NULL,
							area varchar(100) COLLATE Modern_Spanish_CI_AS NULL,
							id_tipo_operacion int NULL,
							segmento varchar(8000) COLLATE Modern_Spanish_CI_AS NULL,
							description text COLLATE Modern_Spanish_CI_AS NULL,
							compromise date NULL,
							status_viaje char(1) COLLATE Modern_Spanish_CI_AS NULL,
							desc_viaje varchar(150) COLLATE Modern_Spanish_CI_AS NULL,
							status_taller char(1) COLLATE Modern_Spanish_CI_AS NULL,
							desc_taller varchar(250) COLLATE Modern_Spanish_CI_AS NULL,
							units_type int NULL,
							iseditable int NULL,
							id_area_taller int NULL,
							area_taller varchar(100) COLLATE Modern_Spanish_CI_AS NULL,
							id_orden int NULL,
							fecha_inicio datetime NULL,
							fecha_ingreso datetime NULL,
							fecha_prometida datetime NULL,
		--NOTE Added trafico_viaje
							lastTrip int null,
							id_remolque1  varchar(20) COLLATE Modern_Spanish_CI_AS NULL,
							id_remolque2 varchar(20) COLLATE Modern_Spanish_CI_AS null,
							id_dolly varchar(10) COLLATE Modern_Spanish_CI_AS null,
							f_despachado datetime null,
							StatusDays varchar(25) COLLATE Modern_Spanish_CI_AS null,
		--NOTE Added trafico_viaje
							created datetime NULL,
							status tinyint null
	) on [primary]      						
set ansi_padding off


-- -------------------------------------------------------------------------------------------------- --
-- NOTE Testing this going to become in a job 
-- -------------------------------------------------------------------------------------------------- --

insert into 
	sistemas.dbo.disponibilidad_tbl_hist_unidades_gst_indicators
	select
				 unidad, id_flota, flota, xid_status, xestatus, id_status, estatus, tipo_status, clasification_name
				,group_name, operador, remolque, id_area, area, id_tipo_operacion, segmento, description, compromise
				,status_viaje, desc_viaje, status_taller, desc_taller, units_type, 0 --as 'iseditable'
				, id_area_taller, area_taller
				,id_orden, fecha_inicio,fecha_ingreso, fecha_prometida
				,lastTrip
				,id_remolque1
				,id_remolque2
				,id_dolly
				,f_despachado
				,StatusDays
				,'2021-08-06' --created
				,1 -- status
	from
--				sistemas.dbo.disponibilidad_main_view_rpt_unidades_gst_indicators
				sistemas.dbo.disponibilidad_currentview_rpt_unidades_gst_indicators
	where 
				created =  convert(nvarchar(max),current_timestamp,23) 




-- UPDATE

update sistemas.dbo.disponibilidad_tbl_hist_unidades_gst_indicators
set 
		id_status = 1 , estatus = 'OPERANDO' , clasification_name = 'Operando' , group_name = 'Disponibilidad'
where 
		created = '2021-08-04'



select count(unidad) as 'units' , created from sistemas.dbo.disponibilidad_tbl_hist_unidades_gst_indicators
group by created

exec sp_columns disponibilidad_main_view_rpt_unidades_gst_indicators
-- -------------------------------------------------------------------------------------------------- --
--  This become in the new main view 
-- -------------------------------------------------------------------------------------------------- --

-- drop main view 
-- join xview and tbl_his as main view 

IF OBJECT_ID ('disponibilidad_main_view_rpt_unidades_gst_indicators', 'V') IS NOT NULL
    DROP VIEW disponibilidad_main_view_rpt_unidades_gst_indicators;
-- now build the view
alter
--create 
view disponibilidad_main_view_rpt_unidades_gst_indicators
with encryption
as
-- NOTE start the new view 
with "main_view" as (
select 
				"current".unidad ,
				"current".id_flota ,
				"current".flota ,
				"current".xid_status ,
				"current".xestatus ,
				"current".id_status ,
				"current".estatus ,
				"current".tipo_status ,
				"current".clasification_name ,
				"current".group_name ,
				"current".operador ,
				"current".remolque ,
				"current".id_area ,
				"current".area ,
				"current".id_tipo_operacion ,
				"current".segmento ,
				"current".description ,
				"current".compromise ,
				"current".status_viaje ,
				"current".desc_viaje ,
				"current".status_taller ,
				"current".desc_taller ,
				"current".units_type ,
				"current".iseditable ,
				"current".id_area_taller ,
				"current".area_taller ,
				"current".id_orden ,
				"current".fecha_inicio ,
			  "current".fecha_ingreso ,
				"current".fecha_prometida ,
				"current".lastTrip,
				"current".id_remolque1,
				"current".id_remolque2,
				"current".id_dolly,
				"current".f_despachado,
				"current".StatusDays,
				"current".created 
from 
				sistemas.dbo.disponibilidad_currentview_rpt_unidades_gst_indicators as "current"
		union all 
select 
				"historical".unidad ,
				"historical".id_flota ,
				"historical".flota ,
				"historical".xid_status ,
				"historical".xestatus ,
				"historical".id_status ,
				"historical".estatus ,
				"historical".tipo_status ,
				"historical".clasification_name ,
				"historical".group_name ,
				"historical".operador ,
				"historical".remolque ,
				"historical".id_area ,
				"historical".area ,
				"historical".id_tipo_operacion ,
				"historical".segmento ,
				"historical".description ,
				"historical".compromise ,
				"historical".status_viaje ,
				"historical".desc_viaje ,
				"historical".status_taller ,
				"historical".desc_taller ,
				"historical".units_type ,
				"historical".iseditable ,
				"historical".id_area_taller ,
				"historical".area_taller ,
				"historical".id_orden ,
				"historical".fecha_inicio ,
				"historical".fecha_ingreso ,
				"historical".fecha_prometida ,
				"historical".lastTrip,
				"historical".id_remolque1,
				"historical".id_remolque2,
				"historical".id_dolly,
				"historical".f_despachado,
				"historical".StatusDays,
				"historical".created 
from 
				sistemas.dbo.disponibilidad_tbl_hist_unidades_gst_indicators as "historical"
)
select 
				"main_view".unidad ,
				"main_view".id_flota ,
				"main_view".flota ,
				"main_view".xid_status ,
				"main_view".xestatus ,
				"main_view".id_status ,
				"main_view".estatus ,
				"main_view".tipo_status ,
				"main_view".clasification_name ,
				"main_view".group_name ,
				"main_view".operador ,
				"main_view".remolque ,
				"main_view".id_area ,
				"main_view".area ,
				"main_view".id_tipo_operacion ,
				"main_view".segmento ,
				"main_view".description ,
				"main_view".compromise ,
				"main_view".status_viaje ,
				"main_view".desc_viaje ,
				"main_view".status_taller ,
				"main_view".desc_taller ,
				"main_view".units_type ,
				"main_view".iseditable ,
				"main_view".id_area_taller ,
				"main_view".area_taller ,
				"main_view".id_orden ,
				"main_view".fecha_inicio ,
				"main_view".fecha_ingreso ,
				"main_view".fecha_prometida ,
				"main_view".lastTrip,
				"main_view".id_remolque1,
				"main_view".id_remolque2,
				"main_view".id_dolly,
				"main_view".f_despachado,
				"main_view".StatusDays,
				"main_view".created 
from
				"main_view"




select 
--			top 10 
			count("test".unidad) as 'unidades',convert(nvarchar(max),"test".created,23) as 'xcreated',created
from 
			sistemas.dbo.disponibilidad_main_view_rpt_unidades_gst_indicators as "test" --where created = '2021-08-16'
group by 
			created

select created,desc_taller,fecha_ingreso,* from sistemas.dbo.disponibilidad_main_view_rpt_unidades_gst_indicators where unidad = 'RD1531' --and created = '2021-07-02'

--select * from sistemas.dbo.providers_controls_files
-- -------------------------------------------------------------------------------------------------- --
--NOTE Working area
select top 10 created, * from sistemas.dbo.disponibilidad_view_rpt_unidades_gst_indicators

select top 100  convert(nvarchar(max),created,23) as 'xcreated',created,* from sistemas.dbo.disponibilidad_tbl_hist_unidades_gst_indicators -- remenber join and check the date data 

select * from sistemas.dbo.disponibilidad_main_view_rpt_unidades_gst_indicators where unidad = 'RD1011' and created between '2021-06-26' and '2021-08-25'





select * from gstdb.dbo.mtto_unidades where id_unidad = 'TT1201'


select id_orden,id_area,id_orden,fecha_prometida,* from sistemas.dbo.disponibilidad_view_rpt_unidades_gst_indicators where unidad = 'TT1201'

select 
				id_orden,id_area,fecha_prometida,fecha_ingreso,remolque,id_remolque1,id_remolque2,id_dolly,lastTrip,StatusDays,created 
	from 
				sistemas.dbo.disponibilidad_currentview_rpt_unidades_gst_indicators where unidad = 'TT1254'

select 
			"orden".id_orden,"orden".id_razon,"orden".id_area,"orden".fecha_ingreso,"orden".fecha_prometida
			,"razon".descripcion,"razon".preventivo
from 
			gstdb.dbo.mtto_orden as "orden"
	inner join 
			gstdb.dbo.mtto_razon_reparacion as "razon"
		on 
		"orden".id_razon  = "razon".id_razon	
where 
		"orden".id_unidad = 'TT1201' and "orden".id_orden = 1984 and "orden".id_area = 5


select * from gstdb.dbo.mtto_razon_reparacion where id_razon = 2

select * from sistemas.dbo.disponibilidad_view_taller_gst_indicators where id_orden = '1984'







-- -------------------------------------------------------------------------------------------------- --



-- -------------------------------------------------------------------------------------------------- --
-- NOTE Principal View 
-- -------------------------------------------------------------------------------------------------- --
/*select count(unidad) from sistemas.dbo.disponibilidad_currentview_rpt_unidades_gst_indicators

select 
			unidad,id_area,fecha_recepcion,lastTrip,StatusDays,* from sistemas.dbo.disponibilidad_currentview_rpt_unidades_gst_indicators 
where 
			--unidad in ('TT1219','TT1221')
			id_status in (4)
*/

IF OBJECT_ID ('disponibilidad_view_rpt_unidades_gst_indicators', 'V') IS NOT NULL
    DROP VIEW disponibilidad_view_rpt_unidades_gst_indicators;
-- now build the view
alter
--create
view disponibilidad_currentview_rpt_unidades_gst_indicators
with encryption
as
select 
		 "unidades".id_unidad as 'unidad'
		,"flotas".id_flota
		,"flotas".nombre as 'flota'
		,'' as 'xid_status'
		,'' as 'xestatus'
		,"unidades".id_status as 'id_status'
		,"status".nombre as 'estatus'
		,"status".tipo_status
		,"class".clasification_name
		,"class".group_name
-- NOTE Add Status Section

		/*-- STALL SECTION */
--		,"stall_unidades".id_status as 'id_status' -- this come from test server
--		,"stall_status".nombre as 'estatus'
		/*-- STALL SECTION */
		,isnull("personal".nombre,'Sin Operador') as 'operador'
		,"kit".id_rem1 +' '+ isnull("kit".id_rem2,'') as 'remolque'
		,"area".id_area
		,"area".ciudad as 'area'
		,"operacion".id_tipo_operacion
		,ltrim(rtrim(replace("operacion".tipo_operacion,'FLETES' , ''))) as 'segmento'
		,"tbl_unit".description
		,"tbl_unit".compromise
		,"isviaje".status_viaje
		,"isviaje".desc_viaje
		,"istaller".status_taller
		,"istaller".desc_taller
		,cast("unidades".id_tipo_unidad as int) as 'units_type'
		,case
			when
					(
--					"isviaje".status_viaje = 'S'
--				or
					"istaller".status_taller = 'A'
					)
				then
					0
			else
				1
		end as 'iseditable'
		,"istaller".id_area as 'id_area_taller'
		,"area_taller".ciudad as 'area_taller'
		,"istaller".id_orden
		,"istaller".fecha_inicio
		,"istaller".fecha_ingreso
		,"istaller".fecha_prometida
		,"receptions".fecha as 'fecha_recepcion'
		--NOTE Added trafico_viaje
		,"trip".no_viaje as 'lastTrip'
		,"trip".id_remolque1
		,"trip".id_remolque2
		,"trip".id_dolly
		,"trip".f_despachado
		--NOTE Cases for StatusDays
		,case --"unidades".id_status
			when 
						"unidades".id_status = 1 --Operando
				then 
							case
								when "isviaje".status_viaje = 'S'
									then
										abs(datediff(dd,current_timestamp , "trip".f_despachado )) -- get days in
									else
										''--'La unidad No se encuentra en viaje'
							end 
			when 
					 "unidades".id_status = 6 --Disponible
				then 
						abs(datediff(dd,current_timestamp , "unidades".fecha_disponible)) -- datediff(dd,current_timestamp,??)
			when 
					"unidades".id_status =	4 --Taller
				then 
					 abs(datediff(dd,current_timestamp , "istaller".fecha_ingreso))
			when 
					"unidades".id_status =	18 --Taller
				then 
					 abs(datediff(dd,current_timestamp , "receptions".fecha))					
			when 
				"unidades".id_status in (7,8,9,10,11,12,13,14,15)
					then
							case
								when ("tbl_unit".compromise is not null)
									then
							 			abs(datediff(dd,current_timestamp , "tbl_unit".compromise))
									else 
										''--'La unidad No contiene una fecha_compromiso registrada'
							end
			else 
					''
		 end as 'StatusDays'
		,convert(nvarchar(max),current_timestamp,23) as 'created'
		from
--		gstdb.dbo.mtto_unidades	as "unidades"
		gstdb.dbo.mtto_unidades	as "unidades"
inner join
		gstdb.dbo.general_area as "area"
	on
		"unidades".id_area = "area".id_area and "area".id_area <> 7 -- NOTE Drop Macu-pana
inner join
		gstdb.dbo.desp_tipooperacion as "operacion"
	on
		"unidades".id_tipo_operacion = "operacion".id_tipo_operacion
inner join
		gstdb.dbo.desp_status as "status"
	on
		"unidades".id_status = "status".id_status
inner join
		sistemas.dbo.disponibilidad_view_unidades_class as "class"
	on 
		"status".id_status = "class".id_status
/*-- STALL SECTION */ --update change to productive
--left join
--		gstdb.dbo.mtto_unidades	as "stall_unidades"
--	on
--		"unidades".id_unidad = "stall_unidades".id_unidad and "unidades".id_area = "stall_unidades".id_area
--left join
--		gstdb.dbo.desp_status as "stall_status"
--	on
--		"stall_unidades".id_status = "stall_status".id_status
/*-- STALL SECTION */
left join
		gstdb.dbo.desp_flotas as "flotas"
	on
		"unidades".id_flota = "flotas".id_flota
left join
		gstdb.dbo.personal_personal as "personal"
	on
		"unidades".id_operador = "personal".id_personal
left join
		gstdb.dbo.trafico_kit_unidad as "kit"
	on
		"unidades".id_unidad = "kit".unidad_motriz
left join
		sistemas.dbo.disponibilidad_view_viajes_gst_indicators as "isviaje"
	on
		"isviaje".id_unidad = "unidades".id_unidad
left join
		sistemas.dbo.disponibilidad_view_taller_gst_indicators as "istaller"
	on
		"istaller".id_unidad = "unidades".id_unidad
left join
		gstdb.dbo.general_area as "area_taller"
	on
		"istaller".id_area = "area_taller".id_area
left join 
		sistemas.dbo.disponibilidad_view_id_unidades_receptions as "receptions"
	on
		"receptions".id_unidad = "unidades".id_unidad
left join
		sistemas.dbo.disponibilidad_view_id_unidades_gst_indicators as "id_unit"
	on
		"id_unit".unidad = "unidades".id_unidad
left join
		sistemas.dbo.disponibilidad_tbl_unidades_gst_indicators as "tbl_unit"
	on
		"tbl_unit".id = "id_unit".id
left join (
	select 
				"viaje".no_viaje,"viaje".id_unidad,"viaje".id_remolque1,"viaje".id_remolque2,"viaje".id_dolly,"viaje".f_despachado 
	from 
				gstdb.dbo.trafico_viaje as "viaje"
	where 
				"viaje".no_viaje  = (select max(cast("trviaje".no_viaje as int )) from gstdb.dbo.trafico_viaje as "trviaje" where "trviaje".id_unidad = "viaje".id_unidad) 
	) as "trip"
	on 
		"trip".id_unidad = "unidades".id_unidad
where
	-- This is added
		"unidades".id_tipo_unidad in (
										-- Tractos
										1,13
										-- dolly
										,4
										-- remolques
										,2,3,5,6,7,8,9
									 )
	-- This is added
	and
		"unidades".estatus = 'A'
--	and
--		"unidades".id_status in (4,6,8,15,14,18,11)

--	and
--		"unidades".id_unidad = 'TT1175'

--=========================================================================================================---
-- NOTE TEST and Monitoring tables and view
select count (unidad) as 'rows' , created,'A'
	from 
	sistemas.dbo.disponibilidad_view_rpt_unidades_gst_indicators
	group by
				created
union all
select count (unidad) as 'rows', created,'B'
from 
sistemas.dbo.disponibilidad_main_view_rpt_unidades_gst_indicators
group by
			created
order by
			created


select created,id_status,id_flota,* from sistemas.dbo.disponibilidad_main_view_rpt_unidades_gst_indicators where created = '2021-06-26'
and id_flota = 1 and unidad like 'TT%'

update sistemas.dbo.disponibilidad_tbl_hist_unidades_gst_indicators
set iseditable = 0 

update sistemas.dbo.disponibilidad_tbl_hist_unidades_gst_indicators
set id_status = 1 , estatus = 'OPERANDO' ,tipo_status = 'A' , clasification_name = 'Operando' , group_name = 'Disponibilidad'
where created = '2021-06-26' 
and id_flota = 1 
and unidad like 'TT%'


--=========================================================================================================--


	select units_type,* from sistemas.dbo.disponibilidad_view_rpt_unidades_gst_indicators
	where units_type in ('1','13') and id_area = 1 and id_status in (4,6,8,15,14,18,11)


 select 
				count(id_status)  as 'unidades'
--				,estatus
			  ,clasification_name
				,group_name 
	from 
				sistemas.dbo.disponibilidad_view_rpt_unidades_gst_indicators
	where 
				units_type in (1,13)
			and 
				group_name is not null
	group by 
--				 estatus,
				 clasification_name,
				 group_name

--	select id_tipo_unidad,* from gstdb.dbo.mtto_unidades

--	select * from gstdb.dbo.mtto_tipos_unidades
-- ======================================================================================================== --
-- Principal view group
-- ======================================================================================================== --
use sistemas
--


select
			 "cross".id_area,"cross".label,"cross".group_name,"area".types
			,sum("area".unidades) as 'unidades' --, "area".label , "area".id_area , "area".group_name 
from 
			sistemas.dbo.disponibilidad_view_cross_units as "cross"
left join 
			sistemas.dbo.disponibilidad_view_area_units as "area"
	on
			"cross".id_area = "area".id_area and "cross".group_name = "area".group_name and "area".units_type in (1,13)
where 1 = 1
group by 
				 "cross".id_area,"cross".label,"cross".group_name,"area".types




select * from sistemas.dbo.disponibilidad_view_cross_units

select * from sistemas.dbo.disponibilidad_view_area_units where units_type in (1,13)

-- NOTE working from hir 

select * from sistemas.dbo.disponibilidad_tbl_unidades_clasifications
select * from sistemas.dbo.projections_view_bussines_units

select * from sistemas.dbo.ingresos_costos_gst_holidays
select * from sistemas.dbo.ingresos_costos_his_holidays
select * from sistemas.dbo.ingresos_costos_gerencial_indicators
-- ======================================================================================================== --
-- Principal view classes
-- ======================================================================================================== --

IF OBJECT_ID ('disponibilidad_view_cross_names', 'V') IS NOT NULL
    DROP VIEW disponibilidad_view_cross_names;
--NOTE now build the view
--create
alter
view disponibilidad_view_cross_names
with encryption
as
			select 
						 "unit".id_area,"unit".label
						,"class".clasification_name
			from 
						projections_view_bussiness_units as "unit"
				cross join 
						 sistemas.dbo.disponibilidad_tbl_unidades_clasifications "class"
		 where 
						"unit".id_area <> 7   -- NOTE MAcuca

select * from disponibilidad_view_cross_names where id_area = 7

select 
			 id_area,area,id_flota,id_tipo_operacion
			,sum(unidades) as 'unidades'
--			,unidades
			,clasification_name,units_type
from
--	select * from 
			sistemas.dbo.disponibilidad_view_rpt_group_gst_indicators 
			where 
				units_type in (1,13)  -- NOTE for tipo de unidad
			and 
				id_flota in (1) -- NOTE flota for operacion granel and others 
			and 
				id_tipo_operacion in (1)
group by	
			id_area,area,id_flota,id_tipo_operacion,clasification_name,units_type

-- ======================================================================================================== --
-- Principal view group
-- ======================================================================================================== --

IF OBJECT_ID ('disponibilidad_view_cross_units', 'V') IS NOT NULL
    DROP VIEW disponibilidad_view_cross_units;
--NOTE now build the view
--create
alter
view disponibilidad_view_cross_units
with encryption
as
			select 
						 "unit".id_area,"unit".label
						,"op".group_name
			from 
						projections_view_bussiness_units as "unit"
				cross join 
						(select group_name from disponibilidad_view_unidades_class  where group_name is not null group by group_name) as "op"
			where 
						"unit".id_area <> 7 -- NOTE drop macuspana


select * from sistemas.dbo.disponibilidad_view_cross_units where id_area = 7						
-- ======================================================================================================== --
-- Principal view group
-- ======================================================================================================== --
select * from disponibilidad_view_area_units order by id_area

select * from sistemas.dbo.disponibilidad_tbl_unit_types_relations

select * from gstdb.dbo.desp_tipooperacion 


IF OBJECT_ID ('disponibilidad_view_area_units', 'V') IS NOT NULL
    DROP VIEW disponibilidad_view_area_units;
--NOTE now build the view
alter view disponibilidad_view_area_units
with encryption
as
with cross_view as (
			select 
						 "unit".id_area,"unit".label
						,"op".group_name
			from 
						projections_view_bussiness_units as "unit"
				cross join 
						(select group_name from disponibilidad_view_unidades_class  where group_name is not null group by group_name) as "op"
			where -- NOTE drop macuspana  
						"unit".id_area <> 7
		)
select 
			"crw".id_area
			,"crw".label
			,"crw".group_name
			,"sums".unidades
			,"sums".units_type
			,"rel".types
from 
			cross_view as "crw"
left join (
					select
								 sum(unidades) as 'unidades'
								,group_name,id_area,units_type 
					from 
								sistemas.dbo.disponibilidad_view_rpt_group_gst_indicators --where units_type in (1,13) --and group_name not in ('Siniestrado','Robo')
					group by
								group_name,id_area,units_type
					) as "sums"
left join --95
			sistemas.dbo.disponibilidad_tbl_unit_types_relations as "rel"
		on 
			"rel".operacion = "sums".units_type
on 
	"crw".id_area = "sums".id_area and "crw".group_name = "sums".group_name


select * from sistemas.dbo.disponibilidad_view_area_units where id_area = 7

-- ============================================================================================================= --
-- NOTE Rebuilt of table grouping                           ==================================================== --
-- ============================================================================================================= --


IF OBJECT_ID ('disponibilidad_view_rpt_group_clasifications_indicators', 'V') IS NOT NULL 
    DROP VIEW disponibilidad_view_rpt_group_clasifications_indicators;
-- now build the view
alter
--create 
view 
disponibilidad_main_view_rpt_group_clasifications_indicators  --when ist done , just drop the main prefix
with encryption
as
with "class" as (
									select --top 10
											 row_number() over(order by "ops".id_area) as 'id'
										 	,count("ops".unidad) as 'unidades'
											,"ops".unidad
											,"ops".id_status
											,"ops".units_type
											,case when "ops".units_type in (1,13) then 'Tractocamiones' when "ops".units_type in (4) then 'Dollys' else 'Remolques' end as 'TipoVehiculo'
											,"ops".estatus
											,"ops".clasification_name
											,[Disponible] = max( case when "ops".clasification_name = 'Disponible' then 1 else null end )
											,[Operando] = max( case when "ops".clasification_name = 'Operando' then 1 else null end )
											,[Taller] = max( case when "ops".clasification_name = 'Taller' then 1 else null end )
											,[Gestoria] = max( case when "ops".clasification_name = 'Gestoria' then 1 else null end )
											,[Siniestrado] = max( case when "ops".clasification_name = 'Siniestrado' then 1 else null end )
											,[Robo] = max( case when "ops".clasification_name = 'Robo' then 1 else null end )
											,[En Exhibicion] = max( case when "ops".clasification_name = 'En Exhibicion' then 1 else null end )
											,[En Venta] = max( case when "ops".clasification_name = 'En Venta' then 1 else null end )
											,"ops".group_name
											,"ops".id_area
											,"ops".area
											,"ops".id_flota
											,"ops".id_tipo_operacion
											,"ops".segmento
											,"ops".created
--											,convert(nvarchar(max),current_timestamp,23) as 'created' --1270
									from
--											sistemas.dbo.disponibilidad_main_view_rpt_unidades_gst_indicators as "ops"
											sistemas.dbo.disponibilidad_currentview_rpt_unidades_gst_indicators as "ops"
									group by
											 "ops".unidad
											,"ops".id_status
											,"ops".units_type
											,"ops".estatus
											,"ops".clasification_name
											,"ops".group_name
											,"ops".id_area
											,"ops".area
											,"ops".id_flota
											,"ops".id_tipo_operacion
											,"ops".segmento
											,"ops".created
)
select 
			 id_area
			,area
			,id_flota
			,id_tipo_operacion
			,segmento as 'Flota'
--			,"class".units_type
			,TipoVehiculo
			,sum(isnull(Disponible,0)) as 'Disponible'
			,sum(isnull(Operando,0)) as 'Operando'
			,sum(isnull(Taller,0)) as 'Taller'
			,sum(isnull(Gestoria,0)) as 'Gestoria'
			,sum(isnull(Siniestrado,0)) as 'Siniestrado'
			,sum(isnull(Robo,0)) as 'Robo'
			,sum(isnull([En Exhibicion],0)) as 'Exhibicion'
			,sum(isnull([En Venta],0)) as 'Venta'
			,sum(isnull(Disponible,0)) + sum(isnull(Operando,0)) as 'TotalDisponibilidad'
			,sum(isnull(Disponible,0)) + sum(isnull(Operando,0)) + sum(isnull(Taller,0)) + sum(isnull(Gestoria,0)) + sum(isnull(Siniestrado,0)) + sum(isnull(Robo,0)) + sum(isnull([En Exhibicion],0)) as 'TotalFlota'
-- NOTE 4Percents 
-- Percent Disponibilidad
			,(sum(isnull(Disponible,0)) + sum(isnull(Operando,0))) * 100 / (sum(isnull(Disponible,0)) + sum(isnull(Operando,0)) + sum(isnull(Taller,0)) + sum(isnull(Gestoria,0)) + sum(isnull(Siniestrado,0)) + sum(isnull(Robo,0)) + sum(isnull([En Exhibicion],0)) + sum(isnull([En Venta],0))) as 'DisponibilidadFlota'
-- Percent FlotaOperando
			,(sum(isnull(Operando,0))) * 100 / (sum(isnull(Disponible,0)) + sum(isnull(Operando,0)) + sum(isnull(Taller,0)) + sum(isnull(Gestoria,0)) + sum(isnull(Siniestrado,0)) + sum(isnull(Robo,0)) + sum(isnull([En Exhibicion],0)) + sum(isnull([En Venta],0))) as 'FlotaOperando'
-- PercentNoTripDisponible
			,( sum(isnull(Disponible,0)) ) * 100 / (sum(isnull(Disponible,0)) + sum(isnull(Operando,0)) + sum(isnull(Taller,0)) + sum(isnull(Gestoria,0)) + sum(isnull(Siniestrado,0)) + sum(isnull(Robo,0)) + sum(isnull([En Exhibicion],0)) + sum(isnull([En Venta],0))) as 'Disp_S_Viaje'
-- Percent OP.FlotaGestoria/Siniester
			,(sum(isnull(Gestoria,0)) + sum(isnull(Siniestrado,0))) * 100 / (sum(isnull(Disponible,0)) + sum(isnull(Operando,0)) + sum(isnull(Taller,0)) + sum(isnull(Gestoria,0)) + sum(isnull(Siniestrado,0)) + sum(isnull(Robo,0)) + sum(isnull([En Exhibicion],0)) + sum(isnull([En Venta],0))) as 'Op_FlotaGestoria_Siniestro'
-- Taller
			,( sum(isnull(Taller,0)) ) * 100 / (sum(isnull(Disponible,0)) + sum(isnull(Operando,0)) + sum(isnull(Taller,0)) + sum(isnull(Gestoria,0)) + sum(isnull(Siniestrado,0)) + sum(isnull(Robo,0)) + sum(isnull([En Exhibicion],0)) + sum(isnull([En Venta],0))) as 'FlotaMtto'
--		,convert(nvarchar(max),current_timestamp,23) as 'created'
			,created
from 
		"class"
where 
			id_area <> 7  -- NOTE Remove Macuca
group by 
--			 "class".units_type
			 TipoVehiculo
			,id_area
			,area
			,id_flota
			,id_tipo_operacion
			,segmento
			,created
--order by
--			Flota
--			,TipoVehiculo


select * from sistemas.dbo.disponibilidad_view_rpt_group_clasifications_indicators where id_area = 8 and id_tipo_operacion = 0

select 
from 
			sistemas.dbo.disponibilidad_main_view_rpt_group_clasifications_indicators where id_area = 1 
and created between '2021-08-04' and GETDATE()
and id_tipo_operacion = 11 and TipoVehiculo = 'Tractocamiones'   


select sum(unidades) as 'unidades',clasification_name,area,units_type from sistemas.dbo.disponibilidad_view_rpt_group_gst_indicators where units_type in (1,13) and area in ('ORIZABA')
group by clasification_name,area,units_type

select * from sistemas.dbo.disponibilidad_view_rpt_group_gst_indicators

select * from sistemas.dbo.disponibilidad_main_view_rpt_group_gst_indicators

IF OBJECT_ID ('disponibilidad_view_rpt_group_gst_indicators', 'V') IS NOT NULL
    DROP VIEW disponibilidad_view_rpt_group_gst_indicators;
-- now build the view
--create
alter 
view disponibilidad_main_view_rpt_group_gst_indicators -- NOTE for revert drop _main
with encryption
as  --150
select
		 row_number() over(order by "dispgrouping".id_area) as 'id'
	 	,count("dispgrouping".unidad) as 'unidades'
		,"dispgrouping".id_status
		,"dispgrouping".units_type
		,"dispgrouping".estatus
		,"dispgrouping".clasification_name
		,"dispgrouping".group_name
		,"dispgrouping".segmento
		,"dispgrouping".id_area
		,"dispgrouping".area
		,"dispgrouping".id_flota
		,"dispgrouping".id_tipo_operacion
		,"dispgrouping".created
from
		sistemas.dbo.disponibilidad_main_view_rpt_unidades_gst_indicators as "dispgrouping"
where 
		"dispgrouping".id_area <> 7  --NOTE drop macu-pana
group by
		 "dispgrouping".id_status
		,"dispgrouping".units_type
		,"dispgrouping".estatus
		,"dispgrouping".clasification_name
		,"dispgrouping".group_name
		,"dispgrouping".segmento
		,"dispgrouping".id_area
		,"dispgrouping".area
		,"dispgrouping".id_flota
		,"dispgrouping".id_tipo_operacion
		,"dispgrouping".created



--	73 rows

	select * from sistemas.dbo.disponibilidad_view_rpt_group_gst_indicators

	select * from sistemas.dbo.disponibilidad_tbl_unidades_clasifications where is_sum = 1 

select
		 row_number() over(order by "dispgrouping".id_area) as 'id'
--	 	,count("dispgrouping".unidad) as 'unidades'
		,"dispgrouping".unidad
		,"dispgrouping".id_status		--- mtto_unidad.id_status = 4
		,"dispgrouping".estatus
		,"dispgrouping".status_taller   --- mtto_orden.estado = 'A'
		,"dispgrouping".status_viaje
		,"dispgrouping".id_area
		,"dispgrouping".area
		,"dispgrouping".id_flota
from
		sistemas.dbo.disponibilidad_view_rpt_unidades_gst_indicators as "dispgrouping"
where
		unidad in
				(
'TT1379'
,'TT1343'
,'TT1271'
,'TT1279'
,'TT1257'
,'TT1264'
,'TT1209'
,'TT1195'
				)




Select MO.id_unidad,MU.id_status,MO.estado from gstdb.dbo.mtto_unidades MU
left join
			gstdb.dbo.mtto_orden MO
		on
			MO.id_unidad = MU.id_unidad --and MO.estado = 'A'
left join gstdb.dbo.mtto_tipos_unidades MTU
		on
			MU.id_tipo_unidad = MTU.id_tipo_unidad
where
		MU.id_status = 4
	and
		MTU.id_tipo_unidad in (1)
and MU.id_unidad in
				(
'TT1379'
,'TT1343'
,'TT1271'
,'TT1279'
,'TT1257'
,'TT1264'
,'TT1209'
,'TT1195'
				)
--order by MO.id_unidad
group by
	MO.id_unidad,MU.id_status,MO.estado


--add Valida si la unidad tiene orden de trabajo abierta

Select
id_area,
id_orden,
id_unidad,
estado,
fecha_inicio,
fecha_prometida
from
gstdb.dbo.mtto_orden
where
estado = 'A'
--and id_unidad =''

-- ======================================================================================================== --
-- trigger gstdb_pba
-- ======================================================================================================== --
select * from sistemas.dbo.disponibilidad_view_unidades_gst_indicators where area = 'Orizaba' and segmento = 'GRANEL'

select * from sistemas.dbo.disponibilidad_view_rpt_unidades_gst_indicators where id_area = 1 and id_tipo_operacion = 1

select * from sistemas.dbo.disponibilidad_view_rpt_group_gst_indicators where id_area = 1
--========================================================================================================================================= --
-- 											Triggers for projections																		--
--========================================================================================================================================= --
use sistemas
-- ======================================================================================================== --
-- NOTE Pointing to test table repoint to production when needed , this do the magic
-- ======================================================================================================== --
-- NOTE This must change for productive --
-- drop trigger disponibilidad_update_unidad_status
use sistemas
--create
--alter 
trigger disponibilidad_update_unidad_status
ON sistemas.dbo.disponibilidad_tbl_unidades_gst_indicators
--with encryption
after insert
as
begin
-- Catch the rows for examination
	set nocount on
	-- then save in statistics
		update gstdb.dbo.mtto_unidades
--		update pruebas_tbk.dbo.mtto_unidades
			set id_status = "i".id_status
		from
				gstdb.dbo.mtto_unidades as "mtto"
--				pruebas_tbk.dbo.mtto_unidades as "mtto"
		inner join
				inserted as "i"
			on
				"mtto".id_unidad = "i".unidad
		where
				"mtto".id_unidad = "i".unidad
	-- Run sp
end


use sistemas

select * from sistemas.dbo.disponibilidad_tbl_unidades_gst_indicators
-- truncate table  sistemas.dbo.disponibilidad_tbl_unidades_gst_indicators
-- truncate table sistemas.dbo.disponibilidad_tbl_mtto_log_gst_indicators

select
--	count(id_control) as 'id',
	id_status,posdate,poslat,poslon,id_status,status_query,id_unidad
--,* 
from sistemas.dbo.disponibilidad_tbl_mtto_log_gst_indicators where id_unidad = 'RN1003'
group by
	id_status,posdate,poslat,poslon,id_status,status_query,id_unidad
order by 
	posdate asc


exec sp_helptext disponibilidad_tbl_unidades_gst_indicators

-- NOTE check triggers in mtto_unidades

	exec sp_helptext  TR_MTTO_UNIDADES_ADICIONALES
	exec sp_helptext	t_mtto_unidades01
	exec sp_helptext	gst_actualiza_area_depto
	exec sp_helptext	gst_replica_status_disponibilidad
	exec sp_helptext	gst_valida_estatus_alta_unidad

-- NOTE First step clone mtto_unidades without constrints

select top 10 * from pruebasdb2019.dbo.mtto_unidades

truncate table  sistemas.dbo.disponibilidad_tbl_mtto_log_gst_indicators

select 
			* 
into 
		sistemas.dbo.disponibilidad_tbl_mtto_log_gst_indicators
from 
		pruebasdb2019.dbo.mtto_unidades
where 
		1 = 2

-- NOTE Reverse update from LIS to Disponibilidad app 
-- ================================================================================================================================================= --
-- NOTE Triggers Section for Solicitud
-- ================================================================================================================================================= --
-- check the current selected database 
--select DB_NAME()

create
alter
trigger gst_tr_reverse_disponibilidad_rows
	ON [gstdb].[dbo].[mtto_unidades]
after update
as 
begin 
	set nocount on
    -- Check if this is an INSERT, UPDATE or DELETE Action.
    DECLARE @action as char(1)

    set @action = 'I' -- Set Action to Insert by default.
    if exists(select * from deleted)
				begin
						set @action = 
								case
									when exists(select * from inserted) 
										then 'U' -- Set Action to Updated.
									else 'D' -- Set Action to Deleted.       
								 end 
        end
	  else
			begin
        if not exists(select * from inserted)
				  begin
						RETURN -- Nothing updated or inserted.
					end
			end
--  declare vars 
 if @action <> 'D'
	begin
--		select 'test'
--	end
--end 
		if UPDATE(id_status)
		begin

					declare
									@id_unidad varchar(10),
									@total_llantas int,
									@tipo_unidad int,
									@id_tipo_motor int,
									@id_marca_unidad int,
									@id_tipo_unidad int,
									@kms_acumulados int,
									@r_f_v int,
									@id_centro_costo int,
									@llantas_extras int,
									@no_permiso int,
									@no_inciso_seguro int,
									@id_operador int,
									@kms_servicio_ultimo int,
									@kms_servicio_sig int,
									@id_area int,
									@id_depto int,
									@kms_inicio_ciclo int,
									@secuencia_inicio int,
									@idcia int,
									@hrs_inicio_ciclo decimal,
									@no_cilindros int,
									@prov_carroceria int,
									@secuencia_ult int,
									@secuencia_sig int,
									@no_circula int,
									@doble_no_circula int,
									@id_tercero int,
									@id_status int,
									@status_comm int,
									@msg_pend int,
									@id_ruta int,
									@id_area_viaje int,
									@no_viaje int,
									@id_pedido int,
									@id_flota int,
									@id_origen int,
									@id_destino int,
									@id_remitente int,
									@id_destinatario int,
									@id_macro_ult int,
									@costo_tenencia decimal,
									@no_poliza decimal,
									@prima_neta decimal,
									@recargos decimal,
									@der_poliza decimal,
									@iva decimal,
									@costo_unidad decimal,
									@costo_carroceria decimal,
									@ultima_tenencia datetime,
									@ultimo_refrendo datetime,
									@ultimo_permiso datetime,
									@vence_seguro datetime,
									@fecha_kms datetime,
									@fecha_servicio_ultimo datetime,
									@fecha_servicio_sig datetime,
									@fecha_adquisicion datetime,
									@fecha_ultima_rev datetime,
									@fecha_inicio_ciclo datetime,
									@fecha_emision datetime,
									@fecha_desde datetime,
									@fecha_hasta datetime,
									@fecha_adq_carr datetime,
									@fecha_estatus datetime,
									@fecha_baja datetime,
									@posdate datetime,
									@f_real_salida datetime,
									@f_prog_llegada datetime,
									@f_entrada_patio datetime,
									@f_mismapos datetime,
									@TEA datetime,
									@rendimiento_esperado decimal,
									@lts_combustible decimal,
									@lts_aceite decimal,
									@kms_por_dia decimal,
									@dim_ancho decimal,
									@dim_largo decimal,
									@dim_altura decimal,
									@rendimiento_aceite decimal,
									@hrs_acumuladas decimal,
									@hrs_por_dia decimal,
									@hrs_servicio_ultimo decimal,
									@hrs_servicio_sig decimal,
									@proveedor decimal,
									@peso_unidades decimal,
									@lts_relleno decimal,
									@lts_saldocomb decimal,
									@no_serie varchar(20),
									@modelo varchar(15),
									@id_ciclo_preventivo varchar(6),
									@placas varchar(8),
									@observaciones varchar(150),
									@no_economico varchar(10),
									@id_servicio_ultimo varchar(6),
									@id_servicio_sig varchar(6),
									@id_servicio_inicio varchar(6),
									@tarjeta_circulacion varchar(10),
									@no_factura varchar(10),
									@tipo_motor varchar(10),
									@potencia_motor varchar(8),
									@pulgadas_cubicas varchar(6),
									@no_motor varchar(20),
									@color varchar(10),
									@no_carroceria varchar(10),
									@obs_carroceria varchar(200),
									@fecha_vence_placas datetime,
									@poliza varchar(40),
									@fecha_vence_tenencia datetime,
									@status_unidad varchar(2),
									@viaje varchar(35),
									@fecha_vence_seguro datetime,
									@id_thermo varchar(10),
									@fecha_vence_verificacion datetime,
									@no_kit varchar(10),
									@desc_marcamotor varchar(25),
									@desc_provcarroceria varchar(80),
									@desc_provunidad varchar(80),
									@tarjeta_llave char(1),
									@no_tarjeta_llave varchar(25),
									@mctnumber varchar(50),
									@posicion varchar(250),
									@poslat decimal,
									@poslon decimal,
									@operador varchar(40),
									@id_unidad_as varchar(10),
									@id_remolque1 varchar(20),
									@id_dolly varchar(10),
									@id_remolque2 varchar(20),
									@origen varchar(30),
									@destino varchar(30),
									@id_despachador varchar(10),
									@tipo_combustible char(1),
									@estado char(1),
									@estado_adquisicion char(1),
									@datos_generales varchar(255),
									@tipo_moneda char(1),
									@forma_pago char(1),
									@kms_hrs char(1),
									@hrs_automaticas char(1),
									@estatus char(1),
									@tipo_programacion char(1),
									@equipo_omnitracs char(1),
									@tercero char(1),
									@causa_baja char(1),
									@tipo char(1),
									@status char(1),
									@status_caja1 char(1),
									@status_caja2 char(1),
									@en_patio char(1),
									@mismapos char(1),
									@f_status_unidad datetime,
									@subcta varchar(25),
									@tipomacro_ult char(1),
									@kms_actuales int,
									@ruta varchar(50),
									@peso_bruto int,
									@fecha_instalacion datetime,
									@kmshrsinicialaparmed int,
									@no_medidor int,
									@tipo_medida char(1),
									@id_operador2 int,
									@hrs_actuales decimal,
									@id_ingreso varchar(15),
									@fecha_ingreso datetime,
									@kms_hrs_actuales int,
									@fecha_disponible datetime,
									@id_plazadestino int,
									@id_tipo_operacion int,
									@id_movimiento int,
									@clas_uni varchar(20),
									@cta_consumo varchar(20),
									@id_status_rem int,
									@ultimo_id_destinatario int,
									@ultimo_id_area_viaje int,
									@ultimo_no_viaje int,
									@id_estacion int,
									@ultimo_id_areapedido int,
									@ultimo_id_pedido int,
									@origen_status int,
									@cargadovacio char(1),
									@kms_ultima_rev int,
									@id_transponder varchar(8),
									@num_soporte varchar(8),
									@vehunitnum varchar(21),
									@nacionalidad int,
									@id_localidaddestino int,
									@id_circuito int,
									@id_compania int,
									@kms_arribo int,
									@ultimo_id_pedidopk int,
									@alarmasnuevas int,
									@mensajesnuevos int,
									@id_linearem1 varchar(10),
									@id_linearem2 varchar(10),
									@modulo int,
									@id_asignacion int,
									@kms_actualesvale int,
									@kms_recorridosvale int,
									@modelo_unidad varchar(20),
									@madre char(1),
									@hija char(1),
									@asignado char(1),
									@unidad_asignado varchar(11),
									@hija_asignada varchar(11),
									@FECHA_LOGISTICA datetime,
									@valor_convenido decimal,
									@deducible_xrobo decimal,
									@deducible_xdmateriales decimal,
									@faset decimal,
									@jefe_flota int,
									@id_plazasigdestino int,
									@id_sector int,
									@id_subflota int,
									@EM_email varchar(150),
									@no_orsan varchar(100),
									@valida_cargacomb int,
									@procesado bit,
									@disponibilidad decimal,
									@capacidad_aliado char(1),
									@marca_cap_eje_delantero varchar(120),
									@marca_cap_eje_trasero varchar(120),
									@marca_modelo_tipo_transmision varchar(120),
									@marca_suspension varchar(120),
									@medida_llanta varchar(120),
									@tipo_ejes varchar(120),
									@tipo_suspension varchar(120),
									@marca_aliado varchar(120),
									@ano_fabricacion int,
									@id_histograma_sig int,
									@id_histograma_ultimo int,
									@fecha_fin_garantia datetime,
									@id_accion int,
									@id_project int,
									@ccosto int,
									@id_tipo_servicio_unidad int,
									@poliza2 varchar(40),
									@no_inciso_seguro2 int,
									@vence_seguro2 datetime,
									@id_aseguradora int,
									@cta_mayor varchar(25)


			 
						-- catch the data just in case -- if needed in the future --
						select
										@id_unidad = id_unidad,
										@total_llantas = total_llantas,
										@tipo_unidad = tipo_unidad,
										@id_tipo_motor = id_tipo_motor,
										@id_marca_unidad = id_marca_unidad,
										@id_tipo_unidad = id_tipo_unidad,
										@kms_acumulados = kms_acumulados,
										@r_f_v = r_f_v,
										@id_centro_costo = id_centro_costo,
										@llantas_extras = llantas_extras,
										@no_permiso = no_permiso,
										@no_inciso_seguro = no_inciso_seguro,
										@id_operador = id_operador,
										@kms_servicio_ultimo = kms_servicio_ultimo,
										@kms_servicio_sig = kms_servicio_sig,
										@id_area = id_area,
										@id_depto = id_depto,
										@kms_inicio_ciclo = kms_inicio_ciclo,
										@secuencia_inicio = secuencia_inicio,
										@idcia = idcia,
										@hrs_inicio_ciclo = hrs_inicio_ciclo,
										@no_cilindros = no_cilindros,
										@prov_carroceria = prov_carroceria,
										@secuencia_ult = secuencia_ult,
										@secuencia_sig = secuencia_sig,
										@no_circula = no_circula,
										@doble_no_circula = doble_no_circula,
										@id_tercero = id_tercero,
										@id_status = id_status,
										@status_comm = status_comm,
										@msg_pend = msg_pend,
										@id_ruta = id_ruta,
										@id_area_viaje = id_area_viaje,
										@no_viaje = no_viaje,
										@id_pedido = id_pedido,
										@id_flota = id_flota,
										@id_origen = id_origen,
										@id_destino = id_destino,
										@id_remitente = id_remitente,
										@id_destinatario = id_destinatario,
										@id_macro_ult = id_macro_ult,
										@costo_tenencia = costo_tenencia,
										@no_poliza = no_poliza,
										@prima_neta = prima_neta,
										@recargos = recargos,
										@der_poliza = der_poliza,
										@iva = iva,
										@costo_unidad = costo_unidad,
										@costo_carroceria = costo_carroceria,
										@ultima_tenencia = ultima_tenencia,
										@ultimo_refrendo = ultimo_refrendo,
										@ultimo_permiso = ultimo_permiso,
										@vence_seguro = vence_seguro,
										@fecha_kms = fecha_kms,
										@fecha_servicio_ultimo = fecha_servicio_ultimo,
										@fecha_servicio_sig = fecha_servicio_sig,
										@fecha_adquisicion = fecha_adquisicion,
										@fecha_ultima_rev = fecha_ultima_rev,
										@fecha_inicio_ciclo = fecha_inicio_ciclo,
										@fecha_emision = fecha_emision,
										@fecha_desde = fecha_desde,
										@fecha_hasta = fecha_hasta,
										@fecha_adq_carr = fecha_adq_carr,
										@fecha_estatus = fecha_estatus,
										@fecha_baja = fecha_baja,
										@posdate = posdate,
										@f_real_salida = f_real_salida,
										@f_prog_llegada = f_prog_llegada,
										@f_entrada_patio = f_entrada_patio,
										@f_mismapos = f_mismapos,
										@TEA = TEA,
										@rendimiento_esperado = rendimiento_esperado,
										@lts_combustible = lts_combustible,
										@lts_aceite = lts_aceite,
										@kms_por_dia = kms_por_dia,
										@dim_ancho = dim_ancho,
										@dim_largo = dim_largo,
										@dim_altura = dim_altura,
										@rendimiento_aceite = rendimiento_aceite,
										@hrs_acumuladas = hrs_acumuladas,
										@hrs_por_dia = hrs_por_dia,
										@hrs_servicio_ultimo = hrs_servicio_ultimo,
										@hrs_servicio_sig = hrs_servicio_sig,
										@proveedor = proveedor,
										@peso_unidades = peso_unidades,
										@lts_relleno = lts_relleno,
										@lts_saldocomb = lts_saldocomb,
										@no_serie = no_serie,
										@modelo = modelo,
										@id_ciclo_preventivo = id_ciclo_preventivo,
										@placas = placas,
										@observaciones = observaciones,
										@no_economico = no_economico,
										@id_servicio_ultimo = id_servicio_ultimo,
										@id_servicio_sig = id_servicio_sig,
										@id_servicio_inicio = id_servicio_inicio,
										@tarjeta_circulacion = tarjeta_circulacion,
										@no_factura = no_factura,
										@tipo_motor = tipo_motor,
										@potencia_motor = potencia_motor,
										@pulgadas_cubicas = pulgadas_cubicas,
										@no_motor = no_motor,
										@color = color,
										@no_carroceria = no_carroceria,
										@obs_carroceria = obs_carroceria,
										@fecha_vence_placas = fecha_vence_placas,
										@poliza = poliza,
										@fecha_vence_tenencia = fecha_vence_tenencia,
										@status_unidad = status_unidad,
										@viaje = viaje,
										@fecha_vence_seguro = fecha_vence_seguro,
										@id_thermo = id_thermo,
										@fecha_vence_verificacion = fecha_vence_verificacion,
										@no_kit = no_kit,
										@desc_marcamotor = desc_marcamotor,
										@desc_provcarroceria = desc_provcarroceria,
										@desc_provunidad = desc_provunidad,
										@tarjeta_llave = tarjeta_llave,
										@no_tarjeta_llave = no_tarjeta_llave,
										@mctnumber = mctnumber,
										@posicion = posicion,
										@poslat = poslat,
										@poslon = poslon,
										@operador = operador,
										@id_unidad_as = id_unidad_as,
										@id_remolque1 = id_remolque1,
										@id_dolly = id_dolly,
										@id_remolque2 = id_remolque2,
										@origen = origen,
										@destino = destino,
										@id_despachador = id_despachador,
										@tipo_combustible = tipo_combustible,
										@estado = estado,
										@estado_adquisicion = estado_adquisicion,
										@datos_generales = datos_generales,
										@tipo_moneda = tipo_moneda,
										@forma_pago = forma_pago,
										@kms_hrs = kms_hrs,
										@hrs_automaticas = hrs_automaticas,
										@estatus = estatus,
										@tipo_programacion = tipo_programacion,
										@equipo_omnitracs = equipo_omnitracs,
										@tercero = tercero,
										@causa_baja = causa_baja,
										@tipo = tipo,
										@status = status,
										@status_caja1 = status_caja1,
										@status_caja2 = status_caja2,
										@en_patio = en_patio,
										@mismapos = mismapos,
										@f_status_unidad = f_status_unidad,
										@subcta = subcta,
										@tipomacro_ult = tipomacro_ult,
										@kms_actuales = kms_actuales,
										@ruta = ruta,
										@peso_bruto = peso_bruto,
										@fecha_instalacion = fecha_instalacion,
										@kmshrsinicialaparmed = kmshrsinicialaparmed,
										@no_medidor = no_medidor,
										@tipo_medida = tipo_medida,
										@id_operador2 = id_operador2,
										@hrs_actuales = hrs_actuales,
										@id_ingreso = id_ingreso,
										@fecha_ingreso = fecha_ingreso,
										@kms_hrs_actuales = kms_hrs_actuales,
										@fecha_disponible = fecha_disponible,
										@id_plazadestino = id_plazadestino,
										@id_tipo_operacion = id_tipo_operacion,
										@id_movimiento = id_movimiento,
										@clas_uni = clas_uni,
										@cta_consumo = cta_consumo,
										@id_status_rem = id_status_rem,
										@ultimo_id_destinatario = ultimo_id_destinatario,
										@ultimo_id_area_viaje = ultimo_id_area_viaje,
										@ultimo_no_viaje = ultimo_no_viaje,
										@id_estacion = id_estacion,
										@ultimo_id_areapedido = ultimo_id_areapedido,
										@ultimo_id_pedido = ultimo_id_pedido,
										@origen_status = origen_status,
										@cargadovacio = cargadovacio,
										@kms_ultima_rev = kms_ultima_rev,
										@id_transponder = id_transponder,
										@num_soporte = num_soporte,
										@vehunitnum = vehunitnum,
										@nacionalidad = nacionalidad,
										@id_localidaddestino = id_localidaddestino,
										@id_circuito = id_circuito,
										@id_compania = id_compania,
										@kms_arribo = kms_arribo,
										@ultimo_id_pedidopk = ultimo_id_pedidopk,
										@alarmasnuevas = alarmasnuevas,
										@mensajesnuevos = mensajesnuevos,
										@id_linearem1 = id_linearem1,
										@id_linearem2 = id_linearem2,
										@modulo = modulo,
										@id_asignacion = id_asignacion,
										@kms_actualesvale = kms_actualesvale,
										@kms_recorridosvale = kms_recorridosvale,
										@modelo_unidad = modelo_unidad,
										@madre = madre,
										@hija = hija,
										@asignado = asignado,
										@unidad_asignado = unidad_asignado,
										@hija_asignada = hija_asignada,
										@FECHA_LOGISTICA = FECHA_LOGISTICA,
										@valor_convenido = valor_convenido,
										@deducible_xrobo = deducible_xrobo,
										@deducible_xdmateriales = deducible_xdmateriales,
										@faset = faset,
										@jefe_flota = jefe_flota,
										@id_plazasigdestino = id_plazasigdestino,
										@id_sector = id_sector,
										@id_subflota = id_subflota,
										@EM_email = EM_email,
										@no_orsan = no_orsan,
										@valida_cargacomb = valida_cargacomb,
										@procesado = procesado,
										@disponibilidad = disponibilidad,
										@capacidad_aliado = capacidad_aliado,
										@marca_cap_eje_delantero = marca_cap_eje_delantero,
										@marca_cap_eje_trasero = marca_cap_eje_trasero,
										@marca_modelo_tipo_transmision = marca_modelo_tipo_transmision,
										@marca_suspension = marca_suspension,
										@medida_llanta = medida_llanta,
										@tipo_ejes = tipo_ejes,
										@tipo_suspension = tipo_suspension,
										@marca_aliado = marca_aliado,
										@ano_fabricacion = ano_fabricacion,
										@id_histograma_sig = id_histograma_sig,
										@id_histograma_ultimo = id_histograma_ultimo,
										@fecha_fin_garantia = fecha_fin_garantia,
										@id_accion = id_accion,
										@id_project = id_project,
										@ccosto = ccosto,
										@id_tipo_servicio_unidad = id_tipo_servicio_unidad,
										@poliza2 = poliza2,
										@no_inciso_seguro2 = no_inciso_seguro2,
										@vence_seguro2 = vence_seguro2,
										@id_aseguradora = id_aseguradora,
										@cta_mayor = cta_mayor
						from inserted
			-- NOTE cached fields into vars 

						-- Update to sistemas
			--NOTE insert into foreign table

			-- NOTE TEST 
			-- select ident_current('sistemas.dbo.disponibilidad_tbl_unidades_gst_indicators')

			-- select * from sistemas.dbo.disponibilidad_tbl_unidades_gst_indicators order by id desc

						insert into 
								sistemas.dbo.disponibilidad_tbl_mtto_log_gst_indicators
								select
											-- null,		--		id_control bigint identity(1,1),
											@id_unidad ,
											@total_llantas ,
											@tipo_unidad ,
											@id_tipo_motor ,
											@id_marca_unidad ,
											@id_tipo_unidad ,
											@kms_acumulados ,
											@r_f_v ,
											@id_centro_costo ,
											@llantas_extras ,
											@no_permiso ,
											@no_inciso_seguro ,
											@id_operador ,
											@kms_servicio_ultimo ,
											@kms_servicio_sig ,
											@id_area ,
											@id_depto ,
											@kms_inicio_ciclo ,
											@secuencia_inicio ,
											@idcia ,
											@hrs_inicio_ciclo ,
											@no_cilindros ,
											@prov_carroceria ,
											@secuencia_ult ,
											@secuencia_sig ,
											@no_circula ,
											@doble_no_circula ,
											@id_tercero ,
											@id_status ,
											@status_comm ,
											@msg_pend ,
											@id_ruta ,
											@id_area_viaje ,
											@no_viaje ,
											@id_pedido ,
											@id_flota ,
											@id_origen ,
											@id_destino ,
											@id_remitente ,
											@id_destinatario ,
											@id_macro_ult ,
											@costo_tenencia ,
											@no_poliza ,
											@prima_neta ,
											@recargos ,
											@der_poliza ,
											@iva ,
											@costo_unidad ,
											@costo_carroceria ,
											@ultima_tenencia ,
											@ultimo_refrendo ,
											@ultimo_permiso ,
											@vence_seguro ,
											@fecha_kms ,
											@fecha_servicio_ultimo ,
											@fecha_servicio_sig ,
											@fecha_adquisicion ,
											@fecha_ultima_rev ,
											@fecha_inicio_ciclo ,
											@fecha_emision ,
											@fecha_desde ,
											@fecha_hasta ,
											@fecha_adq_carr ,
											@fecha_estatus ,
											@fecha_baja ,
											@posdate ,
											@f_real_salida ,
											@f_prog_llegada ,
											@f_entrada_patio ,
											@f_mismapos ,
											@TEA ,
											@rendimiento_esperado ,
											@lts_combustible ,
											@lts_aceite ,
											@kms_por_dia ,
											@dim_ancho ,
											@dim_largo ,
											@dim_altura ,
											@rendimiento_aceite ,
											@hrs_acumuladas ,
											@hrs_por_dia ,
											@hrs_servicio_ultimo ,
											@hrs_servicio_sig ,
											@proveedor ,
											@peso_unidades ,
											@lts_relleno ,
											@lts_saldocomb ,
											@no_serie ,
											@modelo ,
											@id_ciclo_preventivo ,
											@placas ,
											@observaciones ,
											@no_economico ,
											@id_servicio_ultimo ,
											@id_servicio_sig ,
											@id_servicio_inicio ,
											@tarjeta_circulacion ,
											@no_factura ,
											@tipo_motor ,
											@potencia_motor ,
											@pulgadas_cubicas ,
											@no_motor ,
											@color ,
											@no_carroceria ,
											@obs_carroceria ,
											@fecha_vence_placas ,
											@poliza ,
											@fecha_vence_tenencia ,
											@status_unidad ,
											@viaje ,
											@fecha_vence_seguro ,
											@id_thermo ,
											@fecha_vence_verificacion ,
											@no_kit ,
											@desc_marcamotor ,
											@desc_provcarroceria ,
											@desc_provunidad ,
											@tarjeta_llave ,
											@no_tarjeta_llave ,
											@mctnumber ,
											@posicion ,
											@poslat ,
											@poslon ,
											@operador ,
											@id_unidad_as ,
											@id_remolque1 ,
											@id_dolly ,
											@id_remolque2 ,
											@origen ,
											@destino ,
											@id_despachador ,
											@tipo_combustible ,
											@estado ,
											@estado_adquisicion ,
											@datos_generales ,
											@tipo_moneda ,
											@forma_pago ,
											@kms_hrs ,
											@hrs_automaticas ,
											@estatus ,
											@tipo_programacion ,
											@equipo_omnitracs ,
											@tercero ,
											@causa_baja ,
											@tipo ,
											@status ,
											@status_caja1 ,
											@status_caja2 ,
											@en_patio ,
											@mismapos ,
											@f_status_unidad ,
											@subcta ,
											@tipomacro_ult ,
											@kms_actuales ,
											@ruta ,
											@peso_bruto ,
											@fecha_instalacion ,
											@kmshrsinicialaparmed ,
											@no_medidor ,
											@tipo_medida ,
											@id_operador2 ,
											@hrs_actuales ,
											@id_ingreso ,
											@fecha_ingreso ,
											@kms_hrs_actuales ,
											@fecha_disponible ,
											@id_plazadestino ,
											@id_tipo_operacion ,
											@id_movimiento ,
											@clas_uni ,
											@cta_consumo ,
											@id_status_rem ,
											@ultimo_id_destinatario ,
											@ultimo_id_area_viaje ,
											@ultimo_no_viaje ,
											@id_estacion ,
											@ultimo_id_areapedido ,
											@ultimo_id_pedido ,
											@origen_status ,
											@cargadovacio ,
											@kms_ultima_rev ,
											@id_transponder ,
											@num_soporte ,
											@vehunitnum ,
											@nacionalidad ,
											@id_localidaddestino ,
											@id_circuito ,
											@id_compania ,
											@kms_arribo ,
											@ultimo_id_pedidopk ,
											@alarmasnuevas ,
											@mensajesnuevos ,
											@id_linearem1 ,
											@id_linearem2 ,
											@modulo ,
											@id_asignacion ,
											@kms_actualesvale ,
											@kms_recorridosvale ,
											@modelo_unidad ,
											@madre ,
											@hija ,
											@asignado ,
											@unidad_asignado ,
											@hija_asignada ,
											@FECHA_LOGISTICA ,
											@valor_convenido ,
											@deducible_xrobo ,
											@deducible_xdmateriales ,
											@faset ,
											@jefe_flota ,
											@id_plazasigdestino ,
											@id_sector ,
											@id_subflota ,
											@EM_email ,
											@no_orsan ,
											@valida_cargacomb ,
											@procesado ,
											@disponibilidad ,
											@capacidad_aliado ,
											@marca_cap_eje_delantero ,
											@marca_cap_eje_trasero ,
											@marca_modelo_tipo_transmision ,
											@marca_suspension ,
											@medida_llanta ,
											@tipo_ejes ,
											@tipo_suspension ,
											@marca_aliado ,
											@ano_fabricacion ,
											@id_histograma_sig ,
											@id_histograma_ultimo ,
											@fecha_fin_garantia ,
											@id_accion ,
											@id_project ,
											@ccosto ,
											@id_tipo_servicio_unidad ,
											@poliza2 ,
											@no_inciso_seguro2 ,
											@vence_seguro2 ,
											@id_aseguradora ,
											@cta_mayor ,

												-- NOTE AddOns for internal work 
											 null,		--		id_ref	int null,
											 null,		--		id_gst int null ,
											 @action,		--		status_query varchar(50) null,
											 1,		--		status_active tinyint default 1 null,
											 current_timestamp,		--		created datetime ,
											 current_timestamp		--		modified datetime 

			-- NOTE select ident_current('sistemas.dbo.disponibilidad_tbl_mtto_log_gst_indicators')
			-- after that
			-- NOTE then insert into log table 
						insert into sistemas.dbo.disponibilidad_tbl_unidades_gst_indicators
							select 
								 @id_unidad
								,@id_status
								,1
								,ident_current('sistemas.dbo.disponibilidad_tbl_mtto_log_gst_indicators') -- NOTE check for the control insertion id 
								,''										-- Description text
								,@fecha_hasta					-- Compromise Date 
								,current_timestamp
								,current_timestamp
								,1
					end -- 2nd inner If
    end  -- if block
	end 
-- NOTE END TRIGGER




-- --------------------------------------------------------------------------------------------------------------------- --
-- UPDATE For main view in Disponibilidad -- Add Historical view 
-- --------------------------------------------------------------------------------------------------------------------- --

-- Views 
-- disponibilidad_view_status_gst_indicators
-- -


















--		moreno tigers

use sistemas
-- enable 
-- disable 
trigger disponibilidad_update_unidad_status
ON sistemas.dbo.disponibilidad_tbl_unidades_gst_indicators



select * from sistemas.dbo.disponibilidad_tbl_unidades_gst_indicators

insert into sistemas.dbo.disponibilidad_tbl_unidades_gst_indicators
 VALUES ( 'DH0009', 4, 1, 'adsasda', '07/13/2020', '2020-07-13 22:48:45', '2020-07-13 22:48:45',1) 




alter trigger [gstdb].[dbo].[hesatec_unidad_en_taller]
	on [gstdb].[dbo].[mtto_orden] for insert
as
	declare @id_unidad  varchar(10)

	select  @id_unidad = id_unidad from inserted
	-- Actualiza el estatus de la unidad a "En Taller"
	update
			gstdb.dbo.mtto_unidades
		set
			id_status=4
		where
			id_unidad=@id_unidad
--insert first
	update
			pruebas_tbk.dbo.mtto_unidades
		set
			id_status=4
		where
			id_unidad=@id_unidad

--select id_unidad from gstdb.dbo.mtto_unidades group by id_unidad



use gstdb

alter trigger [dbo].[hesatec_unidad_disponible]
	on [dbo].[mtto_orden] for update
as
	declare @id_unidad varchar(10)
	declare @estado char(1)

	select @id_unidad = id_unidad, @estado=estado  from inserted

	if (@estado='L' or @estado='N')
		-- Actualiza el estatus de la unidad a "Disponible"
		update gstdb.dbo.mtto_unidades set id_status=6 where id_unidad=@id_unidad
		update pruebas_tbk.dbo.mtto_unidades set id_status=6 where id_unidad= @id_unidad


--		select id_status, *from pruebas_tbk.dbo.mtto_unidades
-- select *from pruebas_tbk.dbo.mtto_unidades
-- select *from pruebas_tbk.dbo.desp_status




-- truncate table sistemas.dbo.disponibilidad_tbl_unidades_gst_indicators
--
--select id_status,id_unidad from gstdb_pba.dbo.mtto_unidades where tipo_unidad = 1 and id_unidad = 'TT1210'
--
--select * from sistemas.dbo.disponibilidad_tbl_unidades_gst_indicators where unidad = 'TT1210'
--
--select id_status,id_unidad from gstdb.dbo.mtto_unidades where tipo_unidad = 1 and id_unidad = 'TT1210'
--
--
--select * from sistemas.dbo.disponibilidad_view_rpt_unidades_gst_indicators where compromise is not null
-- ======================================================================================================== --
-- -- Detail
-- ======================================================================================================== --
use sistemas
-- select * from sistemas.dbo.disponibilidad_view_historical_gst_indicators
IF OBJECT_ID ('disponibilidad_view_historical_gst_indicators', 'V') IS NOT NULL
    DROP VIEW disponibilidad_view_historical_gst_indicators;
-- now build the view
create view disponibilidad_view_historical_gst_indicators
with encryption
as
	select
			 row_number() over(order by "historical".unidad) as 'id'
			,"historical".unidad
			,"status".nombre as 'estatus'
			,"historical".description as 'descripcion'
			,"historical".compromise as 'compromiso'
			,convert(nvarchar(MAX), "historical".created, 23) as 'creacion'
	from
			sistemas.dbo.disponibilidad_tbl_unidades_gst_indicators as "historical"
	inner join
		gstdb.dbo.desp_status as "status"
	on
		"historical".id_status = "status".id_status
	where
		"historical".status = 1



-- select * from gstdb.dbo.desp_flotas
--

-- ======================================================================================================== --
-- Unidades Sin Documentacion
-- ======================================================================================================== --
use sistemas

IF OBJECT_ID ('georeference_view_positions_hist_gst_indicators', 'V') IS NOT NULL
    DROP VIEW georeference_view_positions_hist_gst_indicators;
-- now build the view
create view georeference_view_positions_hist_gst_indicators
with encryption
as
with "position" as (
select
		 "area".id_area as 'id_area'
		,"area".ciudad as 'Area'
		,"mu".id_unidad as "Unidad"
		,"da".id_asignacion as 'Asignacion'
		,"da".status_asignacion
		,"da".seguimiento_actual
		,"da".fecha_ingreso as "FechaAsignacion"
		,"tv".no_viaje as 'Viaje'
		,(	select
				 count("guia".num_guia)
			from
					gstdb.dbo.trafico_viaje  as "viaje"
			left join
					gstdb.dbo.trafico_guia  as "guia"
				on
					"viaje".no_viaje = "guia".no_viaje and "viaje".id_area = "guia".id_area
			where
				"viaje".no_viaje = "tv".no_viaje
		 ) as 'Cporte'
		,(
			select
					',' + "guiax".num_guia
			from
					gstdb.dbo.trafico_guia as "guiax"
			where
					"guiax".no_viaje = "tv".no_viaje and "guiax".id_area = "tv".id_area
			for xml path('')
		 ) as 'Guias'
		,"tv".f_despachado as "Despachado"
		,"pp".nombre as "Operador"
		,"dp".poslat as 'Latitud'
		,"dp".poslon as 'Longitud'
		,"dp".posdate as 'FechaPosition'
from
		gstdb.dbo.mtto_unidades as "mu"
inner join
		gstdb.dbo.general_area as "area"
	on
		"area".id_area = "mu".id_area
left join
		gstdb.dbo.desp_posicion_unidad as "dp" on "dp".id_unidad = "mu".id_unidad and "dp".id_area = "mu".id_area
	and
		dp.posdate = ( select max("pst".posdate) from gstdb.dbo.desp_posicion_unidad as "pst" where "pst".id_unidad = dp.id_unidad and "pst".id_area = dp.id_area )
left join
		gstdb.dbo.desp_asignacion as "da" on "da".id_unidad = "mu".id_unidad and "da".id_area = "mu".id_area
	and
		"da".no_viaje > 0 and "da".id_seguimiento = 1 -- and da.seguimiento_actual = 1
--		"da".no_viaje = ( select max("trip".no_viaje) from gstdb.dbo.trafico_viaje as "trip" where "trip".id_unidad = "mu".id_unidad and "trip".id_area = "mu".id_area )
--	and
--		"da".no_viaje > 0
	and
		da.id_asignacion = ( select max("asg".id_asignacion) from gstdb.dbo.desp_asignacion as "asg" where "asg".id_unidad = dp.id_unidad and "asg".id_area = dp.id_area )
left join
		gstdb.dbo.trafico_viaje as "tv" on "tv".id_unidad = "mu".id_unidad and "tv".id_area = "mu".id_area -- and tv.viajeactual = 'S'
left join
		gstdb.dbo.personal_personal as "pp" on "pp".id_personal = "tv".id_personal
where
		"mu".id_tipo_unidad = 1
--	and
--		da.status_asignacion not in (3,4) -- 1 asignaado[] , 2 transito -- 3 viaje finalizaso, 4 a.cancelado -- null [en movimiento sin viaje en lis]
--	and
--		dp.posdate = ( select max("pst".posdate) from gstdb.dbo.desp_posicion_unidad as "pst" where "pst".id_unidad = dp.id_unidad and "pst".id_area = dp.id_area )
--	and
--		da.id_asignacion = ( select max("asg".id_asignacion) from gstdb.dbo.desp_asignacion as "asg" where "asg".id_unidad = dp.id_unidad and "asg".id_area = dp.id_area )
	and
		tv.no_viaje = ( select max("trip".no_viaje) from gstdb.dbo.trafico_viaje as "trip" where "trip".id_unidad = "mu".id_unidad and "trip".id_area = "mu".id_area )
)
select
		  row_number() over(order by "position".unidad) as 'id'
		 ,case
			"position".status_asignacion
				when 1 then 'asignado'
				when 2 then 'transito'
				when 3 then 'finalizado'
				when 4 then 'cancelado'
			end as 'AsignacionDesc'
		 ,case
			"position".seguimiento_actual
				when 0 then 'sin viaje asignado'
				when 1 then 'viaje asignado'
			end as 'Seguimiento'
		 ,case
		 -- ==================================================================================== --
		 	when
		 		("position".status_asignacion = 3 or "position".status_asignacion = 4) and "position".seguimiento_actual = 0 and "position".Cporte = 0
		 	then
		 		'Despachado sin Documentacion' -- 'Unidad sin Carta Porte' -- Unidad sin asignacion , sin viaje actual
		 	when
		 		"position".status_asignacion = 1 and "position".seguimiento_actual = 1 and "position".Cporte = 0
		 	then
		 		'Despachado sin Documentacion' --'Pendiente de Carta Porte' -- pedido + unidad [proceso]
		 	when
		 		"position".status_asignacion = 2 and "position".seguimiento_actual = 1 and "position".Cporte = 0
		 	then
		 		'Despachado sin Documentacion' --'Viaje sin Carta Porte'
		 -- ==================================================================================== --
		 	when
		 		"position".status_asignacion = 1 and "position".seguimiento_actual = 1 and "position".Cporte = 1
		 	then
		 		'Despachado con Documentos' --'Pendiente de Carta Porte' -- pedido + unidad [proceso]
		 	when
		 		"position".status_asignacion = 2 and "position".seguimiento_actual = 1 and "position".Cporte = 1
		 	then
		 		'Despachado con Documentos' --'Viaje sin Carta Porte'
		 -- ==================================================================================== --
		 	when
		 		("position".status_asignacion = 3 or "position".status_asignacion = 4) and "position".seguimiento_actual = 0 and "position".Cporte > 0
		 	then
		 		'Sin viaje actual'
		 	when
		 		("position".status_asignacion = 3 or "position".status_asignacion = 4) and "position".seguimiento_actual = 1
		 	then
		 		'Error en Sistema'
		 	when
		 		"position".FechaPosition is null
		 	then
		 		'Unidad sin posicion'
		 	when
		 			"position".status_asignacion is null and "position".seguimiento_actual is null
		 		and
		 			"position".Latitud between "localization".min_latitud and "localization".max_latitud
		 		and
		 			"position".Longitud between "localization".min_longitud and "localization".max_longitud
		 	then
		 		'Unidad en Base'
		 end as
		 		'StatusDescription'
		 ,case
		 -- ==================================================================================== --
		 	when
		 		("position".status_asignacion = 3 or "position".status_asignacion = 4) and "position".seguimiento_actual = 0 and "position".Cporte = 0
		 	then
		 		0 -- 'Unidad sin Carta Porte' -- Unidad sin asignacion , sin viaje actual
		 	when
		 		"position".status_asignacion = 1 and "position".seguimiento_actual = 1 and "position".Cporte = 0
		 	then
		 		0 --'Pendiente de Carta Porte' -- pedido + unidad [proceso]
		 	when
		 		"position".status_asignacion = 2 and "position".seguimiento_actual = 1 and "position".Cporte = 0
		 	then
		 		0 --'Viaje sin Carta Porte'
		 -- ==================================================================================== --
		 	when
		 		"position".status_asignacion = 1 and "position".seguimiento_actual = 1 and "position".Cporte = 1
		 	then
		 		1 --'Pendiente de Carta Porte' -- pedido + unidad [proceso]
		 	when
		 		"position".status_asignacion = 2 and "position".seguimiento_actual = 1 and "position".Cporte = 1
		 	then
		 		1 --'Viaje sin Carta Porte'
		 -- ==================================================================================== --
		 	when
		 		("position".status_asignacion = 3 or "position".status_asignacion = 4) and "position".seguimiento_actual = 0 and "position".Cporte > 0
		 	then
		 		2 --'Sin viaje actual'
		 	when
		 		("position".status_asignacion = 3 or "position".status_asignacion = 4) and "position".seguimiento_actual = 1
		 	then
		 		3 --'Error en Sistema'
		 	when
		 		"position".FechaPosition is null
		 	then
		 		4 --'Unidad sin posicion'
		 	when
		 			"position".status_asignacion is null and "position".seguimiento_actual is null
		 		and
		 			"position".Latitud between "localization".min_latitud and "localization".max_latitud
		 		and
		 			"position".Longitud between "localization".min_longitud and "localization".max_longitud
		 	then
		 		5 --'Unidad en Base'
		 end as
		 		'statdesc'
		 ,case
		 	when
		 		(
			 		"position".Latitud between "localization".min_latitud and "localization".max_latitud
			 	and
			 		"position".Longitud between "localization".min_longitud and "localization".max_longitud
			 	)
			 then
			 	'inside base'
			 else
			 	'outside base'
		 end as "enBase"
		 ,case
		 	when
		 		(
			 		"position".Latitud between "localization".min_latitud and "localization".max_latitud
			 	and
			 		"position".Longitud between "localization".min_longitud and "localization".max_longitud
			 	)
			 then
			 	1 -- true is in base
			 else
			 	0 -- is not in base
		 end as "inBase"
		,case
			when
				(
					"position".Asignacion is not null
				and
					"position".Viaje is null
				)
			then
				'Viaje sin despacho'
			when
				(
					"position".Asignacion is null
				and
					"position".Viaje is null
				)
			then
				'En Movimiento sin despacho LIS'
		 else
		 		'En regla'
		end as 'DescriptionViaje'
		,case
			when
				(
					"position".Asignacion is not null
				and
					"position".Viaje is null
				)
			then
				1 --'Viaje sin despacho'
			when
				(
					"position".Asignacion is null
				and
					"position".Viaje is null
				)
			then
				2 -- 'En Movimiento sin despacho LIS'
		 else
		 		0 -- 'En regla'
		end as 'traceroute'
		,"position".id_area
		,"position".Area
		,"position".Unidad
		,"position".Asignacion
		,"position".status_asignacion
		,"position".seguimiento_actual
		,"position".FechaAsignacion
		,"position".Viaje
		,"position".Cporte
		,substring("position".Guias,2,len("position".Guias)) as 'Guias'
		,"position".Despachado
		,"position".Operador
		,"position".Latitud
		,"position".Longitud
		,"position".FechaPosition
		,"localization".base
		,"localization".max_latitud
		,"localization".min_latitud
		,"localization".max_longitud
		,"localization".min_longitud
from
		"position" --as "position"
cross join
		sistemas.dbo.georeference_view_positions_gst_indicators as "localization"

--
--
--where
--		"position".Unidad in ('TT1321')
--order by
--		Unidad
--	and
--		"position".Latitud between "localization".min_latitud and "localization".max_latitud
--	and
--		"position".Longitud between "localization".min_longitud and "localization".max_longitud




--select max("trip".no_viaje),id_area,viajeactual from gstdb.dbo.trafico_viaje as "trip" where "trip".id_unidad = 'TT1321'
--group by
----		no_viaje,
--		id_area,viajeactual
--

--
--select id_area,* from gstdb.dbo.mtto_unidades where id_unidad = 'TT1321'
--
--
--
--select * from gstdb.dbo.mtto_unidades where id_unidad = 'TT1321'
--
--select * from gstdb.dbo.desp_posicion_unidad where id_unidad = 'TT1321'
--
--select no_viaje,* from gstdb.dbo.desp_asignacion where id_unidad = 'TT1321'
--

select * from sistemas.dbo.georeference_view_positions_hist_gst_indicators where Area = base and statdesc in (0) and inBase = 0
union all
select * from sistemas.dbo.georeference_view_positions_hist_gst_indicators where Area = base and statdesc = 1


use sistemas

select
		 "holidays".id
		,"holidays".cyear
		,"holidays".id_month
		,"holidays"."month"
		,"holidays".period
		,"holidays"._period
		,"holidays".first_day_in_month
		,"holidays".last_day_in_month
		,"holidays".is_current
		,"holidays".hasSun
		,"holidays".hasFullSun
		,"holidays".hasCurrentHoliday
		,"holidays".hisFullHoliday
		,"holidays".labDays
		,"holidays".labFullDays
		,"holidays".labRestDays
from sistemas.dbo.ingresos_costos_gst_holidays as "holidays"




--
-- where UNIDAD = 'TT1321'
--order by UNIDAD


--select * from gstdb.dbo.desp_asignacion where no_viaje = 0

use sistemas
   -- select * from sistemas.dbo.georeference_view_positions_documents_gst_indicators where Area = 'MEXICALI' order by unidad

IF OBJECT_ID ('georeference_view_positions_documents_gst_indicators', 'V') IS NOT NULL
    DROP VIEW georeference_view_positions_documents_gst_indicators;
-- now build the view
create view georeference_view_positions_documents_gst_indicators
with encryption
as
select
	 "pos".id
	,"pos".AsignacionDesc
	,"pos".Seguimiento
	,"pos".StatusDescription
	,"pos".statdesc
	,"pos".enBase
	,"pos".inBase
	,"pos".DescriptionViaje
	,"pos".traceroute
	,"pos".id_area
	,"pos".Area
	,"pos".Unidad
	,"pos".Asignacion
	,"pos".status_asignacion
	,"pos".seguimiento_actual
	,"pos".FechaAsignacion
	,"pos".Viaje
	,"pos".Guias
	,"pos".Despachado
	,"pos".Operador
	,"pos".Latitud
	,"pos".Longitud
	,"pos".FechaPosition
--	,"pos".id_geocerca
	,"pos".base
	,"pos".max_latitud
	,"pos".min_latitud
	,"pos".max_longitud
	,"pos".min_longitud
--	,current_timestamp as 'created'
--	,1 as 'status'
--	,1 as 'control_id'
from
	sistemas.dbo.georeference_view_positions_hist_gst_indicators as "pos"
where
	base = Area --and Latitud is null
	and
		(
			statdesc = 0
		and
			inBase = 0
		or
			statdesc = 1
		)



--	select
--			 "geor".Area
--			,count("geor".Unidad) as "unidades"
--		    ,"geor".StatusDescription
----		    ,cast(substring( convert(nvarchar(MAX), "geor".Despachado, 112) , 1, 6 ) as int) as 'despacho'
--		    ,cast(substring( convert(nvarchar(MAX), "geor".created, 112) , 1, 6 ) as int) as 'periodo'
--	from
----			sistemas.dbo.georeference_view_positions_documents_gst_indicators as "geor"
--			sistemas.dbo.georeference_tbl_positions_documents_gst_indicators as "geor"
--	group by
--			 "geor".Area
--		    ,"geor".StatusDescription
----		    ,cast(substring( convert(nvarchar(MAX), "geor".Despachado, 112) , 1, 6 ) as int)
--			,cast(substring( convert(nvarchar(MAX), "geor".created, 112) , 1, 6 ) as int)


-- ============================================================================================================ --

use sistemas
   -- select * from sistemas.dbo.georeference_view_positions_dash_main_gst_indicators order by unidad
IF OBJECT_ID ('georeference_view_positions_dash_main_gst_indicators', 'V') IS NOT NULL
    DROP VIEW georeference_view_positions_dash_main_gst_indicators;
-- now build the view
create view georeference_view_positions_dash_main_gst_indicators
with encryption
as
	select
			 row_number() over(order by "geor".Area) as 'id'
			,"geor".Area
			,count("geor".Unidad) as "unidades"
		    ,"geor".StatusDescription
		    ,cast(substring( convert(nvarchar(MAX), "geor".Despachado, 112) , 1, 6 ) as int) as 'despacho'
		    ,cast(substring( convert(nvarchar(MAX), current_timestamp , 112) , 1, 6 ) as int) as 'periodo'
	from
			sistemas.dbo.georeference_view_positions_documents_gst_indicators as "geor"
--			sistemas.dbo.georeference_tbl_positions_documents_gst_indicators as "geor"
	group by
			 "geor".Area
		    ,"geor".StatusDescription
		    ,cast(substring( convert(nvarchar(MAX), "geor".Despachado, 112) , 1, 6 ) as int)
--			,cast(substring( convert(nvarchar(MAX), current_timestamp , 112) , 1, 6 ) as int)


-- =================================================================================================== --
-- =============  Build of store table for Description and Compromise Date
-- =================================================================================================== --

use sistemas
-- Ambagasdowa of the future check the Capacity for build and manual historical by id add foreign key for that
ALTER PROCEDURE georeference_store_control_positions
--    @control_id int,
AS
	declare @id_control int
    SET NOCOUNT ON;
	insert into sistemas.dbo.georeference_tbl_positions_control_gst_indicators
   		select
				 cast(current_timestamp as date) as 'inserted'
				,cast(substring( convert(nvarchar(MAX), CURRENT_TIMESTAMP, 112) , 1, 6 ) as int) as 'periodo'
				,day(CURRENT_TIMESTAMP) as 'day'
				,current_timestamp as 'created'
				,1 as 'status'
	-- get the last id of us![Konami?}
   	select @id_control = @@IDENTITY
--   	select @id_control
 	-- XD
	insert into sistemas.dbo.georeference_tbl_positions_documents_gst_indicators
	select
		 "pos".AsignacionDesc
		,"pos".Seguimiento
		,"pos".StatusDescription
		,"pos".statdesc
		,"pos".enBase
		,"pos".inBase
		,"pos".DescriptionViaje
		,"pos".traceroute
		,"pos".id_area
		,"pos".Area
		,"pos".Unidad
		,"pos".Asignacion
		,"pos".status_asignacion
		,"pos".seguimiento_actual
		,"pos".FechaAsignacion
		,"pos".Viaje
		,"pos".Guias
		,"pos".Despachado
		,"pos".Operador
		,"pos".Latitud
		,"pos".Longitud
		,"pos".FechaPosition
		,"pos".base
		,"pos".max_latitud
		,"pos".min_latitud
		,"pos".max_longitud
		,"pos".min_longitud
		,@id_control as 'control_id'
		,cast(current_timestamp as date) as 'inserted'
		,cast(substring( convert(nvarchar(MAX), CURRENT_TIMESTAMP, 112) , 1, 6 ) as int) as 'periodo'
		,day(CURRENT_TIMESTAMP) as 'day'
		,current_timestamp as 'created'
		,1 as 'status'
	from
		sistemas.dbo.georeference_view_positions_hist_gst_indicators as "pos"
	where
		base = Area
	and
		(
			statdesc = 0
		and
			inBase = 0
		or
			statdesc = 1
		)

 -- =================================================================================================== --



use [sistemas]
-- go
IF OBJECT_ID('sistemas.dbo.georeference_tbl_positions_documents_gst_indicators', 'U') IS NOT NULL
  DROP TABLE sistemas.dbo.georeference_tbl_positions_documents_gst_indicators
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on
-- go
create table georeference_tbl_positions_documents_gst_indicators(
		 id								int 			identity(1,1)
		,AsignacionDesc			char(55) 		null
		,Seguimiento				char(55)		null
		,StatusDescription		char(55)		null
		,statdesc					int				null
		,enBase					char(55)		null
		,inBase					int				null
		,DescriptionViaje			char(80)		null
		,traceroute				int				null
		,id_area				int		null
		,Area						char(55)		null
		,Unidad					char(55)		null
		,Asignacion				int				null
		,status_asignacion		int				null
		,seguimiento_actual		int				null
		,FechaAsignacion			datetime		null
		,Viaje					int				null
		,Guias					text			null
		,Despachado				datetime		null
		,Operador					char(120)		null
		,Latitud					decimal			null
		,Longitud					decimal			null
		,FechaPosition			datetime		null
--		,id_geocerca				bigint			null
		,base						char(55)		null
		,max_latitud				decimal			null
		,min_latitud				decimal			null
		,max_longitud				decimal			null
		,min_longitud				decimal			null
		,control_id				int				null
		,inserted			date			null
		,periodo			int				null
		,"day"				int				null
		,created			datetime		null
		,status				tinyint 		default 1 null
) on [primary]
-- go
set ansi_padding off
-- go
-- test

--
--
----declare @tbl_awebo table
--select * from sistemas.dbo.georeference_tbl_positions_documents_gst_indicators_respaldo
--use [sistemas]
---- go
--IF OBJECT_ID('sistemas.dbo.georeference_tbl_positions_documents_gst_indicators_respaldo', 'U') IS NOT NULL
--  DROP TABLE sistemas.dbo.georeference_tbl_positions_documents_gst_indicators_respaldo
--set ansi_nulls on
---- go
--set quoted_identifier on
---- go
--set ansi_padding on
---- go
--create table georeference_tbl_positions_documents_gst_indicators_respaldo(
--		 id						int
--		,AsignacionDesc			char(55) 		null
--		,Seguimiento				char(55)		null
--		,StatusDescription		char(55)		null
--		,statdesc					int				null
--		,enBase					char(55)		null
--		,inBase					int				null
--		,DescriptionViaje			char(80)		null
--		,traceroute				int				null
--		,id_area				int		null
--		,Area						char(55)		null
--		,Unidad					char(55)		null
--		,Asignacion				int				null
--		,status_asignacion		int				null
--		,seguimiento_actual		int				null
--		,FechaAsignacion			datetime		null
--		,Viaje					int				null
----		,Guias					text			null
--		,Despachado				datetime		null
--		,Operador					char(120)		null
--		,Latitud					decimal			null
--		,Longitud					decimal			null
--		,FechaPosition			datetime		null
----		,id_geocerca				bigint			null
--		,base						char(55)		null
--		,max_latitud				decimal			null
--		,min_latitud				decimal			null
--		,max_longitud				decimal			null
--		,min_longitud				decimal			null
--		,control_id				int				null
--		,inserted			date			null
--		,periodo			int				null
--		,"day"				int				null
--		,created			datetime		null
--		,status				tinyint 		default 1 null
--);
--set ansi_padding off
--
--insert into sistemas.dbo.georeference_tbl_positions_documents_gst_indicators
--	select
----		 "tbl".id
--		 "tbl".AsignacionDesc
--		,"tbl".Seguimiento
--		,"tbl".StatusDescription
--		,"tbl".statdesc
--		,"tbl".enBase
--		,"tbl".inBase
--		,"tbl".DescriptionViaje
--		,"tbl".traceroute
--		,"tbl".id_area
--		,"tbl".Area
--		,"tbl".Unidad
--		,"tbl".Asignacion
--		,"tbl".status_asignacion
--		,"tbl".seguimiento_actual
--		,"tbl".FechaAsignacion
--		,"tbl".Viaje
--		,'' as 'Guias'
--		,"tbl".Despachado
--		,"tbl".Operador
--		,"tbl".Latitud
--		,"tbl".Longitud
--		,"tbl".FechaPosition
--		,"tbl".base
--		,"tbl".max_latitud
--		,"tbl".min_latitud
--		,"tbl".max_longitud
--		,"tbl".min_longitud
--		,"tbl".control_id
--		,"tbl".inserted
--		,"tbl".periodo
--		,"tbl".day
--		,"tbl".created
--		,"tbl".status
--	from georeference_tbl_positions_documents_gst_indicators_respaldo as "tbl"
--
--




use [sistemas]
--use master
--
-- delete from sistemas.dbo.georeference_tbl_positions_control_gst_indicators where id in (3,4)
-- select * from georeference_tbl_positions_control_gst_indicators
-- go
IF OBJECT_ID('georeference_tbl_positions_control_gst_indicators', 'U') IS NOT NULL
  DROP TABLE georeference_tbl_positions_control_gst_indicators
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on
	create table georeference_tbl_positions_control_gst_indicators(
		 id					bigint 			identity(1,1)
		,inserted			date			null
		,periodo			int				null
		,"day"				int				null
		,created			datetime		null
		,status				tinyint 		default 1 null
) on [primary]
-- go
set ansi_padding off



--CREATE TABLE sistemas.dbo.foo(ID INT IDENTITY(1,1), name SYSNAME);
--
----We can capture IDENTITY values in a table variable for further consumption.
--
--DECLARE @IDs TABLE(ID INT);
--
---- minor change to INSERT statement; add an OUTPUT clause:
--drop table sistemas.dbo.foo
--INSERT sistemas.dbo.foo(name)
--  OUTPUT inserted.ID INTO @IDs(ID)
--SELECT N'Fred'
--UNION ALL
--SELECT N'Bob';




use sistemas
-- select * from sistemas.dbo.georeference_view_positions_gst_indicators
IF OBJECT_ID ('georeference_view_positions_gst_indicators', 'V') IS NOT NULL
    DROP VIEW georeference_view_positions_gst_indicators;
-- now build the view
create view georeference_view_positions_gst_indicators
with encryption
as
with "pos_lat_max" as (
		select
			 "dg".id_geocerca
			,case "dg".id_geocerca
				when 146
					then
						'CUAUTITLAN 1'
				when 147
					then
						'CUAUTITLAN 2'
				when 150
					then
						'MACUSPANA'
				when 151
					then 'RAMOS ARIZPE'
				when
					154
					then
						'TULTITLAN'
				else
					ltrim(rtrim(replace("dg".nombre,'BASE' , '')))
			 end as 'base'
			,max("dp".latitud) as 'latitud'
		from
			gstdb.dbo.desp_geocercas as "dg"
		left join
			gstdb.dbo.desp_puntos_geocercas as "dp"
		on
			"dp".id_geocerca = "dg".id_geocerca
		where
			"dg".id_geocerca in (148,149,150,151,152,153,154)
		group by
			 "dg".id_geocerca
			,"dg".nombre
		union all
		select
			 "dg".id_geocerca
			,case "dg".id_geocerca
				when 146
					then
						'CUAUTITLAN'
				when 147
					then
						'CUAUTITLAN'
				when 150
					then
						'MACUSPANA'
				when 151
					then 'RAMOS ARIZPE'
				when
					154
					then
						'TULTITLAN'
				else
					ltrim(rtrim(replace("dg".nombre,'BASE' , '')))
			 end as 'base'
			,max("dp".latitud) as 'latitud'
		from
			gstdb.dbo.desp_geocercas as "dg"
		left join
			gstdb.dbo.desp_puntos_geocercas as "dp"
		on
			"dp".id_geocerca = "dg".id_geocerca
		where
			"dg".id_geocerca in (146)
		group by
			 "dg".id_geocerca
			,"dg".nombre
),"pos_lat_min" as (
		select
			 "dg".id_geocerca
			,case "dg".id_geocerca
				when 146
					then
						'CUAUTITLAN 1'
				when 147
					then
						'CUAUTITLAN 2'
				when 150
					then
						'MACUSPANA'
				when 151
					then 'RAMOS ARIZPE'
				when
					154
					then
						'TULTITLAN'
				else
					ltrim(rtrim(replace("dg".nombre,'BASE' , '')))
			 end as 'base'
			,min("dp".latitud) as 'latitud'
		from
			gstdb.dbo.desp_geocercas as "dg"
		left join
			gstdb.dbo.desp_puntos_geocercas as "dp"
		on
			"dp".id_geocerca = "dg".id_geocerca
		where
			"dg".id_geocerca in (148,149,150,151,152,153,154)
		group by
			 "dg".id_geocerca
			,"dg".nombre
		union all
		select
			 146 as 'id_geocerca' --"dg".id_geocerca
			,case "dg".id_geocerca
				when 146
					then
						'CUAUTITLAN'
				when 147
					then
						'CUAUTITLAN'
				when 150
					then
						'MACUSPANA'
				when 151
					then 'RAMOS ARIZPE'
				when
					154
					then
						'TULTITLAN'
				else
					ltrim(rtrim(replace("dg".nombre,'BASE' , '')))
			 end as 'base'
			,min("dp".latitud) as 'latitud'
		from
			gstdb.dbo.desp_geocercas as "dg"
		left join
			gstdb.dbo.desp_puntos_geocercas as "dp"
		on
			"dp".id_geocerca = "dg".id_geocerca
		where
			"dg".id_geocerca in (147)
		group by
			 "dg".id_geocerca
			,"dg".nombre
),"pos_lon_max" as (
		select
			 "dg".id_geocerca
			,case "dg".id_geocerca
				when 146
					then
						'CUAUTITLAN 1'
				when 147
					then
						'CUAUTITLAN 2'
				when 150
					then
						'MACUSPANA'
				when 151
					then 'RAMOS ARIZPE'
				when
					154
					then
						'TULTITLAN'
				else
					ltrim(rtrim(replace("dg".nombre,'BASE' , '')))
			 end as 'base'
			,max("dp".longitud) as 'longitud'
		from
			gstdb.dbo.desp_geocercas as "dg"
		left join
			gstdb.dbo.desp_puntos_geocercas as "dp"
		on
			"dp".id_geocerca = "dg".id_geocerca
		where
			"dg".id_geocerca in (148,149,150,151,152,153,154)
		group by
			 "dg".id_geocerca
			,"dg".nombre
		union all
		select
			 "dg".id_geocerca
			,case "dg".id_geocerca
				when 146
					then
						'CUAUTITLAN'
				when 147
					then
						'CUAUTITLAN'
				when 150
					then
						'MACUSPANA'
				when 151
					then 'RAMOS ARIZPE'
				when
					154
					then
						'TULTITLAN'
				else
					ltrim(rtrim(replace("dg".nombre,'BASE' , '')))
			 end as 'base'
			,max("dp".longitud) as 'longitud'
		from
			gstdb.dbo.desp_geocercas as "dg"
		left join
			gstdb.dbo.desp_puntos_geocercas as "dp"
		on
			"dp".id_geocerca = "dg".id_geocerca
		where
			"dg".id_geocerca in (146)
		group by
			 "dg".id_geocerca
			,"dg".nombre
),"pos_lon_min" as (
		select
			 "dg".id_geocerca
			,case "dg".id_geocerca
				when 146
					then
						'CUAUTITLAN'
				when 147
					then
						'CUAUTITLAN'
				when 150
					then
						'MACUSPANA'
				when 151
					then 'RAMOS ARIZPE'
				when
					154
					then
						'TULTITLAN'
				else
					ltrim(rtrim(replace("dg".nombre,'BASE' , '')))
			 end as 'base'
			,min("dp".longitud) as 'longitud'
		from
			gstdb.dbo.desp_geocercas as "dg"
		left join
			gstdb.dbo.desp_puntos_geocercas as "dp"
		on
			"dp".id_geocerca = "dg".id_geocerca
		where
			"dg".id_geocerca in (148,149,150,151,152,153,154)
		group by
			 "dg".id_geocerca
			,"dg".nombre
		union all
		select
			 146 as 'id_geocerca' -- "dg".id_geocerca
			,case "dg".id_geocerca
				when 146
					then
						'CUAUTITLAN'
				when 147
					then
						'CUAUTITLAN'
				when 150
					then
						'MACUSPANA'
				when 151
					then 'RAMOS ARIZPE'
				when
					154
					then
						'TULTITLAN'
				else
					ltrim(rtrim(replace("dg".nombre,'BASE' , '')))
			 end as 'base'
			,min("dp".longitud) as 'longitud'
		from
			gstdb.dbo.desp_geocercas as "dg"
		left join
			gstdb.dbo.desp_puntos_geocercas as "dp"
		on
			"dp".id_geocerca = "dg".id_geocerca
		where
			"dg".id_geocerca in (147)
		group by
			 "dg".id_geocerca
			,"dg".nombre
)
select
--	 "pos_lat_max".id_geocerca
     row_number() over(order by "pos_lat_max".base) as 'id'
	,"pos_lat_max".base
	,"pos_lat_max".latitud as 'max_latitud'
	,"pos_lat_min".latitud as 'min_latitud'
	,"pos_lon_max".longitud as 'max_longitud'
	,"pos_lon_min".longitud as 'min_longitud'
from
	"pos_lat_max"
inner join
	"pos_lat_min"
on
	"pos_lat_max".id_geocerca = "pos_lat_min".id_geocerca
inner join
	"pos_lon_max"
on
	"pos_lat_max".id_geocerca = "pos_lon_max".id_geocerca
inner join
	"pos_lon_min"
on
	"pos_lat_max".id_geocerca = "pos_lon_min".id_geocerca



-- ==================================================================================================== --
-- update sistemas.dbo.georeference_tbl_positions_control_gst_indicators set inserted = '2019-03-12' where id = 1
--  Schematics for Unidades sin Documentos
--  	exec georeference_store_control_positions
  	select * from sistemas.dbo.georeference_tbl_positions_control_gst_indicators
	select control_id,inserted from sistemas.dbo.georeference_tbl_positions_documents_gst_indicators group by control_id,inserted

--	delete from sistemas.dbo.georeference_tbl_positions_control_gst_indicators where id in (2,5,6)
--	delete from sistemas.dbo.georeference_tbl_positions_documents_gst_indicators where control_id in (2)
--
-- ==================================================================================================== --


--	select
--			"viaje".id_area
--			,"area".ciudad as 'area'
--			,Count ("viaje".no_viaje) as 'total_viajes'
--			,"asignacion".id_seguimiento
--			,cast(substring( convert(nvarchar(MAX), "viaje".f_despachado, 112) , 1, 6 ) as int) as 'periodo'
--	from
--			gstdb.dbo.trafico_viaje as "viaje"
--	inner join
--			gstdb.dbo.desp_asignacion as "asignacion"
--		on
--			"viaje".no_viaje = "asignacion".no_viaje and "viaje".id_area = "asignacion".id_area --and "asignacion".id_seguimiento = 1 and "asignacion".status_asignacion in (2,3)
--	inner join
--			gstdb.dbo.general_area as "area"
--		on
--			"viaje".id_area = "area".id_area
--	where
--			"viaje".id_area = 1
--	group by
--			"viaje".id_area
--			,"area".ciudad
--			,"asignacion".id_seguimiento
--			,cast(substring( convert(nvarchar(MAX), "viaje".f_despachado, 112) , 1, 6 ) as int)
--
--
--
--	select sum(trip_count),area,periodo_despachado from sistemas.dbo.projections_view_full_gst_core_indicators
--	where id_area = 1
--	group by area,periodo_despachado

--
--	1- Cantidad de viaje mensuales  add desp_asignacion.id_seguimento 1=cargado , 2=vacio [Despachdos]
--
--	2- Monto mensual facturado tipo_doc = 1 , total = subtotal + iva - retencion
--
--	3- Cantidad Mensual de fletes por poblacion
--
--	4- Monto por unidad facturado


	select id_seguimiento,status_asignacion from sistemas.dbo.projections_view_full_gst_testcore_indicators

-- 32421 32421
-- 32420

-- ==================================================================================================== --
--	Indicador Cantidad de viaje mensuales
-- ==================================================================================================== --
use sistemas
-- select * from sistemas.dbo.resumen_view_viajes_mensual_gst_indicators
IF OBJECT_ID ('resumen_view_viajes_mensual_gst_indicators', 'V') IS NOT NULL
    DROP VIEW resumen_view_viajes_mensual_gst_indicators;
-- now build the view
create view resumen_view_viajes_mensual_gst_indicators
with encryption
as
	select
			 row_number() over(order by "indicador".id_area) as 'id'
			,"indicador".id_area
			,"indicador".area as 'area'
			,sum("indicador".trip_count) as 'viajes'
			,"indicador".id_seguimiento
			,case
				"indicador".id_seguimiento
					when 1 then 'ViajeCargado'
					when 2 then 'ViajeVacio'
			 end as 'TipoViaje'
			,"indicador".periodo_despachado as 'periodo'
	from
			sistemas.dbo.projections_view_full_gst_core_indicators as "indicador"
	group by
			"indicador".id_area
			,"indicador".area
			,"indicador".id_seguimiento
			,"indicador".periodo_despachado


-- ==================================================================================================== --
--	Indicador Cantidad Mensual de fletes por poblacion
-- ==================================================================================================== --
use sistemas
-- select * from sistemas.dbo.resumen_view_viajes_mensualpoblacion_gst_indicators
IF OBJECT_ID ('resumen_view_viajes_mensualpoblacion_gst_indicators', 'V') IS NOT NULL
    DROP VIEW resumen_view_viajes_mensualpoblacion_gst_indicators;
-- now build the view
create view resumen_view_viajes_mensualpoblacion_gst_indicators
with encryption
as
	select
			 row_number() over(order by "indicador".id_area) as 'id'
			,"indicador".id_area
			,"indicador".area as 'area'
			,sum("indicador".trip_count) as 'viajes'
			,case
				"indicador".id_seguimiento
					when 1 then 'ViajeCargado'
					when 2 then 'ViajeVacio'
			 end as 'TipoViaje'
			,"indicador".periodo_despachado as 'periodo'
			,"indicador".id_destino
			,"destino".desc_plaza as 'poblacion'
	from
			sistemas.dbo.projections_view_full_gst_core_indicators as "indicador"
	inner join
			gstdb.dbo.trafico_plaza as "destino" on "destino".id_plaza = "indicador".id_destino
	where
			"indicador".id_seguimiento = 1
--		and
--			"indicador".periodo_despachado = 201902 and "indicador".id_area = 1
	group by
			"indicador".id_area
			,"indicador".area
			,"indicador".id_seguimiento
			,"indicador".periodo_despachado
			,"indicador".id_destino
			,"destino".desc_plaza


-- ==================================================================================================== --
-- Monto mensual facturado
-- ==================================================================================================== --
use sistemas
-- select * from sistemas.dbo.resumen_view_montofacturado_mensual_gst_indicators
IF OBJECT_ID ('resumen_view_montofacturado_mensual_gst_indicators', 'V') IS NOT NULL
    DROP VIEW resumen_view_montofacturado_mensual_gst_indicators;
-- now build the view
create view resumen_view_montofacturado_mensual_gst_indicators
with encryption
as
	select
			 row_number() over(order by "guia".id_area) as 'id'
			,"guia".id_area
			,"area".ciudad as 'area'
			,sum("guia".flete) as 'flete'
			,sum("guia".subtotal) as 'subtotal'
			,sum("guia".iva_guia) as 'iva'
			,sum("guia".monto_retencion) as 'retencion'
			,sum("guia".subtotal + "guia".iva_guia - "guia".monto_retencion) as 'total'
--			,"guia".tipo_doc
			,cast(substring( convert(nvarchar(MAX), "guia".fecha_guia, 112) , 1, 6 ) as int) as 'periodo'
	from
			gstdb.dbo.trafico_guia as "guia"
	inner join
			gstdb.dbo.general_area as "area"
		on
			"area".id_area = "guia".id_area
	where
			"guia".tipo_doc = 1
	group by
			 "guia".id_area
			,"area".ciudad
--			,"guia".tipo_doc
			,cast(substring( convert(nvarchar(MAX), "guia".fecha_guia, 112) , 1, 6 ) as int)



-- ==================================================================================================== --
-- Monto mensual facturado por unidad
-- ==================================================================================================== --
use sistemas
-- select * from sistemas.dbo.resumen_view_montofacturado_unidad_gst_indicators
IF OBJECT_ID ('resumen_view_montofacturado_unidad_gst_indicators', 'V') IS NOT NULL
    DROP VIEW resumen_view_montofacturado_unidad_gst_indicators;
-- now build the view
create view resumen_view_montofacturado_unidad_gst_indicators
with encryption
as
	select
			 row_number() over(order by "guia".id_area) as 'id'
			,"guia".id_area
			,"area".ciudad as 'area'
			,sum("guia".flete) as 'flete'
			,sum("guia".subtotal) as 'subtotal'
			,sum("guia".iva_guia) as 'iva'
			,sum("guia".monto_retencion) as 'retencion'
			,sum("guia".subtotal + "guia".iva_guia - "guia".monto_retencion) as 'total'
			,"guia".id_unidad as 'unidad'
--			,"guia".tipo_doc
			,cast(substring( convert(nvarchar(MAX), "guia".fecha_guia, 112) , 1, 6 ) as int) as 'periodo'
	from
			gstdb.dbo.trafico_guia as "guia"
	inner join
			gstdb.dbo.general_area as "area"
		on
			"area".id_area = "guia".id_area
	where
			"guia".tipo_doc = 1
	group by
			 "guia".id_area
			,"area".ciudad
			,"guia".id_unidad
--			,"guia".tipo_doc
			,cast(substring( convert(nvarchar(MAX), "guia".fecha_guia, 112) , 1, 6 ) as int)











