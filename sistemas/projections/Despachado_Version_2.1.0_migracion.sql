-- [PREVIEW]
-- select * bonampak_migracion

-- select * from gstdb.dbo.general_centro_costo

-- select * from gstdb.dbo.compania

-- select id_area,id_compania,ciudad,nombre,nombrecorto,* from gstdb.dbo.general_area

-- flotas -> Granel y terceros 

-- tipo operacion[trafico_guia] -> granel y terceros 

-- fraccion -> Cemento , Grado Alimenticio y Varios 

-- select * from gstdb.dbo.trafico_guia


-- =============================================================================================== --
-- REBUILD QUERYS 
-- =============================================================================================== --

-- REPLACE this must change in file[search in trafico_producto]
select * from sistemas.dbo.ingresos_costos_view_fractions
-- src
select * from sistemas.dbo.projections_view_company_fractions

-- [1] REBUILD -- === CHANGE ===   ingresos_costos_view_fractions
use sistemas
IF OBJECT_ID ('ingresos_costos_view_fractions', 'V') IS NOT NULL
    DROP VIEW ingresos_costos_view_fractions;
-- now build the view
create view ingresos_costos_view_fractions
with encryption
as
with "ops" as (
select 
		 case 
			when "top".id_tipo_operacion <> 1 
				then 'OTROS'
			else 
				ltrim(rtrim(replace("top".tipo_operacion ,'FLETES' , '')))
		 end as 'desc_producto'
		 ,case 
			when "top".id_tipo_operacion <> 1 
				then 'OTROS'
			else 
				ltrim(rtrim(replace("top".tipo_operacion ,'FLETES' , '')))
		 end as 'request'
from 
		gstdb.dbo.desp_tipooperacion as "top"
)
select 
		desc_producto,request 
from 
		"ops"
group by
		desc_producto,request


		
		
-- [search in trafico_viaje]
-- Aceptado 
select * from sistemas.dbo.operations_view_full_company_indicators


select top 600 * from sistemas.dbo.ingresos_costos_view_ind_ppto_ingresos

select top 600 * from sistemas.dbo.operations_view_full_company_dispatched_indicators



-- links to server local no change needed 
select * from sistemas.dbo.ingresos_costos_gst_holidays
-- no modifications == NO Change needed
select * from sistemas.dbo.operations_year_selectors


-- 
select * from gstdb.dbo.trafico_producto

select * from gstdb.dbo.desp_tipooperacion

select tipo_vacio,cargadovacio_rem1,cargadovacio_rem2,kms_camion_vacio,kms_camion_lleno,* from gstdb.dbo.trafico_viaje
		
-- 6347

		
select * from gstdb.dbo.gst_sub_tipo_operacion		
		
select * from gstdb.dbo.trafico_ruta

select * from gstdb.dbo.rel_zam_solomon

-- =============================================================================================== --
-- REBUILD QUERYS 
-- =============================================================================================== --
-- FRACTIONS

select 
		id,projections_corporations_id,id_fraccion,desc_producto
from sistemas.dbo.projections_view_fractions

-- =============================================================================================== --
-- REPLACE sistemas.dbo.projections_view_fractions
-- =============================================================================================== --
use sistemas;
select * from sistemas.dbo.projections_view_fractions
IF OBJECT_ID ('projections_view_fractions', 'V') IS NOT NULL
    DROP VIEW projections_view_fractions;
create view projections_view_fractions
with encryption
as
select 
		 id_tipo_operacion as 'id'
		,1 as 'projections_corporations_id'
		,id_tipo_operacion as 'id_fraccion'
		,ltrim(rtrim(replace(tipo_operacion ,'FLETES' , ''))) as 'desc_producto'
from gstdb.dbo.desp_tipooperacion

-- =============================================================================================== --
 -- AREAS
select * from sistemas.dbo.projections_view_bussiness_units order by id_area

select * from sistemas.dbo.projections_view_gst_areas


use sistemas;

-- select * from sistemas.dbo.projections_view_bussiness_units

IF OBJECT_ID ('projections_view_bussiness_units', 'V') IS NOT NULL
    DROP VIEW projections_view_bussiness_units;
-- now build the view
create view projections_view_bussiness_units
with encryption
as


		
	select 
		 "areas".id_area as 'id'
		,1 as projections_corporations_id
		,"areas".id_area
		,"areas".ciudad as 'name'
		,"areas".ciudad as 'label'
		,"areas".nombrecorto as 'tname'
	from 
		sistemas.dbo.projections_view_gst_areas as "areas"
		
		
		

--with "bunits" as
--(
--    select
--		row_number()
--	over 
--		(order by name) as 
--							 id
--							,projections_corporations_id
--							,id_area
--							,name
--							,isnull(label,name) as 'label'
--	from(
--			select
--					 1 as 'projections_corporations_id'
--					,tbk.id_area as 'id_area'
--					,ltrim(rtrim(replace(replace(replace(replace(replace(tbk.nombre ,'AUTOTRANSPORTE' , ''),' S.A. DE C.V.',''),'BONAMPAK',''),'TRANSPORTADORA ESPECIALIZADA INDUSTRIAL','CUAUTITLAN'),'TRANSPORTE DE CARGA GEMINIS','TULTITLAN'))) as 'name'
--					,label.label
--			from 
--					gstdb.dbo.general_area as tbk
--					left join 
--								(
--									select 
--											module_data_definition,module_field_translation as 'label' 
--									from 
--											sistemas.dbo.projections_configs where projections_type_configs_id = 4
--								) as label 
--					on label.module_data_definition collate SQL_Latin1_General_CP1_CI_AS = (ltrim(rtrim(replace(replace(replace(replace(replace(tbk.nombre ,'AUTOTRANSPORTE' , ''),' S.A. DE C.V.',''),'BONAMPAK',''),'TRANSPORTADORA ESPECIALIZADA INDUSTRIAL','CUAUTITLAN'),'TRANSPORTE DE CARGA GEMINIS','TULTITLAN'))))
--			where 
--					tbk.id_area <> 0
--		) as result
--)
--	select 
--			 "units".id
--			,"units".projections_corporations_id
--			,"units".id_area 
--			,"units".name
--			,"units".label
--			,"cia".IDSL as 'tname'
--	from 
--			"bunits" as "units"
--	inner join 
--			(
--				select IDSL,AreaLIS from integraapp.dbo.xrefcia where BDLIS = 'gstdb'
--			)
--		as "cia" on substring("units".label,1,7) = substring("cia".AreaLis,1,7)


--select * from sistemas.dbo.projections_view_gst_areas

		
		
-- ===================================================================================================================== --		
--  Start with areas Definitions base in Serie Guia
-- ===================================================================================================================== --

use sistemas;
select * from sistemas.dbo.projections_view_gst_areas_operaciones


IF OBJECT_ID ('projections_view_gst_areas_operaciones', 'V') IS NOT NULL
    DROP VIEW projections_view_gst_areas_operaciones;
-- now build the view
create view projections_view_gst_areas_operaciones
with encryption
as
-- ===================================================================================================================== --		
--  This table show Tipe of series and consecuently area only groupyn without Series [TipoOperacion]
-- ===================================================================================================================== --
select 
		 "serguia".id_area
		,"serguia".id_serieguia
		,"serguia".id_plaza
		,"serguia".tipo_doc
		,"serguia".num_serie
		,case
			when "serguia".area_solomon is null or "serguia".area_solomon = ''
				then 
					"area".ciudad
			else 
					"serguia".area_solomon
		 end as 'area_solomon'
		,"plz".desc_plaza
		,"area".nombre
		,"area".nombrecorto
		,"area".ciudad
		,"area".direccion
		,"serguia".status
		,"serguia".visible
from 
		gstdb.dbo.trafico_serie_guia as "serguia"
inner join
		gstdb.dbo.trafico_plaza as "plz"
	on 
		"serguia".id_plaza = "plz".id_plaza
inner join 
		gstdb.dbo.general_area as "area"
	on 
		"serguia".id_area = "area".id_area
where 
		"serguia".tipo_doc = 2

-- ===================================================================================================================== --		
--  stock
-- ===================================================================================================================== --

--select id_serieguia from gstdb.dbo.trafico_guia

--select 
--		IDSL,AreaLIS,IDBase,* 
--from 
--		integraapp.dbo.xrefcia as "sl" where BDLis = 'gstdb' 
--inner join 
--	 	gstdb.dbo.trafico_serie_guia as "ls"
--	 on
--		"sl".IDBase collate SQL_Latin1_General_CP1_CI_AS = "ls".num_serie  	
--where 
--		"sl".BDLIS = 'gstdb'
		
-- ===================================================================================================================== --		
--  Add
-- ===================================================================================================================== --
--Tabla de Zonas
--
--Select id_zona, *from gstdb.dbo.trafico_zona
--
----Tabla de Plaza
--Select id_zona, *from gstdb.dbo.trafico_plaza
--
----Vista para unir 
--Use gstdb;
----CREATE VIEW gst_sub_tipo_operacion
----AS
--Select
--TZ.id_zona,TZ.nombre as 'Zona',TC.id_plaza,TP.desc_plaza as 'Plaza',TC.id_cliente,TC.nombre as 'Cliente'
--FROM gstdb.dbo.trafico_zona TZ
--INNER JOIN gstdb.dbo.trafico_plaza TP on
--TZ.id_zona = TP.id_zona
--INNER JOIN gstdb.dbo.trafico_cliente TC on
--TP.id_plaza = TC.id_plaza
--where
--TZ.id_zona <> 0
--order by TZ.id_zona
--
----Select de la vista
--Select *from gstdb.dbo.gst_sub_tipo_operacion
--
--
----Vista INNER al Remitente de las Cartas Porte
--SELECT 
--STO.zona,TG.id_cliente,TG.num_guia 
--FROM 
--gstdb.dbo.trafico_guia TG
--left JOIN 
--gstdb.dbo.gst_sub_tipo_operacion STO on TG.id_remitente = STO.id_cliente
--where tipo_doc = 2

-- ===================================================================================================================== --		
--  Add
-- ===================================================================================================================== --
use sistemas
-- select * from sistemas.dbo.projections_view_gst_areas
IF OBJECT_ID ('projections_view_gst_areas', 'V') IS NOT NULL
    DROP VIEW projections_view_gst_areas;
-- now build the view
create view projections_view_gst_areas
with encryption
as
-- ===================================================================================================================== --		
--  This table show Tipe of series and consecuently area only groupyn without Series [TipoOperacion]
-- ===================================================================================================================== --
select 
		 "areas".id_area
		,"areas".nombre
		,"areas".nombrecorto
		,"areas".ciudad
from 
		sistemas.dbo.projections_view_gst_areas_operaciones as "areas"
group by 
		 "areas".id_area
		,"areas".nombre
		,"areas".nombrecorto
		,"areas".ciudad
		
		
select * from sistemas.dbo.projections_view_gst_areas_operaciones

select 
		no_guia,no_viaje,num_guia,id_area,* 
from 
		sistemas.dbo.projections_view_indicators_dispatch_gst_fulls -- where no_viaje = 45978
where	
		[fecha-confirmacion] is null and prestamo = 'N' and status_guia <> 'B' -- cartas porte directas	
	


-- ===================================================================================================================== --		
--  This table are the full core for all gst and replace old  
-- ===================================================================================================================== --
use sistemas;
-- REPLACE select * from sistemas.dbo.projections_view_full_company_core_indicators
--select * from sistemas.dbo.projections_view_full_gst_core_indicators
select * from sistemas.dbo.projections_view_full_gst_core_indicators where no_viaje = 232810 id_area = 8 and id_tipo_operacion = 9

IF OBJECT_ID ('projections_view_full_gst_core_indicators', 'V') IS NOT NULL
    DROP VIEW projections_view_full_gst_core_indicators;
-- now build the view 
alter view projections_view_full_gst_core_indicators
--	with encryption
as -- fraccion
	with guia_tbk as 
		(
			select
					1 as 'company'
					,coalesce("viaje".id_area,"guia".id_area) as 'id_area'
					,"viaje".no_viaje
					,"viaje".id_area_liq
					,"viaje".id_personal
					,"viaje".kms_real
					,"liquidacion".no_liquidacion
					,"liquidacion".fecha_liquidacion
					,"viaje".id_ruta
					,"viaje".no_ejes_viaje
					,"viaje".id_origen
					,"viaje".id_destino
					,"viaje".fecha_real_viaje as 'fecha-real-viaje'
					,"viaje".fecha_real_fin_viaje as 'fecha-real-fin-viaje'
					,"viaje".f_despachado as 'fecha-despachado'
					,cast("viaje".fecha_real_viaje as date) as 'fecha_real_viaje'
					,cast("viaje".fecha_real_fin_viaje as date) as 'fecha_real_fin_viaje'
					,cast("viaje".f_despachado as date) as 'f_despachado'
					,"viaje".lts_empresa
					,"viaje".lts_viaje
					,"viaje".ton_viaje
					,"viaje".id_unidad
-- TODO Add unit  
					,"manto".id_tipo_unidad
					,"tpomanto".descripcion as 'remolque1'
					,"tpomanto".id_tipo_unidad as 'id_tipo_unidad1'
					-- Added for rentabilidad Module 
					,"tpomantob".descripcion as 'remolque2'
					,"tpomantob".id_tipo_unidad as 'id_tipo_unidad2'
					
					,"viaje".id_remolque1
					,"viaje".id_remolque2
					
					,"viaje".id_dolly
					,"viaje".id_ingreso
					,"viaje".status_viaje
					,"viaje".viajeactual
					,"viaje".no_tarjeta_llave
					,"viaje".id_configuracionviaje
					,"viaje".kms_viaje
					,"viaje".id_areaviaje
					,"viaje".rendimiento_real
					,"viaje".rendimiento_reseteo
					,"viaje".status_desp as 'v_status_desp'
					,"viaje".status_viaje as 'v_status_viaje'
					,"asignacion".id_seguimiento
					,"asignacion".status_asignacion
					,"asignacion".id_asignacion
					,"pedido".num_pedido
				    ,case 
	 					when "pedido".num_pedido is null then 1 else 0
	 	 			 end as 'IsEmptyTrip'
	 	 			,"pedido".f_arribo_prog as [CitaProgramada]
	 	 			,"pedido".f_carini_prog
	 	 			,"pedido".f_desfin_prog
	 	 			,datediff(hour,"pedido".f_carini_prog,"pedido".f_desfin_prog) as [TiempoProgramado] --Tiempo Programado de Carga
--	 	 			,datediff(hour,"pedido".f_carini_prog,"pedido".f_desfin_prog) as [TiempoProgramado] --Tiempo Programado de Descarga
					,cast(substring( convert(nvarchar(MAX), "viaje".fecha_real_viaje, 112) , 1, 6 ) as int) as 'fecha_real_viaje_m'
					,cast(substring( convert(nvarchar(MAX), "viaje".fecha_real_fin_viaje, 112) , 1, 6 ) as int) as 'fecha_real_fin_viaje_m'
					,cast(substring( convert(nvarchar(MAX), "viaje".f_despachado, 112) , 1, 6 ) as int) as 'f_despachado_m'
					,day("viaje".f_despachado) as 'day'
					,upper(left("translation-desp".month_name,1)) + right("translation-desp".month_name,len("translation-desp".month_name) - 1) as 'mes-despacho'
-- guia
					,cast(substring( convert(nvarchar(MAX), "guia".fecha_guia, 112) , 1, 6 ) as int) as 'periodo_aceptado'
					,cast(substring( convert(nvarchar(MAX), "viaje".f_despachado, 112) , 1, 6 ) as int) as 'periodo_despachado'
					,"guia".no_guia
					,"guia".tipo_pago
					,"guia".status_guia
					,"guia".clasificacion_doc
					,"guia".fecha_guia as 'fecha-guia'
					,"guia".fecha_confirmacion as 'fecha-confirmacion' --- hir
					,"guia".fecha_ingreso as 'fecha-ingreso'
					,"guia".fecha_modifico
					,"guia".fecha_cancelacion as 'fecha-cancelacion'
					,"guia".fecha_contabilizado as 'fecha-contabilizado' -- hir
					,cast(substring( convert(nvarchar(MAX), "guia".fecha_contabilizado, 112) , 1, 6 ) as int) as 'fecha_contabilizado_m' --hir
					,cast(substring( convert(nvarchar(MAX), "guia".fecha_confirmacion, 112) , 1, 6 ) as int) as 'fecha_confirmacion_m' --hir
					,cast(substring( convert(nvarchar(MAX), "guia".fecha_guia, 112) , 1, 6 ) as int) as 'fecha_guia_m'
					,cast(substring( convert(nvarchar(MAX), "guia".fecha_cancelacion, 112) , 1, 6 ) as int) as 'fecha_cancelacion_m'
					,cast("guia".fecha_contabilizado as date) as 'fecha_contabilizado'  -- hir
					,cast("guia".fecha_confirmacion as date) as 'fecha_confirmacion'  --hir
					,cast("guia".fecha_guia as date) as 'fecha_guia'
					,cast("guia".fecha_cancelacion as date) as 'fecha_cancelacion'
					,day("guia".fecha_guia) as 'gday'
					,"guia".convenido_tonelada
					,"guia".id_area_facturacion
					,"guia".id_cliente     -- CLIENTE
					,"guia".id_personal as 'gid_personal'
--					,"guia".no_viaje
					,"origen".desc_plaza as 'origen'
					,"destino".desc_plaza as 'destino'
					,"guia".id_remitente  -- REMITENTE
					,"zona".Plaza
					,"zona".Zona
					,"guia".id_serieguia -- SERIES
					-- serie
--					,"serie".id_serieguia
					,"serie".id_plaza
--					,"serie".tipo_doc
					,"serie".num_serie
					,"serie".area_solomon
					,"serie".desc_plaza
					,"serie".nombre
--					,"serie".nombrecorto
					,(
						select
								ltrim(rtrim("areasq".nombrecorto))
						from 
								gstdb.dbo.general_area as "areasq"
						where 
								"areasq".id_area = "viaje".id_area
					) as 'nombrecorto'
					,"serie".ciudad
					-- serie
					,"guia".id_destinatario
					,"guia".kms_guia
					,"guia".flete
					,"guia".seguro
					,"guia".maniobras
					,"guia".autopistas
					,"guia".otros
					,"guia".subtotal
					,"guia".iva_guia
					,"guia".cobro_viaje_kms
					,"guia".tipo_doc
					,"guia".id_origen as 'gid_origen'
					,"guia".id_destino as 'gid_destino'
					,"gorigen".desc_plaza as 'gorigen'
					,"gdestino".desc_plaza as 'gdestino'
					,"guia".id_fraccion
					-- this become obsolete
--					,case 
--						when "guia".id_fraccion in (1,2,6)
--							then 'GRANEL'
--						else 
--							'OTROS'
--					end as 'projections_rp_definition'
--					,"guia".fraccion as 'projections_rp_definition'
					,case 
						when "guia".id_tipo_operacion not in (1,2,6)  
							then 'OTROS'
						else 
							'GRANEL' --ltrim(rtrim(replace("guia".tipo_operacion ,'FLETES' , '')))
					 end as 'fraccion'
					,"guia".plaza_emision
					,"guia".num_guia
					,"guia".no_carta
					,"guia".id_remolque1 as 'gid_remolque1'
					,"guia".id_unidad as 'gid_unidad'
					,"guia".no_remision
					,"guia".motivo_cancelacion
					,"guia".id_remolque2 as 'gid_remolque2'
					,"guia".prestamo
					,"guia".num_guia_asignado
					,"guia".no_deposito
					,"guia".id_ingreso as 'gid_ingreso'
					,"guia".personalnombre
					,"guia".tipo_facturacion
					,"guia".no_transferencia_cobranza
					,"guia".factor_iva
					,"guia".num_guiacancel
					,"guia".tipo_origen
					,"guia".no_poliza
					,"guia".monto_retencion
					,"guia".status_pago
					,"guia".monto_retenciontercero
					,"guia".sustituye_documento
					,"guia".monto_ivaflete
					,"guia".tipocambioconvenio
					,"guia".desc_flete
					,"guia".flete_bruto
					,"guia".id_iva
					,"guia".id_retencion
					,"guia".id_convenio
					,"guia".id_areaconvenio
--					,"guia".id_tipo_operacion
					,coalesce("guia".id_tipo_operacion,"manto".id_tipo_operacion) as 'id_tipo_operacion'
					,"guia".no_kit
					,"guia".monto_ivadescto
					,"guia".monto_retdescto
					,"guia".monto_descto
					,"guia".periodo_facturacion
					,"guia".tipo_producto
					,"guia".num_guia_incentivo
					,"guia".facturado
					,"guia".id_modifico
					,"guia".kms_convenio
					,'' as 'observacion'
-- client , route , manto , liquidacion	
--					,(select datename(mm,guia.fecha_guia)) as 'mes'
					,upper(left("translation".month_name,1)) + right("translation".month_name,len("translation".month_name) - 1) as 'mes'
					,"ruta".desc_ruta      
					,"cliente".nombre as 'cliente'
					,"remitente".nombre as 'remitente'
					,"destinatario".nombre as 'destinatario'
					,"personal".nombre as 'operador'
					,"manto".modelo      
					,"manto".id_flota     
					,(
						select 
								sum(isnull("tren".peso,0)) 
						from 
								gstdb.dbo.trafico_renglon_guia as "tren"
						where	
								"tren".no_guia = "guia".no_guia and "tren".id_area = "viaje".id_area
					 ) as 'peso-aceptado'
					,(
						select 
--								sum(isnull("trend".peso,0) + isnull("trend".peso_estimado,0)) 
								sum(isnull("trend".peso,0))
						from 
								gstdb.dbo.trafico_renglon_guia as "trend"
						where	
								"trend".no_guia = "guia".no_guia and "trend".id_area = "viaje".id_area
					 ) as 'peso-despachado'
					,(
						select 
								descripcion
						from
								gstdb.dbo.trafico_configuracionviaje as "trviaje"
						where
								"trviaje".id_configuracionviaje = viaje.id_configuracionviaje
					) as 'configuracion_viaje'
					,(
						select 
								tipo_operacion
						from
								gstdb.dbo.desp_tipooperacion as "tpop"
						where 
								tpop.id_tipo_operacion = coalesce("guia".id_tipo_operacion,"manto".id_tipo_operacion)
					 ) as 'tipo_de_operacion'
					,(
						select 
								nombre
						from 
								gstdb.dbo.desp_flotas as "fleet"
						where
								fleet.id_flota = manto.id_flota
							
					) as 'flota'
					,(
						select
								ltrim(rtrim(replace(replace(replace(replace(replace(areas.nombre ,'AUTOTRANSPORTE' , ''),' S.A. DE C.V.',''),'BONAMPAK',''),'TRANSPORTADORA ESPECIALIZADA INDUSTRIAL','CUAUTITLAN'),'TRANSPORTE DE CARGA GEMINIS','TULTITLAN')))
						from 
								gstdb.dbo.general_area as "areas"
						where 
								areas.id_area = viaje.id_area
					) as 'area'
					,(
						select
								desc_producto
						from
							gstdb.dbo.trafico_producto as "producto"
						where      
							producto.id_producto = 0 and producto.id_fraccion = guia.id_fraccion
					) as 'fraction'
			from 
						gstdb.dbo.trafico_viaje as "viaje"
						
				left join 
						gstdb.dbo.mtto_unidades as "manto"
					on 
						"manto".id_unidad = "viaje".id_unidad
-- ===================================================================================================================================== --
--	Remolque description 
-- ===================================================================================================================================== --						
				left join 
						gstdb.dbo.mtto_unidades as "manto1"
					on 
						"manto1".id_unidad = "viaje".id_remolque1
				left join 
						gstdb.dbo.mtto_tipos_unidades as "tpomanto"
					on 
						"manto1".id_tipo_unidad = "tpomanto".id_tipo_unidad

				left join 
						gstdb.dbo.mtto_unidades as "manto2"
					on 
						"manto2".id_unidad = "viaje".id_remolque2
				left join 
						gstdb.dbo.mtto_tipos_unidades as "tpomantob"
					on 
						"manto2".id_tipo_unidad = "tpomantob".id_tipo_unidad
-- ===================================================================================================================================== --
				left join 
						gstdb.dbo.trafico_ruta as "ruta"
					on
						viaje.id_ruta = "ruta".id_ruta
				inner join
						sistemas.dbo.generals_month_translations as "translation-desp"
					on
						month("viaje".f_despachado) = "translation-desp".month_num
-- One to one
				left join 
						gstdb.dbo.trafico_guia as "guia"
					on	
						guia.id_area = viaje.id_area and guia.no_viaje = viaje.no_viaje
--						guia.status_guia in (select item from sistemas.dbo.fnSplit('R|T|C|A', '|'))
--					and 
--						guia.prestamo <> 'P'
					and 
						"guia".tipo_doc = 2 
--					and	
--				inner join
--						gstdb.dbo.trafico_renglon_guia as "trg"
--					on
--						"trg".no_guia = "guia".no_guia and "trg".id_area = "viaje".id_area
--
--				left join 
--						sistemas.dbo.projections_fraccion_groups as "fgrp"
--					on 
--						"fgrp".projections_corporations_id = 1 and "guia".id_fraccion = "fgrp".projections_id_fraccion
				left join 
						gstdb.dbo.desp_pedido as "pedido"
					on
						"guia".no_guia = "pedido".no_guia and "guia".id_area = "pedido".id_area
				left join
						gstdb.dbo.personal_personal as "personal"
					on 
						"personal".id_personal = "viaje".id_personal -- porque guia y no viaje ?
--- ===============================  from Viaje
				left join 
						gstdb.dbo.trafico_plaza as "origen"
					on 
						"origen".id_plaza = "viaje".id_origen
				left join
						gstdb.dbo.trafico_plaza as "destino"
					on 
						"destino".id_plaza = "viaje".id_destino
--- =============================== from guia
				left join 
						gstdb.dbo.trafico_plaza as "gorigen"
					on 
						"gorigen".id_plaza = "guia".id_origen
				left join
						gstdb.dbo.trafico_plaza as "gdestino"
					on 
						"gdestino".id_plaza = "guia".id_destino
--- ===============================
				left join 
						gstdb.dbo.trafico_liquidacion as "liquidacion"
					on
						"liquidacion".no_liquidacion = "viaje".no_liquidacion and "liquidacion".id_area = "viaje".id_area
				left join 
							gstdb.dbo.desp_asignacion as "asignacion"
						on 
						"viaje".no_viaje = "asignacion".no_viaje and "viaje".id_area = "asignacion".id_area --and "asignacion".id_seguimiento = 1 and "asignacion".status_asignacion in (2,3)
--- ===============================
				left join
						gstdb.dbo.trafico_cliente as "cliente"
					on 
						"cliente".id_cliente = "guia".id_cliente
--- ===============================
				left join
						gstdb.dbo.trafico_cliente as "remitente"
					on 
						"remitente".id_cliente = "guia".id_remitente
--- ===============================
				left join
						gstdb.dbo.trafico_cliente as "destinatario"
					on 
						"destinatario".id_cliente = "guia".id_destinatario
--- ===============================
				left join
						sistemas.dbo.generals_month_translations as "translation"
					on
						month("guia".fecha_guia) = "translation".month_num
				left join -- Series reemplaza Tipo de Operacion [Tipo Ingreso]
						sistemas.dbo.projections_view_gst_areas_operaciones as "serie"
					on 
						"guia".id_serieguia = "serie".id_serieguia
				left join  -- Zonas reemplaza Flotas [Agrupador de Origen] 
						gstdb.dbo.gst_sub_tipo_operacion as "zona"
					on 
						"guia".id_remitente = "zona".id_cliente
		)
	select 
--				 "guia".id_area ,"guia".id_unidad ,"guia".id_configuracionviaje ,"guia".id_tipo_operacion ,"guia".id_fraccion ,"guia".id_flota 
--				,"guia".no_viaje,"guia".num_guia , "guia".id_ruta ,"guia".id_origen ,"guia".desc_ruta, "guia".monto_retencion
--				,"guia".fecha_guia
--				,"guia".mes ,"guia".f_despachado ,"guia".cliente 
--				
				 "guia".company,"guia".id_area,"guia".no_viaje,"guia".id_area_liq,"guia".id_personal
--				,"guia".kms_real
				,"guia".no_liquidacion,"guia".fecha_liquidacion,"guia".id_ruta
				,"guia".no_ejes_viaje,"guia".id_origen,"guia".id_destino,"guia"."fecha-real-viaje","guia"."fecha-real-fin-viaje","guia"."fecha-despachado"
				,"guia".fecha_real_viaje,"guia".fecha_real_fin_viaje,"guia".f_despachado,"guia".lts_empresa,"guia".lts_viaje,"guia".ton_viaje,"guia".id_unidad
				,"guia".id_tipo_unidad,"guia".remolque1,"guia".id_tipo_unidad1,"guia".remolque2,"guia".id_tipo_unidad2
				,"guia".id_remolque1,"guia".id_remolque2,"guia".id_dolly,"guia".id_ingreso,"guia".status_viaje,"guia".viajeactual,"guia".no_tarjeta_llave
				,"guia".id_configuracionviaje
--				,"guia".kms_viaje
				,"guia".id_areaviaje
				,"guia".rendimiento_real
				,"guia".rendimiento_reseteo
				,"guia".v_status_desp
				,"guia".v_status_viaje
				,"guia".id_seguimiento,"guia".status_asignacion,"guia".id_asignacion
				,"guia".num_pedido
				,"guia".IsEmptyTrip
	 			,"guia".CitaProgramada
	 			,"guia".f_carini_prog
	 			,"guia".f_desfin_prog
	 			,"guia".TiempoProgramado
				,"guia".fecha_real_viaje_m,"guia".fecha_real_fin_viaje_m
				,"guia".f_despachado_m,"guia"."day","guia"."mes-despacho"
				,"guia".periodo_aceptado,"guia".periodo_despachado
				,"guia".no_guia,"guia".tipo_pago,"guia".status_guia,"guia".clasificacion_doc
				,"guia"."fecha-guia","guia"."fecha-confirmacion","guia"."fecha-ingreso","guia".fecha_modifico,"guia"."fecha-cancelacion"
				,"guia"."fecha-contabilizado"
				,"guia".fecha_contabilizado_m,"guia".fecha_confirmacion_m
				,"guia".fecha_guia_m,"guia".fecha_cancelacion_m
				,"guia".fecha_contabilizado,"guia".fecha_confirmacion
				,"guia".fecha_guia,"guia".fecha_cancelacion,"guia".gday
				,"guia".convenido_tonelada,"guia".id_area_facturacion,"guia".id_cliente,"guia".gid_personal,"guia".origen,"guia".destino,"guia".id_remitente
				,"guia".Plaza,"guia".Zona,"guia".id_serieguia
--				,serie
				,"guia".id_plaza
--				,"guia".tipo_doc
				,"guia".num_serie
				,"guia".area_solomon
				,"guia".desc_plaza
				,"guia".nombre
				,"guia".nombrecorto
				,"guia".ciudad
--				,serie
				,"guia".id_destinatario
				,"guia".kms_guia,"guia".flete,"guia".seguro,"guia".maniobras,"guia".autopistas,"guia".otros
--				,"guia".subtotal
				,"guia".iva_guia
				,"guia".cobro_viaje_kms,"guia".tipo_doc,"guia".gid_origen,"guia".gid_destino
				,"guia".gorigen,"guia".gdestino
				,"guia".id_fraccion,"guia".fraccion 
				,"guia".fraccion as 'projections_rp_definition'
				,"guia".plaza_emision,"guia".num_guia
				,"guia".no_carta,"guia".gid_remolque1,"guia".gid_unidad,"guia".no_remision,"guia".motivo_cancelacion,"guia".gid_remolque2,"guia".prestamo
				,"guia".num_guia_asignado,"guia".no_deposito,"guia".gid_ingreso,"guia".personalnombre,"guia".tipo_facturacion,"guia".no_transferencia_cobranza
				,"guia".factor_iva,"guia".num_guiacancel,"guia".tipo_origen,"guia".no_poliza,"guia".monto_retencion,"guia".status_pago
				,"guia".monto_retenciontercero,"guia".sustituye_documento,"guia".monto_ivaflete,"guia".tipocambioconvenio,"guia".desc_flete
				,"guia".flete_bruto,"guia".id_iva,"guia".id_retencion,"guia".id_convenio,"guia".id_areaconvenio,"guia".id_tipo_operacion,"guia".no_kit
				,"guia".monto_ivadescto,"guia".monto_retdescto,"guia".monto_descto,"guia".periodo_facturacion,"guia".tipo_producto
				,"guia".num_guia_incentivo,"guia".facturado,"guia".id_modifico,"guia".kms_convenio,"guia".observacion,"guia".mes,"guia".desc_ruta
				,"guia".cliente
				,"guia".remitente
				,"guia".destinatario
				,"guia".operador,"guia".modelo,"guia".id_flota
--				,"guia"."peso-aceptado","guia"."peso-despachado"
				,"guia".configuracion_viaje,"guia".tipo_de_operacion
				,"guia".flota
--				,case 
--					when "guia".id_tipo_operacion = 12 and company = 1 and id_area = 2
--						then 'LA PAZ'
--					when "guia".id_area = 4 and company = 1 
--						then 'MEXICALI'
--					else
				,"guia".area
--				end as 'area'
				,"guia".fraction
				,case
					when 
						("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m <> "guia".fecha_cancelacion_m)
					then
						case 
							when
--								"guia".fecha_confirmacion_m is null
								"guia".fecha_contabilizado_m is null
							then
								2 -- cancelada efecto zero
							else 
								1 -- keep this cancelation
						end
					when 
						("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m = "guia".fecha_cancelacion_m)
					then
						2 -- cancelada efecto zero
					else	
						0 -- todo lo demas
				 end as 'FlagIsDisminution'
				,case 
					when 
						"guia".prestamo = 'P'
					then 
						1
				 	else
				 		0
				 end as 'FlagIsProvision'
--				,case 
--					when 
--						( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company order by "guia".fecha_guia) ) > 1 
--					then 
--						0 
--					else 
--						"guia".kms_viaje
--				end as 'kms_viaje'
				,case
					when 
						("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m <> "guia".fecha_cancelacion_m)
					then
						case 
							when
--								"guia".fecha_confirmacion_m is null
								"guia".fecha_contabilizado_m is null
							then
								0 -- cancelada efecto zero
							else 
								--1 -- Disminucion al siguiente mes
								"guia".kms_viaje
						end
					when 
						("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m = "guia".fecha_cancelacion_m)
					then
						0 -- cancelada efecto zero
					else	
						--0 -- todo lo demas
						case 
							when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company,"guia".fecha_cancelacion_m order by "guia".fecha_guia) ) > 1 
							then 0 else "guia".kms_viaje
						end
				 end as 'kms_viaje'
--				,case 
--					when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company order by "guia".fecha_guia) ) > 1 
--						then 0 else "guia".kms_real
--				end as 'kms_real'
				,case
					when 
						("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m <> "guia".fecha_cancelacion_m)
					then
						case 
							when
--								"guia".fecha_confirmacion_m is null
								"guia".fecha_contabilizado_m is null
							then
								0 -- cancelada efecto zero
							else 
								--1 -- Disminucion al siguiente mes
								"guia".kms_real
						end
					when 
						("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m = "guia".fecha_cancelacion_m)
					then
						0 -- cancelada efecto zero
					else	
						--0 -- todo lo demas
						case 
							when 
								( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company,"guia".fecha_cancelacion_m order by "guia".fecha_guia) ) > 1 
							then 
								0 
							else 
								"guia".kms_real
						end
				 end as 'kms_real'
				,case
					when 
						"guia".id_tipo_operacion in (1,2,6)
					then
					-- add hir
--						case 
--							when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company order by "guia".fecha_guia) ) > 1
--								then 0 
--							else ("guia".kms_viaje)*2
--						end
						case
							when 
								("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m <> "guia".fecha_cancelacion_m)
							then
								case 
									when
--										"guia".fecha_confirmacion_m is null
										"guia".fecha_contabilizado_m is null
									then
										0 -- cancelada efecto zero
									else 
										--1 -- Disminucion al siguiente mes
										("guia".kms_viaje)*2
								end
							when 
								("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m = "guia".fecha_cancelacion_m)
							then
								0 -- cancelada efecto zero
							else	
								--0 -- todo lo demas
								case 
									when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company,"guia".fecha_cancelacion_m order by "guia".fecha_guia) ) > 1 
									then 0 else ("guia".kms_viaje)*2
								end
						 end
					else
					-- and hir
--						case
--							when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company order by "guia".fecha_guia) ) > 1
--								then 0 
--							else "guia".kms_real
--						end
						case
							when 
								("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m <> "guia".fecha_cancelacion_m)
							then
								case 
									when
--										"guia".fecha_confirmacion_m is null
										"guia".fecha_contabilizado_m is null
									then
										0 -- cancelada efecto zero
									else 
--										1 -- Disminucion al siguiente mes [Provision - Aceptadas en otros meses Cancelasdas en este Mes ]
--										case 
--											when -- check if is in prestamo
----												"guia".prestamo = 'N'
										case 
											when 
												"guia".kms_real = 0 or "guia".kms_real is null
											then 
												"guia".kms_viaje
										else
											"guia".kms_real
										end 
								end
							when 
								("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m = "guia".fecha_cancelacion_m)
							then
								0 -- cancelada efecto zero
							else	
								--0 -- todo lo demas
								case 
									when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company,"guia".fecha_cancelacion_m order by "guia".fecha_guia) ) > 1 
										then 0 
									else 
										case 
											when 
												"guia".kms_real = 0 or "guia".kms_real is null
											then 
												"guia".kms_viaje
										else
											"guia".kms_real
										end
								end
						 end
				end as 'kms'
---> add description				
				,sum("guia".subtotal) as 'subtotal' 
				,sum("guia"."peso-aceptado") as 'peso-aceptado' , sum("guia"."peso-despachado") as 'peso-despachado'
--				,"guia".configuracion_viaje ,"guia".tipo_de_operacion ,"guia".flota ,"guia".area ,"guia".fraccion --,"guia".company 
--				,case 
--					when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company order by "guia".fecha_guia) ) > 1 
--						then 0 else 1
--				end as 'trip_count'
				,case
					when 
						("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m <> "guia".fecha_cancelacion_m)
					then
						case 
							when
--								"guia".fecha_confirmacion_m is null
								"guia".fecha_contabilizado_m is null
							then
								0 -- cancelada efecto zero
							else 
								1 -- Disminucion al siguiente mes
						end
					when 
						("guia".fecha_cancelacion_m is not null and "guia".fecha_guia_m = "guia".fecha_cancelacion_m)
					then
						0 -- cancelada efecto zero
					else	
						--0 -- todo lo demas
						case 
							when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company,"guia".fecha_cancelacion_m order by "guia".fecha_guia) ) > 1 
							then 0 else 1
						end
				 end as 'trip_count'
				 ,0 as 'FlagIsNextMonth'
	from 
				guia_tbk as "guia" -- where "guia".fecha_guia_m = '201810'
	group by 
--				 "guia".id_area ,"guia".id_unidad ,"guia".id_configuracionviaje ,"guia".id_tipo_operacion ,"guia".id_fraccion ,"guia".id_flota
--				,"guia".no_viaje,"guia".num_guia , "guia".id_ruta ,"guia".id_origen ,"guia".desc_ruta, "guia".monto_retencion
--				,"guia".fecha_guia 
--				,"guia".mes,"guia".f_despachado ,"guia".cliente
--				,"guia".kms_viaje,"guia".kms_real
--				,"guia".configuracion_viaje ,"guia".tipo_de_operacion ,"guia".flota ,"guia".area ,"guia".fraccion ,"guia".company
				 "guia".id_area,"guia".no_viaje,"guia".id_area_liq,"guia".id_personal
				,"guia".kms_real
				,"guia".no_liquidacion,"guia".fecha_liquidacion,"guia".id_ruta
				,"guia".no_ejes_viaje,"guia".id_origen,"guia".id_destino,"guia"."fecha-real-viaje","guia"."fecha-real-fin-viaje","guia"."fecha-despachado"
				,"guia".fecha_real_viaje,"guia".fecha_real_fin_viaje,"guia".f_despachado,"guia".lts_empresa,"guia".lts_viaje,"guia".ton_viaje,"guia".id_unidad
				,"guia".id_tipo_unidad,"guia".remolque1,"guia".id_tipo_unidad1,"guia".remolque2,"guia".id_tipo_unidad2
				,"guia".id_remolque1,"guia".id_remolque2,"guia".id_dolly,"guia".id_ingreso,"guia".status_viaje,"guia".viajeactual,"guia".no_tarjeta_llave
				,"guia".id_configuracionviaje
				,"guia".kms_viaje
				,"guia".id_areaviaje
				,"guia".rendimiento_real
				,"guia".rendimiento_reseteo
				,"guia".v_status_desp
				,"guia".v_status_viaje
				,"guia".id_seguimiento,"guia".status_asignacion,"guia".id_asignacion
				,"guia".num_pedido
			    ,"guia".IsEmptyTrip
	 			,"guia".CitaProgramada
	 			,"guia".f_carini_prog
	 			,"guia".f_desfin_prog
	 			,"guia".TiempoProgramado
				,"guia".fecha_real_viaje_m,"guia".fecha_real_fin_viaje_m
				,"guia".f_despachado_m,"guia"."day","guia"."mes-despacho"
				,"guia".periodo_aceptado,"guia".periodo_despachado
				,"guia".no_guia,"guia".tipo_pago,"guia".status_guia,"guia".clasificacion_doc
				,"guia"."fecha-guia","guia"."fecha-confirmacion","guia"."fecha-ingreso","guia".fecha_modifico,"guia"."fecha-cancelacion"
				,"guia"."fecha-contabilizado"
				,"guia".fecha_contabilizado_m,"guia".fecha_confirmacion_m
				,"guia".fecha_guia_m,"guia".fecha_cancelacion_m
				,"guia".fecha_contabilizado,"guia".fecha_confirmacion
				,"guia".fecha_guia,"guia".fecha_cancelacion,"guia".gday
				,"guia".convenido_tonelada,"guia".id_area_facturacion,"guia".id_cliente,"guia".gid_personal,"guia".origen,"guia".destino,"guia".id_remitente
				,"guia".Plaza,"guia".Zona,"guia".id_serieguia
--				,serie
				,"guia".id_plaza
--				,"guia".tipo_doc
				,"guia".num_serie
				,"guia".area_solomon
				,"guia".desc_plaza
				,"guia".nombre
				,"guia".nombrecorto
				,"guia".ciudad
--				,serie
				,"guia".id_destinatario
				,"guia".kms_guia,"guia".flete,"guia".seguro,"guia".maniobras,"guia".autopistas,"guia".otros
				,"guia".subtotal
				,"guia".iva_guia
				,"guia".cobro_viaje_kms,"guia".tipo_doc,"guia".gid_origen,"guia".gid_destino
				,"guia".gorigen,"guia".gdestino
				,"guia".id_fraccion,"guia".fraccion
--				,"guia".fraccion as 'projections_rp_definition'
				,"guia".plaza_emision,"guia".num_guia
				,"guia".no_carta,"guia".gid_remolque1,"guia".gid_unidad,"guia".no_remision,"guia".motivo_cancelacion,"guia".gid_remolque2,"guia".prestamo
				,"guia".num_guia_asignado,"guia".no_deposito,"guia".gid_ingreso,"guia".personalnombre,"guia".tipo_facturacion,"guia".no_transferencia_cobranza
				,"guia".factor_iva,"guia".num_guiacancel,"guia".tipo_origen,"guia".no_poliza,"guia".monto_retencion,"guia".status_pago
				,"guia".monto_retenciontercero,"guia".sustituye_documento,"guia".monto_ivaflete,"guia".tipocambioconvenio,"guia".desc_flete
				,"guia".flete_bruto,"guia".id_iva,"guia".id_retencion,"guia".id_convenio,"guia".id_areaconvenio,"guia".id_tipo_operacion,"guia".no_kit
				,"guia".monto_ivadescto,"guia".monto_retdescto,"guia".monto_descto,"guia".periodo_facturacion,"guia".tipo_producto
				,"guia".num_guia_incentivo,"guia".facturado,"guia".id_modifico,"guia".kms_convenio,"guia".observacion,"guia".mes,"guia".desc_ruta
				,"guia".cliente
				,"guia".remitente
				,"guia".destinatario
				,"guia".operador,"guia".modelo,"guia".id_flota,"guia"."peso-aceptado","guia"."peso-despachado","guia".configuracion_viaje,"guia".tipo_de_operacion
				,"guia".flota,"guia".area,"guia".fraction
				,"guia".company

				
				
-- ===================================================================================================================== --		
--  This table are the full DISCORE for all gst and replace old  
-- ===================================================================================================================== --

-- @test select * from sistemas.dbo.projections_view_full_gst_discore_indicators
use sistemas
IF OBJECT_ID ('projections_view_full_gst_discore_indicators', 'V') IS NOT NULL		
    DROP VIEW projections_view_full_gst_discore_indicators;
    
alter view projections_view_full_gst_discore_indicators
as 				
select 
		 "dissmiss".company
		,"dissmiss".id_area
		,"dissmiss".no_viaje
		,"dissmiss".id_area_liq
		,"dissmiss".id_personal
		,"dissmiss".no_liquidacion
		,"dissmiss".fecha_liquidacion
		,"dissmiss".id_ruta
		,"dissmiss".no_ejes_viaje
		,"dissmiss".id_origen
		,"dissmiss".id_destino
		,"dissmiss".[fecha-real-viaje]
		,"dissmiss".[fecha-real-fin-viaje]
		,"dissmiss".[fecha-despachado]
		,"dissmiss".fecha_real_viaje
		,"dissmiss".fecha_real_fin_viaje
		,"dissmiss".f_despachado  -- NOTE this must change
		,"dissmiss".lts_empresa
	        ,"dissmiss".lts_viaje
		,"dissmiss".ton_viaje
		,"dissmiss".id_unidad
		,"dissmiss".id_tipo_unidad
		,"dissmiss".remolque1
		,"dissmiss".id_tipo_unidad1
		,"dissmiss".remolque2
		,"dissmiss".id_tipo_unidad2
		,"dissmiss".id_remolque1
		,"dissmiss".id_remolque2
		,"dissmiss".id_dolly
		,"dissmiss".id_ingreso
		,"dissmiss".status_viaje
		,"dissmiss".viajeactual
		,"dissmiss".no_tarjeta_llave
		,"dissmiss".id_configuracionviaje
		,"dissmiss".id_areaviaje
		,"dissmiss".rendimiento_real
		,"dissmiss".rendimiento_reseteo
		,"dissmiss".v_status_desp
		,"dissmiss".v_status_viaje
		,"dissmiss".id_seguimiento
		,"dissmiss".status_asignacion
		,"dissmiss".id_asignacion
		,"dissmiss".num_pedido
		,"dissmiss".IsEmptyTrip
		,"dissmiss".CitaProgramada
		,"dissmiss".f_carini_prog
		,"dissmiss".f_desfin_prog
		,"dissmiss".TiempoProgramado
		,"dissmiss".fecha_real_viaje_m
		,"dissmiss".fecha_real_fin_viaje_m
		,cast(substring( convert(nvarchar(MAX), "dissmiss".fecha_cancelacion, 112) , 1, 6 ) as int)  as 'f_despachado_m'
		,"dissmiss".[day]
		,"dissmiss".[mes-despacho]
		,"dissmiss".periodo_aceptado
		,"dissmiss".periodo_despachado
		,"dissmiss".no_guia
		,"dissmiss".tipo_pago
		,"dissmiss".status_guia
		,"dissmiss".clasificacion_doc
		,"dissmiss".[fecha-guia]
		,"dissmiss".[fecha-confirmacion]
		,"dissmiss".[fecha-ingreso]
		,"dissmiss".fecha_modifico
		,"dissmiss".[fecha-cancelacion]
		,"dissmiss".[fecha-contabilizado]
		,"dissmiss".fecha_contabilizado_m
		,"dissmiss".fecha_confirmacion_m
		,cast(substring( convert(nvarchar(MAX), "dissmiss".fecha_cancelacion, 112) , 1, 6 ) as int)  as 'fecha_guia_m'
		,"dissmiss".fecha_cancelacion_m
		,"dissmiss".fecha_contabilizado
		,"dissmiss".fecha_confirmacion
		,"dissmiss".fecha_guia          -- NOTE this must change
		,"dissmiss".fecha_cancelacion
		,"dissmiss".gday
		,"dissmiss".convenido_tonelada
		,"dissmiss".id_area_facturacion
		,"dissmiss".id_cliente
		,"dissmiss".gid_personal
		,"dissmiss".origen
		,"dissmiss".destino
		,"dissmiss".id_remitente
		,"dissmiss".Plaza,"dissmiss".Zona,"dissmiss".id_serieguia
--				,serie
		,"dissmiss".id_plaza
--				,"guia".tipo_doc
		,"dissmiss".num_serie
		,"dissmiss".area_solomon
		,"dissmiss".desc_plaza
		,"dissmiss".nombre
		,"dissmiss".nombrecorto
		,"dissmiss".ciudad
		-- add from new
		,"dissmiss".id_destinatario
		,"dissmiss".kms_guia
		,"dissmiss".flete
		,"dissmiss".seguro
		,"dissmiss".maniobras
		,"dissmiss".autopistas
		,"dissmiss".otros
		,"dissmiss".iva_guia
		,"dissmiss".cobro_viaje_kms
		,"dissmiss".tipo_doc
		,"dissmiss".gid_origen
		,"dissmiss".gid_destino
		,"dissmiss".gorigen
		,"dissmiss".gdestino
		,"dissmiss".id_fraccion
		,"dissmiss".fraccion
		,"dissmiss".projections_rp_definition
		,"dissmiss".plaza_emision
		,"dissmiss".num_guia
		,"dissmiss".no_carta
		,"dissmiss".gid_remolque1
		,"dissmiss".gid_unidad
		,"dissmiss".no_remision
		,"dissmiss".motivo_cancelacion
		,"dissmiss".gid_remolque2
		,"dissmiss".prestamo
		,"dissmiss".num_guia_asignado
		,"dissmiss".no_deposito
		,"dissmiss".gid_ingreso
		,"dissmiss".personalnombre
		,"dissmiss".tipo_facturacion
		,"dissmiss".no_transferencia_cobranza
		,"dissmiss".factor_iva
		,"dissmiss".num_guiacancel
		,"dissmiss".tipo_origen
		,"dissmiss".no_poliza
		,"dissmiss".monto_retencion
		,"dissmiss".status_pago
		,"dissmiss".monto_retenciontercero
		,"dissmiss".sustituye_documento
		,"dissmiss".monto_ivaflete
		,"dissmiss".tipocambioconvenio
		,"dissmiss".desc_flete
		,"dissmiss".flete_bruto
		,"dissmiss".id_iva
		,"dissmiss".id_retencion
		,"dissmiss".id_convenio
		,"dissmiss".id_areaconvenio
		,"dissmiss".id_tipo_operacion
		,"dissmiss".no_kit
		,"dissmiss".monto_ivadescto
		,"dissmiss".monto_retdescto
		,"dissmiss".monto_descto
		,"dissmiss".periodo_facturacion
		,"dissmiss".tipo_producto
		,"dissmiss".num_guia_incentivo
		,"dissmiss".facturado
		,"dissmiss".id_modifico
		,"dissmiss".kms_convenio
		,"dissmiss".observacion
		,"dissmiss".mes
		,"dissmiss".desc_ruta
		,"dissmiss".cliente
		,"dissmiss".remitente
		,"dissmiss".destinatario
		,"dissmiss".operador
		,"dissmiss".modelo
		,"dissmiss".id_flota
		,"dissmiss".configuracion_viaje
		,"dissmiss".tipo_de_operacion
		,"dissmiss".flota
		,"dissmiss".area
		,"dissmiss".fraction
		,"dissmiss".FlagIsDisminution
		,"dissmiss".FlagIsProvision
		,"dissmiss".kms_viaje
		,"dissmiss".kms_real
		,"dissmiss".kms*-1 as 'kms'
		,"dissmiss".subtotal*-1 as 'subtotal'
		,"dissmiss".[peso-aceptado]*-1 as 'peso-aceptado'
		,"dissmiss".[peso-despachado]*-1 as 'peso-despachado'
		,"dissmiss".trip_count*-1 as 'trip_count'
		,1 as 'FlagIsNextMonth'
from
		sistemas.dbo.projections_view_full_gst_core_indicators as "dissmiss"
where 
		"dissmiss".FlagIsDisminution = 1
	and 
		"dissmiss".tipo_doc = 2	
				

				
-- ==================================================================================================================== --	
-- =============================      full query indicators for all company test    ======================================== --
-- ==================================================================================================================== --
-- Build for general core
-- projections_view_full_company_core_indicators
--select 
--		area,area_solomon,ciudad,id_area 
--from
--		sistemas.dbo.projections_view_full_gst_testcore_indicators
--group by 
--		area,area_solomon,ciudad,id_area
				
use sistemas
IF OBJECT_ID ('projections_view_full_gst_testcore_indicators', 'V') IS NOT NULL		
    DROP VIEW projections_view_full_gst_testcore_indicators;
    
alter view projections_view_full_gst_testcore_indicators
as 
(
		select 
				 *
	from 
			sistemas.dbo.projections_view_full_gst_core_indicators where tipo_doc = 2
	union all
	select 
			 *
	from 
			sistemas.dbo.projections_view_full_gst_discore_indicators
) 
-- ==================================================================================================== --	
-- @mechanish for dispatched and acepted web version
-- ==================================================================================================== --	

exec sp_who2




--========================================================================================================================================= --



use [sistemas]
-- go
IF OBJECT_ID('sistemas.dbo.projections_view_indicators_dispatch_periods_full_src_tbl_ops', 'U') IS NOT NULL 
  DROP TABLE sistemas.dbo.projections_view_indicators_dispatch_periods_full_src_tbl_ops 
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on
-- go
create table projections_view_indicators_dispatch_periods_full_src_tbl_ops(

									 id					 int 			identity(1,1)
									,company			 int			null
									,id_area			 int			null
									,area				 char(15)		null
									,tname				 char(7)		null
									,id_fraccion		 int			null
									,fraccion			 char(30)		null
									,cyear				 int			null
									,mes				 char(15)		null
									,kms				 int			null
									,subtotal			 decimal(18,6)	null
									,peso				 decimal(18,6)	null
									,"non-zero"			 int			null
									,FlagIsDisminution	 int			null
									,FlagIsProvision	 int			null
									,FlagIsNextMonth	 int			null
									,f_despachado_m		 int			null
									,created			 datetime		null
									,modified			 datetime		null
									,status				 tinyint default 1 null
) on [primary]
-- go
set ansi_padding off
-- go



use [sistemas]
-- go
IF OBJECT_ID('sistemas.dbo.projections_view_indicators_acepted_periods_full_src_tbl_ops', 'U') IS NOT NULL 
  DROP TABLE sistemas.dbo.projections_view_indicators_acepted_periods_full_src_tbl_ops 
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on
-- go
create table projections_view_indicators_acepted_periods_full_src_tbl_ops(

									 id					 int 			identity(1,1)
									,company			 int			null
									,id_area			 int			null
									,area				 char(15)		null
									,tname				 char(7)		null
									,id_fraccion		 int			null
									,fraccion			 char(30)		null
									,cyear				 int			null
									,mes				 char(15)		null
									,kms				 int			null
									,subtotal			 decimal(18,6)	null
									,peso				 decimal(18,6)	null
									,"non-zero"			 int			null
									,FlagIsDisminution	 int			null
									,FlagIsProvision	 int			null
									,FlagIsNextMonth	 int			null
									,fecha_guia_m		 int			null
									,created			 datetime		null
									,modified			 datetime		null
									,status				 tinyint default 1 null
) on [primary]
-- go
set ansi_padding off
-- go


-- ========================================================================================================================================= --
-- The Procedure every 5 mins or 3 perhaps ??
-- ========================================================================================================================================= --
use sistemas
ALTER procedure [dbo].[sp_projections_indicadores_operativos]
as

	declare @temp_table table ( -- despachado
									 id					 int			null
									,company			 int			null
									,id_area			 int			null
									,area				 char(15)		null
									,tname				 char(7)		null
									,id_fraccion		 int			null
									,fraccion			 char(30)		null
									,cyear				 int			null
									,mes				 char(15)		null
									,kms				 int			null
									,subtotal			 decimal(18,6)	null
									,peso				 decimal(18,6)	null
									,"non-zero"			 int			null
									,FlagIsDisminution	 int			null
									,FlagIsProvision	 int			null
									,FlagIsNextMonth	 int			null
									,f_despachado_m		 int			null
	)
	
	declare @acoe_table table ( -- aceptado
									 id					 int			null
									,company			 int			null
									,id_area			 int			null
									,area				 char(15)		null
									,tname				 char(7)		null
									,id_fraccion		 int			null
									,fraccion			 char(30)		null
									,cyear				 int			null
									,mes				 char(15)		null
									,kms				 int			null
									,subtotal			 decimal(18,6)	null
									,peso				 decimal(18,6)	null
									,"non-zero"			 int			null
									,FlagIsDisminution	 int			null
									,FlagIsProvision	 int			null
									,FlagIsNextMonth	 int			null
									,fecha_guia_m		 int			null
	)
		

	insert into @temp_table
		select * from sistemas.dbo.projections_view_indicators_dispatch_periods_full_first_ops
		
--	select * from @temp_table
		
	truncate table sistemas.dbo.projections_view_indicators_dispatch_periods_full_src_tbl_ops
--	print 'update table ' + cast(getdate() as varchar(max))
	insert into sistemas.dbo.projections_view_indicators_dispatch_periods_full_src_tbl_ops
		select 
				 "src".company
				,"src".id_area
				,"src".area
				,"src".tname
				,"src".id_fraccion
				,"src".fraccion
				,"src".cyear
				,"src".mes
				,"src".kms
				,"src".subtotal
				,"src".peso
				,"src"."non-zero"
				,"src".FlagIsDisminution
				,"src".FlagIsProvision
				,"src".FlagIsNextMonth
				,"src".f_despachado_m
				,current_timestamp
				,current_timestamp
				,1
		from @temp_table as "src"

-- add acepted row
	insert into @acoe_table
		select * from sistemas.dbo.projections_view_indicators_acepted_periods_full_first_ops  -- moded
		
--	select * from @temp_table
		
	truncate table sistemas.dbo.projections_view_indicators_acepted_periods_full_src_tbl_ops  -- moded
--	print 'update table ' + cast(getdate() as varchar(max))
	insert into sistemas.dbo.projections_view_indicators_acepted_periods_full_src_tbl_ops  --moded
		select 
				 "srca".company
				,"srca".id_area
				,"srca".area
				,"srca".tname
				,"srca".id_fraccion
				,"srca".fraccion
				,"srca".cyear
				,"srca".mes
				,"srca".kms
				,"srca".subtotal
				,"srca".peso
				,"srca"."non-zero"
				,"srca".FlagIsDisminution
				,"srca".FlagIsProvision
				,"srca".FlagIsNextMonth
				,"srca".fecha_guia_m
				,current_timestamp
				,current_timestamp
				,1
		from @acoe_table as "srca"	
		
		
--========================================================================================================================================= --

		


-- ====================================================================================================== --
-- ==============   Start Query About 'Despachado' Indicators with the new standart in GST company ====== --
-- ====================================================================================================== --
-- Fixed Compability
-- select * from sistemas.dbo.projections_view_full_gst_core_desp_indicators
-- select * from sistemas.dbo.projections_view_full_gst_core_ops_indicators
use sistemas
--IF OBJECT_ID ('projections_view_full_gst_core_desp_indicators', 'V') IS NOT NULL		
--    DROP VIEW projections_view_full_gst_core_desp_indicators;
IF OBJECT_ID ('projections_view_full_gst_core_ops_indicators', 'V') IS NOT NULL		
    DROP VIEW projections_view_full_gst_core_ops_indicators;
    
create view projections_view_full_gst_core_ops_indicators
with encryption
as 				
--with "despachado" as (
						select --row_number() over (order by no_guia) as 'id',
								 "desp".company
								,"desp".id_area
								,"desp".area
								,"desp".nombrecorto as 'tname'
								,"desp".id_tipo_operacion as 'id_fraccion'
								,ltrim(rtrim(replace("desp".tipo_de_operacion ,'FLETES' , ''))) as 'fraccion' 
								,year("desp".f_despachado) as 'dyear'
								,year("desp".fecha_guia) as 'ayear'
								,"desp".[mes-despacho] as 'dmes'
								,"desp".mes as 'ames'
								,"desp".kms as 'kms'
								,"desp".subtotal as 'subtotal'
								,"desp".[peso-despachado] as 'dpeso'
								,"desp".[peso-aceptado] as 'apeso'
								,"desp".trip_count as 'non-zero'
								,"desp".f_despachado_m
								,"desp".fecha_guia_m
								,"desp".projections_rp_definition
								,"desp".FlagIsDisminution
								,"desp".FlagIsProvision
								,"desp".FlagIsNextMonth
						from
								sistemas.dbo.projections_view_full_gst_testcore_indicators as "desp"
						where
								"desp".FlagIsDisminution <> 2 --> aceptado + canceladas
						and 
								"desp".no_guia is not null 
						-- this is a provisional patch
						and 
								"desp".no_viaje is not null
						and 
							(
								cast(left("desp".f_despachado_m,4) as int) in ( --- in year ?
																 year(current_timestamp)
																,year(dateadd(yy,-1,cast(current_timestamp as date)))
														     )
							or
								cast(left("desp".fecha_guia_m,4) as int) in ( --- in year ?
																 year(current_timestamp)
																,year(dateadd(yy,-1,cast(current_timestamp as date)))
														     )
							)



-- ====================================================================================================== --
-- ==============   Query for Despachado add some improvements in original query in GST company ====== --
-- ====================================================================================================== --
-- Fixed Compability

-- source select * from sistemas.dbo.projections_view_full_company_core_desp_indicators
-- Replace with old query select * from sistemas.dbo.projections_view_indicators_dispatch_periods_full_src_ops
-- SRC: select * from sistemas.dbo.projections_view_indicators_dispatch_periods_full_src_tbl_ops
-- select top 200 * from sistemas.dbo.projections_view_indicators_dispatch_periods_full_first_ops where mes = 'Noviembre' and cyear = 2020 and id_area = 1
use sistemas
IF OBJECT_ID ('projections_view_indicators_dispatch_periods_full_first_ops', 'V') IS NOT NULL		
    DROP VIEW projections_view_indicators_dispatch_periods_full_first_ops;
    
--create
alter view projections_view_indicators_dispatch_periods_full_first_ops
with encryption
as 			
select 
		 row_number() over (order by "desp".id_area) as 'id'
		,"desp".company
		,"desp".id_area
		,"desp".area
--		,"desp".tname
		,"desp".id_fraccion
		,"desp".fraccion
		,cast(left("desp".f_despachado_m,4) as int) as 'cyear'
		,"month".month_uname as 'mes'
		,sum("desp".kms) as 'kms'
		,sum("desp".subtotal) as 'subtotal'
		,sum("desp".dpeso) as 'peso'
		,sum("desp".[non-zero]) as 'non_zero'
		,"desp".FlagIsDisminution
		,"desp".FlagIsProvision
		,"desp".FlagIsNextMonth
		,"desp".f_despachado_m
from 
		sistemas.dbo.projections_view_full_gst_core_ops_indicators as "desp"
inner join 
		sistemas.dbo.generals_month_translations as "month"
	on
		"month".month_num = cast(right("desp".f_despachado_m,2) as int)	
where 
		"desp".FlagIsDisminution = 0
	and
		cast(left("desp".f_despachado_m,4) as int) in ( year(dateadd(yy,-1,current_timestamp)) , year(current_timestamp) )
group by
		 "desp".company
		,"desp".id_area
		,"desp".area
--		,"desp".tname
		,"desp".id_fraccion
		,"desp".fraccion
		,cast(left("desp".f_despachado_m,4) as int)
		,"month".month_uname
		,"desp".FlagIsDisminution
		,"desp".FlagIsProvision
		,"desp".FlagIsNextMonth
		,"desp".f_despachado_m

-- ==================================================================================================== --


-- ====================================================================================================== --
-- ==============   Query for Despachado add some improvements in original query in GST company ====== --
-- ====================================================================================================== --
-- SRC: select * from sistemas.dbo.projections_view_indicators_dispatch_periods_full_src_tbl_ops
-- select * from sistemas.dbo.projections_view_indicators_dispatch_periods_full_src_ops
-- select * from sistemas.dbo.projections_view_indicators_dispatch_periods_full_ops
use sistemas
IF OBJECT_ID ('projections_view_indicators_dispatch_periods_full_src_ops', 'V') IS NOT NULL		
    DROP VIEW projections_view_indicators_dispatch_periods_full_src_ops
    
create view projections_view_indicators_dispatch_periods_full_src_ops
with encryption
as 			
select 
		 row_number() over (order by "desp".id_area) as 'id'
		,"desp".company
		,"desp".id_area
		,rtrim(ltrim("desp".area)) as 'area'
--		,"desp".tname
		,"desp".id_fraccion
		,rtrim(ltrim("desp".fraccion)) as 'fraccion'
		,"desp".cyear
		,rtrim(ltrim("desp".mes)) as 'mes'
		,sum("desp".kms) as 'kms'
		,sum("desp".subtotal) as 'subtotal'
		,sum("desp".peso) as 'peso'
		,sum("desp".[non-zero]) as 'non_zero'
		-- add presupuesto
from 
		sistemas.dbo.projections_view_indicators_dispatch_periods_full_src_tbl_ops as "desp"
group by
		 "desp".company
		,"desp".id_area
		,"desp".area
--		,"desp".tname
		,"desp".id_fraccion
		,"desp".fraccion
		,"desp".cyear
		,"desp".mes
		
		
		
-- ============================================================================================================================ --
-- DEVELOPMENT OF ACEPTED
-- ============================================================================================================================ --		
-- origin view select * from sistemas.dbo.projections_view_indicators_periods_full_fleets		
-- select * from sistemas.dbo.ingresos_costos_granel_toneladas           		-- Presupuesto [mod no need]
--[0][1] select * from sistemas.dbo.projections_view_indicators_periods_fleets 		-- origin builder [must be containt the provision detail] 
--[0][0] select * from sistemas.dbo.projections_view_indicators_periods_full_fleets 	-- view in web [all data compacted]
-- ============================================================================================================================ --				
select * from sistemas.dbo.projections_view_full_company_indicators 	-- live full data [thus must be replaced by core]
select * from sistemas.dbo.projections_closed_period_datas				-- closed data [thus must be replaced by core]
-- thus become in 
select * from sistemas.dbo.projections_view_indicators_periods_fleets_parts    
-- fleets parts are contained in :
select * from sistemas.dbo.projections_view_indicators_periods_fleets   -- this must be [discore]
-- this containt the canceled portes
select * from sistemas.dbo.projections_view_closed_period_units
-- and
select * from sistemas.dbo.projections_dissmiss_cancelations
-- ============================================================================================================================ --
-- prototype
-- ====================================================================================================== --
-- ==============   Query for Despachado add some improvements in original query in GST company ====== --
-- ====================================================================================================== --
-- Fixed Compability
-- Prototype

-- source select * from sistemas.dbo.projections_view_full_gst_core_ops_indicators
-- Replace with old query select * from sistemas.dbo.projections_view_indicators_acepted_periods_full_src_ops
-- SRC: select * from sistemas.dbo.projections_view_indicators_acepted_periods_full_src_tbl_ops
use sistemas
IF OBJECT_ID ('projections_view_indicators_acepted_periods_full_first_ops', 'V') IS NOT NULL		
    DROP VIEW projections_view_indicators_acepted_periods_full_first_ops;
    
create view projections_view_indicators_acepted_periods_full_first_ops
with encryption
as 			
select 
		 row_number() over (order by "desp".id_area) as 'id'
		,"desp".company
		,"desp".id_area
		,"desp".area
		,"desp".tname
		,"desp".id_fraccion
		,"desp".fraccion
		,cast(left("desp".fecha_guia_m,4) as int) as 'cyear'
		,"month".month_uname as 'mes'
		,sum("desp".kms) as 'kms'
		,sum("desp".subtotal) as 'subtotal'
		,sum("desp".dpeso) as 'peso'
		,sum("desp".[non-zero]) as 'non_zero'
		,"desp".FlagIsDisminution
		,"desp".FlagIsProvision
		,"desp".FlagIsNextMonth
		,"desp".fecha_guia_m
from  -- select * from 
		sistemas.dbo.projections_view_full_gst_core_ops_indicators as "desp"
inner join 
		sistemas.dbo.generals_month_translations as "month"
	on
		"month".month_num = cast(right("desp".fecha_guia_m,2) as int)	
where
		"desp".FlagIsProvision = 0
	and
		cast(left("desp".fecha_guia_m,4) as int) in ( year(dateadd(yy,-1,current_timestamp)) , year(current_timestamp) )
group by
		 "desp".company
		,"desp".id_area
		,"desp".area
		,"desp".tname
		,"desp".id_fraccion
		,"desp".fraccion
		,cast(left("desp".fecha_guia_m,4) as int)
		,"month".month_uname
		,"desp".FlagIsDisminution
		,"desp".FlagIsProvision
		,"desp".FlagIsNextMonth
		,"desp".fecha_guia_m

		
select id_fraccion,fraccion,area,fraction,tipo_de_operacion,id_tipo_operacion,* from sistemas.dbo.projections_view_full_gst_testcore_indicators where id_area = 3

-- ==================================================================================================== --
-- Prototype
-- ====================================================================================================== --
-- ==============   Query for Aceptado add some improvements in original query in GST company ====== --
-- ====================================================================================================== --
-- SRC: select * from sistemas.dbo.projections_view_indicators_acepted_periods_full_src_tbl_ops
-- select * from sistemas.dbo.projections_view_indicators_acepted_periods_full_src_ops
-- select * from sistemas.dbo.projections_view_indicators_acepted_periods_full_ops
use sistemas
IF OBJECT_ID ('projections_view_indicators_acepted_periods_full_src_ops', 'V') IS NOT NULL		
    DROP VIEW projections_view_indicators_acepted_periods_full_src_ops
    
create view projections_view_indicators_acepted_periods_full_src_ops
with encryption
as 			
select 
		 row_number() over (order by "desp".id_area) as 'id'
		,"desp".company
		,"desp".id_area
		,rtrim(ltrim("desp".area)) as 'area'
--		,"desp".tname
		,"desp".id_fraccion
		,rtrim(ltrim("desp".fraccion)) as 'fraccion'
		,"desp".cyear
		,rtrim(ltrim("desp".mes)) as 'mes'
		,sum("desp".kms) as 'kms'
		,sum("desp".subtotal) as 'subsubtotal'
		,sum("desp".peso) as 'subpeso'
		,sum("desp".[non-zero]) as 'non_zero'
		,0 as 'presupuesto'
from 
		sistemas.dbo.projections_view_indicators_acepted_periods_full_src_tbl_ops as "desp"
group by
		 "desp".company
		,"desp".id_area
		,"desp".area
--		,"desp".tname
		,"desp".id_fraccion
		,"desp".fraccion
		,"desp".cyear
		,"desp".mes
		
	

-- check point --

--
--	select 
--			 id
--			,"acp".area
--			,"acp".fraccion
--			,"acp".cyear
--			,"acp".mes
--			,"acp".kms
--			,"acp".subsubtotal
--			,"acp".subpeso
--			,"acp".non_zero
--			,"acp".presupuesto
--	from sistemas.dbo.projections_view_indicators_periods_full_fleets	
--		
--	select 
--			id
--			,"acpt".company  		-- maybe not needed 
--			,"acpt".id_area			-- maybe not needed
--			,"acpt".area
--			,"acpt".id_fraccion		-- maybe not needed
--			,"acpt".fraccion
--			,"acpt".cyear
--			,"acpt".mes
--			,"acpt".kms
--			,"acpt".subtotal
--			,"acpt".peso
--			,"acpt".non_zero
--	from sistemas.dbo.projections_view_indicators_acepted_periods_full_src_ops	
--		
-- check point --
		
-- ============================================================================================================================ --
-- PROJECTION - [DEBUG]
-- ============================================================================================================================ --			

select * from sistemas.dbo.projections_view_indicators_periods_fleets
--
--with operativos 
--	as (	
--		select 
--				 "flr".area ,"flr".fraccion ,"flr".cyear ,"flr".mes ,sum("flr".kms) as 'kms' 
--				,sum("flr".subsubtotal) as 'subsubtotal' ,sum("flr".subpeso) as 'subpeso' 
--				,sum("flr".non_zero) as 'non_zero'
--				,case 
--					when ("flr".mes = 'Enero') then sum("prep".PtdBal00)
--					when ("flr".mes = 'Febrero') then sum("prep".PtdBal01)
--					when ("flr".mes = 'Marzo') then sum("prep".PtdBal02)
--					when ("flr".mes = 'Abril') then sum("prep".PtdBal03)
--					when ("flr".mes = 'Mayo') then sum("prep".PtdBal04)
--					when ("flr".mes = 'Junio') then sum("prep".PtdBal05)
--					when ("flr".mes = 'Julio') then sum("prep".PtdBal06)
--					when ("flr".mes = 'Agosto') then sum("prep".PtdBal07)
--					when ("flr".mes = 'Septiembre') then sum("prep".PtdBal08)
--					when ("flr".mes = 'Octubre') then sum("prep".PtdBal09)
--					when ("flr".mes = 'Noviembre') then sum("prep".PtdBal10)
--					when ("flr".mes = 'Diciembre') then sum("prep".PtdBal11)
--				end as 'presupuesto'
--		from -- select * from
--				sistemas.dbo.projections_view_indicators_periods_fleets as "flr"
--			inner join -- select * from
--				sistemas.dbo.projections_view_bussiness_units as "unit"
--			on 
--				"flr".area = "unit".name
--			left join -- select * from
--				sistemas.dbo.ingresos_costos_granel_toneladas as "prep"
--			on 
--				"flr".cyear = "prep".cyear collate Modern_Spanish_CI_AS and "flr".fraccion = "prep".fraction collate Modern_Spanish_CI_AS
--			and 
--				"unit".tname = "prep".CpnyID collate Modern_Spanish_CI_AS
--		group by 
--				"flr".area ,"flr".fraccion ,"flr".cyear ,"flr".mes
--	)
--	select
--			row_number()
--		over 
--			(order by area) as 
--								 id
--								,area
--								,fraccion
--								,cyear
--								,mes
--								,kms
--								,subsubtotal
--								,subpeso 
--								,non_zero
--								,presupuesto
--		from(
--				select
--						area ,fraccion ,cyear ,mes ,kms,subsubtotal,subpeso,non_zero,presupuesto
--				from 
--						operativos
--			) 
--	as result			
		
-- ============================================================================================================================ --
-- [PROJECTION] DEVELOPMENT AREA [WORKING]
-- ============================================================================================================================ --	
-- select top 199 * from sistemas.dbo.projections_view_indicators_periods_gst_fleets where fraccion = 'GRANEL'
use sistemas;
-- go
IF OBJECT_ID ('projections_view_indicators_periods_gst_fleets', 'V') IS NOT NULL		
    DROP VIEW projections_view_indicators_periods_gst_fleets;
   
--create view projections_view_indicators_periods_full_fleets
create view projections_view_indicators_periods_gst_fleets
with encryption
as
--with "operativos"
		select 
				 row_number() over (order by "desp".id_area) as 'id'
				,"desp".company
				,"desp".id_area
				,rtrim(ltrim("desp".area)) as 'area'
		--		,"desp".tname
				,"desp".id_fraccion
				,rtrim(ltrim("desp".fraccion)) as 'fraccion'
				,"desp".cyear
				,rtrim(ltrim("desp".mes)) as 'mes'
				,sum("desp".kms) as 'kms'
				,sum("desp".subtotal) as 'subsubtotal'
				,sum("desp".peso) as 'subpeso'
				,sum("desp".[non-zero]) as 'non_zero'
		--		,0 as 'presupuesto'
				,case 
					when ("desp".mes = 'Enero') then sum("prep".PtdBal00)
					when ("desp".mes = 'Febrero') then sum("prep".PtdBal01)
					when ("desp".mes = 'Marzo') then sum("prep".PtdBal02)
					when ("desp".mes = 'Abril') then sum("prep".PtdBal03)
					when ("desp".mes = 'Mayo') then sum("prep".PtdBal04)
					when ("desp".mes = 'Junio') then sum("prep".PtdBal05)
					when ("desp".mes = 'Julio') then sum("prep".PtdBal06)
					when ("desp".mes = 'Agosto') then sum("prep".PtdBal07)
					when ("desp".mes = 'Septiembre') then sum("prep".PtdBal08)
					when ("desp".mes = 'Octubre') then sum("prep".PtdBal09)
					when ("desp".mes = 'Noviembre') then sum("prep".PtdBal10)
					when ("desp".mes = 'Diciembre') then sum("prep".PtdBal11)
				end as 'presupuesto'
		from -- select * from 
				sistemas.dbo.projections_view_indicators_acepted_periods_full_src_tbl_ops as "desp"
				left join -- select * from
					sistemas.dbo.ingresos_costos_granel_toneladas as "prep"
				on 
					"desp".cyear = "prep".cyear collate Modern_Spanish_CI_AS and "desp".fraccion = "prep".fraction collate Modern_Spanish_CI_AS
				and 
					"desp".tname = "prep".CpnyID collate Modern_Spanish_CI_AS
		group by
				 "desp".company
				,"desp".id_area
				,"desp".area
		--		,"desp".tname
				,"desp".id_fraccion
				,"desp".fraccion
				,"desp".cyear
				,"desp".mes
	
-- ============================================================================================================================ --		
		
use sistemas
		
exec sp_desc ingresos_costos_granel_toneladas

exec sp_desc projections_view_indicators_acepted_periods_full_src_tbl_ops


-- checkpoint 
--select 
--		 "prep".id
--		,"prep".area
--		,"prep".fraccion
--		,"prep".cyear
--		,"prep".mes
--		,"prep".kms
--		,"prep".subsubtotal
--		,"prep".subpeso
--		,"prep".non_zero
--		,"prep".presupuesto
--from sistemas.dbo.projections_view_indicators_periods_full_fleets as "prep" where cyear = 2018	
--
---- 
--select 
--		 "prep".id
--		,"prep".company
--		,"prep".id_area
--		,"prep".area
--		,"prep".id_fraccion
--		,"prep".fraccion
--		,"prep".cyear
--		,"prep".mes
--		,"prep".kms
--		,"prep".subsubtotal
--		,"prep".subpeso
--		,"prep".non_zero
--		,"prep".presupuesto 
--from sistemas.dbo.projections_view_indicators_acepted_periods_full_src_ops as "prep"
--	



-- ============================================================================================================================ --	
-- [Snapshot mode] build backup 
-- ============================================================================================================================ --	
use sistemas
--IF OBJECT_ID ('projections_view_full_gst_core_desp_indicators', 'V') IS NOT NULL		
--    DROP VIEW projections_view_full_gst_core_desp_indicators;
IF OBJECT_ID ('projections_viewtbl_full_gst_core_ops_indicators', 'V') IS NOT NULL		
    DROP VIEW projections_viewtbl_full_gst_core_ops_indicators;
    
create view projections_viewtbl_full_gst_core_ops_indicators
with encryption
as 				
--with "despachado" as (
						select --row_number() over (order by no_guia) as 'id',
								 "desp".company
								,"desp".id_area
								,"desp".area
								,"desp".nombrecorto as 'tname'
								,"desp".id_tipo_operacion as 'id_fraccion'
								,ltrim(rtrim(replace("desp".tipo_de_operacion ,'FLETES' , ''))) as 'fraccion' 
								,year("desp".f_despachado) as 'dyear'
								,year("desp".fecha_guia) as 'ayear'
								,"desp".[mes-despacho] as 'dmes'
								,"desp".mes as 'ames'
								,"desp".kms as 'kms'
								,"desp".subtotal as 'subtotal'
								,"desp".[peso-despachado] as 'dpeso'
								,"desp".[peso-aceptado] as 'apeso'
								,"desp".trip_count as 'non-zero'
								,"desp".f_despachado_m
								,"desp".fecha_guia_m
								,"desp".projections_rp_definition
								,"desp".FlagIsDisminution
								,"desp".FlagIsProvision
								,"desp".FlagIsNextMonth
								,"desp".snapshot_period
						from
								sistemas.dbo.projections_table_full_gst_testcore_indicators as "desp"
						where
								"desp".FlagIsDisminution <> 2 --> aceptado + canceladas
						and 
								"desp".no_guia is not null 
						-- this is a provisional patch
						and 
								"desp".no_viaje is not null
						and 
							(
								cast(left("desp".f_despachado_m,4) as int) in ( --- in year ?
																 year(current_timestamp)
																,year(dateadd(yy,-1,cast(current_timestamp as date)))
														     )
							or
								cast(left("desp".fecha_guia_m,4) as int) in ( --- in year ?
																 year(current_timestamp)
																,year(dateadd(yy,-1,cast(current_timestamp as date)))
														     )
							)
							
							
--	select * from sistemas.dbo.projections_view_full_gst_testcore_indicators
--	
--	use sistemas
--	
--	exec sp_desc projections_view_full_gst_testcore_indicators
	

							
							
							
use [sistemas]

IF OBJECT_ID('sistemas.dbo.projections_table_full_gst_testcore_indicators', 'U') IS NOT NULL 
  DROP TABLE sistemas.dbo.projections_table_full_gst_testcore_indicators 

set ansi_nulls on
set quoted_identifier on
set ansi_padding on

	create table projections_table_full_gst_testcore_indicators (

									 id					 				int 			identity(1,1)  	-- add identity
									,company							int				null			-- start Table
									,id_area							int				null
									,no_viaje							int				null
									,id_area_liq						int				null
									,id_personal						int				null
									,no_liquidacion						int				null
									,fecha_liquidacion					datetime		null
									,id_ruta							int				null
									,no_ejes_viaje						int				null
									,id_origen							int				null
									,id_destino							int				null
									,"fecha-real-viaje"					datetime		null
									,"fecha-real-fin-viaje"				datetime		null
									,"fecha-despachado"					datetime		null
									,fecha_real_viaje					date			null
									,fecha_real_fin_viaje				date			null
									,f_despachado						date			null
									,lts_empresa						decimal			null
									,ton_viaje							decimal			null
									,id_unidad							varchar(10)		null
									,id_remolque1						varchar(20)		null
									,id_remolque2						varchar(20)		null
									,id_dolly							varchar(10)		null
									,id_ingreso							varchar(8)		null
									,status_viaje						char(1)			null
									,viajeactual						char(1)			null
									,no_tarjeta_llave					varchar(25)		null
									,id_configuracionviaje				int				null
									,id_areaviaje						int				null
									,fecha_real_viaje_m					int				null
									,fecha_real_fin_viaje_m				int				null
									,f_despachado_m						int				null
									,"day"								int				null
									,"mes-despacho"						nvarchar(61)	null
									,periodo_aceptado					int				null
									,periodo_despachado					int				null
									,no_guia							int				null
									,tipo_pago							char(1)			null
									,status_guia						char(1)			null
									,clasificacion_doc					int				null
									,"fecha-guia"						datetime		null	
									,"fecha-confirmacion"				datetime		null
									,"fecha-ingreso"					datetime		null
									,fecha_modifico						datetime		null
									,"fecha-cancelacion"				datetime		null
									,"fecha-contabilizado"				datetime		null
									,fecha_contabilizado_m				int				null
									,fecha_confirmacion_m				int				null
									,fecha_guia_m						int				null
									,fecha_cancelacion_m				int				null
									,fecha_contabilizado				date			null
									,fecha_confirmacion					date			null
									,fecha_guia							date			null
									,fecha_cancelacion					date			null
									,gday								int				null
									,convenido_tonelada					decimal			null
									,id_area_facturacion				int				null
									,id_cliente							int				null
									,gid_personal						int				null
									,id_remitente						int				null
									,Plaza								varchar(30)		null
									,Zona								varchar(20)		null
									,id_serieguia						int				null
									,id_plaza							int				null
									,num_serie							varchar(5)		null
									,area_solomon						varchar(100)	null
									,desc_plaza							varchar(30)		null
									,nombre								varchar(40)		null
									,nombrecorto						varchar(10)		null
									,ciudad								varchar(100)	null
									,id_destinatario					int				null
									,kms_guia							int				null
									,flete								decimal			null
									,seguro								decimal			null
									,maniobras							decimal			null
									,autopistas							decimal			null
									,otros								decimal			null
									,iva_guia							decimal			null
									,cobro_viaje_kms					decimal			null
									,tipo_doc							int				null
									,gid_origen							int				null
									,gid_destino						int				null
									,id_fraccion						int				null
									,fraccion							varchar(6)		null
									,projections_rp_definition			varchar(6)		null
									,plaza_emision						int				null
									,num_guia							varchar(20)		null
									,no_carta							varchar(15)		null
									,gid_remolque1						varchar(20)		null
									,gid_unidad							varchar(10)		null
									,no_remision						varchar(50)		null
									,motivo_cancelacion					varchar(250)	null
									,gid_remolque2						varchar(20)		null
									,prestamo							char(1)			null
									,num_guia_asignado					varchar(20)		null
									,no_deposito						int				null
									,gid_ingreso						varchar(8)		null
									,personalnombre						varchar(80)		null
									,tipo_facturacion					varchar(25)		null
									,no_transferencia_cobranza			int				null
									,factor_iva							decimal			null
									,num_guiacancel						varchar(25)		null
									,tipo_origen						int				null
									,no_poliza							varchar(20)		null
									,monto_retencion					decimal			null
									,status_pago						char(1)			null
									,monto_retenciontercero				decimal			null
									,sustituye_documento				varchar(20)		null
									,monto_ivaflete						decimal			null
									,tipocambioconvenio					int				null
									,desc_flete							decimal			null
									,flete_bruto						decimal			null
									,id_iva								int				null
									,id_retencion						int				null
									,id_convenio						int				null
									,id_areaconvenio					int				null
									,id_tipo_operacion					int				null
									,no_kit								varchar(10)		null
									,monto_ivadescto					decimal			null
									,monto_retdescto					decimal			null
									,monto_descto						decimal			null
									,periodo_facturacion				int				null
									,tipo_producto						varchar(20)		null
									,num_guia_incentivo					varchar(20)		null
									,facturado							int				null
									,id_modifico						varchar(50)		null
									,kms_convenio						int				null
									,observacion						varchar(1)		null
									,mes								nvarchar(61)	null
									,desc_ruta							varchar(40)		null
									,cliente							varchar(120)	null
									,id_flota							int				null
									,configuracion_viaje				varchar(20)		null
									,tipo_de_operacion					varchar(20)		null
									,flota								varchar(40)		null
									,area								varchar(8000)	null
									,fraction							varchar(60)		null
									,FlagIsDisminution					int				null
									,FlagIsProvision					int				null
									,kms_viaje							int				null
									,kms_real							int				null
									,kms								int				null
									,subtotal							decimal			null
									,"peso-aceptado"					decimal			null
									,"peso-despachado"					decimal			null
									,trip_count							int				null
									,FlagIsNextMonth					int				null	-- End Table
									,created			 				datetime		null   	-- created date
									,modified			 				datetime		null  	-- if modified
									,status_description					text			null   	-- some notes
									,snapshot_period					int				null   	-- ex: 201901
									,snapshot_ctrl_id					int				not null-- control id for snap				
									,status				 				tinyint default 1 null 	-- set active or inactive
) on [primary]

set ansi_padding off
-- go


-- fill table for snapshot

-- truncate table sistemas.dbo.projections_table_full_gst_testcore_indicators
use sistemas

select snapshot_period from sistemas.dbo.projections_table_full_gst_testcore_indicators
group by snapshot_period




insert into -- select * from sistemas.dbo.projections_view_full_gst_testcore_indicators
		sistemas.dbo.projections_table_full_gst_testcore_indicators
	select 
			 "snapshot".company
			,"snapshot".id_area
			,"snapshot".no_viaje
			,"snapshot".id_area_liq
			,"snapshot".id_personal
			,"snapshot".no_liquidacion
			,"snapshot".fecha_liquidacion
			,"snapshot".id_ruta
			,"snapshot".no_ejes_viaje
			,"snapshot".id_origen
			,"snapshot".id_destino
			,"snapshot"."fecha-real-viaje"
			,"snapshot"."fecha-real-fin-viaje"
			,"snapshot"."fecha-despachado"
			,"snapshot".fecha_real_viaje
			,"snapshot".fecha_real_fin_viaje
			,"snapshot".f_despachado
			,"snapshot".lts_empresa
			,"snapshot".ton_viaje
			,"snapshot".id_unidad
			,"snapshot".id_remolque1
			,"snapshot".id_remolque2
			,"snapshot".id_dolly
			,"snapshot".id_ingreso
			,"snapshot".status_viaje
			,"snapshot".viajeactual
			,"snapshot".no_tarjeta_llave
			,"snapshot".id_configuracionviaje
			,"snapshot".id_areaviaje
			,"snapshot".fecha_real_viaje_m
			,"snapshot".fecha_real_fin_viaje_m
			,"snapshot".f_despachado_m
			,"snapshot"."day"
			,"snapshot"."mes-despacho"
			,"snapshot".periodo_aceptado
			,"snapshot".periodo_despachado
			,"snapshot".no_guia
			,"snapshot".tipo_pago
			,"snapshot".status_guia
			,"snapshot".clasificacion_doc
			,"snapshot"."fecha-guia"
			,"snapshot"."fecha-confirmacion"
			,"snapshot"."fecha-ingreso"
			,"snapshot".fecha_modifico
			,"snapshot"."fecha-cancelacion"
			,"snapshot"."fecha-contabilizado"
			,"snapshot".fecha_contabilizado_m
			,"snapshot".fecha_confirmacion_m
			,"snapshot".fecha_guia_m
			,"snapshot".fecha_cancelacion_m
			,"snapshot".fecha_contabilizado
			,"snapshot".fecha_confirmacion
			,"snapshot".fecha_guia
			,"snapshot".fecha_cancelacion
			,"snapshot".gday
			,"snapshot".convenido_tonelada
			,"snapshot".id_area_facturacion
			,"snapshot".id_cliente
			,"snapshot".gid_personal
			,"snapshot".id_remitente
			,"snapshot".Plaza
			,"snapshot".Zona
			,"snapshot".id_serieguia
			,"snapshot".id_plaza
			,"snapshot".num_serie
			,"snapshot".area_solomon
			,"snapshot".desc_plaza
			,"snapshot".nombre
			,"snapshot".nombrecorto
			,"snapshot".ciudad
			,"snapshot".id_destinatario
			,"snapshot".kms_guia
			,"snapshot".flete
			,"snapshot".seguro
			,"snapshot".maniobras
			,"snapshot".autopistas
			,"snapshot".otros
			,"snapshot".iva_guia
			,"snapshot".cobro_viaje_kms
			,"snapshot".tipo_doc
			,"snapshot".gid_origen
			,"snapshot".gid_destino
			,"snapshot".id_fraccion
			,"snapshot".fraccion
			,"snapshot".projections_rp_definition
			,"snapshot".plaza_emision
			,"snapshot".num_guia
			,"snapshot".no_carta
			,"snapshot".gid_remolque1
			,"snapshot".gid_unidad
			,"snapshot".no_remision
			,"snapshot".motivo_cancelacion
			,"snapshot".gid_remolque2
			,"snapshot".prestamo
			,"snapshot".num_guia_asignado
			,"snapshot".no_deposito
			,"snapshot".gid_ingreso
			,"snapshot".personalnombre
			,"snapshot".tipo_facturacion
			,"snapshot".no_transferencia_cobranza
			,"snapshot".factor_iva
			,"snapshot".num_guiacancel
			,"snapshot".tipo_origen
			,"snapshot".no_poliza
			,"snapshot".monto_retencion
			,"snapshot".status_pago
			,"snapshot".monto_retenciontercero
			,"snapshot".sustituye_documento
			,"snapshot".monto_ivaflete
			,"snapshot".tipocambioconvenio
			,"snapshot".desc_flete
			,"snapshot".flete_bruto
			,"snapshot".id_iva
			,"snapshot".id_retencion
			,"snapshot".id_convenio
			,"snapshot".id_areaconvenio
			,"snapshot".id_tipo_operacion
			,"snapshot".no_kit
			,"snapshot".monto_ivadescto
			,"snapshot".monto_retdescto
			,"snapshot".monto_descto
			,"snapshot".periodo_facturacion
			,"snapshot".tipo_producto
			,"snapshot".num_guia_incentivo
			,"snapshot".facturado
			,"snapshot".id_modifico
			,"snapshot".kms_convenio
			,"snapshot".observacion
			,"snapshot".mes
			,"snapshot".desc_ruta
			,"snapshot".cliente
			,"snapshot".id_flota
			,"snapshot".configuracion_viaje
			,"snapshot".tipo_de_operacion
			,"snapshot".flota
			,"snapshot".area
			,"snapshot".fraction
			,"snapshot".FlagIsDisminution
			,"snapshot".FlagIsProvision
			,"snapshot".kms_viaje
			,"snapshot".kms_real
			,"snapshot".kms
			,"snapshot".subtotal
			,"snapshot"."peso-aceptado"
			,"snapshot"."peso-despachado"
			,"snapshot".trip_count
			,"snapshot".FlagIsNextMonth
			,current_timestamp --"snapshot".created
			,current_timestamp --"snapshot".modified
			,'Periodo August 2021' --"snapshot".status_description
			,202108 --"snapshot".snapshot_period	
			,38  -- snapshot_ctrl_id controls the block of an snapshot
			,1 --"snapshot".status			
	from 
		sistemas.dbo.projections_view_full_gst_testcore_indicators as "snapshot"

--	truncate table sistemas.dbo.projections_table_full_gst_testcore_indicators

		select top 1 * from sistemas.dbo.projections_table_full_gst_testcore_indicators

	
select snapshot_period,snapshot_ctrl_id
--	  ,sum(isnull(subtotal,0)) 
--		,status_description
from sistemas.dbo.projections_table_full_gst_testcore_indicators 
group by snapshot_period,snapshot_ctrl_id--,status_description





where num_guia in ('MA-49903','MA-49935')


select 
	 id_area
--	,id_unidad
--	,id_configuracionviaje
--	,id_tipo_operacion
--	,id_fraccion
--	,id_flota
--	,no_viaje
--	,num_guia
--	,id_ruta
--	,id_origen
--	,desc_ruta
--	,monto_retencion
--	,fecha_guia
	,mes
--	,"year"
--	,dia
--	,f_despachado
--	,cliente
--	,kms
--	,subtotal
--	,peso
--	,configuracion_viaje
--	,tipo_de_operacion
--	,flota
--	,area
--	,fraction
--	,fraccion
--	,company
--	,num_viajes
                ,periodo
                ,Descripcion
                ,sum(subtotal)
                ,fraccion
                ,snapshot_period
                ,snapshot_ctrl_id
--                ,created
from 
	"sistemas"."dbo"."operations_viewtbl_full_gst_dispatched_indicators"
where 
--	id_area = 8 and 
--	periodo = 201909 and 
	snapshot_period = periodo 
group by 
		 id_area
	    ,mes
		,periodo
        ,Descripcion
        ,fraccion
        ,snapshot_period
        ,snapshot_ctrl_id
--      ,created
order by id_area

-- ============================================================================================================================ --
-- DEVELOPMENT AREA [WORKING]
-- ============================================================================================================================ --		
--select * from sistemas.dbo.projections_view_full_gst_discore_indicators    -- must have acepted + edition  
--select * from sistemas.dbo.projections_view_full_company_discore_indicators  -- jan 19 donot have a disminution 


-- Provision edicion passtrough 
-- Disminution cancelesd in other months 


select 
		 status_guia,prestamo,tipo_doc
		,cast(substring( convert(nvarchar(MAX), fecha_guia, 112) , 1, 6 ) as int) as 'fecha_guia'
--		,cast(substring( convert(nvarchar(MAX), f_despacho, 112) , 1, 6 ) as int) as 'f_despachado'
		,cast(substring( convert(nvarchar(MAX), fecha_confirmacion, 112) , 1, 6 ) as int) as 'fecha_confirmacion'
		,cast(substring( convert(nvarchar(MAX), fecha_cancelacion, 112) , 1, 6 ) as int) as 'fecha_cancelacion'
		,id_area
		,count(id_area) as 'count'
from 
		gstdb.dbo.trafico_guia 
where 
		status_guia <> 'B' and prestamo = 'P' and tipo_doc = 2
group by 
		 status_guia,prestamo,tipo_doc
		,cast(substring( convert(nvarchar(MAX), fecha_guia, 112) , 1, 6 ) as int) 
		,cast(substring( convert(nvarchar(MAX), fecha_confirmacion, 112) , 1, 6 ) as int)
		,cast(substring( convert(nvarchar(MAX), fecha_cancelacion, 112) , 1, 6 ) as int)
		,id_area



select * from sistemas.dbo.projections_view_indicators_dispatch_periods_full_src_ops

select 
area
,sum(subtotal) as 'subtotal'
,f_despachado_m,fecha_guia_m,fecha_confirmacion_m,fecha_cancelacion_m
,prestamo,status_guia,tipo_doc,FlagIsDisminution,FlagIsProvision,FlagIsNextMonth
,count(id_area) as 'files'
from sistemas.dbo.projections_view_full_gst_core_indicators 
--where FlagIsProvision = 1 
group by 
area
--,no_viaje
,f_despachado_m,fecha_guia_m,fecha_confirmacion_m,fecha_cancelacion_m
,prestamo,status_guia,tipo_doc,FlagIsDisminution,FlagIsProvision,FlagIsNextMonth





select * from sistemas.dbo.projections_view_fractions

select * from sistemas.dbo.projections_fraccion_groups

select * from sistemas.dbo.projections_type_configs

select * from sistemas.dbo.projections_view_gst_areas

select * from gstdb.dbo.general_area

select * from sistemas.dbo.projections_view_gst_areas_operaciones

select * from sistemas.dbo.projections_view_bussiness_units 
---------------------------------------------------------------
--update 
--sistemas.dbo.module_user_credentials_controls
--set value = 'MEXICALI'
--where value = 'TIJUANA'
---------------------------------------------------------------

select kms_real,kms_viaje,kms,kms_convenio,kms_guia,id_convenio,* from sistemas.dbo.projections_view_full_gst_testcore_indicators where num_guia = 'OR-100527'

select kms_guia,kms_convenio,* from gstdb.dbo.trafico_guia where num_guia = 'OR-100527'

select kms_real,kms_viaje,* from gstdb.dbo.trafico_viaje where no_viaje = 79804

select * from gstdb.dbo.trafico_convenio where id_convenio = 1


select * from sistemas.dbo.module_user_credentials_mains where id = 2

--select @@Servername
exec sistemas.dbo.sp_projections_indicadores_operativos

select * from sistemas.dbo.projections_view_indicators_dispatch_periods_full_first_ops

select * from sistemas.dbo.projections_view_indicators_dispatch_periods_full_src_ops

select * from sistemas.dbo.projections_view_indicators_dispatch_periods_full_src_tbl_ops


select * from sistemas.dbo.projections_view_indicators_acepted_periods_full_first_ops

select * from sistemas.dbo.projections_view_indicators_acepted_periods_full_src_ops

select * from sistemas.dbo.projections_view_indicators_acepted_periods_full_src_tbl_ops

-- PRESUPUESTO
select * from sistemas.dbo.ingresos_costos_granel_toneladas


select 
	 company
	,id_area
	,area
	,frt
	,cyear
	,mes
	,periodo
	,subtotal
	,tname
	,PresupuestoIngresos
	,PresupuestoTon
	,PresupuestoKms
	,PresupuestoViajes
from
	"sistemas"."dbo"."ingresos_costos_view_ind_ppto_ingresos"
where 
--	id_area = 2
--and
--	company = 1
--and 
    area  = 'LA PAZ'
    
-- ============================================================================================================================ --
-- REVIEW OF PRESUPUESTO
-- ============================================================================================================================ --

    select * from sistemas.dbo.ingresos_costos_granel_ingresos where FiscYr = '2019'
    
    select * from sistemas.dbo.ingresos_costos_otros_ingresos where FiscYr = '2019'
    
    select * from sistemas.dbo.ingresos_costos_otros_ingresos_origins where FiscYr = '2019'
    
    select * from sistemas.dbo.ingresos_costos_granel_kilometros where FiscYr = '2019'
    
    
    
-- ============================================================================================================================ --
    
use sistemas    
select * from "ingresos_costos_view_ind_ppto_ingresos"
--cartasporte 2018
--prestamo = N , status_guia <> B ,fecha_confirmacion = null , campo4 = 2018 , campo5 = [1-sinviaje,2conviaje]




						select --row_number() over (order by no_guia) as 'id',
								 "desp".company
								,"desp".id_area
								,"desp".area
								,"desp".nombrecorto as 'tname'
								,"desp".id_tipo_operacion as 'id_fraccion'
								,ltrim(rtrim(replace("desp".tipo_de_operacion ,'FLETES' , ''))) as 'fraccion' 
								,year("desp".f_despachado) as 'dyear'
								,year("desp".fecha_guia) as 'ayear'
								,"desp".[mes-despacho] as 'dmes'
								,"desp".mes as 'ames'
								,"desp".kms as 'kms'
								,"desp".subtotal as 'subtotal'
								,"desp".[peso-despachado] as 'dpeso'
								,"desp".[peso-aceptado] as 'apeso'
								,"desp".trip_count as 'non-zero'
								,"desp".f_despachado_m
								,"desp".fecha_guia_m
								,"desp".projections_rp_definition
								,"desp".FlagIsDisminution
								,"desp".FlagIsProvision
								,"desp".FlagIsNextMonth
								,"desp".snapshot_period
						from
--								sistemas.dbo.projections_table_full_gst_core_indicators as "desp"
								sistemas.dbo.projections_view_full_gst_core_indicators as "desp"
						where
								"desp".FlagIsDisminution <> 2 --> aceptado + canceladas
						and 
								"desp".no_guia is not null 
						-- this is a provisional patch
						and 
								"desp".no_viaje is not null
						and 
							(
								cast(left("desp".f_despachado_m,4) as int) in ( --- in year ?
																 year(current_timestamp)
																,year(dateadd(yy,-1,cast(current_timestamp as date)))
														     )
							or
								cast(left("desp".fecha_guia_m,4) as int) in ( --- in year ?
																 year(current_timestamp)
																,year(dateadd(yy,-1,cast(current_timestamp as date)))
														     )
							)
							





