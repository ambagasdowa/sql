




-- -------------------------------------------------------------------------------------------------- --
-- NOTE Testing this going to become in a job 
-- -------------------------------------------------------------------------------------------------- --

insert into 
	sistemas.dbo.disponibilidad_tbl_hist_unidades_gst_indicators
	select
				 unidad, id_flota, flota, xid_status, xestatus, id_status, estatus, tipo_status, clasification_name
				,group_name, operador, remolque, id_area, area, id_tipo_operacion, segmento, description, compromise
				,status_viaje, desc_viaje, status_taller, desc_taller, units_type, 0 --as 'iseditable'
				, id_area_taller, area_taller
				,id_orden, fecha_inicio,fecha_ingreso, fecha_prometida
				,lastTrip
				,id_remolque1
				,id_remolque2
				,id_dolly
				,f_despachado
				,StatusDays
				,'2021-08-06' --created
				,1 -- status
	from
--				sistemas.dbo.disponibilidad_main_view_rpt_unidades_gst_indicators
				sistemas.dbo.disponibilidad_currentview_rpt_unidades_gst_indicators
	where 
				created =  convert(nvarchar(max),current_timestamp,23) 
