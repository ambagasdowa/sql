
select top 10 * from sistemas.dbo.disponibilidad_view_unidades_gst_indicators where area = 'MACUSPANA' --NOTE drop macuca line 304

select top 10 * from sistemas.dbo.disponibilidad_view_taller_gst_indicators where id_area = 7 --

select top 10 * from sistemas.dbo.disponibilidad_view_rpt_unidades_gst_indicators where id_area = 7

select top 10 * from sistemas.dbo.disponibilidad_view_cross_names --where id_area = 7

select top 10 * from sistemas.dbo.disponibilidad_view_cross_units where id_area = 7

select top 10 * from sistemas.dbo.disponibilidad_view_area_units where id_area = 7

select top 10 * from sistemas.dbo.disponibilidad_view_rpt_group_clasifications_indicators where id_area = 7

select top 10 * from sistemas.dbo.disponibilidad_view_rpt_group_gst_indicators --where id_area = 7


select top 300 * from sistemas.dbo.disponibilidad_view_historical_gst_indicators where unidad = 'TT1219'

select count(unidad) from sistemas.dbo.disponibilidad_view_historical_gst_indicators where unidad = 'TT1219'



--95926
with "this" as (
	select 
				"viaje".no_viaje,"viaje".id_unidad,"viaje".id_remolque1,"viaje".id_remolque2,"viaje".id_dolly,"viaje".f_despachado 
	from 
				gstdb.dbo.trafico_viaje as "viaje"
	where 
				"viaje".no_viaje  = (select max(cast(no_viaje as int )) from gstdb.dbo.trafico_viaje where id_unidad = "viaje".id_unidad) 
			)
	select * from "this" where "this".id_unidad = 'TT1201'

-- ==================================================================================================================================== --
-- NOTE Build in the proms
-- dates in main_view
-- ==================================================================================================================================== --

select 
		count(unidad) as "rows",area ,group_name,created 
from
		sistemas.dbo.disponibilidad_main_view_rpt_unidades_gst_indicators 
where 
		area = 'ORIZABA' and units_type in (1,13) and segmento = 'TOLVA ACERO' 
group by 
		created,area,group_name




with "days" as (
	select 
				1 as 'day',count(unidad) as "counting_rows" ,created 
	from 
				sistemas.dbo.disponibilidad_main_view_rpt_unidades_gst_indicators 
--	where 
--				created between '2021-08-04' and '2021-08-27'
group by created
)
select sum(day) as 'mday',count(counting_rows) as 'days' from "days"


select * from sistemas.dbo.disponibilidad_view_main_calculate_days
select * from sistemas.dbo.disponibilidad_main_view_calculate_days


select * from sistemas.dbo.disponibilidad_main_view_calculate_days

-- select @@SERVERNAME

select * from sistemas.dbo.disponibilidad_main_view_rpt_group_clasifications_indicators




select * from sistemas.dbo.disponibilidad_view_rpt_group_clasifications_indicators where id_area = 6

select
--			 id_area 
--			 area 
--			,id_flota 
--			,id_tipo_operacion 
--			,Flota 
--			,TipoVehiculo 
			 sum(Disponible)/3 as 'Disponible'																	-- qty
			,sum(Operando)/3 as 'Operando'																			-- qty
			,sum(Taller)/3 as 'Taller'																					-- qty
			,sum(Gestoria)/3 as 'Gestoria'																			-- qty
			,sum(Siniestrado)/3 as 'Siniestrado'																-- qty
			,sum(Robo)/3 as 'Robo'																							-- qty
			,sum(Exhibicion)/3 as 'Exhibicion'																	-- qty
			,sum(Venta)/3 as 'Venta'																						-- qty
			,sum(TotalDisponibilidad)/3 as 'TotalDisponibilidad'								-- qty
			,sum(TotalFlota)/3 as 'TotalFlota'																	-- qty Denominator
			,sum(DisponibilidadFlota)/3 as 'DisponibilidadFlota'
			,((sum(Disponible) + sum(Operando) * 100 ) / sum(TotalFlota)) as '%DispFlota'       -- Percentage 
			,sum(FlotaOperando)/3 as 'FlotaOperando'
			,( (sum(Operando) * 100 ) / sum(TotalFlota) ) as '%FlotaOp'
			,sum(Disp_S_Viaje)/3 as 'Disp_S_Viaje' 
			,( sum(Disponible) * 100 ) / sum(TotalFlota) as '%D_S_V'
			,sum(Op_FlotaGestoria_Siniestro)/3 as 'Op_FlotaGestoria_Siniestro' 
			,(sum(Op_FlotaGestoria_Siniestro)*100)/sum(TotalFlota) as '%OpFlotaGesSinies'
			,sum(FlotaMtto)/3 as 'FlotaMtto'
			,(sum(FlotaMtto)*100) / sum(TotalFlota) as '%FlotMtto'
--			,created 
from 
			sistemas.dbo.disponibilidad_main_view_rpt_group_clasifications_indicators where id_area = 1 
	and
			created between '2021-08-04' and GETDATE()
	and 
			id_tipo_operacion = 11 and TipoVehiculo = 'Tractocamiones'   
group by
			-- id_area 
--			area 
			--,id_flota 
--			,id_tipo_operacion 
			--,Flota 
			--,TipoVehiculo 
--			,Disponible 
--			,Operando 
--			,Taller 
--			,Gestoria 
--			,Siniestrado 
--			,Robo 
--			,Exhibicion 
--			,Venta 
--			,TotalDisponibilidad 
--			,TotalFlota 
--			,DisponibilidadFlota 
--			,FlotaOperando 
--			,Disp_S_Viaje 
--			,Op_FlotaGestoria_Siniestro 
--			,FlotaMtto 
--			,created 


 -- ==================================================================================================================================== --
 -- NOTE TEST
 -- ==================================================================================================================================== --



SELECT 
			 [DisponibilidadViewRptGroupClasificationsIndicator].[id_area] AS [DisponibilidadViewRptGroupClasificationsIndicator__0]
			,[DisponibilidadViewRptGroupClasificationsIndicator].[area] AS [DisponibilidadViewRptGroupClasificationsIndicator__1]
			,[DisponibilidadViewRptGroupClasificationsIndicator].[id_flota] AS [DisponibilidadViewRptGroupClasificationsIndicator__2]
			, [DisponibilidadViewRptGroupClasificationsIndicator].[id_tipo_operacion] AS [DisponibilidadViewRptGroupClasificationsIndicator__3]
			, [DisponibilidadViewRptGroupClasificationsIndicator].[Flota] AS [DisponibilidadViewRptGroupClasificationsIndicator__4]
			, [DisponibilidadViewRptGroupClasificationsIndicator].[TipoVehiculo] AS [DisponibilidadViewRptGroupClasificationsIndicator__5]
			, [DisponibilidadViewRptGroupClasificationsIndicator].[Disponible] AS [DisponibilidadViewRptGroupClasificationsIndicator__6]
			, [DisponibilidadViewRptGroupClasificationsIndicator].[Operando] AS [DisponibilidadViewRptGroupClasificationsIndicator__7]
			, [DisponibilidadViewRptGroupClasificationsIndicator].[Taller] AS [DisponibilidadViewRptGroupClasificationsIndicator__8]
			, [DisponibilidadViewRptGroupClasificationsIndicator].[Gestoria] AS [DisponibilidadViewRptGroupClasificationsIndicator__9]
			, [DisponibilidadViewRptGroupClasificationsIndicator].[Siniestrado] AS [DisponibilidadViewRptGroupClasificationsIndicator__10]
			, [DisponibilidadViewRptGroupClasificationsIndicator].[Robo] AS [DisponibilidadViewRptGroupClasificationsIndicator__11]
			, [DisponibilidadViewRptGroupClasificationsIndicator].[Exhibicion] AS [DisponibilidadViewRptGroupClasificationsIndicator__12]
			, [DisponibilidadViewRptGroupClasificationsIndicator].[Venta] AS [DisponibilidadViewRptGroupClasificationsIndicator__13]
			, [DisponibilidadViewRptGroupClasificationsIndicator].[TotalDisponibilidad] AS [DisponibilidadViewRptGroupClasificationsIndicator__14]
			, [DisponibilidadViewRptGroupClasificationsIndicator].[TotalFlota] AS [DisponibilidadViewRptGroupClasificationsIndicator__15]
			, [DisponibilidadViewRptGroupClasificationsIndicator].[DisponibilidadFlota] AS [DisponibilidadViewRptGroupClasificationsIndicator__16]
			, [DisponibilidadViewRptGroupClasificationsIndicator].[FlotaOperando] AS [DisponibilidadViewRptGroupClasificationsIndicator__17]
			, [DisponibilidadViewRptGroupClasificationsIndicator].[Disp_S_Viaje] AS [DisponibilidadViewRptGroupClasificationsIndicator__18]
			, [DisponibilidadViewRptGroupClasificationsIndicator].[Op_FlotaGestoria_Siniestro] AS [DisponibilidadViewRptGroupClasificationsIndicator__19]
			, [DisponibilidadViewRptGroupClasificationsIndicator].[FlotaMtto] AS [DisponibilidadViewRptGroupClasificationsIndicator__20]
--			, CONVERT(VARCHAR(20), [DisponibilidadViewRptGroupClasificationsIndicator].[created], 20) AS [DisponibilidadViewRptGroupClasificationsIndicator__21] 
FROM
			[disponibilidad_main_view_rpt_group_clasifications_indicators] AS [DisponibilidadViewRptGroupClasificationsIndicator] 
WHERE
			[DisponibilidadViewRptGroupClasificationsIndicator].[created] BETWEEN '2021-08-04' AND '2021-09-06' 
GROUP BY 
			id_area, area, id_flota, id_tipo_operacion, Flota, TipoVehiculo  --ORDER BY [Flota] desc, [TipoVehiculo] asc 
		,Disponible 
		,Operando 
		,Taller 
		,Gestoria 
		,Siniestrado 
		,Robo 
		,Exhibicion 
		,Venta 
		,TotalDisponibilidad 
		,TotalFlota 
		,DisponibilidadFlota 
		,FlotaOperando 
		,Disp_S_Viaje 
		,Op_FlotaGestoria_Siniestro 
		,FlotaMtto--,created 











 SELECT  [Disponibilidad].[id_area] AS [Disponibilidad__0], [Disponibilidad].[area] AS [Disponibilidad__1], [Disponibilidad].[id_flota] AS [Disponibilidad__2], [Disponibilidad].[id_tipo_operacion] AS [Disponibilidad__3], [Disponibilidad].[Flota] AS [Disponibilidad__4], [Disponibilidad].[TipoVehiculo] AS [Disponibilidad__5], CONVERT(VARCHAR(20), [Disponibilidad].[created], 20) AS [Disponibilidad__6], (sum([Disponibilidad].[Disponible])) AS  [Disponibilidad__Disponible], (sum([Disponibilidad].[Operando])) AS  [Disponibilidad__Operando], (sum([Disponibilidad].[Taller])) AS  [Disponibilidad__Taller], (sum([Disponibilidad].[Gestoria])) AS  [Disponibilidad__Gestoria], (sum([Disponibilidad].[Siniestrado])) AS  [Disponibilidad__Siniestrado], (sum([Disponibilidad].[Robo])) AS  [Disponibilidad__Robo], (sum([Disponibilidad].[Exhibicion])) AS  [Disponibilidad__Exhibicion], (sum([Disponibilidad].[Venta])) AS  [Disponibilidad__Venta], (sum([Disponibilidad].[TotalDisponibilidad])) AS  [Disponibilidad__TotalDisponibilidad], (sum([Disponibilidad].[TotalFlota])) AS  [Disponibilidad__TotalFlota], (sum([Disponibilidad].[DisponibilidadFlota])) AS  [Disponibilidad__DisponibilidadFlota], (sum([Disponibilidad].[FlotaOperando])) AS  [Disponibilidad__FlotaOperando], (sum([Disponibilidad].[Disp_S_Viaje])) AS  [Disponibilidad__Disp_S_Viaje], (sum([Disponibilidad].[Op_FlotaGestoria_Siniestro])) AS  [Disponibilidad__Op_FlotaGestoria_Siniestro], (sum([Disponibilidad].[FlotaMtto])) AS  [Disponibilidad__FlotaMtto] FROM [disponibilidad_main_view_rpt_group_clasifications_indicators] AS [Disponibilidad]   WHERE [Disponibilidad].[created] BETWEEN '2021-08-04' AND '2021-09-06'   ORDER BY [Flota] desc, [TipoVehiculo] asc 



















