use gstdb

-- ================================================================================================================================= --
-- ACEPTADO TEISA ENERO 2019
-- ================================================================================================================================= --

select 
	sum(subtotal) 
from 
		gstdb.dbo.trafico_guia 
where 
		status_guia <> 'B' and prestamo = 'N' and tipo_doc = 2 and id_area = 8 and id_tipo_operacion not in (1,2,6)
and 
	year(fecha_guia) = 2019 
and 
	month(fecha_guia) = 1 


-- ================================================================================================================================= --
-- DESPACHADO TEISA ENERO 2019
-- ================================================================================================================================= --

select 
		 sum("guia".subtotal)
		,case 
			when "guia".prestamo = 'N'
				then 
					'Aceptado'
			when 
				"guia".prestamo = 'P'
				then 
					'Provision'
		end as 'Desc'
from
		gstdb.dbo.trafico_viaje as "viaje" 
inner join 
		gstdb.dbo.trafico_guia as "guia"
	on 
		"viaje".id_area = "guia".id_area and "viaje".no_viaje = "guia".no_viaje
where 
	"viaje".id_area = 8
and 
	"guia".status_guia <> 'B' and "guia".tipo_doc = 2
and 
	"guia".id_tipo_operacion not in (1,2,6)
and 
	year("viaje".f_despachado) = '2019' and month("viaje".f_despachado) = 1
group by 
	"guia".prestamo
	
	

-- ================================================================================================================================= --
-- ACEPTADO Tutltilan ENERO 2019
-- ================================================================================================================================= --

select 
	sum(subtotal) 
from 
		gstdb.dbo.trafico_guia 
where 
		status_guia <> 'B' and prestamo = 'N' and tipo_doc = 2 
and id_area = 9 and id_tipo_operacion not in (1,2,6)
and 
	year(fecha_guia) = 2019 
and 
	month(fecha_guia) = 1 


-- ================================================================================================================================= --
-- DESPACHADO Tultitlan ENERO 2019
-- ================================================================================================================================= --

select 
		 sum("guia".subtotal)
		,case 
			when "guia".prestamo = 'N'
				then 
					'Aceptado'
			when 
				"guia".prestamo = 'P'
				then 
					'Provision'
		end as 'Desc'
from
		gstdb.dbo.trafico_viaje as "viaje" 
inner join 
		gstdb.dbo.trafico_guia as "guia"
	on 
		"viaje".id_area = "guia".id_area and "viaje".no_viaje = "guia".no_viaje
where 
	"viaje".id_area = 9
and 
	"guia".status_guia <> 'B' and "guia".tipo_doc = 2
and 
	"guia".id_tipo_operacion not in (1,2,6)
and 
	year("viaje".f_despachado) = '2019' and month("viaje".f_despachado) = 1
group by 
	"guia".prestamo
	
	
	

-- ================================================================================================================================= --
-- ACEPTADO Macuspana ENERO 2019
-- ================================================================================================================================= --

select 
	 sum("guia".subtotal) as 'subtotal'
	,sum("rguia".peso) as 'peso'
from 
		gstdb.dbo.trafico_guia as "guia"
inner join 
		gstdb.dbo.trafico_renglon_guia as "rguia"
	on
		"rguia".id_area = "guia".id_area and "rguia".no_guia = "guia".no_guia
where 
		"guia".status_guia <> 'B' and "guia".prestamo = 'N' and "guia".tipo_doc = 2 
and 
		"guia".id_area = 7 and "guia".id_tipo_operacion not in (1,2,6)
and 
	year("guia".fecha_guia) = 2019 
and 
	month("guia".fecha_guia) = 1 


-- ================================================================================================================================= --
-- DESPACHADO Macuspana ENERO 2019
-- ================================================================================================================================= --

select 
		 sum("guia".subtotal) as 'subtotal'
		,sum("rguia".peso) as 'peso' 
		,case 
			when "guia".prestamo = 'N'
				then 
					'Aceptado'
			when 
				"guia".prestamo = 'P'
				then 
					'Provision'
		end as 'Desc'
from
		gstdb.dbo.trafico_viaje as "viaje" 
inner join 
		gstdb.dbo.trafico_guia as "guia"
	on 
		"viaje".id_area = "guia".id_area and "viaje".no_viaje = "guia".no_viaje
inner join 
		gstdb.dbo.trafico_renglon_guia as "rguia"
	on
		"rguia".id_area = "guia".id_area and "rguia".no_guia = "guia".no_guia
where 
	"viaje".id_area = 7
and 
	"guia".status_guia <> 'B' and "guia".tipo_doc = 2
and 
	"guia".id_tipo_operacion not in (1,2,6)
and 
	year("viaje".f_despachado) = '2019' and month("viaje".f_despachado) = 1
group by 
	"guia".prestamo	
	