
	

with guia_tbk as 
		(
			select 
					 viaje.id_area
					,viaje.id_unidad
					,viaje.id_configuracionviaje
					,guia.id_tipo_operacion
					,guia.id_fraccion
					,manto.id_flota
					,viaje.no_viaje
					,viaje.no_liquidacion		-- < add
					,guia.no_guia
--					
					,guia.num_guia
					,viaje.id_ruta
					,viaje.id_origen
					,ruta.desc_ruta
					,guia.monto_retencion
--					
					,cast(guia.fecha_guia as date) as 'fecha_guia'
					,viaje.fecha_real_viaje
--					,(select datename(mm,guia.fecha_guia)) as 'mes'
					,upper(left("translation".month_name,1)) + right("translation".month_name,len("translation".month_name) - 1) as 'mes'
					,viaje.f_despachado
					,"liquidacion".fecha_liquidacion  -- < add
					,cliente.nombre as 'cliente'
					,viaje.kms_viaje
					,viaje.kms_real
					,guia.subtotal
--					,"trg".peso
					,(
						select 
								sum(isnull("tren".peso,0)) 
						from 
								bonampakdb.dbo.trafico_renglon_guia as "tren"
						where	
								"tren".no_guia = guia.no_guia and "tren".id_area = viaje.id_area
					 ) as 'peso'
					,(
						select 
								descripcion
						from
								bonampakdb.dbo.trafico_configuracionviaje as "trviaje"
						where
								trviaje.id_configuracionviaje = viaje.id_configuracionviaje
					) as 'configuracion_viaje'
					,(
						select 
								tipo_operacion
						from
								bonampakdb.dbo.desp_tipooperacion as "tpop"
						where 
								tpop.id_tipo_operacion = guia.id_tipo_operacion
					 ) as 'tipo_de_operacion'
					,(
						select 
								nombre
						from 
								bonampakdb.dbo.desp_flotas as "fleet"
						where
								fleet.id_flota = manto.id_flota
							
					) as 'flota'
					,(
						select
								ltrim(rtrim(replace(replace(replace(replace(replace(areas.nombre ,'AUTOTRANSPORTE' , ''),' S.A. DE C.V.',''),'BONAMPAK',''),'TRANSPORTADORA ESPECIALIZADA INDUSTRIAL','CUAUTITLAN'),'TRANSPORTE DE CARGA GEMINIS','TULTITLAN')))
						from 
								bonampakdb.dbo.general_area as "areas"
						where 
								areas.id_area = viaje.id_area
					) as 'area'
					,(
						select
								desc_producto
						from
							bonampakdb.dbo.trafico_producto as "producto"
						where      
							producto.id_producto = 0 and producto.id_fraccion = guia.id_fraccion
					) as 'fraccion'
					,'1' as 'company'
			from 
						bonampakdb.dbo.trafico_viaje as "viaje"
				left join 
						bonampakdb.dbo.trafico_guia as "guia"
					on	
						guia.status_guia in (select item from sistemas.dbo.fnSplit('R|T|C|A', '|'))
					and 
						guia.prestamo <> 'P'
					and 
						guia.tipo_doc = 2 
					and
						guia.id_area = viaje.id_area and guia.no_viaje = viaje.no_viaje
--				inner join
--						bonampakdb.dbo.trafico_renglon_guia as "trg"
--					on
--						"trg".no_guia = "guia".no_guia and "trg".id_area = "viaje".id_area 
				left join
						bonampakdb.dbo.trafico_cliente as "cliente"
					on 
						cliente.id_cliente = guia.id_cliente
				left join 
						bonampakdb.dbo.mtto_unidades as "manto"
					on 
						manto.id_unidad = viaje.id_unidad
				left join
						sistemas.dbo.generals_month_translations as "translation"
					on
						month(guia.fecha_guia) = "translation".month_num
				left join 
						bonampakdb.dbo.trafico_ruta as "ruta"
					on
						viaje.id_ruta = "ruta".id_ruta
				left join 
						bonampakdb.dbo.trafico_liquidacion as "liquidacion"
					on
						"liquidacion".no_liquidacion = "viaje".no_liquidacion and "liquidacion".id_area = "viaje".id_area
		)
	select 
				 "guia".id_area ,"guia".id_unidad ,"guia".id_configuracionviaje ,"guia".id_tipo_operacion ,"guia".id_fraccion ,"guia".id_flota 
				,"guia".no_viaje,"guia".no_liquidacion -- < add
				,"guia".no_guia,"guia".num_guia , "guia".id_ruta ,"guia".id_origen ,"guia".desc_ruta, "guia".monto_retencion
				,"guia".fecha_guia,"guia".fecha_real_viaje
				,"guia".mes ,"guia".f_despachado ,"guia".fecha_liquidacion -- < add
				,"guia".cliente 
				,case 
					when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company order by "guia".fecha_guia) ) > 1 
						then '0' else "guia".kms_viaje
				end as 'kms_viaje'
				,case 
					when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company order by "guia".fecha_guia) ) > 1 
						then '0' else "guia".kms_real
				end as 'kms_real'
				,sum("guia".subtotal) as 'subtotal' 
				,sum("guia".peso) as 'peso'
				,"guia".configuracion_viaje ,"guia".tipo_de_operacion ,"guia".flota ,"guia".area ,"guia".fraccion ,"guia".company 
				,case 
					when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company order by "guia".fecha_guia) ) > 1 
						then '0' else '1'
				end as 'trip_count'
	from 
				guia_tbk as "guia"
--	where 
--			cast("guia".fecha_liquidacion as date) between '2018-07-01' and '2018-07-23'
	group by 
				 "guia".id_area ,"guia".id_unidad ,"guia".id_configuracionviaje ,"guia".id_tipo_operacion ,"guia".id_fraccion ,"guia".id_flota
				,"guia".no_viaje,"guia".no_liquidacion -- < add
				,"guia".no_guia,"guia".num_guia , "guia".id_ruta ,"guia".id_origen ,"guia".desc_ruta, "guia".monto_retencion
				,"guia".fecha_guia,"guia".fecha_real_viaje
				,"guia".mes ,"guia".f_despachado , "guia".fecha_liquidacion  -- < add
				,"guia".cliente
				,"guia".kms_viaje,"guia".kms_real
				,"guia".configuracion_viaje ,"guia".tipo_de_operacion ,"guia".flota ,"guia".area ,"guia".fraccion ,"guia".company


	
	
-- ==   INSERT INTO PROJECTIONS MODULE == --
-- MODULE PROJECTION 
	
-- ==================================================================================================================== --	
-- =============================      MODDED QUERY FOR KMS PROJECTIONS     ======================================== --
-- ==================================================================================================================== --				
--Consulta Experta por el * pro-aminoguana Miguel
-- ==================================================================================================================== --	
-- =============================      full query indicators for all company    ======================================== --
-- ==================================================================================================================== --

use sistemas
IF OBJECT_ID ('projections_view_full_company_mod_indicators', 'V') IS NOT NULL		
    DROP VIEW projections_view_full_company_mod_indicators;
    
create view projections_view_full_company_mod_indicators
as 
(
	select 
			 id_area ,id_unidad ,id_configuracionviaje ,id_tipo_operacion ,id_fraccion ,id_flota 
			,no_viaje ,no_liquidacion ,num_guia ,fecha_guia ,mes ,f_despachado ,fecha_liquidacion 
			,cliente ,kms_viaje ,kms_real ,subtotal ,peso 
			,configuracion_viaje ,tipo_de_operacion ,flota ,area ,fraccion ,company ,trip_count 	
	from 
			sistemas.dbo.projections_view_full_indicators_tbk_mod_periods
	union all
	select 
			 id_area ,id_unidad ,id_configuracionviaje ,id_tipo_operacion ,id_fraccion ,id_flota 
			,no_viaje ,no_liquidacion ,num_guia ,fecha_guia ,mes ,f_despachado ,fecha_liquidacion 
			,cliente ,kms_viaje ,kms_real ,subtotal ,peso 
			,configuracion_viaje ,tipo_de_operacion ,flota ,area ,fraccion ,company ,trip_count 	
	from 
			sistemas.dbo.projections_view_full_indicators_atm_mod_periods
	union all
	select 
			 id_area ,id_unidad ,id_configuracionviaje ,id_tipo_operacion ,id_fraccion ,id_flota 
			,no_viaje ,no_liquidacion ,num_guia ,fecha_guia ,mes ,f_despachado ,fecha_liquidacion 
			,cliente ,kms_viaje ,kms_real ,subtotal ,peso 
			,configuracion_viaje ,tipo_de_operacion ,flota ,area ,fraccion ,company ,trip_count 	
	from 
			sistemas.dbo.projections_view_full_indicators_tei_mod_periods
) 


-- ==================================================================================================================== --	
-- =================================      full indicators for bonampakdb	     ====================================== --
-- ==================================================================================================================== --

use sistemas
IF OBJECT_ID ('projections_view_full_indicators_tbk_mod_periods', 'V') IS NOT NULL		
    DROP VIEW projections_view_full_indicators_tbk_mod_periods;
    
create view projections_view_full_indicators_tbk_mod_periods

with encryption
as
	with guia_tbk as 
		(
			select 
					 viaje.id_area
					,viaje.id_unidad
					,viaje.id_configuracionviaje
					,guia.id_tipo_operacion
					,guia.id_fraccion
					,manto.id_flota
					,viaje.no_viaje
					,viaje.no_liquidacion		-- < add
--					
					,guia.num_guia
					,viaje.id_ruta
					,viaje.id_origen
					,ruta.desc_ruta
					,guia.monto_retencion
--					
					,cast(guia.fecha_guia as date) as 'fecha_guia'
--					,(select datename(mm,guia.fecha_guia)) as 'mes'
					,upper(left("translation".month_name,1)) + right("translation".month_name,len("translation".month_name) - 1) as 'mes'
					,viaje.f_despachado
					,"liquidacion".fecha_liquidacion  -- < add
					,cliente.nombre as 'cliente'
					,viaje.kms_viaje
					,viaje.kms_real
					,guia.subtotal
--					,"trg".peso
					,(
						select 
								sum(isnull("tren".peso,0)) 
						from 
								bonampakdb.dbo.trafico_renglon_guia as "tren"
						where	
								"tren".no_guia = guia.no_guia and "tren".id_area = viaje.id_area
					 ) as 'peso'
					,(
						select 
								descripcion
						from
								bonampakdb.dbo.trafico_configuracionviaje as "trviaje"
						where
								trviaje.id_configuracionviaje = viaje.id_configuracionviaje
					) as 'configuracion_viaje'
					,(
						select 
								tipo_operacion
						from
								bonampakdb.dbo.desp_tipooperacion as "tpop"
						where 
								tpop.id_tipo_operacion = guia.id_tipo_operacion
					 ) as 'tipo_de_operacion'
					,(
						select 
								nombre
						from 
								bonampakdb.dbo.desp_flotas as "fleet"
						where
								fleet.id_flota = manto.id_flota
							
					) as 'flota'
					,(
						select
								ltrim(rtrim(replace(replace(replace(replace(replace(areas.nombre ,'AUTOTRANSPORTE' , ''),' S.A. DE C.V.',''),'BONAMPAK',''),'TRANSPORTADORA ESPECIALIZADA INDUSTRIAL','CUAUTITLAN'),'TRANSPORTE DE CARGA GEMINIS','TULTITLAN')))
						from 
								bonampakdb.dbo.general_area as "areas"
						where 
								areas.id_area = viaje.id_area
					) as 'area'
					,(
						select
								desc_producto
						from
							bonampakdb.dbo.trafico_producto as "producto"
						where      
							producto.id_producto = 0 and producto.id_fraccion = guia.id_fraccion
					) as 'fraccion'
					,'1' as 'company'
			from 
						bonampakdb.dbo.trafico_viaje as "viaje"
				left join 
						bonampakdb.dbo.trafico_guia as "guia"
					on	
						guia.status_guia in (select item from sistemas.dbo.fnSplit('R|T|C|A', '|'))
					and 
						guia.prestamo <> 'P'
					and 
						guia.tipo_doc = 2 
					and
						guia.id_area = viaje.id_area and guia.no_viaje = viaje.no_viaje
--				inner join
--						bonampakdb.dbo.trafico_renglon_guia as "trg"
--					on
--						"trg".no_guia = "guia".no_guia and "trg".id_area = "viaje".id_area 
				left join
						bonampakdb.dbo.trafico_cliente as "cliente"
					on 
						cliente.id_cliente = guia.id_cliente
				left join 
						bonampakdb.dbo.mtto_unidades as "manto"
					on 
						manto.id_unidad = viaje.id_unidad
				left join
						sistemas.dbo.generals_month_translations as "translation"
					on
						month(guia.fecha_guia) = "translation".month_num
				left join 
						bonampakdb.dbo.trafico_ruta as "ruta"
					on
						viaje.id_ruta = "ruta".id_ruta
				left join 
						bonampakdb.dbo.trafico_liquidacion as "liquidacion"
					on
						"liquidacion".no_liquidacion = "viaje".no_liquidacion and "liquidacion".id_area = "viaje".id_area
		)
	select 
				 "guia".id_area ,"guia".id_unidad ,"guia".id_configuracionviaje ,"guia".id_tipo_operacion ,"guia".id_fraccion ,"guia".id_flota 
				,"guia".no_viaje,"guia".no_liquidacion,"guia".num_guia , "guia".id_ruta ,"guia".id_origen ,"guia".desc_ruta, "guia".monto_retencion
				,"guia".fecha_guia
				,"guia".mes ,"guia".f_despachado ,"guia".fecha_liquidacion,"guia".cliente 
				,case 
					when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company order by "guia".fecha_guia) ) > 1 
						then 0 else "guia".kms_viaje
				end as 'kms_viaje'
				,case 
					when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company order by "guia".fecha_guia) ) > 1 
						then 0 else "guia".kms_real
				end as 'kms_real'
				,sum("guia".subtotal) as 'subtotal' 
				,sum("guia".peso) as 'peso'
				,"guia".configuracion_viaje ,"guia".tipo_de_operacion ,"guia".flota ,"guia".area ,"guia".fraccion ,"guia".company 
				,case 
					when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company order by "guia".fecha_guia) ) > 1 
						then 0 else 1
				end as 'trip_count'
	from 
				guia_tbk as "guia"
	group by 
				 "guia".id_area ,"guia".id_unidad ,"guia".id_configuracionviaje ,"guia".id_tipo_operacion ,"guia".id_fraccion ,"guia".id_flota
				,"guia".no_viaje,"guia".no_liquidacion,"guia".num_guia , "guia".id_ruta ,"guia".id_origen ,"guia".desc_ruta, "guia".monto_retencion
				,"guia".fecha_guia 
				,"guia".mes ,"guia".f_despachado,"guia".fecha_guia,"guia".fecha_liquidacion ,"guia".cliente
				,"guia".kms_viaje,"guia".kms_real
				,"guia".configuracion_viaje ,"guia".tipo_de_operacion ,"guia".flota ,"guia".area ,"guia".fraccion ,"guia".company

				
				
				
				
-- ==================================================================================================================== --	
-- =================================      full indicators for macuspanadb	     ====================================== --
-- ==================================================================================================================== --

use sistemas
IF OBJECT_ID ('projections_view_full_indicators_atm_mod_periods', 'V') IS NOT NULL		
    DROP VIEW projections_view_full_indicators_atm_mod_periods;
create view projections_view_full_indicators_atm_mod_periods

with encryption
as			
	with guia_atm as 
		(
			select 
					 viaje.id_area
					,viaje.id_unidad
					,viaje.id_configuracionviaje
					,guia.id_tipo_operacion
					,guia.id_fraccion
					,manto.id_flota
					,viaje.no_viaje
					,"liquidacion".no_liquidacion
--					
					,guia.num_guia
					,viaje.id_ruta
					,viaje.id_origen
					,ruta.desc_ruta
					,guia.monto_retencion
--					
					,cast(guia.fecha_guia as date) as 'fecha_guia'
--					,(select datename(mm,guia.fecha_guia)) as 'mes'
					,upper(left("translation".month_name,1)) + right("translation".month_name,len("translation".month_name) - 1) as 'mes'
					,viaje.f_despachado
					,"liquidacion".fecha_liquidacion
					,cliente.nombre as 'cliente'
					,viaje.kms_viaje
					,viaje.kms_real
					,guia.subtotal
--					,"trg".peso
					,(
						select 
								sum(isnull("tren".peso,0)) 
						from 
								macuspanadb.dbo.trafico_renglon_guia as "tren"
						where	
								"tren".no_guia = guia.no_guia and "tren".id_area = viaje.id_area
					 ) as 'peso'
					,(
						select 
								descripcion
						from
								macuspanadb.dbo.trafico_configuracionviaje as "trviaje"
						where
								trviaje.id_configuracionviaje = viaje.id_configuracionviaje
					) as 'configuracion_viaje'
					,(
						select 
								tipo_operacion
						from
								macuspanadb.dbo.desp_tipooperacion as "tpop"
						where 
								tpop.id_tipo_operacion = guia.id_tipo_operacion
					 ) as 'tipo_de_operacion'
					,(
						select 
								nombre
						from 
								macuspanadb.dbo.desp_flotas as "fleet"
						where
								fleet.id_flota = manto.id_flota
							
					) as 'flota'
					,(
						select
								ltrim(rtrim(replace(replace(replace(replace(replace(areas.nombre ,'AUTOTRANSPORTE' , ''),' S.A. DE C.V.',''),'BONAMPAK',''),'TRANSPORTADORA ESPECIALIZADA INDUSTRIAL','CUAUTITLAN'),'TRANSPORTE DE CARGA GEMINIS','TULTITLAN')))
						from 
								macuspanadb.dbo.general_area as "areas"
						where 
								areas.id_area = viaje.id_area
					) as 'area'
					,(
						select
								desc_producto
						from
							macuspanadb.dbo.trafico_producto as "producto"
						where      
							producto.id_producto = 0 and producto.id_fraccion = guia.id_fraccion
					) as 'fraccion'
					,'2' as 'company'
			from 
						macuspanadb.dbo.trafico_viaje as "viaje"
				left join 
						macuspanadb.dbo.trafico_guia as "guia"
					on	
						guia.status_guia in (select item from sistemas.dbo.fnSplit('R|T|C|A', '|'))
					and 
						guia.prestamo <> 'P'
					and 
						guia.tipo_doc = 2 
					and
						guia.id_area = viaje.id_area and guia.no_viaje = viaje.no_viaje
--				inner join
--						macuspanadb.dbo.trafico_renglon_guia as "trg"
--					on
--						"trg".no_guia = "guia".no_guia and "trg".id_area = "viaje".id_area 
				left join
						macuspanadb.dbo.trafico_cliente as "cliente"
					on 
						cliente.id_cliente = guia.id_cliente
				left join 
						macuspanadb.dbo.mtto_unidades as "manto"
					on 
						manto.id_unidad = viaje.id_unidad
				left join
						sistemas.dbo.generals_month_translations as "translation"
					on
						month(guia.fecha_guia) = "translation".month_num
				left join 
						macuspanadb.dbo.trafico_ruta as "ruta"
					on
						viaje.id_ruta = "ruta".id_ruta
				left join 
						macuspanadb.dbo.trafico_liquidacion as "liquidacion"
					on
						"liquidacion".no_liquidacion = "viaje".no_liquidacion and "liquidacion".id_area = "viaje".id_area
		)
	select 
				 "guia".id_area ,"guia".id_unidad ,"guia".id_configuracionviaje ,"guia".id_tipo_operacion ,"guia".id_fraccion ,"guia".id_flota 
				,"guia".no_viaje,"guia".no_liquidacion,"guia".num_guia , "guia".id_ruta ,"guia".id_origen ,"guia".desc_ruta, "guia".monto_retencion
				,"guia".fecha_guia
				,"guia".mes ,"guia".f_despachado ,"guia".fecha_liquidacion,"guia".cliente 
				,case 
					when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company order by "guia".fecha_guia) ) > 1 
						then 0 else "guia".kms_viaje
				end as 'kms_viaje'
				,case 
					when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company order by "guia".fecha_guia) ) > 1 
						then 0 else "guia".kms_real
				end as 'kms_real'
				,sum("guia".subtotal) as 'subtotal' 
				,sum("guia".peso) as 'peso'
				,"guia".configuracion_viaje ,"guia".tipo_de_operacion ,"guia".flota ,"guia".area ,"guia".fraccion ,"guia".company 
				,case 
					when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company order by "guia".fecha_guia) ) > 1 
						then 0 else 1
				end as 'trip_count'
	from 
				guia_atm as "guia"
	group by 
				 "guia".id_area ,"guia".id_unidad ,"guia".id_configuracionviaje ,"guia".id_tipo_operacion ,"guia".id_fraccion ,"guia".id_flota 
				,"guia".no_viaje ,"guia".no_liquidacion ,"guia".num_guia , "guia".id_ruta ,"guia".id_origen ,"guia".desc_ruta, "guia".monto_retencion
				,"guia".fecha_guia 
				,"guia".mes ,"guia".f_despachado ,"guia".fecha_liquidacion ,"guia".cliente
				,"guia".kms_viaje,"guia".kms_real
				,"guia".configuracion_viaje ,"guia".tipo_de_operacion ,"guia".flota ,"guia".area ,"guia".fraccion ,"guia".company

				
				
-- ==================================================================================================================== --	
-- ===============================      full indicators for tespecializadadb	  ===================================== --
-- ==================================================================================================================== --

use sistemas
IF OBJECT_ID ('projections_view_full_indicators_tei_mod_periods', 'V') IS NOT NULL		
    DROP VIEW projections_view_full_indicators_tei_mod_periods;
create view projections_view_full_indicators_tei_mod_periods

with encryption
as
	with guia_tei as 
		(
			select 
					 viaje.id_area
					,viaje.id_unidad
					,viaje.id_configuracionviaje
					,guia.id_tipo_operacion
					,guia.id_fraccion
					,manto.id_flota
					,viaje.no_viaje
					,"liquidacion".no_liquidacion
--					
					,guia.num_guia
					,viaje.id_ruta
					,viaje.id_origen
					,ruta.desc_ruta
					,guia.monto_retencion
--					
					,cast(guia.fecha_guia as date) as 'fecha_guia'
--					,(select datename(mm,guia.fecha_guia)) as 'mes'
					,upper(left("translation".month_name,1)) + right("translation".month_name,len("translation".month_name) - 1) as 'mes'
					,viaje.f_despachado
					,"liquidacion".fecha_liquidacion
					,cliente.nombre as 'cliente'
					,viaje.kms_viaje
					,viaje.kms_real
					,guia.subtotal
--					,"trg".peso
					,(
						select 
								sum(isnull("tren".peso,0)) 
						from 
								tespecializadadb.dbo.trafico_renglon_guia as "tren"
						where	
								"tren".no_guia = guia.no_guia and "tren".id_area = viaje.id_area
					 ) as 'peso'
					,(
						select 
								descripcion
						from
								tespecializadadb.dbo.trafico_configuracionviaje as "trviaje"
						where
								trviaje.id_configuracionviaje = viaje.id_configuracionviaje
					) as 'configuracion_viaje'
					,(
						select 
								tipo_operacion
						from
								tespecializadadb.dbo.desp_tipooperacion as "tpop"
						where 
								tpop.id_tipo_operacion = guia.id_tipo_operacion
					 ) as 'tipo_de_operacion'
					,(
						select 
								nombre
						from 
								tespecializadadb.dbo.desp_flotas as "fleet"
						where
								fleet.id_flota = manto.id_flota
							
					) as 'flota'
					,(
						select
								ltrim(rtrim(replace(replace(replace(replace(replace(areas.nombre ,'AUTOTRANSPORTE' , ''),' S.A. DE C.V.',''),'BONAMPAK',''),'TRANSPORTADORA ESPECIALIZADA INDUSTRIAL','CUAUTITLAN'),'TRANSPORTE DE CARGA GEMINIS','TULTITLAN')))
						from 
								tespecializadadb.dbo.general_area as "areas"
						where 
								areas.id_area = viaje.id_area
					) as 'area'
					,(
						select
								desc_producto
						from
							tespecializadadb.dbo.trafico_producto as "producto"
						where      
							producto.id_producto = 0 and producto.id_fraccion = guia.id_fraccion
					) as 'fraccion'
					,'3' as 'company'
			from 
						tespecializadadb.dbo.trafico_viaje as "viaje"
				left join 
						tespecializadadb.dbo.trafico_guia as "guia"
					on	
						guia.status_guia in (select item from sistemas.dbo.fnSplit('R|T|C|A', '|'))
					and 
						guia.prestamo <> 'P'
					and 
						guia.tipo_doc = 2 
					and
						guia.id_area = viaje.id_area and guia.no_viaje = viaje.no_viaje
--				inner join
--						tespecializadadb.dbo.trafico_renglon_guia as "trg"
--					on
--						"trg".no_guia = "guia".no_guia and "trg".id_area = "viaje".id_area 
				left join
						tespecializadadb.dbo.trafico_cliente as "cliente"
					on 
						cliente.id_cliente = guia.id_cliente
				left join 
						tespecializadadb.dbo.mtto_unidades as "manto"
					on 
						manto.id_unidad = viaje.id_unidad
				left join
						sistemas.dbo.generals_month_translations as "translation"
					on
						month(guia.fecha_guia) = "translation".month_num
				left join 
						tespecializadadb.dbo.trafico_ruta as "ruta"
					on
						viaje.id_ruta = "ruta".id_ruta
				left join 
						tespecializadadb.dbo.trafico_liquidacion as "liquidacion"
					on
						"liquidacion".no_liquidacion = "viaje".no_liquidacion and "liquidacion".id_area = "viaje".id_area
		)
	select 
				 "guia".id_area ,"guia".id_unidad ,"guia".id_configuracionviaje ,"guia".id_tipo_operacion ,"guia".id_fraccion ,"guia".id_flota
				,"guia".no_viaje ,"guia".no_liquidacion,"guia".num_guia , "guia".id_ruta ,"guia".id_origen ,"guia".desc_ruta, "guia".monto_retencion
				,"guia".fecha_guia
				,"guia".mes ,"guia".f_despachado , "guia".fecha_liquidacion ,"guia".cliente 
				,case 
					when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company order by "guia".fecha_guia) ) > 1 
						then 0 else "guia".kms_viaje
				end as 'kms_viaje'
				,case 
					when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company order by "guia".fecha_guia) ) > 1 
						then 0 else "guia".kms_real
				end as 'kms_real'
				,sum("guia".subtotal) as 'subtotal' 
				,sum("guia".peso) as 'peso'
				,"guia".configuracion_viaje ,"guia".tipo_de_operacion ,"guia".flota ,"guia".area ,"guia".fraccion ,"guia".company 
				,case 
					when ( row_number() over(partition by "guia".no_viaje,"guia".id_area,"guia".company order by "guia".fecha_guia) ) > 1 
						then 0 else 1
				end as 'trip_count'
	from 
				guia_tei as "guia"
	group by 
				 "guia".id_area ,"guia".id_unidad ,"guia".id_configuracionviaje ,"guia".id_tipo_operacion ,"guia".id_fraccion ,"guia".id_flota 
				,"guia".no_viaje ,"guia".no_liquidacion,"guia".num_guia , "guia".id_ruta ,"guia".id_origen ,"guia".desc_ruta, "guia".monto_retencion
				,"guia".fecha_guia 
				,"guia".mes ,"guia".f_despachado ,"guia".fecha_liquidacion,"guia".cliente
				,"guia".kms_viaje,"guia".kms_real
				,"guia".configuracion_viaje ,"guia".tipo_de_operacion ,"guia".flota ,"guia".area ,"guia".fraccion ,"guia".company
		
				
				
				
	select 
			* 
	from 
			sistemas.dbo.projections_view_full_company_mod_indicators
	where 
			cast(fecha_liquidacion as date) between '2018-07-01' and '2018-07-23'

			
			
	select 
			* 
	from 
			sistemas.dbo.projections_view_full_company_mod_indicators
	where 
			cast(fecha_liquidacion as date) between '2018-08-03' and '2018-09-02'
			
			
			
			
	
	
	select 
			  dateadd(dd,2,DATEADD(month,month(CURRENT_TIMESTAMP)-1,DATEADD(year,year(CURRENT_TIMESTAMP)-1900,0))) as 'offset-2'
			 ,DATEADD(month,month(CURRENT_TIMESTAMP)-1,DATEADD(year,year(CURRENT_TIMESTAMP)-1900,0)) as 'first'
			 ,DATEADD(day,-1,DATEADD(month,month(CURRENT_TIMESTAMP),DATEADD(year,year(CURRENT_TIMESTAMP)-1900,0))) as 'last'
			 ,dateadd(dd,2,DATEADD(day,-1,DATEADD(month,month(CURRENT_TIMESTAMP),DATEADD(year,year(CURRENT_TIMESTAMP)-1900,0)))) as 'offset+2'
			 ,DATEPART(week, CURRENT_TIMESTAMP) as 'semana'
			 ,DATEPART(weekday, CURRENT_TIMESTAMP) as 'diaSem'
			 ,DATEPART(dw, CURRENT_TIMESTAMP) as 'dw'
			 ,datename(dw,CURRENT_TIMESTAMP) as 'day'
			 ,datepart(day,CURRENT_TIMESTAMP) as 'dayMonth'
	-- Start Cases 
			 ,case
			 	when
					-- check if is holiday or sunday
					datename( dw,DATEADD(month,month(CURRENT_TIMESTAMP)-1,DATEADD(year,year(CURRENT_TIMESTAMP)-1900,0)) ) <> 1
			 		and 
			 		
			 
				 
select * from sistemas.dbo.ingresos_costos_view_periods_controls

select * from sistemas.dbo.ingresos_costos_view_periods_controls		 

select * from sistemas.dbo.ingresos_costos_gst_holidays
				
select * from sistemas.dbo.ingresos_costos_his_holidays

select * from sistemas.dbo.ingresos_costos_holidays

exec sistemas.dbo.ingresos_costos_gerencial_indicators




select 
		 "hol".id
		,"hol".yr
		,"hol".Holiday_name
		,"hol".Holiday_date
		,day("hol".Holiday_date) as "dia"
		,month("hol".Holiday_date) as "month"
		,"hol".dw
		,"hol".is_sunday
		,"hol".is_sat_sun
		,"hol"._period
		,"hol".first_day_in_month
		,"hol".last_day_in_month 
from 
		sistemas.dbo.ingresos_costos_holidays as "hol"
		
-- =========================  union with this ============================== --

-- ==================================================================================================================== --	
-- =============================== 	     Calendar for KMS Projection			  ===================================== --
-- ==================================================================================================================== --

use sistemas
IF OBJECT_ID ('projections_view_kms_calendars', 'V') IS NOT NULL		
    DROP VIEW projections_view_kms_calendars;
create view projections_view_kms_calendars
with encryption
as	
with "calendar" as (		
 select 
 		"years"."id","years"."cyear","years"."id_month","years"."month","years"."period","years"."_period"
 		,DATEADD(month,month(cast("years"."cyear"+'-'+"years"."id_month"+'-'+'01' as date))-1,DATEADD(year,year(cast("years"."cyear"+'-'+"years"."id_month"+'-'+'01' as date))-1900,0)) as 'first'
 		,DATEADD(day,-1,DATEADD(month,month(cast("years"."cyear"+'-'+"years"."id_month"+'-'+'01' as date)),DATEADD(year,year(cast("years"."cyear"+'-'+"years"."id_month"+'-'+'01' as date))-1900,0))) as 'last'
 		,"holidays".day_one
 		,"holdays".day_two
 		,"holidays".dw
 		,"holidays".is_sunday
 		,case
 			when 
 				("holidays".day_one is not null ) -- isn't sun or sat
 			then 1
 		 else 0
 		 end as 'one'
 		,case
 			when 
 				("holdays".day_two is not null ) -- isn't sun or sat
 			then 1
 		 else 0
 		 end as 'two'
 		,"holidays".Holiday_name
 		,"holidays".Holiday_date
 from 
 		sistemas.dbo.reporter_views_years as "years"
 	left join 
 		(		
			select 
					 "hol".id
					,"hol".yr
					,"hol".Holiday_name
					,"hol".Holiday_date
					,day("hol".Holiday_date) as "day_one"
					,month("hol".Holiday_date) as "month"
					,"hol".dw
					,"hol".is_sunday
					,"hol".is_sat_sun
					,"hol"._period
					,"hol".first_day_in_month
					,"hol".last_day_in_month 
			from 
					sistemas.dbo.ingresos_costos_holidays as "hol"
 		) as "holidays"
 	on 
 		"years".period collate SQL_Latin1_General_CP1_CI_AS = "holidays"._period
 		and 
 			"years".cyear = "holidays".yr
  		and
 		"holidays".day_one in (1)	
 	left join 
 		(		
			select 
					 "hol".id
					,"hol".yr
					,"hol".Holiday_name
					,"hol".Holiday_date
					,day("hol".Holiday_date) as "day_two"
					,month("hol".Holiday_date) as "month"
					,"hol".dw
					,"hol".is_sunday
					,"hol".is_sat_sun
					,"hol"._period
					,"hol".first_day_in_month
					,"hol".last_day_in_month 
			from 
					sistemas.dbo.ingresos_costos_holidays as "hol"
 		) as "holdays"
 	on 
 		"years".period collate SQL_Latin1_General_CP1_CI_AS = "holdays"._period
 		and 
 			"years".cyear = "holdays".yr
  		and
 		"holdays".day_two in (2) 		
)
select 
	id
	,cyear
	,id_month
	,month
	,period
	,_period
	,"first"
	,last
	,day_one
	,day_two
	,dw
	,is_sunday
	,one
	,two
	,Holiday_name
	,Holiday_date
	,dateadd(dd,(2+one+two),"first") as 'ini'
	,dateadd(dd,2,"last") as 'root-offset'
from "calendar"

-- ==================================================================================================================== --	
-- ============================== 	     Calendar Final for KMS Projection			 ================================== --
-- ==================================================================================================================== --
 
use sistemas
IF OBJECT_ID ('projections_view_kms_calendars_ends', 'V') IS NOT NULL		
    DROP VIEW projections_view_kms_calendars_ends;
create view projections_view_kms_calendars_ends
with encryption
as	
select 
		"cal".id
		,"cal".cyear
		,"cal".id_month
		,"cal"."month"
		,"cal"."period"
		,"cal"."_period"
		,"cal"."first"
		,"cal"."last"
		,"cal"."day_one"
		,"cal"."day_two"
		,"cal"."dw"
		,"cal"."is_sunday"
		,"cal"."one"
		,"cal"."two"
		,"cal"."Holiday_name"
		,"cal"."Holiday_date"
		,"cal"."ini"
		,"cal"."root-offset"
		,isnull((select coalesce(("calendar".one+"calendar".two),0) from sistemas.dbo.projections_view_kms_calendars as "calendar"  where "calendar".cyear = year(dateadd(dd,2,"cal"."last")) and "calendar".id_month = month(dateadd(dd,2,"cal"."last"))),0) as "suml"
		,dateadd(dd,(2+isnull((select coalesce(("calendar".one + "calendar".two),0) from sistemas.dbo.projections_view_kms_calendars as "calendar"  where "calendar".cyear = year(dateadd(dd,2,"cal"."last")) and "calendar".id_month = month(dateadd(dd,2,"cal"."last"))),0)),"cal"."last") as 'end'
from 
		sistemas.dbo.projections_view_kms_calendars as "cal"
--order by 
--		"cal"."period"
	
select * from sistemas.dbo.projections_view_kms_calendars_ends		



-- ==================================================================================================================== --	
-- =========================== 	     Calendar Final Tabular for KMS Projection			 ============================== --
-- ==================================================================================================================== --
 
use sistemas
IF OBJECT_ID ('projections_view_kms_calendars_finals', 'V') IS NOT NULL		
    DROP VIEW projections_view_kms_calendars_finals;
create view projections_view_kms_calendars_finals
with encryption
as		
select 
		 "cal".id
		,"cal".cyear
--		,"cal".id_month
		,"cal"."month"
		,"cal"."period"
--		,"cal"."_period"
--		,"cal"."first"
--		,"cal"."last"
--		,"cal".day_one
--		,"cal".day_two
		,"cal".dw
--		,"cal".is_sunday
--		,"cal".one
--		,"cal".two
		,"cal".Holiday_name
		,"cal".Holiday_date
		,cast("cal".ini as date) as "ini"
		,"cal"."root-offset","cal".suml
		,cast("cal"."end" as date) as "end"
		,"kms".*
from 
		sistemas.dbo.projections_view_kms_calendars_ends as "cal"
inner join 
		sistemas.dbo.projections_view_full_company_mod_indicators as "kms"
	on
		cast("kms".fecha_liquidacion as date) between cast("cal".ini as date) and cast("cal"."end" as date)

		
-- =========================================================================================== --
-- -------------------------  Build the System for Projections Kms --------------------------- --
-- =========================================================================================== --
		
-- Set the Query for kms 
select * from sistemas.dbo.projections_view_full_company_mod_indicators		


-- -------------------------  Query for Liq-Kms --------------------------- --
-- Liq-kms by Period Mech
use sistemas
IF OBJECT_ID ('projections_view_kms_projection_rpts', 'V') IS NOT NULL		
    DROP VIEW projections_view_kms_projection_rpts;
create view projections_view_kms_projection_rpts
with encryption
as		
select 
		"cal".id
		,"cal".cyear
		,"cal"."month"
		,"cal".period
		,"cal".dw,Holiday_name
		,"cal".Holiday_date
		,"cal".ini
		,"cal"."root-offset"
		,"cal".suml
		,"cal"."end"
		,"cal".id_area
		,"cal".id_unidad
		,"cal".id_configuracionviaje
		,"cal".id_tipo_operacion
		,"cal".id_fraccion
		,"cal".id_flota
		,"cal".no_viaje
		,"cal".no_liquidacion
		,"cal".num_guia
		,"cal".fecha_guia
		,"cal".mes
		,"cal".f_despachado
		,"cal".fecha_liquidacion
		,"cal".cliente
		,"cal".kms_viaje
		,"cal".kms_real
		,"cal".subtotal
		,"cal".peso
		,"cal".configuracion_viaje
		,"cal".tipo_de_operacion
		,"cal".flota
		,case 
			when "cal".id_area = 4 and "cal".company = 1 then 'MEXICALI'
			when "cal".id_area = 2 and "cal".company = 1 and "cal".id_tipo_operacion = 12 then 'LA PAZ'
			else "cal".area
		 end as 'area'
		,"cal".fraccion
		,"cal".company
		,"cal".trip_count
from
		sistemas.dbo.projections_view_kms_calendars_finals as "cal"
where "cal".period in 
					 (
--					 	201807,201808,201809
						 substring( convert(nvarchar(MAX), dateadd(mm,-2,cast(current_timestamp as date)), 112) , 1, 6 ) -- current month minus 2
						,substring( convert(nvarchar(MAX), dateadd(mm,-1,cast(current_timestamp as date)), 112) , 1, 6 ) -- current month minus 1
						,substring( convert(nvarchar(MAX), current_timestamp, 112) , 1, 6 ) 							 -- current month					 
					 )
					 

-- Search the current and backwards months
-- Build the costos view 

-- -------------------------  Query for Costos --------------------------- --
-- Costos by Period Mech
-- select * from sistemas.dbo.projections_view_costos_projection_rpts
use sistemas
IF OBJECT_ID ('projections_view_costos_projection_rpts', 'V') IS NOT NULL		
    DROP VIEW projections_view_costos_projection_rpts;
create view projections_view_costos_projection_rpts
with encryption
as	
select
		 "dco".id
		,"dco"."_source_company"
		,"dco".area
		,"dco".UnidadNegocio
		,"dco".mes
		,"dco".account
		,"dco".name
		,sum("dco"."Real") as 'Real'
		,sum("dco".Presupuesto) as 'Presupuesto'
		,"dco".[_period]
		,"dco".Descripcion
		,"dco".NombreEntidad
		,"dco".TipoTransaccion
		,"dco".Referencia
		,"dco".ReferenciaExterna
		,"dco"."type"
		,"dco"."cyear"
from 
		sistemas.dbo.reporter_portal_costos_details_accounts as "dco"
where 
		"dco".[_period] in 
						  (
							 substring( convert(nvarchar(MAX), dateadd(mm,-2,cast(current_timestamp as date)), 112) , 1, 6 ) -- current month minus 2
							,substring( convert(nvarchar(MAX), dateadd(mm,-1,cast(current_timestamp as date)), 112) , 1, 6 ) -- current month minus 1
							,substring( convert(nvarchar(MAX), current_timestamp, 112) , 1, 6 ) 							 -- current month
						  )
group by 
		 "dco".id
		,"dco"."_source_company"
		,"dco".area
		,"dco".UnidadNegocio
		,"dco".mes
		,"dco".account
		,"dco".name
		,"dco"."_period"
		,"dco".Descripcion
		,"dco".NombreEntidad
		,"dco".TipoTransaccion
		,"dco".Referencia
		,"dco".ReferenciaExterna
		,"dco"."type"
		,"dco"."cyear"


-- Next Issue 
use sistemas
IF OBJECT_ID ('projections_view_holidays_projection_rpts', 'V') IS NOT NULL		
    DROP VIEW projections_view_holidays_projection_rpts;
create view projections_view_holidays_projection_rpts
with encryption
as
	select 
			"hol".id
			,"hol".cyear
			,"hol".id_month
			,"hol"."month"
			,"hol".period
			,"hol"."_period"
			,"hol".first_day_in_month
			,"hol".last_day_in_month
			,"hol".is_current
			,"hol".hasSun
			,"hol".hasFullSun
			,"hol".hasCurrentHoliday
			,"hol".hisFullHoliday
			,"hol".labDays
			,"hol".labFullDays
			,"hol".labRestDays
	from
			sistemas.dbo.ingresos_costos_gst_holidays as "hol"
	where
			"hol".period in 
							(
							  substring( convert(nvarchar(MAX), dateadd(mm,-2,cast(current_timestamp as date)), 112) , 1, 6 ) -- current month minus 2
							 ,substring( convert(nvarchar(MAX), dateadd(mm,-1,cast(current_timestamp as date)), 112) , 1, 6 ) -- current month minus 1
							 ,substring( convert(nvarchar(MAX), current_timestamp, 112) , 1, 6 ) 							 -- current month
							)
							

--- ======================  Set querys for Ingreso =============================== ---

		
--- This is the latest for ingresos
		
use sistemas
IF OBJECT_ID ('projections_view_ingresos_projection_rpts', 'V') IS NOT NULL		
    DROP VIEW projections_view_ingresos_projection_rpts;
create view projections_view_ingresos_projection_rpts
with encryption
as		
	select 
			"ind".id
			,"ind".company
			,"ind".id_area
			,"ind".area
			,"ind".id_fraccion
			,"ind".fraccion
			,"ind".cyear
			,"ind".mes
--			,"mnt".month_num
--			,right('00'+convert(varchar(2),"mnt".month_num), 2) as "per"
			,cast("ind".cyear as varchar) + right('00'+convert(varchar(2),"mnt".month_num), 2) as "period"
			,"ind".kms
			,"ind".subtotal
			,"ind".peso
			,"ind".dsubtotal
			,"ind".dpeso
			,"ind".subsubtotal
			,"ind".subpeso
			,"ind".non_zero
	from 
		sistemas.dbo.projections_view_indicators_periods as "ind"
	inner join 
		sistemas.dbo.generals_month_translations as "mnt" 
	on
		"ind".mes collate SQL_Latin1_General_CP1_CI_AS = "mnt".month_name
	where 
		cast("ind".cyear as varchar) + right('00'+convert(varchar(2),"mnt".month_num), 2) in
		(
		  substring( convert(nvarchar(MAX), dateadd(mm,-2,cast(current_timestamp as date)), 112) , 1, 6 ) -- current month minus 2
		 ,substring( convert(nvarchar(MAX), dateadd(mm,-1,cast(current_timestamp as date)), 112) , 1, 6 ) -- current month minus 1
		 ,substring( convert(nvarchar(MAX), current_timestamp, 112) , 1, 6 ) 							 -- current month
		)				

-- === Trimestre 
use sistemas
IF OBJECT_ID ('projections_view_trimestral_period_rpts', 'V') IS NOT NULL		
    DROP VIEW projections_view_trimestral_period_rpts;
create view projections_view_trimestral_period_rpts
with encryption
as	
with "periods" as (
		select substring( convert(nvarchar(MAX), dateadd(mm,-2,cast(current_timestamp as date)), 112) , 1, 6 ) as 'period' -- current month minus 2
	union all			
		select substring( convert(nvarchar(MAX), dateadd(mm,-1,cast(current_timestamp as date)), 112) , 1, 6 ) as 'period'-- current month minus 1
	union all		
		select substring( convert(nvarchar(MAX), current_timestamp, 112) , 1, 6 ) 							 as 'period'-- current month
)
select period from "periods" 
			
-- - =========================================   ========================================== - --
-- KMS -- small footprint 
-- select * from sistemas.dbo.projections_view_kmssmall_projection_rpts

use sistemas
IF OBJECT_ID ('projections_view_kmssmall_projection_rpts', 'V') IS NOT NULL		
    DROP VIEW projections_view_kmssmall_projection_rpts;
create view projections_view_kmssmall_projection_rpts
with encryption
as	
select 
		 id
		,period
		,ini
		,"end"
		,sum(kms_real) as 'kms_real'
		,area
from 
		sistemas.dbo.projections_view_kms_projection_rpts
group by 
		 id
		,period
		,ini
		,"end"
		,area
-- - =========================================   ========================================== - --



--		build a tables in kms 

insert into sistemas.dbo.projections_tbl_costos_projection_rpts -- elapsed time 2min 1s
 select 
--		 id
		  _source_company,area,UnidadNegocio,mes,account,name
		 ,"Real",Presupuesto,_period,Descripcion,NombreEntidad
		 ,TipoTransaccion,Referencia,ReferenciaExterna
		 ,"type",cyear,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1
 from sistemas.dbo.projections_view_costos_projection_rpts

--	example
	use sistemas
	IF OBJECT_ID('projections_tbl_costos_projection_rpts', 'U') IS NOT NULL 
	  DROP TABLE projections_tbl_costos_projection_rpts; 
	
	set ansi_nulls on
	set quoted_identifier on
	set ansi_padding on
	
	create table [projections_tbl_costos_projection_rpts](
			 id						int identity(1,1)
			,"_source_company"		char(12)
			,area					char(12)
			,UnidadNegocio			char(12)
			,mes					char(12)
			,account				char(10)
			,name					char(55)
			,"Real"					decimal(18,2)
			,Presupuesto			decimal(18,2)
			,"_period"				int
			,Descripcion			char(100)
			,NombreEntidad			char(100)
			,TipoTransaccion		char(100)
			,Referencia				char(100)
			,ReferenciaExterna		char(100)
			,"type"					char(2)
			,cyear					int
			,created				datetime
			,modified				datetime
			,user_id				int null
			,"status"				tinyint default 1 null
	) on [primary]
	
	set ansi_padding off
	
		
-- Costos -- small footprint

-- KMS small footprint
insert into sistemas.dbo.projections_tbl_kmssmall_projection_rpts
	select 
--			 id
			 period
			,ini
			,"end"
			,kms_real
			,area
			,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1
	from
			projections_view_kmssmall_projection_rpts
		
--truncate table sistemas.dbo.projections_tbl_kmssmall_projection_rpts
			
			
	use sistemas
	if OBJECT_ID('projections_tbl_kmssmall_projection_rpts', 'U') IS NOT NULL 
	  DROP TABLE projections_tbl_kmssmall_projection_rpts; 
	
	set ansi_nulls on
	set quoted_identifier on
	set ansi_padding on
	
	create table [projections_tbl_kmssmall_projection_rpts](
			 id						int identity(1,1)
			,period					int
			,ini					date
			,"end"					date
			,kms_real				int
			,area					char(25)
			,created				datetime
			,modified				datetime
			,user_id				int null
			,"status"				tinyint default 1 null
	) on [primary]
	
	set ansi_padding off



--  Costos
--	select * from sistemas.dbo.projections_tbl_costos_projection_rpts
--	kms
--	select * from sistemas.dbo.projections_view_kmssmall_projection_rpts

-- =========================================================================================== --
-- Get the average by period
-- =========================================================================================== --	
use sistemas
IF OBJECT_ID ('projections_view_costos_kms_projection_rpts', 'V') IS NOT NULL		
    DROP VIEW projections_view_costos_kms_projection_rpts;
create view projections_view_costos_kms_projection_rpts
with encryption
as	
with "proj" as (
select 
		 "costos".area
		,"costos".account
		,"costos".name
		,sum("costos"."Real") as 'Real'
		,sum("costos".Presupuesto) as 'Presupuesto'
		,"kms".kms_real
		,"costos"."_period"
		,"costos"."type"
from 
		sistemas.dbo.projections_tbl_costos_projection_rpts as "costos"
left join 
		sistemas.dbo.projections_tbl_kmssmall_projection_rpts as "kms" 
	on 
		"costos".[_period] = "kms".period and "costos".area collate SQL_Latin1_General_CP1_CI_AS = "kms".area
group by 
		 "costos".area
		,"costos".account
		,"costos".name
		,"kms".kms_real
		,"costos"."_period"
		,"costos"."type"
)
select 
	"pr".area
	,"pr".account
	,"pr".name
	,"pr"."Real"
	,"pr".Presupuesto
	,"pr".kms_real
	,"pr"."Real"/"pr".kms_real as 'costo/kms'
	,"pr"."_period"
	,"pr"."type"
from "proj" as "pr"		
		

-- WORKING STATE

select 
--		first adquire
		  coalesce("rpt-one".area,"rpt-two".area,"rpt-three".area) as 'area'
		 ,coalesce("rpt-one".account,"rpt-two".account,"rpt-three".account) as 'account'
		 ,coalesce("rpt-one".name,"rpt-two".name,"rpt-three".name) as 'name'
		 ,"rpt-one"."Real"
		 ,"rpt-one".Presupuesto
		 ,"rpt-one".kms_real
		 ,"rpt-one"."costo/kms"
		 ,"rpt-one"."_period"
--		 ,"rpt-two".area as 'area2'
--		 ,"rpt-two".account as 'account2'
--		 ,"rpt-two".name as 'name2'
		 ,"rpt-two"."Real" as 'real2'
		 ,"rpt-two".Presupuesto as 'prep2'
		 ,"rpt-two".kms_real as 'kmsreal2'
		 ,"rpt-two"."costo/kms" as 'costo/kms2'
		 ,"rpt-two"."_period" as 'perio2'
--		 ,"rpt-two"."type" as 'type2'
--		 ,"rpt-three".area as 'area3'
--		 ,"rpt-three".account as 'account3'
--		 ,"rpt-three".name as 'name3'
		 ,"rpt-three"."Real" as 'real3'
		 ,"rpt-three".Presupuesto as 'prep3'
		 ,"rpt-three".kms_real as 'kmsreal3'
		 ,"rpt-three"."costo/kms" as 'costo/kms3'
		 ,"rpt-three"."_period" as 'perio3'
--		 ,"rpt-three"."type" as 'type3'
		 ,("rpt-one"."costo/kms"+"rpt-two"."costo/kms")/2 as 'costos/mks-3'
		 ,coalesce("rpt-one"."type","rpt-two"."type","rpt-three"."type") as 'type'
from 
		sistemas.dbo.projections_view_costos_kms_projection_rpts as "rpt-one"
full join 
		sistemas.dbo.projections_view_costos_kms_projection_rpts as "rpt-two"
	on "rpt-one".area = "rpt-two".area and "rpt-one".account = "rpt-two".account and "rpt-one".[type] = "rpt-two".[type]
	and "rpt-two".[_period] = substring( convert(nvarchar(MAX), dateadd(mm,-1,cast(current_timestamp as date)), 112) , 1, 6 )
full join 
		sistemas.dbo.projections_view_costos_kms_projection_rpts as "rpt-three"
	on "rpt-one".area = "rpt-three".area and "rpt-one".account = "rpt-three".account and "rpt-one".[type] = "rpt-three".[type]
	and "rpt-three".[_period] = substring( convert(nvarchar(MAX), current_timestamp, 112) , 1, 6 )
where 
	"rpt-one".[_period] = substring( convert(nvarchar(MAX), dateadd(mm,-2,cast(current_timestamp as date)), 112) , 1, 6 )
group by 
		 "rpt-one".area,"rpt-one".account,"rpt-one".name,"rpt-one"."Real","rpt-one".Presupuesto,"rpt-one".kms_real
		 ,"rpt-one"."costo/kms","rpt-one"."_period","rpt-one"."type"
		,"rpt-two".area,"rpt-two".account,"rpt-two".name,"rpt-two"."Real","rpt-two".Presupuesto,"rpt-two".kms_real
		,"rpt-two"."costo/kms","rpt-two"."_period","rpt-two"."type"		
		,"rpt-three".area,"rpt-three".account,"rpt-three".name,"rpt-three"."Real","rpt-three".Presupuesto,"rpt-three".kms_real
		,"rpt-three"."costo/kms","rpt-three"."_period","rpt-three"."type"		

		

		
--				case casetas
				select * from bonampakdb.dbo.trafico_liquidacion as "liq" 
				inner join 
					bonampakdb.dbo.trafico_renglon_liquidacion as "ren" on "liq".no_liquidacion = "ren".no_liquidacion and "liq".id_area = "ren".id_area
				where "liq".no_liquidacion = 5175 and "liq".id_area = 4

				select * from bonampakdb.dbo.trafico_ruta

				select * from bonampakdb.dbo.trafico_ruta_caseta as "rtc"
				inner join bonampakdb.dbo.trafico_caseta as "cas" on "rtc".id_caseta = "cas".id_caseta
				 where id_ruta = 52
				
				 
				select * from bonampakdb.dbo.trafico_liquidacion_casetas where no_liquidacion = 5175 and id_area = 4
				
				select * from bonampakdb.dbo.trafico_caseta
				
--				case casetas
				
--something about periods 
		
-- == Promedias Periods == --
select 
		"per".period
from 
		sistemas.dbo.projections_view_trimestral_period_rpts as "per" 
where
		period not in (
						select max(period) from sistemas.dbo.projections_view_trimestral_period_rpts
					  )

--select 
--	"rpt-one".area
--	,"rpt-one".account
--	,"rpt-one".name
--	,"rpt-one"."Real"
--	,"rpt-one".Presupuesto
--	,"rpt-one".kms_real
--	,"rpt-one"."costo/kms","rpt-one"."_period","rpt-one"."type"
----	,"rpt-two".
--from 
--	sistemas.dbo.projections_view_costos_kms_projection_rpts as "rpt-one"
--full join 
--	sistemas.dbo.projections_view_costos_kms_projection_rpts as "rpt-two"
--on
--		
--	

with "periods" as (
	select 
		 substring( convert(nvarchar(MAX), dateadd(mm,-2,cast(current_timestamp as date)), 112) , 1, 6 ) as 'perioda' -- current month minus 2
 		,substring( convert(nvarchar(MAX), dateadd(mm,-1,cast(current_timestamp as date)), 112) , 1, 6 ) as 'periodb'-- current month minus 1
		,substring( convert(nvarchar(MAX), current_timestamp, 112) , 1, 6 ) 							 as 'periodc'-- current month
)
select * from "periods" 







select * from sistemas.dbo.projections_view_fractions



select * from 
-- update 
sistemas.dbo.projections_dissmiss_cancelations
-- set id_fraccion = 7
where id_area = 1 and company = 1 and Cporte in ('OO-048452','OO-048522','OO-048418','OO-048622')

select * from sistemas.dbo.projections_upt_cancelations where num_guia in ('OO-052763','OO-052963','OO-052966','OO-052967')

select * from sistemas.dbo.projections_view_canceled_periods where Cporte in ('OO-048452','OO-048522','OO-048418','OO-048622')
	
--	carta porte cancelada en agosto status_guia = C => Confirmada o transferida
--C=> Confirmada o Transferidas
--B=> Cancelada
 	
--Tipo de documento 
--1=> Factura o Guia
--2=> Carta Porte

	select * from bonampakdb.dbo.trafico_guia where num_guia in ('OO-048452','OO-048522','OO-048418','OO-048622')
--			and	status_guia = 'B' 
--			and	tipo_doc = 2				
--			and	prestamo = 'N'
	
	select * from sistemas.dbo.projections_view_full_company_indicators where num_guia in ('OO-048452','OO-048522','OO-048418','OO-048622')
	
	select * from sistemas.dbo.projections_upt_cancelations where num_guia in ('OO-048452','OO-048522','OO-048418','OO-048622')
	
	select * from integraapp.dbo.zpoling where CPorte in ('OO-048452','OO-048522','OO-048418','OO-048622')
	
	

--	and Estatus = 1
--	and Tmov = 'C'	
--		dinamic view
		select * from sistemas.dbo.projections_view_canceled_periods where year(fecha_cancelacion) = '2018' and month(fecha_cancelacion) = '08'
		and Area = 'Orizaba'
		
--		table 
		select * from sistemas.dbo.projections_dissmiss_cancelations where year(fecha_cancelacion) = '2018' and month(fecha_cancelacion) = '08'
		and Area = 'Orizaba'

		
		
		select * from sistemas.dbo.projections_view_canceled_tbk_periods where year(fecha_cancelacion) = '2018' and month(fecha_cancelacion) = '08'
		and Area = 'Orizaba'

		
		select * from sistemas.dbo.projections_view_indicators_periods where cyear = '2018' and area = 'ORIZABA' and fraccion <> 'GRANEL' and mes = 'Agosto'
		
		
		select * from sistemas.dbo.projections_view_indicators_periods_fleets where area = 'ORIZABA' and mes = 'Agosto'
		
		
		select * from sistemas.dbo.projections_view_indicators_periods_fleets_parts where area = 'ORIZABA' and mes = 'Agosto'

		
		
		

		
		
		
		
		
		
		
		
		
		
select sum(subtotal) from sistemas.dbo.projections_view_full_company_mod_indicators
where area = 'ORIZABA' and year(fecha_guia) = '2018' and month(fecha_guia) = '07'
and fraccion = 'GRANEL'

	
select * from sistemas.dbo.projections_view_indicators_periods_fleets
where area = 'ORIZABA' and fraccion = 'GRANEL' and mes = 'JULIO' and cyear = '2018'

select * from sistemas.dbo.projections_view_indicators_dispatch_periods_full_ops
where area = 'ORIZABA' and fraccion = 'GRANEL' and mes = 'JULIO' and cyear = '2018'



select sum(subtotal) from sistemas.dbo.projections_view_full_company_mod_indicators
where area = 'ORIZABA' and year(f_despachado) = '2018' and month(f_despachado) = '07'	
and fraccion = 'GRANEL'


use sistemas		
select * from sistemas.dbo.projections_view_full_indicators_tbk_mod_periods 




		
select substring( convert(nvarchar(MAX), current_timestamp, 112) , 1, 6 )
 		


-- ====================== Closed Periods ================================== --
				
	select * from sistemas.dbo.monitor_closed_periods

--example close all tbk :: exec sistemas.dbo.sp_build_xd3e_getFullCompanyOperations '1','0','1','0','0';
--example close all atm :: exec sistemas.dbo.sp_build_xd3e_getFullCompanyOperations '2','0','1','0','0';
--example close all tei :: exec sistemas.dbo.sp_build_xd3e_getFullCompanyOperations '3','0','1','0','0';

	select * from bonampakdb.dbo.cont_cierre order by cierre_periodo
	select * from macuspanadb.dbo.cont_cierre order by cierre_periodo
	select * from tespecializadadb.dbo.cont_cierre order by cierre_periodo

-- ====================== Closed Periods ================================== --


select * from sistemas.dbo.projections_view_canceled_periods


		