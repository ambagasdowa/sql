-- ============================================================================================================ --
-- NOTE about Projections App
-- ============================================================================================================ --
-- How to dive into projections
-- first check if viaje or cp exists in querys
-- Principal source =>sistemas.dbo.projections_view_full_gst_core_indicators
-- after check in projections_view_full_gst_testcore_indicators

-- then for xls gui check next -->
--  sistemas.dbo.operations_view_full_gst_indicators for accepted
--  sistemas.dbo.operations_view_full_gst_dispatched_indicators as dispatch


declare  @noviaje as int 			-- set no_viaje
		,@numguia as varchar(50)	-- set CP

set @noviaje = 114403
set @numguia = 'OR-122485'
--use sistemas

select
		 'source' as 'qry',no_viaje,num_guia,status_guia ,prestamo
		,[fecha-guia] ,f_despachado
		,id_area
		,FlagIsDisminution,FlagIsProvision,FlagIsNextMonth
from
		sistemas.dbo.projections_view_full_gst_core_indicators where no_viaje = @noviaje or num_guia = @numguia
	union all
select
		'whit_bussiness_rules' as 'qry',no_viaje,num_guia,status_guia ,prestamo
		,[fecha-guia] ,f_despachado
		,id_area
		,FlagIsDisminution,FlagIsProvision,FlagIsNextMonth
from
		sistemas.dbo.projections_view_full_gst_testcore_indicators where no_viaje = @noviaje or num_guia = @numguia

-- ============================================================================================================ --
-- IMPORTANT The XLS querys retrieve data up to yesterday
-- ============================================================================================================ --

select
		'accepted_xls' as 'qry',no_viaje,num_guia,status_guia ,prestamo
		,[fecha-guia] ,f_despachado
		,id_area
		,FlagIsDisminution,FlagIsProvision,FlagIsNextMonth
from
		sistemas.dbo.operations_view_full_gst_indicators where no_viaje = @noviaje or num_guia = @numguia
union all
select
		'dispatch_xls' as 'qry',no_viaje,num_guia,status_guia ,prestamo
		,[fecha-guia] ,f_despachado
		,id_area
		,FlagIsDisminution,FlagIsProvision,FlagIsNextMonth
from
		sistemas.dbo.operations_view_full_gst_dispatched_indicators where no_viaje = @noviaje or num_guia = @numguia
