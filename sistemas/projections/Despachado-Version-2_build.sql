use sistemas
-- ===================================================================================================================== --		
--  this is the legacy ACEPTED  
-- ===================================================================================================================== --

select 
		"aceptado".id_area
		,"aceptado".id_unidad
		,"aceptado".id_configuracionviaje
		,"aceptado".id_tipo_operacion
		,"aceptado".id_fraccion
		,"aceptado".id_flota
		,"aceptado".no_viaje
		,"aceptado".num_guia
		,"aceptado".id_ruta
		,"aceptado".id_origen
		,"aceptado".desc_ruta
		,"aceptado".monto_retencion
		,"aceptado".fecha_guia
		,"aceptado".mes
		,"aceptado"."year"
		,"aceptado".dia
		,"aceptado".f_despachado
		,"aceptado".cliente
		,"aceptado".kms
		,"aceptado".subtotal
		,"aceptado".peso
		,"aceptado".configuracion_viaje
		,"aceptado".tipo_de_operacion
		,"aceptado".flota
		,"aceptado".area
		,"aceptado".fraction
		,"aceptado".fraccion
		,"aceptado".company
		,"aceptado".num_viajes
		,"aceptado".periodo
		,"aceptado".Descripcion
from 
	sistemas.dbo.operations_view_full_company_indicators


-- ===================================================================================================================== --		
--  this is the legacy DISPATCHED  
-- ===================================================================================================================== --

select 
		"despachado".id_area
		,"despachado".id_unidad
		,"despachado".id_configuracionviaje
		,"despachado".id_tipo_operacion
		,"despachado".id_fraccion
		,"despachado".id_flota
		,"despachado".no_viaje
		,"despachado".num_guia
		,"despachado".id_ruta
		,"despachado".id_origen
		,"despachado".desc_ruta
		,"despachado".monto_retencion
		,"despachado".fecha_guia
		,"despachado".mes
		,"despachado"."year"
		,"despachado".dia
		,"despachado".f_despachado
		,"despachado".cliente
		,"despachado".kms
		,"despachado".subtotal
		,"despachado".peso
		,"despachado".configuracion_viaje
		,"despachado".tipo_de_operacion
		,"despachado".flota
		,"despachado".area
		,"despachado".fraction
		,"despachado".fraccion
		,"despachado".company
		,"despachado".num_viajes
		,"despachado".periodo
		,"despachado".datelink
		,"despachado".Descripcion
from 
		sistemas.dbo.operations_view_full_company_dispatched_indicators as "despachado"

-- ===================================================================================================================== --		
--the new core with accepted and desp until 2018  
-- ===================================================================================================================== --
select * from projections_view_full_company_core_indicators


-- ===================================================================================================================== --		
-- aceptado
-- Origin New-Core [2019]  
-- ===================================================================================================================== --
--select fecha_guia,[fecha-guia],* from sistemas.dbo.operations_view_full_gst_indicators

use sistemas

IF OBJECT_ID ('operations_view_full_gst_indicators', 'V') IS NOT NULL
    DROP VIEW operations_view_full_gst_indicators;
-- now build the view

alter view operations_view_full_gst_indicators
with encryption
as -- 
select 
		 1 as 'company'
		,"aceptado".id_area						-- 1
		,"aceptado".no_viaje					-- 7
		,"aceptado".id_area_liq
		,"aceptado".id_personal
		,"aceptado".no_liquidacion
		,"aceptado".fecha_liquidacion
		,"aceptado".id_ruta
		,"aceptado".no_ejes_viaje
		,"aceptado".id_origen
		,"aceptado".id_destino
		,"aceptado"."fecha-real-viaje"
		,"aceptado"."fecha-real-fin-viaje"
		,"aceptado"."fecha-despachado"
		,"aceptado".fecha_real_viaje
		,"aceptado".fecha_real_fin_viaje
		,"aceptado".f_despachado
		,"aceptado".lts_empresa
		,"aceptado".ton_viaje
		,"aceptado".id_unidad					-- 2
		,"aceptado".id_remolque1
		,"aceptado".id_remolque2
		,"aceptado".id_dolly
		,"aceptado".id_ingreso
		,"aceptado".status_viaje
		,"aceptado".viajeactual
		,"aceptado".no_tarjeta_llave
		,"aceptado".id_configuracionviaje		-- 3
		,"aceptado".id_areaviaje
		,"aceptado".fecha_real_viaje_m
		,"aceptado".fecha_real_fin_viaje_m
		,"aceptado".f_despachado_m
		,"aceptado"."day"
		,"aceptado"."mes-despacho"
		,"aceptado".periodo_aceptado as 'periodo'
		,"aceptado".periodo_despachado
		,"aceptado".no_guia
		,"aceptado".tipo_pago
		,"aceptado".status_guia
		,"aceptado".clasificacion_doc
		,"aceptado"."fecha-guia"
		,"aceptado"."fecha-confirmacion"
		,"aceptado"."fecha-ingreso"
		,"aceptado".fecha_modifico
		,"aceptado"."fecha-cancelacion"
		,"aceptado"."fecha-contabilizado"
		,"aceptado".fecha_contabilizado_m
		,"aceptado".fecha_confirmacion_m
		,"aceptado".fecha_guia_m
		,"aceptado".fecha_cancelacion_m
		,"aceptado".fecha_contabilizado
		,"aceptado".fecha_confirmacion
		,"aceptado".fecha_guia
		,"aceptado".fecha_cancelacion
		,"aceptado".gday as 'dia' -- when is accepted
		,"aceptado".convenido_tonelada
		,"aceptado".id_area_facturacion
		,"aceptado".id_cliente
		,"aceptado".gid_personal
		,"aceptado".id_remitente
		,"aceptado".Plaza
		,"aceptado".Zona
		,"aceptado".id_serieguia
		,"aceptado".id_plaza
		,"aceptado".num_serie
		,"aceptado".area_solomon
		,"aceptado".desc_plaza
		,"aceptado".nombre
		,"aceptado".nombrecorto
		,"aceptado".ciudad
		,"aceptado".id_destinatario
		,"aceptado".kms_guia
		,"aceptado".flete
		,"aceptado".seguro
		,"aceptado".maniobras
		,"aceptado".autopistas
		,"aceptado".otros
		,"aceptado".iva_guia
		,"aceptado".cobro_viaje_kms
		,"aceptado".tipo_doc
		,"aceptado".gid_origen
		,"aceptado".gid_destino
		,"aceptado".id_tipo_operacion as 'id_fraccion'						--5
--		,"aceptado".id_fraccion
		,"aceptado".fraccion
		,"aceptado".projections_rp_definition
		,"aceptado".plaza_emision
		,"aceptado".num_guia
		,"aceptado".no_carta
		,"aceptado".gid_remolque1
		,"aceptado".gid_unidad
		,"aceptado".no_remision
		,"aceptado".motivo_cancelacion
		,"aceptado".gid_remolque2
		,"aceptado".prestamo
		,"aceptado".num_guia_asignado
		,"aceptado".no_deposito
		,"aceptado".gid_ingreso
		,"aceptado".personalnombre
		,"aceptado".tipo_facturacion
		,"aceptado".no_transferencia_cobranza
		,"aceptado".factor_iva
		,"aceptado".num_guiacancel
		,"aceptado".tipo_origen
		,"aceptado".no_poliza
		,"aceptado".monto_retencion
		,"aceptado".status_pago
		,"aceptado".monto_retenciontercero
		,"aceptado".sustituye_documento
		,"aceptado".monto_ivaflete
		,"aceptado".tipocambioconvenio
		,"aceptado".desc_flete
		,"aceptado".flete_bruto
		,"aceptado".id_iva
		,"aceptado".id_retencion
		,"aceptado".id_convenio
		,"aceptado".id_areaconvenio
		,"aceptado".id_tipo_operacion				-- 4
		,"aceptado".no_kit
		,"aceptado".monto_ivadescto
		,"aceptado".monto_retdescto
		,"aceptado".monto_descto
		,"aceptado".periodo_facturacion
		,"aceptado".tipo_producto
		,"aceptado".num_guia_incentivo
		,"aceptado".facturado
		,"aceptado".id_modifico
		,"aceptado".kms_convenio
		,"aceptado".observacion
--		,"aceptado".mes as 'orig_month'
		,"month".month_name as 'mes'
		,"aceptado".desc_ruta
		,"aceptado".cliente
		,"aceptado".id_flota						-- 6
		,"aceptado".configuracion_viaje
		,"aceptado".tipo_de_operacion
		,'' as 'flota' --"aceptado".flota
		,"aceptado".area
		,"aceptado".fraction
		,"aceptado".FlagIsDisminution
		,"aceptado".FlagIsProvision
		,"aceptado".kms_viaje
		,"aceptado".kms_real
		,"aceptado".kms
		,"aceptado".subtotal
		,"aceptado"."peso-aceptado" as 'peso'
		,"aceptado"."peso-despachado"
		,"aceptado".trip_count as 'num_viajes'
		,"aceptado".FlagIsNextMonth
--		,year("aceptado"."fecha-guia") as 'year'
		,cast(left("aceptado".fecha_guia_m,4) as int) as 'year'
		,'' as "Descripcion"
		,"aceptado".fecha_guia_m as 'fecha'
from 
		sistemas.dbo.projections_view_full_gst_testcore_indicators "aceptado"
inner join 
		sistemas.dbo.generals_month_translations as "month"
	on
		"month".month_num = cast(right("aceptado".fecha_guia_m,2) as int)
where 
	(
--			 "aceptado".FlagIsDisminution = 0
		 	 "aceptado".FlagIsDisminution <> 2
		and 
			 "aceptado".flagIsProvision = 0 --Prestamo
	--	and 
	--		 "aceptado".FlagIsNextMonth = 0 --negativo
	--		 "aceptado".FlagIsNextMonth = 0 --negativo
	--	and 
	--		 "core".prestamo
		and 
			 "aceptado".tipo_doc = 2
--	    and 
--	    	"aceptado".status_guia <> 'B'
	    and 
	    	"aceptado".fecha_guia <= dateadd(day,-1,cast(current_timestamp as date))
    )
  OR 
    (
	   	 	"aceptado".FlagIsNextMonth = 1
--		and 
--			"aceptado".FlagIsDisminution <> 2
--		and 
--			"aceptado".FlagIsProvision = 0
--		and 
--		 	"aceptado".tipo_doc = 2
		and 
    		"aceptado".fecha_guia <= dateadd(day,-1,cast(current_timestamp as date))
    )
   
   
    	
    	
-- ===================================================================================================================== --		
-- despachado
-- Origin Core [2019] operations_view_full_company_dispatched_indicators  
-- ===================================================================================================================== --
-- @Test select id_tipo_operacion,tipo_de_operacion,id_cliente,cliente,* from sistemas.dbo.operations_view_full_gst_dispatched_indicators where num_guia = 'CU-176509'
--	select id_tipo_operacion,id_cliente,prestamo,* from gstdb.dbo.trafico_guia where num_guia = 'CU-176509' year
use sistemas
IF OBJECT_ID ('operations_view_full_gst_dispatched_indicators', 'V') IS NOT NULL
    DROP VIEW operations_view_full_gst_dispatched_indicators;
-- now build the view
alter view operations_view_full_gst_dispatched_indicators
with encryption
as -- 
select 
		 1 as 'company'
		,"despachado".id_area						-- 1
		,"despachado".no_viaje					-- 7
		,"despachado".id_area_liq
		,"despachado".id_personal
		,"despachado".no_liquidacion
		,"despachado".fecha_liquidacion
		,"despachado".id_ruta
		,"despachado".no_ejes_viaje
		,"despachado".id_origen
		,"despachado".id_destino
		,"despachado"."fecha-real-viaje"
		,"despachado"."fecha-real-fin-viaje"
		,"despachado"."fecha-despachado"
		,"despachado".fecha_real_viaje
		,"despachado".fecha_real_fin_viaje
		,"despachado".f_despachado
		,"despachado".lts_empresa
		,"despachado".ton_viaje
		,"despachado".id_unidad					-- 2
		,"despachado".id_remolque1
		,"despachado".id_remolque2
		,"despachado".id_dolly
		,"despachado".id_ingreso
		,"despachado".status_viaje
		,"despachado".viajeactual
		,"despachado".no_tarjeta_llave
		,"despachado".id_configuracionviaje		-- 3
		,"despachado".id_areaviaje
		,"despachado".fecha_real_viaje_m
		,"despachado".fecha_real_fin_viaje_m
		,"despachado".f_despachado_m
		,"despachado"."day" as 'dia'
		,"despachado"."mes-despacho" -- as 'mes'
		,"despachado".periodo_aceptado
		,"despachado".periodo_despachado as 'periodo'
		,"despachado".no_guia
		,"despachado".tipo_pago
		,"despachado".status_guia
		,"despachado".clasificacion_doc
		,"despachado"."fecha-guia"
		,"despachado"."fecha-confirmacion"
		,"despachado"."fecha-ingreso"
		,"despachado".fecha_modifico
		,"despachado"."fecha-cancelacion"
		,"despachado"."fecha-contabilizado"
		,"despachado".fecha_contabilizado_m
		,"despachado".fecha_confirmacion_m
		,"despachado".fecha_guia_m
		,"despachado".fecha_cancelacion_m
		,"despachado".fecha_contabilizado
		,"despachado".fecha_confirmacion
		,"despachado".fecha_guia
		,"despachado".fecha_cancelacion
		,"despachado".gday
		,"despachado".convenido_tonelada
		,"despachado".id_area_facturacion
		,"despachado".id_cliente
		,"despachado".gid_personal
		,"despachado".id_remitente
		,"despachado".Plaza
		,"despachado".Zona
		,"despachado".id_serieguia
		,"despachado".id_plaza
		,"despachado".num_serie
		,"despachado".area_solomon
		,"despachado".desc_plaza
		,"despachado".nombre
		,"despachado".nombrecorto
		,"despachado".ciudad
		,"despachado".id_destinatario
		,"despachado".kms_guia
		,"despachado".flete
		,"despachado".seguro
		,"despachado".maniobras
		,"despachado".autopistas
		,"despachado".otros
		,"despachado".iva_guia
		,"despachado".cobro_viaje_kms
		,"despachado".tipo_doc
		,"despachado".gid_origen
		,"despachado".gid_destino
		,"despachado".id_tipo_operacion as 'id_fraccion'						--5
--		,"despachado".id_fraccion
		,"despachado".fraccion
		,"despachado".projections_rp_definition
		,"despachado".plaza_emision
		,"despachado".num_guia
		,"despachado".no_carta
		,"despachado".gid_remolque1
		,"despachado".gid_unidad
		,"despachado".no_remision
		,"despachado".motivo_cancelacion
		,"despachado".gid_remolque2
		,"despachado".prestamo
		,"despachado".num_guia_asignado
		,"despachado".no_deposito
		,"despachado".gid_ingreso
		,"despachado".personalnombre
		,"despachado".tipo_facturacion
		,"despachado".no_transferencia_cobranza
		,"despachado".factor_iva
		,"despachado".num_guiacancel
		,"despachado".tipo_origen
		,"despachado".no_poliza
		,"despachado".monto_retencion
		,"despachado".status_pago
		,"despachado".monto_retenciontercero
		,"despachado".sustituye_documento
		,"despachado".monto_ivaflete
		,"despachado".tipocambioconvenio
		,"despachado".desc_flete
		,"despachado".flete_bruto
		,"despachado".id_iva
		,"despachado".id_retencion
		,"despachado".id_convenio
		,"despachado".id_areaconvenio
		,"despachado".id_tipo_operacion				-- 4
		,"despachado".no_kit
		,"despachado".monto_ivadescto
		,"despachado".monto_retdescto
		,"despachado".monto_descto
		,"despachado".periodo_facturacion
		,"despachado".tipo_producto
		,"despachado".num_guia_incentivo
		,"despachado".facturado
		,"despachado".id_modifico
		,"despachado".kms_convenio
		,"despachado".observacion
--		,"despachado".[mes-despacho] -- old mes
		,"month".month_name as 'mes'
		,"despachado".desc_ruta
		,"despachado".cliente
		,"despachado".id_flota						-- 6
		,"despachado".configuracion_viaje
		,"despachado".tipo_de_operacion
		,'' as 'flota' -- "despachado".flota
		,"despachado".area
		,"despachado".fraction
		,"despachado".FlagIsDisminution
		,"despachado".FlagIsProvision
		,"despachado".kms_viaje
		,"despachado".kms_real
		,"despachado".kms
		,"despachado".subtotal
		,"despachado"."peso-aceptado"
		,"despachado"."peso-despachado" as 'peso'
		,"despachado".trip_count as 'num_viajes'
		,"despachado".FlagIsNextMonth
--		,year("despachado"."f_despachado") as 'year'
		,cast(left("despachado".f_despachado_m,4) as int) as 'year'
		,case 
			when 
					"despachado".FlagIsProvision = 1	
				then 
					'Provision'
			when 
					"despachado".FlagIsProvision = 0
				then 
					'Aceptado'
--			when 	"despachado".no_viaje is null and "despachado".no_guia is not null 
--				then
			else 
					''
		end as "Descripcion"
		,"despachado".f_despachado as 'fecha'
from 
		sistemas.dbo.projections_view_full_gst_testcore_indicators "despachado"
inner join 
		sistemas.dbo.generals_month_translations as "month"
	on
		"month".month_num = cast(right("despachado".f_despachado_m,2) as int)
where 
--	(
		 "despachado".FlagIsDisminution = 0
--		 	 "despachado".FlagIsDisminution <> 2  -- set 0 = aceptada , 1 = cancelada en mes diferente
	--	and 
	--		 "core".flagIsProvision = 0 and 1 --Prestamo
	--	and 
	--		 "despachado".FlagIsNextMonth = 0
	--	and 
	--		 "core".prestamo
		and 
			 "despachado".tipo_doc = 2  -- 1 Factura , 2 CartaPorte
--	    and 
--	    	"despachado".status_guia <> 'B' -- B = Baja
	    and 
	    	"despachado".f_despachado <= dateadd(day,-1,cast(current_timestamp as date))
--	)
--  OR 
--    (
--	   	 	"despachado".FlagIsNextMonth = 1
----		and 
----			"despachado".FlagIsDisminution <> 2
----		and 
----			"despachado".FlagIsProvision = 0
----		and 
----		 	"despachado".tipo_doc = 2
--		and 
--    		"despachado".f_despachado <= dateadd(day,-1,cast(current_timestamp as date))
--    )

    
    
--    ===========================================================
--    	
--   	select id_fraccion,fraccion,id_tipo_operacion,tipo_de_operacion,projections_rp_definition,*
--   	from sistemas.dbo.projections_view_full_gst_testcore_indicators where num_guia in ('MA-49903','MA-49935')
--   	select id_fraccion,fraccion,id_tipo_operacion,tipo_de_operacion,projections_rp_definition,* 
--   	from sistemas.dbo.operations_view_full_gst_dispatched_indicators where num_guia in ('MA-49903','MA-49935')
--   	
--   	select sum(subtotal),fraccion,id_fraccion,tipo_de_operacion,id_tipo_operacion,Descripcion,id_area,Area,projections_rp_definition
--   	from sistemas.dbo.operations_view_full_gst_dispatched_indicators  
--   	where 
----   id_area = 7  and 
--   		Area = 'MACUSPANA'
--   		and f_despachado_m = 201901
--   group by fraccion,id_fraccion,tipo_de_operacion,id_tipo_operacion,Descripcion,id_area,Area,projections_rp_definition
--   	
--   
   
   select 
	 id_area
	,id_unidad
	,id_configuracionviaje
	,id_tipo_operacion
	,id_fraccion
	,id_flota
	,no_viaje
	,num_guia
	,id_ruta
	,id_origen
	,desc_ruta
	,monto_retencion
	,fecha_guia
	,mes
	,"year"
	,dia
	,f_despachado
	,cliente
	,kms
	,subtotal
	,peso
	,configuracion_viaje
	,tipo_de_operacion
	,flota
	,area
	,fraction
	,fraccion
	,company
	,num_viajes
    ,periodo
    ,Descripcion
from 
	"sistemas"."dbo"."operations_view_full_gst_dispatched_indicators"
where 
	id_area = 7
   
   	
   	
-- ===================================================================================================================== --		
-- [SNAPSHOT TABLE] despachado
-- Origin Core [2019] operations_view_full_company_dispatched_indicators  
-- ===================================================================================================================== --
-- @Test select id_tipo_operacion,tipo_de_operacion,id_cliente,cliente,* from sistemas.dbo.operations_view_full_gst_dispatched_indicators where num_guia = 'CU-176509'
--	select id_tipo_operacion,id_cliente,prestamo,* from gstdb.dbo.trafico_guia where num_guia = 'CU-176509'
use sistemas
IF OBJECT_ID ('operations_viewtbl_full_gst_dispatched_indicators', 'V') IS NOT NULL
    DROP VIEW operations_viewtbl_full_gst_dispatched_indicators;
-- now build the view
create view operations_viewtbl_full_gst_dispatched_indicators
with encryption
as -- 
select 
		 1 as 'company'
		,"snapshot".id_area						-- 1
		,"snapshot".no_viaje					-- 7
		,"snapshot".id_area_liq
		,"snapshot".id_personal
		,"snapshot".no_liquidacion
		,"snapshot".fecha_liquidacion
		,"snapshot".id_ruta
		,"snapshot".no_ejes_viaje
		,"snapshot".id_origen
		,"snapshot".id_destino
		,"snapshot"."fecha-real-viaje"
		,"snapshot"."fecha-real-fin-viaje"
		,"snapshot"."fecha-despachado"
		,"snapshot".fecha_real_viaje
		,"snapshot".fecha_real_fin_viaje
		,"snapshot".f_despachado
		,"snapshot".lts_empresa
		,"snapshot".ton_viaje
		,"snapshot".id_unidad					-- 2
		,"snapshot".id_remolque1
		,"snapshot".id_remolque2
		,"snapshot".id_dolly
		,"snapshot".id_ingreso
		,"snapshot".status_viaje
		,"snapshot".viajeactual
		,"snapshot".no_tarjeta_llave
		,"snapshot".id_configuracionviaje		-- 3
		,"snapshot".id_areaviaje
		,"snapshot".fecha_real_viaje_m
		,"snapshot".fecha_real_fin_viaje_m
		,"snapshot".f_despachado_m
		,"snapshot"."day" as 'dia'
		,"snapshot"."mes-despacho" -- as 'mes'
		,"snapshot".periodo_aceptado
		,"snapshot".periodo_despachado as 'periodo'
		,"snapshot".no_guia
		,"snapshot".tipo_pago
		,"snapshot".status_guia
		,"snapshot".clasificacion_doc
		,"snapshot"."fecha-guia"
		,"snapshot"."fecha-confirmacion"
		,"snapshot"."fecha-ingreso"
		,"snapshot".fecha_modifico
		,"snapshot"."fecha-cancelacion"
		,"snapshot"."fecha-contabilizado"
		,"snapshot".fecha_contabilizado_m
		,"snapshot".fecha_confirmacion_m
		,"snapshot".fecha_guia_m
		,"snapshot".fecha_cancelacion_m
		,"snapshot".fecha_contabilizado
		,"snapshot".fecha_confirmacion
		,"snapshot".fecha_guia
		,"snapshot".fecha_cancelacion
		,"snapshot".gday
		,"snapshot".convenido_tonelada
		,"snapshot".id_area_facturacion
		,"snapshot".id_cliente
		,"snapshot".gid_personal
		,"snapshot".id_remitente
		,"snapshot".Plaza
		,"snapshot".Zona
		,"snapshot".id_serieguia
		,"snapshot".id_plaza
		,"snapshot".num_serie
		,"snapshot".area_solomon
		,"snapshot".desc_plaza
		,"snapshot".nombre
		,"snapshot".nombrecorto
		,"snapshot".ciudad
		,"snapshot".id_destinatario
		,"snapshot".kms_guia
		,"snapshot".flete
		,"snapshot".seguro
		,"snapshot".maniobras
		,"snapshot".autopistas
		,"snapshot".otros
		,"snapshot".iva_guia
		,"snapshot".cobro_viaje_kms
		,"snapshot".tipo_doc
		,"snapshot".gid_origen
		,"snapshot".gid_destino
		,"snapshot".id_tipo_operacion as 'id_fraccion'						--5
--		,"snapshot".id_fraccion
		,"snapshot".fraccion
		,"snapshot".projections_rp_definition
		,"snapshot".plaza_emision
		,"snapshot".num_guia
		,"snapshot".no_carta
		,"snapshot".gid_remolque1
		,"snapshot".gid_unidad
		,"snapshot".no_remision
		,"snapshot".motivo_cancelacion
		,"snapshot".gid_remolque2
		,"snapshot".prestamo
		,"snapshot".num_guia_asignado
		,"snapshot".no_deposito
		,"snapshot".gid_ingreso
		,"snapshot".personalnombre
		,"snapshot".tipo_facturacion
		,"snapshot".no_transferencia_cobranza
		,"snapshot".factor_iva
		,"snapshot".num_guiacancel
		,"snapshot".tipo_origen
		,"snapshot".no_poliza
		,"snapshot".monto_retencion
		,"snapshot".status_pago
		,"snapshot".monto_retenciontercero
		,"snapshot".sustituye_documento
		,"snapshot".monto_ivaflete
		,"snapshot".tipocambioconvenio
		,"snapshot".desc_flete
		,"snapshot".flete_bruto
		,"snapshot".id_iva
		,"snapshot".id_retencion
		,"snapshot".id_convenio
		,"snapshot".id_areaconvenio
		,"snapshot".id_tipo_operacion				-- 4
		,"snapshot".no_kit
		,"snapshot".monto_ivadescto
		,"snapshot".monto_retdescto
		,"snapshot".monto_descto
		,"snapshot".periodo_facturacion
		,"snapshot".tipo_producto
		,"snapshot".num_guia_incentivo
		,"snapshot".facturado
		,"snapshot".id_modifico
		,"snapshot".kms_convenio
		,"snapshot".observacion
		,"snapshot"."mes-despacho" as 'mes'
		,"snapshot".desc_ruta
		,"snapshot".cliente
		,"snapshot".id_flota						-- 6
		,"snapshot".configuracion_viaje
		,"snapshot".tipo_de_operacion
		,'' as 'flota' -- "snapshot".flota
		,"snapshot".area
		,"snapshot".fraction
		,"snapshot".FlagIsDisminution
		,"snapshot".FlagIsProvision
		,"snapshot".kms_viaje
		,"snapshot".kms_real
		,"snapshot".kms
		,"snapshot".subtotal
		,"snapshot"."peso-aceptado"
		,"snapshot"."peso-despachado" as 'peso'
		,"snapshot".trip_count as 'num_viajes'
		,"snapshot".FlagIsNextMonth
		,year("snapshot"."f_despachado") as 'year'
		,case 
			when 
					"snapshot".FlagIsProvision = 1	
				then 
					'Provision'
			when 
					"snapshot".FlagIsProvision = 0
				then 
					'Aceptado'
--			when 	"snapshot".no_viaje is null and "snapshot".no_guia is not null 
--				then
			else 
					''
		end as "Descripcion"
			,"snapshot".created
			,"snapshot".modified
			,"snapshot".snapshot_period	
			,"snapshot".snapshot_ctrl_id
			,"snapshot".status	
from 
		sistemas.dbo.projections_table_full_gst_testcore_indicators "snapshot"
where 
		 "snapshot".FlagIsDisminution = 0
--	and 
--		 "core".flagIsProvision = 0 and 1 --Prestamo
	and 
		 "snapshot".FlagIsNextMonth = 0
	 and 
	 	 "snapshot".snapshot_period = "snapshot".periodo_despachado
--	and 
--		 "core".prestamo
	and 
		 "snapshot".tipo_doc = 2  -- 1 Factura , 2 CartaPorte
    and 
    	"snapshot".status_guia <> 'B' -- B = Baja
    and 
    	"snapshot".f_despachado <= dateadd(day,-1,cast(current_timestamp as date))

    	
    	
    	
    
    
    
 -- ================================================================================================ --
	
 -- ================================================================================================ --
    
 -- ================================================================================================ --   
    
-- ================================================================================================ --

    select top 100 'aceptado' as 'rpt',* from sistemas.dbo.operations_view_full_gst_indicators
    union all 
    select top 100 'despachado' as 'rpt',* from sistemas.dbo.operations_view_full_gst_dispatched_indicators
       
    
    select * from sistemas.dbo.projections_view_bussiness_units
    
    select * from sistemas.dbo.projections_view_full_gst_xls_indicators
    
    select * from sistemas.dbo.addenum_view_albaran_relations
    
    exec sp_desc addenum_view_albaran_relations
    
    exec sp_desc projections_view_full_gst_xls_indicators
    
use sistemas
IF OBJECT_ID ('projections_view_full_gst_xls_indicators', 'V') IS NOT NULL
    DROP VIEW projections_view_full_gst_xls_indicators
-- now build the view
alter view projections_view_full_gst_xls_indicators
with encryption
as --     
with "xls" as (
    select 
		 "aw".id_area
		,"aw".mes collate SQL_Latin1_General_CP1_CI_AS as 'mes'
		,"aw".id_tipo_operacion
		,"aw".projections_rp_definition collate SQL_Latin1_General_CP1_CI_AS as 'projections_rp_definition'
		,"aw".area collate SQL_Latin1_General_CP1_CI_AS as 'area'
		,"aw".periodo
		,"aw".FlagIsDisminution
		,"aw".FlagIsProvision
		,"aw".FlagIsNextMonth
		,"aw".kms
		,"aw".subtotal
		,"aw".peso
		,"aw".num_viajes as 'viajes'
		,"aw".[year]
		,'aceptado' collate SQL_Latin1_General_CP1_CI_AS as 'tpo'
		,"aw".fecha
		,"hd".id_month
		,"hd".is_current
		,"hd".labDays
		,"hd".labRestDays
		,"hd".labFullDays
		,"ppto".PresupuestoIngresos
		,"ppto".PresupuestoKms
		,"ppto".PresupuestoTon
		,"ppto".PresupuestoViajes
    from 
    		"sistemas"."dbo"."operations_view_full_gst_indicators" as "aw"   	
    left join 
   			"sistemas"."dbo"."ingresos_costos_gst_holidays" as "hd"
   		on 
   			"aw".periodo = "hd".period
   	left join 
   			"sistemas"."dbo"."ingresos_costos_view_ind_ppto_ingresos" as "ppto"
   		on 
   			"aw".id_area = "ppto".id_area and "aw".projections_rp_definition collate SQL_Latin1_General_CP1_CI_AS = "ppto".frt and "aw".periodo = "ppto".periodo
   	inner join
   		"sistemas"."dbo"."operations_year_selectors" as "yr"
   		on 
   			"aw".[year] = "yr".cyear   		
--	group by 
--		 "aw".id_area
--		,"aw".mes
--		,"aw".id_tipo_operacion
--		,"aw".projections_rp_definition
--		,"aw".area
--		,"aw".periodo
--		,"aw".FlagIsDisminution
--		,"aw".FlagIsProvision
--		,"aw".FlagIsNextMonth
--		,"aw".[year]
--		,"aw".fecha
--		,"hd".id_month
--		,"hd".is_current
--		,"hd".labDays
--		,"hd".labRestDays
--		,"hd".labFullDays
--		,"ppto".PresupuestoIngresos
--		,"ppto".PresupuestoKms
--		,"ppto".PresupuestoTon
--		,"ppto".PresupuestoViajes
   union all 
    select 
		 "dw".id_area
		,"dw".mes collate SQL_Latin1_General_CP1_CI_AS as 'mes'
		,"dw".id_tipo_operacion
		,"dw".projections_rp_definition collate SQL_Latin1_General_CP1_CI_AS as 'projections_rp_definition'
		,"dw".area collate SQL_Latin1_General_CP1_CI_AS as 'area'
		,"dw".periodo
		,"dw".FlagIsDisminution
		,"dw".FlagIsProvision
		,"dw".FlagIsNextMonth
		,"dw".kms
		,"dw".subtotal
		,"dw".peso
		,"dw".num_viajes as 'viajes'
		,"dw".[year]
		,'despachado' collate SQL_Latin1_General_CP1_CI_AS as 'tpo' --collate SQL_Latin1_General_CP1_CI_AS
		,"dw".fecha
		,"xhd".id_month
		,"xhd".is_current
		,"xhd".labDays
		,"xhd".labRestDays
		,"xhd".labFullDays
		,"xppto".PresupuestoIngresos
		,"xppto".PresupuestoKms
		,"xppto".PresupuestoTon
		,"xppto".PresupuestoViajes
    from 
    	sistemas.dbo.operations_view_full_gst_dispatched_indicators as "dw"
    left join 
   			"sistemas"."dbo"."ingresos_costos_gst_holidays" as "xhd"
   		on 
   			"dw".periodo = "xhd".period
   	left join 
   			"sistemas"."dbo"."ingresos_costos_view_ind_ppto_ingresos" as "xppto"
   		on 
   			"dw".id_area = "xppto".id_area and "dw".projections_rp_definition collate SQL_Latin1_General_CP1_CI_AS = "xppto".frt and "dw".periodo = "xppto".periodo
   	inner join
   		"sistemas"."dbo"."operations_year_selectors" as "xyr"
   		on 
   			"dw".[year] = "xyr".cyear
--	group by 
--		 "dw".id_area
--		,"dw".mes
--		,"dw".id_tipo_operacion
--		,"dw".projections_rp_definition
--		,"dw".area
--		,"dw".periodo
--		,"dw".FlagIsDisminution
--		,"dw".FlagIsProvision
--		,"dw".FlagIsNextMonth
--		,"dw".[year]
--		,"dw".fecha
--		,"xhd".id_month
--		,"xhd".is_current
--		,"xhd".labDays
--		,"xhd".labRestDays
--		,"xhd".labFullDays
--		,"xppto".PresupuestoIngresos
--		,"xppto".PresupuestoKms
--		,"xppto".PresupuestoTon
--		,"xppto".PresupuestoViajes  
	)
select 
		 row_number() over(order by "xls".id_area) as 'id'
		,"xls".id_area
		,"xls".mes
		,"xls".id_tipo_operacion
		,"xls".projections_rp_definition
		,"xls".area
		,"xls".periodo
		,"xls".FlagIsDisminution
		,"xls".FlagIsProvision
		,"xls".FlagIsNextMonth
		,"xls".kms
		,"xls".subtotal
		,"xls".peso
		,"xls".viajes
		,"xls".[year]
		,"xls".tpo 
		,"xls".fecha
		,"xls".id_month
		,"xls".is_current
		,"xls".labDays
		,"xls".labRestDays
		,"xls".labFullDays
		,"xls".PresupuestoIngresos
		,"xls".PresupuestoKms
		,"xls".PresupuestoTon
		,"xls".PresupuestoViajes	
from 
		"xls"
    
	
   -- =========================================================================================================== -- 
   -- TEST 
   -- =========================================================================================================== --
   
select * from sistemas.dbo.projections_view_full_gst_xls_indicators as "op" 
where "op".fecha between '2020-01-01' and '2020-01-02' and "op".id_area in (6) --and "op".id_tipo_operacion = 1
   
select 
	     "op".id_area
		,"op".mes
		,"op".id_tipo_operacion
		,"op".projections_rp_definition
		,"op".area
		,"op".periodo
--		,"op".FlagIsDisminution
--		,"op".FlagIsProvision
--		,"op".FlagIsNextMonth
		,sum("op".kms) as 'kms'
		,sum("op".subtotal) as 'subtotal'
		,sum("op".peso) as 'peso'
		,sum("op".viajes) as 'viajes'
		,"op".tpo
		,"op".id_month
		,"op".is_current
		,"op".labDays
		,"op".labRestDays
		,"op".labFullDays
		,"op".PresupuestoIngresos
		,"op".PresupuestoKms
		,"op".PresupuestoTon
		,"op".PresupuestoViajes
--		,"dw".fecha
from 
		sistemas.dbo.projections_view_full_gst_xls_indicators as "op" 
where 
		"op".fecha between '2020-01-01' and '2020-01-05' and "op".id_area in (1) -- and "op".id_tipo_operacion = 7	
group by 
		 "op".id_area
		,"op".mes
		,"op".id_tipo_operacion
		,"op".projections_rp_definition
		,"op".area
		,"op".periodo
--		,"op".FlagIsDisminution
--		,"op".FlagIsProvision
--		,"op".FlagIsNextMonth
		,"op".tpo
		,"op".id_month
		,"op".is_current
		,"op".labDays
		,"op".labRestDays
		,"op".labFullDays
		,"op".PresupuestoIngresos
		,"op".PresupuestoKms
		,"op".PresupuestoTon
		,"op".PresupuestoViajes
--		,"op".fecha
	
	
-- =========================================================================================================== --
   
  with "op" as (
    select 
		 "aw".id_area
		,"aw".mes
		,"aw".id_tipo_operacion
		,"aw".projections_rp_definition
		,"aw".area
		,"aw".periodo
		,"aw".FlagIsDisminution
		,"aw".FlagIsProvision
		,"aw".FlagIsNextMonth
		,sum("aw".kms) as 'kms'
		,sum("aw".subtotal) as 'subtotal'
		,sum("aw".peso) as 'peso'
		,sum("aw".num_viajes) as 'viajes'
		,"aw".[year]
		,'aceptado' as 'tpo'
		,"aw".fecha
		,"hd".id_month
		,"hd".is_current
		,"hd".labDays
		,"hd".labRestDays
		,"hd".labFullDays
		,"ppto".PresupuestoIngresos
		,"ppto".PresupuestoKms
		,"ppto".PresupuestoTon
		,"ppto".PresupuestoViajes
    from 
    		"sistemas"."dbo"."operations_view_full_gst_indicators" as "aw"   	
    left join 
   			"sistemas"."dbo"."ingresos_costos_gst_holidays" as "hd"
   		on 
   			"aw".periodo = "hd".period
   	left join 
   			"sistemas"."dbo"."ingresos_costos_view_ind_ppto_ingresos" as "ppto"
   		on 
   			"aw".id_area = "ppto".id_area and "aw".projections_rp_definition = "ppto".frt and "aw".periodo = "ppto".periodo
   	inner join
   		"sistemas"."dbo"."operations_year_selectors" as "yr"
   		on 
   			"aw".[year] = "yr".cyear   		
	group by 
		 "aw".id_area
		,"aw".mes
		,"aw".id_tipo_operacion
		,"aw".projections_rp_definition
		,"aw".area
		,"aw".periodo
		,"aw".FlagIsDisminution
		,"aw".FlagIsProvision
		,"aw".FlagIsNextMonth
		,"aw".[year]
		,"aw".fecha
		,"hd".id_month
		,"hd".is_current
		,"hd".labDays
		,"hd".labRestDays
		,"hd".labFullDays
		,"ppto".PresupuestoIngresos
		,"ppto".PresupuestoKms
		,"ppto".PresupuestoTon
		,"ppto".PresupuestoViajes
   union all 
    select 
		 "dw".id_area
		,"dw".mes
		,"dw".id_tipo_operacion
		,"dw".projections_rp_definition
		,"dw".area
		,"dw".periodo
		,"dw".FlagIsDisminution
		,"dw".FlagIsProvision
		,"dw".FlagIsNextMonth
		,sum("dw".kms) as 'kms'
		,sum("dw".subtotal) as 'subtotal'
		,sum("dw".peso) as 'peso'
		,sum("dw".num_viajes) as 'viajes'
		,"dw".[year]
		,'despachado' as 'tpo'
		,"dw".fecha
		,"xhd".id_month
		,"xhd".is_current
		,"xhd".labDays
		,"xhd".labRestDays
		,"xhd".labFullDays
		,"xppto".PresupuestoIngresos
		,"xppto".PresupuestoKms
		,"xppto".PresupuestoTon
		,"xppto".PresupuestoViajes
    from 
    	sistemas.dbo.operations_view_full_gst_dispatched_indicators as "dw"
    left join 
   			"sistemas"."dbo"."ingresos_costos_gst_holidays" as "xhd"
   		on 
   			"dw".periodo = "xhd".period
   	left join 
   			"sistemas"."dbo"."ingresos_costos_view_ind_ppto_ingresos" as "xppto"
   		on 
   			"dw".id_area = "xppto".id_area and "dw".projections_rp_definition = "xppto".frt and "dw".periodo = "xppto".periodo
   	inner join
   		"sistemas"."dbo"."operations_year_selectors" as "xyr"
   		on 
   			"dw".[year] = "xyr".cyear
	group by 
		 "dw".id_area
		,"dw".mes
		,"dw".id_tipo_operacion
		,"dw".projections_rp_definition
		,"dw".area
		,"dw".periodo
		,"dw".FlagIsDisminution
		,"dw".FlagIsProvision
		,"dw".FlagIsNextMonth
		,"dw".[year]
		,"dw".fecha
		,"xhd".id_month
		,"xhd".is_current
		,"xhd".labDays
		,"xhd".labRestDays
		,"xhd".labFullDays
		,"xppto".PresupuestoIngresos
		,"xppto".PresupuestoKms
		,"xppto".PresupuestoTon
		,"xppto".PresupuestoViajes
	)
select 
	     "op".id_area
		,"op".mes
		,"op".id_tipo_operacion
		,"op".projections_rp_definition
		,"op".area
		,"op".periodo
--		,"op".FlagIsDisminution
--		,"op".FlagIsProvision
--		,"op".FlagIsNextMonth
		,sum("op".kms) as 'kms'
		,sum("op".subtotal) as 'subtotal'
		,sum("op".peso) as 'peso'
		,sum("op".viajes) as 'viajes'
		,"op".tpo
		,"op".id_month
		,"op".is_current
		,"op".labDays
		,"op".labRestDays
		,"op".labFullDays
		,"op".PresupuestoIngresos
		,"op".PresupuestoKms
		,"op".PresupuestoTon
		,"op".PresupuestoViajes
--		,"dw".fecha
from 
		"op" 
where 
		"op".fecha between '2020-01-01' and '2020-01-05' and "op".id_area in (1,4,6) and "op".id_tipo_operacion = 1	
group by 
		 "op".id_area
		,"op".mes
		,"op".id_tipo_operacion
		,"op".projections_rp_definition
		,"op".area
		,"op".periodo
--		,"op".FlagIsDisminution
--		,"op".FlagIsProvision
--		,"op".FlagIsNextMonth
		,"op".tpo
		,"op".id_month
		,"op".is_current
		,"op".labDays
		,"op".labRestDays
		,"op".labFullDays
		,"op".PresupuestoIngresos
		,"op".PresupuestoKms
		,"op".PresupuestoTon
		,"op".PresupuestoViajes
--		,"op".fecha
	
	
	
	
	
	
	
--	Hollidays
select * from "sistemas"."dbo"."ingresos_costos_gst_holidays"
use sistemas

exec sp_desc "projections_view_full_gst_xls_indicators"
exec sp_desc "ingresos_costos_gst_holidays"
exec sp_desc "ingresos_costos_view_ind_ppto_ingresos"
exec sp_desc "operations_year_selectors"

select * from sistemas.dbo."ingresos_costos_view_ind_ppto_ingresos"
	
-- PPTO

select 
	 id_area
	,area
	,frt
	,cyear
	,mes
	,periodo
	,subtotal
	,tname
	,PresupuestoIngresos
	,PresupuestoTon
	,PresupuestoKms
	,PresupuestoViajes
from -- select * from 
	"sistemas"."dbo"."ingresos_costos_view_ind_ppto_ingresos"
where 
	id_area = 1


-- YEAR Selection

select * from "sistemas"."dbo"."operations_year_selectors"

-- ===================================================================================================================== --		
-- [SNAPSHOT TABLE] despachado
-- Origin Core [2019] operations_view_full_company_dispatched_indicators  
-- ===================================================================================================================== --
-- @Test select id_tipo_operacion,tipo_de_operacion,id_cliente,cliente,* from sistemas.dbo.operations_view_full_gst_dispatched_indicators where num_guia = 'CU-176509'
--	select id_tipo_operacion,id_cliente,prestamo,* from gstdb.dbo.trafico_guia where num_guia = 'CU-176509'
use sistemas
IF OBJECT_ID ('facturations_view_full_gst_edition_monitors', 'V') IS NOT NULL
    DROP VIEW facturations_view_full_gst_edition_monitors;
-- now build the view
create view facturations_view_full_gst_edition_monitors
with encryption
as --   	
select 
		 1 as 'company'
		,"aceptado".id_area						-- 1
		,"aceptado".no_viaje					-- 7
		,"aceptado".id_area_liq
		,"aceptado".id_personal
		,"aceptado".no_liquidacion
		,"aceptado".fecha_liquidacion
		,"aceptado".id_ruta
		,"aceptado".no_ejes_viaje
		,"aceptado".id_origen
		,"aceptado".id_destino
		,"aceptado"."fecha-real-viaje"
		,"aceptado"."fecha-real-fin-viaje"
		,"aceptado"."fecha-despachado"
		,"aceptado".fecha_real_viaje
		,"aceptado".fecha_real_fin_viaje
		,"aceptado".f_despachado
		,"aceptado".lts_empresa
		,"aceptado".ton_viaje
		,"aceptado".id_unidad					-- 2
		,"aceptado".id_remolque1
		,"aceptado".id_remolque2
		,"aceptado".id_dolly
		,"aceptado".id_ingreso
		,"aceptado".status_viaje
		,"aceptado".viajeactual
		,"aceptado".no_tarjeta_llave
		,"aceptado".id_configuracionviaje		-- 3
		,"aceptado".id_areaviaje
		,"aceptado".fecha_real_viaje_m
		,"aceptado".fecha_real_fin_viaje_m
		,"aceptado".f_despachado_m
		,"aceptado"."day"
		,"aceptado"."mes-despacho"
		,"aceptado".periodo_aceptado as 'periodo'
		,"aceptado".periodo_despachado
		,"aceptado".no_guia
		,"aceptado".tipo_pago
		,"aceptado".status_guia
		,"aceptado".clasificacion_doc
		,"aceptado"."fecha-guia"
		,"aceptado"."fecha-confirmacion"
		,"aceptado"."fecha-ingreso"
		,"aceptado".fecha_modifico
		,"aceptado"."fecha-cancelacion"
		,"aceptado"."fecha-contabilizado"
		,"aceptado".fecha_contabilizado_m
		,"aceptado".fecha_confirmacion_m
		,"aceptado".fecha_guia_m
		,"aceptado".fecha_cancelacion_m
		,"aceptado".fecha_contabilizado
		,"aceptado".fecha_confirmacion
		,"aceptado".fecha_guia
		,"aceptado".fecha_cancelacion
		,"aceptado".gday as 'dia' -- when is accepted
		,"aceptado".convenido_tonelada
		,"aceptado".id_area_facturacion
		,"aceptado".id_cliente
		,"aceptado".gid_personal
		,"aceptado".id_remitente
		,"aceptado".Plaza
		,"aceptado".Zona
		,"aceptado".id_serieguia
		,"aceptado".id_plaza
		,"aceptado".num_serie
		,"aceptado".area_solomon
		,"aceptado".desc_plaza
		,"aceptado".nombre
		,"aceptado".nombrecorto
		,"aceptado".ciudad
		,"aceptado".id_destinatario
		,"aceptado".kms_guia
		,"aceptado".flete
		,"aceptado".seguro
		,"aceptado".maniobras
		,"aceptado".autopistas
		,"aceptado".otros
		,"aceptado".iva_guia
		,"aceptado".cobro_viaje_kms
		,"aceptado".tipo_doc
		,"aceptado".gid_origen
		,"aceptado".gid_destino
		,"aceptado".id_tipo_operacion as 'id_fraccion'						--5
--		,"aceptado".id_fraccion
		,"aceptado".fraccion
		,"aceptado".projections_rp_definition
		,"aceptado".plaza_emision
		,"aceptado".num_guia
		,"aceptado".no_carta
		,"aceptado".gid_remolque1
		,"aceptado".gid_unidad
		,"aceptado".no_remision
		,"aceptado".motivo_cancelacion
		,"aceptado".gid_remolque2
		,"aceptado".prestamo
		,"aceptado".num_guia_asignado
		,"aceptado".no_deposito
		,"aceptado".gid_ingreso
		,"aceptado".personalnombre
		,"aceptado".tipo_facturacion
		,"aceptado".no_transferencia_cobranza
		,"aceptado".factor_iva
		,"aceptado".num_guiacancel
		,"aceptado".tipo_origen
		,"aceptado".no_poliza
		,"aceptado".monto_retencion
		,"aceptado".status_pago
		,"aceptado".monto_retenciontercero
		,"aceptado".sustituye_documento
		,"aceptado".monto_ivaflete
		,"aceptado".tipocambioconvenio
		,"aceptado".desc_flete
		,"aceptado".flete_bruto
		,"aceptado".id_iva
		,"aceptado".id_retencion
		,"aceptado".id_convenio
		,"aceptado".id_areaconvenio
		,"aceptado".id_tipo_operacion				-- 4
		,"aceptado".no_kit
		,"aceptado".monto_ivadescto
		,"aceptado".monto_retdescto
		,"aceptado".monto_descto
		,"aceptado".periodo_facturacion
		,"aceptado".tipo_producto
		,"aceptado".num_guia_incentivo
		,"aceptado".facturado
		,"aceptado".id_modifico
		,"aceptado".kms_convenio
		,"aceptado".observacion
		,"aceptado".mes
		,"aceptado".desc_ruta
		,"aceptado".cliente
		,"aceptado".id_flota						-- 6
		,"aceptado".configuracion_viaje
		,"aceptado".tipo_de_operacion
		,'' as 'flota' --"aceptado".flota
		,"aceptado".area
		,"aceptado".fraction
		,"aceptado".FlagIsDisminution
		,"aceptado".FlagIsProvision
		,"aceptado".kms_viaje
		,"aceptado".kms_real
		,"aceptado".kms
		,"aceptado".subtotal
		,"aceptado"."peso-aceptado" as 'peso'
		,"aceptado"."peso-despachado"
		,"aceptado".trip_count as 'num_viajes'
		,"aceptado".FlagIsNextMonth
		,year("aceptado"."fecha-guia") as 'year'
		,'' as "Descripcion"
from 
		sistemas.dbo.projections_view_full_gst_testcore_indicators "aceptado"
where 
		 "aceptado".FlagIsDisminution = 0
	and 
		 "aceptado".flagIsProvision = 1 --Prestamo
	and 
		 "aceptado".FlagIsNextMonth = 0 --negativo
--	and 
--		 "core".prestamo
	and 
		 "aceptado".tipo_doc = 2
    and 
    	"aceptado".status_guia <> 'B'
--    and 
--    	"aceptado".fecha_guia <= dateadd(day,-1,cast(current_timestamp as date))
--    and 
--    	"aceptado".fecha_guia <= dateadd(day,-1,cast(current_timestamp as date))    	
    	

--    select * from sistemas.dbo.operations_view_full_gst_dispatched_indicators where id_area = 6
--    	
--    select status_guia,prestamo,tipo_doc,* from sistemas.dbo.operations_view_full_gst_indicators where id_area = 6
--    	
--    select status_guia,prestamo,tipo_doc,* from gstdb.dbo.trafico_guia where id_area = 6
--    
--    select status_guia,prestamo,tipo_doc,count(id_area) from gstdb.dbo.trafico_guia where id_area = 6
--    group by status_guia,prestamo,tipo_doc
--    

--    	select * from sistemas.dbo.projections_view_full_gst_core_ops_indicators where area = 'LA PAZ'
    
    
    
    
    
    select * from sistemas.dbo.ingresos_costos_holidays order by id
    
    select * from sistemas.dbo.ingresos_costos_gst_holidays order by id
    
    select * from sistemas.dbo.ingresos_costos_his_holidays order by id
    
    
    
    
select no_viaje,id_area,FlagIsDisminution,FlagIsProvision,FlagIsNextMonth,subtotal,projections_rp_definition,f_despachado,[fecha-cancelacion],[fecha-guia],* from sistemas.dbo.projections_view_full_gst_testcore_indicators where FlagIsNextMonth > 0 and periodo_despachado = 202002
    
    
select * from sistemas.dbo.operations_view_full_gst_dispatched_indicators where no_viaje = 134494
    
    