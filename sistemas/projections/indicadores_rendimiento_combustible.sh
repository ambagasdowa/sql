
ip=10.8.0.235

database='sistemas'
#table='disponibilidad_main_view_rpt_unidades_gst_indicators'
table='disponibilidad_main_view_rpt_group_clasifications_indicators'

#sqsh -S SERV -U user -P passwd -D db -L bcp_colsep=',' -m bcp 
#	-C 'select * from some_table where foo=bar' > /path/to/output.out

# Define table variable

sqsh -S $ip -U zam -P lis -D $database -L bcp_colsep=' ' -m bcp -C "select '@'+COLUMN_NAME , DATA_TYPE + (case when CHARACTER_MAXIMUM_LENGTH is not null then '(' + cast(CHARACTER_MAXIMUM_LENGTH as varchar(255)) +')' else '' END) from \"INFORMATION_SCHEMA\".columns where table_name = '${table}'" > /tmp/vars_a.sql && cat /tmp/vars_a.sql

sqsh -S $ip -U zam -P lis -D $database -L bcp_colsep=' ' -m bcp -C "select COLUMN_NAME , DATA_TYPE + (case when CHARACTER_MAXIMUM_LENGTH is not null then '(' + cast(CHARACTER_MAXIMUM_LENGTH as varchar(255)) +')' else '' END) from \"INFORMATION_SCHEMA\".columns where table_name = '${table}'" > /tmp/vars_b.sql && cat /tmp/vars_b.sql


sqsh -S $ip -U zam -P lis -D $database -L bcp_colsep=' ' -m bcp -C "select '@'+COLUMN_NAME + ' = ' + COLUMN_NAME from \"INFORMATION_SCHEMA\".columns where table_name = '${table}'" > /tmp/vars_z.sql && cat /tmp/vars_z.sql


sqsh -S $ip -U zam -P lis -L bcp_colsep=' ' -m bcp -D $database -C "select '@'+COLUMN_NAME + ' , ' from \"INFORMATION_SCHEMA\".columns where table_name = '${table}'" > /tmp/vars_y.sql && cat /tmp/vars_y.sql


sqsh -S $ip -U zam -P lis -D sistemas -L bcp_colsep=' ' -m bcp  -C "select COLUMN_NAME + ' , ' from \"INFORMATION_SCHEMA\".columns where table_name = '${table}'" > /tmp/vars_x.sql && cat /tmp/vars_x.sql


# NOTE set obj.name form columns in table 

sqsh -S $ip -U zam -P lis -L bcp_colsep=' ' -m bcp -D $database -C "select 'test.'+COLUMN_NAME + ' , ' from \"INFORMATION_SCHEMA\".columns where table_name = '${table}'" > /tmp/vars_c.sql && cat /tmp/vars_c.sql
