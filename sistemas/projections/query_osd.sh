
ip=10.8.0.235

#sqsh -S SERV -U user -P passwd -D db -L bcp_colsep=',' -m bcp 
#	-C 'select * from some_table where foo=bar' > /path/to/output.out

# Define table variable

sqsh -S $ip -U zam -P lis -D gstdb -L bcp_colsep=' ' -m bcp -C "select '@'+COLUMN_NAME , DATA_TYPE + (case when CHARACTER_MAXIMUM_LENGTH is not null then '(' + cast(CHARACTER_MAXIMUM_LENGTH as varchar(255)) +')' else '' END) from \"INFORMATION_SCHEMA\".columns where table_name = 'mtto_unidades'" > /tmp/mtto_unidades.sql && cat /tmp/mtto_unidades.sql

sqsh -S $ip -U zam -P lis -D gstdb -L bcp_colsep=' ' -m bcp -C "select COLUMN_NAME , DATA_TYPE + (case when CHARACTER_MAXIMUM_LENGTH is not null then '(' + cast(CHARACTER_MAXIMUM_LENGTH as varchar(255)) +')' else '' END) from \"INFORMATION_SCHEMA\".columns where table_name = 'mtto_unidades'" > /tmp/mttox_unidades.sql && cat /tmp/mttox_unidades.sql


sqsh -S $ip -U zam -P lis -D gstdb -L bcp_colsep=' ' -m bcp -C "select '@'+COLUMN_NAME + ' = ' + COLUMN_NAME from \"INFORMATION_SCHEMA\".columns where table_name = 'mtto_unidades'" > /tmp/zvars.sql && cat /tmp/zvars.sql


sqsh -S $ip -U zam -P lis -L bcp_colsep=' ' -m bcp -D gstdb -C "select '@'+COLUMN_NAME + ' , ' from \"INFORMATION_SCHEMA\".columns where table_name = 'mtto_unidades'" > /tmp/yvars.sql && cat /tmp/yvars.sql


sqsh -S $ip -U zam -P lis -D sistemas -L bcp_colsep=' ' -m bcp  -C "select COLUMN_NAME + ' , ' from \"INFORMATION_SCHEMA\".columns where table_name = 'disponibilidad_tbl_mtto_log_gst_indicators'" > /tmp/xvars.sql && cat /tmp/xvars.sql
