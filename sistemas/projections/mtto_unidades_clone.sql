-- USE [pruebasdb2019]
-- GO

/****** Object:  Table [dbo].[mtto_unidades]    Script Date: 15/06/2021 02:21:44 p.m. ******/
select  DB_NAME()

drop table sistemas.dbo.disponibilidad_tbl_mtto_log_gst_indicators

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
--GO
CREATE TABLE sistemas.dbo.disponibilidad_tbl_mtto_log_gst_indicators(
									id_control int identity(1,1), 

									[id_unidad] [varchar](10) NOT NULL,
									[total_llantas] [int] NULL,
									[tipo_unidad] [int] NULL,
									[id_tipo_motor] [int] NULL,
									[id_marca_unidad] [int] NOT NULL,
									[id_tipo_unidad] [int] NOT NULL,
									[kms_acumulados] [int] NOT NULL,
									[r_f_v] [int] NULL,
									[id_centro_costo] [int] NULL,
									[llantas_extras] [int] NULL,
									[no_permiso] [int] NULL,
									[no_inciso_seguro] [int] NULL,
									[id_operador] [int] NULL,
									[kms_servicio_ultimo] [int] NULL,
									[kms_servicio_sig] [int] NULL,
									[id_area] [int] NOT NULL,
									[id_depto] [int] NOT NULL,
									[kms_inicio_ciclo] [int] NULL,
									[secuencia_inicio] [int] NULL,
									[idcia] [int] NULL,
									[hrs_inicio_ciclo] [decimal](18, 6) NULL,
									[no_cilindros] [int] NULL,
									[prov_carroceria] [int] NULL,
									[secuencia_ult] [int] NULL,
									[secuencia_sig] [int] NULL,
									[no_circula] [int] NULL,
									[doble_no_circula] [int] NULL,
									[id_tercero] [int] NULL,
									[id_status] [int] NULL,
									[status_comm] [int] NULL,
									[msg_pend] [int] NULL,
									[id_ruta] [int] NULL,
									[id_area_viaje] [int] NULL,
									[no_viaje] [int] NULL,
									[id_pedido] [int] NULL,
									[id_flota] [int] NULL,
									[id_origen] [int] NULL,
									[id_destino] [int] NULL,
									[id_remitente] [int] NULL,
									[id_destinatario] [int] NULL,
									[id_macro_ult] [int] NULL,
									[costo_tenencia] [decimal](18, 6) NULL,
									[no_poliza] [decimal](18, 6) NULL,
									[prima_neta] [decimal](18, 6) NULL,
									[recargos] [decimal](18, 6) NULL,
									[der_poliza] [decimal](18, 6) NULL,
									[iva] [decimal](18, 6) NULL,
									[costo_unidad] [decimal](18, 6) NULL,
									[costo_carroceria] [decimal](18, 6) NULL,
									[ultima_tenencia] [datetime] NULL,
									[ultimo_refrendo] [datetime] NULL,
									[ultimo_permiso] [datetime] NULL,
									[vence_seguro] [datetime] NULL,
									[fecha_kms] [datetime] NULL,
									[fecha_servicio_ultimo] [datetime] NULL,
									[fecha_servicio_sig] [datetime] NULL,
									[fecha_adquisicion] [datetime] NULL,
									[fecha_ultima_rev] [datetime] NULL,
									[fecha_inicio_ciclo] [datetime] NULL,
									[fecha_emision] [datetime] NULL,
									[fecha_desde] [datetime] NULL,
									[fecha_hasta] [datetime] NULL,
									[fecha_adq_carr] [datetime] NULL,
									[fecha_estatus] [datetime] NULL,
									[fecha_baja] [datetime] NULL,
									[posdate] [datetime] NULL,
									[f_real_salida] [datetime] NULL,
									[f_prog_llegada] [datetime] NULL,
									[f_entrada_patio] [datetime] NULL,
									[f_mismapos] [datetime] NULL,
									[TEA] [datetime] NULL,
									[rendimiento_esperado] [decimal](18, 6) NULL,
									[lts_combustible] [decimal](18, 6) NULL,
									[lts_aceite] [decimal](18, 6) NULL,
									[kms_por_dia] [decimal](18, 6) NULL,
									[dim_ancho] [decimal](18, 6) NULL,
									[dim_largo] [decimal](18, 6) NULL,
									[dim_altura] [decimal](18, 6) NULL,
									[rendimiento_aceite] [decimal](18, 6) NULL,
									[hrs_acumuladas] [decimal](18, 6) NULL,
									[hrs_por_dia] [decimal](18, 6) NULL,
									[hrs_servicio_ultimo] [decimal](18, 6) NULL,
									[hrs_servicio_sig] [decimal](18, 6) NULL,
									[proveedor] [decimal](18, 6) NULL,
									[peso_unidades] [decimal](18, 6) NULL,
									[lts_relleno] [decimal](18, 6) NULL,
									[lts_saldocomb] [decimal](18, 6) NULL,
									[no_serie] [varchar](20) NULL,
									[modelo] [varchar](15) NOT NULL,
									[id_ciclo_preventivo] [varchar](6) NULL,
									[placas] [varchar](8) NULL,
									[observaciones] [varchar](150) NULL,
									[no_economico] [varchar](10) NULL,
									[id_servicio_ultimo] [varchar](6) NULL,
									[id_servicio_sig] [varchar](6) NULL,
									[id_servicio_inicio] [varchar](6) NULL,
									[tarjeta_circulacion] [varchar](10) NULL,
									[no_factura] [varchar](10) NULL,
									[tipo_motor] [varchar](10) NULL,
									[potencia_motor] [varchar](8) NULL,
									[pulgadas_cubicas] [varchar](6) NULL,
									[no_motor] [varchar](20) NULL,
									[color] [varchar](10) NULL,
									[no_carroceria] [varchar](10) NULL,
									[obs_carroceria] [varchar](200) NULL,
									[fecha_vence_placas] [datetime] NULL,
									[poliza] [varchar](40) NULL,
									[fecha_vence_tenencia] [datetime] NULL,
									[status_unidad] [varchar](2) NULL,
									[viaje] [varchar](35) NULL,
									[fecha_vence_seguro] [datetime] NULL,
									[id_thermo] [varchar](10) NULL,
									[fecha_vence_verificacion] [datetime] NULL,
									[no_kit] [varchar](10) NULL,
									[desc_marcamotor] [varchar](25) NULL,
									[desc_provcarroceria] [varchar](80) NULL,
									[desc_provunidad] [varchar](80) NULL,
									[tarjeta_llave] [char](1) NULL,
									[no_tarjeta_llave] [varchar](25) NULL,
									[mctnumber] [varchar](50) NULL,
									[posicion] [varchar](250) NOT NULL,
									[poslat] [decimal](18, 7) NULL,
									[poslon] [decimal](18, 7) NULL,
									[operador] [varchar](40) NULL,
									[id_unidad_as] [varchar](10) NULL,
									[id_remolque1] [varchar](20) NULL,
									[id_dolly] [varchar](10) NULL,
									[id_remolque2] [varchar](20) NULL,
									[origen] [varchar](30) NULL,
									[destino] [varchar](30) NULL,
									[id_despachador] [varchar](10) NULL,
									[tipo_combustible] [char](1) NOT NULL,
									[estado] [char](1) NULL,
									[estado_adquisicion] [char](1) NULL,
									[datos_generales] [varchar](255) NULL,
									[tipo_moneda] [char](1) NULL,
									[forma_pago] [char](1) NULL,
									[kms_hrs] [char](1) NULL,
									[hrs_automaticas] [char](1) NULL,
									[estatus] [char](1) NULL,
									[tipo_programacion] [char](1) NULL,
									[equipo_omnitracs] [char](1) NULL,
									[tercero] [char](1) NULL,
									[causa_baja] [char](1) NULL,
									[tipo] [char](1) NULL,
									[status] [char](1) NULL,
									[status_caja1] [char](1) NULL,
									[status_caja2] [char](1) NULL,
									[en_patio] [char](1) NULL,
									[mismapos] [char](1) NULL,
									[f_status_unidad] [datetime] NULL,
									[subcta] [varchar](25) NULL,
									[tipomacro_ult] [char](1) NULL,
									[kms_actuales] [int] NOT NULL,
									[ruta] [varchar](50) NULL,
									[peso_bruto] [int] NULL,
									[fecha_instalacion] [datetime] NULL,
									[kmshrsinicialaparmed] [int] NULL,
									[no_medidor] [int] NULL,
									[tipo_medida] [char](1) NULL,
									[id_operador2] [int] NULL,
									[hrs_actuales] [decimal](18, 6) NOT NULL,
									[id_ingreso] [varchar](15) NULL,
									[fecha_ingreso] [datetime] NULL,
									[kms_hrs_actuales] [int] NULL,
									[fecha_disponible] [datetime] NULL,
									[id_plazadestino] [int] NOT NULL,
									[id_tipo_operacion] [int] NOT NULL,
									[id_movimiento] [int] NOT NULL,
									[clas_uni] [varchar](20) NULL,
									[cta_consumo] [varchar](20) NULL,
									[id_status_rem] [int] NOT NULL,
									[ultimo_id_destinatario] [int] NOT NULL,
									[ultimo_id_area_viaje] [int] NOT NULL,
									[ultimo_no_viaje] [int] NOT NULL,
									[id_estacion] [int] NOT NULL,
									[ultimo_id_areapedido] [int] NOT NULL,
									[ultimo_id_pedido] [int] NOT NULL,
									[origen_status] [int] NOT NULL,
									[cargadovacio] [char](1) NOT NULL,
									[kms_ultima_rev] [int] NULL,
									[id_transponder] [varchar](8) NULL,
									[num_soporte] [varchar](8) NULL,
									[vehunitnum] [varchar](21) NULL,
									[nacionalidad] [int] NULL,
									[id_localidaddestino] [int] NOT NULL,
									[id_circuito] [int] NOT NULL,
									[id_compania] [int] NOT NULL,
									[kms_arribo] [int] NOT NULL,
									[ultimo_id_pedidopk] [int] NOT NULL,
									[alarmasnuevas] [int] NOT NULL,
									[mensajesnuevos] [int] NOT NULL,
									[id_linearem1] [varchar](10) NULL,
									[id_linearem2] [varchar](10) NULL,
									[modulo] [int] NOT NULL,
									[id_asignacion] [int] NOT NULL,
									[kms_actualesvale] [int] NULL,
									[kms_recorridosvale] [int] NULL,
									[modelo_unidad] [varchar](20) NULL,
									[madre] [char](1) NOT NULL,
									[hija] [char](1) NOT NULL,
									[asignado] [char](1) NOT NULL,
									[unidad_asignado] [varchar](11) NOT NULL,
									[hija_asignada] [varchar](11) NOT NULL,
									[FECHA_LOGISTICA] [datetime] NULL,
									[valor_convenido] [decimal](18, 6) NULL,
									[deducible_xrobo] [decimal](18, 6) NULL,
									[deducible_xdmateriales] [decimal](18, 6) NULL,
									[faset] [decimal](18, 6) NOT NULL,
									[jefe_flota] [int] NOT NULL,
									[id_plazasigdestino] [int] NOT NULL,
									[id_sector] [int] NOT NULL,
									[id_subflota] [int] NOT NULL,
									[EM_email] [varchar](150) NULL,
									[no_orsan] [varchar](100) NULL,
									[valida_cargacomb] [int] NOT NULL,
									[procesado] [bit] NOT NULL,
									[disponibilidad] [decimal](18, 6) NOT NULL,
									[capacidad_aliado] [char](1) NOT NULL,
									[marca_cap_eje_delantero] [varchar](120) NOT NULL,
									[marca_cap_eje_trasero] [varchar](120) NULL,
									[marca_modelo_tipo_transmision] [varchar](120) NOT NULL,
									[marca_suspension] [varchar](120) NOT NULL,
									[medida_llanta] [varchar](120) NOT NULL,
									[tipo_ejes] [varchar](120) NOT NULL,
									[tipo_suspension] [varchar](120) NOT NULL,
									[marca_aliado] [varchar](120) NOT NULL,
									[ano_fabricacion] [int] NOT NULL,
									[id_histograma_sig] [int] NULL,
									[id_histograma_ultimo] [int] NULL,
									[fecha_fin_garantia] [datetime] NULL,
									[id_accion] [int] NULL,
									[id_project] [int] NULL,
									[ccosto] [int] NOT NULL,
									[id_tipo_servicio_unidad] [int] NULL,
									[poliza2] [varchar](40) NULL,
									[no_inciso_seguro2] [int] NULL,
									[vence_seguro2] [datetime] NULL,
									[id_aseguradora] [int] NULL,
									[cta_mayor] [varchar](25) NULL,

									-- NOTE AddOns for internal work 
									id_ref	int null,
									id_gst int null ,
									status_query varchar(50) null,
									status_active tinyint default 1 null,
									created datetime ,
									modified datetime 
) on [primary]
SET ANSI_PADDING OFF


-- disponibilidad_tbl_unidades_gst_indicators id_mtto_clone || Sales.SalesReason / SalesReasonID
-- disponibilidad_tbl_mtto_log_gst_indicators id_control ||  Sales.TempSalesReason / TempID

ALTER TABLE sistemas.dbo.disponibilidad_tbl_mtto_log_gst_indicators
   ADD CONSTRAINT FK_mtto_LogIndicators FOREIGN KEY (id_control)
	      REFERENCES disponibilidad_tbl_unidades_gst_indicators (id_mtto_clone)
   ON DELETE CASCADE
   ON UPDATE CASCADE
;

select * from disponibilidad_tbl_mtto_log_gst_indicators

	-- NOTE ommit next Lines 


 CONSTRAINT [PK_mtto_unidades] PRIMARY KEY CLUSTERED 
(
	[id_unidad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[mtto_unidades]  WITH CHECK ADD  CONSTRAINT [FK_mtto_unidades_08] FOREIGN KEY([id_status_rem])
REFERENCES [dbo].[desp_status_remolques] ([id_status])
GO

ALTER TABLE [dbo].[mtto_unidades] CHECK CONSTRAINT [FK_mtto_unidades_08]
GO

ALTER TABLE [dbo].[mtto_unidades]  WITH CHECK ADD  CONSTRAINT [FK_mtto_unidades_id_sector] FOREIGN KEY([id_sector])
REFERENCES [dbo].[Sectores] ([id_sector])
GO

ALTER TABLE [dbo].[mtto_unidades] CHECK CONSTRAINT [FK_mtto_unidades_id_sector]
GO

ALTER TABLE [dbo].[mtto_unidades]  WITH CHECK ADD  CONSTRAINT [FK_mtto_unidades_id_subflota] FOREIGN KEY([id_subflota])
REFERENCES [dbo].[Subflotas] ([id_subflota])
GO

ALTER TABLE [dbo].[mtto_unidades] CHECK CONSTRAINT [FK_mtto_unidades_id_subflota]
GO

ALTER TABLE [dbo].[mtto_unidades]  WITH CHECK ADD  CONSTRAINT [FK_mtto_unidades_tmp01] FOREIGN KEY([ultimo_id_destinatario])
REFERENCES [dbo].[trafico_cliente] ([id_cliente])
GO

ALTER TABLE [dbo].[mtto_unidades] CHECK CONSTRAINT [FK_mtto_unidades_tmp01]
GO

ALTER TABLE [dbo].[mtto_unidades]  WITH CHECK ADD  CONSTRAINT [FK_mtto_unidades_tmp02] FOREIGN KEY([id_plazadestino])
REFERENCES [dbo].[trafico_plaza] ([id_plaza])
GO

ALTER TABLE [dbo].[mtto_unidades] CHECK CONSTRAINT [FK_mtto_unidades_tmp02]
GO

ALTER TABLE [dbo].[mtto_unidades]  WITH CHECK ADD  CONSTRAINT [FK_mtto_unidades01] FOREIGN KEY([no_kit])
REFERENCES [dbo].[trafico_kit_unidad] ([no_kit])
GO

ALTER TABLE [dbo].[mtto_unidades] CHECK CONSTRAINT [FK_mtto_unidades01]
GO

ALTER TABLE [dbo].[mtto_unidades]  WITH CHECK ADD  CONSTRAINT [FK_mtto_unidades02] FOREIGN KEY([id_area], [id_depto])
REFERENCES [dbo].[general_departamentos] ([id_area], [id_depto])
GO

ALTER TABLE [dbo].[mtto_unidades] CHECK CONSTRAINT [FK_mtto_unidades02]
GO

ALTER TABLE [dbo].[mtto_unidades]  WITH CHECK ADD  CONSTRAINT [FK_mtto_unidades03] FOREIGN KEY([id_tipo_motor])
REFERENCES [dbo].[trafico_motor] ([id_tipo_motor])
GO

ALTER TABLE [dbo].[mtto_unidades] CHECK CONSTRAINT [FK_mtto_unidades03]
GO

ALTER TABLE [dbo].[mtto_unidades]  WITH CHECK ADD  CONSTRAINT [FK_mtto_unidades04] FOREIGN KEY([id_marca_unidad])
REFERENCES [dbo].[mtto_marcas_unidades] ([id_marca_unidad])
GO

ALTER TABLE [dbo].[mtto_unidades] CHECK CONSTRAINT [FK_mtto_unidades04]
GO

ALTER TABLE [dbo].[mtto_unidades]  WITH CHECK ADD  CONSTRAINT [FK_mtto_unidades05] FOREIGN KEY([id_tipo_unidad])
REFERENCES [dbo].[mtto_tipos_unidades] ([id_tipo_unidad])
GO

ALTER TABLE [dbo].[mtto_unidades] CHECK CONSTRAINT [FK_mtto_unidades05]
GO

ALTER TABLE [dbo].[mtto_unidades]  WITH CHECK ADD  CONSTRAINT [FK_mtto_unidades06] FOREIGN KEY([id_plazadestino])
REFERENCES [dbo].[trafico_plaza] ([id_plaza])
GO

ALTER TABLE [dbo].[mtto_unidades] CHECK CONSTRAINT [FK_mtto_unidades06]
GO

ALTER TABLE [dbo].[mtto_unidades]  WITH CHECK ADD  CONSTRAINT [FK_mtto_unidades07] FOREIGN KEY([id_circuito])
REFERENCES [dbo].[llantas_circuitos] ([id_circuito])
GO

ALTER TABLE [dbo].[mtto_unidades] CHECK CONSTRAINT [FK_mtto_unidades07]
GO

ALTER TABLE [dbo].[mtto_unidades]  WITH CHECK ADD  CONSTRAINT [FK_mtto_unidades09] FOREIGN KEY([id_estacion])
REFERENCES [dbo].[desp_estaciones] ([id_estacion])
GO

ALTER TABLE [dbo].[mtto_unidades] CHECK CONSTRAINT [FK_mtto_unidades09]
GO

ALTER TABLE [dbo].[mtto_unidades]  WITH CHECK ADD  CONSTRAINT [FK_mtto_unidades10] FOREIGN KEY([id_tipo_operacion])
REFERENCES [dbo].[desp_tipooperacion] ([id_tipo_operacion])
GO

ALTER TABLE [dbo].[mtto_unidades] CHECK CONSTRAINT [FK_mtto_unidades10]
GO

ALTER TABLE [dbo].[mtto_unidades]  WITH CHECK ADD  CONSTRAINT [FK_mtto_unidades11] FOREIGN KEY([ultimo_id_pedidopk])
REFERENCES [dbo].[desp_pedido] ([id_pedidopk])
GO

ALTER TABLE [dbo].[mtto_unidades] CHECK CONSTRAINT [FK_mtto_unidades11]
GO

ALTER TABLE [dbo].[mtto_unidades]  WITH CHECK ADD  CONSTRAINT [FK_mtto_unidades12] FOREIGN KEY([id_compania])
REFERENCES [dbo].[compania] ([id_compania])
GO

ALTER TABLE [dbo].[mtto_unidades] CHECK CONSTRAINT [FK_mtto_unidades12]
GO


