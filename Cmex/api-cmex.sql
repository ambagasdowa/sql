-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--  Author						: Jesus Baizabal
--  email		        : baizabal.jesus@gmail.com
--  Create date			: December 01, 2022
--  Description			: Build tables for cmx-api module
--  TODO		          : clean
--  @Last_patch			: --
--  @license					: MIT License (http://www.opensource.org/licenses/mit-license.php)
--  Database owner		: Jesus Baizabal
--  @status					: Stable
--  @version	        : 0.0.1
-- Copyright © 2022, UES devops - portalapps.com, All Rights Reserved
-------------------------------------------------------------------------------
-- Description:        Verbose description of what the query does goes here. Be specific and don't be
--                     afraid to say too much. More is better, than less, every single time. Think about
--                     "what, when, where, how and why" when authoring a description.
-- Call by:            [schema.usp_ProcThatCallsThis]
--                     [Application Name]
--                     [Job]
--                     [PLC/Interface]
-- Affected table(s):  [schema.TableModifiedByProc1]
--                     [schema.TableModifiedByProc2]
-- Used By:            Functional Area this is use in, for example, Payroll, Accounting, Finance
-- Parameter(s):       @param1 - description and usage
--                     @param2 - description and usage
-- Usage:              EXEC dbo.usp_DoSomeStuff
--                         @param1 = 1,
--                         @param2 = 3,
--                         @param3 = 2
--                     Additional notes or caveats about this object, like where is can and cannot be run, or
--                     gotchas to watch for when using it.
-------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Catalog Status
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
--truncate table sistemas.dbo.cmex_api_parents
-- Test 

-- SELECT * FROM INFORMATION_SCHEMA.COLUMNS
-- example 
-- select * from sistemas.dbo.providers_parents

/* SELECT * FROM sysobjects WHERE xtype='U' */ 

/* SELECT TABLE_NAME */ 
/* FROM [<DATABASE_NAME>].INFORMATION_SCHEMA.TABLES */ 
/* WHERE TABLE_TYPE = 'BASE TABLE' */

SELECT TABLE_NAME 
FROM [sistemas].INFORMATION_SCHEMA.TABLES 
WHERE TABLE_TYPE = 'BASE TABLE'
and table_name like 'cmex_api_%'



IF OBJECT_ID('sistemas.dbo.cmex_api_sections', 'U') IS NOT NULL
  DROP TABLE sistemas.dbo.cmex_api_sections;
-- go
set ansi_nulls on
set quoted_identifier on
set ansi_padding on
-- test 
-- select * from  sistemas.dbo.cmex_api_parents
create table [sistemas].[dbo].[cmex_api_sections](
        id                      int identity(1,1),
        section  nvarchar(150)       collate     sql_latin1_general_cp1_ci_as,
        created                 datetime,
        modified                datetime,
        user_id                 int,
        _status                 tinyint default 1 null
) on [primary]
-- go

set ansi_padding off
-- go

insert into sistemas.dbo.cmex_api_sections values
       ('Emisor',CURRENT_TIMESTAMP,null,1,1)                                        -- 1
      ,('Receptor',CURRENT_TIMESTAMP,null,1,1)                                      -- 2
      ,('Conceptos',CURRENT_TIMESTAMP,null,1,1)                                     -- 3
      ,('Impuestos',CURRENT_TIMESTAMP,null,1,1)                                     -- 4
      ,('Retenciones',CURRENT_TIMESTAMP,null,1,1)                                   -- 5
      ,('Traslados',CURRENT_TIMESTAMP,null,1,1)                                     -- 6
      ,('CartaPorte20_CartaPorte',CURRENT_TIMESTAMP,null,1,1)                       -- 7
      ,('CartaPorte20_UbicacionOrigen',CURRENT_TIMESTAMP,null,1,1)                  -- 8
      ,('CartaPorte20_DomicilioOrigen',CURRENT_TIMESTAMP,null,1,1)                  -- 9
      ,('CartaPorte20_UbicacionDestino',CURRENT_TIMESTAMP,null,1,1)               -- 10
      ,('CartaPorte20_DomicilioDestino',CURRENT_TIMESTAMP,null,1,1)               -- 11
      ,('CartaPorte20_Mercancias',CURRENT_TIMESTAMP,null,1,1)                       -- 12
      ,('CartaPorte20_Mercancia',CURRENT_TIMESTAMP,null,1,1)                        -- 13
      ,('CartaPorte20_Autotransporte',CURRENT_TIMESTAMP,null,1,1)                   -- 14
      ,('CartaPorte20_IdentificacionVehicular',CURRENT_TIMESTAMP,null,1,1)          -- 15
      ,('CartaPorte20_Seguros',CURRENT_TIMESTAMP,null,1,1)                          -- 16
      ,('CartaPorte20_Remolques',CURRENT_TIMESTAMP,null,1,1)                        -- 17
      ,('CartaPorte20_TiposFigura',CURRENT_TIMESTAMP,null,1,1)                      -- 18
      ,('Addendas',CURRENT_TIMESTAMP,null,1,1)                                      -- 19
--                                        ,('order',CURRENT_TIMESTAMP,null,1,1)                                         -- 16
                                        ;
-- select * from sistemas.dbo.cmex_api_sections
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Catalog Status Parents
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- select * from sistemas.dbo.cmex_api_standings 


IF OBJECT_ID('sistemas.dbo.cmex_api_tags', 'U') IS NOT NULL
  DROP TABLE sistemas.dbo.cmex_api_tags;
-- go
set ansi_nulls on
set quoted_identifier on
set ansi_padding on

create table [sistemas].[dbo].[cmex_api_tags](
        id                          int identity(1,1),
        cmex_api_section_id            int,
        cmex_api_tagname     nvarchar(150)       collate     sql_latin1_general_cp1_ci_as,
        created                     datetime,
        modified                    datetime,
        user_id                     int,
        _status                     tinyint default 1 null
) on [primary]
-- go

set ansi_padding off
-- go

insert into sistemas.dbo.cmex_api_tags values
    -- Emisor
    (1,'rfc',CURRENT_TIMESTAMP,null,1,1)                -- 1
   ,(1,'nombre',CURRENT_TIMESTAMP,null,1,1)             -- 2
   ,(1,'regimen_fiscal',CURRENT_TIMESTAMP,null,1,1)     -- 3
    -- Receptor
   ,(2,'rfc',CURRENT_TIMESTAMP,null,1,1)     -- 3
   ,(2,'nombre',CURRENT_TIMESTAMP,null,1,1)     -- 3
   ,(2,'domicilio_fiscal_receptor',CURRENT_TIMESTAMP,null,1,1)     -- 3
   ,(2,'regimen_fiscal_receptor',CURRENT_TIMESTAMP,null,1,1)     -- 3
   ,(2,'uso_cfdi',CURRENT_TIMESTAMP,null,1,1)     -- 3
    -- Conceptos
    -- Impuestos
   ,(4,'retenciones',CURRENT_TIMESTAMP,null,1,1)     -- 3
   ,(4,'traslados',CURRENT_TIMESTAMP,null,1,1)     -- 3
   ,(4,'total_impuestos_traslados',CURRENT_TIMESTAMP,null,1,1)     -- 3
   ,(4,'total_impuestos_retenidos',CURRENT_TIMESTAMP,null,1,1)     -- 3
    -- Retenciones     
   ,(5,'impuesto',CURRENT_TIMESTAMP,null,1,1)     -- 3
   ,(5,'importe',CURRENT_TIMESTAMP,null,1,1)     -- 3
    -- Traslados
   ,(6,'base',CURRENT_TIMESTAMP,null,1,1)     -- 3
   ,(6,'impuesto',CURRENT_TIMESTAMP,null,1,1)     -- 3
   ,(6,'tipo_factor',CURRENT_TIMESTAMP,null,1,1)     -- 3
   ,(6,'tasa_o_cuota',CURRENT_TIMESTAMP,null,1,1)     -- 3
   ,(6,'importe',CURRENT_TIMESTAMP,null,1,1)     -- 3

    -- CartaPorte
   ,(7,'version',CURRENT_TIMESTAMP,null,1,1)     -- 3
   ,(7,'transp_internac',CURRENT_TIMESTAMP,null,1,1)     -- 3
   ,(7,'total_dist_rec',CURRENT_TIMESTAMP,null,1,1)     -- 3
    -- UbicacionOrigen
   ,(8,'tipo_ubicacion',CURRENT_TIMESTAMP,null,1,1)     -- 3
   ,(8,'i_d_ubicacion',CURRENT_TIMESTAMP,null,1,1)     -- 3
   ,(8,'r_f_c_remitente_destinatario',CURRENT_TIMESTAMP,null,1,1)     -- 3
   ,(8,'nombre_remitente_destinatario',CURRENT_TIMESTAMP,null,1,1)     -- 3
   ,(8,'fecha_hora_salida_llegada',CURRENT_TIMESTAMP,null,1,1)     -- 3
    -- DomicilioOrigen
   ,(9,'calle',CURRENT_TIMESTAMP,null,1,1)     -- 3
   ,(9,'localidad',CURRENT_TIMESTAMP,null,1,1)     -- 3
   ,(9,'municipio',CURRENT_TIMESTAMP,null,1,1)     -- 3
   ,(9,'estado',CURRENT_TIMESTAMP,null,1,1)     -- 3
   ,(9,'pais',CURRENT_TIMESTAMP,null,1,1)     -- 3
   ,(9,'codigo_postal',CURRENT_TIMESTAMP,null,1,1)     -- 3
    --UbicacionDestino
   ,(10,'tipo_ubicacion',CURRENT_TIMESTAMP,null,1,1)     -- 3
   ,(10,'i_d_ubicacion',CURRENT_TIMESTAMP,null,1,1)     -- 3
   ,(10,'r_f_c_remitente_destinatario',CURRENT_TIMESTAMP,null,1,1)     -- 3
   ,(10,'nombre_remitente_destinatario',CURRENT_TIMESTAMP,null,1,1)     -- 3
   ,(10,'fecha_hora_salida_llegada',CURRENT_TIMESTAMP,null,1,1)     -- 3  
    -- DomicilioDestino
   ,(11,'calle',CURRENT_TIMESTAMP,null,1,1)     -- 3
   ,(11,'localidad',CURRENT_TIMESTAMP,null,1,1)     -- 3
   ,(11,'municipio',CURRENT_TIMESTAMP,null,1,1)     -- 3
   ,(11,'estado',CURRENT_TIMESTAMP,null,1,1)     -- 3
   ,(11,'pais',CURRENT_TIMESTAMP,null,1,1)     -- 3
   ,(11,'codigo_postal',CURRENT_TIMESTAMP,null,1,1)     -- 3
    -- Mercancias
    ,(12,'peso_bruto_total',CURRENT_TIMESTAMP,null,1,1)     -- 3
    ,(12,'unidad_peso',CURRENT_TIMESTAMP,null,1,1)     -- 3
    ,(12,'num_total_mercancias',CURRENT_TIMESTAMP,null,1,1)     -- 3
    -- Mercancia
    ,(13,'bienes_transp',CURRENT_TIMESTAMP,null,1,1)     -- 3
    ,(13,'descripcion',CURRENT_TIMESTAMP,null,1,1)     -- 3
    ,(13,'cantidad',CURRENT_TIMESTAMP,null,1,1)     -- 3
    ,(13,'clave_unidad',CURRENT_TIMESTAMP,null,1,1)     -- 3
    ,(13,'unidad',CURRENT_TIMESTAMP,null,1,1)     -- 3
    ,(13,'peso_en_kg',CURRENT_TIMESTAMP,null,1,1)     -- 3
    -- Autotransporte
    ,(14,'perm_s_c_t',CURRENT_TIMESTAMP,null,1,1)     -- 3
    ,(14,'num_permiso_s_c_t',CURRENT_TIMESTAMP,null,1,1)     -- 3
    -- Identificacion Vehicular
    ,(15,'config_vehicular',CURRENT_TIMESTAMP,null,1,1)     -- 3
    ,(15,'placa_v_m',CURRENT_TIMESTAMP,null,1,1)     -- 3
    ,(15,'anio_modelo_v_m',CURRENT_TIMESTAMP,null,1,1)     -- 3
    -- Seguros
    ,(16,'asegura_resp_civil',CURRENT_TIMESTAMP,null,1,1)     -- 3
    ,(16,'poliza_resp_civil',CURRENT_TIMESTAMP,null,1,1)     -- 3
    --Remolques
    ,(17,'sub_tipo_rem',CURRENT_TIMESTAMP,null,1,1)     -- 3
    ,(17,'placa',CURRENT_TIMESTAMP,null,1,1)     -- 3
    --Figura
    ,(18,'tipo_figura',CURRENT_TIMESTAMP,null,1,1)     -- 3
    ,(18,'r_f_c_figura',CURRENT_TIMESTAMP,null,1,1)     -- 3
    ,(18,'num_licencia',CURRENT_TIMESTAMP,null,1,1)     -- 3
    ,(18,'nombre_figura',CURRENT_TIMESTAMP,null,1,1)     -- 3

--    ,(15,'',CURRENT_TIMESTAMP,null,1,1)     -- 3
    -- Addendas
;

-- select * from sistemas.dbo.cmex_api_tags
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Controls providersControlsFiles
------------------------------------------------------------------------------------------------------------------------------------------------------------------------

IF OBJECT_ID('sistemas.dbo.cmex_api_controls_files', 'U') IS NOT NULL
  DROP TABLE sistemas.dbo.cmex_api_controls_files;
-- go
set ansi_nulls on
set quoted_identifier on
set ansi_padding on

create table [sistemas].[dbo].[cmex_api_controls_files](
        id                          bigint identity(1,1),
--        cmex_api_assoc_vendors_id  int null,
        user_id                     int null,
        labelname                   char(255) null,
        _filename                   nvarchar(250)           collate     sql_latin1_general_cp1_ci_as null,
        _pathname                   char(255) null,
        _extname                    char(10) null,
        _md5sum                     nvarchar(250)           collate     sql_latin1_general_cp1_ci_as null,
        _file_size                  char(255)               collate     sql_latin1_general_cp1_ci_as null,
        _atime                      nvarchar(150)           collate     sql_latin1_general_cp1_ci_as null,
        _mtime                      nvarchar(150)           collate     sql_latin1_general_cp1_ci_as null,
        _ctime                      nvarchar(150)           collate     sql_latin1_general_cp1_ci_as null,
        _username                   nvarchar(50)            collate     sql_latin1_general_cp1_ci_as null,
        _datetime_login             datetime null,
        _ip_remote                  nvarchar(25)            collate     sql_latin1_general_cp1_ci_as null,
        cmex_api_standings_id       int null,
        cmex_api_parents_id         int null,
        created                     datetime null,
        modified                    datetime null,
        _status                     tinyint default 1 null
) on [primary]
-- go

set ansi_padding off
-- go
insert into sistemas.dbo.cmex_api_controls_files values
(
     1,'filename.xml','xxxxxxxxxxxxx','url','xml','xxxxxxxxxxxxxxxxx','1','1','1','1','username',current_timestamp,'1.1.1.1',1,1,current_timestamp,current_timestamp,1
),
(
     1,'filename1.pdf','xxxxxxxxxxxxx','url','pdf','xxxxxxxxxxxxxxxxx','1','1','1','1','username',current_timestamp,'1.1.1.1',2,1,current_timestamp,current_timestamp,1
);


-- select * from sistemas.dbo.cmex_api_controls_files

-- NOTE The view if needed
select 
        *
from 
        sistemas.dbo.cmex_api_controls_files as [files]
    inner join 
        sistemas.dbo.cmex_api_parents as [parents]
    on 
        [files].cmex_api_parents_id = [parents].id
    inner join 
        sistemas.dbo.cmex_api_standings as [stands]
    on 
        [files].cmex_api_standings_id = [stands].id





IF OBJECT_ID ('sistemas.dbo.cmex_api_vendor_files_', 'V') IS NOT NULL
    DROP VIEW sistemas.dbo.cmex_api_vendor_files_

-- select * from sistemas.dbo.cmex_api_vendor_files
-- select * from sistemas.dbo.cmex_api_assoc_vendors
-- NOTE version 1.0.0
create
-- alter
view sistemas.dbo.cmex_api_vendor_files
with encryption
as
    select
         /* "docs".BatNbr */
        /* ,"docs".PONbr */
        /* ,"docs".VendId */
        /* ,"docs".CpnyId */
        /* ,"docs".RefNbr */
         [xml]          = max( case when "stands".cmex_api_standings_name = 'xml' then "files".id else null end)
        ,[voucher]      = max( case when "stands".cmex_api_standings_name = 'voucher' then "files".id else null end)
        ,[order]        = max( case when "stands".cmex_api_standings_name = 'order' then "files".id else null end)
        ,[xml_name]     = max( case when "stands".cmex_api_standings_name = 'xml' then "files".labelname else null end)
        ,[xml_src]      = max( case when "stands".cmex_api_standings_name = 'xml' then "files".[_filename] else null end)
        ,[voucher_name] = max( case when "stands".cmex_api_standings_name = 'voucher' then "files".labelname else null end)
        ,[voucher_src]  = max( case when "stands".cmex_api_standings_name = 'voucher' then "files".[_filename] else null end)
        ,[order_name]   = max( case when "stands".cmex_api_standings_name = 'order' then "files".labelname else null end)
        ,[order_src]    = max( case when "stands".cmex_api_standings_name = 'order' then "files".[_filename] else null end)
    from
--            sistemas.dbo.cmex_api_assoc_vendors as "docs"
--    left join
--            sistemas.dbo.cmex_api_controls_files as "files"
--        on
--            "docs".id = "files".cmex_api_assoc_vendors_id
--        and
--            "docs".[_status] = 1 and "files".[_status] = 1
--    inner join
            sistemas.dbo.cmex_api_standings as "stands"
        on
            "stands".id = "files".cmex_api_standings_id
    inner join
            sistemas.dbo.cmex_api_parents as "parents"
        on
            "parents".id = "files".cmex_api_parents_id
--where "docs".BatNbr = '481583'
    group by
         "docs".BatNbr
        ,"docs".PONbr
        ,"docs".VendId
        ,"docs".CpnyId
        ,"docs".RefNbr



-- Build the xml file
IF OBJECT_ID('sistemas.dbo.cmex_api_cfdi_comprobante', 'U') IS NOT NULL
  DROP TABLE sistemas.dbo.cmex_api_cfdi_comprobante;
-- go
set ansi_nulls on
set quoted_identifier on
set ansi_padding on

create table sistemas.dbo.cmex_api_cfdi_comprobante (
        id                          bigint identity(1,1),
        cmex_api_controls_files_id  int not null,
        version                     varchar(11) null,
        serie                       varchar(25) null,
        folio                       varchar(40),
        fecha                       datetime null,                 --> This
        no_certificado              varchar(20) null,
        subtotal                    varchar(1000) null,
        descuento                   varchar(1000) null,
        total                       nvarchar(max) null,            --> this
        moneda                      varchar(10) null,
        tipo_cambio                 nvarchar(max) null,            --> this
        tipo_comprobante            varchar(25) null ,
        metodo_pago                 varchar(150) null,
        forma_pago                  varchar(255) null,
        condiciones_pago            varchar(255) null,
        exportacion                 varchar(150) null,
        lugar_expedicion            varchar(150) null,
        sello                       nvarchar(max) null,
        certificado                 nvarchar(max) null,
        cmex_api_standings_id       int null,
        cmex_api_parents_id         int null,
        created                     datetime null,
        modified                    datetime null,
        _status                     tinyint default 1 null
    )on [primary]

set ansi_padding off


-- tfd11 data 

-- Build the xml file
IF OBJECT_ID('sistemas.dbo.cmex_api_cfdi_tfds', 'U') IS NOT NULL
  DROP TABLE sistemas.dbo.cmex_api_cfdi_tfds;
-- go
set ansi_nulls on
set quoted_identifier on
set ansi_padding on

create table sistemas.dbo.cmex_api_cfdi_tfds (
        id                              bigint identity(1,1),
        cmex_api_controls_files_id      int not null,
        version                         varchar(11) null,
        no_certificado_sat              nvarchar(max) null,
        uuid                            varchar(36) null,
        fecha_timbrado                  datetime null,                 --> This
        rfc_prov_cert                   varchar(50) null,
        sello_cfd                       nvarchar(max) null,
        sello_sat                       nvarchar(max) null,
        cmex_api_standings_id           int null,
        cmex_api_parents_id             int null,
        created                         datetime null,
        modified                        datetime null,
        _status                         tinyint default 1 null
    )on [primary]

set ansi_padding off


/* -- the last identity value generated by the current connection */
/* select  @@IDENTITY */
/* -- This function will return the value of the last identity inserted in the current executing batch. */
/* select SCOPE_IDENTITY() */
/* -- returns the last identity value inserted in the specified table */
/* select IDENT_CURRENT('sistemas.dbo.cmex_api_cfdi_comprobante'); */






IF OBJECT_ID('sistemas.dbo.cmex_api_cfdi_data', 'U') IS NOT NULL
  DROP TABLE sistemas.dbo.cmex_api_cfdi_data;
-- go
set ansi_nulls on
set quoted_identifier on
set ansi_padding on

create table sistemas.dbo.cmex_api_cfdi_data (
        id                          bigint identity(1,1),
        cmex_api_controls_files_id  int not null,
        cmex_api_section_id         int null,
        cmex_api_tags_id            int null,
        value                       nvarchar(max)       collate     sql_latin1_general_cp1_ci_as,
        created                     datetime null,
        modified                    datetime default null,
        _status                     tinyint default 1 null
    )on [primary]

set ansi_padding off

select * from sistemas.dbo.cmex_api_cfdi_data
select * from sistemas.dbo.cmex_api_cfdi_data where cmex_api_section_id in (8,10)




-- View 
select 
         [files].*
        ,[comprobante].*
        ,[tfd].*
from
        sistemas.dbo.cmex_api_controls_files as [files]
    inner join 
        sistemas.dbo.cmex_api_cfdi_comprobante as [comprobante]
    on 
        [comprobante].cmex_api_controls_files_id = [files].id
    inner join 
        sistemas.dbo.cmex_api_cfdi_tfds as [tfd]
    on 
        [tfd].cmex_api_controls_files_id = [files].id
order by cast([comprobante].folio as int)





select [_md5sum] from sistemas.dbo.cmex_api_controls_files where [_md5sum] = '09e4e5885af59f15b0ae60088b4ec9d3'

select * from sistemas.dbo.cmex_api_controls_files
select * from sistemas.dbo.cmex_api_cfdi_tfds
select * from sistemas.dbo.cmex_api_cfdi_data 
select * from sistemas.dbo.cmex_api_cfdi_comprobante


delete sistemas.dbo.cmex_api_controls_files where id in ('181')
delete sistemas.dbo.cmex_api_cfdi_data where cmex_api_controls_files_id in ('181')
delete sistemas.dbo.cmex_api_cfdi_comprobante where cmex_api_controls_files_id in ('181')
delete sistemas.dbo.cmex_api_cfdi_tfds where cmex_api_controls_files_id in ('181')

select * from sistemas.dbo.cmex_api_controls_files



-- Treuncates
truncate table sistemas.dbo.cmex_api_controls_files
truncate table sistemas.dbo.cmex_api_cfdi_tfds
truncate table sistemas.dbo.cmex_api_cfdi_comprobante
truncate table sistemas.dbo.cmex_api_cfdi_data

