-- =================================================================================== --
--   Issues in integraapp
-- =================================================================================== --

use integraapp


-- check the records
select 
		FiscYr,substring(Perpost,1,4) as 'newyear',* 
from 
		integraapp.dbo.GLTran 
where 	
		FiscYr = '' 
	or 
		FiscYr is null

-- update records
update integraapp.dbo.GLTran
		set FiscYr = substring(Perpost,1,4)
where FiscYr = '' or FiscYr is null