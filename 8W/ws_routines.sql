-- =========================================================================== --
-- NOTE Gateway Tables
--
-- =========================================================================== --
use [ws]
IF OBJECT_ID('dbo.ws_gateway_routes', 'U') IS NOT NULL 
  DROP TABLE dbo.ws_gateway_routes
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on
-- go
create table dbo.ws_gateway_routes(
		 id									bigint	identity(1,1)
		,api_ctrl						bigint	null									-- NOTE set the method 
		,ctrl_id						bigint  null									-- NOTE set the id_record
		,sie_id							bigint  null									-- NOTE set the id_corporation
		,stk_id							bigint  null									-- NOTE additional register
		,api_reg						nvarchar(max)	 null						-- NOTE if string is needed
		,ctrl_reg						nvarchar(max)	 null						-- NOTE if string is needed
		,sie_reg						nvarchar(max)	 null						-- NOTE if string is needed
		,stack							tinyint default 1 null        -- NOTE set in/out
		,[add]							tinyint default 1 null				-- NOTE add or update register
		,rem								tinyint default 0 null				-- NOTE remove register
		,description				text			null								-- NOTE free text 
		,created						datetime 	default current_timestamp null
		,modified						datetime	null
		,status							tinyint 	default 1 null
) on [primary]
-- go
set ansi_padding off


-- =========================================================================== --
-- NOTE id union control for pedido and solicitud
-- NOTE for Implementation
-- =========================================================================== --
use [ws]
IF OBJECT_ID('dbo.ws_items_routes', 'U') IS NOT NULL 
  DROP TABLE dbo.ws_items_routes
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on
-- go
create table dbo.ws_gateway_routes(
		 id									bigint	identity(1,1)
		,api_ctrl						bigint	null									-- NOTE set the method 
		,ctrl_id						int  null											-- NOTE set if pedido = 1 or solicitud = 2
		,sie_id							bigint  null									-- NOTE set the id_corporation
		,stk_id							bigint  null									-- NOTE additional register
		,id_item						nvarchar(max)	 null						-- NOTE if string is needed
		,sie_reg						nvarchar(max)	 null						-- NOTE if string is needed
		,stack							tinyint default 1 null        -- NOTE set in/out
		,description				text			null								-- NOTE free text 
		,created						datetime 	default current_timestamp null
		,modified						datetime	null
		,status							tinyint 	default 1 null
) on [primary]
-- go
set ansi_padding off

