--unblock options , activate for send post data 

exec sp_configure 'show advanced options',1
reconfigure
exec sp_configure 'Ole Automation Procedures',1
-- store procedure for send post and get 
use [ws]


CREATE 
-- ALTER
PROCEDURE [ws].[dbo].[ws_sp_send_post_data]
 (
	@action_id int,
	@postData nvarchar(max) -- just in case
 )
as
set ANSI_NULLS on 
set QUOTED_IDENTIFIER on 
set NOCOUNT on 
-- ==================================================================================================== --
--	NOTE This is the script working for 8W post request
-- ==================================================================================================== --
--NOTE Emulate the execution 
--																					<ApiKey>5679</ApiKey>
--																					<ReferenciaExterna>243021</ReferenciaExterna>
--select @postData
declare @action_id as int = 2 

-- NOTE get the necesary data for the procedure
declare @url as nvarchar(999)
declare @Host as nvarchar(255)
declare @action as nvarchar(900) --= 'ConsultarViajePorFechas'
declare @soap as nvarchar(800)
--declare @postdata as nvarchar(max)
declare @postdata as xml
declare @parameters as nvarchar(max)
declare @request as nvarchar(max)

declare @element as nvarchar(max)

select 
			 @url = url
			,@Host = host
			,@soap = soap 
			,@action = action
			,@postdata = postdata
			,@parameters = parameters
			,@request = request
from 
		  ws.dbo.ws_view_builder_actions
where 
		  id = @action_id

-- DEBUG a for send 
-- NOTE Emulated selected data from query
		declare @apikey as nvarchar(10) = '5679'
		declare @referenciaexterna as nvarchar(10) = '243021'

-- NOTE Cursor Solution ? for each inner_action attach a variable content :: maybe yes because we need execute line per line of inner_action 
-- TODO start cursor for set modifications to xml 
-- https://stackoverflow.com/questions/38857749/the-argument-1-of-the-xml-data-type-method-modify-must-be-a-string-literal

	declare el_cursor cursor for 
		select modify_element from ws.dbo.ws_actions_elements where ws_post_actions_id = @action_id
	 open el_cursor 
		fetch next from el_cursor into @element 

		while @@FETCH_STATUS = 0 
			begin 
-- NOTE test Dynamic SQL --
		declare @sql nvarchar(max)
			set @sql = @element
		exec sp_executesql -- NOTE this is an automatic params 
											@sql
										 ,@parameters
										 ,@postdata = @postdata output
										-- NOTE this is automatically posible ?
										 ,@apikey = @apikey
										 ,@referenciaexterna = @referenciaexterna
-- NOTE end test --			
			fetch next from el_cursor into @element
			end
		close el_cursor
	 deallocate el_cursor

select @postdata 		

--select @postData,@url,@Host,@soap,@action 
DECLARE @status int
DECLARE @responseText as table(responseText nvarchar(max))
DECLARE @res as Int
	-- This going to be a Constant parameter 
--NOTE DECLARE @url as nvarchar(1000) = 'https://cloudmx.unigis.com/8W/mapi/soap/logistic/Service.asmx' -- this must be in the params
declare @contentType NVARCHAR(164)
--declare @postData NVARCHAR(2000)
declare @soapAction nvarchar(800)
--NOTE declare @Host nvarchar(255)
--declare @action as nvarchar(900) = 'ConsultarViajePorFechas'
set @ContentType = 'text/xml; charset=utf-8'
-- NOTE Principal parameter this be build
set @soapAction = @soap+@action     --'ConsultarViajePorFechas' -- this in parameters
--NOTE set @Host = 'cloudmx.unigis.com'                            -- and this too

EXEC sp_OACreate 'MSXML2.ServerXMLHTTP', @res OUT
EXEC sp_OAMethod @res, 'open', NULL, 'POST',@url,'false'
EXEC sp_OAMethod @res, 'setRequestHeader', NULL, 'Content-type', @contentType
EXEC sp_OAMethod @res, 'setRequestHeader', NULL, 'SOAPAction', @soapAction
EXEC sp_OAMethod @res, 'setRequestHeader', NULL, 'Host', @Host
--EXEC sp_OAMethod @res, 'send'
EXEC sp_OAMethod @res, 'send', NULL, @postdata
EXEC sp_OAGetProperty @res, 'status', @status out

INSERT INTO @ResponseText (ResponseText) EXEC sp_OAGetProperty @res, 'responseText'
EXEC sp_OADestroy @res

--exec (@request)
declare @respond nvarchar(max)
select @respond = responseText from @responseText 

		declare @xql nvarchar(max)
			set @xql = @request
		exec sp_executesql -- NOTE this is an automatic params 
											@xql
										 ,N'@status int, @responseText nvarchar(max) , @action nvarchar(899), @referenciaexterna nvarchar(10)'
										-- ,@postdata = @postdata output
										 ,@status = @status
										 ,@responseText = @respond
										 ,@action = @action
										-- NOTE this is automatically posible ?
										 ,@referenciaexterna = @referenciaexterna

--	insert into ws_responses
--		exec (@request)
-- NOTE Working 		
--		select @status, @action ,responseText,'with ReferenciaExterna : ' +  @referenciaexterna,current_timestamp,current_timestamp,1 FROM @responseText



exec 'declare @st int ; set @st=1 ; select @st '

---------------------------- WARNING TESTING WARNING -----------------------------------
--NOTE testing db


declare @sql varchar(249) = ' select @test,@at ', @xest as varchar(20) , @xt as int

execute sp_executesql 
												 @sql
												,'@xest varchar(20) output,@xt int output '
												,@xest = @xest output 
											  ,@xt = @xt output  
select @xest,@xt 



declare @test as char(10) = 'diez'
select 'this is a test' as 'num'
	union all
select @test as 'num'


SELECT message_id AS Error, severity AS Severity,  
[Event Logged] = CASE is_event_logged WHEN 0 THEN 'No' ELSE 'Yes' END,
text AS [Description]
FROM sys.messages
WHERE language_id = 1033 -- <desired language, such as 1033 for US English>
and message_id like 2147--211489
ORDER BY message_id



-- NOTE REPLACE EXAMPLE


DECLARE @myDoc XML
SET @myDoc = '<Root>
<Location LocationID="10"
            LaborHours="1.1"
            MachineHours=".2" >Manufacturing steps are described here.
<step>test@mail.com</step>
<step>Manufacturing step 2 at this work center</step>
</Location>
</Root>'
SELECT @myDoc

-- update text in the first manufacturing step
SET @myDoc.modify('
  replace value of (/Root/Location/step[1]/text())[1]
  with "some@mail.com"
')
SELECT @myDoc
-- update attribute value
SET @myDoc.modify('
  replace value of (/Root/Location/@LaborHours)[1]
  with "100.0"
')
SELECT @myDoc

-- NOTE example 

DECLARE @myDoc XML  
SET @myDoc = '<Root>  
<Location LocationID="10"   
            LaborHours=".1"  
            MachineHours=".2" >Manu steps are described here.  
<step>Manufacturing step 1 at this work center</step>  
<step>Manufacturing step 2 at this work center</step>  
</Location>  
</Root>'  
--SELECT @myDoc  
  
SET @myDoc.modify('  
  replace value of (/Root/Location[1]/@LaborHours)[1]  
  with (  
       if (count(/Root/Location[1]/step) > 3) then  
         "3.0"  
       else  
          "1.0"  
      )  
')  
SELECT @myDoc  



-- NOTE Example 

DROP TABLE T

CREATE TABLE T (i INT, x XML)

INSERT INTO T VALUES(1,'<Root>
<ProductDescription ProductID="1" ProductName="Road Bike">
<Features>
  <Warranty>1 year parts and labor</Warranty>
  <Maintenance>3 year parts and labor extended maintenance is available</Maintenance>
</Features>
</ProductDescription>
</Root>')

-- verify the current <ProductDescription> element
SELECT x.query(' /Root/ProductDescription')
FROM T
-- update the ProductName attribute value
UPDATE T
SET x.modify('
  replace value of (/Root/ProductDescription/@ProductName)[1]
  with "New Road Bike" ')
-- verify the update
SELECT x.query(' /Root/ProductDescription')
FROM T


-- NOTE Typed xml column 
USE AdventureWorks  
GO  
DROP TABLE T  
GO  
CREATE TABLE T(
  ProductModelID INT PRIMARY KEY,   
  Instructions XML (Production.ManuInstructionsSchemaCollection))  
GO  
INSERT T   
SELECT ProductModelID, Instructions  
FROM Production.ProductModel  
WHERE ProductModelID=7  
GO
--insert a new location - <Location 1000/>.   
UPDATE T  
SET Instructions.modify('  
  declare namespace MI="https://schemas.microsoft.com/sqlserver/2004/07/adventure-works/ProductModelManuInstructions";  
insert <MI:Location LocationID="1000"  LaborHours="1000"  LotSize="1000" >  
           <MI:step>Do something using <MI:tool>hammer</MI:tool></MI:step>  
         </MI:Location>  
  as first  
  into (/MI:root)[1]  
')  
GO  
SELECT Instructions  
FROM T  
GO  
-- Now replace manu. tool in location 1000  
UPDATE T  
SET Instructions.modify('  
  declare namespace MI="https://schemas.microsoft.com/sqlserver/2004/07/adventure-works/ProductModelManuInstructions";  
  replace value of (/MI:root/MI:Location/MI:step/MI:tool)[1]   
  with "screwdriver"  
')  
GO  
SELECT Instructions  
FROM T  
-- Now replace value of lot size  
UPDATE T  
SET Instructions.modify('  
  declare namespace MI="https://schemas.microsoft.com/sqlserver/2004/07/adventure-works/ProductModelManuInstructions";  
  replace value of (/MI:root/MI:Location/@LotSize)[1]   
  with 500 cast as xs:decimal ?  
')  
GO  
SELECT Instructions  
FROM T  




declare @xyz varchar(50) = '486'
declare @abc varchar(50) = '785'
declare @qwe varchar(50) = '520'

declare @x xml=
N'<soap:App xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://abcsystems.com/">
  <soap:Client>
    <Addresses>
      <PostalAddress AddressLine1="test">?????</PostalAddress>
      <Postal>?????</Postal>
    </Addresses>
  </soap:Client>
</soap:App>'

set @x.modify(
'
declare namespace soap="http://abcsystems.com/";
declare namespace unis="http://abcsystems.com/unis";
replace value of
(/soap:App/soap:Client/Addresses/PostalAddress/@AddressLine1)[1] 
with sql:variable("@abc")')

set @x.modify('declare namespace soap="http://abcsystems.com/";
replace value of
(/soap:App/soap:Client/Addresses/PostalAddress/text())[1]
with sql:variable("@xyz")')

set @x.modify('declare namespace soap="http://abcsystems.com/";
replace value of
(/soap:App/soap:Client/Addresses/Postal/text())[1]
with sql:variable("@qwe")')

select @x;





declare @apikey varchar(10) = '5679'
declare @referenciaexterna varchar(10) = '243021'

declare @postdata xml = N'<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
     <ConsultarOrdenPedido xmlns="http://unisolutions.com.ar/"> 
         <ApiKey>????</ApiKey>
         <ReferenciaExterna>??????</ReferenciaExterna>
     </ConsultarOrdenPedido>
  </soap:Body>
</soap:Envelope>'

	set @postdata.modify('
	  declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
		declare namespace ns="http://unisolutions.com.ar/";
	  replace value of (/soap:Envelope/soap:Body/ns:ConsultarOrdenPedido/ns:ApiKey/text())[1]
	  with sql:variable("@apikey")')

	set @postdata.modify('
	 declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
	 declare namespace ns="http://unisolutions.com.ar/";
	 replace value of
				(/soap:Envelope/soap:Body/ns:ConsultarOrdenPedido/ns:ReferenciaExterna/text())[1]
	 with	sql:variable("@referenciaexterna")')

	select @postdata
----------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------


DECLARE @myDoc XML  
SET @myDoc = '<Root>  
<Location LocationID="10"   
            LaborHours="1.1"  
            MachineHours=".2" >Manufacturing steps are described here.  
<step>Manufacturing step 1 at this work center</step>  
<step>Manufacturing step 2 at this work center</step>  
</Location>  
</Root>'  
SELECT @myDoc  
  
-- update text in the first manufacturing step  
SET @myDoc.modify('  
  replace value of (/Root/Location/step[1]/text())[1]  
  with "new text describing the manu step"  
')  
SELECT @myDoc  
-- update attribute value  
SET @myDoc.modify('  
  replace value of (/Root/Location/@LaborHours)[1]  
  with "100.0"  
')  
SELECT @myDoc;  



