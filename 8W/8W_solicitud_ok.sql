-- ===================================================================================== --
-- DEBUG SOLICITUD
-- ===================================================================================== --

use ws;
--create 
alter
view "dbo"."ws_view_solicitud_actions"
 with encryption
as
-- Method CrearOrdernesPedido[SOLICITUD]
select
 5679 as 'ApiKey'
,2 as 'ctrl_id'
,cast(cs.id_solicitud as nvarchar(25)) collate SQL_Latin1_General_CP1_CI_AS as 'Pedido_RefDocumento'
,cs.num_solicitud                     												 as 'Pedido_RefDocumentoAdicional'  -- change the datatype isnot working 
,cs.clientepaga_nombre																				 as 'Pedido_Descripcion'
,cs.fecha_ingreso																							 as 'Pedido_Fecha'
,cs.fecha_entrega_prog																				 as 'Pedido_FechaEntrega'
,cs.fecha_recoleccion_prog																		 as 'Pedido_FechaRecoleccion'
,cs.id_destinatario				                                     as 'Cliente_RefCliente'
,cs.destinatario_nombre			                                   as 'Cliente_RazonSocial'
,tcd.tel																											 as 'Cliente_Telefono'
,tcd.fax																										 	 as 'Cliente_Telefono2'
,tcd.email1						                                         as 'Cliente_Email'
,tcd.domicilio					                                       as	'Cliente_Direccion'
,tcd.latpos						                                         as 'Cliente_Latitud'
,tcd.lonpos						                                         as 'Cliente_Longitud'
,tpd.desc_plaza                                                as 'Cliente_Localidad'
,tcd.cp							                                           as 'Cliente_RefDomicilioExterno'
,tcd.nombre						                                         as 'Cliente_DomicilioDescripcion'
,'60'																												   as 'Cliente_TiempoEspera'
,tcd.contacto1					                                       as 'Cliente_Contacto'
,'false'                                                       as 'Cliente_CargaExclusiva'
,'false'                                                       as 'Cliente_IgnorarOperacion' 
,'true'                                                        as 'Cliente_CrearDomicilioOrden'
,'1'																													 as 'Cliente_ActualizarDomicilioOrden'
,'1'																													 as 'Cliente_ValidarDomicilioOrden'
,cs.id_remitente																							 as 'Cliente2_RefCliente'
,cs.remitente_nombre																					 as 'Cliente2_RazonSocial'
,tcr.tel						                                           as	'Cliente2_Telefono'
,tcr.fax																											 as 'Cliente2_Telefono2'
,tcr.email1																									   as 'Cliente2_Email'
,tcr.domicilio																					  		 as 'Cliente2_Direccion'
,tcr.latpos																										 as 'Cliente2_Latitud'
,tcr.lonpos																										 as 'Cliente2_Longitud'
,tpo.desc_plaza																						     as 'Cliente2_Localidad'
,tcr.cp																												 as 'Cliente2_RefDomicilioExterno'
,tcr.nombre																										 as 'Cliente2_Descripcion'
,'60'																													 as 'Cliente2_TiempoEspera'
,tcr.contacto1																								 as 'Cliente2_Contacto'
,'false'																											 as 'Cliente2_CargaExclusiva'
,'false'																											 as 'Cliente2_IgnorarOperacion' 
,'true'															 												   as 'Cliente2_CrearDomicilioOrden'
,'1'																													 as 'Cliente2_ActualizarDomicilioOrden'
,'1'																													 as 'Cliente2_ValidarDomicilioOrden'
,cs.id_clientepaga																						 as 'Pedido_Varchar1'
,cs.clientepaga_nombre				                                 as	'Pedido_Varchar2'
,tcc.domicilio				                                       	 as 'Pedido_Varchar3'
,'MEXICO'                                                      as 'Pedido_Varchar4'
,tcc.cp							                                           as 'Pedido_Varchar5'
,cf.num_factura                                                as 'Pedido_Varchar6'
,cs.id_area					                                        	 as 'Pedido_CodigoSucursal'
,cast(cs.recoleccion as nvarchar(1))	                     	 as 'Pedido_TipoPedido'
,'900'                                                         as 'Pedido_InicioHorario'
,'1800'																												 as 'Pedido_FinHorario1'
,'60'																												   as 'Pedido_TiempoEspera'
,'5'																													 as 'Pedido_Categoria'
,'1'																													 as 'Pedido_Prioridad'
,cs.id_area																										 as 'depositoSalida_RefDepositoExterno'
,cs.id_area																										 as 'depositoLlegada_RefDepositoExterno'
,'false'	                                                     as 'PedidoItem_usarProductos'
--,csd.desc_producto																						 as 'Items_Descripcion'
--,CASE (csd.embalaje) WHEN '0' THEN 'SIN ASIGNAR' WHEN '1' THEN 'TARIMAS' WHEN '2' THEN 'BULTOS' WHEN '3' THEN 'CARTONES' WHEN '4' THEN 'ROLLOS' WHEN '5' THEN 'PACAS'
--WHEN '6' THEN 'PAPELERIA' WHEN '7' THEN 'CAMIONETAS' WHEN '8' THEN 'SACOS' WHEN '9' THEN 'GRANEL' WHEN '10' THEN 'CAJAS' WHEN '11' THEN 'CONTENEDOR' WHEN '12' THEN '_'
--WHEN '13' THEN 'PALETS' WHEN '14' THEN 'TAMBOS' WHEN '15' THEN 'PZAS' WHEN '16' THEN 'ATADOS' WHEN '17' THEN 'LOTE' WHEN '18' THEN 'PAQUETES' WHEN '99' THEN 'OTROS'
--END																														 as 'Items_RefDocumento'
--,cast(csd.cantidad as decimal(18,6)	)													 as 'Items_Cantidad'
--,'0'																													 as 'Items_Volumen'
--,isnull (csd.peso_capturado / NULLIF(csd.cantidad,0),0)		     as 'Items_Peso'
--,'0'																													 as 'Items_Bulto'
--,'0'																												   as 'Items_Pallets'
,'false'																											 as 'Pedido_agruparItems'
,'1'																												   as 'Pedido_CodigoOperacion'
,'1'                                                           as 'Pedido2_CodigoSucursal'
,'1'																													 as 'Pedido_Unidades'
,'true'																											   as 'Pedido_usarProductos'
,'false'                                                       as 'Pedido_soloInsertarProductos'
,'false'                                                       as 'Pedido2_agruparItems'
,cf.flete																											 as 'Pedido_ValorDeclarado'
,'0'                                                           as 'Pedido_Float1'
,'0'                                                           as 'Pedido_Float2'
,cast(cs.id_solicitud as nvarchar(25)) collate SQL_Latin1_General_CP1_CI_AS as 'PedidoPk_RefDocumento'
from
pruebasdb2019.dbo.cons_solicitud cs  --Tabla a moitorear.
--left join
--pruebasdb2019.dbo.cons_solicituddet csd on cs.id_solicitud = csd.id_solicitud
left join 
pruebasdb2019.dbo.trafico_cliente tcc on cs.id_clientepaga = tcc.id_cliente 
left join
pruebasdb2019.dbo.trafico_cliente tcr on cs.id_remitente = tcr.id_cliente 
left join
pruebasdb2019.dbo.trafico_cliente tcd on cs.id_destinatario = tcd.id_cliente
left join
pruebasdb2019.dbo.trafico_plaza tpo on cs.id_plazaorigen = tpo.id_plaza 
left join
pruebasdb2019.dbo.trafico_plaza tpd on cs.id_plazadestino = tpd.id_plaza 
left join
pruebasdb2019.dbo.cons_factura cf on cs.id_solicitud = cf.id_solicitud and cs.id_area_pedido = cf.id_areacobranza
--where
--cs.fecha_ingreso between '2020-01-03 00:00:00.000' and '2020-30-06 23:59:59.000'
--and
---cs.id_area = 8

--select * from pruebasdb2019.dbo.cons_solicituddet where id_solicitud= 242880


