
--IF OBJECT_ID ('ws_view_pedido_actions', 'V') IS NOT NULL
--    DROP VIEW ws_view_pedido_actions'

use ws;
 alter view "dbo"."ws_view_pedido_actions"
 with encryption
as
-- Method CrearOrdernesPedido[PEDIDO]

select
	   5679 as 'ApiKey'
		,1 as 'ctrl_id'
		,cast(dp.num_pedido as nvarchar(25)) collate SQL_Latin1_General_CP1_CI_AS	as 'Pedido_RefDocumento'
		,tg.num_guia as 'Pedido_RefDocumentoAdicional'
		,tcc.nombre	as	'Pedido_Descripcion'
		,dp.fecha_ingreso	as 'Pedido_Fecha'
		,dp.f_pedido	as 'Pedido_FechaEntrega'
		,dp.f_pedido	as 'Pedido_FechaRecoleccion'
		,dp.id_destinatario	as 'Cliente_RefCliente'
		,tcd.nombre	as 'Cliente_RazonSocial'
		,tcd.tel	as 'Cliente_Telefono'
		,tcd.fax as 'Cliente_Telefono2'
		,tcd.email1 as 'Cliente_Email'
		,tcd.domicilio as 'Cliente_Direccion'
		,tcd.latpos	as 'Cliente_Latitud'
		,tcd.lonpos	'Cliente_Longitud'
		,tpd.desc_plaza as 'Cliente_Localidad'
		,tcd.cp	as 'Cliente_RefDomicilioExterno'
		,tcd.nombre	as 'Cliente_DomicilioDescripcion'
		,'60' as 'Cliente_TiempoEspera'
		,tcd.contacto1 as 'Cliente_Contacto'
		,'false' as 'Cliente_CargaExclusiva'
		,'false' as 'Cliente_IgnorarOperacion' 
		,'true' as 'Cliente_CrearDomicilioOrden'
		,'1' as 'Cliente_ActualizarDomicilioOrden'
		,'1' as 'Cliente_ValidarDomicilioOrden'
		,dp.id_remitente as 'Cliente2_RefCliente'
		,tcr.nombre	as 'Cliente2_RazonSocial'
		,tcr.tel	as 'Cliente2_Telefono'
		,tcr.fax	as 'Cliente2_Telefono2'
		,tcr.email1 as 'Cliente2_Email'
		,tcr.domicilio as 'Cliente2_Direccion'
		,tcr.latpos	as 'Cliente2_Latitud'
		,tcr.lonpos	as 'Cliente2_Longitud'
		,tpo.desc_plaza as 'Cliente2_Localidad'
		,tcr.cp	as 'Cliente2_RefDomicilioExterno'
		,tcr.nombre	as 'Cliente2_Descripcion'
		,'60' as 'Cliente2_TiempoEspera'
		,tcr.contacto1	as 'Cliente2_Contacto'
		,'false' as 'Cliente2_CargaExclusiva'
		,'false' as 'Cliente2_IgnorarOperacion' 
		,'true' as 'Cliente2_CrearDomicilioOrden'
		,'1' as 'Cliente2_ActualizarDomicilioOrden'
		,'1' as 'Cliente2_ValidarDomicilioOrden'
		,dp.id_cliente as 'Pedido_Varchar1'
		,tcc.nombre	as 'Pedido_Varchar2'
		,tcc.domicilio	as 'Pedido_Varchar3'
		,'MEXICO' as 'Pedido_Varchar4'
		,tcc.cp	as 'Pedido_Varchar5'
		,tg.num_guia as 'Pedido_Varchar6'
		,dp.id_area	as 'Pedido_CodigoSucursal'
		,'0' as 'Pedido_TipoPedido'
		,'900' as 'Pedido_InicioHorario'
		,'1800' as 'Pedido_FinHorario1'
		,'60' as 'Pedido_TiempoEspera'
		,'1' as 'Pedido_Categoria'
		,'1' as 'Pedido_Prioridad'
--		,'false' as 'Pedido_CargaExclusiva'
		,dp.id_area	as 'depositoSalida_RefDepositoExterno'
		,dp.id_area	as 'depositoLlegada_RefDepositoExterno'
		,'false' as 'PedidoItem_usarProductos'
--		,dpp.descripcion_producto	as 'Items_Descripcion'
--		,CASE (dpp.embalaje) WHEN '0' THEN 'SIN ASIGNAR' WHEN '1' THEN 'TARIMAS' WHEN '2' THEN 'BULTOS' WHEN '3' THEN 'CARTONES' WHEN '4' THEN 'ROLLOS' WHEN '5' THEN 'PACAS'
--		WHEN '6' THEN 'PAPELERIA' WHEN '7' THEN 'CAMIONETAS' WHEN '8' THEN 'SACOS' WHEN '9' THEN 'GRANEL' WHEN '10' THEN 'CAJAS' WHEN '11' THEN 'CONTENEDOR' WHEN '12' THEN '_'
--		WHEN '13' THEN 'PALETS' WHEN '14' THEN 'TAMBOS' WHEN '15' THEN 'PZAS' WHEN '16' THEN 'ATADOS' WHEN '17' THEN 'LOTE' WHEN '18' THEN 'PAQUETES' WHEN '99' THEN 'OTROS'
--		END as 'Items_RefDocumento'
--		,dpp.cantidad	as 'Items_Cantidad'
--		,'0' as 'Items_Volumen'
--		,isnull (dpp.peso / NULLIF(dpp.cantidad,0),0)	as 'Items_Peso'
--		,'0' as 'Items_Bulto'
--		,'0' as 'Items_Pallets'
		,'false' as 'Pedido_agruparItems'
		,'1' as 'Pedido_CodigoOperacion'
		,'1' as 'Pedido2_CodigoSucursal'
		,'1' as 'Pedido_Unidades'
		,'true' as 'Pedido_usarProductos'
		,'false' as 'Pedido_soloInsertarProductos'
		,'false' as 'Pedido2_agruparItems'
		,tg.flete	as 'Pedido_ValorDeclarado'
		,'0' as 'Pedido_Float1'
		,'0' as 'Pedido_Float2'
		,cast(dp.id_pedidopk as nvarchar(25)) collate SQL_Latin1_General_CP1_CI_AS	as 'PedidoPk_RefDocumento'
		from
		pruebasdb2019.dbo.desp_pedido dp   --Tabla a monitorear
--		left join
--		pruebasdb2019.dbo.desp_pedido_productos dpp on dp.id_pedidopk = dpp.id_pedidopk
		left join 
		pruebasdb2019.dbo.trafico_cliente tcc on dp.id_cliente = tcc.id_cliente 
		left join
		pruebasdb2019.dbo.trafico_cliente tcr on dp.id_remitente = tcr.id_cliente 
		left join
		pruebasdb2019.dbo.trafico_cliente tcd on dp.id_destinatario = tcd.id_cliente
		left join
		pruebasdb2019.dbo.trafico_plaza tpo on dp.id_origen = tpo.id_plaza 
		left join
		pruebasdb2019.dbo.trafico_plaza tpd on dp.id_destino = tpd.id_plaza 
		left join
		pruebasdb2019.dbo.trafico_guia tg on dp.no_guia = tg.no_guia and dp.id_area = tg.id_area
		where 
		dp.sist_origen = 'zam'

--	and 
--			dp.num_pedido = 458	
--		and
--		dp.f_pedido between '2020-01-05 00:00:00.000' and '2020-15-05 23:59:59.000'
--		and
--		dp.id_area = 8



	-- exec sp_columns 'ws_view_pedido_actions'
