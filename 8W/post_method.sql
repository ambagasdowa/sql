exec sp_helpserver

--The following example shows how to view the current setting of OLE Automation procedures.

EXEC sp_configure 'Ole Automation Procedures'; 

--check server /

SELECT CONVERT( sysname, SERVERPROPERTY('INTEGRABD')); 
SELECT CONVERT( sysname, SERVERPROPERTY('local')); 

--proccess to activate  'Ole Automation Procedures'

use sistemas

sp_configure 'show advanced options', 1;  

RECONFIGURE;  
  
sp_configure 'Ole Automation Procedures', 1;  

RECONFIGURE;  
  

-- ================================================================================================== --


--create/alter a stored proceudre  accordingly
create procedure webRequest
as 
DECLARE @authHeader NVARCHAR(64);
DECLARE @contentType NVARCHAR(64);
DECLARE @postData NVARCHAR(2000);
DECLARE @responseText NVARCHAR(2000);
DECLARE @responseXML NVARCHAR(2000);
DECLARE @ret INT;
DECLARE @status NVARCHAR(32);
DECLARE @statusText NVARCHAR(32);
DECLARE @token INT;
DECLARE @url NVARCHAR(256);
DECLARE @Authorization NVARCHAR(200);

--set your post params
SET @authHeader = 'BASIC 0123456789ABCDEF0123456789ABCDEF';
SET @contentType = 'application/x-www-form-urlencoded';
SET @postData = 'KeyValue1=value1&KeyValue2=value2'
SET @url = 'set your url end point here'

-- Open the connection.
EXEC @ret = sp_OACreate 'MSXML2.ServerXMLHTTP', @token OUT;
IF @ret <> 0 RAISERROR('Unable to open HTTP connection.', 10, 1);

-- Send the request.
EXEC @ret = sp_OAMethod @token, 'open', NULL, 'POST', @url, 'false';
--set a custom header Authorization is the header key and VALUE is the value in the header
EXEC sp_OAMethod @token, 'SetRequestHeader', NULL, 'Authorization', 'VALUE'

EXEC @ret = sp_OAMethod @token, 'setRequestHeader', NULL, 'Authentication', @authHeader;
EXEC @ret = sp_OAMethod @token, 'setRequestHeader', NULL, 'Content-type', @contentType;
EXEC @ret = sp_OAMethod @token, 'send', NULL, @postData;

-- Handle the response.
EXEC @ret = sp_OAGetProperty @token, 'status', @status OUT;
EXEC @ret = sp_OAGetProperty @token, 'statusText', @statusText OUT;
EXEC @ret = sp_OAGetProperty @token, 'responseText', @responseText OUT;

-- Show the response.
PRINT 'Status: ' + @status + ' (' + @statusText + ')';
PRINT 'Response text: ' + @responseText;

-- Close the connection.
EXEC @ret = sp_OADestroy @token;
IF @ret <> 0 RAISERROR('Unable to close HTTP connection.', 10, 1);



-- =================================================================================================== --
-- Working Example for post data in json format
-- =================================================================================================== --

DECLARE @Object AS INT;
DECLARE @ResponseText AS VARCHAR(8000);
DECLARE @Body AS NVARCHAR(4000) = 
'{
    "name": "DulaXHB"
}'  

EXEC sp_OACreate 'MSXML2.ServerXMLHTTP', @Object OUT;
EXEC sp_OAMethod @Object, 'open', NULL, 'post','https://hookb.in/wNa0NNKPqocLQYzYDnng', 'false'

EXEC sp_OAMethod @Object, 'setRequestHeader', null, 'Content-Type', 'application/json'

EXEC sp_OAMethod @Object, 'send', null, @Body

EXEC sp_OAMethod @Object, 'responseText', @ResponseText OUTPUT
SELECT @ResponseText

EXEC sp_OADestroy @Object



-- =================================================================================================== --
-- Working Example for post data in json format
-- =================================================================================================== --

--api carving
DECLARE @Object AS INT;
DECLARE @ResponseText AS VARCHAR(8000);
DECLARE @Body AS NVARCHAR(4000) = ''
EXEC sp_OACreate 'MSXML2.ServerXMLHTTP', @Object OUT;
EXEC sp_OAMethod @Object, 'open', NULL, 'get','https://cloudmx.unigis.com/8W/mapi/soap/logistic/Service.asmx', 'false'
EXEC sp_OAMethod @Object, 'setRequestHeader', null, 'Content-Type', 'text/xml; charset=utf-8'
--EXEC sp_OAMethod @Object, 'setRequestHeader', null, 'SOAPAction', 'http://unisolutions.com.ar/ConsultarViajePorFechas'
--EXEC sp_OAMethod @Object, 'setRequestHeader', null, 'Host', 'cloudmx.unigis.com'
EXEC sp_OAMethod @Object, 'send', null, @Body
EXEC sp_OAMethod @Object, 'responseText', @ResponseText output

SELECT @ResponseText

EXEC sp_OADestroy @Object



DECLARE @Object AS INT;
DECLARE @ResponseText AS VARCHAR(8000);
DECLARE @Body AS NVARCHAR(4000) = 
'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:unis="http://unisolutions.com.ar/">
   <soapenv:Header/>
   <soapenv:Body>
      <unis:ConsultarViajePorFechas>
         <!--Optional:-->
         <!--<unis:ApiKey>?</unis:ApiKey>-->
         <unis:FechaJornadaDesde>2019-01-10</unis:FechaJornadaDesde>
         <unis:FechaJornadaHasta>2020-06-10</unis:FechaJornadaHasta>
      </unis:ConsultarViajePorFechas>
   </soapenv:Body>
</soapenv:Envelope>';


EXEC sp_OACreate 'MSXML2.ServerXMLHTTP', @Object OUT;
EXEC sp_OAMethod @Object, 'open', NULL, 'post','https://cloudmx.unigis.com/8W/mapi/soap/logistic/Service.asmx', 'false';

EXEC sp_OAMethod @Object, 'setRequestHeader', null, 'Content-Type', 'text/xml; charset=utf-8';
EXEC sp_OAMethod @Object, 'setRequestHeader', null, 'SOAPAction', 'http://unisolutions.com.ar/ConsultarViajePorFechas';
EXEC sp_OAMethod @Object, 'setRequestHeader', null, 'Host', 'cloudmx.unigis.com';

EXEC sp_OAMethod @Object, 'send', null, @Body;

EXEC sp_OAMethod @Object, 'responseText', @ResponseText output;

SELECT @ResponseText;
EXEC sp_OADestroy @Object;


-- =================================================================================================== --
-- Working Example for post data in xml format
-- =================================================================================================== --

DECLARE @Object AS INT;
DECLARE @ResponseText AS VARCHAR(8000);
DECLARE @Body AS NVARCHAR(4000) = 
'{
    "name": "DulaXHB"
}'  

EXEC sp_OACreate 'MSXML2.ServerXMLHTTP', @Object OUT;
EXEC sp_OAMethod @Object, 'open', NULL, 'post','https://hookb.in/wNa0NNKPqocLQYzYDnng', 'false'

EXEC sp_OAMethod @Object, 'setRequestHeader', null, 'Content-Type', 'application/xml'

EXEC sp_OAMethod @Object, 'send', null, @Body

EXEC sp_OAMethod @Object, 'responseText', @ResponseText OUTPUT
SELECT @ResponseText

EXEC sp_OADestroy @Object



--examples

declare @st as nvarchar(4000)

set @st = '{"json":"name"}';

select @st

-- ==================================================================================================== --

-- ==================================================================================================== --
DECLARE @status int
DECLARE @responseText as table(responseText nvarchar(max))
DECLARE @res as Int;
DECLARE @url as nvarchar(1000) = 'https://cloudmx.unigis.com/8W/mapi/soap/logistic/Service.asmx'

EXEC sp_OACreate 'MSXML2.ServerXMLHTTP', @res OUT
EXEC sp_OAMethod @res, 'open', NULL, 'GET',@url,'false'
EXEC sp_OAMethod @res, 'send'
EXEC sp_OAGetProperty @res, 'status', @status OUT
INSERT INTO @ResponseText (ResponseText) EXEC sp_OAGetProperty @res, 'responseText'
EXEC sp_OADestroy @res

SELECT @status, responseText FROM @responseText
-- ==================================================================================================== --

DECLARE @authHeader NVARCHAR(64);
DECLARE @contentType NVARCHAR(164);
DECLARE @postData NVARCHAR(2000);
--DECLARE @responseText nvarchar(max);
DECLARE @responseText as table(responseText nvarchar(max))
DECLARE @responseXML NVARCHAR(2000);
DECLARE @ret INT;
--DECLARE @status NVARCHAR(32);
DECLARE @status int
DECLARE @statusText NVARCHAR(32);
DECLARE @token INT;
DECLARE @url NVARCHAR(256);
declare @soapAction nvarchar(800)
declare @userAgent nvarchar(800)
declare @cacheControl nvarchar(256)
declare @Host nvarchar(255)
declare @Conn nvarchar(255)
declare @AcceptEncoding nvarchar(255)
declare @lenght nvarchar(255)

SET @authHeader = 'BASIC 0123456789ABCDEF0123456789ABCDEF';
--SET @authHeader = null;
--SET @contentType = 'application/x-www-form-urlencoded';
--SET @contentType = 'text/xml;charset=UTF-8';
--set @ContentType = 'application/soap+xml; charset=utf-8';
set @ContentType = 'text/xml; charset=utf-8';
--set @ContentType = 'application/json';
--set @ContentType = 'text/xml';
set @lenght = '479'
set @AcceptEncoding = 'gzip, deflate';
SET @postData = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:unis="http://unisolutions.com.ar/">
   <soapenv:Header/>
   <soapenv:Body>
      <unis:ConsultarViajePorFechas>
         <!--Optional:-->
         <!--<unis:ApiKey>?</unis:ApiKey>-->
         <unis:FechaJornadaDesde>2019-01-10</unis:FechaJornadaDesde>
         <unis:FechaJornadaHasta>2020-06-10</unis:FechaJornadaHasta>
      </unis:ConsultarViajePorFechas>
   </soapenv:Body>
</soapenv:Envelope>';

set @soapAction = 'http://unisolutions.com.ar/ConsultarViajePorFechas'
set @userAgent = 'Apache-HttpClient/4.1.1 (java 1.5)' 
set @cacheControl = 'no-cache'
set @Host = 'cloudmx.unigis.com'
set @Conn = 'Keep-Alive'

--SET @url = 'https://en43ylz3txlaz.x.pipedream.net';
set @url = 'https://cloudmx.unigis.com/8W/mapi/soap/logistic/Service.asmx';
-- Open the connection.
EXEC @ret = sp_OACreate 'MSXML2.ServerXMLHTTP', @token OUT;
IF @ret <> 0 RAISERROR('Unable to open HTTP connection.', 10, 1);
-- Send the request.
--EXEC @ret = sp_OAMethod @token, 'open', NULL, 'POST', @url, 'false';
EXEC @ret = sp_OAMethod @token, 'open', NULL, 'POST', @url, 'false';
--EXEC @ret = sp_OAMethod @token, 'setRequestHeader', NULL, 'Authentication', @authHeader;
EXEC @ret = sp_OAMethod @token, 'setRequestHeader', NULL, 'Content-type', @contentType;
EXEC @ret = sp_OAMethod @token, 'setRequestHeader', NULL, 'SOAPAction', @soapAction;
--EXEC @ret = sp_OAMethod @token, 'setRequestHeader', NULL, 'User-Agent', @userAgent;
--EXEC @ret = sp_OAMethod @token, 'setRequestHeader', NULL, 'cache-control', @cacheControl;
EXEC @ret = sp_OAMethod @token, 'setRequestHeader', NULL, 'Host', @Host;
--EXEC @ret = sp_OAMethod @token, 'setRequestHeader', NULL, 'Connection', @Conn;
--EXEC @ret = sp_OAMethod @token, 'setRequestHeader', NULL, 'Accept-Encoding', @AcceptEncoding;
--EXEC @ret = sp_OAMethod @token, 'setRequestHeader', NULL, 'Content-Length', @lenght;
EXEC @ret = sp_OAMethod @token, 'send', NULL, @postData;
-- Handle the response.
EXEC @ret = sp_OAGetProperty @token, 'status', @status OUT;
EXEC @ret = sp_OAGetProperty @token, 'statusText', @statusText OUT;
--EXEC @ret = sp_OAGetProperty @token, 'responseText', @responseText OUT;
INSERT INTO @ResponseText (ResponseText) EXEC sp_OAGetProperty @token, 'responseText', @responseText OUT
-- Show the response.
select 'Status: ' + cast(@status as char(2)) + ' (' + @statusText + ')';

SELECT @status, responseText FROM @responseText
-- Close the connection.
EXEC @ret = sp_OADestroy @token;
IF @ret <> 0 RAISERROR('Unable to close HTTP connection.', 10, 1);
--SELECT @status, responseText FROM @responseText


-- ==================================================================================================== --
--	NOTE This is the script working for 8W post request
-- ==================================================================================================== --

DECLARE @status int
DECLARE @responseText as table(responseText nvarchar(max))
DECLARE @res as Int;
DECLARE @url as nvarchar(1000) = 'https://cloudmx.unigis.com/8W/mapi/soap/logistic/Service.asmx'

declare @contentType NVARCHAR(164);
declare @postData NVARCHAR(2000);
declare @soapAction nvarchar(800)
declare @Host nvarchar(255)

set @ContentType = 'text/xml; charset=utf-8';
SET @postData = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:unis="http://unisolutions.com.ar/">
   <soapenv:Header/>
   <soapenv:Body>
      <unis:ConsultarViajePorFechas>
         <!--Optional:-->
         <!--<unis:ApiKey>?</unis:ApiKey>-->
         <unis:FechaJornadaDesde>2019-01-10</unis:FechaJornadaDesde>
         <unis:FechaJornadaHasta>2020-06-10</unis:FechaJornadaHasta>
      </unis:ConsultarViajePorFechas>
   </soapenv:Body>
</soapenv:Envelope>';
set @soapAction = 'http://unisolutions.com.ar/ConsultarViajePorFechas'
set @Host = 'cloudmx.unigis.com'

EXEC sp_OACreate 'MSXML2.ServerXMLHTTP', @res OUT
EXEC sp_OAMethod @res, 'open', NULL, 'POST',@url,'false'

EXEC sp_OAMethod @res, 'setRequestHeader', NULL, 'Content-type', @contentType;
EXEC sp_OAMethod @res, 'setRequestHeader', NULL, 'SOAPAction', @soapAction;
EXEC sp_OAMethod @res, 'setRequestHeader', NULL, 'Host', @Host;

--EXEC sp_OAMethod @res, 'send'
EXEC sp_OAMethod @res, 'send', NULL, @postData;
EXEC sp_OAGetProperty @res, 'status', @status out

INSERT INTO @ResponseText (ResponseText) EXEC sp_OAGetProperty @res, 'responseText'
EXEC sp_OADestroy @res

SELECT @status, responseText FROM @responseText



-- ==================================================================================================== --
DECLARE @XMLResponse xml
DECLARE @obj INT
DECLARE @ValorDeRegreso INT
DECLARE @sUrl NVARCHAR(200)
DECLARE @response VARCHAR(MAX)
DECLARE @hr INT
DECLARE @src NVARCHAR(255)
DECLARE @desc NVARCHAR(255)
DECLARE @StringRequest NVARCHAR(500)
SET @StringRequest = 'searchstring=V34432221'
SET @sUrl = 'https://cloudmx.unigis.com/8W/mapi/soap/logistic/Service.asmx'
EXEC sp_OACreate 'MSXML2.ServerXMLHttp', @obj OUT
EXEC sp_OAMethod @obj, 'Open', NULL, 'POST', @sUrl, false
EXEC sp_OAMethod @obj, 'setRequestHeader', NULL, 'Content-Type', 'application/x-www-form-urlencoded'
--EXEC sp_OAMethod @obj, 'setRequestHeader', NULL, 'Content-Type', 'text/xml;charset=UTF-8'
--EXEC sp_OAMethod @obj, 'send', NULL, @StringRequest
EXEC sp_OAGetProperty @obj, 'responseText', @XMLResponse out
select @XMLResponse

--select @XMLResponse
--process @XMLResponse...























