-- ==================================================================================================== --
-- Analisis ? EightW Method Pedido
-- ==================================================================================================== --

select
	 cs.id_solicitud                 -- CrearOrdenesPedido:Pedido.ReferenciaExterna
	,cs.num_solicitud				 -- Cliente.Direccion
	,cs.fecha_ingreso				 -- Pedido.Fecha
	,cs.fecha_recoleccion_prog		 -- Pedido.FechaEntrega
	,cs.fecha_entrega_prog			 -- Pedido.FechaEntregaOriginal
	,cs.id_remitente				 -- ClienteOrden.RefClienteExterno
	,cs.remitente_nombre			 -- clienteOrden.RazonSocial
	,tcr.tel						 -- DomicilioOrden.Telefono
	,tcr.fax						 -- DomicilioOrden.Telefono2
	,tcr.email1						 -- DomicilioOrden.Email
	,tcr.domicilio					 --	DomicilioOrden.Direccion
	,tcr.latpos						 -- DomicilioOrden.Latitud
	,tcr.lonpos						 -- DomicilioOrden.Longitud
	,tcr.cp							 -- DomicililoOrden.RefDomicilioExterno
	,tcr.nombre						 -- DomicilioOrden.Descripcion
	, '900' as 'DomicilioOrden.InicioHorario'
	, '900' as 'DomicilioOrden.InicioHorario2'
	, '1800' as 'DomicilioOrden.FinHorario1'
	, '1800' as 'DomicilioOrden.FinHorario2'
	, '60' as 'DomicilioOrden.TiempoEspera'
	, '0' as 'DomicilioOrden.Varchar1'
	, '0' as 'DomicilioOrden.Varchar2'
	,tcr.contacto1					 -- DomiciliOrden.Contacto
	,cs.id_destinatario				 -- ClienteOrden.RefClienteExterno
	,cs.destinatario_nombre			 -- clienteOrden.RazonSocial
	,tcd.tel						 --	DomicilioOrden.Telefono
	,tcd.fax						 -- DomicilioOrden.Telefono2
	,tcd.email1						 -- DomicilioOrden.Email
	,tcd.domicilio					 -- DomicilioOrden.Direccion
	,tcd.latpos						 -- DomicilioOrden.Latitud
	,tcd.lonpos						 -- DomicilioOrden.Longitud
	,tcd.cp							 -- DomicililoOrden.RefDomicilioExterno
	,tcd.nombre						 -- DomicilioOrden.Descripcion
	, '900' as 'DomicilioOrden.InicioHorario'
	, '900' as 'DomicilioOrden.InicioHorario2'
	, '1800' as 'DomicilioOrden.FinHorario1'
	, '1800' as 'DomicilioOrden.FinHorario2'
	, '60' as 'DomicilioOrden.TiempoEspera'
	, '0' as 'DomicilioOrden.Varchar1'
	, '0' as 'DomicilioOrden.Varchar2'
	,tcd.contacto1					 -- DomiciliOrden.Contacto
	,tcc.tel						 -- Pedido.Telefono
	,tcc.fax						 -- Pedido.Telefono2
	,cs.clientepaga_nombre			 --	Pedido.Descripcion
	,cs.id_area						 -- Sucursal.ReferenciaExterna
	,cs.recoleccion					 -- TipoPedido.ReferenciaExterna
	,cs.id_clientepaga				 -- Cliente.ReferenciaExterna
	,cs.clientepaga_nombre			 -- Cliente.RazonSocial
	,tcc.tel						 -- Cliente.Telefono
	,tcc.fax						 -- Cliente.Telefono2
	,tcc.email1						 -- Cliente.eMailGestordeFlota
	,tcc.domicilio					 -- Cliente.Direccion
	, 'MEXICO' as 'DomicilioOrden.Pais'
	,tcc.latpos						 -- DomicilioOrden.Latitud
	,tcc.lonpos						 -- DomicilioOrden.Longitud
	,tcc.cp							 -- DomicilioOrden.RefDomicilioExterno
	,tcc.nombre						 -- DomicilioOrden.Descripcion
	, '900' as 'DomicilioOrden.InicioHorario'
	, '900' as 'DomicilioOrden.InicioHorario2'
	, '1800' as 'DomicilioOrden.FinHorario1'
	, '1800' as 'DomicilioOrden.FinHorario2'
	, '60' as 'DomicilioOrden.TiempoEspera'
	, '0' as 'DomicilioOrden.Varchar1'
	, '0' as 'DomicilioOrden.Varchar2'
	,tcc.contacto1					 -- DomicilioOden.Contacto
	, '0' as 'DomiciilioOrden.Float1'
	, '0' as 'DomiciilioOrden.Float2'
	, '5' as 'sie_ctrl'
	, '1' as 'Pedido.Prioridad'
	, 'false' as 'Pedido.CargaExclusiva'
	,cs.id_plazaorigen				 -- Deposito.RefdepositoExterno
	,cs.id_plazadestino				 -- Deposito.RefdepositoExterno
	,tcc.cp							 -- Pedido.CodigoPostal
	,cs.id_sucursalposicion			 -- Operaci�n.RefereciaExterna
	,csd.id_producto				 -- PedidoItem.RefDocumento
	,csd.embalaje					 -- PedidoItem.RefDocumentoAdicional
	,csd.cantidad					 -- PedidoItem.Cantidad
	,csd.peso_capturado				 -- PedidoItem.Peso
	, '0' as 'PedidoItem.Bulto'
	, '0' as 'PedidoItem.Pallets'
	, 'false' as 'PARAMETRO DEL WS'
	, 'false' as 'PARAMETRO DEL WS'
	, 'false' as 'PARAMETRO DEL WS'
	,cf.flete						 -- Pedido.ValorDeclarado
	, '0' as 'Pedido.Float1'
	, '0' as 'Pedido.Float2'
from
	pruebasdb2019.dbo.cons_solicitud cs
left join
	pruebasdb2019.dbo.cons_solicituddet csd on cs.id_solicitud = csd.id_solicitud
left join
	pruebasdb2019.dbo.trafico_cliente tcc on cs.id_clientepaga = tcc.id_cliente
left join
	pruebasdb2019.dbo.trafico_cliente tcr on cs.id_remitente = tcr.id_cliente
left join
	pruebasdb2019.dbo.trafico_cliente tcd on cs.id_destinatario = tcd.id_cliente
left join
	pruebasdb2019.dbo.cons_factura cf on cs.id_solicitud = cf.id_solicitud and cs.id_area_pedido = cf.id_areacobranza
where
	cs.id_solicitud = 243171
