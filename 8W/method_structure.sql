/*===============================================================================
 Author         : Jesus Baizabal
 email			    : ambagasdowa@gmail.com
 Create date    : October 10, 2020
 Description    : Store for send data over post or get method   
 @license       : MIT License (http://www.opensource.org/licenses/mit-license.php)
 Database owner : Elephantom Inc
 @status        : Stable
 @version				: 0.00.01
 ===============================================================================*/

create database ws 


-- NOTE build name of the table into 8w datasets
-- Build wrking tables 

use [ws]

IF OBJECT_ID('ws.dbo.ws_post_config_clients', 'U') IS NOT NULL 
  DROP TABLE ws.dbo.ws_post_config_clients; 

set ansi_nulls on
set quoted_identifier on
set ansi_padding on
create table [ws].[dbo].[ws_post_config_clients](
		id												int identity(1,1),
		user_id										int null,
		client										text null,
		description               text null,
		created										datetime default current_timestamp,
		modified									datetime default current_timestamp,
		_status										tinyint default 1 null
--		,constraint ak_user_id unique(user_id)
) on [primary]
set ansi_padding off

insert into ws.dbo.ws_post_config_clients values (1,'8w','xxxxxx',current_timestamp,current_timestamp,1);

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Controls ProvidersControlsUsers
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
use [ws]
go
IF OBJECT_ID('dbo.ws_send_post_params', 'U') IS NOT NULL 
  DROP TABLE dbo.ws_send_post_params; 

set ansi_nulls on
set quoted_identifier on
set ansi_padding on
create table [ws].[dbo].[ws_send_post_params](
		id												int identity(1,1),
		user_id										int null,
		ws_post_config_clients_id int,
		description               text null,
		url												nvarchar(1000) null,
	  host								      nvarchar(1000) null,
		soap                      nvarchar(999) null,
		created										datetime default current_timestamp,
		modified									datetime default current_timestamp,
		_status										tinyint default 1 null
--		,constraint ak_user_id unique(user_id)
) on [primary]
set ansi_padding off

insert into ws.dbo.ws_send_post_params values(
			 1                                                                   -- user_id
			,1                                                                   -- ws_post_config_clients_id
			,'General Config Params Conections between 8W and Unigis'            -- description
			,'https://cloudmx.unigis.com/8W/mapi/soap/logistic/Service.asmx'     -- url
			,'cloudmx.unigis.com'                                                -- host
			,'http://unisolutions.com.ar/'                                       -- soap
			,current_timestamp
			,current_timestamp
			,1
		);


use [ws]

IF OBJECT_ID('dbo.ws_post_actions', 'U') IS NOT NULL 
  DROP TABLE dbo.ws_post_actions; 
-- truncate table ws_post_actions
set ansi_nulls on
set quoted_identifier on
set ansi_padding on
create table [ws].[dbo].[ws_post_actions](
		id												int identity(1,1),
		send_post_params_id				int null,
		action                    nvarchar(800) not null,
		postdata									nvarchar(max) not null,
		parameters                nvarchar(max) not null,
		request										nvarchar(max) not null,
		created										datetime default current_timestamp,
		modified									datetime default current_timestamp,
		_status										tinyint default 1 null
--		,constraint ak_user_id unique(user_id)
) on [primary]
set ansi_padding off

insert into ws.dbo.ws_post_actions values
							  (
											1
											,'ConsultarViajePorFechas'
											,'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:unis="http://unisolutions.com.ar/">
                           <soapenv:Header/>
                           <soapenv:Body>
                              <unis:ConsultarViajePorFechas>
                                 <!--optional:-->
                                 <!--<unis:apikey>?</unis:apikey>-->
                                 <unis:FechaJornadaDesde>2019-01-10</unis:FechaJornadaDesde>
                                 <unis:FechaJornadaHasta>2020-06-10</unis:FechaJornadaHasta>
                              </unis:ConsultarViajePorFechas>
                           </soapenv:Body>
                         </soapenv:Envelope>'
                      ,'@apikey nvarchar(10) , @referenciaexterna nvarchar(8) , @postdata xml output'
											,'insert into ws.dbo.ws_responses 
													select @status, @action ,@responseText,''with ReferenciaExterna : '' +  @referenciaexterna,current_timestamp,current_timestamp,1'
											,current_timestamp,current_timestamp,1
								)
							 ,(
											1
											,'ConsultarOrdenPedido'
											,'<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"> 
                        <soap:Body>
                        	<ConsultarOrdenPedido xmlns="http://unisolutions.com.ar/">
                        		<ApiKey>????</ApiKey>
                        		<ReferenciaExterna>??????</ReferenciaExterna>
                        	</ConsultarOrdenPedido>
                        </soap:Body>
                      </soap:Envelope>'
											,'@apikey nvarchar(10) , @referenciaexterna nvarchar(10) , @postdata xml output'
											,'insert into ws.dbo.ws_responses 
													select @status, @action ,@responseText,''with ReferenciaExterna : '' +  @referenciaexterna,current_timestamp,current_timestamp,1'
											,current_timestamp,current_timestamp,1
								)
							 ,(
											1
											,'CrearOrdenesPedido'
											,'
												<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:unis="http://unisolutions.com.ar/">
													 <soapenv:Header/>
													 <soapenv:Body>
															<unis:CrearOrdenesPedido>
																 <unis:apiKey>???????</unis:apiKey>
																 <unis:pedidos>
																		<unis:pOrdenPedido>
																			 <unis:RefDocumento>??????</unis:RefDocumento>
																			 <unis:RefDocumentoAdicional>???????</unis:RefDocumentoAdicional>
																			 <unis:Descripcion>????????</unis:Descripcion>
																			 <unis:Fecha>?????-??-??</unis:Fecha>
																			 <unis:FechaEntrega>????-??-??</unis:FechaEntrega>
																			 <unis:FechaRecoleccion>????-??-??</unis:FechaRecoleccion>
																			 <unis:Cliente>
																					<unis:RefCliente>????</unis:RefCliente>
																					<unis:RazonSocial>?????? ????????</unis:RazonSocial>
																					<unis:Telefono>???????</unis:Telefono>
																					<unis:Telefono2>????</unis:Telefono2>
																					<unis:EMail>?????????</unis:EMail>
																					<unis:Direccion>??????</unis:Direccion>
																					<unis:Latitud>?????????????</unis:Latitud>
																					<unis:Longitud>??????????????????</unis:Longitud>
																					<unis:Localidad>???????????????</unis:Localidad>
																					<unis:RefDomicilioExterno>????????</unis:RefDomicilioExterno>
																					<unis:DomicilioDescripcion>????????</unis:DomicilioDescripcion>
																					<unis:TiempoEspera>?????????</unis:TiempoEspera>
																					<unis:Contacto>??????????</unis:Contacto>
																					<unis:CargaExclusiva>???????</unis:CargaExclusiva>
																					<unis:IgnorarOperacion>??????</unis:IgnorarOperacion>
																					<unis:CrearDomicilioOrden>????????</unis:CrearDomicilioOrden>
																					<unis:ActualizarDomicilioOrden>???????</unis:ActualizarDomicilioOrden>
																					<unis:ValidarDomicilioOrden>???????</unis:ValidarDomicilioOrden>
																			 </unis:Cliente>
																			 <unis:Cliente2>
																					<unis:RefCliente>?????</unis:RefCliente>
																					<unis:RazonSocial>???????</unis:RazonSocial>
																					<unis:Telefono>??? ??-??-??-??</unis:Telefono>
																					<unis:Telefono2>?????</unis:Telefono2>
																					<unis:EMail>????????????????????@????.???;?????????@????.???</unis:EMail>
																					<unis:Direccion>?</unis:Direccion>
																					<unis:Latitud>?.??????????</unis:Latitud>
																					<unis:Longitud>?.??????????????</unis:Longitud>
																					<unis:Localidad>?????????</unis:Localidad>
																					<unis:RefDomicilioExterno>??????</unis:RefDomicilioExterno>
																					<unis:DomicilioDescripcion>?????????</unis:DomicilioDescripcion>
																					<unis:TiempoEspera>??????</unis:TiempoEspera>
																					<unis:Contacto>????????</unis:Contacto>
																					<unis:CargaExclusiva>???</unis:CargaExclusiva>
																					<unis:IgnorarOperacion>????</unis:IgnorarOperacion>
																					<unis:CrearDomicilioOrden>?????</unis:CrearDomicilioOrden>
																					<unis:ActualizarDomicilioOrden>???????</unis:ActualizarDomicilioOrden>
																					<unis:ValidarDomicilioOrden>???????</unis:ValidarDomicilioOrden>
																			 </unis:Cliente2> 		
																			 <unis:Varchar1>???????</unis:Varchar1>
																			 <unis:Varchar2>????????</unis:Varchar2>
																			 <unis:Varchar3>??????</unis:Varchar3>
																			 <unis:Varchar4>????????</unis:Varchar4>
																			 <unis:Varchar5>??????</unis:Varchar5>
																			 <unis:Varchar6>????????</unis:Varchar6>
																			 <unis:CodigoSucursal>???????</unis:CodigoSucursal>
																			 <unis:TipoPedido>??????</unis:TipoPedido>
																			 <unis:InicioHorario1>???</unis:InicioHorario1>
																			 <unis:FinHorario1>????</unis:FinHorario1>
																			 <unis:TiempoEspera>????</unis:TiempoEspera>
																			 <unis:Categoria>????</unis:Categoria>
																			 <unis:Prioridad>????</unis:Prioridad>
																			 <unis:depositoSalida>
																					<unis:RefDepositoExterno>???????</unis:RefDepositoExterno>
																			 </unis:depositoSalida>
																			 <unis:depositoLlegada>
																					<unis:RefDepositoExterno>???????</unis:RefDepositoExterno>
																			 </unis:depositoLlegada>
																			 <unis:usarProductos>???????</unis:usarProductos>
																			 <unis:Items>
																				?
																			 </unis:Items>
																			 <unis:agruparItems>?????</unis:agruparItems>
																			 <unis:CodigoSucursal>?????</unis:CodigoSucursal>
																			 <unis:Unidades>????</unis:Unidades>
																			 <unis:usarProductos>????</unis:usarProductos>
																			 <unis:soloInsertarProductos>?????</unis:soloInsertarProductos>
																			 <unis:agruparItems>?????</unis:agruparItems>
																			 <unis:ValorDeclarado>??????????</unis:ValorDeclarado>
																			 <unis:Float1>????</unis:Float1>
																			 <unis:Float2>????</unis:Float2>
																		</unis:pOrdenPedido>
																 </unis:pedidos>
															</unis:CrearOrdenesPedido>
													 </soapenv:Body>
												</soapenv:Envelope>
											'
											,'
												 @ApiKey																	 nvarchar(120)                             
                        ,@Pedido_RefDocumento											 nvarchar(25)             
                        ,@Pedido_RefDocumentoAdicional						 nvarchar(25)      
                        ,@Pedido_Descripcion											 nvarchar(120)                
                        ,@Pedido_Fecha                             datetime 
                        ,@Pedido_FechaEntrega											 datetime 
                        ,@Pedido_FechaRecoleccion									 datetime 
                        ,@Cliente_RefCliente											 bigint 
                        ,@Cliente_RazonSocial											 nvarchar(120)
                        ,@Cliente_Telefono												 nvarchar(25)
                        ,@Cliente_Telefono2												 nvarchar(25)
                        ,@Cliente_Email														 nvarchar(250)
                        ,@Cliente_Direccion												 nvarchar(240)
                        ,@Cliente_Latitud													 decimal(10,6) 
                        ,@Cliente_Longitud												 decimal(10,6)
                        ,@Cliente_Localidad												 nvarchar(30)
                        ,@Cliente_RefDomicilioExterno							 nvarchar(25) 
                        ,@Cliente_DomicilioDescripcion						 nvarchar(120)
                        ,@Cliente_TiempoEspera										 nvarchar(60)
                        ,@Cliente_Contacto												 nvarchar(80)
                        ,@Cliente_CargaExclusiva									 nvarchar(5)
                        ,@Cliente_IgnorarOperacion								 nvarchar(5)
                        ,@Cliente_CrearDomicilioOrden							 nvarchar(4)
                        ,@Cliente_ActualizarDomicilioOrden				 nvarchar(1)
                        ,@Cliente_ValidarDomicilioOrden						 nvarchar(1)
                        ,@Cliente2_RefCliente											 int 
                        ,@Cliente2_RazonSocial										 nvarchar(120)
                        ,@Cliente2_Telefono												 nvarchar(25)
                        ,@Cliente2_Telefono2											 nvarchar(25)
                        ,@Cliente2_Email													 nvarchar(250)
                        ,@Cliente2_Direccion											 nvarchar(240)
                        ,@Cliente2_Latitud												 decimal(10,6)
                        ,@Cliente2_Longitud												 decimal(10,6)
                        ,@Cliente2_Localidad											 nvarchar(30)
                        ,@Cliente2_RefDomicilioExterno						 nvarchar(25)
                        ,@Cliente2_Descripcion						         nvarchar(120)
                        ,@Cliente2_TiempoEspera										 nvarchar(60)
                        ,@Cliente2_Contacto												 nvarchar(80)
                        ,@Cliente2_CargaExclusiva									 nvarchar(5)
                        ,@Cliente2_IgnorarOperacion								 nvarchar(5)
                        ,@Cliente2_CrearDomicilioOrden						 nvarchar(4)
                        ,@Cliente2_ActualizarDomicilioOrden				 nvarchar(1)
                        ,@Cliente2_ValidarDomicilioOrden					 nvarchar(1)
                        ,@Pedido_Varchar1													 bigint
                        ,@Pedido_Varchar2													 nvarchar(120)
                        ,@Pedido_Varchar3													 nvarchar(240) 
                        ,@Pedido_Varchar4													 nvarchar(6)
                        ,@Pedido_Varchar5													 nvarchar(25)
                        ,@Pedido_Varchar6													 nvarchar(20)
                        ,@Pedido_CodigoSucursal										 int 
                        ,@Pedido_TipoPedido												 nvarchar(1)
                        ,@Pedido_InicioHorario										 nvarchar(3)
                        ,@Pedido_FinHorario1											 nvarchar(4)
                        ,@Pedido_TiempoEspera											 nvarchar(2)
                        ,@Pedido_Categoria												 nvarchar(1)
                        ,@Pedido_Prioridad												 nvarchar(1)
                       -- ,@Pedido_CargaExclusiva										 nvarchar(5)
                        ,@depositoSalida_RefDepositoExterno				 int 
                        ,@depositoLlegada_RefDepositoExterno			 int 
                        ,@PedidoItem_usarProductos								 nvarchar(25)
												,@xmlStrRow																 nvarchar(max)
--                        ,@Items_Descripcion												 nvarchar(60)
--                        ,@Items_RefDocumento											 nvarchar(11)
--                        ,@Items_Cantidad													 decimal(18,6)
--                        ,@Items_Volumen														 nvarchar(1)
--                        ,@Items_Peso															 nvarchar(27)
--                        ,@Items_Bulto															 nvarchar(1)
--                        ,@Items_Pallets														 nvarchar(1)
                        ,@Pedido_agruparItems											 nvarchar(5)
--                        ,@Pedido_CodigoOperacion									 nvarchar(1)
                        ,@Pedido2_CodigoSucursal									 nvarchar(1)
                        ,@Pedido_Unidades													 nvarchar(1)
                        ,@Pedido_usarProductos										 nvarchar(4)
                        ,@Pedido_soloInsertarProductos						 nvarchar(5)
                        ,@Pedido2_agruparItems										 nvarchar(5)
                        ,@Pedido_ValorDeclarado										 decimal(18,6)
                        ,@Pedido_Float1														 varchar(10)
                        ,@Pedido_Float2														 varchar(10)
												,@postdata xml output'
											,'insert into ws.dbo.ws_responses 
													select @status, @action ,@responseText,''text'',current_timestamp,current_timestamp,1 '
											,current_timestamp,current_timestamp,1
								);

--update ws.dbo.ws_post_actions set action = 'CrearOrdenesPedidos' where id = 1

select * from ws.dbo.ws_post_actions
-- Preguntar pago a proveedor Virtualizacion



use [ws]

-- truncate table ws_actions_elements

IF OBJECT_ID('dbo.ws_actions_elements', 'U') IS NOT NULL 
  DROP TABLE dbo.ws_actions_elements; 

set ansi_nulls on
set quoted_identifier on
set ansi_padding on
create table [ws].[dbo].[ws_actions_elements](
		id												int identity(1,1),
		ws_post_actions_id				int,
		action_element            nvarchar(255) null,
	  modify_element            nvarchar(max) null,
		description               text null,
		created										datetime default current_timestamp,
		modified									datetime default current_timestamp,
		_status										tinyint default 1 null
--		,constraint ak_user_id unique(user_id)
) on [primary]
set ansi_padding off

insert into ws.dbo.ws_actions_elements values(
			 2																																																					-- ws_post_actions_id
			,'ApiKey'																																																		-- action_element //name			
			,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
	      declare namespace ns="http://unisolutions.com.ar/";
	      replace value of (/soap:Envelope/soap:Body/ns:ConsultarOrdenPedido/ns:ApiKey/text())[1]
				  with sql:variable("@apikey")'')'																																						-- modify_element //command to modify the values of xml instance
			,'Method : ConsultarOrdernesPedido , element : ApiKey'																											-- description //if necesary
			,current_timestamp
			,current_timestamp
			,1
		)
   ,(
			 2																																																					-- ws_post_actions_id
			,'ReferenciaExterna'																																												-- action_element //name			
			,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
	      declare namespace ns="http://unisolutions.com.ar/";
	      replace value of (/soap:Envelope/soap:Body/ns:ConsultarOrdenPedido/ns:ReferenciaExterna/text())[1]
				  with sql:variable("@referenciaexterna")'')'																																-- modify_element //command to modify the values of xml instance
			,'Method : ConsultarOrdernesPedido , element :ReferenciaExterna'                                            -- description //if necesary
			,current_timestamp
			,current_timestamp
			,1
		)
-- NOTE Module Pedidos
   ,(
		 3																																																					
		,'ApiKey'																																																			
		,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
      declare namespace ns="http://unisolutions.com.ar/";
      replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:apiKey/text())[1]
			  with sql:variable("@ApiKey")'')'																																				
		,'Method : CrearOrdenesPedido, element : ApiKey'																											
		,current_timestamp
		,current_timestamp
		,1
	)
 ,(
		 3																																																					-- ws_post_actions_id
		,'RefDocumento'																																												-- action_element //name			
		,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
      declare namespace ns="http://unisolutions.com.ar/";
      replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:RefDocumento/text())[1]
			  with sql:variable("@Pedido_RefDocumento")'')'																																-- modify_element //command to modify the values of xml instance
		,'Method : CrearOrdenesPedido , element :RefDocumento'                                            -- description //if necesary
		,current_timestamp
		,current_timestamp
		,1
	)
 ,(                                                                                                                                                                               	
  	 3																																																					-- ws_post_actions_id
  	,'RefDocumentoAdicional'																																												-- action_element //name			
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:RefDocumentoAdicional/text())[1]
  			with sql:variable("@Pedido_RefDocumentoAdicional")'')'															
  	,'Method : CrearOrdenesPedido , element :RefDocumentoAdicional'                                            -- description //if necesary
  	,current_timestamp
  	,current_timestamp
  	,1
  	) -- NOTE From Hir 
   ,(                                                                                                                                                                               	
    	 3																																																					-- ws_post_actions_id
    	,'Descripcion'																																												-- action_element //name			
    	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
    	  declare namespace ns="http://unisolutions.com.ar/";
    	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Descripcion/text())[1]
    			with sql:variable("@Pedido_Descripcion")'')'																								-- modify_element //command to modify the values of xml instance
    	,'Method : CrearOrdenesPedido , element Descripcion'                                            -- description //if necesary
    	,current_timestamp
    	,current_timestamp
    	,1
	)
	,(                                                                                                                                                                               	
  	 3																																																					-- ws_post_actions_id
  	,'Fecha'																																												-- action_element //name			
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Fecha/text())[1]
  			with sql:variable("@Pedido_Fecha")'')'																			-- modify_element //command to modify the values of xml instance
  	,'Method : CrearOrdenesPedido , element Fecha'                                            -- description //if necesary
  	,current_timestamp
  	,current_timestamp
  	,1
		)
  	,(                                                                                                                                                                               	
    	 3																																																					-- ws_post_actions_id
    	,'FechaEntrega'																																												-- action_element //name			
    	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
    	  declare namespace ns="http://unisolutions.com.ar/";
    	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:FechaEntrega/text())[1]
    			with sql:variable("@Pedido_FechaEntrega")'')'														-- modify_element //command to modify the values of xml instance
    	,'Method : CrearOrdenesPedido , element :FechaEntrega'                      -- description //if necesary
    	,current_timestamp
    	,current_timestamp
    	,1
	)
	,(                                                                                                                                                                               	
  	 3																																																					-- ws_post_actions_id
  	,'FechaRecoleccion'																																												-- action_element //name			
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:FechaRecoleccion/text())[1]
  			with sql:variable("@Pedido_FechaRecoleccion")'')'																	-- modify_element //command to modify the values of xml instance
  	,'Method : CrearOrdenesPedido , element :FechaRecoleccion'                            -- description //if necesary
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(-- NOTE CLIENT ONE SECTION
		 3
  	,'RefCliente'																														-- action_element //name			
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Cliente/ns:RefCliente/text())[1]
  			with sql:variable("@Cliente_RefCliente")'')'													-- modify_element //command to modify the values of xml instance
  	,'Method : CrearOrdenesPedido , element :RefCliente'                     -- description //if necesary
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                                                                                                                               	
  	 3																																																					-- ws_post_actions_id
  	,'RazonSocial'																																												-- action_element //name			
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Cliente/ns:RazonSocial/text())[1]
  			with sql:variable("@Cliente_RazonSocial")'')'											-- modify_element //command to modify the values of xml instance
  	,'Method : CrearOrdenesPedido , element :RazonSocial'                 -- description //if necesary
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                                                                                                                               	
  	 3																																																					-- ws_post_actions_id
  	,'Telefono'																																												-- action_element //name			
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Cliente/ns:Telefono/text())[1]
  			with sql:variable("@Cliente_Telefono")'')'																							-- modify_element //command to modify the values of xml instance
  	,'Method : CrearOrdenesPedido , element :RefDocumentoAdicional'                                       -- description //if necesary
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                                                                                                                               	
  	 3																																																					-- ws_post_actions_id
  	,'Telefono2'																																												-- action_element //name			
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Cliente/ns:Telefono2/text())[1]
  			with sql:variable("@Cliente_Telefono2")'')'															-- modify_element //command to modify the values of xml instance
  	,'Method : CrearOrdenesPedido , element :Telefono2'                                            -- description //if necesary
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                                                                                                                               	
  	 3
  	,'Cliente_Email'	
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Cliente/ns:EMail/text())[1]
				with sql:variable("@Cliente_Email")'')'
	  ,'Method : CrearOrdenesPedido , element :Cliente_Email'
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                                                                                                                               	
  	 3																																																					-- ws_post_actions_id
  	,'Direccion'																																												-- action_element //name			
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Cliente/ns:Direccion/text())[1]
  			with sql:variable("@Cliente_Direccion")'')'													
  	,'Method : CrearOrdenesPedido , element :Direccion'                                            -- description //if necesary
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                                                                                                                               	
  	 3																																																					-- ws_post_actions_id
  	,'Latitud'																																												-- action_element //name			
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Cliente/ns:Latitud/text())[1]
  			with sql:variable("@Cliente_Latitud")'')'															-- modify_element //command to modify the values of xml instance
  	,'Method : CrearOrdenesPedido , element Latitud'                                            -- description //if necesary
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                                                                                                                               	
  	 3																																																					-- ws_post_actions_id
  	,'Cliente_Longitud'																																												-- action_element //name			
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Cliente/ns:Longitud/text())[1]
  			with sql:variable("@Cliente_Longitud")'')'														-- modify_element //command to modify the values of xml instance
  	,'Method : CrearOrdenesPedido , element :Longitud'                                            -- description //if necesary
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                                                                                                                               	
  	 3																																																					-- ws_post_actions_id
  	,'Cliente_Localidad'																																												-- action_element //name			
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Cliente/ns:Localidad/text())[1]
  			with sql:variable("@Cliente_Localidad")'')'															-- modify_element //command to modify the values of xml instance
  	,'Method : CrearOrdenesPedido , element : Cliente_Localidad'                                            -- description //if necesary
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                                                                                                                               	
  	 3																																																					-- ws_post_actions_id
  	,'Cliente_RefDomicilioExterno'																																												-- action_element //name			
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Cliente/ns:RefDomicilioExterno/text())[1]
  			with sql:variable("@Cliente_RefDomicilioExterno")'')'								
  	,'Method : CrearOrdenesPedido , element : Cliente_RefDomicilioExterno'                                            -- description //if necesary
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                                                                                                                               	
  	 3																																																					-- ws_post_actions_id
  	,'Cliente_DomicilioDescripcion'																																												-- action_element //name			
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Cliente/ns:DomicilioDescripcion/text())[1]
  			with sql:variable("@Cliente_DomicilioDescripcion")'')'
  	,'Method : CrearOrdenesPedido , element : Cliente_DomicilioDescripcion'
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                                                                                                                               	
  	 3																																																					-- ws_post_actions_id
  	,'Cliente_TiempoEspera'																																											-- action_element //name			
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Cliente/ns:TiempoEspera/text())[1]
  			with sql:variable("@Cliente_TiempoEspera")'')'																			-- modify_element //command to modify the values of xml instance
  	,'Method : CrearOrdenesPedido , element : Cliente_TiempoEspera'                         -- description //if necesary
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                                                                                                                               	
  	 3																																																					-- ws_post_actions_id
  	,'Cliente_Contacto'																																												-- action_element //name			
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Cliente/ns:Contacto/text())[1]
  			with sql:variable("@Cliente_Contacto")'')'																										-- modify_element //command to modify the values of xml instance
  	,'Method : CrearOrdenesPedido , element : Cliente_Contacto'                                       -- description //if necesary
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                                                                                                                               	
  	 3																																																					-- ws_post_actions_id
  	,'Cliente_CargaExclusiva'																																												-- action_element //name			
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Cliente/ns:CargaExclusiva/text())[1]
  			with sql:variable("@Cliente_CargaExclusiva")'')'			  														-- modify_element //command to modify the values of xml instance
  	,'Method : CrearOrdenesPedido , element : Cliente_CargaExclusiva'                                            -- description //if necesary
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                                                                                                                               	
  	 3																																																					-- ws_post_actions_id
  	,'Cliente_IgnorarOperacion'																																												-- action_element //name			
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Cliente/ns:IgnorarOperacion/text())[1]
  			with sql:variable("@Cliente_IgnorarOperacion")'')'																	-- modify_element //command to modify the values of xml instance
  	,'Method : CrearOrdenesPedido , element : Cliente_IgnorarOperacion'                     -- description //if necesary
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                                                                                                                               	
  	 3																																																					-- ws_post_actions_id
  	,'Cliente_CrearDomicilioOrden'																																												-- action_element //name			
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Cliente/ns:CrearDomicilioOrden/text())[1]
  			with sql:variable("@Cliente_CrearDomicilioOrden")'')'																-- modify_element //command to modify the values of xml instance
  	,'Method : CrearOrdenesPedido , element : Cliente_CrearDomicilioOrden'                  -- description //if necesary
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                                                                                                                               	
  	 3																																																					-- ws_post_actions_id
  	,'Cliente_ActualizarDomicilioOrden'																																												-- action_element //name			
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Cliente/ns:ActualizarDomicilioOrden/text())[1]
  			with sql:variable("@Cliente_ActualizarDomicilioOrden")'')'													-- modify_element //command to modify the values of xml instance
  	,'Method : CrearOrdenesPedido , element : Cliente_ActualizarDomicilioOrden'             -- description //if necesary
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                                                                                                                               	
  	 3																																																					-- ws_post_actions_id
  	,'Cliente_ValidarDomicilioOrden'																																												-- action_element //name			
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Cliente/ns:ValidarDomicilioOrden/text())[1]
  			with sql:variable("@Cliente_ValidarDomicilioOrden")'')'														-- modify_element //command to modify the values of xml instance
  	,'Method : CrearOrdenesPedido , element : Cliente_ValidarDomicilioOrden'             -- description //if necesary
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(  -- NOTE Start CLIENTE2 SECTION                                                                             
  	 3																																																					-- ws_post_actions_id
  	,'Cliente2_RefCliente'																																												-- action_element //name			
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Cliente2/ns:RefCliente/text())[1]
  			with sql:variable("@Cliente2_RefCliente")'')'																						
  	,'Method : CrearOrdenesPedido , element : Cliente2_RefCliente'         -- description //if necesary
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3																																																					-- ws_post_actions_id
  	,'Cliente2_RazonSocial'																																												-- action_element //name			
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Cliente2/ns:RazonSocial/text())[1]
  			with sql:variable("@Cliente2_RazonSocial")'')'																														
  	,'Method : CrearOrdenesPedido , element : Cliente2_RazonSocial'                                            -- description //if necesary
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3																																																					-- ws_post_actions_id
  	,'Cliente2_Telefono'																																												-- action_element //name			
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Cliente2/ns:Telefono/text())[1]
  			with sql:variable("@Cliente2_Telefono")'')'
  	,'Method : CrearOrdenesPedido , element : Cliente2_Telefono'              -- description //if necesary
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3																																																					-- ws_post_actions_id
  	,'Cliente2_Telefono2'																																												-- action_element //name			
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Cliente2/ns:Telefono2/text())[1]
  			with sql:variable("@Cliente2_Telefono2")'')'																															
  	,'Method : CrearOrdenesPedido , element : Cliente2_Telefono2'                                            -- description //if necesary
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3																																																	
  	,'Cliente2_Email'																																															
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Cliente2/ns:EMail/text())[1]
  			with sql:variable("@Cliente2_Email")'')'															
  	,'Method : CrearOrdenesPedido , element : Cliente2_Email' 
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3																																																					-- ws_post_actions_id
  	,'Cliente2_Direccion'																																												-- action_element //name			
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Cliente2/ns:Direccion/text())[1]
  			with sql:variable("@Cliente2_Direccion")'')'																														
  	,'Method : CrearOrdenesPedido , element : Cliente2_Direccion'                                            
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3																																																					-- ws_post_actions_id
  	,'Cliente2_Latitud'																																												-- action_element //name			
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Cliente2/ns:Latitud/text())[1]
  			with sql:variable("@Cliente2_Latitud")'')'																											
  	,'Method : CrearOrdenesPedido , element : Cliente2_Latitud'                                            -- description //if necesary
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3																																																					-- ws_post_actions_id
  	,'Cliente2_Longitud'																																												-- action_element //name			
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Cliente2/ns:Longitud/text())[1]
  			with sql:variable("@Cliente2_Longitud")'')'												-- modify_element //command to modify the values of xml instance
  	,'Method : CrearOrdenesPedido , element : Cliente2_Longitud'                                            -- description //if necesary
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3																																																					-- ws_post_actions_id
  	,'Cliente2_Localidad'																																												-- action_element //name			
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Cliente2/ns:Localidad/text())[1]
  			with sql:variable("@Cliente2_Localidad")'')'															-- modify_element //command to modify the values of xml instance
  	,'Method : CrearOrdenesPedido , element : Cliente2_Localidad'                     -- description //if necesary
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3																																																					-- ws_post_actions_id
  	,'Cliente2_RefDomicilioExterno'																																												-- action_element //name			
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Cliente2/ns:RefDomicilioExterno/text())[1]
  			with sql:variable("@Cliente2_RefDomicilioExterno")'')'											-- modify_element //command to modify the values of xml instance
  	,'Method : CrearOrdenesPedido , element : Cliente2_RefDomicilioExterno'                                            -- description //if necesary
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3																																																					-- ws_post_actions_id
  	,'Cliente2_Descripcion'																																												-- action_element //name			
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Cliente2/ns:DomicilioDescripcion/text())[1]
  			with sql:variable("@Cliente2_Descripcion")'')'														-- modify_element //command to modify the values of xml instance
  	,'Method : CrearOrdenesPedido , element : Cliente2_Descripcion'                                            -- description //if necesary
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3																																																					-- ws_post_actions_id
  	,'Cliente2_TiempoEspera'																																												-- action_element //name			
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Cliente2/ns:TiempoEspera/text())[1]
  			with sql:variable("@Cliente2_TiempoEspera")'')'															-- modify_element //command to modify the values of xml instance
  	,'Method : CrearOrdenesPedido , element : Cliente2_TiempoEspera'                            -- description //if necesary
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3																																																					-- ws_post_actions_id
  	,'Cliente2_Contacto'																																												-- action_element //name			
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Cliente2/ns:Contacto/text())[1]
  			with sql:variable("@Cliente2_Contacto")'')'											-- modify_element //command to modify the values of xml instance
  	,'Method : CrearOrdenesPedido , element : Cliente2_Contacto'        -- description //if necesary
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3																																																					-- ws_post_actions_id
  	,'Cliente2_CargaExclusiva'																																												-- action_element //name			
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Cliente2/ns:CargaExclusiva/text())[1]
  			with sql:variable("@Cliente2_CargaExclusiva")'')'											-- modify_element //command to modify the values of xml instance
  	,'Method : CrearOrdenesPedido , element : Cliente2_CargaExclusiva'        -- description //if necesary
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3																																																					-- ws_post_actions_id
  	,'Cliente2_IgnorarOperacion'																																												-- action_element //name			
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Cliente2/ns:IgnorarOperacion/text())[1]
  			with sql:variable("@Cliente2_IgnorarOperacion")'')'											-- modify_element //command to modify the values of xml instance
  	,'Method : CrearOrdenesPedido , element : Cliente2_IgnorarOperacion'        -- description //if necesary
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3																																																					-- ws_post_actions_id
  	,'Cliente2_CrearDomicilioOrden'																																												-- action_element //name			
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Cliente2/ns:CrearDomicilioOrden/text())[1]
  			with sql:variable("@Cliente2_CrearDomicilioOrden")'')'												-- modify_element //command to modify the values of xml instance
  	,'Method : CrearOrdenesPedido , element : Cliente2_CrearDomicilioOrden'          -- description //if necesary
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3																																																					-- ws_post_actions_id
  	,'Cliente2_ActualizarDomicilioOrden'																																												-- action_element //name			
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Cliente2/ns:ActualizarDomicilioOrden/text())[1]
  			with sql:variable("@Cliente2_ActualizarDomicilioOrden")'')'								-- modify_element //command to modify the values of xml instance
  	,'Method : CrearOrdenesPedido , element : Cliente2_ActualizarDomicilioOrden'           -- description //if necesary
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3																																																					-- ws_post_actions_id
  	,'Cliente2_ValidarDomicilioOrden'																																												-- action_element //name			
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Cliente2/ns:ValidarDomicilioOrden/text())[1]
  			with sql:variable("@Cliente2_ValidarDomicilioOrden")'')'							-- modify_element //command to modify the values of xml instance
  	,'Method : CrearOrdenesPedido , element : Cliente2_ValidarDomicilioOrden'          -- description //if necesary
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3			
  	,'Pedido_Varchar1'		
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Varchar1/text())[1]
  			with sql:variable("@Pedido_Varchar1")'')'	
  	,'Method : CrearOrdenesPedido , element : Pedido_Varchar1'
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3			
  	,'Pedido_Varchar2'		
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Varchar2/text())[1]
  			with sql:variable("@Pedido_Varchar2")'')'	
  	,'Method : CrearOrdenesPedido , element : Pedido_Varchar2'
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3			
  	,'Pedido_Varchar3'		
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Varchar3/text())[1]
  			with sql:variable("@Pedido_Varchar3")'')'	
  	,'Method : CrearOrdenesPedido , element : Pedido_Varchar3'
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3			
  	,'Pedido_Varchar4'		
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Varchar4/text())[1]
  			with sql:variable("@Pedido_Varchar4")'')'	
  	,'Method : CrearOrdenesPedido , element : Pedido_Varchar4'
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3			
  	,'Pedido_Varchar5'		
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Varchar5/text())[1]
  			with sql:variable("@Pedido_Varchar5")'')'	
  	,'Method : CrearOrdenesPedido , element : Pedido_Varchar5'
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3			
  	,'Pedido_Varchar6'		
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Varchar6/text())[1]
  			with sql:variable("@Pedido_Varchar6")'')'	
  	,'Method : CrearOrdenesPedido , element : Pedido_Varchar6'
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3			
  	,'Pedido_CodigoSucursal'		
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:CodigoSucursal/text())[1]
  			with sql:variable("@Pedido_CodigoSucursal")'')'	
  	,'Method : CrearOrdenesPedido , element : Pedido_CodigoSucursal'
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3			
  	,'Pedido_TipoPedido'		
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:TipoPedido/text())[1]
  			with sql:variable("@Pedido_TipoPedido")'')'	
  	,'Method : CrearOrdenesPedido , element : Pedido_TipoPedido'
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3			
  	,'Pedido_InicioHorario'		
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:InicioHorario1/text())[1]
  			with sql:variable("@Pedido_InicioHorario")'')'	
  	,'Method : CrearOrdenesPedido , element : Pedido_InicioHorario'
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3			
  	,'Pedido_FinHorario1'		
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:FinHorario1/text())[1]
  			with sql:variable("@Pedido_FinHorario1")'')'	
  	,'Method : CrearOrdenesPedido , element : Pedido_FinHorario1'
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3			
  	,'Pedido_TiempoEspera'		
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:TiempoEspera/text())[1]
  			with sql:variable("@Pedido_TiempoEspera")'')'	
  	,'Method : CrearOrdenesPedido , element : Pedido_TiempoEspera'
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3			
  	,'Pedido_Categoria'		
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Categoria/text())[1]
  			with sql:variable("@Pedido_Categoria")'')'	
  	,'Method : CrearOrdenesPedido , element : Pedido_Categoria'
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3			
  	,'Pedido_Prioridad'		
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Prioridad/text())[1]
  			with sql:variable("@Pedido_Prioridad")'')'	
  	,'Method : CrearOrdenesPedido , element : Pedido_Prioridad'
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3			
  	,'depositoSalida_RefDepositoExterno'		
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:depositoSalida/ns:RefDepositoExterno/text())[1]
  			with sql:variable("@depositoSalida_RefDepositoExterno")'')'	
  	,'Method : CrearOrdenesPedido , element : depositoSalida_RefDepositoExterno'
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3			
  	,'depositoLlegada_RefDepositoExterno'		
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:depositoLlegada/ns:RefDepositoExterno/text())[1]
  			with sql:variable("@depositoLlegada_RefDepositoExterno")'')'	
  	,'Method : CrearOrdenesPedido , element : depositoLlegada_RefDepositoExterno'
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3			
  	,'PedidoItem_usarProductos'		
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:usarProductos/text())[1]
  			with sql:variable("@PedidoItem_usarProductos")'')'	
  	,'Method : CrearOrdenesPedido , element : PedidoItem_usarProductos'
  	,current_timestamp
  	,current_timestamp
  	,1
	)
/*
,(                                                                             
  	 3			
  	,'Items'		
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
		  insert sql:variable("@xmlStrRow")
			as first    
  	  into (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Items)[1]'')'
  	,'Method : CrearOrdenesPedido , element : Items'
  	,current_timestamp
  	,current_timestamp
  	,1
	)
*/
--/*
,(                                                                             
  	 3			
  	,'Items'		
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Items/text())[1]
  			with sql:variable("@xmlStrRow")'')'	
  	,'Method : CrearOrdenesPedido , element : Items'
  	,current_timestamp
  	,current_timestamp
  	,1
	)
--*/
/*	,(                                                                             
  	 3			
  	,'Items_Cantidad'		
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Items/ns:pOrdenPedidoItem/ns:Cantidad/text())[1]
  			with sql:variable("@Items_Cantidad")'')'	
  	,'Method : CrearOrdenesPedido , element : Items_Cantidad'
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3			
  	,'Items_Volumen'		
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Items/ns:pOrdenPedidoItem/ns:Volumen/text())[1]
  			with sql:variable("@Items_Volumen")'')'	
  	,'Method : CrearOrdenesPedido , element : Items_Volumen'
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3			
  	,'Items_Peso'		
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Items/ns:pOrdenPedidoItem/ns:Peso/text())[1]
  			with sql:variable("@Items_Peso")'')'	
  	,'Method : CrearOrdenesPedido , element : Items_Peso'
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3			
  	,'Items_Bulto'		
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Items/ns:pOrdenPedidoItem/ns:Bulto/text())[1]
  			with sql:variable("@Items_Bulto")'')'	
  	,'Method : CrearOrdenesPedido , element : Items_Bulto'
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3			
  	,'Items_Pallets'		
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Items/ns:pOrdenPedidoItem/ns:Pallets/text())[1]
  			with sql:variable("@Items_Pallets")'')'	
  	,'Method : CrearOrdenesPedido , element : Items_Pallets'
  	,current_timestamp
  	,current_timestamp
  	,1
	)
*/
 ,(                                                                             
  	 3			
  	,'Pedido_agruparItems'		
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:agruparItems/text())[1]
  			with sql:variable("@Pedido_agruparItems")'')'	
  	,'Method : CrearOrdenesPedido , element : Pedido_agruparItems'
  	,current_timestamp
  	,current_timestamp
  	,1
	)
/*	,(                                                                             
  	 3			
  	,'Pedido_CodigoOperacion'		
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:CodigoOperacion/text())[1]
  			with sql:variable("@Pedido_CodigoOperacion")'')'	
  	,'Method : CrearOrdenesPedido , element : Pedido_CodigoOperacion'
  	,current_timestamp
  	,current_timestamp
  	,1
	)
*/	,(                                                                             
  	 3			
  	,'Pedido2_CodigoSucursal'		
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:CodigoSucursal/text())[2]
  			with sql:variable("@Pedido2_CodigoSucursal")'')'	
  	,'Method : CrearOrdenesPedido , element : Pedido2_CodigoSucursal'
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3			
  	,'Pedido_Unidades'		
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Unidades/text())[1]
  			with sql:variable("@Pedido_Unidades")'')'	
  	,'Method : CrearOrdenesPedido , element : Pedido_Unidades'
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3			
  	,'Pedido_usarProductos'		
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:usarProductos/text())[2]
  			with sql:variable("@Pedido_usarProductos")'')'	
  	,'Method : CrearOrdenesPedido , element : Pedido_usarProductos'
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3			
  	,'Pedido_soloInsertarProductos'		
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:soloInsertarProductos/text())[1]
  			with sql:variable("@Pedido_soloInsertarProductos")'')'	
  	,'Method : CrearOrdenesPedido , element : Pedido_soloInsertarProductos'
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3			
  	,'Pedido2_agruparItems'		
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:agruparItems/text())[2]
  			with sql:variable("@Pedido2_agruparItems")'')'	
  	,'Method : CrearOrdenesPedido , element : Pedido2_agruparItems'
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3			
  	,'Pedido_ValorDeclarado'		
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:ValorDeclarado/text())[1]
  			with sql:variable("@Pedido_ValorDeclarado")'')'	
  	,'Method : CrearOrdenesPedido , element : Pedido_ValorDeclarado'
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3			
  	,'Pedido_Float1'		
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Float1/text())[1]
  			with sql:variable("@Pedido_Float1")'')'	
  	,'Method : CrearOrdenesPedido , element : Pedido_Float1'
  	,current_timestamp
  	,current_timestamp
  	,1
	)
	,(                                                                             
  	 3			
  	,'Pedido_Float2'		
  	,'set @postdata.modify(''declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
  	  declare namespace ns="http://unisolutions.com.ar/";
  	  replace value of (/soap:Envelope/soap:Body/ns:CrearOrdenesPedido/ns:pedidos/ns:pOrdenPedido/ns:Float2/text())[1]
  			with sql:variable("@Pedido_Float2")'')'	
  	,'Method : CrearOrdenesPedido , element : Pedido_Float2'
  	,current_timestamp
  	,current_timestamp
  	,1
	)
		;

-- 
-- ============================================================================================================ --
-- NOTE Actions Catalog
-- ============================================================================================================ --


IF OBJECT_ID ('ws_view_builder_actions', 'V') IS NOT NULL
    DROP VIEW ws_view_builder_actions;
use ws

alter view "dbo"."ws_view_builder_actions"
 with encryption
as
		 select
					 "actions".id
					,"params".description
					,"params".url
					,"params".host
					,"params".soap
					,"actions".action
					,"actions".postdata
					,"actions".parameters
					,"actions".request
		 from
					"ws"."dbo"."ws_send_post_params" as "params"
		 inner join
					"ws"."dbo"."ws_post_actions" as "actions"
			on
					"actions"."send_post_params_id" = "params"."id"
;


-- ====================================================================================================== --
-- 
-- ====================================================================================================== --

IF OBJECT_ID('dbo.ws_responses', 'U') IS NOT NULL 
  DROP TABLE dbo.ws_responses; 

set ansi_nulls on
set quoted_identifier on
set ansi_padding on
create table [ws].[dbo].[ws_responses](
		id												int identity(1,1),
		status_response						int,
		action										nvarchar(254) null,
	  response									nvarchar(max) null,
		description               text null,
		postdata									xml,
		created										datetime default current_timestamp,
		modified									datetime default current_timestamp,
		_status										tinyint default 1 null
--		,constraint ak_user_id unique(user_id)
) on [primary]
set ansi_padding off


-- ============================================================================================================ --
-- NOTE last Additions add multiple items in procedure && Sqhema for send an unique ws_id to ... unigis server  
-- ============================================================================================================ --
--for PEDIDOS

IF OBJECT_ID('dbo.ws_zero_items', 'U') IS NOT NULL 
  DROP TABLE dbo.ws_zero_items; 

create table [ws].[dbo].[ws_zero_items]
set ansi_nulls on
set quoted_identifier on
set ansi_padding on
create table [ws].[dbo].[ws_zero_items](
		id														bigint identity(1,1),
		id_pedidopk										nvarchar(25) null,
		ctrl_id												int null,
		xml_data											nvarchar(max) null,
		description										text null,
		created												datetime default current_timestamp,
		modified											datetime default current_timestamp,
		_status												tinyint default 1 null
--		,constraint ak_user_id unique(user_id)
) on [primary]
set ansi_padding off


-- NOTE start as procedure 
-- create
alter
--create
procedure [dbo].[ws_sp_inner_pedidos]
 (
	 @id_pedidopk nvarchar(25)				 -- NOTE reference_document id [LIS]  
	,@ctrl_id int											 -- NOTE swicth mode for This Specific Method view [1 pedido|2 solicitud]
	,@xmlStrRow as nvarchar(max) out
 )
as
set ANSI_NULLS on 
set QUOTED_IDENTIFIER on 
set NOCOUNT on 

-- NOTE Declare inner variables
--  declare @id_pedidopk nvarchar(25) = '371337', @ctrl_id int = 1 , @xmlStrRow nvarchar(max)
--  declare @id_pedidopk nvarchar(25) = '243151', @ctrl_id int = 2 , @xmlStrRow nvarchar(max)
	declare
					 @Descripcion as nvarchar(60)
          ,@RefDocumento as nvarchar(25)
					,@Cantidad as nvarchar(255)-- decimal(18,6)
					,@Volumen as nvarchar(10)
					,@Peso as nvarchar(25)
					,@Bulto as nvarchar(10)
					,@Pallets as nvarchar(10)
					,@Consecutivo as int 

		set @xmlStrRow = '' -- Initialize 
		declare xml_cursor cursor for 					
			select consecutivo from ws.dbo.ws_view_mode_details where id_pedidopk = @id_pedidopk and ctrl_id = @ctrl_id
		open xml_cursor 
			fetch next from xml_cursor into @Consecutivo
				while @@FETCH_STATUS = 0
				begin

					select  @xmlStrRow = @xmlStrRow + 
													'<unis:pOrdenPedidoItem>' +
														'<unis:Descripcion>' +
															"bxml".descripcion_producto +
														'</unis:Descripcion>' +
														'<unis:RefDocumento>' +
														  @id_pedidopk +  -- "bxml".embalaje +   -- NOTE appears that this can be replace by id_pedidopk
														'</unis:RefDocumento>'+
														'<unis:Cantidad>' +
															"bxml".cantidad +
														'</unis:Cantidad>' +
														'<unis:Volumen>' +
															'0' +
														'</unis:Volumen>' +
														'<unis:Peso>' +
															"bxml".peso +
														'</unis:Peso>' +
														'<unis:Bulto>' +
															'0' +
														'</unis:Bulto>' +
														'<unis:Pallets>' +
															'0' + 
														'</unis:Pallets>' +
													'</unis:pOrdenPedidoItem>'
												from 
													ws.dbo.ws_view_mode_details as "bxml"
												where                              
														"bxml".id_pedidopk = @id_pedidopk
													and 
														"bxml".ctrl_id = @ctrl_id
													and 
														"bxml".consecutivo = @Consecutivo						
							--NOTE  get next row
--							print @Consecutivo
							fetch next from xml_cursor into @Consecutivo
        end -- cursor 
			close xml_cursor
			deallocate xml_cursor
	-- NOTE Return 
--select @xmlStrRow

-- =========================================================================================================================== --
-- NOTE TEST the Store
-- =========================================================================================================================== --
-- 371145,243151
	declare @xmlStrRow nvarchar(max)
  execute ws_sp_inner_pedidos '371145',1, @xmlStrRow OUTPUT
  select  @xmlStrRow	

-- =========================================================================================================================== --
-- NOTE View and Normalization in detail products 
-- =========================================================================================================================== --

IF OBJECT_ID ('ws_view_mode_details', 'V') IS NOT NULL
    DROP VIEW ws_view_mode_details;
use ws

alter
--create
view "dbo"."ws_view_mode_details"
 with encryption
as
select --top 10
		 "pedido".id_pedidopk
		,1 as 'ctrl_id'
		,"pedido".descripcion_producto
	--	NOTE the next  case clause is the  poor aportation of britany                                                                                                                	
  	,CASE ("pedido".embalaje) WHEN '0' THEN 'SIN ASIGNAR' WHEN '1' THEN 'TARIMAS' WHEN '2' THEN 'BULTOS' WHEN '3' THEN 'CARTONES' WHEN '4' THEN 'ROLLOS' WHEN '5' THEN 'PACAS'
        WHEN '6' THEN 'PAPELERIA' WHEN '7' THEN 'CAMIONETAS' WHEN '8' THEN 'SACOS' WHEN '9' THEN 'GRANEL' WHEN '10' THEN 'CAJAS' WHEN '11' THEN 'CONTENEDOR' WHEN '12' THEN '_'
  		  WHEN '13' THEN 'PALETS' WHEN '14' THEN 'TAMBOS' WHEN '15' THEN 'PZAS' WHEN '16' THEN 'ATADOS' WHEN '17' THEN 'LOTE' WHEN '18' THEN 'PAQUETES' WHEN '99' THEN 'OTROS'
  		-- and the else clause ?
     END as 'embalaje'
  	,cast(cast("pedido".cantidad as int) as nvarchar(255)) as 'cantidad'
  --		NOTE this is obscure and not defined yet , again by the fucking Bre
  	,cast(isnull("pedido".peso/nullif("pedido".cantidad,0),0) as nvarchar(255)) as 'peso'
		,"pedido".consecutivo
from 
		pruebasdb2019.dbo.desp_pedido_productos as "pedido"
union all
select --top 10
		 "solicitud".id_solicitud as 'id_pedidopk'
		,2 as 'ctrl_id'
		,"solicitud".desc_producto as 'descripcion_producto'
--	NOTE the next  case clause is the  poor aportation of britany
		,CASE ("solicitud".embalaje) WHEN '0' THEN 'SIN ASIGNAR' WHEN '1' THEN 'TARIMAS' WHEN '2' THEN 'BULTOS' WHEN '3' THEN 'CARTONES' WHEN '4' THEN 'ROLLOS' WHEN '5' THEN 'PACAS'
      WHEN '6' THEN 'PAPELERIA' WHEN '7' THEN 'CAMIONETAS' WHEN '8' THEN 'SACOS' WHEN '9' THEN 'GRANEL' WHEN '10' THEN 'CAJAS' WHEN '11' THEN 'CONTENEDOR' WHEN '12' THEN '_'
		  WHEN '13' THEN 'PALETS' WHEN '14' THEN 'TAMBOS' WHEN '15' THEN 'PZAS' WHEN '16' THEN 'ATADOS' WHEN '17' THEN 'LOTE' WHEN '18' THEN 'PAQUETES' WHEN '99' THEN 'OTROS'
		-- and the else clause ?
     END as 'embalaje'
		,cast(cast("solicitud".cantidad as int) as nvarchar(255)) as 'cantidad'
--		NOTE this is obscure and not defined yet , again by the fucking Bre
		,cast(isnull("solicitud".peso_capturado/nullif("solicitud".cantidad,0),0) as nvarchar(255)) as 'peso'
		,"solicitud".consecutivo
from 
		pruebasdb2019.dbo.cons_solicituddet as "solicitud"



/*
declare @xml_data as xml 

	with xmlnamespaces('http://unisolutions.com.ar' as "unis")
  select @xml_data = (
		select  
			 "bxml".descripcion_producto as 'unis:Descripcion'
			,"bxml".embalaje as 'unis:RefDocumento'
			,"bxml".cantidad as 'unis:Cantidad'
			,'0' as 'unis:Volumen'
			,"bxml".peso/isnull("bxml".cantidad,1) as 'unis:Peso'
			,'0' as 'unis:Bulto'
			,'0' as 'unis:Pallets' 
		from 
			pruebasdb2019.dbo.desp_pedido_productos as "bxml"
		where 
			"bxml".id_pedidopk = @id_pedidopk
		for xml path ('unis:pOrdenPedidoItems') , root('xml')
	)
insert into txml (xml_data)
select replace(replace(cast(@xml_data as nvarchar(max)),'<xml xmlns:unis="http://unisolutions.com.ar">',''),'</xml>','')

*/


-- ====================================================================================================== --
-- DEBUG Query for Solicitud and Pedidos Mode Methods 
-- ====================================================================================================== --

IF OBJECT_ID ('ws_view_mode_actions', 'V') IS NOT NULL
    DROP VIEW ws_view_mode_actions;
use ws

alter
--create 
		view "dbo"."ws_view_mode_actions"
		with encryption
		as
			select 
							* 
			from 
					"ws"."dbo".ws_view_pedido_actions
			 union all 
			select 
							* 
			from 
					ws_view_solicitud_actions


-- NOTE Temporal query


-- =========================================================================== --
-- NOTE Logs_Table   select * from ws.dbo.api_ws_historico_logs
-- =========================================================================== --
use [ws]
IF OBJECT_ID('dbo.api_ws_historico_logs', 'U') IS NOT NULL 
  DROP TABLE dbo.api_ws_historico_logs
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on
-- go
create table dbo.api_ws_historico_logs(
		 id					 bigint					identity(1,1)
		,message		     text			    	null
		,created			 datetime 				default current_timestamp null
		,modified			 datetime				null
		,status				 tinyint 				default 1 null
) on [primary]
-- go
set ansi_padding off


-- ======================================================================================================================== --
-- PEDIDOS AND SOLICITUD METHODS
-- ======================================================================================================================== --
-- NOTE Build the view 

--CREATE 
ALTER
PROCEDURE [dbo].[ws_sp_send_post_data_ps]
 (
	 @action_id int				 -- NOTE XML Method specification for POST request [Unigis Ws]
	,@id_document nvarchar(25)     -- NOTE reference_document id [LIS]  
	,@ctrl_id int  -- NOTE swicth mode for This Specific Method view [1 pedido|2 solicitud]
	,@key int  -- NOTE apikey or password or access key to WS
 )
as
set ANSI_NULLS on 
set QUOTED_IDENTIFIER on 
set NOCOUNT on 
-- ==================================================================================================== --
--	NOTE This is the script working for 8W post request
-- ==================================================================================================== --
--NOTE Emulate the execution 
--																					<ApiKey>5679</ApiKey>
--																					<ReferenciaExterna>243021</ReferenciaExterna>
-- DEBUG TEST STORE VARS
--select @postData
/*
declare
					@action_id as int = 3      -- method for request 
				,@id_document  int = 370807--450--439-- 370807     -- RefDocumento
				,@ctrl_id int = 1						 -- Pedido or Solicitud
*/

--	declare @action_id int = 3 ,@id_document nvarchar(25) = '371332', @ctrl_id int = 1 ,@key int = 5679

-- NOTE END EMuLATION

-- DEBUG NOTE Check entries for procedure 

-- DEBUG 
if exists (select * from "ws"."dbo"."ws_view_mode_actions" where Pedido_RefDocumento = @id_document and ctrl_id =  @ctrl_id)
	begin 
				insert into ws.dbo.api_ws_historico_logs
				values  (	
									 'id_document => ' + @id_document + ' key => ' + cast(@key as varchar(10)) + ' ctrl_id => ' + cast(@ctrl_id as varchar(15))
									,current_timestamp
									,current_timestamp
									,1
						    )  
	end 
else 
		insert into ws.dbo.api_ws_historico_logs
				values  (	
									'ERROR' 
									,current_timestamp
									,current_timestamp
									,1
						    )  
--RETURN
-- NOTE items Products
	declare @xmlStrRow nvarchar(max)

-- NOTE get the necesary data for the procedure
declare @url as nvarchar(999)
declare @Host as nvarchar(255)
declare @action as nvarchar(900) --= 'ConsultarViajePorFechas'
declare @soap as nvarchar(800)
--declare @postdata as nvarchar(max)
declare @postdata as xml
declare @parameters as nvarchar(max)
declare @request as nvarchar(max)
declare @element as nvarchar(max)

select 
			 @url = url
			,@Host = host
			,@soap = soap 
			,@action = action
			,@postdata = postdata
			,@parameters = parameters
			,@request = request
from 
		  ws.dbo.ws_view_builder_actions
where 
		  id = @action_id

--select @postdata
-- DEBUG a for send 
-- NOTE Emulated selected data from query
		declare  
						 @ApiKey																	as nvarchar(120)                             
            ,@Pedido_RefDocumento											as nvarchar(25)             
            ,@Pedido_RefDocumentoAdicional						as nvarchar(25)      
            ,@Pedido_Descripcion											as nvarchar(120)                
            ,@Pedido_Fecha                            as datetime 
            ,@Pedido_FechaEntrega											as datetime 
            ,@Pedido_FechaRecoleccion									as datetime 
            ,@Cliente_RefCliente											as bigint 
            ,@Cliente_RazonSocial											as nvarchar(120)
            ,@Cliente_Telefono												as nvarchar(25)
            ,@Cliente_Telefono2												as nvarchar(25)
            ,@Cliente_Email														as nvarchar(250)
            ,@Cliente_Direccion												as nvarchar(240)
            ,@Cliente_Latitud													as decimal(10,6) 
            ,@Cliente_Longitud												as decimal(10,6)
            ,@Cliente_Localidad												as nvarchar(30)
            ,@Cliente_RefDomicilioExterno							as nvarchar(25) 
            ,@Cliente_DomicilioDescripcion						as nvarchar(120)
            ,@Cliente_TiempoEspera										as nvarchar(60)
            ,@Cliente_Contacto												as nvarchar(80)
            ,@Cliente_CargaExclusiva									as nvarchar(5)
            ,@Cliente_IgnorarOperacion								as nvarchar(5)
            ,@Cliente_CrearDomicilioOrden							as nvarchar(4)
            ,@Cliente_ActualizarDomicilioOrden				as nvarchar(1)
            ,@Cliente_ValidarDomicilioOrden						as nvarchar(1)
            ,@Cliente2_RefCliente											as int 
            ,@Cliente2_RazonSocial										as nvarchar(120)
            ,@Cliente2_Telefono												as nvarchar(25)
            ,@Cliente2_Telefono2											as nvarchar(25)
            ,@Cliente2_Email													as nvarchar(250)
            ,@Cliente2_Direccion											as nvarchar(240)
            ,@Cliente2_Latitud												as decimal(10,6)
            ,@Cliente2_Longitud												as decimal(10,6)
            ,@Cliente2_Localidad											as nvarchar(30)
            ,@Cliente2_RefDomicilioExterno						as nvarchar(25)
            ,@Cliente2_Descripcion										as nvarchar(120)
            ,@Cliente2_TiempoEspera										as nvarchar(60)
            ,@Cliente2_Contacto												as nvarchar(80)
            ,@Cliente2_CargaExclusiva									as nvarchar(5)
            ,@Cliente2_IgnorarOperacion								as nvarchar(5)
            ,@Cliente2_CrearDomicilioOrden						as nvarchar(4)
            ,@Cliente2_ActualizarDomicilioOrden				as nvarchar(1)
            ,@Cliente2_ValidarDomicilioOrden					as nvarchar(1)
            ,@Pedido_Varchar1													as bigint
            ,@Pedido_Varchar2													as nvarchar(120)
            ,@Pedido_Varchar3													as nvarchar(240) 
            ,@Pedido_Varchar4													as nvarchar(6)
            ,@Pedido_Varchar5													as nvarchar(25)
            ,@Pedido_Varchar6													as nvarchar(20)
            ,@Pedido_CodigoSucursal										as int 
            ,@Pedido_TipoPedido												as nvarchar(1)
            ,@Pedido_InicioHorario										as nvarchar(3)
            ,@Pedido_FinHorario1											as nvarchar(4)
            ,@Pedido_TiempoEspera											as nvarchar(2)
            ,@Pedido_Categoria												as nvarchar(1)
            ,@Pedido_Prioridad												as nvarchar(1)
           -- ,@Pedido_CargaExclusiva										as nvarchar(5)
            ,@depositoSalida_RefDepositoExterno				as int 
            ,@depositoLlegada_RefDepositoExterno			as int 
            ,@PedidoItem_usarProductos								as nvarchar(25)
--            ,@Items_Descripcion												as nvarchar(59)
--            ,@Items_RefDocumento											as nvarchar(11)
--            ,@Items_Cantidad													as decimal(18,6)
--            ,@Items_Volumen														as nvarchar(1)
--            ,@Items_Peso															as nvarchar(27)
--            ,@Items_Bulto															as nvarchar(1)
--            ,@Items_Pallets														as nvarchar(1)
            ,@Pedido_agruparItems											as nvarchar(5)
--            ,@Pedido_CodigoOperacion									as nvarchar(1)
            ,@Pedido2_CodigoSucursal									as nvarchar(1)
            ,@Pedido_Unidades													as nvarchar(1)
            ,@Pedido_usarProductos										as nvarchar(4)
            ,@Pedido_soloInsertarProductos						as nvarchar(5)
            ,@Pedido2_agruparItems										as nvarchar(5)
            ,@Pedido_ValorDeclarado										as decimal(18,6)
            ,@Pedido_Float1														as nvarchar(10)
            ,@Pedido_Float2														as nvarchar(10)

-- ======================================================================================= --
-- NOTE SET the VARS

	select 
	  @ApiKey=ApiKey                                                        			 
   ,@Pedido_RefDocumento=isnull(cast(Pedido_RefDocumento as nvarchar(25)) , @id_document)                           
   ,@Pedido_RefDocumentoAdicional=isnull(Pedido_RefDocumentoAdicional,'0')             
   ,@Pedido_Descripcion=Pedido_Descripcion                                 
   ,@Pedido_Fecha=Pedido_Fecha                                             
   ,@Pedido_FechaEntrega=Pedido_FechaEntrega                               
   ,@Pedido_FechaRecoleccion=isnull(Pedido_FechaRecoleccion,'1900-01-01 01:01:01')                       
   ,@Cliente_RefCliente=Cliente_RefCliente                                 
   ,@Cliente_RazonSocial=Cliente_RazonSocial                               
   ,@Cliente_Telefono=Cliente_Telefono                                     
   ,@Cliente_Telefono2=Cliente_Telefono2                                   
   ,@Cliente_Email=Cliente_Email                                           
   ,@Cliente_Direccion=Cliente_Direccion                                   
   ,@Cliente_Latitud=Cliente_Latitud                                       
   ,@Cliente_Longitud=Cliente_Longitud                                     
   ,@Cliente_Localidad=Cliente_Localidad                                   
   ,@Cliente_RefDomicilioExterno=Cliente_RefDomicilioExterno               
   ,@Cliente_DomicilioDescripcion=Cliente_DomicilioDescripcion             
   ,@Cliente_TiempoEspera=Cliente_TiempoEspera                             
   ,@Cliente_Contacto=Cliente_Contacto                                     
   ,@Cliente_CargaExclusiva=Cliente_CargaExclusiva                         
   ,@Cliente_IgnorarOperacion=Cliente_IgnorarOperacion                     
   ,@Cliente_CrearDomicilioOrden=Cliente_CrearDomicilioOrden               
   ,@Cliente_ActualizarDomicilioOrden=Cliente_ActualizarDomicilioOrden     
   ,@Cliente_ValidarDomicilioOrden=Cliente_ValidarDomicilioOrden           
   ,@Cliente2_RefCliente=Cliente2_RefCliente                               
   ,@Cliente2_RazonSocial=Cliente2_RazonSocial                             
   ,@Cliente2_Telefono=Cliente2_Telefono                                   
   ,@Cliente2_Telefono2=Cliente2_Telefono2                                 
   ,@Cliente2_Email=Cliente2_Email                                         
   ,@Cliente2_Direccion=Cliente2_Direccion                                 
   ,@Cliente2_Latitud=Cliente2_Latitud                                     
   ,@Cliente2_Longitud=Cliente2_Longitud                                   
   ,@Cliente2_Localidad=Cliente2_Localidad                                 
   ,@Cliente2_RefDomicilioExterno=Cliente2_RefDomicilioExterno             
   ,@Cliente2_Descripcion=Cliente2_Descripcion                             
   ,@Cliente2_TiempoEspera=Cliente2_TiempoEspera                           
   ,@Cliente2_Contacto=Cliente2_Contacto                                   
   ,@Cliente2_CargaExclusiva=Cliente2_CargaExclusiva                       
   ,@Cliente2_IgnorarOperacion=Cliente2_IgnorarOperacion                   
   ,@Cliente2_CrearDomicilioOrden=Cliente2_CrearDomicilioOrden             
   ,@Cliente2_ActualizarDomicilioOrden=Cliente2_ActualizarDomicilioOrden   
   ,@Cliente2_ValidarDomicilioOrden=Cliente2_ValidarDomicilioOrden         
   ,@Pedido_Varchar1=Pedido_Varchar1                                       
   ,@Pedido_Varchar2=Pedido_Varchar2                                       
   ,@Pedido_Varchar3=Pedido_Varchar3                                       
   ,@Pedido_Varchar4=Pedido_Varchar4                                       
   ,@Pedido_Varchar5=Pedido_Varchar5                                       
   ,@Pedido_Varchar6=isnull(Pedido_Varchar6,'0')                                       
   ,@Pedido_CodigoSucursal=Pedido_CodigoSucursal                           
   ,@Pedido_TipoPedido=cast(Pedido_TipoPedido as nvarchar(1))                                   
   ,@Pedido_InicioHorario=Pedido_InicioHorario                             
   ,@Pedido_FinHorario1=Pedido_FinHorario1                                 
   ,@Pedido_TiempoEspera=Pedido_TiempoEspera                               
   ,@Pedido_Categoria=Pedido_Categoria                                     
   ,@Pedido_Prioridad=Pedido_Prioridad                                     
  -- ,@Pedido_CargaExclusiva=Pedido_CargaExclusiva                           
   ,@depositoSalida_RefDepositoExterno=depositoSalida_RefDepositoExterno   
   ,@depositoLlegada_RefDepositoExterno=depositoLlegada_RefDepositoExterno 
   ,@PedidoItem_usarProductos=PedidoItem_usarProductos                     
--   ,@Items_Descripcion=isnull(Items_Descripcion,'.')                                   
--   ,@Items_RefDocumento=isnull(Items_RefDocumento,'0')                                 
--   ,@Items_Cantidad=cast(isnull(Items_Cantidad,'0') as decimal(18,6))                                        
--   ,@Items_Volumen=Items_Volumen                                           
--   ,@Items_Peso=Items_Peso                                                 
--   ,@Items_Bulto=Items_Bulto                                               
--   ,@Items_Pallets=Items_Pallets                                           
   ,@Pedido_agruparItems=Pedido_agruparItems                               
 --  ,@Pedido_CodigoOperacion=Pedido_CodigoOperacion                         
   ,@Pedido2_CodigoSucursal=Pedido2_CodigoSucursal                         
   ,@Pedido_Unidades=Pedido_Unidades                                       
   ,@Pedido_usarProductos=Pedido_usarProductos                             
   ,@Pedido_soloInsertarProductos=Pedido_soloInsertarProductos             
   ,@Pedido2_agruparItems=Pedido2_agruparItems                             
   ,@Pedido_ValorDeclarado=isnull(Pedido_ValorDeclarado,'0')                           
   ,@Pedido_Float1=Pedido_Float1                                           
   ,@Pedido_Float2=Pedido_Float2                                           
from 
--"ws"."dbo"."ws_view_mode_actions" where Pedido_RefDocumento = (case when @ctrl_id = 1 then cast(@id_document as int) else @id_document end) and ctrl_id =  @ctrl_id
"ws"."dbo"."ws_view_mode_actions" where Pedido_RefDocumento = @id_document and ctrl_id =  @ctrl_id

--NOTE DEBUG Get the previous for ITEMS 

  execute ws_sp_inner_pedidos @id_document,@ctrl_id, @xmlStrRow OUTPUT

-- DEBUG 
				insert into ws.dbo.api_ws_historico_logs
				values  (	
									 'Before start the cursor => @Pedido_RefDocumento => ' + @Pedido_RefDocumento + ' Pedido_Varchar6 => ' + @Pedido_Varchar6
									+ ' Apikey => ' + cast(@Apikey as varchar(10)) + ' id_document => ' + @id_document + ' key => ' + cast(@key as varchar(10)) + 'xmlStrRow => ' + @xmlStrRow
									,current_timestamp
									,current_timestamp
									,1
						    )  

-- NOTE Cursor Solution ? for each inner_action attach a variable content :: maybe yes because we need execute line per line of inner_action 
-- TODO start cursor for set modifications to xml 
-- https://stackoverflow.com/questions/38857749/the-argument-1-of-the-xml-data-type-method-modify-must-be-a-string-literal

	declare el_cursor cursor for 
		select modify_element from ws.dbo.ws_actions_elements where ws_post_actions_id = @action_id 
/*
		and id in 
																																														(
																																															  3      -- ApiKey                             
																																															, 4      -- RefDocumento                       
																																															, 5      -- RefDocumentoAdicional              
																																															, 6      -- Descripcion                        
																																															, 7      -- Fecha                              
																																															, 8      -- FechaEntrega                       
																																															, 9      -- FechaRecoleccion                   
																																															, 10     -- RefCliente                         
																																															, 11     -- RazonSocial                        
																																															, 12     -- Telefono                           
																																															, 13     -- Telefono2                          
																																															, 14     -- Cliente_Email                      
																																															, 15     -- Direccion                          
																																															, 16     -- Latitud                            
																																															, 17     -- Cliente_Longitud                   
																																															, 18     -- Cliente_Localidad                  
																																															, 19     -- Cliente_RefDomicilioExterno        
																																															, 20     -- Cliente_DomicilioDescripcion       
																																															, 21     -- Cliente_TiempoEspera               
																																															, 22     -- Cliente_Contacto                   
																																															, 23     -- Cliente_CargaExclusiva             
																																															, 24     -- Cliente_IgnorarOperacion           
																																															, 25     -- Cliente_CrearDomicilioOrden        
																																															, 26     -- Cliente_ActualizarDomicilioOrden   
																																															, 27     -- Cliente_ValidarDomicilioOrden      
																																															, 28     -- Cliente2_RefCliente                
																																															, 29     -- Cliente2_RazonSocial               
																																															, 30     -- Cliente2_Telefono                  
																																															, 31     -- Cliente2_Telefono2                 
																																															, 32     -- Cliente2_Email                     
																																															, 33     -- Cliente2_Direccion                 
																																															, 34     -- Cliente2_Latitud                   
																																															, 35     -- Cliente2_Longitud                  
																																															, 36     -- Cliente2_Localidad                 
																																															, 37     -- Cliente2_RefDomicilioExterno       
																																															, 38     -- Cliente2_Descripcion               
																																															, 39     -- Cliente2_TiempoEspera              
																																															, 40     -- Cliente2_Contacto                  
																																															, 41     -- Cliente2_CargaExclusiva            
																																															, 42     -- Cliente2_IgnorarOperacion          
																																															, 43     -- Cliente2_CrearDomicilioOrden       
																																															, 44     -- Cliente2_ActualizarDomicilioOrden  
																																															, 45     -- Cliente2_ValidarDomicilioOrden     
																																															, 46     -- Pedido_Varchar1                    
																																															, 47     -- Pedido_Varchar2                    
																																															, 48     -- Pedido_Varchar3                    
																																															, 49     -- Pedido_Varchar4                    
																																															, 50     -- Pedido_Varchar5                    
																																															, 51     -- Pedido_Varchar6                    
																																															, 52     -- Pedido_CodigoSucursal              
																																															, 53     -- Pedido_TipoPedido                  
																																															, 54     -- Pedido_InicioHorario               
																																															, 55     -- Pedido_FinHorario1                 
																																															, 56     -- Pedido_TiempoEspera                
																																															, 57     -- Pedido_Categoria                   
																																															, 58     -- Pedido_Prioridad                   
																																															, 59     -- depositoSalida_RefDepositoExterno  
																																															, 60     -- depositoLlegada_RefDepositoExterno 
																																															, 61     -- PedidoItem_usarProductos           
																																															, 62     -- Items_Descripcion                  
																																															, 63     -- Items_RefDocumento                 
																																															, 64     -- Items_Cantidad                     
																																															, 65     -- Items_Volumen                      
																																															, 66     -- Items_Peso                         
																																															, 67     -- Items_Bulto                        
																																															, 68     -- Items_Pallets                      
																																															, 69     -- Pedido_agruparItems                
																																															, 70     -- Pedido2_CodigoSucursal             
																																															, 71     -- Pedido_Unidades                    
																																															, 72     -- Pedido_usarProductos               
																																															, 73     -- Pedido_soloInsertarProductos       
																																															, 74     -- Pedido2_agruparItems               
																																															, 75     -- Pedido_ValorDeclarado              
																																															, 76     -- Pedido_Float1                      
																																															, 77     -- Pedido_Float2                      
																																												   	) 
*/
	 open el_cursor 
		fetch next from el_cursor into @element 

		while @@FETCH_STATUS = 0 
			begin 
-- NOTE test Dynamic SQL --
		declare @sql nvarchar(max)
			set @sql = @element
		exec sp_executesql -- NOTE this is an automatic params 
											@sql
										 ,@parameters
										 ,@postdata = @postdata output
										-- NOTE this is automatically posible ?
										 ,@ApiKey= @ApiKey                                                         
										 ,@Pedido_RefDocumento= @Pedido_RefDocumento                               
										 ,@Pedido_RefDocumentoAdicional= @Pedido_RefDocumentoAdicional             
										 ,@Pedido_Descripcion= @Pedido_Descripcion                                 
										 ,@Pedido_Fecha= @Pedido_Fecha                                             
										 ,@Pedido_FechaEntrega= @Pedido_FechaEntrega                               
										 ,@Pedido_FechaRecoleccion= @Pedido_FechaRecoleccion                       
										 ,@Cliente_RefCliente= @Cliente_RefCliente                                 
										 ,@Cliente_RazonSocial= @Cliente_RazonSocial                               
										 ,@Cliente_Telefono= @Cliente_Telefono                                     
										 ,@Cliente_Telefono2= @Cliente_Telefono2                                   
										 ,@Cliente_Email= @Cliente_Email                                           
										 ,@Cliente_Direccion= @Cliente_Direccion                                   
										 ,@Cliente_Latitud= @Cliente_Latitud                                       
										 ,@Cliente_Longitud= @Cliente_Longitud                                     
										 ,@Cliente_Localidad= @Cliente_Localidad                                   
										 ,@Cliente_RefDomicilioExterno= @Cliente_RefDomicilioExterno               
										 ,@Cliente_DomicilioDescripcion= @Cliente_DomicilioDescripcion             
										 ,@Cliente_TiempoEspera= @Cliente_TiempoEspera                             
										 ,@Cliente_Contacto= @Cliente_Contacto                                     
										 ,@Cliente_CargaExclusiva= @Cliente_CargaExclusiva                         
										 ,@Cliente_IgnorarOperacion= @Cliente_IgnorarOperacion                     
										 ,@Cliente_CrearDomicilioOrden= @Cliente_CrearDomicilioOrden               
										 ,@Cliente_ActualizarDomicilioOrden= @Cliente_ActualizarDomicilioOrden     
										 ,@Cliente_ValidarDomicilioOrden= @Cliente_ValidarDomicilioOrden           
										 ,@Cliente2_RefCliente= @Cliente2_RefCliente                               
										 ,@Cliente2_RazonSocial= @Cliente2_RazonSocial                             
										 ,@Cliente2_Telefono= @Cliente2_Telefono                                   
										 ,@Cliente2_Telefono2= @Cliente2_Telefono2                                 
										 ,@Cliente2_Email= @Cliente2_Email                                         
										 ,@Cliente2_Direccion= @Cliente2_Direccion                                 
										 ,@Cliente2_Latitud= @Cliente2_Latitud                                     
										 ,@Cliente2_Longitud= @Cliente2_Longitud                                   
										 ,@Cliente2_Localidad= @Cliente2_Localidad                                 
										 ,@Cliente2_RefDomicilioExterno= @Cliente2_RefDomicilioExterno             
										 ,@Cliente2_Descripcion= @Cliente2_Descripcion                             
										 ,@Cliente2_TiempoEspera= @Cliente2_TiempoEspera                           
										 ,@Cliente2_Contacto= @Cliente2_Contacto                                   
										 ,@Cliente2_CargaExclusiva= @Cliente2_CargaExclusiva                       
										 ,@Cliente2_IgnorarOperacion= @Cliente2_IgnorarOperacion                   
										 ,@Cliente2_CrearDomicilioOrden= @Cliente2_CrearDomicilioOrden             
										 ,@Cliente2_ActualizarDomicilioOrden= @Cliente2_ActualizarDomicilioOrden   
										 ,@Cliente2_ValidarDomicilioOrden= @Cliente2_ValidarDomicilioOrden         
										 ,@Pedido_Varchar1= @Pedido_Varchar1                                       
										 ,@Pedido_Varchar2= @Pedido_Varchar2                                       
										 ,@Pedido_Varchar3= @Pedido_Varchar3                                       
										 ,@Pedido_Varchar4= @Pedido_Varchar4                                       
										 ,@Pedido_Varchar5= @Pedido_Varchar5                                       
										 ,@Pedido_Varchar6= @Pedido_Varchar6                                       
										 ,@Pedido_CodigoSucursal= @Pedido_CodigoSucursal                           
										 ,@Pedido_TipoPedido= @Pedido_TipoPedido                                   
										 ,@Pedido_InicioHorario= @Pedido_InicioHorario                             
										 ,@Pedido_FinHorario1= @Pedido_FinHorario1                                 
										 ,@Pedido_TiempoEspera= @Pedido_TiempoEspera                               
										 ,@Pedido_Categoria= @Pedido_Categoria                                     
										 ,@Pedido_Prioridad= @Pedido_Prioridad                                     
										-- ,@Pedido_CargaExclusiva= @Pedido_CargaExclusiva                           
										 ,@depositoSalida_RefDepositoExterno= @depositoSalida_RefDepositoExterno   
										 ,@depositoLlegada_RefDepositoExterno= @depositoLlegada_RefDepositoExterno 
										 ,@PedidoItem_usarProductos= @PedidoItem_usarProductos
										-- NOTE insert items xml 
										 ,@xmlStrRow = @xmlStrRow
--										 ,@Items_Descripcion= @Items_Descripcion                                   
--										 ,@Items_RefDocumento= @Items_RefDocumento                                 
--										 ,@Items_Cantidad= @Items_Cantidad                                         
--										 ,@Items_Volumen= @Items_Volumen                                           
--										 ,@Items_Peso= @Items_Peso                                                 
--										 ,@Items_Bulto= @Items_Bulto                                               
--										 ,@Items_Pallets= @Items_Pallets                                           
										 ,@Pedido_agruparItems= @Pedido_agruparItems                               
--										 ,@Pedido_CodigoOperacion= @Pedido_CodigoOperacion                         
										 ,@Pedido2_CodigoSucursal= @Pedido2_CodigoSucursal                         
										 ,@Pedido_Unidades= @Pedido_Unidades                                       
										 ,@Pedido_usarProductos= @Pedido_usarProductos                             
										 ,@Pedido_soloInsertarProductos= @Pedido_soloInsertarProductos             
										 ,@Pedido2_agruparItems= @Pedido2_agruparItems                             
										 ,@Pedido_ValorDeclarado= @Pedido_ValorDeclarado                           
										 ,@Pedido_Float1= @Pedido_Float1                                           
										 ,@Pedido_Float2= @Pedido_Float2                                           
-- NOTE end test --		
-- DEBUG
--		select @postdata
-- DEBUG 
			fetch next from el_cursor into @element
			end
		close el_cursor
	 deallocate el_cursor

-- NOTE DEBUG final xml
/*				insert into ws.dbo.api_ws_historico_logs
				values  (	
									 cast(@postdata as nvarchar(max))
									,current_timestamp
									,current_timestamp
									,1
						    )
			  return
*/
set @postdata = cast( replace(replace(cast(@postdata as nvarchar(max)),'&gt;','>'),'&lt;','<') as xml )

-- NOTE DEBUG

--select @postData,@url,@Host,@soap,@action 
DECLARE @status int
DECLARE @responseText as table(responseText nvarchar(max))
DECLARE @res as Int
	-- This going to be a Constant parameter 
--NOTE DECLARE @url as nvarchar(1000) = 'https://cloudmx.unigis.com/8W/mapi/soap/logistic/Service.asmx' -- this must be in the params
declare @contentType NVARCHAR(164)
--declare @postData NVARCHAR(2000)
declare @soapAction nvarchar(800)
--NOTE declare @Host nvarchar(255)
--declare @action as nvarchar(900) = 'ConsultarViajePorFechas'
set @ContentType = 'text/xml; charset=utf-8'
-- NOTE Principal parameter this be build
set @soapAction = @soap+@action     --'ConsultarViajePorFechas' -- this in parameters
--NOTE set @Host = 'cloudmx.unigis.com'                            -- and this too

EXEC sp_OACreate 'MSXML2.ServerXMLHTTP', @res OUT
EXEC sp_OAMethod @res, 'open', NULL, 'POST',@url,'false'
EXEC sp_OAMethod @res, 'setRequestHeader', NULL, 'Content-type', @contentType
EXEC sp_OAMethod @res, 'setRequestHeader', NULL, 'SOAPAction', @soapAction
EXEC sp_OAMethod @res, 'setRequestHeader', NULL, 'Host', @Host
--EXEC sp_OAMethod @res, 'send'
EXEC sp_OAMethod @res, 'send', NULL, @postdata
EXEC sp_OAGetProperty @res, 'status', @status out

INSERT INTO @ResponseText (ResponseText) EXEC sp_OAGetProperty @res, 'responseText'
EXEC sp_OADestroy @res

--exec (@request)
/*
declare @respond nvarchar(max)
select @respond = responseText from @responseText 

		declare @xql nvarchar(max)
			set @xql = @request
		exec sp_executesql -- NOTE this is an automatic params 
											@xql
										 ,N'@status int, @responseText nvarchar(max) , @action nvarchar(900), @referenciaexterna nvarchar(10)'
										-- ,@postdata = @postdata output
										 ,@status = @status
										 ,@responseText = @respond
										 ,@action = @action
										-- NOTE this is automatically posible ?
										 ,@referenciaexterna = @referenciaexterna
*/
 insert into ws.dbo.ws_responses
-- NOTE Working
      select @status, @action ,responseText,'with Document: ' +  cast(@id_document as char(20)) + ' mode :' + cast(@ctrl_id as char(30)),@postdata,current_timestamp,current_timestamp,1 FROM @responseText
								


-- ================================================================================================================================================= --
-- TEST DEBUG Store 
-- ================================================================================================================================================= --
exec  [ws_sp_send_post_data_ps]
					 3					-- method CrearOrdenesPedido => 3
  				,371332     -- RefDocumento
  				,1					 -- Pedido => 1 or Solicitud => 2
					,5679


select * from ws.dbo.ws_responses
-- ================================================================================================================================================= --
-- TEST DEBUG Store 
-- ================================================================================================================================================= --

-- ================================================================================================================================================= --
	
-- ================================================================================================================================================= --
-- NOTE Triggers Section for Pedido
-- ================================================================================================================================================= --
--create

alter
trigger ws_tr_send_post_datap
	ON [pruebasdb2019].[dbo].[desp_pedido]
after update --insert,update,delete
as 
begin 
	set nocount on
    -- Check if this is an INSERT, UPDATE or DELETE Action.
    -- 
    DECLARE @action as char(1)

    set @action = 'I' -- Set Action to Insert by default.
    if exists(select * from deleted)
    begin
        set @action = 
            case
							when exists(select * from inserted) 
								then 'U' -- Set Action to Updated.
							else 'D' -- Set Action to Deleted.       
						end 
		end
	  else 
        if not exists(select * from inserted) RETURN -- Nothing updated or inserted.

--  declare vars 
	 declare @id_pedido as int ,@num_pedido as nvarchar(25), @id_pedidopk as int , @id_pedido_pk as int , @id_pedido_maestro as int 
-- select @var = "insert".var from inserted as "insert"
	 select 
	--				 @id_pedido = "in".id_pedido
					@num_pedido = "in".num_pedido
	 from 
					inserted as "in"

-- DEBUG 
				insert into ws.dbo.api_ws_historico_logs
				values  (	
									'@num_pedido => ' + @num_pedido + ' action => ' + @action
									,current_timestamp
									,current_timestamp
									,1
						    )
--set XACT_ABORT on   

--			begin TRY
--				begin transaction pedido -- initializa pedido
--/*
					execute  [ws].[dbo].[ws_sp_send_post_data_ps]
										 3					-- method CrearOrdenesPedido => 3
										,@num_pedido    -- RefDocumento
										,1					 -- Pedido => 1 or Solicitud => 2
										,5679
--*/										
--				commit transaction pedido
--			end TRY
/*		
			begin CATCH
		
			    if (XACT_STATE()) = -1  
			    begin  

		       	insert into ws.dbo.api_ws_historico_logs
						values (
										'The transaction is in an uncommittable state. desp.pedido: Rolling back transaction.'
										,current_timestamp
										,current_timestamp
										,1
									 )
					rollback transaction pedido
			    end  
			    -- Test whether the transaction is committable.  
			    if (XACT_STATE()) = 1  
			    begin  
		       	insert into ws.dbo.api_ws_historico_logs
						values (
											'The transaction is committable. desp.pedido: Committing transaction.'
											,current_timestamp
											,current_timestamp
											,1
									 )  
			        commit transaction pedido
			    end   
		    
				insert into ws.dbo.api_ws_historico_logs (message)
				values (	
							'errNum : ' + cast(ERROR_NUMBER() as varchar(255)) + '| ' +
							'errLine : ' + cast(ERROR_LINE() as varchar(500)) + '| ' +
							'errMsg : ' + cast(ERROR_MESSAGE() as varchar(500)) + '| ' +
							'errState : ' + cast(ERROR_STATE() as varchar(255)) + '| ' +
							'errSeverity : ' + cast(ERROR_SEVERITY() as varchar(255)) + '| '
					   )
			
			end CATCH
*/
end --trigger


-- ================================================================================================================================================= --
-- NOTE Triggers Section for Solicitud
-- ================================================================================================================================================= --
alter
--create 
trigger ws_tr_send_post_datas
	ON [pruebasdb2019].[dbo].[cons_solicitud]
after update
as 
begin 
	set nocount on
    -- Check if this is an INSERT, UPDATE or DELETE Action.
    -- 
    DECLARE @action as char(1)

    set @action = 'I' -- Set Action to Insert by default.
    if exists(select * from deleted)
    begin
        set @action = 
            case
							when exists(select * from inserted) 
								then 'U' -- Set Action to Updated.
							else 'D' -- Set Action to Deleted.       
						end 
		end
	  else 
        if not exists(select * from inserted) RETURN -- Nothing updated or inserted.

--  declare vars 
	 declare @num_pedido as nvarchar(25)
-- select @var = "insert".var from inserted as "insert"
	 select 
--					 @id_pedido = "in".id_solicitud
					 @num_pedido = "in".id_solicitud
	 from 
					inserted as "in"

--build the squema with procedures 							
--set XACT_ABORT on   
-- DEBUG 
				insert into ws.dbo.api_ws_historico_logs
				values  (	
									'@num_pedido => ' + @num_pedido  + ' action => ' + cast( @action as varchar(25) )
									,current_timestamp
									,current_timestamp
									,1
						    ) 

--			begin TRY
--				begin transaction pedido -- initializa pedido

					execute   [ws].[dbo].[ws_sp_send_post_data_ps]
										 3					-- method CrearOrdenesPedido => 3
										,@num_pedido     -- RefDocumento
										,2					 -- Pedido => 1 or Solicitud => 2
										,5679
										
	--			commit transaction pedido
	--		end TRY
/*		
			begin CATCH
		
			    if (XACT_STATE()) = -1  
			    begin  

		       	insert into ws.dbo.api_ws_historico_logs (message)
						values ('The transaction is in an uncommittable state. desp.pedido: Rolling back transaction.')
					rollback transaction pedido
			    end  
			    -- Test whether the transaction is committable.  
			    if (XACT_STATE()) = 1  
			    begin  
		       	insert into ws.dbo.api_ws_historico_logs (message)
						values ('The transaction is committable. desp.pedido: Committing transaction.')  
			        commit transaction pedido
			    end   
		    
				insert into ws.dbo.api_ws_historico_logs (message)
				values (	
							'errNum : ' + cast(ERROR_NUMBER() as varchar(255)) + '| ' +
							'errLine : ' + cast(ERROR_LINE() as varchar(500)) + '| ' +
							'errMsg : ' + cast(ERROR_MESSAGE() as varchar(500)) + '| ' +
							'errState : ' + cast(ERROR_STATE() as varchar(255)) + '| ' +
							'errSeverity : ' + cast(ERROR_SEVERITY() as varchar(255)) + '| '
					   )
			
			end CATCH
	*/
end --trigger


