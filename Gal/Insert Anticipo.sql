-- download re-revelations

-- https://1fichier.com/?1s6u51dyadtdiq9xmo1h&af=62851
-- https://1fichier.com/?ghijn07jx0usjzjxb2ji&af=62851
-- https://1fichier.com/?20ouwa9nug5mqwrb2mic&af=62851

insert into
  galdb.dbo.trafico_anticipo
      (
         id_area
        ,no_anticipo
        ,no_viaje
        ,fecha_anticipo
        ,monto_anticipo
        ,status_anticipo
        ,monto_anticipo_iva
        ,id_ingreso
        ,fecha_ingreso
        ,observaciones
        ,id_area_liq
        ,transfer_nomina
        ,monto_devo
        ,tipo_anticipo
        ,id_operador
        ,no_viajeant
        ,id_asignacion
        ,id_areaviaje
        ,id_plazaorigen
        ,id_areapago
        ,clasificacion
        ,rebasa_presupuesto
        ,tipo_autorizacion
        ,id_compania
        ,impreso
        ,tipo_anticipo_convenio
        ,id_ingreso_antext
      )
values
      (
        1                         -- id_area
       ,50775                     -- no_anticipo
       ,117305                    -- no_viaje
       ,'2019-08-30 00:00:00'     -- fecha_anticipo
       ,100                       -- monto_anticipo
       ,'A'                       -- status_anticipo
       ,0                         -- monto_anticipo_iva
       ,'SA'                      -- id_ingreso
       ,'2019-11-02 13:58:49'     -- fecha_ingreso
       ,''                        -- observaciones
       ,0                         -- id_area_liq
       ,0                         -- transfer_nomina
       ,0                         -- monto_devo
       ,0                         -- tipo_anticipo
       ,995                       -- id_operador
       ,0                         -- no_viajeant
       ,118480                    -- id_asignacion
       ,1                         -- id_areaviaje
       ,0                         -- id_plazaorigen
       ,1                         -- id_areapago
       ,0                         -- clasificacion
       ,0                         -- rebasa_presupuesto
       ,0                         -- tipo_autorizacion
       ,0                         -- id_compania
       ,0                         -- impreso
       ,'E'                       -- tipo_anticipo_convenio
       ,'UNIGIS'                  -- id_ingreso_antext
      )

-- 2nd step
insert into
  galdb.dbo.trafico_renglon_anticipo
      (
         id_area
        ,no_anticipo
        ,id_concepto
        ,importe_anticipo
        ,iva_anticipo
        ,cantidad
      )
values
      (
         1        -- id_area
        ,50775    -- no_anticipo
        ,11       -- id_concepto
        ,100      -- importe_anticipo
        ,0        -- iva_anticipo
        ,1        -- cantidad
      )
