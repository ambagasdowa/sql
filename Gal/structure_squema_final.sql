
SELECT 'Before Staring any Transaction', @@TRANCOUNT 

BEGIN TRANSACTION TRAN1
	
	SELECT 'After Staring First Transaction', @@TRANCOUNT 

	SAVE TRAN TRAN2 	
		SELECT 'Within the Save Transaction', @@TRANCOUNT 
	ROLLBACK TRAN TRAN2

	SELECT 'After Rollback th Save Transaction', @@TRANCOUNT 

COMMIT TRANSACTION TRAN1

SELECT 'After the First Transaction is Commited', @@TRANCOUNT


--save examle 
SELECT 'Before Staring any Transaction', @@TRANCOUNT 

BEGIN TRANSACTION TRAN1
	
	SELECT 'After Staring First Transaction', @@TRANCOUNT 

	SAVE TRAN TRAN2 	
		SELECT 'Within the Save Transaction', @@TRANCOUNT 
	ROLLBACK TRAN TRAN2

	SELECT 'After Rollback th Save Transaction', @@TRANCOUNT 

COMMIT TRANSACTION TRAN1

SELECT 'After the First Transaction is Commited', @@TRANCOUNT


   				 id_area                  int null
                ,id_ruta                  int null
                ,id_configuracionviaje    int null
                ,id_personal              int null
                ,id_unidad                varchar(10) null
                ,id_remolque1             varchar(20) null
                ,id_remolque2             varchar(20) null
                ,id_dolly                 varchar(10) null
                ,id_ingreso               varchar(8) null
                ,no_viaje                 int null
                ,fecha_ingreso            datetime null
                ,f_prog_ini_viaje         datetime null
                ,f_prog_fin_viaje         datetime null
                ,kms_camion_vacio         int null
                ,kms_camion_lleno         int null
                ,kms_operador             int null
                ,lts_viaje                decimal(18,6) null
                ,rendimiento_viaje        decimal(18,6) null
                ,rendimiento_real         decimal(18,6) null
                ,status_viaje             char(1) null
                ,sueldo_operador          decimal(18,6) null
                ,compensaciones           decimal(18,6) null
                ,lts_empresa              decimal(18,6) null
                ,rend_esperado            decimal(18,6) null
                ,status_desp              char(1) null
                ,viajeactual              char(1) null
                ,status_cs                varchar(1) null
                ,id_origen                int null 
                ,id_destino               int null
                ,id_linearem1             varchar(10) null
                ,id_linearem2             varchar(10) null
                ,id_statusviaje           int null
                ,id_contrato              int null
                ,id_tipo_servicio         int null
                
                
-- =========================================================================== --
--Build the base tables 
-- =========================================================================== --
-- select * from galdb.dbo.api_gal_viajes
use [galdb]

IF OBJECT_ID('galdb.dbo.api_gal_viajes', 'U') IS NOT NULL 
  DROP TABLE galdb.dbo.api_gal_viajes
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on
-- go
create table galdb.dbo.api_gal_viajes(
		 id 								bigint 			identity(1,1) not null	
 		,sie_ctrl 							int				null
		,api_ctrl 							int				null 
		,api_id_ruta 						int				null 
		,asignacion_num_asignacion 			varchar(20)		null
		,asignacion_id_configuracionviaje 	int				null
		,api_id_personal 					int				null
		,api_id_unidad 						varchar(10)		null
		,api_id_remolque1 					varchar(20)		null
		,api_id_remolque2 					varchar(20)		null
		,api_id_dolly 						varchar(20)		null
		,api_no_viaje 						int				null
		,api_fecha_ingreso 					datetime		null
		,api_f_prog_ini_viaje 				datetime		null
		,api_f_prog_fin_viaje 				datetime		null
		,asignacion_id_seguimiento 			int				null
		,asignacion_kms_ruta 				int 			null
		,api_id_remitente 					int				null
		,api_id_destinatario 				int				null
		,viaje_kms_camion_vacio 			int				null
		,viaje_kms_camion_lleno 			int				null
		,viaje_kms_operador 				int				null
		,api_id_origen 						int				null
		,api_id_destino 					int 			null
		,pedido_id_producto 				int				null
		,pedido_f_pedido 					datetime		null
		,pedido_id_cliente 					int				null
		-- start anticipo
		,anticipo_fecha_anticipo 			datetime		null 
		,anticipo_monto_anticipo			decimal(18,6)	null
		,anticipo_monto_anticipo_iva		decimal(18,6)	null
		,anticipo_fecha_ingreso				datetime		null
		,anticipo_observaciones				varchar(250)	null
		-- start CapcomPowerSystem
		,guia_num_guia						varchar(255)	null
		,guia_fecha_guia					datetime		null
		,guia_flete							decimal(18,6)   null
		,guia_otro							decimal(18,6)   null
		,guia_subtotal						decimal(18,6)   null
		,guia_iva_guia						decimal(18,6)   null
		,guia_monto_retencion				decimal(18,6)   null
		,guia_id_serieguia					int 			null
		,guia_id_iva						int 			null
		,guia_id_retencion					int 			null
		,guia_id_metodo_pago				int 			null
		,guia_no_cuenta						varchar(25)		null
		,renglon_id_producto				int 			null
		,renglon_id_fraccion				int 			null
		,otro_monto_iva_otro				decimal(18,6)	null
		,otro_desc_otro						varchar(100)	null
		,otro_id_otro						int 			null
) on [primary]
-- go
set ansi_padding off


select 
 "api".id
,"api".sie_ctrl
,"api".api_ctrl
,"api".api_id_ruta
,"api".asignacion_num_asignacion
,"api".asignacion_id_configuracionviaje
,"api".api_id_personal
,"api".api_id_unidad
,"api".api_id_remolque1
,"api".api_id_remolque2
,"api".api_id_dolly
,"api".api_no_viaje
,"api".api_fecha_ingreso
,"api".api_f_prog_ini_viaje
,"api".api_f_prog_fin_viaje
,"api".asignacion_id_seguimiento
,"api".asignacion_kms_ruta
,"api".api_id_remitente
,"api".api_id_destinatario
,"api".viaje_kms_camion_vacio
,"api".viaje_kms_camion_lleno
,"api".viaje_kms_operador
,"api".api_id_origen
,"api".api_id_destino
,"api".pedido_id_producto
,"api".pedido_f_pedido
,"api".pedido_id_cliente
-- start anticipo
,"api".anticipo_fecha_anticipo
,"api".anticipo_monto_anticipo
,"api".anticipo_monto_anticipo_iva
,"api".anticipo_fecha_ingreso
,"api".anticipo_observaciones
-- start CapcomPowerSystem
,"api".guia_fecha_guia
,"api".guia_flete
,"api".guia_otro
,"api".guia_subtotal
,"api".guia_iva_guia
,"api".guia_monto_retencion
,"api".guia_id_serieguia
,"api".guia_id_iva
,"api".guia_id_retencion
,"api".guia_id_metodo_pago
,"api".guia_no_cuenta
,"api".renglon_id_producto
,"api".renglon_id_fraccion
,"api".otro_monto_iva_otro
,"api".otro_desc_otro
,"api".otro_id_otro
from 
	galdb.dbo.api_gal_viajes as "api"


select * from galdb.dbo.api_gal_viajes

select * from galdb.dbo.api_gal_viajes_logs

-- =========================================================================== --
-- Build neccesary data   select * from galdb.dbo.api_gal_viajes_logs
-- =========================================================================== --


IF OBJECT_ID('galdb.dbo.api_gal_viajes_logs', 'U') IS NOT NULL 
  DROP TABLE galdb.dbo.api_gal_viajes_logs
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on
-- go
create table "api_gal_viajes_logs" (
						 id			bigint 			identity(1,1)  	-- add identity
						,sie_ctrl	int	null	--Identificador del Evento:  • 0
						,api_ctrl	int	null	--Identificador del Evento:  • 0
--						id	int	null	--Identificador consecutivo de la tabla.
						,asignacion_id_asignacion	int	null	--Identificador consecutivo autogenerado en LIS
						,asignacion_num_asignacion	varchar(20)	null --	Identificador consecutivo autogenerado en LIS
						,api_id_area	int	null --Identificador del Evento: • 1 | Área definida en LIS 
						,api_id_ruta	int	null --	Identificador de ruta recorrida (pendiente definición)
						,asignacion_id_configuracionviaje	int	null --	Identificador del Evento: • 1 | Unidad Motriz • 2 | Sencillo • 3 | Full
						,asignacion_id_operador2	int	null --	Identificador del Evento: • NULL
						,api_id_personal	int	null --	Numero de empleado LIS, que realizo el viaje.
						,api_id_unidad	varchar(10)	null --	Identificador de unidad LIS, que realizo el viaje.
						,api_id_remolque1	varchar(20)	null --	Identificador de remolque 1 LIS, que realizo el viaje.
						,api_id_remolque2	varchar(20)	null --	Identificador de remolque 2 LIS, que realizo el viaje.
						,api_id_dolly	varchar(10)	null --	Identificador de dolly LIS, que realizo el viaje.
						,api_id_ingreso	varchar(8)	null --	Identificador del Evento (se creará el usuario):  • UNIGIS
						,api_no_viaje	int	null --	Identificador consecutivo autogenerado en LIS
						,api_fecha_ingreso	datetime	null --	Fecha y hora de registro del viaje:  • YYYY-MM-DD HH:MM:SS 
						,api_f_prog_ini_viaje	datetime	null --	Fecha y hora de registro del viaje:  • YYYY-MM-DD HH:MM:SS 
						,api_f_prog_fin_viaje	datetime	null --	Fecha y hora de registro del viaje:  • YYYY-MM-DD HH:MM:SS 
						,asignacion_status_asignacion	int	null --	Identificador del Evento:  • 3 | Viaje terminado
						,asignacion_seguimiento_actual	int	null --	Identificador del Evento:  • 0
						,asignacion_observaciones	varchar(255)	null --	Identificador del Evento: • NULL
						,asignacion_id_flota	int	null --	Identificador del Evento:  • 1 | Local • 2 | Foráneo
						,asignacion_id_seguimiento	int	null --	Identificador del Evento:  • 1 | Cargado • 2 | Vacío
						,asignacion_no_kit	int	null --	Identificador del Evento: • NULL
						,api_id_tipo_operacion	int	null --	Identificador del Evento:  • 0
						,asignacion_kms_ruta	int	null --	Registro de kilómetros de la ruta-viaje.
						,asignacion_tiempo_ruta	int	null --	Identificador del Evento: • NULL
						,asignacion_sist_origen	varchar(4)	null --	Identificador del Evento:  • ZAM
						,api_id_remitente	int	null --	Numero de cliente LIS.
						,api_id_destinatario	int	null --	Numero de cliente LIS.
						,viaje_kms_camion_vacio	int	null --	Registro de kilómetros de la ruta-viaje en caso de que sea cargado, sino 0
						,viaje_kms_camion_lleno	int	null --	Registro de kilómetros de la ruta-viaje en caso de que sea vacio, sino 0
						,viaje_kms_operador	int	null --	Registro de kilómetros de la ruta-viaje.
						,viaje_lts_viaje	decimal(18, 6)	null --	Identificador del Evento:  • 0.0000
						,viaje_rendimiento_viaje	decimal(18, 6)	null --	Identificador del Evento:  • 0.0000
						,viaje_rendimiento_real	decimal(18, 6)	null --	Identificador del Evento:  • 0.0000
						,viaje_status_viaje	char(1)	null --	Identificador del Evento:  • C
						,viaje_sueldo_operador	decimal(18, 6)	null --	Identificador del Evento:  • 0.0000
						,viaje_compensaciones	decimal(18, 6)	null --	Identificador del Evento:  • 0.0000
						,viaje_lts_empresa	decimal(18, 6)	null --	Identificador del Evento:  • 0.0000
						,viaje_rend_esperado	decimal(18, 6)	null --	Identificador del Evento:  • 0.0000
						,viaje_status_desp	char(1)	null --	Identificador del Evento:  • T
						,viaje_viajeactual	char(1)	null --	Identificador del Evento: • N
						,viaje_status_cs	varchar(1)	null --	Identificador del Evento:  • E
						,api_id_origen	int	null --	 Numero de plaza LIS (pendiente definición).
						,api_id_destino	int	null --	 Numero de plaza LIS (pendiente definición).
						,api_id_linearem1	varchar(10)	null --	Identificador del Evento:  • ZAM
						,api_id_linearem2	varchar(10)	null --	Identificador del Evento:  • ZAM
						,viaje_id_statusviaje	int	null --	Identificador del Evento:  • 6
						,api_id_contrato	int	null --	Identificador del Evento:  • 0
						,viaje_observacionesviaje	varchar(255)	null --	Identificador del Evento: • NULL
						,viaje_id_tipo_servicio	int	null --	Identificador del Evento:  • 0
					    ,viaje_id_tramo		int null --	Identificador del Evento:  • 0
					    ,viaje_consecutivo_viaje decimal(18,6) null --Identificador del Evento:  • 1.0000
					    ,viaje_status_km	varchar(10) null --Identificador del Evento:  • ''
					    ,viaje_direccion_tramo int null --Identificador del Evento:  • 1
				    	,viaje_ramal	int null --Identificador del Evento:  • 0
				    	,viaje_desc_tramo varchar(255) null --descripcion de la ruta 
				    	,viaje_kms_tramo	int null --Registro de kilometros de la ruta-viaje
				    	,viaje_vol_recuperado	varchar(255) null --Identificador del Evento:  • NULL
						,pedido_id_pedido	int	null --	Identificador consecutivo autogenerado en LIS
						,pedido_status_pedido	int	null --	Identificador del Evento:  • 3
						,pedido_id_producto	int	null --	Identificador del Evento:  • 1
						,pedido_tipo_serv	char(1)	null --	Identificador del Evento:  • D
						,pedido_f_pedido	datetime	null --	Fecha y hora de registro del pedido:  • YYYY-MM-DD HH:MM:SS 
						,pedido_no_remision	varchar(30)	null --	Identificador del Evento: • NULL
						,pedido_id_cliente	int	null --	Numero de cliente LIS.
						,pedido_observaciones_pedido	varchar(50)	null --	Identificador del Evento: • NULL
						,pedido_id_convenio	int	null --	Identificador del Evento:  • 0
						,pedido_fecha_modifico	datetime	null --	Fecha y hora de registro del pedido:  • YYYY-MM-DD HH:MM:SS 
						,pedido_id_modifico	varchar(8)	null --	Identificador del Evento (se creará el usuario):  • UNIGIS
						,pedido_id_areaconvenio	int	null --	Identificador del Evento:  • 0
						,pedido_no_guia	int	null --	Identificador del Evento:  • 0
						,pedido_f_real_entrega	datetime	null --	Fecha y hora de registro del pedido:  • YYYY-MM-DD HH:MM:SS 
						,pedido_sist_origen	varchar(4)	null --	Identificador del Evento:  • ZAM
						,pedido_id_pedido_maestro	int	null --	Identificador del Evento:  • 0
						,pedido_id_area_guia	int	null --	Identificador del Evento:  • 1
						,pedido_id_tipo_cancelacion	int	null --	Identificador del Evento:  • 0
						,pedido_num_pedido	varchar(25)	null --	Identificador consecutivo autogenerado en LIS
						,pedido_por_cargar	int	null --	Identificador del Evento:  • 1
						,pedido_cargado	int	null --	Identificador del Evento:  • 0
						,pedido_pedido_creado	int	null --	Identificador del Evento:  • 0
						,pedido_id_solicitud	int	null --	I///dentificador del Evento:  • 0
						,pedido_tarifa	decimal(18, 6)	null --	Identificador del Evento:  • 0
						,pedido_id_autorizo	varchar(8)	null --	Identificador del Evento (se creará el usuario):  • UNIGIS
						,pedido_f_desp_prog	datetime	null --	Fecha y hora de registro del pedido:  • YYYY-MM-DD HH:MM:SS 
						,pedido_id_tipo_unidad	int	null --	Identificador del Evento:  • 0
						,pedido_id_clienteFinal	int	null --	Identificador del Evento:  • 0
						,pedido_id_pedidopk	int	null --	Identificador consecutivo autogenerado en LIS
						-- start anticipo
						,anticipo_no_anticipo			int 			null
						,anticipo_fecha_anticipo 		datetime		null 
						,anticipo_monto_anticipo		decimal(18,6)	null
						,anticipo_monto_anticipo_iva	decimal(18,6)	null
						,anticipo_fecha_ingreso			datetime		null
						,anticipo_observaciones			varchar(250)	null
						-- start CapcomPowerSystem
						,guia_num_guia					varchar(255)	null
						,guia_fecha_guia				datetime		null
						,guia_flete						decimal(18,6)   null
						,guia_otro						decimal(18,6)   null
						,guia_subtotal					decimal(18,6)   null
						,guia_iva_guia					decimal(18,6)   null
						,guia_monto_retencion			decimal(18,6)   null
						,guia_id_serieguia				int 			null
						,guia_id_iva					int 			null
						,guia_id_retencion				int 			null
						,guia_id_metodo_pago			int 			null
						,guia_no_cuenta					varchar(25)		null
						,renglon_id_producto			int 			null
						,renglon_id_fraccion			int 			null
						,otro_monto_iva_otro			decimal(18,6)	null
						,otro_desc_otro					varchar(100)	null
						,otro_id_otro					int 			null
						-- inner control
						,created			 			datetime		null   	-- created date
						,modified			 			datetime		null  	-- if modified
						,status				 			tinyint default 1 null 	-- set active or inactive
) on [primary]
-- go
set ansi_padding off

-- =========================================================================== --
-- Logs   select * from galdb.dbo.api_ws_historico_logs
-- =========================================================================== --

IF OBJECT_ID('dbo.api_ws_historico_logs', 'U') IS NOT NULL 
  DROP TABLE dbo.api_ws_historico_logs
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on
-- go
create table dbo.api_ws_historico_logs(
		 id					 bigint					identity(1,1)
		,message		     text			    	null
		,created			 datetime 				default current_timestamp null
		,modified			 datetime				null
		,status				 tinyint 				default 1 null
) on [primary]
-- go
set 0pansi_padding off

-- =========================================================================== --
-- Viajes trigger
-- =========================================================================== --

use [galdb]

alter trigger trigger_insert_api_gal_viajes
	ON galdb.dbo.api_gal_viajes
after insert
as 

begin 
	set nocount on
	
	declare 
 		 @sie_ctrl 							int
		,@api_ctrl 							int 
		,@api_id_ruta 						int 
		,@asignacion_num_asignacion 		varchar(20)
		,@asignacion_id_configuracionviaje 	int
		,@api_id_personal 					int
		,@api_id_unidad 					varchar(10)
		,@api_id_remolque1 					varchar(20)
		,@api_id_remolque2 					varchar(20)
		,@api_id_dolly 						varchar(20)
		,@api_no_viaje 						int
		,@api_fecha_ingreso 				datetime
		,@api_f_prog_ini_viaje 				datetime
		,@api_f_prog_fin_viaje 				datetime
		,@asignacion_id_seguimiento 		int
		,@asignacion_kms_ruta 				int 
		,@api_id_remitente 					int
		,@api_id_destinatario 				int
		,@viaje_kms_camion_vacio 			int
		,@viaje_kms_camion_lleno 			int
		,@viaje_kms_operador 				int
		,@api_id_origen 					int
		,@api_id_destino 					int 
		,@pedido_id_producto 				int
		,@pedido_f_pedido 					datetime
		,@pedido_id_cliente 				int
		-- Start anticipo
		,@anticipo_fecha_anticipo 			datetime		 
		,@anticipo_monto_anticipo			decimal(18,6)	
		,@anticipo_monto_anticipo_iva		decimal(18,6)	
		,@anticipo_fecha_ingreso			datetime		
		,@anticipo_observaciones			varchar(250)	
		-- start CapcomPowerSystem
		,@guia_num_guia						varchar(255)	
		,@guia_fecha_guia					datetime		
		,@guia_flete						decimal(18,6)   
		,@guia_otro							decimal(18,6)   
		,@guia_subtotal						decimal(18,6)   
		,@guia_iva_guia						decimal(18,6)   
		,@guia_monto_retencion				decimal(18,6)   
		,@guia_id_serieguia					int 			
		,@guia_id_iva						int 			
		,@guia_id_retencion					int 			
		,@guia_id_metodo_pago				int 			
		,@guia_no_cuenta					varchar(25)		
		,@renglon_id_producto				int 			
		,@renglon_id_fraccion				int 			
		,@otro_monto_iva_otro				decimal(18,6)	
		,@otro_desc_otro					varchar(100)	
		,@otro_id_otro						int 			
	
	declare 
			 @id_asignacion int
			,@num_asignacion varchar(20)
		    ,@num_pedido varchar(25)
	    	,@id_pedidopk int
	    	,@no_viaje int
	    	,@ctrl_log_id int
	    	,@ctrl_pedido_id int 
	    	-- Anticipo Vars
	    	,@no_anticipo int
	    	-- Cartas Porte
	    	,@no_guia int
	    
	declare 
			@api_id_ingreso varchar(20)
	
-- CURRENT WORK FOR ADD THE LAST METHODS IN WS		

-- 	set @ins_id = (select "in".id from inserted as "in" where "in".status = 2 group by "in".id)

--	set the variables 
 	
 	select 
	 	 @sie_ctrl = "insert".sie_ctrl
		,@api_ctrl = "insert".api_ctrl 
		,@api_id_ruta = "insert".api_id_ruta
		,@asignacion_num_asignacion = "insert".asignacion_num_asignacion
		,@asignacion_id_configuracionviaje = "insert".asignacion_id_configuracionviaje
		,@api_id_personal = "insert".api_id_personal
		,@api_id_unidad = "insert".api_id_unidad
		,@api_id_remolque1 = "insert".api_id_remolque1
		,@api_id_remolque2 = "insert".api_id_remolque2
		,@api_id_dolly = "insert".api_id_dolly
		,@api_no_viaje = "insert".api_no_viaje
		,@api_fecha_ingreso = "insert".api_fecha_ingreso
		,@api_f_prog_ini_viaje = "insert".api_f_prog_ini_viaje
		,@api_f_prog_fin_viaje = "insert".api_f_prog_fin_viaje
		,@asignacion_id_seguimiento = "insert".asignacion_id_seguimiento
		,@asignacion_kms_ruta = "insert".asignacion_kms_ruta 
		,@api_id_remitente = "insert".api_id_remitente
		,@api_id_destinatario = "insert".api_id_destinatario
		,@viaje_kms_camion_vacio = "insert".viaje_kms_camion_vacio
		,@viaje_kms_camion_lleno = "insert".viaje_kms_camion_lleno
		,@viaje_kms_operador = "insert".viaje_kms_operador
		,@api_id_origen = "insert".api_id_origen
		,@api_id_destino = "insert".api_id_destino 
		,@pedido_id_producto = "insert".pedido_id_producto
		,@pedido_f_pedido = "insert".pedido_f_pedido
		,@pedido_id_cliente = "insert".pedido_id_cliente
		-- Start anticipo
		,@anticipo_fecha_anticipo = "insert".anticipo_fecha_anticipo		 
		,@anticipo_monto_anticipo = "insert".anticipo_monto_anticipo
		,@anticipo_monto_anticipo_iva = "insert".anticipo_monto_anticipo_iva
		,@anticipo_fecha_ingreso = "insert".anticipo_fecha_ingreso
		,@anticipo_observaciones = "insert".anticipo_observaciones
		-- start CapcomPowerSystem
		,@guia_num_guia = "insert".guia_num_guia
		,@guia_fecha_guia = "insert".guia_fecha_guia		
		,@guia_flete = "insert".guia_flete   
		,@guia_otro = "insert".guia_otro    
		,@guia_subtotal = "insert".guia_subtotal   
		,@guia_iva_guia = "insert".guia_iva_guia   
		,@guia_monto_retencion = "insert".guia_monto_retencion    
		,@guia_id_serieguia = "insert".guia_id_serieguia
		,@guia_id_iva = "insert".guia_id_iva	
		,@guia_id_retencion = "insert".guia_id_retencion					 			
		,@guia_id_metodo_pago = "insert".guia_id_metodo_pago				 			
		,@guia_no_cuenta = "insert".guia_no_cuenta
		,@renglon_id_producto = "insert".renglon_id_producto
		,@renglon_id_fraccion = "insert".renglon_id_fraccion
		,@otro_monto_iva_otro = "insert".otro_monto_iva_otro
		,@otro_desc_otro = "insert".otro_desc_otro 
		,@otro_id_otro = "insert".otro_id_otro
	from 
		inserted  as "insert"
	
--NOTE Internal id		
	-- check the type of squema 
	
		
--	define the insert squema 
		-- ================================================================= --
		
		-- ================================================================= --


		
if @sie_ctrl = 1 
	begin
		set @id_asignacion = (select max(id_asignacion)+1 from galdb.dbo.desp_asignacion)
		set @num_asignacion = right(replicate('0', len(@id_asignacion)+1) + ltrim(@id_asignacion), len(@id_asignacion)+1) 
		set @num_pedido = (select max(cast(num_pedido as int))+1 from galdb.dbo.desp_pedido)
	-- /// Set calcualte the next pk id 	
		set @id_pedidopk = (select max(cast(id_pedidopk as int))+1 from galdb.dbo.desp_pedido)
	--	create a rosseta table for no_viaje 
		set @no_viaje = (select max(cast(no_viaje as int))+1 from galdb.dbo.trafico_viaje)		
	end

-- Maybe need some validations like validate if viaje exits or if viaje is null 
	
	if @sie_ctrl = 2
		begin
			set @no_anticipo = ( select max(cast(no_anticipo as int))+1 from galdb.dbo.trafico_anticipo )		
		end
	else 
		begin
			set @no_anticipo = 0
		end

	if @sie_ctrl = 3
	begin
		set @no_guia = (select max(cast(no_guia as int))+1 from galdb.dbo.trafico_guia)		
	end

--select num_asignacion from galdb.dbo.desp_asignacion where id_asignacion = 118544

--	set nocount on
	
	declare @ws table (
--					note
						 sie_ctrl	int	null	--Identificador del Evento:  • 0
						,api_ctrl	int	null	--Identificador del Evento:  • 0
--						id	int	null	--Identificador consecutivo de la tabla.
						,asignacion_id_asignacion	int	null	--Identificador consecutivo autogenerado en LIS
						,asignacion_num_asignacion	varchar(20)	null --	Identificador consecutivo autogenerado en LIS
						,api_id_area	int	null --Identificador del Evento: • 1 | Área definida en LIS 
						,api_id_ruta	int	null --	Identificador de ruta recorrida (pendiente definición)
						,asignacion_id_configuracionviaje	int	null --	Identificador del Evento: • 1 | Unidad Motriz • 2 | Sencillo • 3 | Full
						,asignacion_id_operador2	int	null --	Identificador del Evento: • NULL
						,api_id_personal	int	null --	Numero de empleado LIS, que realizo el viaje.
						,api_id_unidad	varchar(10)	null --	Identificador de unidad LIS, que realizo el viaje.
						,api_id_remolque1	varchar(20)	null --	Identificador de remolque 1 LIS, que realizo el viaje.
						,api_id_remolque2	varchar(20)	null --	Identificador de remolque 2 LIS, que realizo el viaje.
						,api_id_dolly	varchar(10)	null --	Identificador de dolly LIS, que realizo el viaje.
						,api_id_ingreso	varchar(8)	null --	Identificador del Evento (se creará el usuario):  • UNIGIS
						,api_no_viaje	int	null --	Identificador consecutivo autogenerado en LIS
						,api_fecha_ingreso	datetime	null --	Fecha y hora de registro del viaje:  • YYYY-MM-DD HH:MM:SS 
						,api_f_prog_ini_viaje	datetime	null --	Fecha y hora de registro del viaje:  • YYYY-MM-DD HH:MM:SS 
						,api_f_prog_fin_viaje	datetime	null --	Fecha y hora de registro del viaje:  • YYYY-MM-DD HH:MM:SS 
						,asignacion_status_asignacion	int	null --	Identificador del Evento:  • 3 | Viaje terminado
						,asignacion_seguimiento_actual	int	null --	Identificador del Evento:  • 0
						,asignacion_observaciones	varchar(255)	null --	Identificador del Evento: • NULL
						,asignacion_id_flota	int	null --	Identificador del Evento:  • 1 | Local • 2 | Foráneo
						,asignacion_id_seguimiento	int	null --	Identificador del Evento:  • 1 | Cargado • 2 | Vacío
						,asignacion_no_kit	int	null --	Identificador del Evento: • NULL
						,api_id_tipo_operacion	int	null --	Identificador del Evento:  • 0
						,asignacion_kms_ruta	int	null --	Registro de kilómetros de la ruta-viaje.
						,asignacion_tiempo_ruta	int	null --	Identificador del Evento: • NULL
						,asignacion_sist_origen	varchar(4)	null --	Identificador del Evento:  • ZAM
						,api_id_remitente	int	null --	Numero de cliente LIS.
						,api_id_destinatario	int	null --	Numero de cliente LIS.
						,viaje_kms_camion_vacio	int	null --	Registro de kilómetros de la ruta-viaje en caso de que sea cargado, sino 0
						,viaje_kms_camion_lleno	int	null --	Registro de kilómetros de la ruta-viaje en caso de que sea vacio, sino 0
						,viaje_kms_operador	int	null --	Registro de kilómetros de la ruta-viaje.
						,viaje_lts_viaje	decimal(18, 6)	null --	Identificador del Evento:  • 0.0000
						,viaje_rendimiento_viaje	decimal(18, 6)	null --	Identificador del Evento:  • 0.0000
						,viaje_rendimiento_real	decimal(18, 6)	null --	Identificador del Evento:  • 0.0000
						,viaje_status_viaje	char(1)	null --	Identificador del Evento:  • C
						,viaje_sueldo_operador	decimal(18, 6)	null --	Identificador del Evento:  • 0.0000
						,viaje_compensaciones	decimal(18, 6)	null --	Identificador del Evento:  • 0.0000
						,viaje_lts_empresa	decimal(18, 6)	null --	Identificador del Evento:  • 0.0000
						,viaje_rend_esperado	decimal(18, 6)	null --	Identificador del Evento:  • 0.0000
						,viaje_status_desp	char(1)	null --	Identificador del Evento:  • T
						,viaje_viajeactual	char(1)	null --	Identificador del Evento: • N
						,viaje_status_cs	varchar(1)	null --	Identificador del Evento:  • E
						,api_id_origen	int	null --	 Numero de plaza LIS (pendiente definición).
						,api_id_destino	int	null --	 Numero de plaza LIS (pendiente definición).
						,api_id_linearem1	varchar(10)	null --	Identificador del Evento:  • ZAM
						,api_id_linearem2	varchar(10)	null --	Identificador del Evento:  • ZAM
						,viaje_id_statusviaje	int	null --	Identificador del Evento:  • 6
						,api_id_contrato	int	null --	Identificador del Evento:  • 0
						,viaje_observacionesviaje	varchar(255)	null --	Identificador del Evento: • NULL
						,viaje_id_tipo_servicio	int	null --	Identificador del Evento:  • 0
						,viaje_id_tramo		int null --	Identificador del Evento:  • 0
					    ,viaje_consecutivo_viaje decimal(18,6) null --Identificador del Evento:  • 1.0000
					    ,viaje_status_km	varchar(10) null --Identificador del Evento:  • ''
					    ,viaje_direccion_tramo int null --Identificador del Evento:  • 1
				    	,viaje_ramal	int null --Identificador del Evento:  • 0
				    	,viaje_desc_tramo varchar(255) null --descripcion de la ruta 
				    	,viaje_kms_tramo	int null --Registro de kilometros de la ruta-viaje
				    	,viaje_vol_recuperado	varchar(255) null --Identificador del Evento:  • NULL
						,pedido_id_pedido	int	null --	Identificador consecutivo autogenerado en LIS
						,pedido_status_pedido	int	null --	Identificador del Evento:  • 3
						,pedido_id_producto	int	null --	Identificador del Evento:  • 1
						,pedido_tipo_serv	char(1)	null --	Identificador del Evento:  • D
						,pedido_f_pedido	datetime	null --	Fecha y hora de registro del pedido:  • YYYY-MM-DD HH:MM:SS 
						,pedido_no_remision	varchar(30)	null --	Identificador del Evento: • NULL
						,pedido_id_cliente	int	null --	Numero de cliente LIS.
						,pedido_observaciones_pedido	varchar(50)	null --	Identificador del Evento: • NULL
						,pedido_id_convenio	int	null --	Identificador del Evento:  • 0
						,pedido_fecha_modifico	datetime	null --	Fecha y hora de registro del pedido:  • YYYY-MM-DD HH:MM:SS 
						,pedido_id_modifico	varchar(8)	null --	Identificador del Evento (se creará el usuario):  • UNIGIS
						,pedido_id_areaconvenio	int	null --	Identificador del Evento:  • 0
						,pedido_no_guia	int	null --	Identificador del Evento:  • 0
						,pedido_f_real_entrega	datetime	null --	Fecha y hora de registro del pedido:  • YYYY-MM-DD HH:MM:SS 
						,pedido_sist_origen	varchar(4)	null --	Identificador del Evento:  • ZAM
						,pedido_id_pedido_maestro	int	null --	Identificador del Evento:  • 0
						,pedido_id_area_guia	int	null --	Identificador del Evento:  • 1
						,pedido_id_tipo_cancelacion	int	null --	Identificador del Evento:  • 0
						,pedido_num_pedido	varchar(25)	null --	Identificador consecutivo autogenerado en LIS
						,pedido_por_cargar	int	null --	Identificador del Evento:  • 1
						,pedido_cargado	int	null --	Identificador del Evento:  • 0
						,pedido_pedido_creado	int	null --	Identificador del Evento:  • 0
						,pedido_id_solicitud	int	null --	Identificador del Evento:  • 0
						,pedido_tarifa	decimal(18, 6)	null --	Identificador del Evento:  • 0
						,pedido_id_autorizo	varchar(8)	null --	Identificador del Evento (se creará el usuario):  • UNIGIS
						,pedido_f_desp_prog	datetime	null --	Fecha y hora de registro del pedido:  • YYYY-MM-DD HH:MM:SS 
						,pedido_id_tipo_unidad	int	null --	Identificador del Evento:  • 0
						,pedido_id_clienteFinal	int	null --	Identificador del Evento:  • 0
						,pedido_id_pedidopk	int	null --	Identificador consecutivo autogenerado en LIS
						-- Start anticipo
						,anticipo_no_anticipo				int 			null
						,anticipo_fecha_anticipo 			datetime		null 
						,anticipo_monto_anticipo			decimal(18,6)	null
						,anticipo_monto_anticipo_iva		decimal(18,6)	null
						,anticipo_fecha_ingreso				datetime		null
						,anticipo_observaciones				varchar(250)	null
						-- start CapcomPowerSystem
						,guia_num_guia						varchar(255)	null
						,guia_fecha_guia					datetime		null
						,guia_flete							decimal(18,6)   null
						,guia_otro							decimal(18,6)   null
						,guia_subtotal						decimal(18,6)   null
						,guia_iva_guia						decimal(18,6)   null
						,guia_monto_retencion				decimal(18,6)   null
						,guia_id_serieguia					int 			null
						,guia_id_iva						int 			null
						,guia_id_retencion					int 			null
						,guia_id_metodo_pago				int 			null
						,guia_no_cuenta						varchar(25)		null
						,renglon_id_producto				int 			null
						,renglon_id_fraccion				int 			null
						,otro_monto_iva_otro				decimal(18,6)	null
						,otro_desc_otro						varchar(100)	null
						,otro_id_otro						int 			null
	) 
-- insert working data
			-- release work on triger
			insert into @ws 
							(
								 sie_ctrl		--Identificador del Evento:  • 0
								,api_ctrl		--Identificador del Evento:  • 0
--								id		--Identificador consecutivo de la tabla.
								,asignacion_id_asignacion		--Identificador consecutivo autogenerado en LIS
								,asignacion_num_asignacion	 --	Identificador consecutivo autogenerado en LIS
								,api_id_area	 --Identificador del Evento: • 1 | Área definida en LIS 
								,api_id_ruta	 --	Identificador de ruta recorrida (pendiente definición)
								,asignacion_id_configuracionviaje	 --	Identificador del Evento: • 1 | Unidad Motriz • 2 | Sencillo • 3 | Full
--								,asignacion_id_operador2	 --	Identificador del Evento: • NULL
								,api_id_personal	 --	Numero de empleado LIS, que realizo el viaje.
								,api_id_unidad	 --	Identificador de unidad LIS, que realizo el viaje.
								,api_id_remolque1	 --	Identificador de remolque 1 LIS, que realizo el viaje.
								,api_id_remolque2	 --	Identificador de remolque 2 LIS, que realizo el viaje.
								,api_id_dolly	 --	Identificador de dolly LIS, que realizo el viaje.
								,api_id_ingreso	 --	Identificador del Evento (se creará el usuario):  • UNIGIS
								,api_no_viaje	 --	Identificador consecutivo autogenerado en LIS
								,api_fecha_ingreso	 --	Fecha y hora de registro del viaje:  • YYYY-MM-DD HH:MM:SS 
								,api_f_prog_ini_viaje	 --	Fecha y hora de registro del viaje:  • YYYY-MM-DD HH:MM:SS 
								,api_f_prog_fin_viaje	 --	Fecha y hora de registro del viaje:  • YYYY-MM-DD HH:MM:SS 
								,asignacion_status_asignacion	 --	Identificador del Evento:  • 3 | Viaje terminado
								,asignacion_seguimiento_actual	 --	Identificador del Evento:  • 0
--								,asignacion_observaciones	 --	Identificador del Evento: • NULL
								,asignacion_id_flota	 --	Identificador del Evento:  • 1 | Local • 2 | Foráneo
								,asignacion_id_seguimiento	 --	Identificador del Evento:  • 1 | Cargado • 2 | Vacío
--								,asignacion_no_kit	 --	Identificador del Evento: • NULL
								,api_id_tipo_operacion	 --	Identificador del Evento:  • 0
								,asignacion_kms_ruta	 --	Registro de kilómetros de la ruta-viaje.
--								,asignacion_tiempo_ruta	 --	Identificador del Evento: • NULL
								,asignacion_sist_origen	 --	Identificador del Evento:  • ZAM
								,api_id_remitente	 --	Numero de cliente LIS.
								,api_id_destinatario	 --	Numero de cliente LIS.
								,viaje_kms_camion_vacio	 --	Registro de kilómetros de la ruta-viaje en caso de que sea cargado, sino 0
								,viaje_kms_camion_lleno	 --	Registro de kilómetros de la ruta-viaje en caso de que sea vacio, sino 0
								,viaje_kms_operador	 --	Registro de kilómetros de la ruta-viaje.
								,viaje_lts_viaje	 --	Identificador del Evento:  • 0.0000
								,viaje_rendimiento_viaje	 --	Identificador del Evento:  • 0.0000
								,viaje_rendimiento_real	 --	Identificador del Evento:  • 0.0000
								,viaje_status_viaje	 --	Identificador del Evento:  • C
								,viaje_sueldo_operador	 --	Identificador del Evento:  • 0.0000
								,viaje_compensaciones	 --	Identificador del Evento:  • 0.0000
								,viaje_lts_empresa	 --	Identificador del Evento:  • 0.0000
								,viaje_rend_esperado	 --	Identificador del Evento:  • 0.0000
								,viaje_status_desp	 --	Identificador del Evento:  • T
								,viaje_viajeactual	 --	Identificador del Evento: • N
								,viaje_status_cs	--var --	Identificador del Evento:  • E
								,api_id_origen	 --	 Numero de plaza LIS (pendiente definición).
								,api_id_destino	 --	 Numero de plaza LIS (pendiente definición).
								,api_id_linearem1	 --	Identificador del Evento:  • ZAM
								,api_id_linearem2	 --	Identificador del Evento:  • ZAM
								,viaje_id_statusviaje	 --	Identificador del Evento:  • 6
								,api_id_contrato	 --	Identificador del Evento:  • 0
--								,viaje_observacionesviaje	 --	Identificador del Evento: • NULL
								,viaje_id_tipo_servicio	 --	Identificador del Evento:  • 0
								,viaje_id_tramo		--int null --	Identificador del Evento:  • 0
							    ,viaje_consecutivo_viaje --decimal(18,6) null --Identificador del Evento:  • 1.0000
							    ,viaje_status_km	--varchar(10) null --Identificador del Evento:  • ''
							    ,viaje_direccion_tramo --int null --Identificador del Evento:  • 1
						    	,viaje_ramal	--int null --Identificador del Evento:  • 0
						    	,viaje_desc_tramo --varchar(255) null --descripcion de la ruta 
						    	,viaje_kms_tramo	--int null --Registro de kilometros de la ruta-viaje
						    	,viaje_vol_recuperado	--varchar(255) null --Identificador del Evento:  • NULL
								,pedido_id_pedido	 --	Identificador consecutivo autogenerado en LIS
								,pedido_status_pedido	 --	Identificador del Evento:  • 3
								,pedido_id_producto	 --	Identificador del Evento:  • 1
								,pedido_tipo_serv	 --	Identificador del Evento:  • D
								,pedido_f_pedido	 --	Fecha y hora de registro del pedido:  • YYYY-MM-DD HH:MM:SS 
--								,pedido_no_remision	 --	Identificador del Evento: • NULL
								,pedido_id_cliente	 --	Numero de cliente LIS.
--								,pedido_observaciones_pedido	 --	Identificador del Evento: • NULL
								,pedido_id_convenio	 --	Identificador del Evento:  • 0
								,pedido_fecha_modifico	 --	Fecha y hora de registro del pedido:  • YYYY-MM-DD HH:MM:SS 
								,pedido_id_modifico	 --	Identificador del Evento (se creará el usuario):  • UNIGIS
								,pedido_id_areaconvenio	 --	Identificador del Evento:  • 0
								,pedido_no_guia	 --	Identificador del Evento:  • 0
								,pedido_f_real_entrega	 --	Fecha y hora de registro del pedido:  • YYYY-MM-DD HH:MM:SS 
								,pedido_sist_origen	 --	Identificador del Evento:  • ZAM
								,pedido_id_pedido_maestro	 --	Identificador del Evento:  • 0
								,pedido_id_area_guia	 --	Identificador del Evento:  • 1
								,pedido_id_tipo_cancelacion	 --	Identificador del Evento:  • 0
								,pedido_num_pedido	 --	Identificador consecutivo autogenerado en LIS
								,pedido_por_cargar	 --	Identificador del Evento:  • 1
								,pedido_cargado	 --	Identificador del Evento:  • 0
								,pedido_pedido_creado	 --	Identificador del Evento:  • 0
								,pedido_id_solicitud	 --	Identificador del Evento:  • 0
								,pedido_tarifa	 --	Identificador del Evento:  • 0
								,pedido_id_autorizo	 --	Identificador del Evento (se creará el usuario):  • UNIGIS
								,pedido_f_desp_prog	 --	Fecha y hora de registro del pedido:  • YYYY-MM-DD HH:MM:SS 
								,pedido_id_tipo_unidad	 --	Identificador del Evento:  • 0
								,pedido_id_clienteFinal	 --	Identificador del Evento:  • 0
								,pedido_id_pedidopk	 --	Identificador consecutivo autogenerado en LIS
								-- START Anticipo
								,anticipo_no_anticipo
								,anticipo_fecha_anticipo		 
								,anticipo_monto_anticipo	
								,anticipo_monto_anticipo_iva	
								,anticipo_fecha_ingreso		
								,anticipo_observaciones	
								-- start CapcomPowerSystem
								,guia_num_guia	
								,guia_fecha_guia		
								,guia_flete   
								,guia_otro   
								,guia_subtotal   
								,guia_iva_guia   
								,guia_monto_retencion   
								,guia_id_serieguia 			
								,guia_id_iva 			
								,guia_id_retencion 			
								,guia_id_metodo_pago 			
								,guia_no_cuenta		
								,renglon_id_producto 			
								,renglon_id_fraccion 			
								,otro_monto_iva_otro	
								,otro_desc_otro	
								,otro_id_otro 			
							)
				values
							(
								 @sie_ctrl								--sie_ctrl		--Identificador del Evento:  • 0
								,@api_ctrl								--api_ctrl		--Identificador del Evento:  • 0
--								,id										--Identificador consecutivo de la tabla.
								,@id_asignacion 						--asignacion_id_asignacion		--Identificador consecutivo autogenerado en LIS
								,@num_asignacion 						--asignacion_num_asignacion	 --	Identificador consecutivo autogenerado en LIS
								,1 										--api_id_area  Identificador del Evento: • 1 | Área definida en LIS 
								,@api_id_ruta	 						--	Identificador de ruta recorrida (pendiente definición)
								,@asignacion_id_configuracionviaje	 	--	Identificador del Evento: • 1 | Unidad Motriz • 2 | Sencillo • 3 | Full
--								,asignacion_id_operador2	 			--	Identificador del Evento: • NULL
								,@api_id_personal	 					--	Numero de empleado LIS, que realizo el viaje.
								,@api_id_unidad	 						--	Identificador de unidad LIS, que realizo el viaje.
								,@api_id_remolque1	 					--	Identificador de remolque 1 LIS, que realizo el viaje.
								,@api_id_remolque2	 					--	Identificador de remolque 2 LIS, que realizo el viaje.
								,@api_id_dolly	 						--	Identificador de dolly LIS, que realizo el viaje.
								,@api_id_ingreso	 					--	Identificador del Evento (se creará el usuario):  • UNIGIS
								,@no_viaje	 							--	Identificador consecutivo autogenerado en LIS // Block Change for No_viaje 
								,@api_fecha_ingreso	 					--	Fecha y hora de registro del viaje:  • YYYY-MM-DD HH:MM:SS 
								,@api_f_prog_ini_viaje	 				--	Fecha y hora de registro del viaje:  • YYYY-MM-DD HH:MM:SS 
								,@api_f_prog_fin_viaje					--	Fecha y hora de registro del viaje:  • YYYY-MM-DD HH:MM:SS 
								,3    									--asignacion_status_asignacion	 --	Identificador del Evento:  • 3 | Viaje terminado
								,0 										--asignacion_seguimiento_actual	 --	Identificador del Evento:  • 0
--								,asignacion_observaciones	 --	Identificador del Evento: • NULL
								,1 -- ???? asignacion_id_flota	 --	Identificador del Evento:  • 1 | Local • 2 | Foráneo
								,@asignacion_id_seguimiento	 			--	Identificador del Evento:  • 1 | Cargado • 2 | Vacío
--								,asignacion_no_kit	--	Identificador del Evento: • NULL
								,0 										--api_id_tipo_operacion	 --	Identificador del Evento:  • 0
								,@asignacion_kms_ruta	 				--	Registro de kilómetros de la ruta-viaje.
--								,asignacion_tiempo_ruta	 --	Identificador del Evento: • NULL
								,'ZAM' 									--asignacion_sist_origen	 --	Identificador del Evento:  • ZAM
								,@api_id_remitente	 					--	Numero de cliente LIS.
								,@api_id_destinatario	 				--	Numero de cliente LIS.
								,@viaje_kms_camion_vacio	 			--	Registro de kilómetros de la ruta-viaje en caso de que sea cargado, sino 0
								,@viaje_kms_camion_lleno	 			--	Registro de kilómetros de la ruta-viaje en caso de que sea vacio, sino 0
								,@viaje_kms_operador	 				--	Registro de kilómetros de la ruta-viaje.
								,0.0000 								--	viaje_lts_viaje	 --	Identificador del Evento:  • 0.0000
								,0.0000 								--	viaje_rendimiento_viaje	 --	Identificador del Evento:  • 0.0000
								,0.0000 								--	viaje_rendimiento_real	 --	Identificador del Evento:  • 0.0000
								,'C' 									--	viaje_status_viaje	 --	Identificador del Evento:  • C
								,0.0000 								--	viaje_sueldo_operador	 --	Identificador del Evento:  • 0.0000
								,0.0000 								--	viaje_compensaciones	 --	Identificador del Evento:  • 0.0000
								,0.0000 								--	viaje_lts_empresa	 --	Identificador del Evento:  • 0.0000
								,0.0000 								--	viaje_rend_esperado	 --	Identificador del Evento:  • 0.0000
								,'T' 									--	viaje_status_desp	 --	Identificador del Evento:  • T
								,'N' 									--	viaje_viajeactual	 --	Identificador del Evento: • N
								,'E'									--	viaje_status_cs	var --	Identificador del Evento:  • E
								,@api_id_origen	 						--	Numero de plaza LIS (pendiente definición).
								,@api_id_destino	 					--	Numero de plaza LIS (pendiente definición).
								,'ZAM' 									-- 	api_id_linearem1	 --	Identificador del Evento:  • ZAM
								,'ZAM' 									--	api_id_linearem2	 --	Identificador del Evento:  • ZAM
								,6 										--	viaje_id_statusviaje	 --	Identificador del Evento:  • 6
								,0 										--	api_id_contrato	 --	Identificador del Evento:  • 0
--								,viaje_observacionesviaje	 			--	Identificador del Evento: • NULL
								,0										--	viaje_id_tipo_servicio	 --	Identificador del Evento:  • 0
							
								,0-- viaje_id_tramo		int null --	Identificador del Evento:  • 0
							    ,1.0000 --viaje_consecutivo_viaje decimal(18,6) null --Identificador del Evento:  • 1.0000
							    ,'' --viaje_status_km	varchar(10) null --Identificador del Evento:  • ''
							    ,1 --viaje_direccion_tramo int null --Identificador del Evento:  • 1
						    	,0 --viaje_ramal	int null --Identificador del Evento:  • 0
						    	,'' --viaje_desc_tramo varchar(255) null --descripcion de la ruta 
						    	,0 --viaje_kms_tramo	int null --Registro de kilometros de la ruta-viaje
						    	,'' --viaje_vol_recuperado	varchar(255) null --Identificador del Evento:  • NULL
							
								,3 										--pedido_id_pedido	 --	Identificador consecutivo autogenerado en LIS
								,3 										--pedido_status_pedido	 --	Identificador del Evento:  • 3
								,@pedido_id_producto	 				--	Identificador del Evento:  • 1
								,'D' 									--pedido_tipo_serv	 --	Identificador del Evento:  • D
								,@pedido_f_pedido	 					--	Fecha y hora de registro del pedido:  • YYYY-MM-DD HH:MM:SS 
--								,pedido_no_remision	 --	Identificador del Evento: • NULL
								,@pedido_id_cliente	 					--	Numero de cliente LIS.
--								,pedido_observaciones_pedido	 --	Identificador del Evento: • NULL
								,0 										--pedido_id_convenio	 --	Identificador del Evento:  • 0
								,@pedido_f_pedido --pedido_fecha_modifico	 --	Fecha y hora de registro del pedido:  • YYYY-MM-DD HH:MM:SS 
								,'UNIGIS' 								-- pedido_id_modifico	 --	Identificador del Evento (se creará el usuario):  • UNIGIS
								,0 										--pedido_id_areaconvenio	 --	Identificador del Evento:  • 0
								,0 										--pedido_no_guia	 --	Identificador del Evento:  • 0
								,@pedido_f_pedido --pedido_f_real_entrega	 --	Fecha y hora de registro del pedido:  • YYYY-MM-DD HH:MM:SS 
								,'ZAM'									--pedido_sist_origen	 --	Identificador del Evento:  • ZAM
								,0 										--pedido_id_pedido_maestro	 --	Identificador del Evento:  • 0
								,1										--pedido_id_area_guia	 --	Identificador del Evento:  • 1
								,0 										--pedido_id_tipo_cancelacion	 --	Identificador del Evento:  • 0
								,@num_pedido							--	Identificador consecutivo autogenerado en LIS
								,1 										--pedido_por_cargar	 --	Identificador del Evento:  • 1
								,0 										--pedido_cargado	 --	Identificador del Evento:  • 0
								,0 										--pedido_pedido_creado	 --	Identificador del Evento:  • 0
								,0 										--pedido_id_solicitud	 --	Identificador del Evento:  • 0
								,0 										--pedido_tarifa	 --	Identificador del Evento:  • 0
								,'UNIGIS' 								--pedido_id_autorizo	 --	Identificador del Evento (se creará el usuario):  • UNIGIS
								,@pedido_f_pedido --pedido_f_desp_prog	 --	Fecha y hora de registro del pedido:  • YYYY-MM-DD HH:MM:SS 
								,0 										--pedido_id_tipo_unidad	 --	Identificador del Evento:  • 0
								,0 										--pedido_id_clienteFinal	 --	Identificador del Evento:  • 0
								,@id_pedidopk	 --	Identificador consecutivo autogenerado en LIS
								-- Start anticipo
								,@no_anticipo
								,@anticipo_fecha_anticipo		 
								,@anticipo_monto_anticipo
								,@anticipo_monto_anticipo_iva
								,@anticipo_fecha_ingreso
								,@anticipo_observaciones
								-- start CapcomPowerSystem
								,@guia_num_guia
								,@guia_fecha_guia		
								,@guia_flete   
								,@guia_otro    
								,@guia_subtotal   
								,@guia_iva_guia   
								,@guia_monto_retencion    
								,@guia_id_serieguia
								,@guia_id_iva	
								,@guia_id_retencion					 			
								,@guia_id_metodo_pago				 			
								,@guia_no_cuenta
								,@renglon_id_producto
								,@renglon_id_fraccion
								,@otro_monto_iva_otro
								,@otro_desc_otro
								,@otro_id_otro
							)	
--test
					insert into galdb.dbo.api_ws_historico_logs (message)
						select 
								'record into var table ' 
								+ '_EXECUTE AT '
								+ cast(current_timestamp as varchar(30)) 
--test
--			insert into backup log 

						insert into galdb.dbo.api_gal_viajes_logs(
															 sie_ctrl		--Identificador del Evento:  • 0
															,api_ctrl		--Identificador del Evento:  • 0
							--								id		--Identificador consecutivo de la tabla.
															,asignacion_id_asignacion		--Identificador consecutivo autogenerado en LIS
															,asignacion_num_asignacion	 --	Identificador consecutivo autogenerado en LIS
															,api_id_area	 --Identificador del Evento: • 1 | Área definida en LIS 
															,api_id_ruta	 --	Identificador de ruta recorrida (pendiente definición)
															,asignacion_id_configuracionviaje	 --	Identificador del Evento: • 1 | Unidad Motriz • 2 | Sencillo • 3 | Full
							--								,asignacion_id_operador2	 --	Identificador del Evento: • NULL
															,api_id_personal	 --	Numero de empleado LIS, que realizo el viaje.
															,api_id_unidad	 --	Identificador de unidad LIS, que realizo el viaje.
															,api_id_remolque1	 --	Identificador de remolque 1 LIS, que realizo el viaje.
															,api_id_remolque2	 --	Identificador de remolque 2 LIS, que realizo el viaje.
															,api_id_dolly	 --	Identificador de dolly LIS, que realizo el viaje.
															,api_id_ingreso	 --	Identificador del Evento (se creará el usuario):  • UNIGIS
															,api_no_viaje	 --	Identificador consecutivo autogenerado en LIS
															,api_fecha_ingreso	 --	Fecha y hora de registro del viaje:  • YYYY-MM-DD HH:MM:SS 
															,api_f_prog_ini_viaje	 --	Fecha y hora de registro del viaje:  • YYYY-MM-DD HH:MM:SS 
															,api_f_prog_fin_viaje	 --	Fecha y hora de registro del viaje:  • YYYY-MM-DD HH:MM:SS 
															,asignacion_status_asignacion	 --	Identificador del Evento:  • 3 | Viaje terminado
															,asignacion_seguimiento_actual	 --	Identificador del Evento:  • 0
							--								,asignacion_observaciones	 --	Identificador del Evento: • NULL
															,asignacion_id_flota	 --	Identificador del Evento:  • 1 | Local • 2 | Foráneo
															,asignacion_id_seguimiento	 --	Identificador del Evento:  • 1 | Cargado • 2 | Vacío
							--								,asignacion_no_kit	 --	Identificador del Evento: • NULL
															,api_id_tipo_operacion	 --	Identificador del Evento:  • 0
															,asignacion_kms_ruta	 --	Registro de kilómetros de la ruta-viaje.
							--								,asignacion_tiempo_ruta	 --	Identificador del Evento: • NULL
															,asignacion_sist_origen	 --	Identificador del Evento:  • ZAM
															,api_id_remitente	 --	Numero de cliente LIS.
															,api_id_destinatario	 --	Numero de cliente LIS.
															,viaje_kms_camion_vacio	 --	Registro de kilómetros de la ruta-viaje en caso de que sea cargado, sino 0
															,viaje_kms_camion_lleno	 --	Registro de kilómetros de la ruta-viaje en caso de que sea vacio, sino 0
															,viaje_kms_operador	 --	Registro de kilómetros de la ruta-viaje.
															,viaje_lts_viaje	 --	Identificador del Evento:  • 0.0000
															,viaje_rendimiento_viaje	 --	Identificador del Evento:  • 0.0000
															,viaje_rendimiento_real	 --	Identificador del Evento:  • 0.0000
															,viaje_status_viaje	 --	Identificador del Evento:  • C
															,viaje_sueldo_operador	 --	Identificador del Evento:  • 0.0000
															,viaje_compensaciones	 --	Identificador del Evento:  • 0.0000
															,viaje_lts_empresa	 --	Identificador del Evento:  • 0.0000
															,viaje_rend_esperado	 --	Identificador del Evento:  • 0.0000
															,viaje_status_desp	 --	Identificador del Evento:  • T
															,viaje_viajeactual	 --	Identificador del Evento: • N
															,viaje_status_cs	 --	Identificador del Evento:  • E
															,api_id_origen	 --	 Numero de plaza LIS (pendiente definición).
															,api_id_destino	 --	 Numero de plaza LIS (pendiente definición).
															,api_id_linearem1	 --	Identificador del Evento:  • ZAM
															,api_id_linearem2	 --	Identificador del Evento:  • ZAM
															,viaje_id_statusviaje	 --	Identificador del Evento:  • 6
															,api_id_contrato	 --	Identificador del Evento:  • 0
							--								,viaje_observacionesviaje	 --	Identificador del Evento: • NULL
															,viaje_id_tipo_servicio	 --	Identificador del Evento:  • 0
															,viaje_id_tramo		--int null --	Identificador del Evento:  • 0
														    ,viaje_consecutivo_viaje --decimal(18,6) null --Identificador del Evento:  • 1.0000
														    ,viaje_status_km	--varchar(10) null --Identificador del Evento:  • ''
														    ,viaje_direccion_tramo --int null --Identificador del Evento:  • 1
													    	,viaje_ramal	--int null --Identificador del Evento:  • 0
													    	,viaje_desc_tramo --varchar(255) null --descripcion de la ruta 
													    	,viaje_kms_tramo	--int null --Registro de kilometros de la ruta-viaje
													    	,viaje_vol_recuperado	--varchar(255) null --Identificador del Evento:  • NULL
															,pedido_id_pedido	 --	Identificador consecutivo autogenerado en LIS
															,pedido_status_pedido	 --	Identificador del Evento:  • 3
															,pedido_id_producto	 --	Identificador del Evento:  • 1
															,pedido_tipo_serv	 --	Identificador del Evento:  • D
															,pedido_f_pedido	 --	Fecha y hora de registro del pedido:  • YYYY-MM-DD HH:MM:SS 
							--								,pedido_no_remision	 --	Identificador del Evento: • NULL
															,pedido_id_cliente	 --	Numero de cliente LIS.
							--								,pedido_observaciones_pedido	 --	Identificador del Evento: • NULL
															,pedido_id_convenio	 --	Identificador del Evento:  • 0
															,pedido_fecha_modifico	 --	Fecha y hora de registro del pedido:  • YYYY-MM-DD HH:MM:SS 
															,pedido_id_modifico	 --	Identificador del Evento (se creará el usuario):  • UNIGIS
															,pedido_id_areaconvenio	 --	Identificador del Evento:  • 0
															,pedido_no_guia	 --	Identificador del Evento:  • 0
															,pedido_f_real_entrega	 --	Fecha y hora de registro del pedido:  • YYYY-MM-DD HH:MM:SS 
															,pedido_sist_origen	 --	Identificador del Evento:  • ZAM
															,pedido_id_pedido_maestro	 --	Identificador del Evento:  • 0
															,pedido_id_area_guia	 --	Identificador del Evento:  • 1
															,pedido_id_tipo_cancelacion	 --	Identificador del Evento:  • 0
															,pedido_num_pedido	 --	Identificador consecutivo autogenerado en LIS
															,pedido_por_cargar	 --	Identificador del Evento:  • 1
															,pedido_cargado	 --	Identificador del Evento:  • 0
															,pedido_pedido_creado	 --	Identificador del Evento:  • 0
															,pedido_id_solicitud	 --	Identificador del Evento:  • 0
															,pedido_tarifa	 --	Identificador del Evento:  • 0
															,pedido_id_autorizo	 --	Identificador del Evento (se creará el usuario):  • UNIGIS
															,pedido_f_desp_prog	 --	Fecha y hora de registro del pedido:  • YYYY-MM-DD HH:MM:SS 
															,pedido_id_tipo_unidad	 --	Identificador del Evento:  • 0
															,pedido_id_clienteFinal	 --	Identificador del Evento:  • 0
															,pedido_id_pedidopk	 --	Identificador consecutivo autogenerado en LIS
															-- Start anticipo
															,anticipo_no_anticipo
															,anticipo_fecha_anticipo		 
															,anticipo_monto_anticipo
															,anticipo_monto_anticipo_iva
															,anticipo_fecha_ingreso
															,anticipo_observaciones
															-- start CapcomPowerSystem
															,guia_num_guia
															,guia_fecha_guia		
															,guia_flete   
															,guia_otro    
															,guia_subtotal   
															,guia_iva_guia   
															,guia_monto_retencion    
															,guia_id_serieguia
															,guia_id_iva	
															,guia_id_retencion					 			
															,guia_id_metodo_pago				 			
															,guia_no_cuenta
															,renglon_id_producto
															,renglon_id_fraccion
															,otro_monto_iva_otro
															,otro_desc_otro
															,otro_id_otro
														    -- End Anticipo
															,created	--		 				datetime		null   	-- created date
															,modified	--		 				datetime		null  	-- if modified
															,status		--		 				tinyint default 1 null 	-- set active or inactive
							)
							select 
								 sie_ctrl		--Identificador del Evento:  • 0
								,api_ctrl		--Identificador del Evento:  • 0
--								id		--Identificador consecutivo de la tabla.
								,asignacion_id_asignacion		--Identificador consecutivo autogenerado en LIS
								,asignacion_num_asignacion	 --	Identificador consecutivo autogenerado en LIS
								,api_id_area	 --Identificador del Evento: • 1 | Área definida en LIS 
								,api_id_ruta	 --	Identificador de ruta recorrida (pendiente definición)
								,asignacion_id_configuracionviaje	 --	Identificador del Evento: • 1 | Unidad Motriz • 2 | Sencillo • 3 | Full
--								,asignacion_id_operador2	 --	Identificador del Evento: • NULL
								,api_id_personal	 --	Numero de empleado LIS, que realizo el viaje.
								,api_id_unidad	 --	Identificador de unidad LIS, que realizo el viaje.
								,api_id_remolque1	 --	Identificador de remolque 1 LIS, que realizo el viaje.
								,api_id_remolque2	 --	Identificador de remolque 2 LIS, que realizo el viaje.
								,api_id_dolly	 --	Identificador de dolly LIS, que realizo el viaje.
								,api_id_ingreso	 --	Identificador del Evento (se creará el usuario):  • UNIGIS
								,api_no_viaje	 --	Identificador consecutivo autogenerado en LIS
								,api_fecha_ingreso	 --	Fecha y hora de registro del viaje:  • YYYY-MM-DD HH:MM:SS 
								,api_f_prog_ini_viaje	 --	Fecha y hora de registro del viaje:  • YYYY-MM-DD HH:MM:SS 
								,api_f_prog_fin_viaje	 --	Fecha y hora de registro del viaje:  • YYYY-MM-DD HH:MM:SS 
								,asignacion_status_asignacion	 --	Identificador del Evento:  • 3 | Viaje terminado
								,asignacion_seguimiento_actual	 --	Identificador del Evento:  • 0
--								,asignacion_observaciones	 --	Identificador del Evento: • NULL
								,asignacion_id_flota	 --	Identificador del Evento:  • 1 | Local • 2 | Foráneo
								,asignacion_id_seguimiento	 --	Identificador del Evento:  • 1 | Cargado • 2 | Vacío
--								,asignacion_no_kit	 --	Identificador del Evento: • NULL
								,api_id_tipo_operacion	 --	Identificador del Evento:  • 0
								,asignacion_kms_ruta	 --	Registro de kilómetros de la ruta-viaje.
--								,asignacion_tiempo_ruta	 --	Identificador del Evento: • NULL
								,asignacion_sist_origen	 --	Identificador del Evento:  • ZAM
								,api_id_remitente	 --	Numero de cliente LIS.
								,api_id_destinatario	 --	Numero de cliente LIS.
								,viaje_kms_camion_vacio	 --	Registro de kilómetros de la ruta-viaje en caso de que sea cargado, sino 0
								,viaje_kms_camion_lleno	 --	Registro de kilómetros de la ruta-viaje en caso de que sea vacio, sino 0
								,viaje_kms_operador	 --	Registro de kilómetros de la ruta-viaje.
								,viaje_lts_viaje	 --	Identificador del Evento:  • 0.0000
								,viaje_rendimiento_viaje	 --	Identificador del Evento:  • 0.0000
								,viaje_rendimiento_real	 --	Identificador del Evento:  • 0.0000
								,viaje_status_viaje	 --	Identificador del Evento:  • C
								,viaje_sueldo_operador	 --	Identificador del Evento:  • 0.0000
								,viaje_compensaciones	 --	Identificador del Evento:  • 0.0000
								,viaje_lts_empresa	 --	Identificador del Evento:  • 0.0000
								,viaje_rend_esperado	 --	Identificador del Evento:  • 0.0000
								,viaje_status_desp	 --	Identificador del Evento:  • T
								,viaje_viajeactual	 --	Identificador del Evento: • N
								,viaje_status_cs	var --	Identificador del Evento:  • E
								,api_id_origen	 --	 Numero de plaza LIS (pendiente definición).
								,api_id_destino	 --	 Numero de plaza LIS (pendiente definición).
								,api_id_linearem1	 --	Identificador del Evento:  • ZAM
								,api_id_linearem2	 --	Identificador del Evento:  • ZAM
								,viaje_id_statusviaje	 --	Identificador del Evento:  • 6
								,api_id_contrato	 --	Identificador del Evento:  • 0
--								,viaje_observacionesviaje	 --	Identificador del Evento: • NULL
								,viaje_id_tipo_servicio	 --	Identificador del Evento:  • 0
								,viaje_id_tramo		--int null --	Identificador del Evento:  • 0
							    ,viaje_consecutivo_viaje --decimal(18,6) null --Identificador del Evento:  • 1.0000
							    ,viaje_status_km	--varchar(10) null --Identificador del Evento:  • ''
							    ,viaje_direccion_tramo --int null --Identificador del Evento:  • 1
						    	,viaje_ramal	--int null --Identificador del Evento:  • 0
						    	,viaje_desc_tramo --varchar(255) null --descripcion de la ruta 
						    	,viaje_kms_tramo	--int null --Registro de kilometros de la ruta-viaje
						    	,viaje_vol_recuperado	--varchar(255) null --Identificador del Evento:  • NULL
								,pedido_id_pedido	 --	Identificador consecutivo autogenerado en LIS
								,pedido_status_pedido	 --	Identificador del Evento:  • 3
								,pedido_id_producto	 --	Identificador del Evento:  • 1
								,pedido_tipo_serv	 --	Identificador del Evento:  • D
								,pedido_f_pedido	 --	Fecha y hora de registro del pedido:  • YYYY-MM-DD HH:MM:SS 
--								,pedido_no_remision	 --	Identificador del Evento: • NULL
								,pedido_id_cliente	 --	Numero de cliente LIS.
--								,pedido_observaciones_pedido	 --	Identificador del Evento: • NULL
								,pedido_id_convenio	 --	Identificador del Evento:  • 0
								,pedido_fecha_modifico	 --	Fecha y hora de registro del pedido:  • YYYY-MM-DD HH:MM:SS 
								,pedido_id_modifico	 --	Identificador del Evento (se creará el usuario):  • UNIGIS
								,pedido_id_areaconvenio	 --	Identificador del Evento:  • 0
								,pedido_no_guia	 --	Identificador del Evento:  • 0
								,pedido_f_real_entrega	 --	Fecha y hora de registro del pedido:  • YYYY-MM-DD HH:MM:SS 
								,pedido_sist_origen	 --	Identificador del Evento:  • ZAM
								,pedido_id_pedido_maestro	 --	Identificador del Evento:  • 0
								,pedido_id_area_guia	 --	Identificador del Evento:  • 1
								,pedido_id_tipo_cancelacion	 --	Identificador del Evento:  • 0
								,pedido_num_pedido	 --	Identificador consecutivo autogenerado en LIS
								,pedido_por_cargar	 --	Identificador del Evento:  • 1
								,pedido_cargado	 --	Identificador del Evento:  • 0
								,pedido_pedido_creado	 --	Identificador del Evento:  • 0
								,pedido_id_solicitud	 --	Identificador del Evento:  • 0
								,pedido_tarifa	 --	Identificador del Evento:  • 0
								,pedido_id_autorizo	 --	Identificador del Evento (se creará el usuario):  • UNIGIS
								,pedido_f_desp_prog	 --	Fecha y hora de registro del pedido:  • YYYY-MM-DD HH:MM:SS 
								,pedido_id_tipo_unidad	 --	Identificador del Evento:  • 0
								,pedido_id_clienteFinal	 --	Identificador del Evento:  • 0
								,pedido_id_pedidopk	 --	Identificador consecutivo autogenerado en LIS
								-- Start anticipo
								,anticipo_no_anticipo
								,anticipo_fecha_anticipo		 
								,anticipo_monto_anticipo
								,anticipo_monto_anticipo_iva
								,anticipo_fecha_ingreso
								,anticipo_observaciones
								-- start CapcomPowerSystem
								,guia_num_guia
								,guia_fecha_guia		
								,guia_flete   
								,guia_otro    
								,guia_subtotal   
								,guia_iva_guia   
								,guia_monto_retencion    
								,guia_id_serieguia
								,guia_id_iva	
								,guia_id_retencion					 			
								,guia_id_metodo_pago				 			
								,guia_no_cuenta
								,renglon_id_producto
								,renglon_id_fraccion
								,otro_monto_iva_otro
								,otro_desc_otro
								,otro_id_otro
							    -- End Anticipo
								,current_timestamp --created			 				datetime		null   	-- created date
								,'' --modified			 				datetime		null  	-- if modified
								,1 --status				 				tinyint default 1 null 	-- set active or inactive
							from @ws
--							save the last inserted id						
				set @ctrl_log_id = SCOPE_IDENTITY()		
--		save a record in logs 
				insert into galdb.dbo.api_ws_historico_logs (message)
					select 
								'record with id ' 
								+ cast( @ctrl_log_id as varchar(max) ) 
								+'- INSERT INTO DBO.api_gal_viajes_logs -'
								+ '_EXECUTE AT '
								+ cast(current_timestamp as varchar(30)) 


--build the squema with procedures 							

set XACT_ABORT on   

	if @sie_ctrl = 1 --BEGIN Viaje Transaction
		begin 
			
			begin TRY
				begin transaction pedido -- initializa pedido
			insert into galdb.dbo.desp_pedido 
														(
										 id_area	 				--	Identificador del Evento: • 1 | Área definida en LIS 
										,id_ruta	 				--	Identificador de ruta recorrida (pendiente definición)
										,id_operador  				-- 	id_personal	 --	Numero de empleado LIS, que realizo el viaje.
										,id_unidad	 				--	Identificador de unidad LIS, que realizo el viaje.
										,id_remolque1	 			--	Identificador de remolque 1 LIS, que realizo el viaje.
										,id_remolque2	 			--	Identificador de remolque 2 LIS, que realizo el viaje.
										,id_dolly	 					--	Identificador de dolly LIS, que realizo el viaje.
										,id_ingreso	 				--	Identificador del Evento (se creará el usuario):  • UNIGIS
										,no_viaje	 				--	Identificador consecutivo autogenerado en LIS
										,fecha_ingreso	 			--	Fecha y hora de registro del viaje:  • YYYY-MM-DD HH:MM:SS 
										,observaciones	 			--	Identificador del Evento: • NULL
		--								,id_flota	 				--	Identificador del Evento:  • 1 | Local • 2 | Foráneo
										,id_tipo_operacion			--	Identificador del Evento:  • 0
		--								,sist_origen	 			--	Identificador del Evento:  • ZAM
										,id_remitente	 			--	Numero de cliente LIS.
										,id_destinatario			--	Numero de cliente LIS.
										,id_origen	 				--	Numero de plaza LIS (pendiente definición).
										,id_destino	 				--	Numero de plaza LIS (pendiente definición).
										,id_linearem1	 			--	Identificador del Evento:  • ZAM
										,id_linearem2	 			--	Identificador del Evento:  • ZAM
										,id_contrato	 			--	Identificador del Evento:  • 0
										,id_pedido	 				--	Identificador consecutivo autogenerado en LIS
										,status_pedido	 			--	Identificador del Evento:  • 3
										,id_producto	 			--	Identificador del Evento:  • 1
										,tipo_serv	 				--	Identificador del Evento:  • D
										,f_pedido	 				--	Fecha y hora de registro del pedido:  • YYYY-MM-DD HH:MM:SS 
										,id_cliente	 				--	Numero de cliente LIS.
										,id_clienteconvenio			--  ADD From @pedido_id_cliente
										,id_convenio	 			--	Identificador del Evento:  • 0
										,fecha_modifico	 			--	Fecha y hora de registro del pedido:  • YYYY-MM-DD HH:MM:SS 
										,id_modifico	 			--	Identificador del Evento (se creará el usuario):  • UNIGIS
										,id_areaconvenio			--	Identificador del Evento:  • 0
										,no_guia	 				--	Identificador del Evento:  • 0
										,f_real_entrega	 			--	Fecha y hora de registro del pedido:  • YYYY-MM-DD HH:MM:SS 
										,sist_origen	 			--	Identificador del Evento:  • ZAM
										,id_pedido_maestro			--	Identificador del Evento:  • 0
										,id_area_guia	 			--	Identificador del Evento:  • 1
										,id_tipo_cancelacion	 	--	Identificador del Evento:  • 0
										,num_pedido	 				--	Identificador consecutivo autogenerado en LIS
										,por_cargar	 				--	Identificador del Evento:  • 1
										,cargado	 				--	Identificador del Evento:  • 0
										,pedido_creado	 			--	Identificador del Evento:  • 0
										,id_solicitud	 			--	Identificador del Evento:  • 0
										,tarifa	 					--	Identificador del Evento:  • 0
										,id_autorizo	 			--	Identificador del Evento (se creará el usuario):  • UNIGIS
										,f_desp_prog	 			--	Fecha y hora de registro del pedido:  • YYYY-MM-DD HH:MM:SS 
										,id_tipo_unidad	 			--	Identificador del Evento:  • 0
										,id_clienteFinal	 		--	Identificador del Evento:  • 0
		--								,id_pedidopk	 			--	Identificador consecutivo autogenerado en LIS
		--							from galdb.dbo.desp_pedido where 1 = 0
														)
								select 
										 api_id_area	 --Identificador del Evento: • 1 | Área definida en LIS 
										,api_id_ruta	 --	Identificador de ruta recorrida (pendiente definición)
		--								,asignacion_id_configuracionviaje	 --	Identificador del Evento: • 1 | Unidad Motriz • 2 | Sencillo • 3 | Full
										,api_id_personal	 --	Numero de empleado LIS, que realizo el viaje.
										,api_id_unidad	 --	Identificador de unidad LIS, que realizo el viaje.
										,api_id_remolque1	 --	Identificador de remolque 1 LIS, que realizo el viaje.
										,api_id_remolque2	 --	Identificador de remolque 2 LIS, que realizo el viaje.
										,api_id_dolly	 --	Identificador de dolly LIS, que realizo el viaje.
										,api_id_ingreso	 --	Identificador del Evento (se creará el usuario):  • UNIGIS
										,api_no_viaje	 --	Identificador consecutivo autogenerado en LIS
										,api_fecha_ingreso	 --	Fecha y hora de registro del viaje:  • YYYY-MM-DD HH:MM:SS 
										,asignacion_observaciones	 --	Identificador del Evento: • NULL
		--								,asignacion_id_flota	 --	Identificador del Evento:  • 1 | Local • 2 | Foráneo
										,api_id_tipo_operacion	 --	Identificador del Evento:  • 0
		--								,asignacion_sist_origen	 --	Identificador del Evento:  • ZAM
										,api_id_remitente	 --	Numero de cliente LIS.
										,api_id_destinatario	 --	Numero de cliente LIS.
										,api_id_origen	 --	 Numero de plaza LIS (pendiente definición).
										,api_id_destino	 --	 Numero de plaza LIS (pendiente definición).
										,api_id_linearem1	 --	Identificador del Evento:  • ZAM
										,api_id_linearem2	 --	Identificador del Evento:  • ZAM
										,api_id_contrato	 --	Identificador del Evento:  • 0
										,pedido_id_pedido	 --	Identificador consecutivo autogenerado en LIS
										,pedido_status_pedido	 --	Identificador del Evento:  • 3
										,pedido_id_producto	 --	Identificador del Evento:  • 1
										,pedido_tipo_serv	 --	Identificador del Evento:  • D
										,pedido_f_pedido	 --	Fecha y hora de registro del pedido:  • YYYY-MM-DD HH:MM:SS 
										,pedido_id_cliente	 --	Numero de cliente LIS.
										,pedido_id_cliente 	 --id_clienteconvenio
										,pedido_id_convenio	 --	Identificador del Evento:  • 0
										,pedido_fecha_modifico	 --	Fecha y hora de registro del pedido:  • YYYY-MM-DD HH:MM:SS 
										,pedido_id_modifico	 --	Identificador del Evento (se creará el usuario):  • UNIGIS
										,pedido_id_areaconvenio	 --	Identificador del Evento:  • 0
										,pedido_no_guia	 --	Identificador del Evento:  • 0
										,pedido_f_real_entrega	 --	Fecha y hora de registro del pedido:  • YYYY-MM-DD HH:MM:SS 
										,pedido_sist_origen	 --	Identificador del Evento:  • ZAM
										,pedido_id_pedido_maestro	 --	Identificador del Evento:  • 0
										,pedido_id_area_guia	 --	Identificador del Evento:  • 1
										,pedido_id_tipo_cancelacion	 --	Identificador del Evento:  • 0
										,pedido_num_pedido	 --	Identificador consecutivo autogenerado en LIS
										,pedido_por_cargar	 --	Identificador del Evento:  • 1
										,pedido_cargado	 --	Identificador del Evento:  • 0
										,pedido_pedido_creado	 --	Identificador del Evento:  • 0
										,pedido_id_solicitud	 --	Identificador del Evento:  • 0
										,pedido_tarifa	 --	Identificador del Evento:  • 0
										,pedido_id_autorizo	 --	Identificador del Evento (se creará el usuario):  • UNIGIS
										,pedido_f_desp_prog	 --	Fecha y hora de registro del pedido:  • YYYY-MM-DD HH:MM:SS 
										,pedido_id_tipo_unidad	 --	Identificador del Evento:  • 0
										,pedido_id_clienteFinal	 --	Identificador del Evento:  • 0
		--								,pedido_id_pedidopk	 --	Identificador consecutivo autogenerado en LIS
								from @ws
		--	catch the id_pedido_pk
								set @ctrl_pedido_id = SCOPE_IDENTITY()	
							
							
		--					go backwards and update or insert the id 
							
							insert into galdb.dbo.api_ws_historico_logs (message)
								select 
										'record with id '
										+ cast( @ctrl_log_id as varchar(max) ) 
										+ 'AND PEDIDO ID '
										+ cast( @ctrl_pedido_id as varchar(max) )
										+'- INSERT INTO dbo.desp_pedido -'
										+ '_EXECUTE AT '
										+ cast(current_timestamp as varchar(30)) 
									
		--insert data in pedido_detalle 
					insert into galdb.dbo.desp_pedido_detalle
							values
									(
										 @ctrl_pedido_id
										,'' --grr
										,'' --pedido_cliente
										,'' --ref_almacen
										,'' --ref_destinatario
										,0  --chk_cita
										,0  --chk_especial
										,0  --chk_urgente
										,0  --chk_recojo
										,0  --num_interno_tms
										,'' --tipo_documento
										,'' --tipo_doc_ref_dest
										,'' --ubigeo_origen
										,'' --ubigeo_dest
										,0.000000 --valor_mercaderia
										,0  --id_flota
									)
		
							insert into galdb.dbo.api_ws_historico_logs (message)
								select 
										'record with logID ' 
										+ cast( @ctrl_log_id as varchar(max) ) 
										+ 'AND PEDIDO ID '
										+ cast( @ctrl_pedido_id as varchar(max) )
										+'- INSERT INTO dbo.desp_pedido_detalle -'
										+ '_EXECUTE AT '
										+ cast(current_timestamp as varchar(30)) 
				commit transaction pedido
			end TRY
		
			begin CATCH
		
			    if (XACT_STATE()) = -1  
			    begin  
			        
			       	insert into galdb.dbo.api_ws_historico_logs (message)
						values ('The transaction is in an uncommittable state. desp.pedido: Rolling back transaction.')
					rollback transaction pedido
			    end  
			    -- Test whether the transaction is committable.  
			    if (XACT_STATE()) = 1  
			    begin  
			       	insert into galdb.dbo.api_ws_historico_logs (message)
						values ('The transaction is committable. desp.pedido: Committing transaction.')  
			        commit transaction pedido
			    end   
		    
				insert into galdb.dbo.api_ws_historico_logs (message)
				values (	
							'errNum : ' + cast(ERROR_NUMBER() as varchar(255)) + '| ' +
							'errLine : ' + cast(ERROR_LINE() as varchar(500)) + '| ' +
							'errMsg : ' + cast(ERROR_MESSAGE() as varchar(500)) + '| ' +
							'errState : ' + cast(ERROR_STATE() as varchar(255)) + '| ' +
							'errSeverity : ' + cast(ERROR_SEVERITY() as varchar(255)) + '| '
					   )
			
			end CATCH
		
		-- Set Modular inserts 
		
			begin TRY
				begin transaction asignacion -- initialize asignacion
					insert into galdb.dbo.desp_asignacion -- select  
														(
															 id_asignacion		--Identificador consecutivo autogenerado en LIS
															,num_asignacion	 --	Identificador consecutivo autogenerado en LIS
															,id_area	 --Identificador del Evento: • 1 | Área definida en LIS 
															,id_ruta	 --	Identificador de ruta recorrida (pendiente definición)
															,id_configuracionviaje	 --	Identificador del Evento: • 1 | Unidad Motriz • 2 | Sencillo • 3 | Full
		--													,id_operador2
														    ,id_personal	 --	Numero de empleado LIS, que realizo el viaje.
															,id_unidad	 --	Identificador de unidad LIS, que realizo el viaje.
															,id_remolque1	 --	Identificador de remolque 1 LIS, que realizo el viaje.
															,id_remolque2	 --	Identificador de remolque 2 LIS, que realizo el viaje.
															,id_dolly	 --	Identificador de dolly LIS, que realizo el viaje.
															,id_ingreso	 --	Identificador del Evento (se creará el usuario):  • UNIGIS
															,id_area_viaje
															,no_viaje	 --	Identificador consecutivo autogenerado en LIS
															,fecha_ingreso	 --	Fecha y hora de registro del viaje:  • YYYY-MM-DD HH:MM:SS 
															,f_prog_ini_viaje	 --	Fecha y hora de registro del viaje:  • YYYY-MM-DD HH:MM:SS 
															,f_prog_fin_viaje	 --	Fecha y hora de registro del viaje:  • YYYY-MM-DD HH:MM:SS 
															,status_asignacion	 --	Identificador del Evento:  • 3 | Viaje terminado
															,seguimiento_actual	 --	Identificador del Evento:  • 0
															,observaciones
															,id_flota	 --	Identificador del Evento:  • 1 | Local • 2 | Foráneo ??
															,id_seguimiento	 --	Identificador del Evento:  • 1 | Cargado • 2 | Vacío
															,no_kit
															,id_tipo_operacion	 --	Identificador del Evento:  • 0
															,kms_ruta	 --	Registro de kilómetros de la ruta-viaje.
															,kms_ruta1
															,tiempo_ruta
															,sist_origen	 --	Identificador del Evento:  • ZAM
		--													,capacidad_cargam3 --??
		--													,capacidad_cargakg --??
															,id_remitente	 --	Numero de cliente LIS.
															,id_destinatario	 --	Numero de cliente LIS.
															,id_linearem1	 --	Identificador del Evento:  • ZAM
															,id_linearem2	 --	Identificador del Evento:  • ZAM
														)
													select 
															 @id_asignacion		--Identificador consecutivo autogenerado en LIS
															,@num_asignacion	 --	Identificador consecutivo autogenerado en LIS
															,api_id_area	 --Identificador del Evento: • 1 | Área definida en LIS 
															,api_id_ruta	 --	Identificador de ruta recorrida (pendiente definición)
															,asignacion_id_configuracionviaje	 --	Identificador del Evento: • 1 | Unidad Motriz • 2 | Sencillo • 3 | Full
															,api_id_personal	 --	Numero de empleado LIS, que realizo el viaje.
															,api_id_unidad	 --	Identificador de unidad LIS, que realizo el viaje.
															,api_id_remolque1	 --	Identificador de remolque 1 LIS, que realizo el viaje.
															,api_id_remolque2	 --	Identificador de remolque 2 LIS, que realizo el viaje.
															,api_id_dolly	 --	Identificador de dolly LIS, que realizo el viaje.
															,api_id_ingreso	 --	Identificador del Evento (se creará el usuario):  • UNIGIS
															,0   --id_area_viaje 
															,api_no_viaje	 --	Identificador consecutivo autogenerado en LIS
															,api_fecha_ingreso	 --	Fecha y hora de registro del viaje:  • YYYY-MM-DD HH:MM:SS 
															,api_f_prog_ini_viaje	 --	Fecha y hora de registro del viaje:  • YYYY-MM-DD HH:MM:SS 
															,api_f_prog_fin_viaje	 --	Fecha y hora de registro del viaje:  • YYYY-MM-DD HH:MM:SS 
															,asignacion_status_asignacion	 --	Identificador del Evento:  • 3 | Viaje terminado
															,asignacion_seguimiento_actual	 --	Identificador del Evento:  • 0
															,'' --observaciones
															,asignacion_id_flota	 --	Identificador del Evento:  • 1 | Local • 2 | Foráneo ??
															,asignacion_id_seguimiento	 --	Identificador del Evento:  • 1 | Cargado • 2 | Vacío
															,'' -- no kit
															,api_id_tipo_operacion	 --	Identificador del Evento:  • 0
															,asignacion_kms_ruta	 --	Registro de kilómetros de la ruta-viaje.
															,asignacion_kms_ruta as 'kms_ruta1'
															,0.0000 --tiempo
															,asignacion_sist_origen	 --	Identificador del Evento:  • ZAM
		--													,
		--													,
															,api_id_remitente	 --	Numero de cliente LIS.
															,api_id_destinatario	 --	Numero de cliente LIS.
															,api_id_linearem1	 --	Identificador del Evento:  • ZAM
															,api_id_linearem2	 --	Identificador del Evento:  • ZAM
													from @ws
												
		--										set @id_asignacion = SCOPE_IDENTITY()
												
							insert into galdb.dbo.api_ws_historico_logs (message)
								select 
										'record with LogID' 
										+ cast( @ctrl_log_id as varchar(max) ) 
										+ 'AND ID-ASIGNACION '
										+ cast( @id_asignacion as varchar(max) )
										+'- INSERT INTO dbo.desp_asignacion -'
										+ '_EXECUTE AT '
										+ cast(current_timestamp as varchar(30))	
				commit transaction asignacion
			end TRY
			begin CATCH
		
				    if (XACT_STATE()) = -1  
			    begin  
			        
			       	insert into galdb.dbo.api_ws_historico_logs (message)
						values ('The transaction is in an uncommittable state.: Rolling back transaction.')
					rollback transaction asignacion
			    end  
			    -- Test whether the transaction is committable.  
			    if (XACT_STATE()) = 1  
			    begin  
			       	insert into galdb.dbo.api_ws_historico_logs (message)
						values ('The transaction is committable.: Committing transaction.')  
			        commit transaction asignacion
			    end   
		    
				insert into galdb.dbo.api_ws_historico_logs (message)
				values (	
							'errNum : ' + cast(ERROR_NUMBER() as varchar(255)) + '| ' +
							'errLine : ' + cast(ERROR_LINE() as varchar(500)) + '| ' +
							'errMsg : ' + cast(ERROR_MESSAGE() as varchar(500)) + '| ' +
							'errState : ' + cast(ERROR_STATE() as varchar(255)) + '| ' +
							'errSeverity : ' + cast(ERROR_SEVERITY() as varchar(255)) + '| '
					   )
			
			end CATCH
			
			begin TRY
				begin transaction trip -- initialize viaje
						insert into galdb.dbo.trafico_viaje (  -- select 
										 id_area	 --Identificador del Evento: • 1 | Área definida en LIS 
										,id_ruta	 --	Identificador de ruta recorrida (pendiente definición)
										,id_configuracionviaje	 --	Identificador del Evento: • 1 | Unidad Motriz • 2 | Sencillo • 3 | Full
										,id_personal	 --	Numero de empleado LIS, que realizo el viaje.
										,id_unidad	 --	Identificador de unidad LIS, que realizo el viaje.
										,id_remolque1	 --	Identificador de remolque 1 LIS, que realizo el viaje.
										,id_remolque2	 --	Identificador de remolque 2 LIS, que realizo el viaje.
										,id_dolly	 --	Identificador de dolly LIS, que realizo el viaje.
										,id_ingreso	 --	Identificador del Evento (se creará el usuario):  • UNIGIS
										,no_viaje	 --	Identificador consecutivo autogenerado en LIS
										,fecha_ingreso	 --	Fecha y hora de registro del viaje:  • YYYY-MM-DD HH:MM:SS 
										,f_prog_ini_viaje	 --	Fecha y hora de registro del viaje:  • YYYY-MM-DD HH:MM:SS 
										,f_prog_fin_viaje	 --	Fecha y hora de registro del viaje:  • YYYY-MM-DD HH:MM:SS 
										,kms_camion_vacio	 --	Registro de kilómetros de la ruta-viaje en caso de que sea cargado, sino 0
										,kms_camion_lleno	 --	Registro de kilómetros de la ruta-viaje en caso de que sea vacio, sino 0
										,kms_operador	 --	Registro de kilómetros de la ruta-viaje.
										,lts_viaje	 --	Identificador del Evento:  • 0.0000
										,rendimiento_viaje	 --	Identificador del Evento:  • 0.0000
										,rendimiento_real	 --	Identificador del Evento:  • 0.0000
										,status_viaje	 --	Identificador del Evento:  • C
										,sueldo_operador	 --	Identificador del Evento:  • 0.0000
										,compensaciones	 --	Identificador del Evento:  • 0.0000
										,lts_empresa	 --	Identificador del Evento:  • 0.0000
										,rend_esperado	 --	Identificador del Evento:  • 0.0000
										,status_desp	 --	Identificador del Evento:  • T
										,viajeactual	 --	Identificador del Evento: • N
										,status_cs	 --	Identificador del Evento:  • E
										,id_origen	 --	 Numero de plaza LIS (pendiente definición).
										,id_destino	 --	 Numero de plaza LIS (pendiente definición).
										,id_linearem1	 --	Identificador del Evento:  • ZAM
										,id_linearem2	 --	Identificador del Evento:  • ZAM
										,id_statusviaje	 --	Identificador del Evento:  • 6
										,id_contrato	 --	Identificador del Evento:  • 0
										,id_tipo_servicio	 --	Identificador del Evento:  • 0
										
						)
						select 
										 api_id_area	 --Identificador del Evento: • 1 | Área definida en LIS 
										,api_id_ruta	 --	Identificador de ruta recorrida (pendiente definición)
										,asignacion_id_configuracionviaje	 --	Identificador del Evento: • 1 | Unidad Motriz • 2 | Sencillo • 3 | Full
										,api_id_personal	 --	Numero de empleado LIS, que realizo el viaje.
										,api_id_unidad	 --	Identificador de unidad LIS, que realizo el viaje.
										,api_id_remolque1	 --	Identificador de remolque 1 LIS, que realizo el viaje.
										,api_id_remolque2	 --	Identificador de remolque 2 LIS, que realizo el viaje.
										,api_id_dolly	 --	Identificador de dolly LIS, que realizo el viaje.
										,api_id_ingreso	 --	Identificador del Evento (se creará el usuario):  • UNIGIS
										,api_no_viaje	 --	Identificador consecutivo autogenerado en LIS
										,api_fecha_ingreso	 --	Fecha y hora de registro del viaje:  • YYYY-MM-DD HH:MM:SS 
										,api_f_prog_ini_viaje	 --	Fecha y hora de registro del viaje:  • YYYY-MM-DD HH:MM:SS 
										,api_f_prog_fin_viaje	 --	Fecha y hora de registro del viaje:  • YYYY-MM-DD HH:MM:SS 
										,viaje_kms_camion_vacio	 --	Registro de kilómetros de la ruta-viaje en caso de que sea cargado, sino 0
										,viaje_kms_camion_lleno	 --	Registro de kilómetros de la ruta-viaje en caso de que sea vacio, sino 0
										,viaje_kms_operador	 --	Registro de kilómetros de la ruta-viaje.
										,viaje_lts_viaje	 --	Identificador del Evento:  • 0.0000
										,viaje_rendimiento_viaje	 --	Identificador del Evento:  • 0.0000
										,viaje_rendimiento_real	 --	Identificador del Evento:  • 0.0000
										,viaje_status_viaje	 --	Identificador del Evento:  • C
										,viaje_sueldo_operador	 --	Identificador del Evento:  • 0.0000
										,viaje_compensaciones	 --	Identificador del Evento:  • 0.0000
										,viaje_lts_empresa	 --	Identificador del Evento:  • 0.0000
										,viaje_rend_esperado	 --	Identificador del Evento:  • 0.0000
										,viaje_status_desp	 --	Identificador del Evento:  • T
										,viaje_viajeactual	 --	Identificador del Evento: • N
										,viaje_status_cs	 --	Identificador del Evento:  • E
										,api_id_origen	 --	 Numero de plaza LIS (pendiente definición).
										,api_id_destino	 --	 Numero de plaza LIS (pendiente definición).
										,api_id_linearem1	 --	Identificador del Evento:  • ZAM
										,api_id_linearem2	 --	Identificador del Evento:  • ZAM
										,viaje_id_statusviaje	 --	Identificador del Evento:  • 6
										,api_id_contrato	 --	Identificador del Evento:  • 0
										,viaje_id_tipo_servicio	 --	Identificador del Evento:  • 0
					 	from @ws
					 
		----			 LOG
					 			insert into galdb.dbo.api_ws_historico_logs (message)
								select 
										'record with id ' 
										+ cast( @ctrl_log_id as varchar(max) ) 
										+ 'AND NO VIAJE '
										+ cast( @no_viaje as varchar(max) )
										+'- INSERT INTO dbo.trafico_viaje -'
										+ '_EXECUTE AT '
										+ cast(current_timestamp as varchar(30)) 
				commit transaction trip
			end TRY
			begin CATCH
		
				    if (XACT_STATE()) = -1  
			    begin  
			        
			       	insert into galdb.dbo.api_ws_historico_logs (message)
						values ('The transaction is in an uncommittable state.: Rolling back transaction.')
					rollback transaction trip
			    end  
		--	     Test whether the transaction is committable.  
			    if (XACT_STATE()) = 1  
			    begin  
			       	insert into galdb.dbo.api_ws_historico_logs (message)
						values ('The transaction is committable.: Committing transaction.')  
			        commit transaction trip
			    end   
		    
				insert into galdb.dbo.api_ws_historico_logs (message)
				values (	
							'errNum : ' + cast(ERROR_NUMBER() as varchar(255)) + '| ' +
							'errLine : ' + cast(ERROR_LINE() as varchar(500)) + '| ' +
							'errMsg : ' + cast(ERROR_MESSAGE() as varchar(500)) + '| ' +
							'errState : ' + cast(ERROR_STATE() as varchar(255)) + '| ' +
							'errSeverity : ' + cast(ERROR_SEVERITY() as varchar(255)) + '| '
					   )
			
			end CATCH
		
			
			begin TRY
		
				begin transaction renglon  --initialize trafico_renglon_viaje
		
						insert into galdb.dbo.trafico_renglon_viaje ( --select
										 id_area	 --Identificador del Evento: • 1 | Área definida en LIS 
										,no_viaje	 --Identificador consecutivo autogenerado en LIS
										,id_tramo	 --int null 	Identificador del Evento:  • 0
									    ,consecutivo_viaje --decimal(18,6) null Identificador del Evento:  • 1.0000
									    ,status_km	--varchar(10) null Identificador del Evento:  • ''
									    ,direccion_tramo --int null Identificador del Evento:  • 1
								    	,ramal	--int null Identificador del Evento:  • 0
								    	,desc_tramo --varchar(255) null descripcion de la ruta 
								    	,kms_tramo	--int null Registro de kilometros de la ruta-viaje
		--						    	,vol_recuperado	--varchar(255) null Identificador del Evento:  • NULL
										,no_guia	 	--Identificador del Evento:  • 0
		--add next fields 		 
				 	)
				 				select 
										 api_id_area	-- Identificador del Evento: • 1 | Área definida en LIS 
										,@no_viaje	 	--Identificador consecutivo autogenerado en LIS
										,viaje_id_tramo		--int null 	Identificador del Evento:  • 0
									    ,viaje_consecutivo_viaje --decimal(18,6) null Identificador del Evento:  • 1.0000
									    ,viaje_status_km	--varchar(10) null Identificador del Evento:  • ''
									    ,viaje_direccion_tramo --int null Identificador del Evento:  • 1
								    	,viaje_ramal	--int null Identificador del Evento:  • 0
								    	,viaje_desc_tramo --varchar(255) null descripcion de la ruta 
								    	,viaje_kms_tramo	--int null Registro de kilometros de la ruta-viaje
		--						    	,viaje_vol_recuperado	--varchar(255) null Identificador del Evento:  • NULL
										,pedido_no_guia	 	--Identificador del Evento:  • 0		 			
								from 
										@ws
									
		--			 LOG
					 			insert into galdb.dbo.api_ws_historico_logs (message)
								select 
										'record with id ' 
										+ cast( @ctrl_log_id as varchar(max) ) 
										+ 'AND NOVIAJE '
										+ cast( @no_viaje as varchar(max) )
										+'- INSERT INTO dbo.trafico_renglon_viaje -'
										+ '_EXECUTE AT '
										+ cast(current_timestamp as varchar(30)) 
		
									
								-- update no_viaje and id_asignacion 
								select 
										 @id_asignacion = asignacion_id_asignacion
										,@no_anticipo = anticipo_no_anticipo 
								from 
										galdb.dbo.api_gal_viajes_logs
								where 
										sie_ctrl = 2 and api_ctrl = @api_ctrl
						--		tables : 
								update 
									galdb.dbo.trafico_anticipo
								set 
									 no_viaje = @no_viaje
									,id_asignacion  = @id_asignacion
								where 
									no_anticipo = @no_anticipo

				commit transaction renglon 
			end TRY
			begin CATCH
		
				    if (XACT_STATE()) = -1  
			    begin  
			        
			       	insert into galdb.dbo.api_ws_historico_logs (message)
						values ('The transaction is in an uncommittable state.: Rolling back transaction.')
					rollback transaction renglon
			    end  
		 
			    if (XACT_STATE()) = 1  
			    begin  
			       	insert into galdb.dbo.api_ws_historico_logs (message)
						values ('The transaction is committable.: Committing transaction.')  
			        commit transaction renglon
			    end   
		    
				insert into galdb.dbo.api_ws_historico_logs (message)
				values (	
							'errNum : ' + cast(ERROR_NUMBER() as varchar(255)) + '| ' +
							'errLine : ' + cast(ERROR_LINE() as varchar(500)) + '| ' +
							'errMsg : ' + cast(ERROR_MESSAGE() as varchar(500)) + '| ' +
							'errState : ' + cast(ERROR_STATE() as varchar(255)) + '| ' +
							'errSeverity : ' + cast(ERROR_SEVERITY() as varchar(255)) + '| '
					   )			
			end CATCH
			
--			start updates
						
-- =========================================================================== --
-- Update from Viaje
-- =========================================================================== --

	-- Next issue	
--		begin TRY
--			begin transaction upt_anticipo
--				-- update no_viaje and id_asignacion 
--				select 
--						 @id_asignacion = asignacion_id_asignacion
--						,@no_anticipo = anticipo_no_anticipo 
--				from 
--						galdb.dbo.api_gal_viajes_logs
--				where 
--						sie_ctrl = 2 and api_ctrl = @api_ctrl
--		--		tables : 
--				update 
--					galdb.dbo.trafico_anticipo
--				set 
--					 no_viaje = @no_viaje
--					,id_asignacion  = @id_asignacion
--				where 
--					no_anticipo = @no_anticipo
--				
--			commit transaction upt_anticipo
--		end TRY
--		begin CATCH
--	
--			    if (XACT_STATE()) = -1  
--		    begin  
--		        
--		       	insert into galdb.dbo.api_ws_historico_logs (message)
--					values ('The transaction is in an uncommittable state.: Rolling back transaction.')
--				rollback transaction upt_anticipo
--		    end  
--		    -- Test whether the transaction is committable.  
--		    if (XACT_STATE()) = 1  
--		    begin  
--		       	insert into galdb.dbo.api_ws_historico_logs (message)
--					values ('The transaction is committable.: Committing transaction.')  
--		        commit transaction upt_anticipo
--		    end   
--	    
--			insert into galdb.dbo.api_ws_historico_logs (message)
--			values (	
--						'errNum : ' + cast(ERROR_NUMBER() as varchar(255)) + '| ' +
--						'errLine : ' + cast(ERROR_LINE() as varchar(500)) + '| ' +
--						'errMsg : ' + cast(ERROR_MESSAGE() as varchar(500)) + '| ' +
--						'errState : ' + cast(ERROR_STATE() as varchar(255)) + '| ' +
--						'errSeverity : ' + cast(ERROR_SEVERITY() as varchar(255)) + '| '
--				   )
--		
--		end CATCH
			
	end -- END IF Viaje

-- =========================================================================== --
-- Anticipo trigger
-- =========================================================================== --

--	2nd case with anticipo
if  @sie_ctrl = 2 -- @api_ctrl = no_viaje
	begin -- begin if Anticipo
--		declare @trancount int;
--		set @trancount = @@trancount;
-- Next issue	
		begin TRY
	
--		if @trancount = 0
			begin transaction anticipo
--		else
--			begin transaction anticipo
		
--		test anticipo
--	select max(no_anticipo) from galdb.dbo.trafico_anticipo
--  NOTE in the insert must set no_anticipo to [id+1] and no_viaje = 0 and id_asignacion = 0
		
				insert into
				  galdb.dbo.trafico_anticipo
				      (
				         id_area
				        ,no_anticipo		-- [id+1]
				        ,no_viaje			-- set to zero
				        ,fecha_anticipo
				        ,monto_anticipo
				        ,status_anticipo
				        ,monto_anticipo_iva
				        ,id_ingreso
				        ,fecha_ingreso
				        ,observaciones
				        ,id_area_liq
				        ,transfer_nomina
				        ,monto_devo
				        ,tipo_anticipo
				        ,id_operador
				        ,no_viajeant
				        ,id_asignacion		-- set to zero
				        ,id_areaviaje
				        ,id_plazaorigen
				        ,id_areapago
				        ,clasificacion
				        ,rebasa_presupuesto
				        ,tipo_autorizacion
				        ,id_compania
				        ,impreso
				        ,tipo_anticipo_convenio
				        ,id_ingreso_antext
				      )
				select 
					-- fields @...
				        1                         		-- id_area
				       ,@no_anticipo                    -- no_anticipo 				--BUILD
				       ,0 			                   	-- no_viaje					-- set to zero,update later
				       ,cast(anticipo_fecha_anticipo as datetime)     	-- fecha_anticipo			--BUILD
				       ,anticipo_monto_anticipo        	-- monto_anticipo			--BUILD
				       ,'A'                       		-- status_anticipo			--BUILD
				       ,anticipo_monto_anticipo_iva		-- monto_anticipo_iva		--BUILD
				       ,'SA'                      		-- id_ingreso				--BUILD
				       ,anticipo_fecha_ingreso    		-- fecha_ingreso			--BUILD
				       ,anticipo_observaciones    		-- observaciones
				       ,0                         		-- id_area_liq
				       ,0                         		-- transfer_nomina
				       ,0                         		-- monto_devo
				       ,0                         		-- tipo_anticipo
				       ,api_id_personal                 -- id_operador
				       ,0                         		-- no_viajeant
				       ,0  				                -- id_asignacion   			-- set to zero,update later
				       ,1                         		-- id_areaviaje
				       ,0                         		-- id_plazaorigen
				       ,1                         		-- id_areapago
				       ,0                         		-- clasificacion
				       ,0                         		-- rebasa_presupuesto
				       ,0                         		-- tipo_autorizacion
				       ,0                         		-- id_compania
				       ,0                         		-- impreso
				       ,'E'                       		-- tipo_anticipo_convenio
				       ,'UNIGIS'                  		-- id_ingreso_antext
				from 
					@ws

-- ================================================================================================================= --		
				update 
						galdb.dbo.api_gal_viajes_logs 
				set 
						anticipo_no_anticipo = @no_anticipo 
				where 
						sie_ctrl = 2 and api_ctrl = @api_ctrl and id = @ctrl_log_id
-- ================================================================================================================= --

	 			insert into galdb.dbo.api_ws_historico_logs (message)
				select 
						'record with id trafico-anticipo' 
						+ cast( @ctrl_log_id as varchar(max) ) 
						+ 'AND NOANTICIPO '
						+ cast( @no_anticipo as varchar(max) )
						+'- INSERT INTO dbo.trafico_anticipo -'
						+ '_EXECUTE AT '
						+ cast(current_timestamp as varchar(30))

				commit transaction anticipo		
			
		end TRY
--	
		begin CATCH
--	
			if (XACT_STATE()) = -1  
			    begin  
			        
			       	insert into galdb.dbo.api_ws_historico_logs (message)
						values ('The transaction is in an uncommittable state.: Rolling back transaction. -- AnticipoModule --')
					rollback transaction anticipo
			    end  
		    -- Test whether the transaction is committable.  
		    if (XACT_STATE()) = 1  
			    begin  
			       	insert into galdb.dbo.api_ws_historico_logs (message)
						values ('The transaction is committable.: Committing transaction. -- AnticipoModule --')  
			        commit transaction anticipo
			    end   
	    
			insert into galdb.dbo.api_ws_historico_logs (message)
			values (	
						'errNum : ' + cast(ERROR_NUMBER() as varchar(255)) + '| ' +
						'errLine : ' + cast(ERROR_LINE() as varchar(500)) + '| ' +
						'errMsg : ' + cast(ERROR_MESSAGE() as varchar(500)) + '| ' +
						'errState : ' + cast(ERROR_STATE() as varchar(255)) + '| ' +
						'errSeverity : ' + cast(ERROR_SEVERITY() as varchar(255)) + '| '
				   )
		
		end CATCH
		
		-- =========================================================================== --
-- Cartas Porte trigger
-- =========================================================================== --

-- Next issue	
		begin TRY
			begin transaction renglon_anticipo
		
					insert into -- select * from 
						galdb.dbo.trafico_renglon_anticipo
							(
								 id_area
								,no_anticipo
								,id_concepto
								,importe_anticipo
								,iva_anticipo
								,cantidad
								,id_concepto_clasif
							)
						select 
								 1								--id_area
								,@no_anticipo					--no_anticipo
								,11								--id_concepto
								,anticipo_monto_anticipo		--importe_anticipo
								,anticipo_monto_anticipo_iva	--iva_anticipo
								,1.00							--cantidad
								,0								--id_concepto_clasif
						from 
							@ws
						
		 			insert into galdb.dbo.api_ws_historico_logs (message)
					select 
							'record with id renglon-anticipo' 
							+ cast( @ctrl_log_id as varchar(max) ) 
							+ 'AND NOANTICIPO '
							+ cast( @no_anticipo as varchar(max) )
							+'- INSERT INTO dbo.trafico_anticipo -'
							+ '_EXECUTE AT '
							+ cast(current_timestamp as varchar(30))
						
						
			commit transaction renglon_anticipo
		end TRY
		begin CATCH
	
			    if (XACT_STATE()) = -1  
		    begin  
		        
		       	insert into galdb.dbo.api_ws_historico_logs (message)
					values ('The transaction is in an uncommittable state.: Rolling back transaction.')
				rollback transaction other
		    end  
		    -- Test whether the transaction is committable.  
		    if (XACT_STATE()) = 1  
		    begin  
		       	insert into galdb.dbo.api_ws_historico_logs (message)
					values ('The transaction is committable.: Committing transaction.')  
		        commit transaction other
		    end   
	    
			insert into galdb.dbo.api_ws_historico_logs (message)
			values (	
						'errNum : ' + cast(ERROR_NUMBER() as varchar(255)) + '| ' +
						'errLine : ' + cast(ERROR_LINE() as varchar(500)) + '| ' +
						'errMsg : ' + cast(ERROR_MESSAGE() as varchar(500)) + '| ' +
						'errState : ' + cast(ERROR_STATE() as varchar(255)) + '| ' +
						'errSeverity : ' + cast(ERROR_SEVERITY() as varchar(255)) + '| '
				   )
		
		end CATCH
		
	end -- end if anticipo 

	
-- =========================================================================== --
-- Cartas Porte trigger
-- =========================================================================== --
-- select max(no_guia) from galdb.dbo.trafico_guia 
-- Next issue	
if  @sie_ctrl = 3 -- @api_ctrl = no_viaje
	begin -- begin if Carta Porte
	begin TRY
		begin transaction cporte
				insert into galdb.dbo.trafico_guia
						(
                           id_area                 --
                          ,no_guia
                          ,tipo_pago
                          ,status_guia
                          ,tipo_cobro
                          ,clasificacion_doc
                          ,fecha_guia
                          ,fecha_ingreso
                          ,conv_operador
                          ,conv_permisionario
                          ,id_area_facturacion
                          ,id_cliente              -- 
                          ,id_personal
                          ,no_viaje
                          ,id_remitente
                          ,id_destinatario
                          ,id_condujo
--                          ,id_operador2
                          ,kms_guia
                          ,flete
                          ,seguro
                          ,maniobras
                          ,autopistas
                          ,otros
                          ,subtotal
                          ,iva_guia
                          ,monto_tipo_cambio
                          ,tipo_doc
                          ,id_origen
                          ,id_destino
                          ,id_fraccion
                          ,plaza_emision
                          ,conducir_de
                          ,conducir_a
                          ,id_tipo_moneda
                          ,num_guia
                          ,id_unidad
                          ,valor_declarado
                          ,prestamo
                          ,id_ingreso
--                          ,unidadplaca
--                          ,unidadtipo
                          ,monto_ncredito
                          ,monto_ncargo
                          ,monto_pago
                          ,factor_iva
                          ,cant_movguia
                          ,id_serieguia
                          ,monto_ivancredito
                          ,monto_ivancargo
                          ,monto_comisiontercero
                          ,tipo_origen
                          ,monto_retencion
                          ,status_pago
                          ,monto_retenciontercero
                          ,control_pago
                          ,monto_ivaflete
                          ,cobro_viaje_kms
--                          ,id_convenio
                          ,observaciones_guia
                          ,id_iva
                          ,id_retencion
                          ,tipo_serv
						  ,id_tipo_operacion
					  	  ,id_seguro
--					      ,tipo_detalle
				          ,id_serieguia2
			          	  ,numero_contrato
			              ,codigo
		              	  ,medida
		                  ,campo8
	                      ,campo9
                      	  ,kms_cpac
                          ,tipo_cpac
                          ,factor_cpac
                          ,id_compania
                          ,id_modifico
                          ,fecha_modifico
                          ,id_contrato
						)
				select 
                           1 							-- id_area                 --
                          ,@no_guia 		-- no_guia
                          ,'R' 							-- tipo_pago
                          ,'C' 							-- status_guia
                          ,'V' 							-- tipo_cobro
                          ,1 							-- clasificacion_doc
                          ,guia_fecha_guia 				-- fecha_guia
                          ,guia_fecha_guia 				-- fecha_ingreso
                          ,0.00 						-- conv_operador
                          ,0.00 						-- conv_permisionario
                          ,0 							-- id_area_facturacion
                          ,pedido_id_cliente  			-- id_cliente 
                          ,api_id_personal
                          ,0							-- no_viaje set viaje as zero for next update
                          ,api_id_remitente
                          ,api_id_destinatario
                          ,api_id_personal 				-- id_condujo
--                          ,id_operador2
                          ,asignacion_kms_ruta
                          ,guia_flete --flete    		-- Valor Proveeniente de GST
                          ,0.00 						-- seguro
                          ,0.00 						-- maniobras
                          ,0.00 						-- autopistas
                          ,guia_flete 					-- otros
                          ,guia_subtotal
                          ,guia_iva_guia
                          ,1 							-- monto_tipo_cambio
                          ,2 							-- tipo_doc
                          ,api_id_origen
                          ,api_id_destino
                          ,renglon_id_fraccion 			-- id_fraccion
                          ,54 							-- plaza_emision
                          ,api_id_origen 				-- conducir_de
                          ,api_id_destino 				-- conducir_a
                          ,1 							-- id_tipo_moneda
                          ,guia_num_guia  				-- num_guia
                          ,api_id_unidad
                          ,'0' 							-- valor_declarado
                          ,'N' 							-- prestamo
                          ,'GAL' 						-- id_ingreso
--                          ,unidadplaca
--                          ,unidadtipo
                          ,0.00 						-- monto_ncredito
                          ,0.00 						-- monto_ncargo
                          ,0.00 						-- monto_pago
                          ,0.16 						-- factor_iva
                          ,0 							-- cant_movguia
                          ,guia_id_serieguia 		--'valor proveniente de GST' --id_serieguia
                          ,0.00   	--monto_ivancredito
                          ,0.00   	--monto_ivancargo
                          ,0.00   	--monto_comisiontercero
                          ,0      	--tipo_origen
                          ,guia_monto_retencion      	--'valor proveniente de GST ' --monto_retencion
                          ,'N'    	--status_pago
                          ,0.00   	--monto_retenciontercero
                          ,'N'    	--control_pago
                          ,0.00		--monto_ivaflete
                          ,guia_flete --cobro_viaje_kms
--                          ,id_convenio
                          ,'UNIGIS' --observaciones_guia
                          ,guia_id_iva
                          ,guia_id_retencion
                          ,04 --tipo_serv
						  ,0 --id_tipo_operacion
					  	  ,0 --id_seguro
--					      ,tipo_detalle
				          ,0 --id_serieguia2
			          	  ,'' --numero_contrato
			              ,'' --codigo
		              	  ,'' --medida
		                  ,'' --campo8
	                      ,'' --campo9
                      	  ,0 --kms_cpac
                          ,0 --tipo_cpac
                          ,0.00 --factor_cpac
                          ,0 --id_compania
                          ,'GAL' --id_modifico
                          ,guia_fecha_guia --fecha_modifico
                          ,0 --id_contrato
				from 
					@ws
				
	 			insert into galdb.dbo.api_ws_historico_logs (message)
				select 
						'record with id guia' 
						+ cast( @ctrl_log_id as varchar(max) ) 
						+ 'AND NOANTICIPO '
						+ cast( @no_guia as varchar(max) )
						+'- INSERT INTO dbo.trafico_anticipo -'
						+ '_EXECUTE AT '
						+ cast(current_timestamp as varchar(30))
					
		commit transaction cporte
----		
	end TRY
	begin CATCH

		    if (XACT_STATE()) = -1  
	    begin  
	        
	       	insert into galdb.dbo.api_ws_historico_logs (message)
				values ('The transaction is in an uncommittable state.: Rolling back transaction.')
			rollback transaction cporte
	    end  
	    -- Test whether the transaction is committable.  
	    if (XACT_STATE()) = 1  
	    begin  
	       	insert into galdb.dbo.api_ws_historico_logs (message)
				values ('The transaction is committable.: Committing transaction.')  
	        commit transaction cporte
	    end   
    
		insert into galdb.dbo.api_ws_historico_logs (message)
		values (	
					'errNum : ' + cast(ERROR_NUMBER() as varchar(255)) + '| ' +
					'errLine : ' + cast(ERROR_LINE() as varchar(500)) + '| ' +
					'errMsg : ' + cast(ERROR_MESSAGE() as varchar(500)) + '| ' +
					'errState : ' + cast(ERROR_STATE() as varchar(255)) + '| ' +
					'errSeverity : ' + cast(ERROR_SEVERITY() as varchar(255)) + '| '
			   )
	
	end CATCH
	

--RENGLON GUIA
-- =========================================================================== --
-- Cartas Porte Renglon guia 
-- =========================================================================== --

-- Next issue	
	begin TRY
		begin transaction renglonguia
	
 			insert into galdb.dbo.api_ws_historico_logs (message)
				select 
						'record with id renglon guia' 
						+ cast( @ctrl_log_id as varchar(max) ) 
						+ 'AND NO_GUIA '
						+ cast( @no_guia as varchar(max) )
						+'- INSERT INTO dbo.trafico_anticipo -'
						+ '_EXECUTE AT '
						+ cast((select no_guia from galdb.dbo.trafico_renglon_guia where no_guia = @no_guia) as varchar(max))
					
--select * from galdb.dbo.trafico_renglon_guia
				insert into galdb.dbo.trafico_renglon_guia
							( -- select 
								 id_area
								,consecutivo
								,no_guia				-- from trafico_guia
								,id_producto
								,id_fraccion
								,cantidad
								,embalaje 
								,descripcion_producto
								,peso 
								,volumen_m3
								,peso_estimado
								,importe
								,importe_reparto
								,volumen_20
								,volumen_natural
								,volumen_carga
								,volumen_descarga
								,peso_especifico
								,temperatura
								,temperatura_carga
								,temperatura_descarga
								,id_unidad_medida
								,sku
								,unidad_medida
								,linea
								,clase
								,subclase
								,num_correlativo_cte
								,peso_bruto
								,peso_tara
								,volumen_20_descarga --from galdb.dbo.trafico_renglon_guia
							)
						select 
								 1 			--id_area
								,1 			--consecutivo
								,@no_guia
								,16			--id_producto
								,0			--id_fraccion
								,0.00		--cantidad
								,99			--embalaje 
								,'VARIOS' 	--descripcion_producto
								,0.00  --peso 
								,0.00  --volument_m3
								,0.00  --peso_estimado
								,0.00  --importe
								,0.00  --importe_reparto
								,0.00  --volumen_20
								,0.00  --volumen_natural
								,0.00  --volumen_carga
								,0.00  --volumen_descarga
								,0.00  --peso_especifico
								,0.00  --temperatura
								,0.00  --temperatura_carga
								,0.00  --temperatura_descarga
								,0  --id_unidad_medida
								,''  --sku
								,''  --unidad_medida
								,''  --linea
								,''  --clase
								,''  --subclase
								,0  --num_correlativo_cte
								,0.00  --peso_bruto
								,0.00  --peso_tara
								,0.00  --volumen_20_descarga
						from
							@ws
--					
	 			insert into galdb.dbo.api_ws_historico_logs (message)
				select 
						'record with id renglon guia' 
						+ cast( @ctrl_log_id as varchar(max) ) 
						+ 'AND NO_GUIA '
						+ cast( @no_guia as varchar(max) )
						+'- INSERT INTO dbo.trafico_anticipo -'
						+ '_EXECUTE AT '
						+ cast(current_timestamp as varchar(30))

		commit transaction renglonguia
	end TRY

	begin CATCH

		    if (XACT_STATE()) = -1  
	    begin  
	        
	       	insert into galdb.dbo.api_ws_historico_logs (message)
				values ('The transaction is in an uncommittable state.: Rolling back transaction.')
			rollback transaction renglonguia
	    end  
	    -- Test whether the transaction is committable.  
	    if (XACT_STATE()) = 1  
	    begin  
	       	insert into galdb.dbo.api_ws_historico_logs (message)
				values ('The transaction is committable.: Committing transaction.')  
	        commit transaction renglonguia
	    end   
    
		insert into galdb.dbo.api_ws_historico_logs (message)
		values (	
					'errNum : ' + cast(ERROR_NUMBER() as varchar(255)) + '| ' +
					'errLine : ' + cast(ERROR_LINE() as varchar(500)) + '| ' +
					'errMsg : ' + cast(ERROR_MESSAGE() as varchar(500)) + '| ' +
					'errState : ' + cast(ERROR_STATE() as varchar(255)) + '| ' +
					'errSeverity : ' + cast(ERROR_SEVERITY() as varchar(255)) + '| '
			   )
	
	end CATCH
-- ======================================================================================================== --

-- =========================================================================== --
-- GUIA_DETALLE
-- =========================================================================== --

-- Next issue	
	begin TRY
		begin transaction guia_detalle
					
				-- GUIA DETALLE
				insert into galdb.dbo.trafico_guia_detalle
							(
								 no_guia
								,id_area
								,grr
								,pedido_cliente
								,ref_almacen
								,ref_destinatario
								,chk_cita
								,chk_especial
								,chk_urgente
								,chk_recojo
								,num_interno_tms
								,tipo_documento
								,tipo_doc_ref_dest
								,ubigeo_origen
								,ubigeo_dest
								,valor_mercaderia				
							)
						select 
								 @no_guia  -- no_guia
								,1  -- id_area
								,'' -- grr
								,'' -- pedido_cliente
								,'' --ref_almacen
								,'' --ref_destinatario
								,0 	--chk_cita
								,0 	--chk_especial
								,0 	--chk_urgente
								,0 	--chk_recojo
								,0 	--num_interno_tms
								,'' --tipo_documento
								,'' --tipo_doc_ref_dest
								,'' --ubigeo_origen
								,'' --ubigeo_dest
								,0 --valor_mercaderia
						from
							@ws
					
	 			insert into galdb.dbo.api_ws_historico_logs (message)
				select 
						'record with id Guia Detalle' 
						+ cast( @ctrl_log_id as varchar(max) ) 
						+ 'AND NOANTICIPO '
						+ cast( @no_anticipo as varchar(max) )
						+'- INSERT INTO dbo.trafico_guia_detalle -'
						+ '_EXECUTE AT '
						+ cast(current_timestamp as varchar(30))

		commit transaction guia_detalle
	end TRY
	begin CATCH

		    if (XACT_STATE()) = -1  
	    begin  
	        
	       	insert into galdb.dbo.api_ws_historico_logs (message)
				values ('The transaction is in an uncommittable state.: Rolling back transaction.')
			rollback transaction other
	    end  
	    -- Test whether the transaction is committable.  
	    if (XACT_STATE()) = 1  
	    begin  
	       	insert into galdb.dbo.api_ws_historico_logs (message)
				values ('The transaction is committable.: Committing transaction.')  
	        commit transaction other
	    end   
    
		insert into galdb.dbo.api_ws_historico_logs (message)
		values (	
					'errNum : ' + cast(ERROR_NUMBER() as varchar(255)) + '| ' +
					'errLine : ' + cast(ERROR_LINE() as varchar(500)) + '| ' +
					'errMsg : ' + cast(ERROR_MESSAGE() as varchar(500)) + '| ' +
					'errState : ' + cast(ERROR_STATE() as varchar(255)) + '| ' +
					'errSeverity : ' + cast(ERROR_SEVERITY() as varchar(255)) + '| '
			   )
	
	end CATCH
-- ======================================================================================================== --
	
	end -- IfCartaPorte
-- ======================================================================================================== --

	
-- =========================================================================== --
-- Cartas Porte trigger
-- =========================================================================== --

-- Next issue	
--	begin TRY
--		begin transaction other
--	
--		commit transaction other
--	end TRY
--	begin CATCH
--
--		    if (XACT_STATE()) = -1  
--	    begin  
--	        
--	       	insert into galdb.dbo.api_ws_historico_logs (message)
--				values ('The transaction is in an uncommittable state.: Rolling back transaction.')
--			rollback transaction other
--	    end  
--	    -- Test whether the transaction is committable.  
--	    if (XACT_STATE()) = 1  
--	    begin  
--	       	insert into galdb.dbo.api_ws_historico_logs (message)
--				values ('The transaction is committable.: Committing transaction.')  
--	        commit transaction other
--	    end   
--    
--		insert into galdb.dbo.api_ws_historico_logs (message)
--		values (	
--					'errNum : ' + cast(ERROR_NUMBER() as varchar(255)) + '| ' +
--					'errLine : ' + cast(ERROR_LINE() as varchar(500)) + '| ' +
--					'errMsg : ' + cast(ERROR_MESSAGE() as varchar(500)) + '| ' +
--					'errState : ' + cast(ERROR_STATE() as varchar(255)) + '| ' +
--					'errSeverity : ' + cast(ERROR_SEVERITY() as varchar(255)) + '| '
--			   )
--	
--	end CATCH
-- ======================================================================================================== --

end --Trigger
	


-- == TEST == --
--select * from galdb.dbo.api_ws_historico_logs

--SELECT  
--   CONNECTIONPROPERTY('net_transport') AS net_transport,
--   CONNECTIONPROPERTY('protocol_type') AS protocol_type,
--   CONNECTIONPROPERTY('auth_scheme') AS auth_scheme,
--   CONNECTIONPROPERTY('local_net_address') AS local_net_address,
--   CONNECTIONPROPERTY('local_tcp_port') AS local_tcp_port,
--   CONNECTIONPROPERTY('client_net_address') AS client_net_address 

