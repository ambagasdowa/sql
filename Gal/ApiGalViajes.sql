
-- mariadb
-- show databases;
-- drop database `gal`
-- create database `gal`
-- grant usage on gal.* to gal@localhost identified by '@gal#';
-- grant select, insert, update, delete, drop, alter, create , create temporary tables on gal.* to gal@localhost;
-- grant file on *.* to 'gal'@'localhost';
-- flush privileges;
-- SHOW GRANTS FOR 'gal'@'localhost';
-- GRANT ALL PRIVILEGES ON *.* TO gal@'localhost' -- with grant options;
-- flush privileges;



use [galdb];
-- go
-- test tables 

-- select * from sistemas.dbo.addenum_tbl_albaran_relations

select * from galdb.dbo.sie_ws_gal_viajes_unigis

insert into galdb.dbo.sie_ws_gal_viajes_unigis (company,id_area,no_viaje,status_description,created,status)
	values (132,2333,101,'description.....status',current_timestamp,1)

--main ws table 

IF OBJECT_ID('galdb.dbo.sie_ws_gal_viajes_unigis', 'U') IS NOT NULL 
  DROP TABLE galdb.dbo.sie_ws_gal_viajes_unigis
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on
-- go
create table dbo.sie_ws_gal_viajes_unigis(
				 id					 				int 			identity(1,1)  	-- add identity
				,company							int				null			-- start Table
				,id_area							int				null
				,no_viaje							int				null
				,status_description					text			null   	-- some notes
				,created			 				datetime		null   	-- created date
				,modified			 				datetime		null  	-- if modified
				,status				 				tinyint default 1 null 	-- set active or inactive
) on [primary]
-- go
set ansi_padding off







-- truncate table galdb.dbo.api_gal_viajes 

	select * from galdb.dbo.api_gal_viajes order by id
	select * from galdb.dbo.api_gal_viajes_logs

select * from galdb.dbo.desp_asignacion order by id_asignacion desc --118544


select max(no_viaje) from galdb.dbo.trafico_viaje


select no_viaje from galdb.dbo.trafico_viaje

use [galdb]

IF OBJECT_ID('galdb.dbo.api_gal_viajes', 'U') IS NOT NULL 
  DROP TABLE galdb.dbo.api_gal_viajes
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on
-- go
create table galdb.dbo.api_gal_viajes(
		 id bigint identity(1,1) not null	
 		,sie_ctrl int	null
		,api_ctrl int	null 
		,api_id_ruta int	null 
		,asignacion_num_asignacion varchar(20)	null
		,asignacion_id_configuracionviaje int	null
		,api_id_personal int	null
		,api_id_unidad varchar(10)	null
		,api_id_remolque1 varchar(20)	null
		,api_id_remolque2 varchar(20)	null
		,api_id_dolly varchar(20)	null
		,api_no_viaje int	null
		,api_fecha_ingreso datetime	null
		,api_f_prog_ini_viaje datetime	null
		,api_f_prog_fin_viaje datetime	null
		,asignacion_id_seguimiento int	null
		,asignacion_kms_ruta int 	null
		,api_id_remitente int	null
		,api_id_destinatario int	null
		,viaje_kms_camion_vacio int	null
		,viaje_kms_camion_lleno int	null
		,viaje_kms_operador int	null
		,api_id_origen int	null
		,api_id_destino int 	null
		,pedido_id_producto int	null
		,pedido_f_pedido datetime	null
		,pedido_id_cliente int	null
) on [primary]
-- go
set ansi_padding off


-- create an x-force trigger 

--build log table for control 

use galdb

select * from galdb.dbo.api_gal_viajes_logs

select * from 
	--truncate table 
galdb.dbo.api_ws_historico_logs

IF OBJECT_ID('galdb.dbo.api_gal_viajes_logs', 'U') IS NOT NULL 
  DROP TABLE galdb.dbo.api_gal_viajes_logs
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on
-- go
create table "api_gal_viajes_logs" (
						 id			bigint 			identity(1,1)  	-- add identity
						,sie_ctrl	int	null	--Identificador del Evento:  • 0
						,api_ctrl	int	null	--Identificador del Evento:  • 0
--						id	int	null	--Identificador consecutivo de la tabla.
						,asignacion_id_asignacion	int	null	--Identificador consecutivo autogenerado en LIS
						,asignacion_num_asignacion	varchar(20)	null --	Identificador consecutivo autogenerado en LIS
						,api_id_area	int	null --Identificador del Evento: • 1 | Área definida en LIS 
						,api_id_ruta	int	null --	Identificador de ruta recorrida (pendiente definición)
						,asignacion_id_configuracionviaje	int	null --	Identificador del Evento: • 1 | Unidad Motriz • 2 | Sencillo • 3 | Full
						,asignacion_id_operador2	int	null --	Identificador del Evento: • NULL
						,api_id_personal	int	null --	Numero de empleado LIS, que realizo el viaje.
						,api_id_unidad	varchar(10)	null --	Identificador de unidad LIS, que realizo el viaje.
						,api_id_remolque1	varchar(20)	null --	Identificador de remolque 1 LIS, que realizo el viaje.
						,api_id_remolque2	varchar(20)	null --	Identificador de remolque 2 LIS, que realizo el viaje.
						,api_id_dolly	varchar(10)	null --	Identificador de dolly LIS, que realizo el viaje.
						,api_id_ingreso	varchar(8)	null --	Identificador del Evento (se creará el usuario):  • UNIGIS
						,api_no_viaje	int	null --	Identificador consecutivo autogenerado en LIS
						,api_fecha_ingreso	datetime	null --	Fecha y hora de registro del viaje:  • YYYY-MM-DD HH:MM:SS 
						,api_f_prog_ini_viaje	datetime	null --	Fecha y hora de registro del viaje:  • YYYY-MM-DD HH:MM:SS 
						,api_f_prog_fin_viaje	datetime	null --	Fecha y hora de registro del viaje:  • YYYY-MM-DD HH:MM:SS 
						,asignacion_status_asignacion	int	null --	Identificador del Evento:  • 3 | Viaje terminado
						,asignacion_seguimiento_actual	int	null --	Identificador del Evento:  • 0
						,asignacion_observaciones	varchar(255)	null --	Identificador del Evento: • NULL
						,asignacion_id_flota	int	null --	Identificador del Evento:  • 1 | Local • 2 | Foráneo
						,asignacion_id_seguimiento	int	null --	Identificador del Evento:  • 1 | Cargado • 2 | Vacío
						,asignacion_no_kit	int	null --	Identificador del Evento: • NULL
						,api_id_tipo_operacion	int	null --	Identificador del Evento:  • 0
						,asignacion_kms_ruta	int	null --	Registro de kilómetros de la ruta-viaje.
						,asignacion_tiempo_ruta	int	null --	Identificador del Evento: • NULL
						,asignacion_sist_origen	varchar(4)	null --	Identificador del Evento:  • ZAM
						,api_id_remitente	int	null --	Numero de cliente LIS.
						,api_id_destinatario	int	null --	Numero de cliente LIS.
						,viaje_kms_camion_vacio	int	null --	Registro de kilómetros de la ruta-viaje en caso de que sea cargado, sino 0
						,viaje_kms_camion_lleno	int	null --	Registro de kilómetros de la ruta-viaje en caso de que sea vacio, sino 0
						,viaje_kms_operador	int	null --	Registro de kilómetros de la ruta-viaje.
						,viaje_lts_viaje	decimal(18, 6)	null --	Identificador del Evento:  • 0.0000
						,viaje_rendimiento_viaje	decimal(18, 6)	null --	Identificador del Evento:  • 0.0000
						,viaje_rendimiento_real	decimal(18, 6)	null --	Identificador del Evento:  • 0.0000
						,viaje_status_viaje	char(1)	null --	Identificador del Evento:  • C
						,viaje_sueldo_operador	decimal(18, 6)	null --	Identificador del Evento:  • 0.0000
						,viaje_compensaciones	decimal(18, 6)	null --	Identificador del Evento:  • 0.0000
						,viaje_lts_empresa	decimal(18, 6)	null --	Identificador del Evento:  • 0.0000
						,viaje_rend_esperado	decimal(18, 6)	null --	Identificador del Evento:  • 0.0000
						,viaje_status_desp	char(1)	null --	Identificador del Evento:  • T
						,viaje_viajeactual	char(1)	null --	Identificador del Evento: • N
						,viaje_status_cs	varchar(1)	null --	Identificador del Evento:  • E
						,api_id_origen	int	null --	 Numero de plaza LIS (pendiente definición).
						,api_id_destino	int	null --	 Numero de plaza LIS (pendiente definición).
						,api_id_linearem1	varchar(10)	null --	Identificador del Evento:  • ZAM
						,api_id_linearem2	varchar(10)	null --	Identificador del Evento:  • ZAM
						,viaje_id_statusviaje	int	null --	Identificador del Evento:  • 6
						,api_id_contrato	int	null --	Identificador del Evento:  • 0
						,viaje_observacionesviaje	varchar(255)	null --	Identificador del Evento: • NULL
						,viaje_id_tipo_servicio	int	null --	Identificador del Evento:  • 0
						
					    ,viaje_id_tramo		int null --	Identificador del Evento:  • 0
					    ,viaje_consecutivo_viaje decimal(18,6) null --Identificador del Evento:  • 1.0000
					    ,viaje_status_km	varchar(10) null --Identificador del Evento:  • ''
					    ,viaje_direccion_tramo int null --Identificador del Evento:  • 1
				    	,viaje_ramal	int null --Identificador del Evento:  • 0
				    	,viaje_desc_tramo varchar(255) null --descripcion de la ruta 
				    	,viaje_kms_tramo	int null --Registro de kilometros de la ruta-viaje
				    	,viaje_vol_recuperado	varchar(255) null --Identificador del Evento:  • NULL
					    
						,pedido_id_pedido	int	null --	Identificador consecutivo autogenerado en LIS
						,pedido_status_pedido	int	null --	Identificador del Evento:  • 3
						,pedido_id_producto	int	null --	Identificador del Evento:  • 1
						,pedido_tipo_serv	char(1)	null --	Identificador del Evento:  • D
						,pedido_f_pedido	datetime	null --	Fecha y hora de registro del pedido:  • YYYY-MM-DD HH:MM:SS 
						,pedido_no_remision	varchar(30)	null --	Identificador del Evento: • NULL
						,pedido_id_cliente	int	null --	Numero de cliente LIS.
						,pedido_observaciones_pedido	varchar(50)	null --	Identificador del Evento: • NULL
						,pedido_id_convenio	int	null --	Identificador del Evento:  • 0
						,pedido_fecha_modifico	datetime	null --	Fecha y hora de registro del pedido:  • YYYY-MM-DD HH:MM:SS 
						,pedido_id_modifico	varchar(8)	null --	Identificador del Evento (se creará el usuario):  • UNIGIS
						,pedido_id_areaconvenio	int	null --	Identificador del Evento:  • 0
						,pedido_no_guia	int	null --	Identificador del Evento:  • 0
						,pedido_f_real_entrega	datetime	null --	Fecha y hora de registro del pedido:  • YYYY-MM-DD HH:MM:SS 
						,pedido_sist_origen	varchar(4)	null --	Identificador del Evento:  • ZAM
						,pedido_id_pedido_maestro	int	null --	Identificador del Evento:  • 0
						,pedido_id_area_guia	int	null --	Identificador del Evento:  • 1
						,pedido_id_tipo_cancelacion	int	null --	Identificador del Evento:  • 0
						,pedido_num_pedido	varchar(25)	null --	Identificador consecutivo autogenerado en LIS
						,pedido_por_cargar	int	null --	Identificador del Evento:  • 1
						,pedido_cargado	int	null --	Identificador del Evento:  • 0
						,pedido_pedido_creado	int	null --	Identificador del Evento:  • 0
						,pedido_id_solicitud	int	null --	I///dentificador del Evento:  • 0
						,pedido_tarifa	decimal(18, 6)	null --	Identificador del Evento:  • 0
						,pedido_id_autorizo	varchar(8)	null --	Identificador del Evento (se creará el usuario):  • UNIGIS
						,pedido_f_desp_prog	datetime	null --	Fecha y hora de registro del pedido:  • YYYY-MM-DD HH:MM:SS 
						,pedido_id_tipo_unidad	int	null --	Identificador del Evento:  • 0
						,pedido_id_clienteFinal	int	null --	Identificador del Evento:  • 0
						,pedido_id_pedidopk	int	null --	Identificador consecutivo autogenerado en LIS
						,created			 				datetime		null   	-- created date
						,modified			 				datetime		null  	-- if modified
						,status				 				tinyint default 1 null 	-- set active or inactive
) on [primary]
-- go
set ansi_padding off


--Define logs

use [galdb]

select * from galdb.dbo.api_gal_viajes_logs

select * from galdb.dbo.api_ws_historico_logs

-- go
IF OBJECT_ID('dbo.api_ws_historico_logs', 'U') IS NOT NULL 
  DROP TABLE dbo.api_ws_historico_logs
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on
-- go
create table dbo.api_ws_historico_logs(
		 id					 bigint					identity(1,1)
		,message		     text			    	null
		,created			 datetime 				default current_timestamp null
		,modified			 datetime				null
		,status				 tinyint 				default 1 null
) on [primary]
-- go
set ansi_padding off


--start trigger




--Instrucción INSERT en conflicto con la restricción FOREIGN KEY "FK_desp_pedido_18net". 
--El conflicto ha aparecido en la base de datos "galdb", tabla "dbo.trafico_convenio".

--
--Instrucción INSERT en conflicto con la restricción FOREIGN KEY "FK_desp_pedido08". 
--	El conflicto ha aparecido en la base de datos "galdb", tabla "dbo.mtto_unidades"
--, column 'id_unidad'.
--
--
--Instrucción INSERT en conflicto con la restricción FOREIGN KEY "FK_desp_asignaciondet02". 
--El conflicto ha aparecido en la base de datos "galdb", tabla "dbo.desp_pedido", column 'id_pedidopk'.

--
--Infracción de la restricción PRIMARY KEY 'XPKdesp_asignaciondet'. 
--No se puede insertar una clave duplicada en el objeto 'dbo.desp_asignaciondet'. 
--El valor de la clave duplicada es (118543, 1).


No se puede insertar el valor NULL en la columna 'id_asignacion'
, tabla 'galdb.dbo.desp_asignacion'. La columna no admite valores NULL. Error de INSERT.

Infracción de la restricción PRIMARY KEY 'XPKdesp_asignaciondet'.
No se puede insertar una clave duplicada en el objeto 'dbo.desp_asignaciondet'
. El valor de la clave duplicada es (118543, 1).


Infracción de la restricción PRIMARY KEY 'XPKdesp_asignaciondet'. 
No se puede insertar una clave duplicada en el objeto 'dbo.desp_asignaciondet'. 
El valor de la clave duplicada es (118543, 1).

Infracción de la restricción PRIMARY KEY 'XPKdesp_asignaciondet'. 
No se puede insertar una clave duplicada en el objeto 'dbo.desp_asignaciondet'. 
El valor de la clave duplicada es (118543, 2).

ip gal server 10.44.45.10



select * from galdb.dbo.desp_asignacion order by id_asignacion DESC

select max(consecutivo) from galdb.dbo.desp_asignaciondet

select * from galdb.dbo.desp_asignaciondet group by consecutivo

select count(consecutivo) as 'count' from galdb.dbo.desp_asignaciondet 


select * from galdb.dbo.desp_asignaciondet where id_asignacion = 118543 or id_pedidopk = 118542

select max(id_asignacion) from galdb.dbo.desp_asignaciondet


select * from galdb.dbo.desp_asignaciondet where id_asignacion in (118543,118542) 



				select 
								 id_area	 --Identificador del Evento: • 1 | Área definida en LIS 
								,id_ruta	 --	Identificador de ruta recorrida (pendiente definición)
								,id_configuracionviaje	 --	Identificador del Evento: • 1 | Unidad Motriz • 2 | Sencillo • 3 | Full
								,id_personal	 --	Numero de empleado LIS, que realizo el viaje.
								,id_unidad	 --	Identificador de unidad LIS, que realizo el viaje.
								,id_remolque1	 --	Identificador de remolque 1 LIS, que realizo el viaje.
								,id_remolque2	 --	Identificador de remolque 2 LIS, que realizo el viaje.
								,id_dolly	 --	Identificador de dolly LIS, que realizo el viaje.
								,id_ingreso	 --	Identificador del Evento (se creará el usuario):  • UNIGIS
								,no_viaje	 --	Identificador consecutivo autogenerado en LIS
								,fecha_ingreso	 --	Fecha y hora de registro del viaje:  • YYYY-MM-DD HH:MM:SS 
								,f_prog_ini_viaje	 --	Fecha y hora de registro del viaje:  • YYYY-MM-DD HH:MM:SS 
								,f_prog_fin_viaje	 --	Fecha y hora de registro del viaje:  • YYYY-MM-DD HH:MM:SS 
								,kms_camion_vacio	 --	Registro de kilómetros de la ruta-viaje en caso de que sea cargado, sino 0
								,kms_camion_lleno	 --	Registro de kilómetros de la ruta-viaje en caso de que sea vacio, sino 0
								,kms_operador	 --	Registro de kilómetros de la ruta-viaje.
								,lts_viaje	 --	Identificador del Evento:  • 0.0000
								,rendimiento_viaje	 --	Identificador del Evento:  • 0.0000
								,rendimiento_real	 --	Identificador del Evento:  • 0.0000
								,status_viaje	 --	Identificador del Evento:  • C
								,sueldo_operador	 --	Identificador del Evento:  • 0.0000
								,compensaciones	 --	Identificador del Evento:  • 0.0000
								,lts_empresa	 --	Identificador del Evento:  • 0.0000
								,rend_esperado	 --	Identificador del Evento:  • 0.0000
								,status_desp	 --	Identificador del Evento:  • T
								,viajeactual	 --	Identificador del Evento: • N
								,status_cs	 --	Identificador del Evento:  • E
								,id_origen	 --	 Numero de plaza LIS (pendiente definición).
								,id_destino	 --	 Numero de plaza LIS (pendiente definición).
								,id_linearem1	 --	Identificador del Evento:  • ZAM
								,id_linearem2	 --	Identificador del Evento:  • ZAM
								,id_statusviaje	 --	Identificador del Evento:  • 6
								,id_contrato	 --	Identificador del Evento:  • 0
								,id_tipo_servicio	 --	Identificador del Evento:  • 0
				from galdb.dbo.trafico_viaje
			





select max(id_pedidopk) from galdb.dbo.desp_pedido

	select * from galdb.dbo.desp_pedido where id_pedidopk = 118545

	select * from galdb.dbo.desp_asignacion where id_asignacion = 118543

	select max(id_asignacion) from galdb.dbo.desp_asignacion

select *  
--	delete 
from galdb.dbo.desp_asignaciondet where id_asignacion = 118542



					SET IDENTITY_INSERT desp_asignaciondet OFF
					
					insert into galdb.dbo.desp_asignaciondet
					(
						 id_asignacion
						,consecutivo
						,id_area_pedido
						,id_pedido
						,id_pedidopk
					)
					values(
							 118543
							,1 --as 'consecutivo'
							,1 --as 'id_area_pedido'
							,117574--117585 --as 'id_pedido'
							,117583--117585 --as 'id_pedidopk'
						)
					
					SET IDENTITY_INSERT desp_asignaciondet ON
					

-- select * from galdb.dbo.trafico_convenio
-- select * from galdb.dbo.desp_pedido
select * from galdb.dbo.mtto_unidades

select id_linearem1 from galdb.dbo.trafico_viaje

select id_linearem1,* from galdb.dbo.trafico_viaje

--select * from galdb.dbo.api_gal_viajes_logs


			
use [galdb]



IF OBJECT_ID('dbo.api_test_trips', 'U') IS NOT NULL 
  DROP TABLE dbo.api_test_trips
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on	
create table dbo.api_test_trips ( 
                 id_area                  int null
                ,id_ruta                  int null
                ,id_configuracionviaje    int null
                ,id_personal              int null
                ,id_unidad                varchar(10) null
                ,id_remolque1             varchar(20) null
                ,id_remolque2             varchar(20) null
                ,id_dolly                 varchar(10) null
                ,id_ingreso               varchar(8) null
                ,no_viaje                 int null
                ,fecha_ingreso            datetime null
                ,f_prog_ini_viaje         datetime null
                ,f_prog_fin_viaje         datetime null
                ,kms_camion_vacio         int null
                ,kms_camion_lleno         int null
                ,kms_operador             int null
                ,lts_viaje                decimal(18,6) null
                ,rendimiento_viaje        decimal(18,6) null
                ,rendimiento_real         decimal(18,6) null
                ,status_viaje             char(1) null
                ,sueldo_operador          decimal(18,6) null
                ,compensaciones           decimal(18,6) null
                ,lts_empresa              decimal(18,6) null
                ,rend_esperado            decimal(18,6) null
                ,status_desp              char(1) null
                ,viajeactual              char(1) null
                ,status_cs                char(1) null
                ,id_origen                int null 
                ,id_destino               int null
                ,id_linearem1             varchar(10) null
                ,id_linearem2             varchar(10) null
                ,id_statusviaje           int null
                ,id_contrato              int null
                ,id_tipo_servicio         int null
)on [primary]
-- go`
set ansi_padding off
			


 ---   TESTING WS ---
 
select * from galdb.dbo.api_gal_viajes

select max(no_viaje) from galdb.dbo.trafico_viaje


 	insert into galdb.dbo.api_gal_viajes (
		 sie_ctrl
		,api_ctrl
		,api_id_ruta
		,asignacion_num_asignacion
		,asignacion_id_configuracionviaje
		,api_id_personal
		,api_id_unidad
		,api_id_remolque1
		,api_id_remolque2
		,api_id_dolly
		,api_no_viaje
		,api_fecha_ingreso
		,api_f_prog_ini_viaje
		,api_f_prog_fin_viaje
		,asignacion_id_seguimiento
		,asignacion_kms_ruta
		,api_id_remitente
		,api_id_destinatario
		,viaje_kms_camion_vacio
		,viaje_kms_camion_lleno
		,viaje_kms_operador
		,api_id_origen
		,api_id_destino
		,pedido_id_producto
		,pedido_f_pedido
		,pedido_id_cliente
	)
		values(
		 1
		,1123
		,51
		,1
		,1
		,92
		,'G-169'
		,'RV3560'
		,'RV6456'
		,1
		,117378
		,'2019-09-15T22:10:15'
		,'2019-09-15T22:10:15'
		,'2019-09-15T22:10:15'
		,1
		,1
		,1
		,1
		,1
		,1
		,1
		,1
		,1
		,1
		,'2019-09-15T22:10:15'
		,0
)

-- ANTICIPO

 	insert into galdb.dbo.api_gal_viajes (
		 sie_ctrl
		,api_ctrl
		,anticipo_fecha_anticipo
		,anticipo_monto_anticipo
		,anticipo_monto_anticipo_iva
		,anticipo_fecha_ingreso
		,anticipo_observaciones
		,api_id_personal
	)
		values(
		 2 --anticipo
		,10002
		,'2019-09-15T22:10:15'
		,121355.00
		,125.00
		,'2019-09-15T22:10:15'
		,'Anticipo INsert SqlClientTEST'
		,'92'
)


--anticipo
select 		 
		 sie_ctrl
		,api_ctrl
		,anticipo_fecha_anticipo
		,anticipo_monto_anticipo
		,anticipo_monto_anticipo_iva
		,anticipo_fecha_ingreso
		,anticipo_observaciones
		,api_id_personal 
from galdb.dbo.api_gal_viajes



-- CP

 	insert into galdb.dbo.api_gal_viajes ( 
	 sie_ctrl
 	,api_ctrl
	,pedido_id_cliente
	,api_id_personal
	,api_id_remitente
	,api_id_destinatario
	,asignacion_kms_ruta
	,api_id_origen
	,api_id_destino
	,guia_num_guia
	,guia_fecha_guia
	,guia_flete
	,guia_otro
	,guia_subtotal
	,guia_iva_guia
	,guia_monto_retencion
	,guia_id_serieguia
	,guia_id_iva
	,guia_id_retencion
	,guia_id_metodo_pago
	,guia_no_cuenta
	,renglon_id_producto
	,renglon_id_fraccion
	,otro_monto_iva_otro
	,otro_desc_otro
	,otro_id_otro
	) values (
	 3
	,10002
	,0
	,92
	,1
	,1
	,300
	,149
	,105
	,'GAL-01000'
	,'2019-09-15T22:10:15'
	,1.00
	,1.00
	,1.00
	,1.00
	,1.00
	,1
	,1
	,1
	,1
	,0.00
	,1
	,1
	,1.00
	,'Nombre'
	,1
	)
	


select 
		 sie_ctrl
		,api_ctrl
		,guia_num_guia
		,guia_fecha_guia
		,guia_flete
		,guia_otro
		,guia_subtotal
		,guia_iva_guia
		,guia_monto_retencion
		,guia_id_serieguia
		,guia_id_iva
		,guia_id_retencion
		,guia_id_metodo_pago
		,guia_no_cuenta
		,renglon_id_producto
		,renglon_id_fraccion
		,otro_monto_iva_otro
		,otro_desc_otro
		,otro_id_otro
from galdb.dbo.api_gal_viajes

select * from galdb.dbo.trafico_guia



select * from galdb.dbo.api_ws_historico_logs
----
select * from galdb.dbo.api_gal_viajes_logs order by id desc




select * from galdb.dbo.api_gal_viajes where api_ctrl = 10003

select * from galdb.dbo.api_gal_viajes_logs where api_ctrl = 10002
	
select api_id_personal,anticipo_fecha_anticipo,* from galdb.dbo.api_gal_viajes 

select id_origen,id_destino,* from galdb.dbo.trafico_guia

select id_origen,id_destino from galdb.dbo.trafico_viaje

select id_plaza,desc_plaza from galdb.dbo.trafico_plaza

select max(no_guia) from galdb.dbo.trafico_renglon_guia
union all
select max(no_guia) from galdb.dbo.trafico_guia
union all
select max(no_guia) from galdb.dbo.desp_pedido


select * from galdb.dbo.trafico_producto


select no_guia,* from galdb.dbo.trafico_guia where no_guia = 138363

select no_guia,* from galdb.dbo.trafico_renglon_guia where no_guia = 138363

select no_guia,* from galdb.dbo.desp_pedido where no_guia = 138363


select fecha_anticipo,* from galdb.dbo.trafico_anticipo where no_anticipo = 50783

select * from galdb.dbo.trafico_renglon_anticipo where no_anticipo = 50783




SELECT  
   CONNECTIONPROPERTY('net_transport') AS net_transport,
   CONNECTIONPROPERTY('protocol_type') AS protocol_type,
   CONNECTIONPROPERTY('auth_scheme') AS auth_scheme,
   CONNECTIONPROPERTY('local_net_address') AS local_net_address,
   CONNECTIONPROPERTY('local_tcp_port') AS local_tcp_port,
   CONNECTIONPROPERTY('client_net_address') AS client_net_address 




select * from galdb.dbo.desp_pedido where id_pedidopk = 117657

select * from galdb.dbo.desp_pedido_detalle where id_pedidopk = 117657

select * from galdb.dbo.desp_asignacion where id_asignacion = 118552

select max(no_viaje) from galdb.dbo.trafico_viaje 
--
select 
--		id_asignacion
		*
--		max(id_asignacion) 
from galdb.dbo.desp_asignaciondet
where id_asignacion = 118552


--select * from galdb.dbo.api_test_trips where no_viaje = 117366

select 
		no_viaje
		,*
--		max(no_viaje)				
from galdb.dbo.trafico_viaje 
where no_viaje = 117372

select 
		 no_viaje
		,*
--		max(no_viaje) 
from galdb.dbo.trafico_renglon_viaje
where no_viaje = 117372
		
-- ================================================================= --
--	select * from sistemas.dbo.ws_tbl_cls

-- ================================================================= --



--select max(id_asignacion) from galdb.dbo.desp_asignacion


INSERT INTO 
desp_asignacion(
				 id_asignacion
				,num_asignacion
			    ,id_area
		    	,id_ruta
		    	,id_configuracionviaje
		    	,id_operador2 --
		    	,id_personal
		    	,id_unidad
		    	,id_remolque1
		    	,id_remolque2
		    	,id_dolly
		    	,id_linearem1
		    	,id_linearem2
		    	,id_ingreso
		    	,id_area_viaje
		    	,no_viaje
				,fecha_ingreso
				,f_prog_ini_viaje
				,f_prog_fin_viaje
				,status_asignacion
				,seguimiento_actual
				,observaciones
				,id_flota
				,id_seguimiento
				,no_kit
				,id_tipo_operacion
				,kms_ruta
				,kms_ruta1
				,tiempo_ruta
				,sist_origen
				,capacidad_cargam3
				,capacidad_cargakg
				,id_remitente
				,id_destinatario
				,id_ventana_chrysler
				,id_clasificacionmov 
			) 
VALUES 
			(
				118544
				,'0118544'
				,1
				,2
				,2
				,0
				,1175
				,'G-179'
				,'TGV-92'
				,''
				,''
				,''
				,''
				,'sa'
				,0
				,0
				,'2019-09-09 23:10:06'
				,'2019-09-09 00:00:00'
				,'2019-09-09 00:00:00'
				,1
				,1
				,'Prueba de viaje 2'
				,1
				,2
				,null
				,0
				,180
				,180
				,2
				,'ZAM'
				,0
				,0
				,0
				,0
				,0
				,null
			)


INSERT INTO 
trafico_viaje  (
				 id_area
				,no_viaje
				,id_unidad
				,id_personal
				,kms_camion_vacio
				,kms_camion_lleno
				,kms_operador
				,lts_viaje
				,rendimiento_viaje
				,rendimiento_real
				,status_viaje
				,id_ruta
				,sueldo_operador
				,compensaciones
				,lts_empresa
				,id_remolque1
				,id_remolque2
				,rend_esperado
				,id_dolly
				,id_ingreso
				,fecha_ingreso
				,status_desp
				,f_prog_ini_viaje
				,f_prog_fin_viaje
				,viajeactual
				,status_cs
				,id_origen
				,id_destino
				,id_linearem1
				,id_linearem2
				,id_statusviaje
				,id_contrato
				,observacionesviaje
				,id_tipo_servicio 
			)  
VALUES 
			(
			 1
		 	,117367
		 	,'G-179'
		 	,1175
		 	,379
		 	,0
		 	,379
		 	,'0.000000'
		 	,'0.000000'
		 	,'0.000000'
		 	,'A'
		 	,2
		 	,'0.000000'
		 	,'0.000000'
		 	,'0.000000'
		 	,'TGV-92'
		 	,null
		 	,'0.000000'
		 	,null
		 	,'sa'
		 	,'2019-09-10 00:18:41'
		 	,'T'
		 	,'2019-09-09 00:00:00'
		 	,'2019-09-09 00:00:00'
		 	,'S'
		 	,'E'
		 	,1
		 	,6
		 	,null
		 	,null
		 	,1
		 	,0
		 	,'Prueba viaje 2'
		 	,0
		 	)

		 
INSERT INTO 
			trafico_renglon_viaje (
									 id_area
									,no_viaje
									,no_guia
									,id_tramo
									,consecutivo_viaje
									,status_km
									,direccion_tramo
									,ramal
									,desc_tramo
									,kms_tramo
									,vol_recuperado
								 ) 
VALUES 
								(
									 1
									,117367
									,0
									,0
									,1
									,'0'
									,1
									,0
									,'001 AMARIS-CANCUN FS'
									,379
									,null
								)

update desp_asignacion
	set status_asignacion= 2, seguimiento_actual= 1
where id_asignacion=118544
	and num_asignacion='0118544'


update mtto_unidades
	set id_status = 1
where id_unidad = 'g-179'





--Balanza 
--cuenta,nombre_cta,entidad,nombre_entidad -- balanza comprobacion
--