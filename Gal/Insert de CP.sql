----Cartas Porte

INSERT INTO
            trafico_guia (
                           id_area
                          ,no_guia
                          ,tipo_pago
                          ,status_guia
                          ,tipo_cobro
                          ,clasificacion_doc
                          ,fecha_guia
                          ,fecha_ingreso
                          ,conv_operador
                          ,conv_permisionario
                          ,id_cliente
                          ,id_personal
                          ,no_viaje
                          ,id_remitente
                          ,id_destinatario
                          ,id_condujo
                          ,id_operador2
                          ,kms_guia
                          ,flete
                          ,seguro
                          ,maniobras
                          ,autopistas
                          ,otros
                          ,subtotal
                          ,iva_guia
                          ,monto_tipo_cambio
                          ,tipo_doc
                          ,id_origen
                          ,id_destino
                          ,id_fraccion
                          ,plaza_emision
                          ,conducir_de
                          ,conducir_a
                          ,id_tipo_moneda
                          ,num_guia
                          ,valor_declarado
                          ,prestamo
                          ,id_ingreso
                          ,monto_ncredito
                          ,monto_ncargo
                          ,monto_pago
                          ,factor_iva
                          ,cant_movguia
                          ,id_serieguia
                          ,monto_ivancredito
                          ,monto_ivancargo
                          ,monto_comisiontercero
                          ,tipo_origen
                          ,monto_retencion
                          ,monto_retenciontercero
                          ,monto_ivaflete
                          ,id_unidad
                          ,unidadtipo
                          ,unidadplaca
                          ,id_iva
                          ,id_retencion
                          ,status_pago
                          ,control_pago
                          ,cobro_viaje_kms
                          ,observaciones_guia
                          ,id_convenio
                          ,campo8
                          ,campo9
                          ,id_modifico
                          ,fecha_modifico
                          ,id_serieguia2
                          ,id_tipo_operacion
                          ,id_area_facturacion
                          ,tipo_detalle
                          ,tipo_serv
                          ,tipo_cpac
                          ,kms_cpac
                          ,factor_cpac
                          ,codigo
                          ,medida
                          ,id_contrato
                          ,numero_contrato
                          ,id_seguro
                          ,id_compania
                        )
VALUES
        (
              1,138364,'R','C','V',1,'2019-08-30 00:00:00','2019-11-02 12:26:13'
             ,0,0,1,0,0,1,1733,0,0,0,16497.32,0,0,0,0,16497.32,2639.57,1,2,54,13,14,54,54,13,1
             ,'GA-000001','0','N','UNIGIS',0,0,0,0.160000,0,1,0,0,0,0,659.89,0,0,6,2
             ,'N','N',16497.32,'OBSERVACIONES',2,'','','UNIGIS','2019-11-02 12:26:13'
             ,0,0,0,1,'D',0,0,0,0,'',0,0
        )



      INSERT INTO
          trafico_guia_detalle
              (
                 no_guia,id_area,grr,pedido_cliente,ref_almacen,ref_destinatario,chk_cita
                ,chk_especial,chk_urgente,chk_recojo,num_interno_tms,tipo_documento
                ,tipo_doc_ref_dest,ubigeo_origen,ubigeo_dest,valor_mercaderia
              )
      VALUES
        (138364,1,'','','','',0,0,0,0,0,'',' ',' ','',0)



      INSERT INTO
          trafico_renglon_guia
          (
               id_area, consecutivo, no_guia, id_producto, id_fraccion, cantidad, embalaje
              ,descripcion_producto, peso, volumen_m3, peso_estimado, importe
              ,importe_reparto, volumen_carga, volumen_descarga, volumen_natural, peso_especifico
              ,temperatura, temperatura_carga, temperatura_descarga,id_unidad_medida, volumen_20
              ,peso_bruto, peso_tara, sku, unidad_medida, linea, clase, subclase, num_correlativo_cte,   volumen_20_descarga
          )
      VALUES
        (1,1,138364,16,0,0,99,'VARIOS',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'','','','','',0,0)
