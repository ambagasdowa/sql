--declare 35461


use sistemas;
-- go
IF OBJECT_ID ('liquidaciones_gst_monto_adicional', 'V') IS NOT NULL		
    DROP VIEW liquidaciones_gst_monto_adicional;

create view liquidaciones_gst_monto_adicional
as
with "liquidacion" as (
	select 
			  "tliq".id_area
			 ,"tliq".no_liquidacion
			 ,"tliq".fecha_liquidacion
			 ,cast("tliq".fecha_ingreso as date) as 'fechaIngreso'
			 ,"tliq".id_personal
			 ,"tliq".id_unidad
			 ,"tliq".fecha_ingreso
			 ,"tliq".monto_sueldo
			 ,"tliq".monto_d_anticipo
			 ,"tliq".monto_gastos
			 ,"tliq".monto_gastos_iva
			 ,isnull ( (
			 	select 
						sum("trl".monto_concepto) as 'monto_adicional'
				from
						gstdb.dbo.trafico_renglon_liquidacion as "trl"
				where 
						"trl".no_liquidacion = "tliq".no_liquidacion
					and
						"trl".id_area = "tliq".id_area
					and 
						"trl".tipo_concepto = 8
					and
						"trl".naturaleza_concepto = 1		
			   ),0) as 'monto_adicional'
	from 
			gstdb.dbo.trafico_liquidacion as "tliq"
	where 
			"tliq".status_liq = 'A' --and cast("tliq".fecha_ingreso as date) >= '2019-01-25'
)
select 
		 "liquida".id_area
		,"liquida".no_liquidacion
		,"liquida".fecha_liquidacion
		,"liquida".fechaIngreso
		,"liquida".monto_sueldo
		,"liquida".id_personal
		,"liquida".id_unidad
		,"liquida".fecha_ingreso
		,( "liquida".monto_sueldo + "liquida".monto_adicional ) as 'sueldoxviaje'
		,"liquida".monto_d_anticipo
		,"liquida".monto_gastos
		,"liquida".monto_gastos_iva
		,"liquida".monto_adicional
		,( ("liquida".monto_gastos + "liquida".monto_gastos_iva ) - "liquida".monto_adicional) as 'gasto'
		,("liquida".monto_d_anticipo - (("liquida".monto_gastos + "liquida".monto_gastos_iva ) - "liquida".monto_adicional)) as 'reemCorrecto'
from 
		"liquidacion" as "liquida"
--where 
--		"liquida".no_liquidacion = 35479


		
select * from sistemas.dbo.liquidaciones_gst_monto_adicional where no_liquidacion = 27278
		
		
		

select 
		*
from 
		gstdb.dbo.trafico_liquidacion where no_liquidacion = 35461 
		
select 
		sum(monto_concepto) as 'monto_adicional'
from
		gstdb.dbo.trafico_renglon_liquidacion 
where 
		no_liquidacion = 35461
	and 
		tipo_concepto = 8
	and
		naturaleza_concepto = 1		