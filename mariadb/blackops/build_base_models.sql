-- ==================================================================================================================== --	
-- =================================     DEVOPS :: Cakephp base dev        ============================================ --
-- ==================================================================================================================== --

/*===============================================================================
 Author         : Jesus Baizabal
 email			: ambagasdowa@gmail.com
 Create date    : May 02, 2018
 Description    : Build a devops base php app
 TODO			: clean
 @Last_patch	: --
 @license       : MIT License (http://www.opensource.org/licenses/mit-license.php)
 Database owner : Jesus Baizabal
 @status        : Stable
 @version		: 0.0.1
 ===============================================================================*/
show databases;
drop database `blackops`
create database `blackops`
grant usage on blackops.* to blackops@localhost identified by '@blackops#';
grant select, insert, update, delete, drop, alter, create , create temporary tables on blackops.* to blackops@localhost;
grant file on *.* to 'blackops'@'localhost';
flush privileges;
SHOW GRANTS FOR 'blackops'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO blackops@'localhost' -- with grant options;
flush privileges;


-- use blackops;
-- show databases;
-- show tables;
-- describe users;
-- select * from blackops.users
-- use cakephp

--    select * from roles
--    select * from users
--    select * from groups
--    select * from aros

-- ==================================================================================================================== --	
-- ===================================      	  table definitions 		  ========================================= --
-- ==================================================================================================================== --

-- Core definitions

create or replace table `dev_users` (
  `id` 						int unsigned not null auto_increment primary key,
  `username`				varchar not null , 
  `email`					varchar not null , 
  `password`				varchar not null , 
  `created`         	   	datetime,
  `modified`           		datetime,
  `status`					bool not null default true
)engine=InnoDB default charset=utf8mb4;


create or replace table `dev_groups` (
  `id` 						int unsigned not null auto_increment primary key,
  `name`					varchar not null , 
  `description`				text,
  `created`         	   	datetime,
  `modified`           		datetime,
  `status`					bool not null default true
)engine=InnoDB default charset=utf8mb4;


create or replace table `dev_usergroups` (
  `id` 						int unsigned not null auto_increment primary key,
  `dev_users_id`			int unsigned null , 
  `dev_groups_id`			int unsigned null , 
  `description`				text,
  `created`         	   	datetime,
  `modified`           		datetime,
  `status`					bool not null default true
)engine=InnoDB default charset=utf8mb4;


create or replace table `dev_cleanpass` (
  `id` 						int unsigned not null auto_increment primary key,
  `dev_users_id`			int unsigned null , 
  `created`         	   	datetime,
  `modified`           		datetime,
  `status`					bool not null default true
)engine=InnoDB default charset=utf8mb4;

create or replace table `dev_logs` (
  `id` 						int unsigned not null auto_increment primary key,
  `name`					text,
  `description`				text,
  `log`						text,
  `created`         	   	datetime,
  `modified`           		datetime,
  `status`					bool not null default true
)engine=InnoDB default charset=utf8mb4;


-- adding roles 

CREATE TABLE roles (
    id CHAR(36) NOT NULL PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    created DATETIME,
    modified DATETIME
);


ALTER TABLE users
    ADD role_id CHAR(36) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL AFTER role,
    ADD INDEX role_id (role_id),
    ADD FOREIGN KEY (role_id) REFERENCES roles(id);

ALTER TABLE users
    DROP role;


-- ==================================================================================================================== --
-- ===================================      	  Apptable definitions 		  ========================================= --
-- ==================================================================================================================== --
-- Per App Definitions




