-- -----------------------------------------------------------------------------
-- -----------------------------------------------------------------------------
--  Author			: Jesus Baizabal
--  email		        : baizabal.jesus@gmail.com
--  Create date			: October 27, 2022
--  Description			: Build tables , procedures and triggers for bms module
--  TODO		        : clean
--  @Last_patch			: --
--  @license			: MIT License (http://www.opensource.org/licenses/mit-license.php)
--  Database owner		: Jesus Baizabal
--  @status			: Stable
--  @version			: 0.0.1
-- Copyright © 2022, UES devops - portalapps.com, All Rights Reserved
-- -----------------------------------------------------------------------------
-- Description:        This tables controls the behaviour of bms modulo describes the book and set's
-- 		       the inputs way for the user and set it's positions in the page
--		       the components manipulates the forms values and css of the inputs   
-- Call by:            [schema.usp_ProcThatCallsThis -- --> undefined]
--                     [BMS Book Manager System]
--                     [Job -- --> undefined]
--                     [PLC/Interface -- --> undefined]
-- Affected table(s):  [schema.TableModifiedByProc1]
--                     [schema.TableModifiedByProc2]
-- Used By:            Functional Area this is use in :
--                     learner/content/content/{book_id}
-- Parameter(s):       @param1 - description and usage
--                     @param2 - description and usage
-- Usage:              EXEC dbo.usp_DoSomeStuff
--                         @param1 = 1,
--                         @param2 = 3,
--                         @param3 = 2
--                     Additional notes or caveats about this object, like where is can and cannot be run, or
--                     gotchas to watch for when using it.
-- INSTALL mysql --user=user --password=pass db_ediq2021 < mariadb/panamericano/bms.sql
-- -----------------------------------------------------------------------------
 select 'Creating the procedures ...';
-- note procedure fro creating the cache table input-usr
-- steps of the procedure 
-- clean the bms_cache_inputs_ctrls
-- insert updated data into
-- release 
-- DELIMITER //
-- TODO Create index for cache tables
CREATE OR REPLACE PROCEDURE bms_proc_build_cache_inp_usr  (
--  OUT param1 CHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_bin'
 )
 BEGIN

if (select count(user_id) from `db_ediq2021`.`bms_cache_books`) > 0 then
 truncate table `db_ediq2021`.`bms_cache_books`;
end if;

select 'Building bms_cache_books ...';

insert into `db_ediq2021`.`bms_cache_books`
     select 
	 null            
	,`book`.`book_id`       
	,`book`.`pages`         
	,`book`.`book_name`     
	,`book`.`is_url`	
	,`usr`.`user_id` 	
	,now()
    from 
	`db_ediq2021`.bms_books as `book`
    cross join 
	`db_ediq2021`.system_users as `usr`
      on 
	`usr`.first_name <> 'DEMO'; 
END;
-- //

