-- -----------------------------------------------------------------------------
-- -----------------------------------------------------------------------------
--  Author						: Jesus Baizabal
--  email		        : baizabal.jesus@gmail.com
--  Create date			: October 27, 2022
--  Description			: Build tables for bms module
--  TODO		          : clean
--  @Last_patch			: --
--  @license					: MIT License (http://www.opensource.org/licenses/mit-license.php)
--  Database owner		: Jesus Baizabal
--  @status					: Stable
--  @version	        : 0.0.1
-- Copyright © 2022, UES devops - portalapps.com, All Rights Reserved
-- -----------------------------------------------------------------------------
-- Description:        Verbose description of what the query does goes here. Be specific and don't be
--                     afraid to say too much. More is better, than less, every single time. Think about
--                     "what, when, where, how and why" when authoring a description.
-- Call by:            [schema.usp_ProcThatCallsThis]
--                     [Application Name]
--                     [Job]
--                     [PLC/Interface]
-- Affected table(s):  [schema.TableModifiedByProc1]
--                     [schema.TableModifiedByProc2]
-- Used By:            Functional Area this is use in, for example, Payroll, Accounting, Finance
-- Parameter(s):       @param1 - description and usage
--                     @param2 - description and usage
-- Usage:              EXEC dbo.usp_DoSomeStuff
--                         @param1 = 1,
--                         @param2 = 3,
--                         @param3 = 2
--                     Additional notes or caveats about this object, like where is can and cannot be run, or
--                     gotchas to watch for when using it.
-- -----------------------------------------------------------------------------
-- NOTE The view 
-- id                   int(10) unsigned  NO    PRI  <null>   auto_increment
-- bms_inputs_ctrls_id  int(10) unsigned  NO         <null>
-- user_id              int(10) unsigned  NO         <null>
-- attribute            varchar(255)      YES        <null>
-- value                text              YES        <null>
-- created              datetime          YES        <null>
-- modified             datetime          YES        <null>
-- status               tinyint(1)        NO         1

 


select
    biv.bms_inputs_ctrls_id
   ,biv.user_id
   ,biv.attribute
   ,biv.value
   ,bmu.value as `answer`
   ,case 
        when biv.value = bmu.value 
            then 1 else 0 
    end as `eval`
from
   `bms_inputs_values` as biv
  left join `bms_inputs_values` as bmu
    on 
        `biv`.`bms_inputs_ctrls_id` = `bmu`.`bms_inputs_ctrls_id`
    and 
        `bmu`.`user_id` = 1702
    where
        biv.user_id <> 1702;




