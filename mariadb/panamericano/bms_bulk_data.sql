-- -----------------------------------------------------------------------------
-- -----------------------------------------------------------------------------
--  Author			: Jesus Baizabal
--  email		        : baizabal.jesus@gmail.com
--  Create date			: October 27, 2022
--  Description			: Build tables , procedures and triggers for bms module
--  TODO		        : clean
--  @Last_patch			: --
--  @license			: MIT License (http://www.opensource.org/licenses/mit-license.php)
--  Database owner		: Jesus Baizabal
--  @status			: Stable
--  @version			: 0.0.1
-- Copyright © 2022, UES devops - portalapps.com, All Rights Reserved
-- -----------------------------------------------------------------------------
-- Description:        This tables controls the behaviour of bms modulo describes the book and set's
-- 		       the inputs way for the user and set it's positions in the page
--		       the components manipulates the forms values and css of the inputs   
-- Call by:            [schema.usp_ProcThatCallsThis -- --> undefined]
--                     [BMS Book Manager System]
--                     [Job -- --> undefined]
--                     [PLC/Interface -- --> undefined]
-- Affected table(s):  [schema.TableModifiedByProc1]
--                     [schema.TableModifiedByProc2]
-- Used By:            Functional Area this is use in :
--                     learner/content/content/{book_id}
-- Parameter(s):       @param1 - description and usage
--                     @param2 - description and usage
-- Usage:              EXEC dbo.usp_DoSomeStuff
--                         @param1 = 1,
--                         @param2 = 3,
--                         @param3 = 2
--                     Additional notes or caveats about this object, like where is can and cannot be run, or
--                     gotchas to watch for when using it.
-- INSTALL mysql --user=user --password=pass db_ediq2021 < mariadb/panamericano/bms.sql
-- -----------------------------------------------------------------------------


-- debug : set the example
select 'Set the example values';
-- note: create the book 

/* insert into */ 
/*     bms_books values */
/*     (null, 1, 8, 'Guia UV', false, now(), now(), true); */

/* select 'create the pages'; */

/* insert into */ 
/*     bms_bookpages values */ 
/*      (null, 1, 1, "", 'https://ediq.mx/public/guias/guia_demo_uv_040321_2/pages/1.jpg', now(), now(), true) */
/*     ,(null, 1, 2, '/src/bms/src', '/book-source/guia/unam/001/pages/14.jpg', now(), now(), true) */
/*     ,(null, 1, 3, '/src/bms/src', '/book-source/guia/unam/001/pages/15.jpg', now(), now(), true) */
/*     ,(null, 1, 4, '/src/bms/src', '/book-source/guia/unam/001/pages/147.jpg', now(), now(), true) */
/*     ,(null, 1, 5, '/src/bms/src', '/book-source/guia/unam/001/pages/35.jpg', now(), now(), true) */
/*     ,(null, 1, 6, '/src/bms/src', '/book-source/guia/unam/001/pages/36.jpg', now(), now(), true) */
/*     ,(null, 1, 7, "", 'https://ediq.mx/public/guias/guia_demo_uv_040321_2/pages/7.jpg', now(), now(), true) */
/*     ,(null, 1, 8, '/src/bms/src', '/book-source/guia/unam/001/pages/180.jpg', now(), now(), true); */

select 'Set the positions';
insert into bms_positions values
(
      null,
      1,
      2,
      2,
      '',
      '',
      '',
      '',
      '',
      '.pages_2>form>#a2{top:54.4%;left:14.4%;width:67%;} .pages_2>form>#b2{top:55.9%;left:20.4%;width:61.3%;} .pages_2>form>#cx{top:57.4%;left:20.4%;width:61.2%;} .pages_2>form>#d2{top:69.5%;left:14.4%;width:67%;} .pages_2>form>#e2{top:71.1%;left:20.4%;width:61.3%;} .pages_2>form>#f2{top:72.5%;left:20.4%;width:61.2%;}.pages_2>form>#g2{top:89.1%;left:14.4%;width:67%;}.pages_2>form>#h2{top:90.6%;left:20.4%;width:61.3%;}.pages_2>form>#i2{top:92.2%;left:20.4%;width:61.2%;}',
      now(),
      now(),
      1
  ),
(
      null,
      1,
      3,
      3,
      '',
      '',
      '',
      '',
      '',
      '.pages_3 > form > #a{top:36%;left:48.7%;width:3%;} .pages_3 > form > #b{top:37.5%;left:48.7%;width:3%;} .pages_3 > form > #c{top:38.9%;left:48.7%;width:3%;} .pages_3 > form > #d{top:40.4%;left:48.7%;width:3%;} .pages_3 > form > #a2{top:44.9%;left:48.7%;width:3%;} .pages_3 > form > #b2{top:46.3%;left:48.7%;width:3%;} .pages_3 > form > #c2{top:47.8%;left:48.7%;width:3%;} .pages_3 > form > #d2{top:49.2%;left:48.7%;width:3%;}',
      now(),
      now(),
      1
  ),
(
      null,
      1,
      4,
      5,
      '',
      '',
      '',
      '',
      '',
      '.pages_5 > form > #a{top:70.2%;left:190px;width:3%;} .pages_5 > form > #b{bottom:17.5%;right:66.5%;width:3%;} .pages_5 > form > #c{top:80.2%;left:43%;width:3%;}',
      now(),
      now(),
      1
  ),
(
      null,
      1,
      5,
      6,
      '',
      '',
      '',
      '',
      '',
      ' .pages_6 > form > #a{top:60.2%;left:190px;width:3%;} .pages_6 > form > #b{bottom:17.5%;right:66.5%;width:3%;} .pages_6 > form > #c{top:80.2%;left:43%;width:3%;}',
      now(),
      now(),
      1
  ),
(
      null,
      1,
      6,
      7,
      '',
      '',
      '',
      '',
      '',
      ' .pages_7 > form > #a{top:90.2%;left:190px;width:3%;} .pages_7 > form > #b{bottom:17.5%;right:66.5%;width:3%;} .pages_7 > form > #c{top:80.2%;left:43%;width:3%;}',
      now(),
      now(),
      1
  ),
(
      null,
      1,
      7,
      8,
      '',
      '',
      '',
      '',
      '',
      '.pages_8 > form > #a{top:90.2%;left:190px;width:3%;} .pages_8 > form > #b{bottom:17.5%;right:66.5%;width:3%;} .pages_8 > form > #c{top:80.2%;left:43%;width:3%;}',
      now(),
      now(),
      1
  ); 

select 'Create a input';

  insert into bms_inputs_ctrls values 
 -- page 2
   (null,1,2,'Primer group of inputs 1 in book_id 1 ',now(),now(),1)   -- 1
  ,(null,1,2,'firts group of inputs 2 in book_id 1 ',now(),now(),1)   -- 2
  ,(null,1,2,'firts group of inputs 3 in book_id 1 ',now(),now(),1)   -- 3
  ,(null,1,2,'firts group of inputs 4 in book_id 1 ',now(),now(),1)   -- 4
  ,(null,1,2,'firts group of inputs 5 in book_id 1 ',now(),now(),1)   -- 5
  ,(null,1,2,'firts group of inputs 6 in book_id 1 ',now(),now(),1)   -- 6
  ,(null,1,2,'firts group of inputs 7 in book_id 1 ',now(),now(),1)   -- 7
  ,(null,1,2,'firts group of inputs 8 in book_id 1 ',now(),now(),1)   -- 8
  ,(null,1,2,'firts group of inputs 9 in book_id 1 ',now(),now(),1)   -- 9
-- page 3
  ,(null,1,3,'second 1 page',now(),now(),1)  -- 10
  ,(null,1,3,'second 2 page',now(),now(),1)  -- 11
  ,(null,1,3,'second 3 page',now(),now(),1)  -- 12
  ,(null,1,3,'second 4 page',now(),now(),1)  -- 13
  ,(null,1,3,'second 5 page',now(),now(),1)  -- 14
  ,(null,1,3,'second 6 page',now(),now(),1)  -- 15
  ,(null,1,3,'second 7 page',now(),now(),1)  -- 16
  ,(null,1,3,'second 8 page',now(),now(),1)  -- 17
-- page 5
  ,(null,1,5,'fifth 1 page',now(),now(),1)  -- 18
  ,(null,1,5,'fifth 2 page',now(),now(),1)  -- 19
  ,(null,1,5,'fifth 3 page',now(),now(),1)  -- 20
-- page 6  
  ,(null,1,6,'sixth 1 page',now(),now(),1)  -- 21
  ,(null,1,6,'sixth 2 page',now(),now(),1)  -- 22
  ,(null,1,6,'sixth 3 page',now(),now(),1)  -- 23
-- page 7  
  ,(null,1,7,'seventh 1 page',now(),now(),1)  -- 24
  ,(null,1,7,'seventh 2 page',now(),now(),1)  -- 25
  ,(null,1,7,'seventh 3 page',now(),now(),1);  -- 26



select 'Building the inputs';

-- page 2 of book --> Guia_UV
  insert into bms_inputs_pages values
     (null,1, 'type', 'text',now(),now(),1)
    ,(null,1, 'name', 'a2',now(),now(),1)
    ,(null,1, 'id', 'a2',now(),now(),1)
    ,(null,1, 'autofocus', 'on',now(),now(),1)
    ,(null,1, 'placeholder', 'Responder Actividad 3 pregunta 1',now(),now(),1)
-- next field
    ,(null,2, 'type', 'text',now(),now(),1)
    ,(null,2, 'name', 'b2',now(),now(),1)
    ,(null,2, 'id', 'b2',now(),now(),1)
    ,(null,2, 'placeholder', 'Responder Actividad 3 pregunta 2',now(),now(),1)
  
    ,(null,3, 'type', 'text',now(),now(),1)
    ,(null,3, 'name', 'c222',now(),now(),1)
    ,(null,3, 'id', 'cx',now(),now(),1)
    ,(null,3, 'placeholder', 'Responder Actividad 3 pregunta 3',now(),now(),1)
    ,(null,3, 'class', 'blank',now(),now(),1)

    ,(null,4, 'type', 'text',now(),now(),1)
    ,(null,4, 'name', 'a2',now(),now(),1)
    ,(null,4, 'id', 'd2',now(),now(),1)
    ,(null,4, 'autofocus', 'on',now(),now(),1)
    ,(null,4, 'placeholder', 'Responder Actividad 3 pregunta 4',now(),now(),1)

    ,(null,5, 'type', 'text',now(),now(),1)
    ,(null,5, 'name', 'b2',now(),now(),1)
    ,(null,5, 'id', 'e2',now(),now(),1)
    ,(null,5, 'placeholder', 'Responder Actividad 3 pregunta 5',now(),now(),1)
 
    ,(null,6, 'type', 'text',now(),now(),1)
    ,(null,6, 'name', 'c222',now(),now(),1)
    ,(null,6, 'id', 'f2',now(),now(),1)
    ,(null,6, 'placeholder', 'Responder Actividad 3 pregunta 6',now(),now(),1)
    ,(null,6, 'class', 'blank',now(),now(),1)

    ,(null,7, 'type', 'text',now(),now(),1)
    ,(null,7, 'name', 'c222',now(),now(),1)
    ,(null,7, 'id', 'g2',now(),now(),1)
    ,(null,7, 'placeholder', 'Responder Actividad 3 pregunta 7',now(),now(),1)
    ,(null,7, 'class', 'blank',now(),now(),1)

    ,(null,8, 'type', 'text',now(),now(),1)
    ,(null,8, 'name', 'c222',now(),now(),1)
    ,(null,8, 'id', 'h2',now(),now(),1)
    ,(null,8, 'placeholder', 'Responder Actividad 3 pregunta 8',now(),now(),1)
    ,(null,8, 'class', 'blank',now(),now(),1)

    ,(null,9, 'type', 'text',now(),now(),1)
    ,(null,9, 'name', 'c222',now(),now(),1)
    ,(null,9, 'id', 'i2',now(),now(),1)
    ,(null,9, 'placeholder', 'Responder Actividad 3 pregunta 9',now(),now(),1)
    ,(null,9, 'class', 'blank',now(),now(),1)

-- New Box group page 3
    ,(null,10,"type","radio",now(),now(),1)
    ,(null,10,"name","actividad-one",now(),now(),1)
    ,(null,10,"id","a",now(),now(),1)
    ,(null,10,"class","option-inputradio",now(),now(),1)
    ,(null,10,"onclick","onlyOne(this,'actividad-one')",now(),now(),1)

    ,(null,11,"type","radio",now(),now(),1)
    ,(null,11,"name","actividad-one",now(),now(),1)
    ,(null,11,"id","b",now(),now(),1)
    ,(null,11,"class","option-inputradio",now(),now(),1)
    ,(null,11,"onclick","onlyOne(this,'actividad-one')",now(),now(),1)

    ,(null,12,"type","radio",now(),now(),1)
    ,(null,12,"name","actividad-one",now(),now(),1)
    ,(null,12,"id","c",now(),now(),1)
    ,(null,12,"class","option-inputradio",now(),now(),1)
    ,(null,12,"onclick","onlyOne(this,'actividad-one')",now(),now(),1)

    ,(null,13,"type","radio",now(),now(),1)
    ,(null,13,"name","actividad-one",now(),now(),1)
    ,(null,13,"id","d",now(),now(),1)
    ,(null,13,"class","option-inputradio",now(),now(),1)
    ,(null,13,"onclick","onlyOne(this,'actividad-one')",now(),now(),1)
-- NEXT GROUP
    ,(null,14,"type","radio",now(),now(),1)
    ,(null,14,"name","actividad-two",now(),now(),1)
    ,(null,14,"id","a2",now(),now(),1)
    ,(null,14,"class","option-inputradio",now(),now(),1)
    ,(null,14,"onclick","onlyOne(this,'actividad-two')",now(),now(),1)

    ,(null,15,"type","radio",now(),now(),1)
    ,(null,15,"name","actividad-two",now(),now(),1)
    ,(null,15,"id","b2",now(),now(),1)
    ,(null,15,"class","option-inputradio",now(),now(),1)
    ,(null,15,"onclick","onlyOne(this,'actividad-two')",now(),now(),1)

    ,(null,16,"type","radio",now(),now(),1)
    ,(null,16,"name","actividad-two",now(),now(),1)
    ,(null,16,"id","c2",now(),now(),1)
    ,(null,16,"class","option-inputradio",now(),now(),1)
    ,(null,16,"onclick","onlyOne(this,'actividad-two')",now(),now(),1)

    ,(null,17,"type","radio",now(),now(),1)
    ,(null,17,"name","actividad-two",now(),now(),1)
    ,(null,17,"id","d2",now(),now(),1)
    ,(null,17,"class","option-inputradio",now(),now(),1)
    ,(null,17,"onclick","onlyOne(this,'actividad-two')",now(),now(),1)

-- next page --> 5

    ,(null,18, 'type', 'text',now(),now(),1)
    ,(null,18, 'name', 'a',now(),now(),1)
    ,(null,18, 'id', 'a',now(),now(),1)
    ,(null,18, 'placeholder', 'response a5',now(),now(),1)
    ,(null,18, 'class', 'blank',now(),now(),1)

    ,(null,19, 'type', 'text',now(),now(),1)
    ,(null,19, 'name', 'b',now(),now(),1)
    ,(null,19, 'id', 'b',now(),now(),1)
    ,(null,19, 'placeholder', 'response b5',now(),now(),1)
    ,(null,19, 'class', 'blank',now(),now(),1)

    ,(null,20, 'type', 'text',now(),now(),1)
    ,(null,20, 'name', 'c',now(),now(),1)
    ,(null,20, 'id', 'c',now(),now(),1)
    ,(null,20, 'placeholder', 'response c5',now(),now(),1)
    ,(null,20, 'class', 'blank',now(),now(),1)

-- next --> 6
    ,(null,21, 'type', 'text',now(),now(),1)
    ,(null,21, 'name', 'a',now(),now(),1)
    ,(null,21, 'id', 'a',now(),now(),1)
    ,(null,21, 'placeholder', 'response a6',now(),now(),1)
    ,(null,21, 'class', 'blank',now(),now(),1)

    ,(null,22, 'type', 'text',now(),now(),1)
    ,(null,22, 'name', 'b',now(),now(),1)
    ,(null,22, 'id', 'b',now(),now(),1)
    ,(null,22, 'placeholder', 'response b6',now(),now(),1)
    ,(null,22, 'class', 'blank',now(),now(),1)

    ,(null,23, 'type', 'text',now(),now(),1)
    ,(null,23, 'name', 'c',now(),now(),1)
    ,(null,23, 'id', 'c',now(),now(),1)
    ,(null,23, 'placeholder', 'response c6',now(),now(),1)
    ,(null,23, 'class', 'blank',now(),now(),1)
-- page --> 7
    ,(null,24, 'type', 'text',now(),now(),1)
    ,(null,24, 'name', 'a',now(),now(),1)
    ,(null,24, 'id', 'a',now(),now(),1)
    ,(null,24, 'placeholder', 'response a7',now(),now(),1)
    ,(null,24, 'class', 'blank',now(),now(),1)

    ,(null,25, 'type', 'text',now(),now(),1)
    ,(null,25, 'name', 'b',now(),now(),1)
    ,(null,25, 'id', 'b',now(),now(),1)
    ,(null,25, 'placeholder', 'response b7',now(),now(),1)
    ,(null,25, 'class', 'blank',now(),now(),1)

    ,(null,26, 'type', 'text',now(),now(),1)
    ,(null,26, 'name', 'c',now(),now(),1)
    ,(null,26, 'id', 'c',now(),now(),1)
    ,(null,26, 'placeholder', 'response c7',now(),now(),1)
    ,(null,26, 'class', 'blank',now(),now(),1)
;

-- NOTE End of the layer of the book
-- NOTE Start values by user

-- NOTE Examples values for hipotetically user with id 1702 
insert into bms_inputs_values values
-- NOTE this is linked whith firsts input linked to PAGE 2
	 (null,1,1702,'value','Previous Answer not Editable',now(),now(),1)
	,(null,1,1702,'disabled','disabled',now(),now(),1)
	,(null,1,1702,'style','color:magenta!important',now(),now(),1)
	,(null,1,1702,'class','saved',now(),now(),1)
 -- NOTE next item input id 2 
	,(null,2,1702,'value','Previous Answer and Editable',now(),now(),1)
	,(null,2,1702,'class','edition',now(),now(),1)
-- NOTE item with id 3 as a blank value 

-- NOTE next form item input id 4
	,(null,4,1702,'value','Previous Answer not Editable',now(),now(),1)
	,(null,4,1702,'disabled','disabled',now(),now(),1)
	,(null,4,1702,'style','color:magenta!important',now(),now(),1)
	,(null,4,1702,'class','saved',now(),now(),1)

	,(null,5,1702,'value','Previous Answer and Editable',now(),now(),1)
	,(null,5,1702,'class','edition',now(),now(),1)

-- input  6 to  9 are blank 
	
       ,(null,10,1702,"value","a",now(),now(),1)
--	(null,10,1702,'checked','checked',now(),now(),1)
       ,(null,11,1702,"value","b",now(),now(),1)
       ,(null,12,1702,"value","c",now(),now(),1)
       ,(null,13,1702,"value","d",now(),now(),1)

       ,(null,14,1702,"value","a",now(),now(),1)
       ,(null,15,1702,"value","b",now(),now(),1)
       ,(null,16,1702,"value","c",now(),now(),1)
       ,(null,17,1702,"value","d",now(),now(),1)
;
-- inputs page 5 to 9 are empty in this example


-- ////////////////////////////////////   Next Book ////////////////////////////////////////

insert into 
    bms_books values
    (null, 228, 8, 'Guia UV', false, now(), now(), true);

select 'create the pages';

insert into 
    bms_bookpages values 
     (null, 228, 1, "", 'https://ediq.mx/public/guias/guia_demo_uv_040321_2/pages/1.jpg', now(), now(), true)
    ,(null, 228, 2, '/src/bms/src', '/book-source/guia/unam/001/pages/14.jpg', now(), now(), true)
    ,(null, 228, 3, '/src/bms/src', '/book-source/guia/unam/001/pages/15.jpg', now(), now(), true)
    ,(null, 228, 4, '/src/bms/src', '/book-source/guia/unam/001/pages/147.jpg', now(), now(), true)
    ,(null, 228, 5, '/src/bms/src', '/book-source/guia/unam/001/pages/35.jpg', now(), now(), true)
    ,(null, 228, 6, '/src/bms/src', '/book-source/guia/unam/001/pages/36.jpg', now(), now(), true)
    ,(null, 228, 7, "", 'https://ediq.mx/public/guias/guia_demo_uv_040321_2/pages/7.jpg', now(), now(), true)
    ,(null, 228, 8, '/src/bms/src', '/book-source/guia/unam/001/pages/180.jpg', now(), now(), true);

select 'Set the positions';
insert into bms_positions values
(
      null,
      228,
      10,
      1,
      '',
      '',
      '',
      '',
      '',
      '',
      now(),
      now(),
      1
  ),
(
      null,
      228,
      10,
      2,
      '',
      '',
      '',
      '',
      '',
      '.pages_2>form>#a2{top:54.4%;left:14.4%;width:67%;} .pages_2>form>#b2{top:55.9%;left:20.4%;width:61.3%;} .pages_2>form>#cx{top:57.4%;left:20.4%;width:61.2%;} .pages_2>form>#d2{top:69.5%;left:14.4%;width:67%;} .pages_2>form>#e2{top:71.1%;left:20.4%;width:61.3%;} .pages_2>form>#f2{top:72.5%;left:20.4%;width:61.2%;}.pages_2>form>#g2{top:89.1%;left:14.4%;width:67%;}.pages_2>form>#h2{top:90.6%;left:20.4%;width:61.3%;}.pages_2>form>#i2{top:92.2%;left:20.4%;width:61.2%;}',
      now(),
      now(),
      1
  ),
(
      null,
      228,
      11,
      3,
      '',
      '',
      '',
      '',
      '',
      '.pages_3 > form > #a{top:36%;left:48.7%;width:3%;} .pages_3 > form > #b{top:37.5%;left:48.7%;width:3%;} .pages_3 > form > #c{top:38.9%;left:48.7%;width:3%;} .pages_3 > form > #d{top:40.4%;left:48.7%;width:3%;} .pages_3 > form > #a2{top:44.9%;left:48.7%;width:3%;} .pages_3 > form > #b2{top:46.3%;left:48.7%;width:3%;} .pages_3 > form > #c2{top:47.8%;left:48.7%;width:3%;} .pages_3 > form > #d2{top:49.2%;left:48.7%;width:3%;}',
      now(),
      now(),
      1
  ),
(
      null,
      228,
      10,
      4,
      '',
      '',
      '',
      '',
      '',
      '',
      now(),
      now(),
      1
  ),
(
      null,
      228,
      13,
      5,
      '',
      '',
      '',
      '',
      '',
      '.pages_5 > form > #a{top:70.2%;left:190px;width:3%;} .pages_5 > form > #b{bottom:17.5%;right:66.5%;width:3%;} .pages_5 > form > #c{top:80.2%;left:43%;width:3%;}',
      now(),
      now(),
      1
  ),
(
      null,
      228,
      14,
      6,
      '',
      '',
      '',
      '',
      '',
      ' .pages_6 > form > #a{top:60.2%;left:190px;width:3%;} .pages_6 > form > #b{bottom:17.5%;right:66.5%;width:3%;} .pages_6 > form > #c{top:80.2%;left:43%;width:3%;}',
      now(),
      now(),
      1
  ),
(
      null,
      228,
      15,
      7,
      '',
      '',
      '',
      '',
      '',
      ' .pages_7 > form > #a{top:90.2%;left:190px;width:3%;} .pages_7 > form > #b{bottom:17.5%;right:66.5%;width:3%;} .pages_7 > form > #c{top:80.2%;left:43%;width:3%;}',
      now(),
      now(),
      1
  ),
(
      null,
      228,
      16,
      8,
      '',
      '',
      '',
      '',
      '',
      '.pages_8 > form > #a{top:90.2%;left:190px;width:3%;} .pages_8 > form > #b{bottom:17.5%;right:66.5%;width:3%;} .pages_8 > form > #c{top:80.2%;left:43%;width:3%;}',
      now(),
      now(),
      1
  ); 

select 'Create a input';

  insert into bms_inputs_ctrls values 
 -- page 1
   (null,228,1,'Empty group of inputs for page 1 in book_id 1 ',now(),now(),1)   -- 27
 -- page 2
  ,(null,228,2,'Primer group of inputs 1 in book_id 1 ',now(),now(),1)   -- 27
  ,(null,228,2,'firts group of inputs 2 in book_id 1 ',now(),now(),1)   -- 28
  ,(null,228,2,'firts group of inputs 3 in book_id 1 ',now(),now(),1)   -- 29
  ,(null,228,2,'firts group of inputs 4 in book_id 1 ',now(),now(),1)   -- 30
  ,(null,228,2,'firts group of inputs 5 in book_id 1 ',now(),now(),1)   -- 31
  ,(null,228,2,'firts group of inputs 6 in book_id 1 ',now(),now(),1)   -- 32
  ,(null,228,2,'firts group of inputs 7 in book_id 1 ',now(),now(),1)   -- 33
  ,(null,228,2,'firts group of inputs 8 in book_id 1 ',now(),now(),1)   -- 34
  ,(null,228,2,'firts group of inputs 9 in book_id 1 ',now(),now(),1)   -- 35
-- page 3
  ,(null,228,3,'second 1 page',now(),now(),1)  -- 36
  ,(null,228,3,'second 2 page',now(),now(),1)  -- 37
  ,(null,228,3,'second 3 page',now(),now(),1)  -- 38
  ,(null,228,3,'second 4 page',now(),now(),1)  -- 39
  ,(null,228,3,'second 5 page',now(),now(),1)  -- 40
  ,(null,228,3,'second 6 page',now(),now(),1)  -- 41
  ,(null,228,3,'second 7 page',now(),now(),1)  -- 42
  ,(null,228,3,'second 8 page',now(),now(),1)  -- 43
-- page 4
  ,(null,228,4,'Empty group of inputs for page 4 in book_id 1 ',now(),now(),1)   -- 27
-- page 5
  ,(null,228,5,'fifth 1 page',now(),now(),1)  -- 44
  ,(null,228,5,'fifth 2 page',now(),now(),1)  -- 45
  ,(null,228,5,'fifth 3 page',now(),now(),1)  -- 46
-- page 6  
  ,(null,228,6,'sixth 1 page',now(),now(),1)  -- 47
  ,(null,228,6,'sixth 2 page',now(),now(),1)  -- 48
  ,(null,228,6,'sixth 3 page',now(),now(),1)  -- 49
-- page 7  
  ,(null,228,7,'seventh 1 page',now(),now(),1)  -- 50
  ,(null,228,7,'seventh 2 page',now(),now(),1)  -- 51
  ,(null,228,7,'seventh 3 page',now(),now(),1)  -- 52
-- page 8
  ,(null,228,8,'Empty group of inputs for page 8 in book_id 1 ',now(),now(),1);   -- 27


select 'Building the inputs';

-- page 2 of book --> Guia_UV
  insert into bms_inputs_pages values
     (null,27, 'type', 'text',now(),now(),1)
    ,(null,27, 'name', 'a2',now(),now(),1)
    ,(null,27, 'id', 'a2',now(),now(),1)
    ,(null,27, 'autofocus', 'on',now(),now(),1)
    ,(null,27, 'placeholder', 'Responder Actividad 3 pregunta 1',now(),now(),1)
-- next field
    ,(null,28, 'type', 'text',now(),now(),1)
    ,(null,28, 'name', 'b2',now(),now(),1)
    ,(null,28, 'id', 'b2',now(),now(),1)
    ,(null,28, 'placeholder', 'Responder Actividad 3 pregunta 2',now(),now(),1)
  
    ,(null,29, 'type', 'text',now(),now(),1)
    ,(null,29, 'name', 'c222',now(),now(),1)
    ,(null,29, 'id', 'cx',now(),now(),1)
    ,(null,29, 'placeholder', 'Responder Actividad 3 pregunta 3',now(),now(),1)
    ,(null,29, 'class', 'blank',now(),now(),1)

    ,(null,30, 'type', 'text',now(),now(),1)
    ,(null,30, 'name', 'a2',now(),now(),1)
    ,(null,30, 'id', 'd2',now(),now(),1)
    ,(null,30, 'autofocus', 'on',now(),now(),1)
    ,(null,30, 'placeholder', 'Responder Actividad 3 pregunta 4',now(),now(),1)

    ,(null,31, 'type', 'text',now(),now(),1)
    ,(null,31, 'name', 'b2',now(),now(),1)
    ,(null,31, 'id', 'e2',now(),now(),1)
    ,(null,31, 'placeholder', 'Responder Actividad 3 pregunta 5',now(),now(),1)
 
    ,(null,32, 'type', 'text',now(),now(),1)
    ,(null,32, 'name', 'c222',now(),now(),1)
    ,(null,32, 'id', 'f2',now(),now(),1)
    ,(null,32, 'placeholder', 'Responder Actividad 3 pregunta 6',now(),now(),1)
    ,(null,32, 'class', 'blank',now(),now(),1)

    ,(null,33, 'type', 'text',now(),now(),1)
    ,(null,33, 'name', 'c222',now(),now(),1)
    ,(null,33, 'id', 'g2',now(),now(),1)
    ,(null,33, 'placeholder', 'Responder Actividad 3 pregunta 7',now(),now(),1)
    ,(null,33, 'class', 'blank',now(),now(),1)

    ,(null,34, 'type', 'text',now(),now(),1)
    ,(null,34, 'name', 'c222',now(),now(),1)
    ,(null,34, 'id', 'h2',now(),now(),1)
    ,(null,34, 'placeholder', 'Responder Actividad 3 pregunta 8',now(),now(),1)
    ,(null,34, 'class', 'blank',now(),now(),1)

    ,(null,35, 'type', 'text',now(),now(),1)
    ,(null,35, 'name', 'c222',now(),now(),1)
    ,(null,35, 'id', 'i2',now(),now(),1)
    ,(null,35, 'placeholder', 'Responder Actividad 3 pregunta 9',now(),now(),1)
    ,(null,35, 'class', 'blank',now(),now(),1)

-- New Box group page 3
    ,(null,36,"type","radio",now(),now(),1)
    ,(null,36,"name","actividad-one",now(),now(),1)
    ,(null,36,"id","a",now(),now(),1)
    ,(null,36,"class","option-inputradio",now(),now(),1)
    ,(null,36,"onclick","onlyOne(this,'actividad-one')",now(),now(),1)

    ,(null,37,"type","radio",now(),now(),1)
    ,(null,37,"name","actividad-one",now(),now(),1)
    ,(null,37,"id","b",now(),now(),1)
    ,(null,37,"class","option-inputradio",now(),now(),1)
    ,(null,37,"onclick","onlyOne(this,'actividad-one')",now(),now(),1)

    ,(null,38,"type","radio",now(),now(),1)
    ,(null,38,"name","actividad-one",now(),now(),1)
    ,(null,38,"id","c",now(),now(),1)
    ,(null,38,"class","option-inputradio",now(),now(),1)
    ,(null,38,"onclick","onlyOne(this,'actividad-one')",now(),now(),1)

    ,(null,39,"type","radio",now(),now(),1)
    ,(null,39,"name","actividad-one",now(),now(),1)
    ,(null,39,"id","d",now(),now(),1)
    ,(null,39,"class","option-inputradio",now(),now(),1)
    ,(null,39,"onclick","onlyOne(this,'actividad-one')",now(),now(),1)
-- NEXT GROUP
    ,(null,40,"type","radio",now(),now(),1)
    ,(null,40,"name","actividad-two",now(),now(),1)
    ,(null,40,"id","a2",now(),now(),1)
    ,(null,40,"class","option-inputradio",now(),now(),1)
    ,(null,40,"onclick","onlyOne(this,'actividad-two')",now(),now(),1)

    ,(null,41,"type","radio",now(),now(),1)
    ,(null,41,"name","actividad-two",now(),now(),1)
    ,(null,41,"id","b2",now(),now(),1)
    ,(null,41,"class","option-inputradio",now(),now(),1)
    ,(null,41,"onclick","onlyOne(this,'actividad-two')",now(),now(),1)

    ,(null,42,"type","radio",now(),now(),1)
    ,(null,42,"name","actividad-two",now(),now(),1)
    ,(null,42,"id","c2",now(),now(),1)
    ,(null,42,"class","option-inputradio",now(),now(),1)
    ,(null,42,"onclick","onlyOne(this,'actividad-two')",now(),now(),1)

    ,(null,43,"type","radio",now(),now(),1)
    ,(null,43,"name","actividad-two",now(),now(),1)
    ,(null,43,"id","d2",now(),now(),1)
    ,(null,43,"class","option-inputradio",now(),now(),1)
    ,(null,43,"onclick","onlyOne(this,'actividad-two')",now(),now(),1)

-- next page --> 5

    ,(null,44, 'type', 'text',now(),now(),1)
    ,(null,44, 'name', 'a',now(),now(),1)
    ,(null,44, 'id', 'a',now(),now(),1)
    ,(null,44, 'placeholder', 'response a5',now(),now(),1)
    ,(null,44, 'class', 'blank',now(),now(),1)

    ,(null,45, 'type', 'text',now(),now(),1)
    ,(null,45, 'name', 'b',now(),now(),1)
    ,(null,45, 'id', 'b',now(),now(),1)
    ,(null,45, 'placeholder', 'response b5',now(),now(),1)
    ,(null,45, 'class', 'blank',now(),now(),1)

    ,(null,46, 'type', 'text',now(),now(),1)
    ,(null,46, 'name', 'c',now(),now(),1)
    ,(null,46, 'id', 'c',now(),now(),1)
    ,(null,46, 'placeholder', 'response c5',now(),now(),1)
    ,(null,46, 'class', 'blank',now(),now(),1)

-- next --> 6
    ,(null,47, 'type', 'text',now(),now(),1)
    ,(null,47, 'name', 'a',now(),now(),1)
    ,(null,47, 'id', 'a',now(),now(),1)
    ,(null,47, 'placeholder', 'response a6',now(),now(),1)
    ,(null,47, 'class', 'blank',now(),now(),1)

    ,(null,48, 'type', 'text',now(),now(),1)
    ,(null,48, 'name', 'b',now(),now(),1)
    ,(null,48, 'id', 'b',now(),now(),1)
    ,(null,48, 'placeholder', 'response b6',now(),now(),1)
    ,(null,48, 'class', 'blank',now(),now(),1)

    ,(null,49, 'type', 'text',now(),now(),1)
    ,(null,49, 'name', 'c',now(),now(),1)
    ,(null,49, 'id', 'c',now(),now(),1)
    ,(null,49, 'placeholder', 'response c6',now(),now(),1)
    ,(null,49, 'class', 'blank',now(),now(),1)
-- page --> 7
    ,(null,50, 'type', 'text',now(),now(),1)
    ,(null,50, 'name', 'a',now(),now(),1)
    ,(null,50, 'id', 'a',now(),now(),1)
    ,(null,50, 'placeholder', 'response a7',now(),now(),1)
    ,(null,50, 'class', 'blank',now(),now(),1)

    ,(null,51, 'type', 'text',now(),now(),1)
    ,(null,51, 'name', 'b',now(),now(),1)
    ,(null,51, 'id', 'b',now(),now(),1)
    ,(null,51, 'placeholder', 'response b7',now(),now(),1)
    ,(null,51, 'class', 'blank',now(),now(),1)

    ,(null,52, 'type', 'text',now(),now(),1)
    ,(null,52, 'name', 'c',now(),now(),1)
    ,(null,52, 'id', 'c',now(),now(),1)
    ,(null,52, 'placeholder', 'response c7',now(),now(),1)
    ,(null,52, 'class', 'blank',now(),now(),1)
;


-- note Start values by user

-- NOTE Examples values for hipotetically user with id 1702 
insert into bms_inputs_values values
-- NOTE this is linked whith firsts input linked to PAGE 2
	 (null,27,1702,'value','Previous Answer not Editable',now(),now(),1)
	,(null,27,1702,'disabled','disabled',now(),now(),1)
	,(null,27,1702,'style','color:magenta!important',now(),now(),1)
	,(null,27,1702,'class','saved',now(),now(),1)
 -- NOTE next item input id 2 
	,(null,28,1702,'value','Previous Answer and Editable',now(),now(),1)
	,(null,28,1702,'class','edition',now(),now(),1)
-- NOTE item with id 3 as a blank value 

-- NOTE next form item input id 4
	,(null,30,1702,'value','Previous Answer not Editable',now(),now(),1)
	,(null,30,1702,'disabled','disabled',now(),now(),1)
	,(null,30,1702,'style','color:magenta!important',now(),now(),1)
	,(null,30,1702,'class','saved',now(),now(),1)

	,(null,31,1702,'value','Previous Answer and Editable',now(),now(),1)
	,(null,31,1702,'class','edition',now(),now(),1)

-- input  6 to  9 are blank 
	
       ,(null,36,1702,"value","a",now(),now(),1)
--	(null,36,1702,'checked','checked',now(),now(),1)
       ,(null,37,1702,"value","b",now(),now(),1)
       ,(null,38,1702,"value","c",now(),now(),1)
       ,(null,39,1702,"value","d",now(),now(),1)

       ,(null,40,1702,"value","a",now(),now(),1)
       ,(null,41,1702,"value","b",now(),now(),1)
       ,(null,42,1702,"value","c",now(),now(),1)
       ,(null,43,1702,"value","d",now(),now(),1)
;
-- inputs page 5 to 9 are empty in this example


