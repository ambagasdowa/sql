-- drop database if exists ttrss ;
-- create database db_ediq2021;
drop user if exists ediq@localhost;
-- print 'User is dropped'
grant usage on db_ediq2021.* to ediq@localhost identified by 'db_ediq2021#';
-- grant select, insert, update, delete, drop, alter, create , create temporary tables on db_ediq2021.* to ediq@localhost;
-- grant file on *.* to 'ediq'@'localhost';
grant all privileges on db_ediq2021.* to ediq@localhost;
flush privileges;



/*
*drop database uruk;
*
*create database uruk;
*
*create or replace user `uruk`@`localhost` IDENTIFIED by '@uruk#';
*grant usage on uruk.* to uruk@localhost identified by '@uruk#';
*grant select, insert, update, delete, drop, alter, create , create temporary tables on uruk.* to uruk@localhost;
*grant file on *.* to 'uruk'@'localhost';
*flush privileges;
*SHOW GRANTS FOR 'uruk'@'localhost';
*GRANT ALL PRIVILEGES ON *.* TO uruk@'localhost' with grant option;
*flush privileges;
*/
