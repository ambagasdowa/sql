/* NOTE testing table 
*/


/*
CREATE TABLE geolocation (
  ID INTEGER NOT NULL AUTO_INCREMENT, 
  IP varchar(12) default null , 
  COUNTRY varchar(120) default null,
  COUNTRYCODE varchar(5) default null,  
  REGION varchar(5) default null,
  REGIONNAME varchar(120) default null,
  CITY varchar(120) default null,
  ZIP int default null, 
  LATITUDE decimal(8,6) default null,
  LONGITUDE decimal(9,6) default null,
  TIMEZONE varchar(120) default null
  ISP varchar(180) default null,
  ORG varchar(180) default null,
  AS varchar(120) default null,
  OSM varchar(2083) default null,
  GOOGLE varchar(2083) default null,
  BING varchar(2083) default null,
  OSMOSE varchar(2083) default null,
  HERE varchar(2083) default null,
  SERVER_ONE varchar(2083) default null,
  SERVER_TWO varchar(2083) default null,
  PRIMARY KEY (ID)) ENGINE=INNODB;"
*/


select CLIENT,IP,LATITUDE,LONGITUDE,CITY,CREATED,TIMEHOST from ocsweb.geolocation;


create table `ocsweb`.`geolog`(
  id INTEGER not null AUTO_INCREMENT,
  description text default null,
  created DATETIME NULL DEFAULT NOW(),
  primary key (id)
) ENGINE=INNODB;


DELIMITER //
CREATE or replace TRIGGER build_urls_geolocation
before INSERT ON `ocsweb`.`geolocation`
FOR EACH ROW
BEGIN
 insert into `ocsweb`.`geolog`(description) values('HARDWARE_ID => '+ cast(NEW.HARDWARE_ID as varchar(255)) + 'id => ' + cast(NEW.id as varchar(255)) + 'CLIENT => '+ cast(NEW.CLIENT as varchar(255)));
END; //

DELIMITER ;

drop trigger ocsweb.build_urls_geolocation






























































