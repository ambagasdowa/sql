/*
 * The default root user accounts created by mysql_install_db have this privilege
 */

CREATE USER 'root'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost' WITH GRANT OPTION;
GRANT PROXY ON ''@'%' TO 'root'@'localhost' WITH GRANT OPTION;

/*
Grant Examples
Granting Root-like Privileges

You can create a user that has privileges similar to the default root accounts by executing the following:
*/
CREATE USER 'alexander'@'localhost';
GRANT ALL PRIVILEGES ON  *.* to 'alexander'@'localhost' WITH GRANT OPTION;




drop user portal_cloud@localhost

grant usage on portal_nextcloud.* to portal_nextcloud@localhost identified by '@portal_nextcloud#';
grant select, insert, update, delete, drop, alter, create , create temporary tables on portal_nextcloud.* to portal_nextcloud@localhost;
flush privileges;




drop database uruk;

create database uruk;

create or replace user `uruk`@`localhost` IDENTIFIED by '@uruk#';
grant usage on uruk.* to uruk@localhost identified by '@uruk#';
grant select, insert, update, delete, drop, alter, create , create temporary tables on uruk.* to uruk@localhost;
grant file on *.* to 'uruk'@'localhost';
flush privileges;
SHOW GRANTS FOR 'uruk'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO uruk@'localhost' with grant option;
flush privileges;
