
drop database uruk;

create database uruk;

create or replace user `uruk`@`localhost` IDENTIFIED by '@uruk#';
grant usage on uruk.* to uruk@localhost identified by '@uruk#';
grant select, insert, update, delete, drop, alter, create , create temporary tables on uruk.* to uruk@localhost;
grant file on *.* to 'uruk'@'localhost';
flush privileges;
SHOW GRANTS FOR 'uruk'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO uruk@'localhost' with grant option;
flush privileges;


