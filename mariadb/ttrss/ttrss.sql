-- Just automatic db creation script
-- Copyright © 2022 Ambagasdowa
drop database if exists ttrss_uruk ;
create database ttrss_uruk;
DROP USER IF EXISTS ttrss@localhost;
grant usage on ttrss_uruk.* to 'ttrss'@'localhost' identified by '@ttrss#';
GRANT ALL PRIVILEGES ON ttrss_uruk.* TO 'ttrss'@'localhost' WITH GRANT OPTION;
-- GRANT RELOAD ON ttrss_uruk.* TO 'ttrss'@'localhost';
 FLUSH PRIVILEGES;
