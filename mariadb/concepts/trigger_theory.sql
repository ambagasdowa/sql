-- NOTE this is for MSSQL
-- Determine if this is an INSERT,UPDATE, or DELETE Action or a "failed delete".

--DECLARE @Action as char(1);
--    SET @Action = (CASE WHEN EXISTS(SELECT * FROM INSERTED)
--                         AND EXISTS(SELECT * FROM DELETED)
--                        THEN 'U'  -- Set Action to Updated.
--                        WHEN EXISTS(SELECT * FROM INSERTED)
--                        THEN 'I'  -- Set Action to Insert.
--                        WHEN EXISTS(SELECT * FROM DELETED)
--                        THEN 'D'  -- Set Action to Deleted.
--                        ELSE NULL -- Skip. It may have been a "failed delete".   
--                    END
--		)
--
--
--
--CREATE TRIGGER dbo.TableName_IUD
--ON dbo.TableName
--AFTER INSERT, UPDATE, DELETE
--AS 
--BEGIN
--    SET NOCOUNT ON;
--
--    --
--    -- Check if this is an INSERT, UPDATE or DELETE Action.
--    -- 
--    DECLARE @action as char(1);
--
--    SET @action = 'I'; -- Set Action to Insert by default.
--    IF EXISTS(SELECT * FROM DELETED)
--    BEGIN
--        SET @action = 
--            CASE
--                WHEN EXISTS(SELECT * FROM INSERTED) THEN 'U' -- Set Action to Updated.
--                ELSE 'D' -- Set Action to Deleted.       
--            END
--    END
--    ELSE 
--        IF NOT EXISTS(SELECT * FROM INSERTED) RETURN; -- Nothing updated or inserted.
--
----    ...
--
--    END




