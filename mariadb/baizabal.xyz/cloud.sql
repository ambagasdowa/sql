drop database if EXISTS nextcloud;
drop user if exists 'cloud.uruk'@'localhost';

CREATE USER 'cloud.uruk'@'localhost' IDENTIFIED BY '@cloud.uruk#';
CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER, CREATE TEMPORARY TABLES ON nextcloud.* TO 'cloud.uruk'@'localhost' IDENTIFIED BY '@cloud.uruk#';
FLUSH privileges;

-- CREATE USER 'cloud.uruk'@'localhost' IDENTIFIED BY '@cloud.uruk#';
-- CREATE DATABASE IF NOT EXISTS nextcloud.uruk CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
-- GRANT ALL PRIVILEGES on nextcloud.uruk.* to 'cloud.uruk'@'localhost';
-- FLUSH privileges;
