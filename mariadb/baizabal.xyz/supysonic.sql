
drop database supysonic;

create database supysonic;

create or replace user `supysonic`@`localhost` IDENTIFIED by '@supysonic#';
grant usage on supysonic.* to supysonic@localhost identified by '@supysonic#';
grant select, insert, update, delete, drop, alter, create , create temporary tables on supysonic.* to supysonic@localhost;
grant file on *.* to 'supysonic'@'localhost';
flush privileges;
SHOW GRANTS FOR 'supysonic'@'localhost';

-- GRANT ALL PRIVILEGES ON *.* TO supysonic@'localhost' with grant option;
-- flush privileges;


