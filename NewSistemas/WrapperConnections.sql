
select * from [192.168.20.235].sistemas.dbo.projections_configs where projections_type_configs_id = 4


select * from [192.168.20.235].sistemas.dbo.projections_view_bussiness_units


use sistemas 

exec sp_desc projections_view_bussiness_units



use sistemas
IF OBJECT_ID('external_view_bussiness_units', 'U') IS NOT NULL 
  DROP TABLE external_view_bussiness_units; 
-- go
set ansi_nulls on
-- go
set quoted_identifier on
-- go
set ansi_padding on
-- go
 
create table [dbo].[external_view_bussiness_units](
		id								int identity(1,1),
		projections_corporations_id		int,			
		id_area							int,					
		name							varchar(100),			
		label							varchar(100),		
		tname							varchar(10),
		created							datetime,
		modified						datetime,
		user_id							int,
		_status							tinyint default 1 null
) on [primary]
-- go
set ansi_padding off

insert into sistemas.dbo.external_view_bussiness_units
	select * from [192.168.20.235].sistemas.dbo.projections_view_bussiness_units



use sistemas;

IF OBJECT_ID ('general_view_bussiness_units', 'V') IS NOT NULL
    DROP VIEW general_view_bussiness_units;
-- now build the view
create view projections_view_bussiness_units
with encryption
as
with "bunits" as
(
    select
		row_number()
	over 
		(order by name) as 
							 id
							,projections_corporations_id
							,id_area
							,name
							,isnull(label,name) as 'label'
	from(
			--select (select 0) as 'projections_corporations_id',(select 0) as 'id_area', (select 'SIN ASIGNAR') as 'name'
			--union all
			select
					 1 as 'projections_corporations_id'
					,tbk.id_area as 'id_area'
					,ltrim(rtrim(replace(replace(replace(replace(replace(tbk.nombre ,'AUTOTRANSPORTE' , ''),' S.A. DE C.V.',''),'BONAMPAK',''),'TRANSPORTADORA ESPECIALIZADA INDUSTRIAL','CUAUTITLAN'),'TRANSPORTE DE CARGA GEMINIS','TULTITLAN'))) as 'name'
					,label.label
			from 
					[192.168.20.235].gstdb.dbo.general_area as tbk
					left join 
								(
									select 
											module_data_definition,module_field_translation as 'label' 
									from 
											[192.168.20.235].sistemas.dbo.projections_configs where projections_type_configs_id = 4
								) as label 
					on label.module_data_definition collate SQL_Latin1_General_CP1_CI_AS = (ltrim(rtrim(replace(replace(replace(replace(replace(tbk.nombre ,'AUTOTRANSPORTE' , ''),' S.A. DE C.V.',''),'BONAMPAK',''),'TRANSPORTADORA ESPECIALIZADA INDUSTRIAL','CUAUTITLAN'),'TRANSPORTE DE CARGA GEMINIS','TULTITLAN'))))
			where 
					tbk.id_area <> 0
		) as result
)
	select 
			 "units".id
			,"units".projections_corporations_id
			,"units".id_area 
			,"units".name
			,"units".label
			,"cia".IDSL as 'tname'
	from 
			"bunits" as "units"
	inner join 
			(
				select IDSL,AreaLIS from integraapp.dbo.xrefcia
			)
		as "cia" on substring("units".label,1,7) = substring("cia".AreaLis,1,7)
		
		
		