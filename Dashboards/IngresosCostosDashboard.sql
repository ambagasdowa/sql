/*
===============================================================================
 Author         : Jesus Baizabal
 email			    : ambagasdowa@gmail.com
 Create date    : November 1, 2020
 Description    : Simple Query for a Dashboard 
 @license       : MIT License (http://www.opensource.org/licenses/mit-license.php)
 Database owner : GST Corp.
 @status        : Stable
 @version				: 0.00.01
 ===============================================================================
 */
-- NOTE Main Years Done

-- select * from sistemas.dbo.reporter_views_main_years



-- NOTE Presupuesto
select 
	 company
	,id_area
	,area
	,frt
	,cyear
	,mes
	,periodo
	,subtotal
	,tname
	,PresupuestoIngresos
	,PresupuestoTon
	,PresupuestoKms
	,PresupuestoViajes
from
	"sistemas"."dbo"."ingresos_costos_view_ind_ppto_ingresos"
where 
	cyear in (select cyear from [192.168.20.240].sistemas.dbo.reporter_views_main_years) 

 -- NOTE tables / Views for dashboard 
-- DEBUG  NOT WORKING
use sistemas
IF OBJECT_ID ('dashboard_view_costos_ingresos', 'V') IS NOT NULL
	drop view dashboard_view_costos_ingresos;

--create 
alter 
view dashboard_view_costos_ingresos
as                                                                             
select --top 10
		 "acc".Mes
		,"acc".Compania
		,"areas".id_area
		,"areas".label as 'area'
		,sum("acc".Abono) as 'Abono'
		,sum("acc".Cargo) as 'Cargo'
		,( sum("acc".Cargo) - sum("acc".Abono) ) as 'Real'
		,sum("acc".Presupuesto) as 'Presupuesto'
		,"acc".FiscYr as 'Año'
		,"acc".[_key] as 'rpto'
from --  
		sistemas.dbo.reporter_view_report_accounts as "acc"
	inner join 
		[192.168.20.235].sistemas.dbo.projections_view_bussiness_units as "areas"
	on 
		"areas".tname collate SQL_Latin1_General_CP1_CI_AS = "acc".Compania
where 
			"acc"."_key" in  ('OF','OV','MF','MV') 
		and 
			"acc".UnidadNegocio not in ('00') and "acc".FiscYr in (select cyear from sistemas.dbo.reporter_views_main_years) 
group by 
		 "acc".Mes,"acc".Compania,"acc".FiscYr,"acc".[_key]
		,"areas".id_area, "areas".label

--order by "acc".Mes,"acc".FiscYr

--  select * from sistemas.dbo.projections_view_full_gst_core_ops_indicators

-- select top 10 * from dashboard_view_costos_ingresos
-- DEBUG Working
use sistemas
IF OBJECT_ID ('dashboard_view_costos_ingresos', 'V') IS NOT NULL
	drop view dashboard_view_costos_ingresos;

--create 
alter 
view dashboard_view_costos_ingresos
as                                                                             
select 
       "acc".mes as 'Mes'
			,'' as 'Compania'
			,"areas".id_area
			,"acc".area 
			,'' as 'Abono'
			,'' as 'Cargo'
			,sum("acc"."Real") as 'Real'
			,sum("acc"."Presupuesto") as 'Presupuesto'
			,"acc".cyear as 'Año'
--			,_period
			,"acc".[type] as 'rpto'
from 
			sistemas.dbo.reporter_costos as "acc"
	inner join 
-- select * from  
		[192.168.20.235].sistemas.dbo.projections_view_bussiness_units as "areas"
	on 
		"areas".label collate SQL_Latin1_General_CP1_CI_AS = "acc".area
 where 
		"acc".cyear in (select cyear from sistemas.dbo.reporter_views_main_years) 
	and 
		"acc".[type] not in ('AD')
 group by 
		"acc".mes,"acc".area,"areas".id_area,"acc".cyear,"acc".[type]







EXEC sp_columns 'projections_view_indicators_dispatch_periods_full_first_ops'



select top 100 * from sistemas.dbo.projections_view_indicators_dispatch_periods_full_first_ops 
where 
		mes = 'Noviembre'
	and
		cyear in (select cyear from [192.168.20.240].sistemas.dbo.reporter_views_main_years) 
	and id_area = 1













